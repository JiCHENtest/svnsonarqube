
/**
 * @author Capgemini 
 */

/**
 * @Module Name - utilService
 * @Description - Singleton class for webservices, This is common gateway for all fetch/post calls 
 */

import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';
import { JsonstoreService } from './jsonstore';
import { LocalStorage } from './localStorage';
import { HandleError } from './handleerror';
import {ConstantData} from "./constantService";
import { GatewayService } from '../global/utilService';
declare var WLResourceRequest;
//declare var WL;
//declare var WLAuthorizationManager;



@Injectable()
export class GbShareService {

  public isUserLoggedIn : Boolean = true;
  public isFirstTimeUser : Boolean = true;
  loader;
  public isLoaderOn:Boolean=false;
  XPAX : any;
  isPrepaidVal : boolean;
  mobileNumber:any;
  BillingAccountNo:any;
  customerInfo:any;
  selectedMobileNumber:any;
  PhonebookNumber:any;
  checkingValue:number = 0;

  constructor(public alertCtrl:AlertController, public loadingCtrl:LoadingController, public jsonstoreService:JsonstoreService, public localStorage:LocalStorage,public handleerror: HandleError,public gatewayService: GatewayService){
    //this.MfpInit();
  };



  MfpInit() {
    console.debug('-- trying to init WL client');
  }

  
  
GroupSharing(params) {
  
  try{
  
     return new Promise((resolve, reject) => {
   this.gatewayService.CallAdapter(ConstantData.adapterUrls.GroupSharing,"POST",params).then((res)=> {
     console.log("*********************************** component  GroupSharing"+JSON.stringify(res));
      resolve(res);
   })
   .catch(function(e){
     console.log("******************************* component  failure GroupSharing"+e);
    
     //alert("error user info");
     reject(e);
  });
  });
  }catch(e){
   //alert("error"+JSON.stringify(e));
  }
  
  
  
  }


  GBShareStatus(params) {
    
    try{
    
       return new Promise((resolve, reject) => {
     this.gatewayService.CallAdapter(ConstantData.adapterUrls.GBShareStatus,"POST",params).then((res)=> {
       console.log("*********************************** component  GBShareStatus"+JSON.stringify(res));
        resolve(res);
     })
     .catch(function(e){
       console.log("******************************* component  failure GBShareStatus"+e);
       //alert("error user info");
       reject(e);
    });
    });
    }catch(e){
     //alert("error"+JSON.stringify(e));
    }
    
    
    
    }
    GoingToAllocation(params) {
      
      try{
      
         return new Promise((resolve, reject) => {
       this.gatewayService.CallAdapter(ConstantData.adapterUrls.GoingAllocation,"POST",params).then((res)=> {
         console.log("*********************************** component  createForAllocating"+JSON.stringify(res));
          resolve(res);
       })
       .catch(function(e){
         console.log("******************************* component  failure createForAllocating"+e);
         //alert("error user info");
         reject(e);
      });
      });
      }catch(e){
       //alert("error"+JSON.stringify(e));
      }
      
      
      
 }
      GoingToGroup(params) {
        
        try{
        
           return new Promise((resolve, reject) => {
         this.gatewayService.CallAdapter(ConstantData.adapterUrls.GoingGroupShare,"POST",params).then((res)=> {
           console.log("*********************************** component  GoingToGroup"+JSON.stringify(res));
            resolve(res);
         })
         .catch(function(e){
           console.log("******************************* component  failure GoingToGroup"+e);
           //alert("error user info");
           reject(e);
        });
        });
        }catch(e){
         //alert("error"+JSON.stringify(e));
        }
        
        
        
        }
       DeletedGBShare(params){
        try{
          
             return new Promise((resolve, reject) => {
           this.gatewayService.CallAdapter(ConstantData.adapterUrls.DeletedAllocation,"POST",params).then((res)=> {
             console.log("*********************************** component  DeletedAllocation"+JSON.stringify(res));
              resolve(res);
           })
           .catch(function(e){
             console.log("******************************* component  failure DeletedAllocation"+e);
             //alert("error user info");
             reject(e);
          });
          });
          }catch(e){
           //alert("error"+JSON.stringify(e));
          }
       }

       GetSubLines(params){
        try{
          
             return new Promise((resolve, reject) => {
           this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetLineNumber,"POST",params).then((res)=> {
             console.log("*********************************** component  GetSubLines"+JSON.stringify(res));
              resolve(res);
           })
           .catch(function(e){
             console.log("******************************* component  failure GetSubLines"+e);
             //alert("error user info");
             reject(e);
          });
          });
          }catch(e){
           //alert("error"+JSON.stringify(e));
          }
       }
       AllocatedGBShare(params){
        try{
          
             return new Promise((resolve, reject) => {
           this.gatewayService.CallAdapter(ConstantData.adapterUrls.AddGBAllocation,"POST",params).then((res)=> {
             console.log("*********************************** component  AllocatedGBShare"+JSON.stringify(res));
              resolve(res);
           })
           .catch(function(e){
             console.log("******************************* component  failure AllocatedGBShare"+e);
             //alert("error user info");
             reject(e);
          });
          });
          }catch(e){
           //alert("error"+JSON.stringify(e));
          }
       }



}
