// /*
//  *  © Copyright 2016 IBM Corp.
//  *
//  *  Licensed under the Apache License, Version 2.0 (the "License");
//  *  you may not use this file except in compliance with the License.
//  *  You may obtain a copy of the License at
//  *
//  *      http://www.apache.org/licenses/LICENSE-2.0
//  *
//  *  Unless required by applicable law or agreed to in writing, software
//  *  distributed under the License is distributed on an "AS IS" BASIS,
//  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  *  See the License for the specific language governing permissions and
//  *  limitations under the License.
//  */
// asb.factory('Auth', function ($rootScope,MFPInit,$q,$ionicHistory,$location,jsonStorage,$localstorage,$ionicPopup,userInfo,activityIndicator,PopUpService) {
//     //authInProgress = false;
//     var initFun = function(){
//         //var def = $q.defer();
//         MFPInit.then(function(){
//             //alert('challenge handler is registered');
//         var securityCheckName = 'Authentication';
// 		$rootScope.authInProgress = false;
// 		_$scope = null;
// 		challengeHandler = null;
// 		activeUser = null;
    
// 	challengeHandler = WL.Client.createSecurityCheckChallengeHandler(securityCheckName);
// 	challengeHandler.securityCheckName = securityCheckName;
//     //alert('challenge registered');
//     //def.resolve();
// 	challengeHandler.handleChallenge = function (challenge) {
//         //alert('in challenge handler challenge'+JSON.stringify(challenge));
// 		$rootScope.authInProgress = true;

// 		if (challenge.errorMsg !== null && _$scope) {
// //			_$scope.$emit('login-error', {
// //				message: challenge.errorMsg
// //			});
//             //alert(challenge.errorMsg);
//             PopUpService.showpopUpMsg(challenge.errorMsg);

//             activityIndicator.hide();
// 		} else {
//             //alert('error'+challenge.errorMsg)   
// 			// redirect to login page
// 			//$rootScope.$emit('login-challenge');
//             //$ionicHistory.clearHistory();
//             //$location.path('/sign-in');
//             jsonStorage.getData('login').then(
//                     function (response) {
//                         //alert("JSONStoreResponse: " +JSON.stringify(response));
//                         var userObj = {'username': response.username, 'password': response.password, rememberMe: response.rememberMeState}
//                         challengeHandler.submitChallengeAnswer(userObj);
//                     },
//                     function (response) {
//                         activityIndicator.hide();
//                         //alert('failed to get json data'+JSON.stringify(response));
//                     });
// 		}
// 	};

// 	challengeHandler.handleSuccess = function (data) {
//         console.log('in success callback'+JSON.stringify(data.user.attributes));
//         //alert('in success callback'+JSON.stringify(data.user.attributes));
//         var obj = userInfo.getUserLogin();
//         var isLogin = $localstorage.get('isLoggedIn');
//         var firstName = data.user.attributes.firstName;
//         $localstorage.set('username',firstName);
//         $localstorage.setObject("userObj",{'username': obj['username'], "rememberMe": obj['rememberMeState']});
//         try{
//         if(isLogin!="true"){
//             //alert('user is not logged in');
//         var addOptions = {
//                             /*
//                              * Mark data as dirty (true = yes, false = no), default true.
//                              */
//                             markDirty: false
//                         };
//                         jsonStorage.clear('login');
//                         var userObj = data.user.attributes;
//                         userObj['username'] = obj['username'];
//                         userObj['password'] = obj['password'];
//                         userObj['rememberMeState'] = obj['rememberMeState'];
        
//                         jsonStorage.put(userObj, 'login', addOptions)
//                                 .then(function (res_unused) {
//                                     //alert('json store successful');
//                                     //responseCallback(data.responseText);
//                                     //deferred.resolve(response);
//                         });
//         }
//         }catch(e){
//             //alert(JSON.stringify(e));
//         }
// 		$rootScope.authInProgress = false;

// 		activeUser = data.user;
//         //alert('activeUser'+activeUser);
//         //def.resolve(data);
// //		if(_$scope) {
// //			_$scope.$emit('login-success', {
// //				data: data
// //			});
// //		}
// 	};

// 	challengeHandler.handleFailure = function (error) {
// 		$rootScope.authInProgress = false; 
//         PopUpService.showpopUpMsg("Oops, there seems to be an error. Please try again later.");
//         //alert('in failure callback'+JSON.stringify(error));
// 	};
//         })
//         //return def.promise;
//     }
//     initFun();

// 	return {
        
// 		logout: function () {
//             var deferred = $q.defer();
// 			return WLAuthorizationManager.logout('Authentication');
// 		},
// 		getActiveUser: function () {
// 			return activeUser ? activeUser : false;
// 		},
// 		login: function ($scope, username, password, rememberMeState) {
//             //authInProgress = false;
//             //initFun().then(function(){
//                 //alert('login called'+username+password+$rootScope.authInProgress);
//                 var deferred = $q.defer();
//                 _$scope = $scope;
            
//                 if (!username || !password) {
//     //				$scope.$emit('login-error', {
//     //					message: 'Username and Password are required.'
//     //				});
//                     //alert('wrong username password');
//                 } 
//                 else if ($rootScope.authInProgress) {
//                     //alert('submit answer'+JSON.stringify($rootScope.authInProgress));
//                     challengeHandler.submitChallengeAnswer({'username': username, 'password': password, rememberMe: rememberMeState});
//                 } 
//                  else {
//                     //alert('login');
//                     WLAuthorizationManager.login('Authentication', {'username': username, 'password': password, rememberMe: rememberMeState})
//                         .then(
//                     function (response) {
//                         console.log('Login Success Response');
//                         //alert('login callback'+JSON.stringify(response));
//                         WL.Logger.debug("login onSuccess");
//                         //alert('Login Success'+response);
// //                        var addOptions = {
// //                            /*
// //                             * Mark data as dirty (true = yes, false = no), default true.
// //                             */
// //                            markDirty: false
// //                        };
// //                        jsonStorage.clear('login');
// //                        var userObj = {'username': username, 'password': password, rememberMe: rememberMeState}
// //                        jsonStorage.put(userObj, 'login', addOptions)
// //                                .then(function (res_unused) {
// //                                    //alert('json store successful');
// //                                    //responseCallback(data.responseText);
// //                                    deferred.resolve(response);
// //                        });
//                         deferred.resolve();
                        
//                     },
//                     function (response) {
//                         WL.Logger.debug("login onFailure: " + JSON.stringify(response));
//                         //alert('error'+JSON.stringify(response));
//                       if(response.errorMsg!="Forbidden"){
//                         PopUpService.showpopUpMsg(response.errorMsg);

//                       }

//                         //alert(response.errorMsg)
//                         deferred.reject(response);
//                     });
//                 }
//             return deferred.promise;
//             //})
                
// 		}
// 	};
    
// });
