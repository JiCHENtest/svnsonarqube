// import { Headers, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';
@Injectable()
export class HandleError {


    constructor( public loadingCtrl:LoadingController, public alertCtrl: AlertController){
        
    };

    handleErrorFetchUserInfo(res : any, funcName : any) : Promise<any> {
      console.log("handleError FetchUserInfo ",funcName );

      var check_value = (funcName == "FetchUserInfo" && res.Envelope.Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.CustomerID) ? true : false;
      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError FetchUserInfo");
          resolve(true);
        }
        else {
          console.log("handleError else handleError FetchUserInfo");
          try{
            // let alert_1 = this.alertCtrl.create({
            //   title: 'Notification',
            //   subTitle: ' ',
            //   message:"No data found.",
            //   buttons: [
            //   {
            //     text: 'Ok',
            //     handler: () => {
            //       console.log('clicked on ok button.');
                  resolve(false);
            //     }
            //   }],
            //   cssClass: 'error-message'
            // });
            // alert_1.present();
          }
          catch(e){
            console.log("catch exception handleError FetchUserInfo ",e);
          }
        }
      });        
    }

    handleErrorFetchPostpaidBillingInfo(res : any, funcName : any) : Promise<any> {
      
      console.log("handleError FetchPostpaidBillingInfo",funcName );
      console.log("handleError FetchPostpaidBillingInfo",JSON.stringify(res) );
      

      var check_value = (funcName == "FetchPostpaidBillingInfo" && res.Envelope.Body.FirstAppUsageResponse.ResponseBody.BalanceSummaryDetails.CmuBalanceSummaryVbc.LatestAmountDue !== undefined &&res.Envelope.Body.FirstAppUsageResponse.ResponseBody.BalanceSummaryDetails.CmuBalanceSummaryVbc.LatestAmountDue)?  true : false;
          
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError FetchPostpaidBillingInfo");
          resolve(true);
        }
        else {
          console.log("handleError else handleError FetchPostpaidBillingInfo");
          try{
             resolve(false);          
          }
          catch(e){
            console.log("catch exception handleError FetchPostpaidBillingInfo ",e);
          }
        }
      });        
    }

    handleErrorFetchAllDataPostpaidPrepaid(res : any, funcName : any) : Promise<any> {
      console.log("handleError handleError FetchAllDataPostpaidPrepaid ",funcName );

      var check_value = (funcName == "FetchAllDataPostpaidPrepaid" && res.MAX_StatusResponse.ResponseMessage == "Success")? true : false;
      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError FetchAllDataPostpaidPrepaid");
          resolve(true);
        }
        else {
          console.log("handleError else handleError FetchAllDataPostpaidPrepaid");
          try{
             resolve(false);          
          }
          catch(e){
            console.log("catch exception handleError FetchAllDataPostpaidPrepaid ",e);
          }
        }
      });        
    }

    handleErrorFetchDetailUsageTalkSMSInternet(res : any, funcName : any) : Promise<any> {
      console.log("handleError handleError FetchAllDataPostpaidPrepaid ",funcName );

      var check_value = ((funcName == "SMS" || funcName == "Talk" || funcName == "Roaming") && res.Envelope.Body.UsageTalkSMSRoamingResponse !== undefined && res.Envelope.Body.UsageTalkSMSRoamingResponse.ResponseBody.StatusCode == "SUCCESS")? true : false;
      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError handleErrorFetchDetailUsageTalk ",funcName," Internet");
          resolve(true);
        }
        else {
          console.log("handleError else handleError handleErrorFetchDetailUsageTalk ",funcName," Internet");
          try{
             resolve(false);          
          }
          catch(e){
            console.log("catch exception handleError handleErrorFetchDetailUsageTalk ",funcName," Internet",e);
          }
        }
      });        
    }


    handleErrorFetchOverAllPrepaidData(res : any, funcName : any) : Promise<any> {
      console.log("handleError handleError FetchAllDataPostpaidPrepaid ",funcName );

      var check_value = (funcName == "FetchOverAllPrepaidData" &&  res.Envelope.Body.ActQuotaUsageQueryFUPResponse.StatusCode !== undefined && res.Envelope.Body.ActQuotaUsageQueryFUPResponse.StatusCode == "OK")? true : false;
      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError FetchOverAllPrepaidData");
          resolve(true);
        }
        else {
          console.log("handleError else handleError FetchOverAllPrepaidData");
          try{
             resolve(false);          
          }
          catch(e){
            console.log("catch exception handleError FetchOverAllPrepaidData ",e);
          }
        }
      });        
    }

    handleErrorFetchBalancePrepaidData(res : any, funcName : any) : Promise<any> {
      console.log("handleError handleErrorFetchBalancePrepaidData ",funcName );

      var check_value = (funcName == "FetchBalancePrepaidData" && res.Envelope.Body.IntegrationEnquiryResultMsg.ResultHeader.ResultDesc.CDATA == "Operation successful.")? true : false;
      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError FetchBalancePrepaidData");
          resolve(true);
        }
        else {
          console.log("handleError else handleError FetchBalancePrepaidData");
          try{
             resolve(false);          
          }
          catch(e){
            console.log("catch exception handleError FetchBalancePrepaidData ",e);
          }
        }
      });        
    }


    handleErrorSubscriptionAddOnInfo(res : any, funcName : any) : Promise<any> {
      console.log("handleError handleErrorFetchBalancePrepaidData ",funcName );

      var check_value = (funcName == "SubscriptionAddOnInfo" && res.ProductInquiryResponse.ResponseMessage != undefined && res.ProductInquiryResponse.ResponseMessage == "Success")? true : false;
      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError handleErrorSubscriptionAddOnInfo");
          resolve(true);
        }
        else {
          resolve(false);
          console.log("handleError else handleError handleErrorSubscriptionAddOnInfo");
        }
      });
    }

    handleErrorSubscriptionRoamingDataForPostAndPrepaid(res : any, funcName : any) : Promise<any> {
      console.log("handleError handleErrorSubscriptionRoamingDataForPostAndPrepaid ",funcName );

      var check_value = (funcName == "SubscriptionRoamingDataForPostAndPrepaid" && res.ROAMPRODUCTINQUIRYRESPONSE.ResponseMessage != undefined && res.ROAMPRODUCTINQUIRYRESPONSE.ResponseMessage == "Success")? true : false;
      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError handleErrorSubscriptionRoamingDataForPostAndPrepaid");
          resolve(true);
        }
        else {
          resolve(false);
          console.log("handleError else handleError handleErrorSubscriptionRoamingDataForPostAndPrepaid");
        }
      });
    }

    handleErrorSubscriptionAddOnInfoSubscribe(res : any, funcName : any) : Promise<any> {
      console.log("handleError Maxup and MI handleErrorSubscriptionAddOnInfoSubscribe ",funcName );

      var check_value;
      // for maxup purchase
      if(funcName == "SubscriptionAddOnInfoSubscribe") {
        console.log(" funcName 1 ",funcName);
        check_value = (res.MAXUPPurchaseResponse.ResponseMessage != undefined && res.MAXUPPurchaseResponse.ResponseMessage == "Success") ? true : false;
      } // for mi purchase
      else if(funcName == "SubscriptionAddOnInfoSubscribeForMI") {
        console.log(" funcName 2 ",funcName);
        check_value = (res.MIPurchaseResponse.ResponseMessage != undefined && res.MIPurchaseResponse.ResponseMessage == "Success") ? true : false;
      } // for legacy purchase
      else {
        console.log(" funcName 3 ",funcName);
        check_value = (res.MIPurchaseResponse.ResponseMessage != undefined && res.MIPurchaseResponse.ResponseMessage == "Success") ? true : false;
      }

      // var check_value = (funcName == "SubscriptionAddOnInfoSubscribe" && ( (res.MAXUPPurchaseResponse.ResponseMessage != undefined && res.MAXUPPurchaseResponse.ResponseMessage == "Success") || (res.MIPurchaseResponse.ResponseMessage != undefined && res.MIPurchaseResponse.ResponseMessage == "Success") ) )? true : false;
      // "SubscriptionAddOnInfoSubscribeForMI"
      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError Maxup and MI if handleError handleError Subscription AddOnInfo Subscribe ");
          resolve(true);
        }
        else {
          console.log("handleError Maxup and MI else handleError handleErrorSubscriptionAddOnInfoSubscribe");
          resolve(false);          
        }
      });
    }

    handleErrorRoamingSubscriptionSubscribe(res : any, funcName : any) : Promise<any> {
      console.log("handleError handleErrorRoamingSubscriptionSubscribe ",funcName );

      var check_value = (funcName == "RoamingSubscriptionSubscribe" && res.ROAMPASSPURCHASERESPONSE.ResponseMessage != undefined && res.ROAMPASSPURCHASERESPONSE.ResponseMessage == "Success")? true : false;
      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError handleErrorRoamingSubscriptionSubscribe");
          resolve(true);
        }
        else {
          resolve(false);
          console.log("handleError else handleError handleErrorRoamingSubscriptionSubscribe");
        }
      });
    }

    // mydeal list
    handleErrorFetchMydeals(res : any, funcName : any) : Promise<any> {
      console.log("handleError handleErrorFetchMydeals ",funcName );

      var check_value = (funcName == "FetchMydeals" && res != undefined && res.statusReason != "Bad Request" && (res.statusCode != 400 || res.statusCode != "400") )? true : false;
      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError handleErrorFetchMydeals");
          resolve(true);
        }
        else {
          resolve(false);
          console.log("handleError else handleError handleErrorFetchMydeals");
        }
      });
    }

    // purchase mydeal
    handleErrorPurchaseMydeals(res : any, funcName : any) : Promise<any> {
      console.log("handleError handleErrorPurchaseMydeals ",funcName );

      var check_value = (funcName == "PurchaseMydeals" && res != undefined  && res.statusReason != "Bad Request" && (res.statusCode != 400 || res.statusCode != "400"))? true : false;
      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError handleErrorPurchaseMydeals");
          resolve(true);
        }
        else {
          resolve(false);
          console.log("handleError else handleError handleErrorPurchaseMydeals");
        }
      });
    }

    // service - oleole gift list
    handleErrorGiftListForCat(res : any, funcName : any) : Promise<any> {
      console.log("handleError handleError GiftCat ",funcName );

      // var check_value = (funcName == "GiftListForPostpaidAndPrepaid" && res != undefined && res.ProductInquiryResponse.ResponseMessage == "Success")? true : false;

      var check_value = (funcName == "GiftCat" && res != undefined && res.VOUCHERCATALOGUE != undefined)? true : false;

      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError handleError GiftCat");
          resolve(true);
        }
        else {
          resolve(false);
          console.log("handleError else handleError handleError GiftCat");
        }
      });
    }

    // oleole gift list
    handleErrorGiftListForPostpaidAndPrepaid (res : any, funcName : any) : Promise<any> {
      console.log("handleError handleError GiftList For PostpaidAndPrepaid ",funcName );

      // var check_value = (funcName == "GiftListForPostpaidAndPrepaid" && res != undefined && res.ProductInquiryResponse.ResponseMessage == "Success")? true : false;

      var check_value = (funcName == "GiftListForPostpaidAndPrepaid" && res != undefined && res.ProductInquiryResponse != undefined)? true : false;

      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError handleError GiftList For PostpaidAndPrepaid");
          resolve(true);
        }
        else {
          resolve(false);
          console.log("handleError else handleError handleError GiftList For PostpaidAndPrepaid");
        }
      });
    }

    handleErrorGiftPurchase (res : any, funcName : any) : Promise<any> {
      console.log("handleError handleError GiftList For PostpaidAndPrepaid ",funcName );

      // var check_value = (funcName == "GiftListForPostpaidAndPrepaid" && res != undefined && res.ProductInquiryResponse.ResponseMessage == "Success")? true : false;

      var check_value = (funcName == "GiftTreq2" && res != undefined && res.MAXUPPurchaseResponse != undefined && res.MAXUPPurchaseResponse.ResponseMessage == 'Success')? true : false;

      
      return new Promise((resolve, reject) => {
        if(check_value) {
          console.log("handleError if handleError handleError GiftList For PostpaidAndPrepaid");
          resolve(true);
        }
        else {
          resolve(false);
          console.log("handleError else handleError handleError GiftList For PostpaidAndPrepaid");
        }
      });
    }
}


// sample 
/* 
   handleErrorInfo(res, funcName) : Promise<any> {
    console.log("handleError ",funcName," ",res.Envelope.Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.CustomerID);
    var check_value = (funcName == "FetchUserInfo") ? res.Envelope.Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.CustomerID : ((funcName == "FetchPostpaidBillingInfo")? res.Envelope.Body.FirstAppUsageResponse.ResponseBody.CmuBalanceSummaryVbc.LatestAmountDue : false);

    // 

    return new Promise((resolve, reject) => {
      if(check_value){
        console.log("handleError if");
        resolve(true);
      }
      else {
        console.log("handleError else");
        try{
          let alert_1 = this.alertCtrl.create({
            title: 'Notification',
            subTitle: ' ',
            message:"No data found.",
            buttons: [
            {
              text: 'Ok',
              handler: () => {
                console.log('clicked on ok button.');
                resolve(false);
              }
            }],
            cssClass: 'error-message'
          });
          alert_1.present();
        }
        catch(e){
          console.log("catch exception handleError ",e);
        }
      }
    });
    
  }
*/