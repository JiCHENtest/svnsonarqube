import { Injectable } from '@angular/core';

@Injectable()
export class RendererService {
    /**
   * create <script> tag in renderer
   */
    private createScriptElementForRenderer(renderer) {
        const elem = renderer.createElement('script');
        return elem;
    }
    /**
     * To set property for an element using renderer
     * @param elem
     * @param propName 
     * @param propValue 
     */
    private setPropertyForRenderer(renderer, elem, propName, propValue) {
        renderer.setProperty(elem, propName, propValue);
    }
    /**
     * Append child for an element using renderer
     * @param parentElem
     * @param childElem 
     */
    private appendChildForRenderer(renderer, parentElem, childElem) {
        renderer.appendChild(parentElem, childElem);
    }
    /**
     * Set Attribute for an element using renderer
     * @param elem
     * @param attrName 
     * @param attrValue 
     */
    private setAttributeForRenderer(renderer, elem, attrName, attrValue) {
        renderer.setAttribute(elem, attrName, attrValue);
    }
    /**
     * Update the innerHtml text for an element using renderer
     * @param elem
     * @param innerHTMLText 
     */
    private updateTextForRenderer(document, renderer, elemId, innerHTMLText) {
        let elem = document.getElementById(elemId);
        const text = renderer.createText(innerHTMLText);
        renderer.appendChild(elem, text);
    }

    /**
    * Set Header script tag for adobe analytics
    * @param headerScript 
    */
    setHeaderScriptAdobe(renderer, head, headerScript) {
        const headerElem = this.createScriptElementForRenderer(renderer);
        this.setPropertyForRenderer(renderer, headerElem, 'src', headerScript);
        //this.renderer.setAttribute(headerElem, 'id', 'adobeHeaderScript');
        this.appendChildForRenderer(renderer, head, headerElem);
    }

    /**
     * Set footer script tag for adobe analytics
     * @param footerScript 
     */
    setFooterScriptAdobe(renderer, document, body, footerScript) {
        let footerElem = this.createScriptElementForRenderer(renderer);
        this.setPropertyForRenderer(renderer, footerElem, 'type', 'text/javascript');
        this.setAttributeForRenderer(renderer, footerElem, 'id', 'adobeFooterScript');
        this.appendChildForRenderer(renderer, body, footerElem);
        this.updateTextForRenderer(document, renderer, 'adobeFooterScript', footerScript);
    }
    /**
    * Create script for Adobe datalayer
    */
    createScriptForAdobeDataLayer(renderer, document, body, digitalDataResp) {
        let newObj = {
            "contextData" : digitalDataResp
        };
        const datLayerElem = this.createScriptElementForRenderer(renderer);
        this.setPropertyForRenderer(renderer, datLayerElem, 'type', 'text/javascript')
        this.setAttributeForRenderer(renderer, datLayerElem, 'id', 'digitalDataAdobe');
        this.appendChildForRenderer(renderer, body, datLayerElem);
        this.updateTextForRenderer(document, renderer, 'digitalDataAdobe', 'var contextData ='+JSON.stringify(digitalDataResp));
    }
    /**
     * Remove the text in adobe datalayer script
     * Append the new json text
     */
    updateDataLayerScript(document, renderer, digitalDataResp) {
        let updatedObj = {
            "contextData" : digitalDataResp
        };
        if (document.getElementById('digitalDataAdobe') !== null) {
            document.getElementById('digitalDataAdobe').innerHTML = "";
            this.updateTextForRenderer(document, renderer, 'digitalDataAdobe', 'var contextData ='+JSON.stringify(digitalDataResp));
        }
    }
    /**
    * Create script for Adobe datalayer
    */
   createTrackScriptForAdobeDataLayer(renderer, document, body) {
        const traceElement = this.createScriptElementForRenderer(renderer);
        this.setPropertyForRenderer(renderer, traceElement, 'type', 'text/javascript')
        this.setAttributeForRenderer(renderer, traceElement, 'id', 'traceAnalytics');
        this.appendChildForRenderer(renderer, body, traceElement);
        this.updateTextForRenderer(document, renderer, 'traceAnalytics', "_satellite.track('page load');");
    }
    /**
     * Remove the text in adobe datalayer script
     * Append the new json text
     */
    updateTraceScript(document, renderer, body) {
        if (document.getElementById('traceAnalytics') !== null) {
            let elem = document.getElementById('traceAnalytics');
            elem.parentNode.removeChild(elem);
            this.createTrackScriptForAdobeDataLayer(renderer,document,body);
        }  
    }
}