declare var WL;
//declare var WLAuthorizationManager;
export class JsonstoreService {  
    COLLECTION_NAME = 'celcom';
    inited = false;
    collections = {
        loginstatus: {
            searchFields: {'isuserloggedin': 'string'}
        },
        firsttimeloggedin: {
            searchFields: {'isfirsttimeuser': 'string'}
        },
        mobilenumber: {
            searchFields: {'mobilenum': 'integer'}
        }
    };

    constructor(){
        //this.initJSONStore();   
    };

    initJSONStore() : Promise<any> {

        //var initDeferred = $q.defer();
        return new Promise((resolve, reject) => {
            if (this.inited) {
                //alert('store is already inited');
                //initDeferred.resolve();
            } else {
                //Initialize the collection
                //alert('initializing store');
                return WL.JSONStore.init(this.collections).then(data => {
                    console.log("-> JSONStore init successful ",JSON.stringify(data));
                    //alert('init is successful');
                    this.inited = true;
                    resolve(data);
                }, err => {
                    //alert('init is failed'+errorObject.msg);
                    console.log("-> JSONStore error: " + err.msg);
                    reject();                    
                });
                //return initDeferred.promise;
            }
        });
    }

    // put data
    putData(data, name) {
        console.log("putData in ",data, "  ",name,"  ",data.length," ",data[0], " ",data," ",JSON.stringify(data));
        //this.clear(name);
        var query = {};
        var collectionName = 'mobilenumber';
        var options = {exact: true};
        WL.JSONStore.get(collectionName).remove(query, options).then(function (numberOfDocsRemoved) {
            console.log("Inside remove fn");
			
			try {
                // for (var i = 0; i < data.length; i++) {
                //     var obj = data[i];
                //     var strObj = JSON.stringify(data[i]);
               
    
                    WL.JSONStore.get(name).add(data,{}).then(res =>{ 
                        console.log("putData success ",JSON.stringify(res)); 
                    },
                    err => { console.log("putData error in ",JSON.stringify(err)); }
                    );
                // }
            }
            catch(e){
                console.log("putData erro ",e);
            }
			
            }).fail(function (error) {
                console.log("Inside Failure:"+error);
            });  
        // WL.JSONStore.get(name).remove().then(function (removeCollectionReturnCode) {
        //     // handle success
        //     console.log("Inside success removeCollectionReturnCode"+removeCollectionReturnCode);
        //     try {
        //         // for (var i = 0; i < data.length; i++) {
        //         //     var obj = data[i];
        //         //     var strObj = JSON.stringify(data[i]);
               
    
        //             WL.JSONStore.get(name).add(data,{}).then(res =>{ 
        //                 console.log("putData success ",JSON.stringify(res)); 
        //             },
        //             err => { console.log("putData error in ",JSON.stringify(err)); }
        //             );
        //         // }
        //     }
        //     catch(e){
        //         console.log("putData erro ",e);
        //     }
        // }).fail(function (error) {
        //     // handle failure
        //     console.log("Inside failure removeCollectionReturnCode"+error);
        // });
        
    }

    // get Data 
    getData(query, name) : Promise<any>{
        console.log("gettData in ",query, "  ",name," ",JSON.stringify(query));

        return new Promise((resolve, reject) => {
            try {
                return WL.JSONStore.get(name).findAll().then(res1 => {
                    console.log("gettData find success ",JSON.stringify(res1));
                    resolve(res1);
                },
                err => {
                    console.log("gettData find err ",err);
                    reject(); 
                });
            }
            catch(e){
                console.log("gettData erro ",e);
            }
        });
    }


    // get Data 
    replaceData(query, name) : Promise<any>{
        //WL.JSONStore.get(name).clear();
        var options = {};
        return new Promise((resolve, reject) => {
            try {
                return WL.JSONStore.get(name).replace(query, options).then((res)=> {
                    // handle success
                    console.log("replaceData find success ",JSON.stringify(res));
                    resolve(res);
                },(err) =>{
                    // handle failure
                    console.log("replaceData find err ",err);
                    reject(); 
                });
            }
            catch(e){
                console.log("replaceData erro ",e);
            }
        });
    }

    // clear collection
    clear(collecName) : any {
        // WL.JSONStore.get(collecName).clear();
        // clear(collecName) : any {
            WL.JSONStore.get(name).removeCollection().then(function (removeCollectionReturnCode) {
                // handle success
                console.log("Inside success removeCollectionReturnCode"+removeCollectionReturnCode);
            }).fail(function (error) {
                // handle failure
                console.log("Inside failure removeCollectionReturnCode"+error);
            });
        //} 
    }


}
// angular.module('ASB.jsonstore', [])

//         .factory('jsonStorage', function ($q, $rootScope) {

//             'use strict';
//             var COLLECTION_NAME = 'todos';

//             var collections = {
//                 login: {
//                     searchFields: {username: 'string'}
//                 },
//                 todos: {
//                     searchFields: {'data.Id': 'number'}
//                 }
//             };

//             var inited = false;
//             function initJSONStore() {
//                 var initDeferred = $q.defer();
//                 if (inited) {
//                     //alert('store is already inited');
//                     initDeferred.resolve();
//                 } else {
//                     //Initialize the collection
//                     //alert('initializing store');
//                     WL.JSONStore.init(collections).then(function () {
//                         console.log("-> JSONStore init successful");
//                         //alert('init is successful');
//                         initDeferred.resolve();
//                     }).fail(function (errorObject) {
//                         //alert('init is failed'+errorObject.msg);
//                         console.log("-> JSONStore error: " + errorObject.msg);
//                     });
//                     return initDeferred.promise;
//                 };
//             }

//             function putData(data, name) {
//                 for (var i = 0; i < data.length; i++) {
//                     var obj = data[i];
//                     var strObj = JSON.stringify(data[i]);
//                     WL.JSONStore.get(name).add(strObj);
//                 }
//             }

//             return {
//                 getData: function (name) {
//                     var deferred = $q.defer();
//                     WL.JSONStore.get(name).findAll().then(function (res) {
//                         if (res.length > 0) {
//                             if (name == 'login')
//                                 deferred.resolve(res[0].json.data || '[]');
//                             else
//                                 deferred.resolve(res);
//                         } else {
//                             deferred.resolve(res);
//                         }

//                     }).fail(function (errorObject) {
//                         console.log("JSONStore findAll error: errorObject: " + JSON.stringify(errorObject));
//                         console.log("JSONStore findAll error: errorObject: " + JSON.stringify(errorObject));
//                     });
//                     return deferred.promise;
//                 },
//                 clear: function (collecName) {
//                     WL.JSONStore.get(collecName).clear();
//                 },
//                 put: function (data, name, addOptions) {
//                     console.log("StoreAppJsonStorage.put START:");
//                     console.log("data: " + JSON.stringify(data));
//                     console.log("name: " + name);
//                     console.log("JSON.stringify(addOptions): " + JSON.stringify(addOptions));
//                     var initDeferred = $q.defer();

//                     //for (var i = 0; i < data.length; i++) {

//                         var obj = data;
//                         var strObj = JSON.stringify(data);
//                         WL.JSONStore.get(name).add({data: obj}, addOptions).then(function (data) {
//                             console.log("-> JSONStore put successful" + data);
//                             console.log("-> JSONStore put successful" + data);
//                             initDeferred.resolve();
//                         }).fail(function (errorObject) {
//                             console.log('init is failed' + errorObject.msg);
//                             console.log("-> JSONStore error: " + errorObject.msg);
//                         });
//                     //}
//                     return initDeferred.promise;
//                 },
//                 putPed: function (data, name, callback) {
//                     WL.JSONStore.get(name).clear();
//                     for (var i = 0; i < data.length; i++) {
//                         var obj = data[i];
//                         WL.JSONStore.get(name).add(obj);
//                     }                  
//                 },
//                 replaceById: function (id, updatedData, collecName, onReplaceSuccess, onReplaceFailure) {

//                     var doc = {
//                         "_id": id,
//                         "json": updatedData
//                     };
//                     WL.JSONStore.get(collecName).replace(doc)
//                             .then(onReplaceSuccess)
//                             .fail(onReplaceFailure);
//                 },
//                 findById: function (currIbmId, collecName, onFindByIdSuccess, onFindByIdFailure) {

//                     WL.JSONStore.get(collecName).findById(currIbmId)
//                             .then(onFindByIdSuccess)
//                             .fail(onFindByIdFailure);
//                 },
//                 findByKey: function (searchKey, searchValue, collecName, onFindSuccess, onFindFailure) {

//                     var query = {};
//                     query[searchKey] = searchValue;
//                     var options = {};
//                     WL.JSONStore.get(collecName).find(query, options)
//                             .then(onFindSuccess)
//                             .fail(onFindFailure);
//                 },
//                 getDirtyDocuments: function (collecName, onGetDirtySuccess, onGetDirtyFailure) {

//                     WL.JSONStore.get(collecName).getPushRequired()
//                             .then(onGetDirtySuccess)
//                             .fail(onGetDirtyFailure);
//                 },
//                 checkIsDirty: function (collecName, doc, onCheckIsDirtySuccess, onCheckIsDirtyFailure) {

//                     WL.JSONStore.get(collecName).isDirty(doc)
//                             .then(onCheckIsDirtySuccess)
//                             .fail(onCheckIsDirtyFailure);
//                 },
//                 initJSON: function () {
//                     var initDeferred = $q.defer();
//                     if (inited) {
//                         //('store is already inited');
//                         return initDeferred.promise;
//                     } else {
//                         //Initialize the collection
//                         //alert('initializing store');
//                         WL.JSONStore.init(collections).then(function () {
//                             inited = true;
//                             console.log("-> JSONStore init successful");
//                             //alert('init is successful');
//                             initDeferred.resolve();
//                         }).fail(function (errorObject) {
//                             //alert('init is failed' + errorObject.msg);
//                             console.log("-> JSONStore error: " + errorObject.msg);
//                         });
//                         return initDeferred.promise;
//                     }                    ;
//                 },
//                 destroyStore: function () {
//                     var initDeferred = $q.defer();
//                     WL.JSONStore.destroy()

//                             .then(function () {
//                                 inited = true;
//                                 initDeferred.resolve();
//                             })

//                             .fail(function (errorObject) {
//                                 console.log('destroy is failed' + errorObject.msg);
//                                 //initDeferred.resolve();
//                             });
//                     return WL.JSONStore.init(collections);
//                 },               
//                 isInited: function () {
//                     return inited;
//                 }
//             };
//         });

