import { Injectable } from '@angular/core';
import { Platform, AlertController} from 'ionic-angular';



@Injectable()
export class AlertService {

  onDevice: boolean;
  toast:any;
  constructor(private platform: Platform, private alertCtrl: AlertController) {
    this.onDevice = this.platform.is('cordova');

  }

  showAlert(title, msg, btn,cssClassVal?,dismissOnPageChange?,duration?) {
    console.log("Alert open");
    if(Array.isArray(btn)){

      try {
       /* let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          buttons: [{
            text: btn[1],
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: btn[0],
            handler: () => {
              console.log('Call to Action Clicked');
            }
          }],
          cssClass:'error-message'
        // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
        });*/

        let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          //message:"do eiusmod tempor incididunt ut lab ore et dolore magna aliqua",
          buttons: [
             {
            text: btn[1],
            cssClass:'submit-button',
            //role: 'cancel',
            handler: () => {
                console.log('Cancel clicked');
          }

    },
    {
      text: btn[0],
      cssClass:'submit-button',
      handler: () => {
        console.log('Call to Action Clicked');
      }

    }],
    cssClass: 'success-message error-message'
  });
        alert.present();
        if(duration!=null && alert._state==1){
          setTimeout(()=>{
            alert.dismiss().then(succ=>{
            });
          },2000);
        }
      }
      catch(e) {
        console.log("Exception : Alert open ",e);
        
      }
    }
    else{
      try {

    let alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
     //message:"do eiusmod tempor incididunt ut lab ore et dolore magna aliqua",
     buttons: [

      {
        text: btn,
        cssClass:'error-button-float-right submit-button',
        handler: () => {
          console.log('Call to Action Clicked');
        }

      }],
      cssClass: 'success-message error-message'
    });
  
        alert.present();
        if(duration!=null && alert._state==1){
          setTimeout(()=>{
            alert.dismiss().then(succ=>{
            });
          },2000);
        }
      }
      catch(e) {
        console.log("Exception : Alert open ",e);
        
      }
    }
    
    
  }
 

}
