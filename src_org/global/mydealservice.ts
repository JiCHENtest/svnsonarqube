import { Injectable } from '@angular/core';

declare var cordova;
/*
  Generated class for the SpeedcheckerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyDealProvider {

    fetchMyDealList : any;

    constructor() {
        console.log('Hello MyDealProvider');    
    }

    SetFetchMydeals(obj) {        
        console.log('SetFetchMydeals ');    
        // this.fetchMyDealList = this.staticFetchMydeal();
        this.fetchMyDealList = obj;
        
        console.log("this.fetchMyDealList ",this.fetchMyDealList);
    }

    GetFetchMydeals() {  
        console.log('GetFetchMydeals ',this.fetchMyDealList); 
        // console.log('GetFetchMydeals offerList ',JSON.stringify(this.fetchMyDealList.getPromotionOffersResponse.offerList));            
        // return this.fetchMyDealList.getPromotionOffersResponse.offerList;
        return this.fetchMyDealList.getPromotionOffersResponse;
    }
    
    staticFetchMydeal() {
        return {
                "getPromotionOffersResponse": {
                "msisdn": "0191234567",
                "offerList": [
                    {
                        "productID ": "12345",
                        "offerScore ": "50",
                        "offerName": "GrpA_RM3_5Mins Call",
                        "offerDesc": "RM3 get 5 mins OnNet Voice Call",
                        "treatmentCode": "30 ce9 .161 b.ffffffffcb8418c5 .61021765",
                        "offerCode": "000005658",
                        "campaignObjectiveCategory": "Increase domestic usage / revenue",
                        "campaignMechanics": "Use RM3 get 5 mins OnNet Voice Call",
                        "selectionCriteria": "XPAX TURBO,UOX,SOX,Xpax,Xmax,XMid,Xlite,Blue",
                        "adjustmentCode": "FUB205VC",
                        "notificationName": "Notification_Msg_English",
                        "notificationValue": "RM0 Just for you!Get FREE 5 mins call 2 the same network within 48 hours with Xpax when you use RM3 TODAY!Promo valid till 11.59 pm.SincerelyCelcom.",
                        "fulfillmentName": "Fulfilment_Msg_English",
                        "fulfillmentValue": "RM0 Congrats!5 mins Voice Calls to the same ntwk has been credited into ur account.Valid for 3 days.Stay with Celcom for more rewards!T & C ",
                        "costPerOffer ": "15",
                        "expirationDate": "2017-09-30",
                        "offerCaption": "Get the Deal !!",
                        "offerCaption1": "MyDeals",
                        "planDescription1": "Data Add-On 5G RM10",
                        "planSubDescription1": "You will also get FREE 3G extra data with the same validity period"
                    }, 
                    {
                        "productID ": "5678",
                        "offerScore ": "50",
                        "offerName": "GrpA_RM8_10Mins Call",
                        "offerDesc": "RM8 get 10 mins OnNet Voice Call",
                        "treatmentCode": "30 cf3 .15 ca .378e b14b.ffffffff9ff7181b",
                        "offerCode": "000005577",
                        "campaignObjectiveCategory": "Increase domestic usage / revenue",
                        "campaignMechanics": "Use RM8 get 10 mins OnNet Voice Call",
                        "selectionCriteria": "IOX,X2",
                        "adjustmentCode": "FUB610VC",
                        "notificationName": "Notification_Msg_English",
                        "notificationValue": "RM0 Just for you!Get FREE 10 mins call 2 the same network within 48 hours with Xpax when you use RM8 TODAY!Promo valid till 11.59 pm.SincerelyCelcom.",
                        "fulfillmentName": "Fulfilment_Msg_English",
                        "fulfillmentValue": "RM0 Congrats!10 mins Voice Calls to the same ntwk has been credited into ur account.Valid for 3 days.Stay with Celcom for more rewards!T & C ",
                        "costPerOffer ": "16",
                        "expirationDate": "2017-09-30",
                        "offerCaption": "Get the Deal of the day !!",
                        "offerCaption1": "MyDeals",
                        "planDescription1": "Data Add-On 5G RM20",
                        "planSubDescription1": "You will also get FREE 3G extra data with the same validity period"
                    }
                ]
            }
        };
    }

    purchasePlan(params) {
        /*
        var _that = this;
        var params = {
            "planPurchaseRequest": {
            "msisdn": params.msisdn,
            "treatmentCode": params.treatmentCode,
            "productID": params.productID,
            "eventValue": "Event"
            }
        };
        return new Promise((resolve, reject) => {
            this.CallAdapter(ConstantData.adapterUrls.PurchaseMydeal,"POST",params).then(function(res){
                console.log("************************************* component success FetchMydeals"+JSON.stringify(res));
                _that.handleerror.handleErrorFetchMydeals(res,"FetchMydeals").then((result) => {
                if(result == true) {
                    console.log("hai if FetchMydeals ",result);
                    resolve(res);
                    
                }
                else {
                    console.log("hai else FetchMydeals ",result);
                    reject("error : no data found.");
                }
                });
                // this.handleError(res,"FetchMydeals");
                // resolve(res);
            })
            .catch(function(e){
                console.log("****************************************** component failure FetchMydeals"+e);
                //alert("error user info FetchMydeals ");
                reject(e);
            });
        });
      */
    }
}   