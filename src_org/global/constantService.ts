/**
 * @Service Name - constantService
 * @Description - This service is to define all constant variables used in whole application
 */
export class ConstantData {
    public static isOnline = true;
    public static isPrepaid = true;
    public static isLoginBypassed=true;
    public static isDashboardBypassed=false;
    //public static BASE_URL = 'http://3.209.176.253:1001';
    public static adapterUrls = {
        LoginUrl:"/adapters/Login/GetLogin",
        NetworkLoginUrl:"/adapters/Login/NetworkLogin",
        MobileConnect:"/adapters/Login/MobileConnect",
        LoginWithTac:"/adapters/Login/LoginWithTac",
        GetUserInfo:"/adapters/Login/GetUserInfo",
        //DashboardUrl:"/adapters/Dashboard/GetDashboardInfo",
        DashboardDetailPostpaidUrl:"/adapters/DashboardDetails/DetailedUsagePostpaid",//QueryUsageTalkSMSRoamingDetailsreqABCSImpl
        DashboardDetailPrepaidUrl:"/adapters/DashboardDetails/DetailedUsagePrepaid",
        GetPostpaidBillingInfo:"/adapters/Dashboard/GetPostpaidBillingInfo",//QueryFirstAppUsageCommsReqABCSImpl
        GetAllDataPostpaidPrepaid:"/adapters/RestAdapter/GetAllDataPostpaidPrepaid",
        GetOverAllPrepaidData:"/adapters/DashboardPrepaid/GetOverAllPrepaidData",//QuerySubscriberUsageXPAXCommsReqABCSImpl
        GetBalancePrepaidData:"/adapters/DashboardPrepaid/GetBalancePrepaidData",//XPAXIntegrationEnquiry
        GetPlanDetails:"/adapters/PlanDetailsAdapter/getPlans",

        GetCardMapping:"/adapters/CardMappingAdapter/getCardMapping",
        GetRoamStatus:"/adapters/RestAdapter/GetRoamStatus",
        GetRoamStatusPre:"/adapters/RestAdapter/GetRoamStatusPre",
        GetStopAutorenew:"/adapters/RestAdapter/GetStopRenew",
        GetFreeUnitDetailsProcess:"/adapters/GBShareAdapter/GroupSharing",
        QueryQuotaUsageDetailsProcess:"/adapters/Login/QueryQuotaUsageDetailsProcess",
        GetTalkPostpaid:"/adapters/Dashboard/GetTalkPostpaid",
        //MobileConnect adapters
        registerNew:"/adapters/MobileConnectAdapter/registerNew",
        register:"/adapters/MobileConnectAdapter/register",
        validate:"/adapters/MobileConnectAdapter/validate",

	    GetMobileNumber:"/adapters/reloadAdapter/CustomerRetrieve",
        GetValidityBalance:"/adapters/ReloadAdapterXPAXIntergration/getDateBlance",
        GetInternetPlans:"/adapters/GetInternetPlanDetails/internetPlans",
        GetCreditReloadPlans:"/adapters/GetInternetPlanDetails/creditReload",
        DigitPin:"/adapters/DigitPin/checkValidationDigitPin",

        GetSubscriptionData:"/adapters/Subscription/GetSubscriptionData",
        SubscribeToAddOn:"/adapters/Subscription/SubscribeToAddOn",
        SubscriptionRoamingListForPostpaid :"/adapters/Subscription/SubscriptionRoamingListForPostpaid",
        SubscribeRoamingPlanForPostpaid:"/adapters/Subscription/SubscribeRoamingPlanForPostpaid",
        SubscriptionRoamingListForPrepaid:"/adapters/Subscription/SubscriptionRoamingListForPrepaid",
        SubscribeRoamingPlanForPrepaid:"/adapters/Subscription/SubscribeRoamingPlanForPrepaid",
        GbShareAdmin:"/adapters/Subscription/GbShareAdmin",
        GetCountryListName:"/adapters/Subscription/GetCountryListName",
        
        // mydeals
        FetchMydeals : "/adapters/Mydeals/FetchMydeals",
        PurchaseMydeal : "/adapters/Mydeals/PurchaseMydeal",

        // oleole
        GiftListForPostpaidAndPrepaid : "/adapters/OleGift/GiftListForPostpaidAndPrepaid",
        GiftTreq2 : "/adapters/OleGift/GiftTreq2",
        GiftCat : "/adapters/OleGift/GiftCat",
        registerMobileConnect2 : "/adapters/MobileConnect2/registerMobileConnect2",
        registerNewMobileConnect2 : "/adapters/MobileConnect2/registerNewMobileConnect2",
        validateMobileConnect2 : "/adapters/MobileConnect2/validateMobileConnect2",

        PostpaidPaymentURL:"/adapters/PaymentAdapter/getPostpaidPaymentURL",
        PrepaidPaymentURL:"/adapters/PaymentAdapter/getPrepaidPaymentURL",
	    youthQRYKawKaw:"/adapters/KawKawSquardAdapter/youthQRY",
        youthSubsKawKaw:"/adapters/KawKawSquardAdapter/youthSubs",
        fetchSavedCards :"/adapters/PaymentAdapter/fetchSavedCards",
        fetchSavedCardsAutoBilling :"/adapters/PaymentAdapter/fetchSavedCardsAutoBilling",
        deleteSavedCard:"/adapters/PaymentAdapter/deleteSavedCard",

        GetBillDueDetails:"/adapters/PayBill_Due/GetBillDueDetails",
        PostReloadPin:"/adapters/Postpaid_reload/postReload_verification",
        autobillingTokenizerURL:"/adapters/PaymentAdapter/autobillingTokenizerURL",
        updateProfile:"/adapters/UpdateProfile_Autobilling/updateProfile",
        fetchTransDetails:"/adapters/PaymentAdapter/fetchTransDetails",

        GroupSharing:"/adapters/GBShareAdapter/GroupSharing",
        GBShareStatus:"/adapters/GBShareStatus/GroupStatus",
        GoingAllocation:"/adapters/GBShareStatus/EnableAllocation",
        GoingGroupShare:"/adapters/GBShareStatus/DisableAllocation",

        GetLineNumber:"/adapters/GBAllocationShareAdapter/AllocationSharing",
        DeletedAllocation:"/adapters/GBAllocationShareAdapter/DeletedAllocated",
        AddGBAllocation:"/adapters/GBAllocationShareAdapter/createForAllocating",

        ECEMEligible:"/adapters/ECEMAdapter/ECEMEligible",
        ECEMPlanDetails:"/adapters/ECEMAdapter/ECEMPlanDetails",
        ECEMERedeemraya:"/adapters/ECEMAdapter/ECEMERedeemraya",
        PUKEnquiry:"/adapters/puk_details/enquirePUK",
        SubmitCreditTransfer:'/adapters/TransferCredit/creditTransferSubmit',
        UpgradePlanDetail:'/adapters/UpgradePlanDetail/upgradePlanDetail',
        SubmitValidityExtension:'/adapters/ValidityExtensionAdapter/submitValidityExtension',

        PromiseToPay:"/adapters/CallBarAdapter/promiseToPay",
        updateCreditLimit:"/adapters/CallBarAdapter/updateCreditLimit",
        updatePTPProfile:"/adapters/DashboardPrepaid/updateProfile",
        CreditTransfer:"/adapters/CreditManagement/transferAmount",

        GetCreditAdvanceEligibility:"/adapters/CreditAdvance/GetCreditAdvanceEligibility",
        creditAdvanceDenominations:"/adapters/CreditAdvance/creditAdvanceDenominations",
        creditAdvanceTransaction:"/adapters/CreditAdvance/creditAdvanceTransaction",

        GetPrepaidBalance:"/adapters/PrepaidAccountStatus/getPrepaidAccountStatus",
        GetStoreLocatorData:"/adapters/StoreLocatorAdapter/getStoreData",
        getTransactionHistory :"/adapters/Transactions/getTransactionHistory",
        getTransactionReloadPaybill: "/adapters/Transactions/getOnlineReloads",
        fetchBillListRetrieve:"/adapters/Transactions/fetchBillListRetrieve",

        BillDownload:"/adapters/BillDownload/getPDFDownload",


        QueryVoucher:"/adapters/LifeStyleAdapter/QueryVoucher",
        EnableOptIn:"/adapters/LifeStyleAdapter/EnableOptIn",
        RedeemPrivilege:"/adapters/LifeStyleAdapter/RedeemPrivilege",
        simReplacement_checkOpenOrder:"/adapters/ValidateSimReplacement/openOrderValidation",
        simReplacement_isSimBlocked:"/adapters/ValidateSimBlocked/simBlockCheck",
        simReplacement_validateAddress:"/adapters/ValidateSimReplacement/validateUserAddress",
        simReplacement_getSimRelacementCharges:"/adapters/ValidateSimReplacement/getSimReplChargeByService",
        simReplacement_SubmitSimRelacement:"/adapters/SimReplacement/submitSIMReplaceOrderService",
        networkLogin:"http://mobile-oneapp.celcom.com.my/mfp/api/adapters/Login/networkLogin",
        GetLatestPlanDetails:"/adapters/PlanDetailsAdapter/getLatestPlan" 
        //network login
        //networkLogin:"/adapters/Login/networkLogin"
        //networkLogin:"http://mobile-oneapp.celcom.com.my/mfp/adapters/Login/networkLogin"
    }

    public static cardMapping = {
        "Prepaid":{
            "No Internet Plans":"Card CTA #1",
            "Limited Internet Plans (Daily)":"Card Usage with Limit #1",
            "Limited Internet Plans (Weekly/ Monthly)":"Card Toggle #1",
            "Limited Data Add-Ons (Including Asia Pass free data)":"Card Usage with Limit #1",
            "Asia Pass Free Data":"Card Bundle #1 CTA",
            "Unlimited Data Add-Ons":"Card Description #1",
            "Special Features - Limited Data":"Card Usage with Limit #1",
            "Activation Bonus":"Card Usage with Limit #1",
            "Pay per use Voice Calls":"Card PPU #1",
            "Pay per use Video Calls":"Card PPU #1",
            "Unlimited Voice Calls Add-Ons":"Card Description #1",
            "IDD Passes":"Card Bundle #1 CTA",
            "Kad Ceria Talk/SMS Bonus":"Card Usage with Limit #2 (Kad Ceria)",
            "Special Features - Limited Voice Calls":"Card Usage with Limit #1",
            "Pay per use SMS":"Card PPU #1",
            "Pay per use MMS":"Card PPU #1",
            "Special Features - Limited SMS":"Card Usage with Limit #1",
            "No Roaming Passes - Subscribe to roaming pass":"Card CTA #1",
            "No Roaming Passes - Total Roaming Call Charges":"Card PPU #1",
            "No Roaming Passes - Total Roaming SMS Charges":"Card PPU #1",
            "Roaming - Display pass info, visiting country & operator pure info (For all Passes)":"Card Info (Roaming) #1",
            "Roaming Pass - All 1 Day Passes for Prepaid":"Card Bundle #1 CTA",
            "Pay per use Internet Data - RM10 Cap":"Card PPU #1",
            "Pay per use Internet Data - 150MB":"Card Usage with Limit #1",
            "Friends and Family Voice":"Card PPU #1",
            "Friends and Family Video":"Card PPU #1",
            "Friends and Family SMS":"Card PPU #1",
        },
        "Postpaid":{
            "No Internet Plans":"Card CTA #1",
            "Limited Internet Plans (Weekday/Anyday)":"Card Usage with Limit #1",
            "Limited Internet Plans (Weekend)":"Card Usage with Limit (Weekend Internet) #3",
            "Limited Data Add-Ons":"Card Usage with Limit #1",
            "Unlimited Data Add-Ons":"Card Description #1",
            "Unlimited Other Offerings":"Card Description #1",
            "Limited Other Offerings":"Card Usage with Limit #1",
            "Pay per use Internet (Legacy Plan: CMP)":"Card PPU #1",
            "Limited Postpaid Voice Calls":"Card Usage with Limit #1",
            "Unlimited Postpaid Voice Calls":"Card Description #1",
            "PPU Talk (Including IDD)":"Card PPU #1",
            "Pay per use Video Calls":"Card PPU #1",
            "Limited Video Calls (Legacy Plans)":"Card Usage with Limit #1",
            "Limited SMS":"Card Usage with Limit #1",
            "Unlimited SMS":"Card Description #1",
            "Pay per use SMS":"Card PPU #1",
            "Pay per use MMS":"Card PPU #1",
            "Unlimited MMS (Legacy Plans)":"Card Description #1",
            "Limited MMS (Legacy Plans)":"Card Usage with Limit #1",
            "No Roaming Passes - Subscribe to roaming pass":"Card CTA #1",
            "No Roaming Passes - Total Roaming Call Charges":"Card PPU #1",
            "No Roaming Passes - Total Roaming SMS Charges":"Card PPU #1",
            "Roaming - Display pass info, visiting country & operator pure info (For all Passes)":"Card Info (Roaming) #1",
            "Roaming Pass - 1 Day Call & SMS, 7 Day 3 in 1 (Not Activated)":"Card Info (Plan Activation) #1",
            "Roaming Pass - 1 Day Call & SMS (Activated)":"Card Description #1",
            "Roaming Pass - 7 Day 3 in 1 (Activated)":"Card Bundle (CTA) #1 && Card Description #1",
            "Roaming Pass - 1 Day Internet Pass":"Card Usage with Limit #1",
            "Group Sharing (Admin & Line)":"CARD USAGE + CTA (GBSHARE Group Sharing) #4",
            "Allocated Sharing (Admin)":"Card Bundle #1 CTA",
            "Allocated Sharing (Line)":"CARD USAGE + CTA (GBSHARE Line Allocated) #4"
        }
    }


    public static dialMapping = {
        "postpaid":{

            "FIRST Blue II":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Carry Forward": "Card Usage with Limit #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Gold 2.0":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Gold Plus": {
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Gold Supreme": {
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Video Walla": "Card Description #1",
                    "Yonder Music": "Card Description #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "First Platinum 2.0": {
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Video Walla": "Card Description #1",
                    "Yonder Music": "Card Description #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "First Platinum Plus":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Video Walla": "Card Description #1",
                    "Yonder Music": "Card Description #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "First 1 + 5 2.0":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Carry Forward": "Card Usage with Limit #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "FIRST Blue Internet":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "SMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Carry Forward": "Card Usage with Limit #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Gold Internet":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "SMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "Carry Forward": "Card Usage with Limit #1",
                    "Dial": "Unlimited"
                }
            },
            "Ambassador Plan":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "Ambassador Supplementary":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Carry Forward": "Card Usage with Limit #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Gold Plus with GBshare":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Platinum with GBshare":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "iFlix": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "GBshare Line":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Whatsapp WeChat": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "GBshare Line for FiRST Platinum":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Whatsapp WeChat": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "First Blue 1.0 Old":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Carry Forward": "Card Usage with Limit #1",
                    "Dial": "Unlimited"
                }
            },
            "First Blue 1.0 Upgraded":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Blue 2.0 Old":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Carry Forward": "Card Usage with Limit #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Blue 2.0 Upgraded":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Gold 1.0 Old":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "Carry Forward": "Card Usage with Limit #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Gold 1.0 Upgraded":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Gold 2.0 Old":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Whatsapp WeChat": "Card Description #1",
                    "Carry Forward": "Card Usage with Limit #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Gold 2.0 Upgraded":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "iFlix": "Card Description #1",
                    "Carry Forward": "Card Usage with Limit #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Platinum 1.0 Old":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Unlimited"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "iFlix": "Card Description #1",
                    "Carry Forward": "Card Usage with Limit #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Platinum 1.0 Upgraded":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "iFlix": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Platinum 2.0 Old":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",

                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"

                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "iFlix": "Card Description #1",
                    "Carry Forward": "Card Usage with Limit #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Platinum 2.0 Upgraded":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "iFlix": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Basic 38":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "FiRST Basic 85 Old":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"

                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "FiRST Basic 85 Upgraded":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "FiRST Basic 85 Internet":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": "N/A",
                "Dial": "Limited"
            },
            "FiRST Gold (100) Old":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Carry Forward": "Card Usage with Limit #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Gold (100) New":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Unlimited"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": {
                    "Yonder Music": "Card Description #1",
                    "Dial": "Unlimited"
                }
            },
            "FiRST Elite 235 (1st SIM) Old":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"

                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "FiRST Elite 235 (1st SIM) Upgraded":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "FiRST Elite 235 (2nd SIM) Old":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "Celcom FIRST™ Prime":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "Card Usage with Limit #1",
                "Dial": "Limited"
            },
            "Celcom FIRST™ Premier":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "Card Usage with Limit #1",
                "Dial": "Limited"
            },
            "Celcom FIRST™ Elite":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "Card Usage with Limit #1",
                "Dial": "Limited"
            },
            "Celcom FIRST™ 1+5":{
                "Internet": {
                    "weekday Internet": "",
                    "weekend Internet": "",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "",
                    "Off-net": "",
                    "Dial": "NA"
                },
                "SMS": {
                    "On-net": "",
                    "Off-net": "",
                    "Dial": "NA"
                },
                "MMS": {
                    "On-net": "",
                    "Off-net": "",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "",
                    "Off-net": "",
                    "Dial": "NA"
                },
                "Other Offerings": "Card Usage with Limit #1",
                "Dial": "NA"
            },
            "Celcom FIRST™  for Business Biz Prime":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "Card Usage with Limit #1",
                "Dial": "Unlimited"
            },
            "Celcom FIRST™  for Business Biz Premier":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "Other Offerings": "Card Usage with Limit #1",
                "Dial": "Unlimited"
            },
            "Celcom FIRST™  for Business Biz Elite":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "Other Offerings": "Card Usage with Limit #1",
                "Dial": "Unlimited"
            },
            "FiRST One Plan 5GB (Retail)":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"

                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A",
                "Dial": "Limited"
            },
            "FiRST One Plan 10GB (Retail)":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "Other Offerings": "N/A",
                "Dial": "Limited"
            },
            "FiRST One Plan 5GB (Enterprise)":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A",
                "Dial": "Limited"
            },
            "FiRST One Plan 10GB (Enterprise)":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A",
                "Dial": "Limited"
            },
            "BE 50":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A",
                "Dial": "Limited"
            },
            "Minutes Plan - Normal Postpaid":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "Minutes Plan - 100 Minute Supplimentary Plan":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "NA"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "Minutes Plan - Minute Postpaid-C250":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "Minutes Plan - Minute Postpaid-C500":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "Minutes Plan - Minute Postpaid-C800":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "Minutes Plan - Minute Postpaid-C1400":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"

                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "Minutes Plan - Family Postpaid":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "Celcom Exect (CE) - Celcom Executive 50 (CE50)":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A",
                "Dial": "Limited"
            },
            "Celcom Exect (CE) - Celcom Executive 250 (CE250)":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A",
                "Dial": "Limited"
            },
            "Celcom Exect (CE) - Celcom Executive 20 (CE20)":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A",
                "Dial": "Limited"
            },
            "Celcom Exect (CE) - 1+5":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Limited"
                },
                "Video Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Limited"
                },
                "Other Offerings": "Card Usage with Limit #1",
                "Dial": "Limited"
            },
            "1+5 Plan":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Limited"
                },
                "Video Call": {
                    "On-net": "Card Usage with Limit #1",
                    "Off-net": "Card Description #1",
                    "Dial": "Limited"
                },
                "Other Offerings": "N/A",
                "Dial": "Limited"
            },
            "Business 1 + 5":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "SMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "MMS": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "Video Call": {
                    "On-net": "Card Description #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "Limited"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "Power Plan - P28":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "Card Usage with Limit #1",
                "Dial": "Limited"
            },
            "Power Plan - P48":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A"
            },
            "Power Plan - P98":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "Power Plan - P148":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "Old MI Quota":{
                "Internet": {
                    "weekday Internet": "Card Usage with Limit #1",
                    "weekend Internet": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "SMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "CMP":{
                "Internet": {
                    "weekday Internet": "Card PPU #1",
                    "weekend Internet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "Platinum (TM Group & OKU)":{
                "Internet": {
                    "weekday Internet": "Card CTA #1",
                    "weekend Internet": "Card CTA #1",
                    "Dial": "NA"
                },
                "Call": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "On-net": "Card PPU #1",
                    "Off-net": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Video Call": {
                    "On-net": "N/A",
                    "Off-net": "N/A",
                    "Dial": "NA"
                },
                "Other Offerings": "N/A",
                "Dial": "NA"
            },
            "Add-Ons":{
                "Internet Add On":"Card Usage with Limit #1",
                "Music Add On":"Card Usage with Limit #1",
                "Video Add On":"Card Usage with Limit #1",
                "Carry forward internet":"Card Usage with Limit #1",
                "Calls":"Card Usage with Limit #1",
                "SMS":"Card Usage with Limit #1",
                "Roaming":"Card Bundle #2 CTA"
            },
            "Add-Ons-AutoRenew":{
                "Internet Add On":"Card Toggle #1",
                "Music Add On":"Card Toggle #1",
                "Video Add On":"Card Toggle #1",
                "Carry forward internet":"Card Toggle #1",
                "Calls":"Card Toggle #1",
                "SMS":"Card Toggle #1",
                "Roaming":"Card Toggle #1",
            }


        },
        "prepaid":{
            "Xpax plan": {
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "N/A",
                    "150MB": "N/A",
                    "Dial": "NA"

                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": ""
                },
                "Special Features": {
                    "Free FB": "Card Usage with Limit #1",
                    "Free Basic":" Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Activation Bonus":"Card Usage with Limit #1",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Xpax": {
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "N/A",
                    "150MB": "N/A",
                    "Dial": "NA"

                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": ""
                },
                "Special Features": {
                    "Free FB": "Card Usage with Limit #1",
                    "Free Basic":" Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Activation Bonus":"Card Usage with Limit #1",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Xpax RM10": {
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "N/A",
                    "150MB": "N/A",
                    "Dial": "NA"

                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": ""
                },
                "Special Features": {
                    "Free FB": "Card Usage with Limit #1",
                    "Free Basic":" Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Activation Bonus":"Card Usage with Limit #1",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Xpax Turbo": {
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "N/A",
                    "150MB": "N/A",
                    "Dial": "NA"
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": "NA"
                },
                "Special Features": {
                    "Free Basic": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Activation Bonus":"Card Usage with Limit #1",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Magic SIM": {
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "N/A",
                    "150MB": "N/A",
                    "Dial": "NA"
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": "NA"
                },
                "Special Features": {
                    "Free Basic": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Activation Bonus":"Card Usage with Limit #1",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Xpax24": {
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "PPU"
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": "NA"
                },
                "Special Features": {
                    "Free 3 calls":" Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "New SOX": {
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": "NA"
                },
                "Special Features": {
                    "1 Freebie": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "New UOX": {
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": "NA"
                },
                "Special Features": {
                    "Dial": "Limited"

                },
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "X2": {
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": "NA"
                },
                "Special Features": "N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "IOX": {
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": "NA"
                },
                "Special Features": {
                    "Facebook": "Free 10GB",
                    "Basic Internet": "Free 10GB",
                    "Dial": "Limited"
                },
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "1Malaysia":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A"
                },
                "Special Features": {
                    "Facebook": "Free 10GB",
                    "Basic Internet": "Free 10GB",
                    "Dial": "Limited"
                },
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Xmax [Peak]":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "MMS": "Card PPU #1",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A"
                },
                "Special Features": "N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Xmax [Offpeak]":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "MMS": "Card PPU #1",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A"
                },
                "Special Features": "N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "XMid":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "MMS": "N/A",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A"
                },
                "Special Features":"N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Xlite [Peak]":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "MMS": "Card PPU #1",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A"
                },
                "Special Features":"N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Xlite [Offpeak]":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "MMS": "Card PPU #1",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A"
                },
                "Special Features": "N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "XPax":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "Card PPU #1",
                "FF Video": "Card PPU #1",
                "Friends & Family SMS":{
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Special Features":"N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"In Plan Details",
                "Early Reload Bonus":"In Plan Details",
                "Surprise Bonus":"In Plan Details",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Blue":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "Card PPU #1",
                "FF Video": "Card PPU #1",
                "Friends & Family SMS":{
                    "Onnet": "Card PPU #1",
                    "Offnet": "N/A",
                    "Dial": "PPU"
                },
                "Special Features": "N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Blue Khas":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": ""
                },
                "Special Features":"N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Frenz":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "N/A",

                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": ""
                },
                "Special Features":"N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Celcom Frenz":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"

                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": "NA"
                },
                "Special Features": "N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "SOX":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "Card PPU #1",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "Limited"

                },
                "Special Features":"N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"In Plan Details",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "UOX":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "Card PPU #1",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "Limited"
                },
                "Special Features": "N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"In Plan Details",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Traveler":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": ""
                },
                "Special Features": "N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Sukses":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Dial": "PPU",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": ""
                },
                "Special Features": "N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Sukses International":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "SMS": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1",
                    "Dial": "PPU"
                },
                "MMS": "Card PPU #1",
                "Data PPU":  {
                    "RM10 Cap": "Card PPU #1",
                    "150MB": "Card Usage with Limit #1",
                    "Dial": "Limited"
                },
                "FF Voice": "N/A",
                "Dial": "",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "N/A",
                    "Offnet": "N/A",
                    "Dial": ""
                },
                "Special Features": "N/A",
                "Activation Bonus":"N/A",
                "Every Month Bonus":"In Plan Details",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Internet Plan - Monthly Internet":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "SMS": {
                    "Onnet": "15 sen/sms",
                    "Offnet": "15 sen/sms"
                },
                "MMS": "",
                "Data PPU":  {
                    "RM10 Cap": "30 sen/min",
                    "150MB": "Card Usage with Limit #1",
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "15 sen/sms",
                    "Offnet": "15 sen/sms"
                },
                "Special Features": {
                    "Facebook": "Free 10GB",
                    "Basic Internet": "Free 10GB"
                },
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Internet Plan - Weekly Internet":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "SMS": {
                    "Onnet": "15 sen/sms",
                    "Offnet": "15 sen/sms"
                },
                "MMS": "",
                "Data PPU":  {
                    "RM10 Cap": "30 sen/min",
                    "150MB": "Card Usage with Limit #1",
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "15 sen/sms",
                    "Offnet": "15 sen/sms"
                },
                "Special Features": {
                    "Facebook": "Free 10GB",
                    "Basic Internet": "Free 10GB"
                },
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Internet Plan - Daily Internet":{
                "Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "Video Call": {
                    "Onnet": "Card PPU #1",
                    "Offnet": "Card PPU #1"
                },
                "SMS": {
                    "Onnet": "15 sen/sms",
                    "Offnet": "15 sen/sms"
                },
                "MMS": "",
                "Data PPU":  {
                    "RM10 Cap": "30 sen/min",
                    "150MB": "Card Usage with Limit #1",
                },
                "FF Voice": "N/A",
                "FF Video": "N/A",
                "Friends & Family SMS":{
                    "Onnet": "15 sen/sms",
                    "Offnet": "15 sen/sms"
                },
                "Special Features": {
                    "Facebook": "Free 10GB",
                    "Basic Internet": "Free 10GB"
                },
                "Activation Bonus":"N/A",
                "Every Month Bonus":"N/A",
                "Early Reload Bonus":"N/A",
                "Surprise Bonus":"N/A",
                "Birthday Bonus":"N/A",
                "Bonus Banyak Guna":"N/A",
                "Bonus Setia":"N/A"
            },
            "Add-Ons":{
                "Internet Plan - Monthly Internet":"Card Toggle #1",
                "Internet Plan - Weekly Internet":"Card Toggle #1",
                "Internet Plan - Daily Internet":"Card Usage with Limit #1",
                "Internet Quota Add-on":"Card Usage with Limit #1",
                "Late Night Internet":"Card Usage with Limit #1",
                "Video Walla":"Card Usage with Limit #1",
                "Music Walla":"Card Usage with Limit #1",
                "Instagram Walla":"Card Usage with Limit #1",
                "Facebook Walla":"Card Usage with Limit #1",
                "Call Add-on":"Card Usage with Limit #1",
                "Basic Internet":"Card Usage with Limit #1",
                "IDD: Asia Pass":"Card Bundle #2 CTA",
                "Roaming Pass":"Card Bundle #2 CTA"
            }
        }
    }


}
