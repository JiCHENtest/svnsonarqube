// /* Directives */

// asb.directive('validPasswordC', function(userInfo) {
//   return {
//     require: 'ngModel',
//     scope: {
//       reference: '=validPasswordC'
//     },
//     link: function(scope, elm, attrs, ctrl) {
//       ctrl.$parsers.unshift(function(viewValue, $scope) {
//           console.log(viewValue);
//         var noMatch = viewValue != scope.reference
//         ctrl.$setValidity('noMatch', !noMatch);        
//         userInfo.setConfirmPass(viewValue);
//         return (noMatch)?noMatch:!noMatch;
//       });

//       scope.$watch("reference", function(value) {;
//         ctrl.$setValidity('noMatch', value === ctrl.$viewValue);

//       });
//     }
//   }
// });

// asb.directive('fallbackSrc', function () {
//               var fallbackSrc = {
//               link: function postLink(scope, iElement, iAttrs) {
//               iElement.bind('error', function() {
//                             angular.element(this).attr("src", iAttrs.fallbackSrc);
//                             });
//               }
//               }
//               return fallbackSrc;
//               });

// asb.directive('actualImage', ['$timeout', function($timeout) {
//     return {
//          link: function($scope, element, attrs) {
//              function waitForImage(url) {
//                  var tempImg = new Image();
//                  tempImg.onload = function() {
//                      $timeout(function() {
//                          element.attr('src', url);
//                      });
//                  }
//                  tempImg.src = url;
//              }

//              attrs.$observe('actualImage', function(newValue) {
//                  if (newValue) {
//                      waitForImage(newValue);
//                  }
//              });
//          }
//     }
// }]);