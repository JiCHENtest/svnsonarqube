
/**
 * @author Capgemini 
 */

/**
 * @Module Name - utilService
 * @Description - Singleton class for webservices, This is common gateway for all fetch/post calls 
 */

import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';
import { JsonstoreService } from './jsonstore';
import { LocalStorage } from './localStorage';
import { HandleError } from './handleerror';
import { ConstantData } from "./constantService";
import { GatewayService } from '../global/utilService';
declare var WLResourceRequest;
//declare var WL;
//declare var WLAuthorizationManager;



@Injectable()
export class ReloadService {

  public isUserLoggedIn: Boolean = true;
  public isFirstTimeUser: Boolean = true;
  loader;
  public isLoaderOn: Boolean = false;
  XPAX: any;
  isPrepaidVal: boolean;
  mobileNumber: any;
  BillingAccountNo: any;
  customerInfo: any;
  selectedMobileNumber: any;
  PhonebookNumber: any;
  checkingValue: number = 0;
  ErrorStatus: boolean = false;
  loginMobileNumber : any;
  transactionDate: any;
  transactionId: any;
  is_reload: any;
  loginMobileNumberServiceType:any;
  constructor(public alertCtrl: AlertController, public loadingCtrl: LoadingController, public jsonstoreService: JsonstoreService, public localStorage: LocalStorage, public handleerror: HandleError, public gatewayService: GatewayService) {
    //this.MfpInit();
    //   var mobile_number = {};
    //   this.jsonstoreService.getData(mobile_number, "mobilenumber").then(
    //   (result_mob) => {
    //   console.log("mobile_number update() ", JSON.stringify(result_mob));
    //   this.loginMobileNumber = result_mob.length > 0 ? result_mob[0].json.mobilenum : "";
    //   },
    //   (err) => {
    //     console.log("mobile_number update() err ", err);
    //  alert('NO DATA FOUND');//   this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
    //   }); 
  };


  gettingLoginNumber() {
    return this.loginMobileNumber;
    //return this.loginMobileNumber
  }
  setingLoginNumber(loginNumber) {
    this.loginMobileNumber = loginNumber;
  }
  setingLoginNumberServiceType(type){
    this.loginMobileNumberServiceType = type;
  }
  gettingLoginNumberServiceType(){
   return this.loginMobileNumberServiceType;
  }
  MfpInit() {
    console.debug('-- trying to init WL client');
  }

  showingHidenLoader(status) {
    if (status) {
      this.checkingValue++;
    } else {
      this.checkingValue--;
    }

  }
  getshowingHidenLoader() {
    return this.checkingValue;
  }
  GetIsFirstTimeUser() {
    return this.isFirstTimeUser;
  }
  DataAlignment(gettingDate) {
    var day = gettingDate.slice(0, 8);
    var year = day.match(/.{1,4}/g);
    var date = year[1].match(/.{1,2}/g);
    var finalDate = date[1] + '/' + date[0] + '/' + year[0];
    return finalDate;
  }

  SetFirstTimeUser(value) {
    this.isFirstTimeUser = value;
  }


  GetSelectMobileNumber() {
    return this.selectedMobileNumber;
  }
  SetSelectMobileNumber(value) {
    this.selectedMobileNumber = value;
  }

  GetErrorStatu() {
    return this.ErrorStatus;
  }
  SetErrorStatus(value) {
    this.ErrorStatus = value;
  }

  getPhoneBookNumber() {
    return this.PhonebookNumber;
  }

  setPhonebookNumber(value) {
    this.PhonebookNumber = value;
  }
  setTransactionDate(value) {
    this.transactionDate = value;
  }
  GetTransactionDate() {
    return this.transactionDate;
  }
  setTransactionId(value) {
    this.transactionId = value;
  }
  GetTransactionId() {
    return this.transactionId;
  }
  setIsreload(value) {
    this.is_reload = value;
  }
  getIsreload() {
    return this.is_reload;
  }



  //Get Validty Date and Balace Prepaid Number in Reload 
  FeachValidityBlance(selectedNumber) {
    try {
      //alert("SelectedNumber"+selectedNumber)
      // var getMobileNumber = "0136426573";
      var params = {
        "MobileNumber": selectedNumber
      }
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetValidityBalance, "POST", params).then((res) => {

          console.log("*********************************** component GetValidityBalance FeachValidityBlance" + JSON.stringify(res));
          //alert("AlertFine"+res);
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  GetValidityBalance failure FeachValidityBlance" + e);
            // alert("ERROR"+e);
            //alert("error user info");
            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }
  }
  //Feach ALL Plans related CreditReload
  //Feach ALL Plans related InternetPlans




  //fetchSavedCards

  FetchSavedCards(param) {
    try {
      //alert("SelectedNumber"+selectedNumber)
      // var getMobileNumber = "0136426573";
      var params = { "loginID": param };
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.fetchSavedCards, "POST", params).then((res) => {

          console.log("*********************************** component FetchSavedCards " + JSON.stringify(res));
          //alert("AlertFine"+res);
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  FetchSavedCards failure " + e);
            // alert("ERROR"+e);
            //alert("error user info");
            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }
  }




  //service for DigitalPIn
  CheckDigtalPin(params) {

    try {

      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.DigitPin, "POST", params).then((res) => {
          console.log("*********************************** component  CheckDigtalPin" + JSON.stringify(res));
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  failure CheckDigtalPin" + e);
            //alert("error user info");
            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }



  }


  PrepaidPaymentGatway(params) {

    try {

      return new Promise((resolve, reject) => {

        this.gatewayService.CallAdapter(ConstantData.adapterUrls.PrepaidPaymentURL, "POST", params).then(function (res) {
          console.log("*********************************** component  PrepaidPaymentGatway" + JSON.stringify(res));
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  failure PrepaidPaymentGatway" + e);
            //alert("error user info");
            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }



  }

  //




  //Feach ALL Plans related CreditReload
  FeachingCreditReloadPlans() {
    try {
      //alert("SelectedNumber"+selectedNumber)
      // var getMobileNumber = "0136426573";
      var params = {}
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetCreditReloadPlans, "POST", params).then((res) => {

          console.log("*********************************** component GetValidityBalance FetchUserInfo" + JSON.stringify(res));
          //alert("AlertFine"+res);
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  GetValidityBalance failure FetchUserInfo" + e);
            // alert("ERROR"+e);
            //alert("error user info");
            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }
  }
  //Feach ALL Plans related InternetPlans
  FeachingInternetPlans() {
    try {
      //alert("SelectedNumber"+selectedNumber)
      // var getMobileNumber = "0136426573";
      var params = {}
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetInternetPlans, "POST", params).then((res) => {

          console.log("*********************************** component GetValidityBalance FetchUserInfo" + JSON.stringify(res));
          //alert("AlertFine"+res);
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  GetValidityBalance failure FetchUserInfo" + e);
            // alert("ERROR"+e);
            //alert("error user info");
            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }
  }

  EcemPassDetails(selectedNumber) {

    try {

      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.ECEMPlanDetails, "POST", selectedNumber).then(function (res) {

          console.log("*********************************** component ECEMPlanDetails" + JSON.stringify(res));
          //alert("AlertFine"+res);
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  failure ECEMPlanDetails" + e);
            //alert("error user info FetchUserInfo");

            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }

  }
  EcemEligible(selectedNumber) {

    try {
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.ECEMEligible, "POST", selectedNumber).then(function (res) {

          console.log("*********************************** component ECEMEligible" + JSON.stringify(res));
          //alert("AlertFine"+res);
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  failure ECEMEligible" + e);
            //alert("error user info FetchUserInfo");

            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }

  }
  EcemSubmission(params) {

    try {

      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.ECEMERedeemraya, "POST", params).then(function (res) {

          console.log("*********************************** component ECEMERedeemraya" + JSON.stringify(res));
          //alert("AlertFine"+res);
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  failure ECEMERedeemraya" + e);
            //alert("error user info FetchUserInfo");

            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }

  }
  //service for youthQRY
  CheckyouthQRY(mobileNumber) {

    var params = {
      "MobileNumber": mobileNumber
    }

    try {

      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.youthQRYKawKaw, "POST", params).then((res) => {
          console.log("*********************************** component  youthQRYKawKaw" + JSON.stringify(res));
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  failure youthQRYKawKaw" + e);
            //alert("error user info");
            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }



  }

  checkAddedNewNumber(addedNumber) {
    var _that = this;
    //alert("fetch info mobile number"+this.mobileNumber);
    try {
      var params = {
        "SERIAL_NUM": addedNumber//this.localStorage.get("MobileNum","")
      }
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetUserInfo, "POST", params).then(function (res) {
          console.log("*********************************** component success checkAddedNewNumber" + JSON.stringify(res));
          _that.handleerror.handleErrorFetchUserInfo(res, "FetchUserInfo").then((result) => {
            if (result == true) {
              console.log("hai if ", result);
              resolve(res);
            }
            else {
              console.log("hai else ", result);
              reject("error : no data found.");
            }

          }, (err) => {
            console.log("hai else err ", err);
            reject("error : no data found.");
          });
        })
          .catch(function (e) {
            console.log("******************************* component  failure checkAddedNewNumber" + e);
            //alert("error user info FetchUserInfo");

            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }
  }
  fetchTransactionDetails(params) {
    try {

      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.fetchTransDetails, "POST", params).then((res) => {

          console.log("*********************************** component fetchTransactionDetails" + JSON.stringify(res));

          //alert("AlertFine"+res);
          resolve(res);

        })
          .catch(function (e) {
            console.log("******************************* component failure fetchTransactionDetails" + e);

            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }
  }

}
