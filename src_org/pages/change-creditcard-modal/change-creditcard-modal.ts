import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { GatewayService } from '../../global/utilService';
/**
 * Generated class for the ChangeCreditcardModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-change-creditcard-modal',
  templateUrl: 'change-creditcard-modal.html',
})
export class ChangeCreditcardModalPage {
  selectedCard: string;
  cardArray;
  second:string = "(Default)";
  is_autobilling:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public events: Events,public viewCtrl: ViewController, private renderer: Renderer2, public gatewayService: GatewayService) {
    this.cardArray = this.navParams.get('getCardArrayCard');
    // for(var i=0;i< this.cardArray.length; i++){
    
    //   this.cardArray.cardType=this.GetCardType(this.cardArray[i].masked);
    // }
    // events.subscribe('isAutobilling', (inAutobilling) => {
    //   console.log(' inAutobilling::'+ inAutobilling);
    //   this.is_autobilling=true;
    // });
    this.selectedCard =  this.navParams.get('myCard');
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Change credit card modal", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangeCreditcardModalPage');

  }
  carDetails(mobileNumber){
      var hiddenDetails = mobileNumber.replace(/.(?=.{4})/g,'*');
      return hiddenDetails;
  }
  dismiss() {
    console.log("Selected is "+ JSON.stringify(this.selectedCard));
    this.viewCtrl.dismiss(this.selectedCard);
  }
  dismissWithoutSave(){
    this.viewCtrl.dismiss();

  }
  GetCardType(number){
    // visa
    var re = new RegExp("^4");
    if (number.match(re) != null){
      return "Visa";
    }
    // Mastercard 
    // Updated for Mastercard 2017 BINs expansion
     else if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number)) {
      return "Mastercard";
     }

     return "";
    }
}
