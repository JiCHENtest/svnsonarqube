import { Component, Renderer2 } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GatewayService } from '../../global/utilService';

/**
 * Generated class for the MyDealsCardsStyleGuidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-deals-cards-style-guide',
  templateUrl: 'my-deals-cards-style-guide.html',
})
export class MyDealsCardsStyleGuidePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private renderer: Renderer2, public gatewayService:GatewayService) {
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("My deals card style", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyDealsCardsStyleGuidePage');
  }

}
