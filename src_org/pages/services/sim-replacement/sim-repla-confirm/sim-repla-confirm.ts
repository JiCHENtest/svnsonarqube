import { Component, ViewChild, Renderer2 } from '@angular/core';
import { NavController, NavParams, Nav, AlertController, ViewController  } from 'ionic-angular';
import { Events } from "ionic-angular";

import { SimReplacementProvider } from '../sim-replacementProvider';
import { GatewayService } from '../../../../global/utilService';
import { CreditManageService } from '../../credit-manage/CreditManageService';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { ServicesPage } from '../../services';

@Component({
  selector: 'page-sim-repla-confirm',
  templateUrl: 'sim-repla-confirm.html',
})
export class SimReplaConfirmPage {
  myVar: boolean = false;
  simReplacementParam: any;
  userInfoData: any;
  mobileNo: any;
  address: any;
  costTotal: any;
  costService: any;
  costRegion: any;
  creditBalance: any;
  remainingBalance: string;
  numberOfDays: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public events: Events,
    public simReplacementProvider: SimReplacementProvider,
    public gatewayService: GatewayService,
    public creditManageService: CreditManageService,
    public loadingCtrl: LoadingController,
    public viewCtrl: ViewController,
    private renderer: Renderer2) {
    this.simReplacementParam = this.navParams.get('simreplaceparam');
    this.userInfoData = this.navParams.get('userinfodata');
    this.numberOfDays = this.navParams.get('numberofdays');
    this.events.unsubscribe('servicesOnSimReplacementSuccess');
      events.subscribe('servicesOnSimReplacementSuccess',(params)=>{
        let currentIndex = this.viewCtrl.index;
        this.navCtrl.remove(2,this.viewCtrl.index-1);
        //this.navCtrl.pop();
     });
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("SIM replacement confirmation", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmationScreensPage');
    this.mobileNo = this.gatewayService.GetMobileNumber();
    this.address = this.simReplacementParam.streetAddress
      + ', ' + this.simReplacementParam.state
      + ', ' + this.simReplacementParam.city
      + ', ' + this.simReplacementParam.postalCode;
    this.costTotal = this.simReplacementParam.cost;
    this.costService = Number(this.userInfoData.costOnService).toFixed(2);
    this.costRegion = Number(this.userInfoData.costOnAddress).toFixed(2);
    if (this.userInfoData.ListOfServices.Services.ServiceType == "Postpaid" || this.userInfoData.ListOfServices.Services.Pre_Pos_Indicator
      == "Postpaid") {
      this.remainingBalance = Number(this.costTotal).toFixed(2);
    } else {
      //get credit balance in case of prepaid
      var loader = this.loadingCtrl.create({
        content: "Loading"
      });
      loader.present().then(() => {
        this.creditManageService.getAccountStatus().then((res) => {
          this.creditBalance = res["creditBlance"];
          let balanceamt = Number(this.creditBalance) - Number(this.costTotal);
          this.remainingBalance = Number(balanceamt).toFixed(2);
          // if(this.remainingBalance<0){
          //   this.presentsucess("Uh Oh.","Insufficient balance");
          // }
          loader.dismiss();
        }).catch((res) => {
          loader.dismiss();
          this.presentsucess("Uh Oh. Please be patient. System's busy.", "Please try again later");
        });
      });
    }
  }

  toggleList() {
    event.stopPropagation();
    this.myVar = !this.myVar;
    console.log("toggleList ", this.myVar);
  }

  submitSimReplacement() {
    let loader = this.loadingCtrl.create({
      content: "Loading"
    });
    loader.present().then(() => {
    this.simReplacementProvider.submitSimReplacement(this.simReplacementParam).then((res) => {
      console.log("sim replacment -> confirmation screen -> submitSimReplacement -> success -> " + JSON.stringify(res));
      if(res['status']=="success"){
        this.showsuccessAlert("Congratulations! SIM Replacement Request Sent.", 'Delivery will arrive at the stated address.', "Ok");
      }else{
        this.showsuccessAlert("Uh Oh. SIM Replacement Request Failed.", 'SIM replacement request failed due to '+res["message"], "Ok");
      }
      loader.dismiss();
    }).catch((res) => {
      console.log("sim replacment -> confirmation screen -> submitSimReplacement -> error -> " + res);
      loader.dismiss();
      this.presentsucess("Uh Oh. Please be patient. System's busy.", "Please try again later");
    });
    });
  }

  presentsucess(title, messageInput) {
    const alert = this.alertCtrl.create({
      title: title,
      message: messageInput,
      buttons: ['Ok'],
      cssClass: 'success-message error-message'
    });
    alert.present();
  }

  showsuccessAlert(title, msg, btn,cssClassVal?,dismissOnPageChange?,duration?) {
    var _that = this;
      try {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          buttons: [
          {
            text: btn,
            cssClass:'error-button-float-right submit-button',
            handler: () => {
              this.events.publish('servicesOnSimReplacementSuccess');
            }
          }],
          cssClass:'success-message error-message',
         enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
        });
        alert.present();
        if(duration!=null && alert._state==1){
          setTimeout(()=>{
            alert.dismiss().then(succ=>{
            });
          },2000);
        }
      }
      catch(e) {
        console.log("Exception : Alert open ",e);
      }
  }
}
