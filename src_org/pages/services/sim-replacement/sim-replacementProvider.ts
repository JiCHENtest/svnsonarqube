import { Injectable } from '@angular/core';
import { ConstantData } from "../../../global/constantService";
import { GatewayService } from '../../../global/utilService';
import { HandleError } from '../../../global/handleerror';


@Injectable()
export class SimReplacementProvider {

  State: string;
  Prepaid_Cost: number;
  PostalCode: string;
  Region: string;
  City: string;
  regionToGST: any = {
    "Peninsular Malaysia": "0.60",
    "Kuching": "0.95",
    "East Malaysia": "1.12"
  };

  constructor(public handleerror: HandleError, public gatewayService: GatewayService) {

  };

  submitSimReplacement(inputObj) {
    try {

      var params = {
        "MobileNumber": this.gatewayService.GetMobileNumber(),
        "cost": parseFloat(inputObj.cost).toFixed(2),
        "city": inputObj.city,
        "postalCode": inputObj.postalCode,
        "reason": inputObj.reason,
        "state": inputObj.state,
        "streetAddress": inputObj.streetAddress,
        "unitNumber": inputObj.unitNumber,
        "AccountId": inputObj.AccountId,
        "emailId": inputObj.emailId,
        "contactRowID": inputObj.contactRowID,
        "workPhNo": inputObj.ContactAlternateNum,
        "assetIntegrationId": inputObj.assetIntegrationId,
        "partNumber": inputObj.partNumber,
        "prodPromPartNumber": inputObj.prodPromPartNumber,
        "prodPromInstanceId": inputObj.prodPromInstanceId,
        "ServiceType": inputObj.ServiceType,
        "billingAccountNum": inputObj.billingAccountNum,
        "billingProfileId": inputObj.billingProfileId
      }
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.simReplacement_SubmitSimRelacement, "POST", params).then((res) => {
          console.log("Inside response:" + JSON.stringify(res));
          let orderNum = '';
          console.log("Inside orderNum");
            orderNum = res["Envelope"].Body.ChangeSIMResponse.ResponseBody.OrderNumber;
            if (orderNum != null && orderNum != "" && typeof orderNum != undefined) {
              resolve({
                status: "success",
                orderNum: orderNum
              });
            } else {
              resolve({
                status: "error",
                message: res["Envelope"].Body.ChangeSIMResponse.ResponseBody.Description
              });
            } 
        }).catch(function (e) {
          reject(e);
        });
      });
    } catch (e) {

    }
  }

  checkOpenOrderValidation() {
    try {
      var params = {
        "MobileNumber":  this.gatewayService.GetMobileNumber()
      }
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.simReplacement_checkOpenOrder, "POST", params).then((res) => {
          if (res["isSuccessful"] == true) {
            let isOpenOrder = res["Envelope"].Body.CEL_spcATG_spcOrder_spcCheck_spcWorkflow_spcWS_Output.CELOpenOrderFlag;
            resolve(isOpenOrder);
          } else {
            console.log("sim replacement -> checkOpenOrderValidation -> Unable to fetch open order status");
            reject("Unable to fetch open order status");
          }
        }).catch(function (e) {
          console.log("sim replacement -> checkOpenOrderValidation -> Error -> Unable to fetch open order status");
          reject("Unable to fetch open order status");
        });
      });
    } catch (e) {

    }
  }

  checkIsSimBlocked() {
    try {
      var params = {
        "BillingProfileId":  "" //get billing profile id from customer retrive
      }
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.simReplacement_isSimBlocked, "POST", params).then((res) => {
          if (res["isSuccessful"] == true) {
            let isSimBlocked = res["simBlocked"];
            resolve(isSimBlocked);
          } else {
            reject("Unable to check block sim status");
          }
        }).catch(function (e) {
          reject("Unable to check block sim status");
        });
      });
    } catch (e) {

    }
  }

  validateAddress(postalCode, plantype) {
    try {
      var params = {
        "postcode": postalCode,
        "plantype": plantype
      };
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.simReplacement_validateAddress, "POST", params).then((res) => {
          if (res["isSuccessful"] == true) {
            let responseData = res["Envelope"].Body.RegionCodeRetrieveResponse.ResponseBody.Responses;
            this.State = responseData.State;
            this.Prepaid_Cost = responseData.Prepaid_OTC;
            this.PostalCode = responseData.PostalCode;
            this.Region = responseData.Region;
            this.City = responseData.City;
            let GSTOnRegion = 0;
            if (plantype == "Postpaid") {
              GSTOnRegion = this.regionToGST[responseData.Region];
            }
            if (typeof responseData.Prepaid_OTC == 'undefined' || responseData.Prepaid_OTC == '') {
              responseData.Prepaid_OTC = 0;
            }
            this.Prepaid_Cost = Number(responseData.Prepaid_OTC) + Number(GSTOnRegion);
            resolve({
              simCostOnAddress: this.Prepaid_Cost
            });
          } else {
            reject("Unable to validate address");
          }
        }).catch(function (e) {
          reject("Unable to validate address");
        });
      });
    } catch (e) {

    }
  }

  getSimRelacementCharges(plantype) {
    try {
      var params = {
        "MobileNumber":  this.gatewayService.GetMobileNumber()
      }
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.simReplacement_getSimRelacementCharges, "POST", params).then((res) => {
          if (res["isSuccessful"] == true) {
            let simCharges = res["simCharges"];
            if (plantype == "Postpaid") {
              simCharges = Number(simCharges)+0.30;
            }
            console.log("getSimRelacementCharges -> For "+plantype+" is "+simCharges);
            resolve({
              simCostOnServiceLength: simCharges,
              asset_data: res["asset"]
            });
          } else {
            reject("Unable to get Sim Relacement Charges");
          }
        }).catch(function (e) {
          reject("Unable get Sim Relacement Charges");
        });
      });
    } catch (e) {

    }
  }


}