import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DeliveryOptionsPage } from "./delivery-options/delivery-options";
import { GatewayService } from '../../../global/utilService';

@Component({
  selector: 'page-sim-replacement',
  templateUrl: 'sim-replacement.html',
})
export class SimReplacementPage {

  selectedReason: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private renderer: Renderer2, public gatewayService: GatewayService) {
  }

  selectReason(reason){
    this.selectedReason = reason;
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("SIM replacement", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SimReplacementPage');
  }

    goToDeliveryOptions(){
        this.navCtrl.push(DeliveryOptionsPage,{reason:this.selectedReason});
    }
}
