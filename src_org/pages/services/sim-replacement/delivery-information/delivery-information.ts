import { Component, NgZone, Renderer2 } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { DashboardService } from '../../../home/homeService';
import { SimReplaConfirmPage } from '../sim-repla-confirm/sim-repla-confirm';
import { SimReplacementProvider } from '../sim-replacementProvider';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { GatewayService } from '../../../../global/utilService';


@Component({
  selector: 'page-delivery-information',
  templateUrl: 'delivery-information.html',
})
export class DeliveryInformationPage {

  CurrentAddressSelected: boolean = true;
  userInfoData: any = {};
  simReplaceParams: any;
  reason: string;
  numberOfDays: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private keyboard: Keyboard,
    private dashboardService: DashboardService,
    public simReplacementProvider: SimReplacementProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private gatewayService: GatewayService,
    private zone: NgZone,
    private renderer: Renderer2) {
    this.reason = this.navParams.get('reason');
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("SIM replacement delivery information", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeliveryInformationPage');
    //fetch users current address and mark Use Current Billing Address as true
    let userInfoData_fetch = this.dashboardService.GetUserInfo();
    this.userInfoData = JSON.parse(JSON.stringify(userInfoData_fetch));
    this.userInfoData.addrLine1 = this.getUserInfoAddr1(userInfoData_fetch);
    this.userInfoData.addrLine2 = "" + userInfoData_fetch.StreetAddress;
    this.userInfoData.PostalCode = userInfoData_fetch.PostalCode.split('_')[0];
  }
  handleKeyDone(e) {
    if (e.which === 13) {
      this.keyboard.close();
    }
  }

  onChange(isChecked) {
    if (isChecked) {
      this.CurrentAddressSelected = true;
      let userInfoData_fetch = this.dashboardService.GetUserInfo();
      this.userInfoData = JSON.parse(JSON.stringify(userInfoData_fetch));
      this.userInfoData.addrLine1 = this.getUserInfoAddr1(userInfoData_fetch);
      this.userInfoData.addrLine2 = "" + userInfoData_fetch.StreetAddress;
      this.userInfoData.PostalCode = userInfoData_fetch.PostalCode.split('_')[0];
    } else {
      this.CurrentAddressSelected = false;
    }
  }

  keyUpChecker(eve, varname) {
    let _thisRef = this;
    let returnVal = this.gatewayService.keyUpCheckerNumberOnly(eve, this.keyboard);
    this.zone.run(() => {
      _thisRef.userInfoData[varname] = returnVal;
    })
  }

  blurChecker(ev) {
    if (ev.currentTarget.children[0]) {
      ev.currentTarget.children[0].value = this.gatewayService.keyUpCheckerNumberOnly(ev, this.keyboard);
    }
    if (ev.currentTarget) {
      ev.currentTarget.value = this.gatewayService.keyUpCheckerNumberOnly(ev, this.keyboard);
    }

  }

  submitSimReplacement() {
    //first check if address is valid
    //then fetch params from that service for further 
    //fetch cost of sim replacement and other data
    if (this.userInfoData.ContactAlternateNum != "" && typeof this.userInfoData.ContactAlternateNum != undefined && this.userInfoData.ContactAlternateNum.length >= 10 && this.userInfoData.ContactAlternateNum.length <= 11) {
      this.isValidAddress();
    } else {
      this.presentsucess("Alternate Contact Number Incorrect", "Kindly enter a valid Alternative Delivery Contact number");
    }

  }

  isValidAddress() {
    //then fetch cost based on region
    let serviceType;
    var loader = this.loadingCtrl.create({
      content: "Loading"
    });
    loader.present().then(() => {
      if (this.userInfoData.ListOfServices.Services.Pre_Pos_Indicator) {
        serviceType = this.userInfoData.ListOfServices.Services.Pre_Pos_Indicator;
      }
      else {
        serviceType = this.userInfoData.ListOfServices.Services.ServiceType;
      }
      this.simReplacementProvider.validateAddress(this.userInfoData.PostalCode, serviceType).then((res) => {
        if (res["simCostOnAddress"]) {
          this.userInfoData.costOnAddress = res["simCostOnAddress"];
          this.fetchCostOfSimReplacement();
        } else {
          console.log("sim replacement -> delivery info -> isValidAddress " + res);
          this.presentsucess("Uh Oh. Please be patient. System's busy.", "Please try again later");
        }
        loader.dismiss();
      }).catch((res) => {
        console.log("sim replacement -> delivery info -> isValidAddress " + res);
        loader.dismiss();
        this.presentsucess("Uh Oh. Please be patient. System's busy.", "Please try again later");
      });
    });
  }

  fetchCostOfSimReplacement() {
    //navigate to confirmation page    
    var loader = this.loadingCtrl.create({
      content: "Loading"
    });
    let serviceType;
    if (this.userInfoData.ListOfServices.Services.Pre_Pos_Indicator) {
      serviceType = this.userInfoData.ListOfServices.Services.Pre_Pos_Indicator;
    }
    else {
      serviceType = this.userInfoData.ListOfServices.Services.ServiceType;
    }

    loader.present().then(() => {
      this.simReplacementProvider.getSimRelacementCharges(serviceType).then((res) => {
        if (res["simCostOnServiceLength"]) {
          this.userInfoData.costOnService = res["simCostOnServiceLength"];
          this.userInfoData.asses_data = res["asset_data"];
          if (typeof this.userInfoData.costOnAddress == 'undefined' || this.userInfoData.costOnAddress == '') {
            this.userInfoData.costOnAddress = 0;
          }
          if (typeof this.userInfoData.costOnService == 'undefined' || this.userInfoData.costOnService == '') {
            this.userInfoData.costOnService = 0;
          }
          let costTotal = Number(this.userInfoData.costOnAddress) + Number(this.userInfoData.costOnService);
          let serviceType;
          if (this.userInfoData.ListOfServices.Services.Pre_Pos_Indicator) {
            serviceType = this.userInfoData.ListOfServices.Services.Pre_Pos_Indicator;
          }
          else {
            serviceType = this.userInfoData.ListOfServices.Services.ServiceType;
          }
          this.simReplaceParams = {
            cost: costTotal.toFixed(2),
            city: this.userInfoData.City,
            postalCode: this.userInfoData.PostalCode,
            reason: this.reason,
            state: this.userInfoData.State,
            streetAddress: this.userInfoData.addrLine1 + ',' + this.userInfoData.addrLine2,
            unitNumber: this.userInfoData.UnitNo,
            AccountId: this.userInfoData.CustomerRowId,
            emailId: this.userInfoData.ContactEmail,
            contactRowID: this.userInfoData.ContactRowID,
            workPhNo: this.userInfoData.ContactBusinessPhone,
            assetIntegrationId: this.userInfoData.asses_data["AssetIntegrationID"],
            partNumber: this.userInfoData.asses_data["PartNumber1"],
            prodPromPartNumber: this.userInfoData.asses_data["ProdPromPartNumber"],
            prodPromInstanceId: this.userInfoData.asses_data["ProdPromInstanceId"],
            ServiceType: serviceType,
            billingAccountNum: this.userInfoData.ListOfServices.Services.AssetBillingAccountNo,
            billingProfileId: this.userInfoData.ListOfServices.Services.BillingProfileId,
            ContactAlternateNum: this.userInfoData.ContactAlternateNum
          }
          this.navCtrl.push(SimReplaConfirmPage, { simreplaceparam: this.simReplaceParams, userinfodata: this.userInfoData, numberofdays: this.numberOfDays });
          /*assest data sample
          "ICCID": "896019170197029412",
          "CAPACITY": "64K USIM",
          "ProdPromInstanceId": "1-SP6PUG",
          "ProdPromPartNumber": "PB10360",
          "CATEGORY": "SIM CARD",
          "PRODUCT": "CARDS",
          "IMSI": "502191340911048",
          "AssetIntegrationID": "1-SP6PTQ",
          "HLR TYPE": "UDCHLR (3G)",
          "PartNumber1": "MS00010",
          "PartNumber2": "SM00010",
          "BRAND": "REPLACEMENT"*/
        } else {
          console.log("sim replacement -> delivery info -> fetchCostOfSimReplacement " + res);
          this.presentsucess("Uh Oh. Please be patient. System's busy.", "Please try again later");
        }
        loader.dismiss();
      }).catch((res) => {
        console.log("sim replacement -> delivery info -> fetchCostOfSimReplacement " + res);
        loader.dismiss();
        this.presentsucess("Uh Oh. Please be patient. System's busy.", "Please try again later");
      });
    });
  }

  presentsucess(title, messageInput) {
    const alert = this.alertCtrl.create({
      title: title,
      message: messageInput,
      buttons: [{
        text: "OK",
        role: "Ok",
        cssClass: "error-button-float-right submit-button"
      }],
      cssClass: 'success-message error-message'
    });
    alert.present();
  }
  /**
   * Address line 1 consist of following
   * AddressYType;
      UnitNo;
      FloorNo;
      BuildingName;
      StreetType;
    */
  getUserInfoAddr1(userInfoData_fetch) {
    let userInfoReturn = "";
    userInfoReturn += (userInfoData_fetch.AddressYType != '' && typeof userInfoData_fetch.AddressYType != undefined) ? userInfoData_fetch.AddressYType + ", " : "";
    userInfoReturn += (userInfoData_fetch.UnitNo != '' && typeof userInfoData_fetch.UnitNo != undefined) ? userInfoData_fetch.UnitNo + ", " : "";
    userInfoReturn += (userInfoData_fetch.FloorNo != '' && typeof userInfoData_fetch.FloorNo != undefined) ? userInfoData_fetch.FloorNo + ", " : "";
    userInfoReturn += (userInfoData_fetch.BuildingName != '' && typeof userInfoData_fetch.BuildingName != undefined) ? userInfoData_fetch.BuildingName + ", " : "";
    userInfoReturn += userInfoData_fetch.StreetType;
    return userInfoReturn;
  }

}
