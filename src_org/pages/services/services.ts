import {Component, ViewChild, Renderer2} from "@angular/core";
import {NavController, Navbar, AlertController} from "ionic-angular";
import { OleoleShopPage } from "./oleole-shop/oleole-shop";
import { AutoBillingPage } from "./auto-billing/auto-billing";
import { CreditManagePage } from "./credit-manage/credit-manage";
import { UpgradeLatestPlanPage } from "./upgrade-latest-plan/upgrade-latest-plan";
import { SimReplacementPage } from "./sim-replacement/sim-replacement";
import { HomePage } from "../home/home";
import { CataloguePage } from "../services/oleole-shop/catalogue/catalogue";
// service
import { GatewayService } from '../../global/utilService';
import { ConstantData } from '../../global/constantService';
import { HandleError } from '../../global/handleerror';
import { AlertService } from '../../global/alert-service';
import { OleoleService } from '../../global/oleole-service';
import { LoadingController } from "ionic-angular/components/loading/loading-controller";
import { UpgardeLatestPlanService } from "./upgrade-latest-plan/upgrade-latest-planService";
import { Events } from "ionic-angular/util/events";
import { SimReplacementProvider } from "./sim-replacement/sim-replacementProvider";
import { ReloadService } from '../../global/reloadServices';
import { DashboardService } from '../../pages/home/homeService';


/**
 * Generated class for the ServicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
})



export class ServicesPage {
  @ViewChild(Navbar) navBar: Navbar;
  selectedTab: number;
  mobileNumber : any; // mobile number
  loader : any; // loader 
  plan_name:any;
  errorDetail: any;
  planData: any;
  dropdownNumber:any;
  loginNumber:any;
  
  isPrepaid:boolean;
  check_plan_name:any;
  loggedInIsPrepaid:any;
  showAutobill:any=false;
  billingType:any;
  userInfo :any;
  constructor(
    public navCtrl: NavController, 

    public gatewayService:GatewayService, 
    public handleerror: HandleError, 
    private alertCtrlInstance: AlertService,
    public oleoleService : OleoleService,
    private loadingCtrl: LoadingController,
    private upgradeService: UpgardeLatestPlanService,
    private events: Events,
    public reloadService: ReloadService,
    private simReplacementService: SimReplacementProvider,
    public alertCtrl: AlertController,
    public dashboardService:DashboardService,
    private renderer: Renderer2) {
 
      this.isPrepaid = this.gatewayService.isPrePaid(); //to check whether prepaid or postpaid number
      // this.loggedInIsPrepaid = this.reloadService.gettingLoginNumberServiceType();
      // if(this.loggedInIsPrepaid == 'Prepaid')
      // {
      //   this.loggedInIsPrepaid = true;
      // }
      // else{
      //   this.loggedInIsPrepaid = false;
      // }
      this.dropdownNumber = this.gatewayService.GetMobileNumber();
      this.loginNumber = this.reloadService.gettingLoginNumber();
      this.userInfo = this.dashboardService.GetUserInfo();
      console.log("user infor in service.ts ++++"+JSON.stringify(this.userInfo));
      this.billingType= this.userInfo["ListOfServices"].Services.BillingType;

     
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Services", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicesPage');
  
    
    var prodPromeName = this.userInfo.ListOfServices.Services.ProdPromName;
    
      prodPromeName = prodPromeName.toLowerCase();

    if(this.loginNumber==this.dropdownNumber && this.billingType=="Billable" && prodPromeName.indexOf("cmp") == -1){
      this.showAutobill=true;
    }
    console.log("show autobill="+this.showAutobill);

    // this.navBar.backButtonClick = (e:UIEvent)=>{
    //   console.log('click back button');
    //   this.navCtrl.push(HomePage);
    //  };
    var loader = this.loadingCtrl.create({
      content: "Loading"
    });
    console.log('ionViewDidLoad UpgradeLatestPlanPage');
    loader.present().then(() => {
      this.upgradeService.getPlanDetails().then((res) => {
        this.planData = res; 
        this.plan_name = res["plan_name"];
        this.check_plan_name = res["check_plan_name"];
        loader.dismiss();
      },
        (err) => {
          this.errorDetail = err;
          loader.dismiss();
        });
    });
   
  }

  goToOleOle(){
    // this.navCtrl.push( OleoleShopPage,{"isOleole" : true} );
    this.navCtrl.push( CataloguePage,{"isOleole" : false} );
  }

  goToEstore() {
    // this.navCtrl.push( OleoleShopPage,{"isOleole" : false} );
    this.navCtrl.push( CataloguePage,{"isOleole" : false} );    
  }

  // // service
  // ngOnInit() {

  //   var _that = this;
  //   _that.loader = _that.gatewayService.LoaderService();
  //   //loader.duration = 2000;
  //   _that.loader.present()

  //   var params = {
  //     "msisdn": _that.mobileNumber,
  //     "operation_id": "POSTPAID",
  //     "txn_id": _that.gatewayService.generateRandomNum(16)
  //   };

  //   return new Promise((resolve, reject) => {
  //     _that.gatewayService.CallAdapter(ConstantData.adapterUrls.GiftListForPostpaidAndPrepaid,"POST",params).then(function(res){
  //       console.log("************************************* component success FetchMydeals"+JSON.stringify(res));
  //       _that.handleerror.handleErrorGiftListForPostpaidAndPrepaid(res,"GiftListForPostpaidAndPrepaid").then((result) => {
  //           if(result == true) {
  //               console.log("hai if handleErrorGiftListForPostpaidAndPrepaid ",result);
  //               // resolve(res);
  //               // _that.events.publish('mydeal:created'); 
  //               _that.oleoleService.SetGiftList(res);   
  //               _that.loader.dismiss();
  //           }
  //           else {
  //               console.log("hai else handleErrorGiftListForPostpaidAndPrepaid ",result);
  //               _that.loader.dismiss();
  //               _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
  //               // reject("error : no data found.");
  //           }
  //       });
  //           // this.handleError(res,"FetchMydeals");
  //           // resolve(res);
  //     })
  //     .catch(function(e){
  //       console.log("****************************************** component failure handleErrorGiftListForPostpaidAndPrepaid "+e);
  //       console.log("error user info handleErrorGiftListForPostpaidAndPrepaid ");
  //       _that.loader.dismiss();
  //       _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
  //       // reject(e);
  //     });
  //   });
  // }


  goToAutoBilling(){
    this.navCtrl.push( AutoBillingPage,{} );
  }

  goToCreditManage(selectedTab){
    this.publishTabSelectedEvent(selectedTab);
    this.navCtrl.push( CreditManagePage,selectedTab );
  }

  publishTabSelectedEvent(index){
    let eventname = "creditmanage"+index;
    this.events.publish(eventname);
  }
  
  goToLatestPlan(){
    
    this.navCtrl.push( UpgradeLatestPlanPage,this.planData);
  }
  
  // goToSimReplacement(){
  //   this.simReplacementService.checkOpenOrderValidation().then((res)=>{
  //     if(res=='N'){
  //       this.navCtrl.push( SimReplacementPage,{} );
  //     }else{
  //       this.presentsucess("Uh ho","Already a open order");
  //     }      
  //   }).catch((err)=>{
  //     this.presentsucess("Uh Oh. Please be patient. System's busy.","Please try again later");
  //   });
    
  // }

  goToSimReplacement(){
    console.log("Inside goToSimReplacement");
    var loader = this.loadingCtrl.create({
      content: "Loading"
    });
     loader.present().then(() => {
          this.simReplacementService.checkOpenOrderValidation().then((res)=>{
            if(res=='N'){
              this.navCtrl.push( SimReplacementPage,{} );
              loader.dismiss();
            }else{
              loader.dismiss();
              this.presentsucess("Uh Oh. Unable to Process Request for SIM Replacement.","You have already requested for SIM replacement");
            }      
          }).catch((err)=>{
            loader.dismiss();
            this.presentsucess("Uh Oh. Please be patient. System's busy.","Please try again later");
          });
    });
 }

  presentsucess(title,messageInput) {
    const alert = this.alertCtrl.create({
      title: title,
      message: messageInput,
      buttons: [{
        text: "OK",
        role: "Ok",
        cssClass: "error-button-float-right submit-button"
      }],
      cssClass: 'success-message error-message'
    });
    alert.present();
  }
}
