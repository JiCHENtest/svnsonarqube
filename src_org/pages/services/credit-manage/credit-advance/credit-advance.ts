import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, Events, LoadingController, AlertController } from 'ionic-angular';
import { CreditManageService } from '../CreditManageService';
import { ConfirmationScreensPage } from '../../../confirmation-screens/confirmation-screens';
import { GatewayService } from '../../../../global/utilService';


/**
 * Generated class for the CreditAdvancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-credit-advance',
  templateUrl: 'credit-advance.html',
})
export class CreditAdvancePage {
  id: number;
  denomList: any;
  cardArray: any = [];
  cardDenom: any;
  creditBalance: any;
  indexValue: any;
  isCreditAdv: boolean = true;
  nextDisable: boolean = true;
  advanceResponse: any;
  remaining: any;
  cDenom: any;
  cardDenomination: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events, public loadingCtrl: LoadingController, public gatewayService: GatewayService,
    public creditMService: CreditManageService, public alertCtrl: AlertController, private renderer: Renderer2) {
    console.log('credit advance constructor called');


    this.events.unsubscribe('callAdvanceDenominations');
    events.subscribe('callAdvanceDenominations', (params) => {
      this.creditAdvanceDenominations();
    });


    this.events.unsubscribe('creditmanage2');
    events.subscribe('creditmanage2', (params) => {
      console.log('event subscribed creditmanage2 -> credit Advance');
    });
    this.getAccountInfo();
    this.id = navParams.get('id');
    if (this.id == 2) {
      //this.getAccountInfo();
    }


  }

  getAccountInfo() {
    this.creditBalance = this.creditMService.getCreditBalance();
  }

  presenterror(messageInput) {
    const alert = this.alertCtrl.create({
      title: 'Uh Oh. Credit Advance system is Busy',
      message: messageInput,
      buttons: ['Ok'],
      cssClass: 'success-message error-message',
      enableBackdropDismiss: false
    });
    alert.present();
  }

  addCss(index) {
    console.log("came into addCss function and index is" + index);
    this.cardDenom = "";
    this.indexValue = index;

    this.nextDisable = false;
    this.cardDenom = this.cardArray[index].DenomValue * 100;


    //this.denomMethod[index].type;
    console.log("came into addCss function and cardDenom is" + this.cardDenom);

  }


  CreditAdvanceEligibility() {
    var loader = this.loadingCtrl.create({
      content: "Loading"
    });
    loader.present().then(() => {
      this.creditMService.getCreditAdvanceEligibility().then((res) => {
        console.log("getCreditAdvanceEligibility -> " + JSON.stringify(res));
        loader.dismiss();
        this.cardArray = [];
        //this.cardDenom="";
        this.denomList = res["Envelope"].Body.CreditAdvanceResponse.ResponseBody.AllowedDenomList;
        if (this.denomList == "" || this.denomList == null || this.denomList == undefined) {
          this.nextDisable = true;
          var message = "You are not eligible for this service. Please contact our Customer service at 1111 for assistance";
          this.showAlert("Uh Oh. Unable to perform Credit Advance.", message, "OK");
        }

        if (this.denomList.AllowedDenom.length > 1) {
          for (let i in this.denomList.AllowedDenom) {
            var allowedDenom = this.denomList.AllowedDenom[i];
            var allowedDenomination = {
              "DenomFee": allowedDenom.DenomFee,
              "DenomType": allowedDenom.DenomType,
              "DenomValue": (allowedDenom.DenomValue) / 100
            }
            console.log("DenomValue multi" + allowedDenomination.DenomValue);
            this.cardArray.push(allowedDenomination);
          }
        } else {
          var allowedDenom = this.denomList.AllowedDenom;
          var allowedDenomination = {
            "DenomFee": allowedDenom.DenomFee,
            "DenomType": allowedDenom.DenomType,
            "DenomValue": (allowedDenom.DenomValue) / 100
          }
          console.log("DenomValue" + allowedDenomination.DenomValue);
          this.cardArray.push(allowedDenomination);
        }

      },
        (err) => {
          loader.dismiss();
          console.log("getCreditAdvanceEligibility Error " + err);
          if (err == "No internet connection")
            return;
          this.presenterror("Please try again later");
        });
    });
  }

  creditAdvanceDenominations() {
    var loader = this.loadingCtrl.create({
      content: "Loading"
    });
    loader.present().then(() => {
      console.log("cardDenom::" + this.cardDenom);
      this.cardDenomination = this.cardDenom / 100;
      console.log("card denomination::" + this.cardDenomination);
      // var denomination= parseFloat(this.cardDenomination).toFixed(2);
      // console.log("card denomination::"+ denomination);
      //   var finalDenom=cardDenomination-0.5;
      //   console.log(" finalDenom::"+ finalDenom);
      //   var denomination=finalDenom*100;
      //   console.log("denomination::"+ denomination);
      this.creditMService.creditAdvanceDenominations(this.cardDenomination).then((res) => {
        console.log("creditAdvanceDenominations -> " + JSON.stringify(res));
        loader.dismiss();
        this.advanceResponse = res["Envelope"].Body.CreditAdvanceResponse.ResponseHeader.StatusDescription;
        console.log("error/success response::" + this.advanceResponse);
        if (this.advanceResponse == "General error") {
          this.showAlertsuccess("Uh Oh. Credit Advance system is Busy", "Please try again later", "OK");
        } else {
          //success response
          this.events.publish('reloadHeaderManageCredit');

          this.gatewayService.setFlagForDashboardReload(true);
          this.showAlertsuccessBack("Advancing Happiness", "Your balance is now RM" + this.remaining.toFixed(2) + ". Credit Advance amount of RM" + this.cDenom.toFixed(2) + "  will be deducted on your next reload.", "OK");


          //getBalanceValidity
        }

      },
        (err) => {
          loader.dismiss();
          console.log("creditAdvanceDenominations Error " + err);
          if (err == "No internet connection")
            return;
          this.presenterror("Please try again later");
        });
    });
  }

  creditAdvanceTransaction() {
    var loader = this.loadingCtrl.create({
      content: "Loading"
    });
    loader.present().then(() => {
      this.creditMService.creditAdvanceTransaction().then((res) => {
        console.log("creditAdvanceTransaction -> " + JSON.stringify(res));


        loader.dismiss();
      },
        (err) => {
          loader.dismiss();
          console.log("creditAdvanceTransaction Error " + err);
          if (err == "No internet connection")
            return;
          this.presenterror("Please try again later");
        });
    });
  }

  ngOnDestroy() {
    this.events.unsubscribe('creditmanage2');
  }

  confirmAdvance() {
    //parseFloat(amount).toFixed(2);

    this.cDenom = this.cardDenom / 100;
    console.log("cDenom in confirmAdvance: " + this.cDenom);
    var minus = this.cDenom - 0.50;
    var finaldenom = this.cDenom + 0.50;
    console.log("finaldenom in confirmAdvance: " + finaldenom);
    console.log("creditBalance in confirmAdvance: " + this.creditBalance);
    // this.creditBalance=parseFloat(this.creditBalance).toFixed(2);
    this.remaining = Number(this.creditBalance) + Number(minus);
    console.log("remaining amount in confirmAdvance: " + this.remaining);


    var creditAdvance = {
      "cardDenom": "RM " + this.cDenom,
      "finalDenom": "RM " + finaldenom,
      "mobileNumber": this.creditMService.GetSelectMobileNumber(),
      // "remainingBal":"RM "+this.remaining
      "remainingBal": this.remaining
    }

    this.navCtrl.parent.parent.push(ConfirmationScreensPage, { "creditAdvanceDetails": creditAdvance, "isCreditAdv": this.isCreditAdv });
  }

  ionViewDidLoad() {

    console.log('credit advance ionviewdidload called');

  }

  ionViewDidEnter() {

    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Credit advance service", this.renderer);

    console.log("Credit balance in credit advance+++" + this.creditBalance);
    // if(this.creditBalance> 0.5){

    //   var res=" Credit advance is available for any balance RM0.50 and below.";
    //   this.showAlert("Uh Oh. Unable to perform Credit Advance", res , "OK");
    // }else{
    this.CreditAdvanceEligibility();
    //}
  }

  showAlert(title, msg, btn, cssClassVal?, dismissOnPageChange?, duration?) {
    console.log("Alert open");
    var _that = this;
    try {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: msg,
        buttons: [
          {
            text: btn,
            cssClass: 'error-button-float-right submit-button',
            handler: () => {
              console.log('Call to Action Clicked');
              //_that.navCtrl.setRoot(ServicesPage);
              _that.events.publish('goToServicePage');
            }
          }],
          cssClass:'success-message error-message',
          enableBackdropDismiss: false
        // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
      });
      alert.present();
      if (duration != null && alert._state == 1) {
        setTimeout(() => {
          alert.dismiss().then(succ => {
          });
        }, 2000);
      }
    }
    catch (e) {
      console.log("Exception : Alert open ", e);
    }



  }
  showAlertsuccess(title, msg, btn, cssClassVal?, dismissOnPageChange?, duration?) {
    console.log("Alert open");
    var _that = this;
    try {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: msg,
        buttons: [
          {
            text: btn,
            cssClass: 'error-button-float-right submit-button',
            handler: () => {
              console.log('Call to Action Clicked');
            }
          }],
          cssClass:'success-message error-message',
          enableBackdropDismiss: false
        });
        alert.present();
      }
      catch(e) {
        console.log("Exception : Alert open ",e);
      }
  }

  showAlertsuccessBack(title, msg, btn, cssClassVal?, dismissOnPageChange?, duration?) {
    console.log("Alert open");
    var _that = this;
    try {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: msg,
        buttons: [
          {
            text: btn,
            cssClass: 'error-button-float-right submit-button',
            handler: () => {
              console.log('Call to Action Clicked');
              _that.events.publish('goToServicePage');
            }
          }],
          cssClass:'success-message error-message',
          enableBackdropDismiss: false
        });
        alert.present();
      }
      catch(e) {
        console.log("Exception : Alert open ",e);
      }
  }


}
