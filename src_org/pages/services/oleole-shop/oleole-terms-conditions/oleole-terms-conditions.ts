import {Component, Renderer2} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import { GatewayService } from '../../../../global/utilService';
/**
 * Generated class for the LoginLegalTermsPage .
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-oleole-terms-conditions',
  templateUrl: 'oleole-terms-conditions.html',
})
export class OleOleTermsConditionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private renderer: Renderer2, public gatewayService: GatewayService) {
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("OleOle terms and conditions", this.renderer);
  }

}
