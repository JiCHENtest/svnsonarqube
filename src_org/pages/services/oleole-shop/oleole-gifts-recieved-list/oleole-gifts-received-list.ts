import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { ProductDetailsDesignGuidePage } from '../../../product-details-design-guide/product-details-design-guide';
import { GatewayService } from '../../../../global/utilService';

/**
 * Generated class for the OleoleGiftsRecievedListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-oleole-gifts-recieved-list',
  templateUrl: 'oleole-gifts-received-list.html',
})
export class OleoleGiftsRecievedListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private renderer: Renderer2, public gatewayService: GatewayService) {
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("OleOle gift recieved list", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OleoleGiftsRecievedListPage');
  }

  goToProductDetails(){
    this.navCtrl.push(ProductDetailsDesignGuidePage);
  }

}
