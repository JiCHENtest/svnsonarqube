

import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductDetailsDesignGuidePage } from '../product-details-design-guide/product-details-design-guide';
import { AlertService } from '../../global/alert-service';
import { GatewayService } from '../../global/utilService';
import { ReloadService } from '../../global/reloadServices';
/**
 * Generated class for the ReloadBonusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-promotional-campaign-bonus',
  templateUrl: 'promotional-campaign-bonus.html',
})

export class PromotionalCampaignPage {
  RegisterFlag;
  title = " Get 50% more bonus for your data if you are younger then 25.";
  imageName = "mydeals.png";
  kawkaw:any;
  loader:any;
   EmptyStatus:boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public reloadService:ReloadService, 
    public alertCtrlInstance:AlertService,public gatewayService:GatewayService,  private renderer: Renderer2) {
  }

  
  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Promotional campaign privilege", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReloadBonusPage');
    this.loader = this.gatewayService.LoaderService();
    this.loader.present();
    this.gatewayService.CheckyouthQRY().then((res)=> { 
      this.loader.dismiss();  
      if(res["STDNT_CHECK"].IS_ELIGIBLE == "Y" && res["STDNT_CHECK"].REGISTERED_ALRDY == "Y"){
           this.EmptyStatus = false;
        this.RegisterFlag = true;
        this.kawkaw = "kawkaw";
      }else if(res["STDNT_CHECK"].IS_ELIGIBLE == "Y" && res["STDNT_CHECK"].REGISTERED_ALRDY == "N"){
        this.RegisterFlag = false;
        this.EmptyStatus = true;
        this.kawkaw = "kawkaw";
    }else if(res["STDNT_CHECK"].IS_ELIGIBLE == "N"){
         this.kawkaw = false;
         this.EmptyStatus = true;
    }
  }).catch(err => {
     this.EmptyStatus = true;
      this.kawkaw = false;
      this.loader.dismiss();  
    console.log("*****reload*******"+JSON.stringify(err));
    if(err=="No internet connection")
    return;
     this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
    // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
});

  }
  
  goToProductDetails(){
    this.navCtrl.push(ProductDetailsDesignGuidePage,{
      param1:this.RegisterFlag,param2:this.kawkaw
    });
  }

}
