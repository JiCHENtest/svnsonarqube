import {Component, NgZone, Renderer2} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {SubscriptionSelectCountryPage} from "../subscription-select-country/subscription-select-country";
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { ConfirmationScreensPage } from '../../confirmation-screens/confirmation-screens';
//services
import { GatewayService } from '../../../global/utilService';
import { DashboardService } from '../../../pages/home/homeService';
import { SubscriptionService } from "../../subscriptions/subscriptionService";
import { GlobalVars } from "../../../providers/globalVars";
import { ConstantData } from '../../../global/constantService';
/**
 * Generated class for the SubscriptionConfirmationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-subscription-confirmation',
  templateUrl: 'subscription-confirmation.html',
})
export class SubscriptionConfirmationPage {

  planDetails : any;
  mobileNo : any;
  isRoaming : boolean = false;
  country_name : any;
  currentClass : any;
  vlrObject : any;
  vlrId : any;
  ischecked: boolean = false;
  isPrepaidThenShow : boolean = true;
  indexOfCountryPlansToShowQuote :number;
  isMaxUp: boolean = false;
  isIDD : boolean = false;

  title : any;
  forLateNight : any;

  islegacy : any;
  // "isMaxUp":true

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public gatewayService:GatewayService,
    public dashboardService : DashboardService,
    public subscriptionService : SubscriptionService,
    public zone : NgZone,
    public globalVar: GlobalVars,
    public loadingCtrl: LoadingController,
    private renderer: Renderer2) {

    // header title for page
    this.title = this.navParams.get('header_title');
    console.log("SubscriptionConfirmationPage -> constructor() -> title = ",this.navParams.get('header_title'));

    // paln title
    console.log("SubscriptionConfirmationPage -> constructor() -> Header title = ",this.navParams.get('header_title'));
    this.forLateNight = (this.navParams.get('header_title') == 'Late Night Internet (1am to 7am)') ? ' (1am to 7am)':'' ;
    // islegacy internet plan
    this.islegacy = (this.navParams.get('islegacy')== true && this.navParams.get('islegacy') != undefined) ? true: false;
    console.log("SubscriptionConfirmationPage islegacy ",this.islegacy);

    // is passing internet tab's plan
    this.isMaxUp = (this.navParams.get('isMaxUp') != undefined && this.navParams.get('isMaxUp') == true) ? true :false;
    console.log("SubscriptionConfirmationPage isMaxUp ",this.isMaxUp);
    // plan list
    this.planDetails = this.navParams.get('details');

    this.mobileNo = this.gatewayService.GetMobileNumber();
    console.log("this.planDetails ",JSON.stringify(this.planDetails))

    // VRL Object
    var userInfo = this.dashboardService.GetUserInfo();
    if(userInfo.ListOfServices.Services.ServiceType != "Postpaid")
      this.isPrepaidThenShow = true;
    else
      this.isPrepaidThenShow = false;

    this.vlrObject = this.gatewayService.GetVLRDetailsData();

    // VLR
    // this.vlrId = this.gatewayService.GetVLR();

    console.log("this.vlrObject ",JSON.stringify(this.vlrObject))

    // country details
    this.country_name = this.navParams.get('country_name');
    console.log("Info param ",JSON.stringify(this.navParams.get('country_name')));

    this.isRoaming = this.navParams.get('isRoaming');
    this.zone.run(()=>{
      this.indexOfCountryPlansToShowQuote = this.planDetails.PASS_NAME != undefined ? this.subscriptionService.indexOfCountryPlans(this.planDetails) : undefined;

      console.log("indexOfCountryPlansToShowQuote ",this.indexOfCountryPlansToShowQuote);
    });
    // button to show if vlrid matched or not
    // console.log("this.vlrObject != undefined ",this.vlrObject != undefined);
    // console.log("this.country_name != undefined ",(this.country_name != undefined));
    // console.log("this.planDetails.PASS_NAME.toLowerCase() !=1-Day CALLS &amp; SMS PASS ",this.planDetails.PASS_NAME.toLowerCase() !=("1-Day CALLS &amp; SMS PASS").toLowerCase());
    // console.log("this.planDetails.PASS_NAME.toLowerCase() !=7-Day 3-IN-1 PASS",this.planDetails.PASS_NAME.toLowerCase() !=("7-Day 3-IN-1 PASS").toLowerCase());
    // console.log("this.planDetails.PASS_NAME.toLowerCase() !=1-Day CALLS & SMS PASS",this.planDetails.PASS_NAME.toLowerCase() !=("1-Day CALLS & SMS PASS").toLowerCase());

    if(this.vlrObject != undefined && this.country_name != undefined && this.planDetails.PASS_NAME.toLowerCase() !=("1-Day CALLS &amp; SMS PASS").toLowerCase() && this.planDetails.PASS_NAME.toLowerCase() !=("7-Day 3-IN-1 PASS").toLowerCase() && this.planDetails.PASS_NAME.toLowerCase() !=("1-Day CALLS & SMS PASS").toLowerCase() ) {

       if(this.vlrObject.RoamingFlag != undefined &&
        this.vlrObject.RoamingFlag.CDATA != undefined) {

        var country_vlrId_ = this.country_name.Vlrids[0].id.split("/");
        var vlrId = this.vlrObject.RoamingFlag.CDATA;
        this.subscriptionService.checkVlrId(country_vlrId_,vlrId).then((vlr) => {
          console.log("vlr----------- ",vlr);

          // set according to vlr not matched for 1-day internet pass
          this.ischecked =  vlr;

          if(this.ischecked)
            this.subscriptionService.SetCheckVlrIdForOtherCountry(this.country_name, this.ischecked);
          else
            this.subscriptionService.SetCheckVlrIdForOtherCountry(this.country_name, this.ischecked);
          // else

          console.log("ischecked---------- ",vlr);
          console.log("this.country_name---------- ",this.country_name);

        });
        console.log(" in----------- ",this.ischecked);
        console.log("in this.country_name---------- ",this.country_name);
      }
      else {
        console.log(" out -----------------  ",this.ischecked);
        this.subscriptionService.SetCheckVlrIdForOtherCountry(this.country_name, this.ischecked);
        // if no RoamingFlag found.
        // this.ischecked = true;
      }
    }
    else {
      console.log("this.vlrObject == undefined tha is why.");
      this.ischecked = true;

      console.log(" outter -----------------  ",this.ischecked);
    }


  }


  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Subscription confirmation", this.renderer);
  }

  ionViewWillEnter() {
    this.isIDD = this.navParams.get('isIDD') ? true: false;
    console.log("SubscriptionConfirmationPage -> ionViewWillEnter -> isIDD = ",this.isIDD);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubscriptionConfirmationPage');

    // var params = {
    //   "MobileNumber": this.gatewayService.GetMobileNumber()
    // }

    // console.log("ionViewDidLoad params ",params);

    // this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetValidityBalance, "POST", params).then((res) => {

    //   console.log("*********************************** Subscription component GetValidityBalance FeachValidityBlance" + JSON.stringify(res));
    //   var creditBlance : any = '';
    //   var balanceArray : any = res["Envelope"].Body.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.BalanceRecordList.BalanceRecord;

    //   if(balanceArray.length == undefined) {

    //     if(balanceArray.AccountType == "2000") {
    //       creditBlance =  Number(balanceArray.Balance) /10000;
    //       creditBlance = "RM"+parseFloat(creditBlance).toFixed(2);
    //     }
    //   } else{
    //     balanceArray.forEach(element => {
    //       if(element.AccountType == "2000"){
    //         //this.lineValidity = this.reloadService.DataAlignment(element.ExpireTime);
    //         creditBlance =  Number(element.Balance) /10000;
    //         creditBlance = "RM"+parseFloat(creditBlance).toFixed(2);
    //       }
    //     });
    //   }

    //   console.log("creditBlance ",creditBlance);

    // });



  }

  goToConfirmation(){

    console.log("SubscriptionConfirmationPage -> goToConfirmation() -> Header titile = ",this.navParams.get('header_title'));

    this.navCtrl.push(ConfirmationScreensPage,{"confirmDetails":this.planDetails,"isRoaming":this.isRoaming,"isMaxUp":this.isMaxUp,"islegacy":this.islegacy,"header_title":this.navParams.get('header_title')});
  }

  // callback...
  myCallbackFunction = (_params) => {
    console.log("Callback ",JSON.stringify(_params));
    return new Promise((resolve, reject) => {

      this.country_name = _params;

      console.log(" hai  myCallbackFunction ",this.subscriptionService.GetCheckVlrIdForOtherCountry());
      var data_info : any = this.subscriptionService.GetCheckVlrIdForOtherCountry();

      console.log(" " ,this.country_name.name ," == ",data_info.country_name);
      if(this.country_name.name  == data_info.country_name && data_info.isVlrId == true)
        this.ischecked = true;
      else
        this.ischecked = false;

      // for 7-day and 1 day calls
      if(this.planDetails.PASS_NAME.toLowerCase() == ("1-Day CALLS &amp; SMS PASS").toLowerCase() || this.planDetails.PASS_NAME.toLowerCase() == ("7-Day 3-IN-1 PASS").toLowerCase() || this.planDetails.PASS_NAME.toLowerCase() == ("1-Day CALLS & SMS PASS").toLowerCase() ){
        console.log("irrespective of vlrid -> (1-day call and sms) & (7-Day 3-IN-1 PASS) i.e. button will be visible.");
        this.ischecked = true;
        console.log("ischecked ",this.ischecked);
      }

        console.log("myCallbackFunction -> myCallbackFunction -> ischecked = " ,this.ischecked);

        resolve();
    });
  }


  openSelectCountryModal() {
    let loading = this.loadingCtrl.create({
      content: "Loading"
    });

    loading.present();

    const modalOptions:any = {
      showBackdrop: false,
      enableBackdropDismiss: true
    }

    const  modal = this.modalCtrl.create(SubscriptionSelectCountryPage, {callback: this.myCallbackFunction, "plan_name":this.planDetails}, modalOptions );

    setTimeout(() => {
      loading.dismiss();
      modal.present();
    }, 5000);
    //PASS_PRICE_DISPLAY

    // this.navCtrl.push(SubscriptionSelectCountryPage,{callback: this.myCallbackFunction});
  }

  // isCapZone() {
  //   console.log("isCapZone() = ",this.globalVar.getCapZoneStatus());
  //   console.log("isCallBar() = ",this.globalVar.getcallBarredStatus());
  //   // return this.globalVar.getCapZoneStatus();
  //   return true;
  // }

}

