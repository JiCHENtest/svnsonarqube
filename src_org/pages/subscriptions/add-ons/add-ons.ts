import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';

//page to navigate
import { SubscriptionConfirmationPage } from '../subscription-confirmation/subscription-confirmation';
import { ProductDetailsDesignGuidePage } from '../../product-details-design-guide/product-details-design-guide';
import { SubscriptionProductDetailsDesignGuidePage } from "../../subscriptions/subscription-product-details-design-guide/subscription-product-details-design-guide";
import { GatewayService } from '../../../global/utilService';
// service 
import { SubscriptionService } from "../../subscriptions/subscriptionService";
import { DashboardService } from '../../../pages/home/homeService';
import { SubscriptionsTabService } from '../subscriptionsTabService';
import { OleoleService } from '../../../global/oleole-service';


//
// import { GatewayService } from '../../../global/utilService';
// import { ConstantData } from '../../../global/constantService';

// import _ from 'underscore';
/**
 * Generated class for the AddOnsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-ons',
  templateUrl: 'add-ons.html',
})
export class AddOnsPage {

  addOnceCategoryPlan: any;
  checkStatus: boolean;
  isGiftName: any;
  indexValue: any;

  isBasePlanAvailable: boolean = false;

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public subscriptionService: SubscriptionService,
    public dashboardService: DashboardService,
    public subscriptionsTabService: SubscriptionsTabService,
    public oleoleService: OleoleService,
    public gatewayService: GatewayService,
    private renderer: Renderer2
  ) {
    console.log("On add-ons");
    console.log(" ye below ", this.navParams.data);


  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Add ons subscription", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddOnsPage');

    this.indexValue = "";
    // console.log("Postpaid internet add-on ",this.subscriptionService.getUserInterSubscriptionInfoPostpaid());
    var giftType: any = this.navParams.data;
    if (giftType.p1 == 'POSTPAID')  // oleole mcp
      this.isGiftName = true;
    else if (giftType.p1 == 'PREPAID')  // oleole xpax
      this.isGiftName = false;
    else if (giftType.p1 == '' || giftType.p1 == undefined) // subscription
      this.isGiftName = true;


    console.log("AddOnsPage -> giftType = ", giftType);

    try {
      console.log("AddOnsPage -> giftType = ", giftType.p1);
    }
    catch (e) {
      console.log("Exceptions AddOnsPage -> giftType(catch) = ", e);
    }
    console.log("AddOnsPage -> this.isGiftName = ", this.isGiftName);

    this.events.subscribe('user:created', () => {

      // user and time are the same arguments passed in `events.publish(user, time)`
      var userInfo = this.dashboardService.GetUserInfo();

      console.log("pre_pos_Indicator ", userInfo.ListOfServices.Services.Pre_Pos_Indicator);

      if (userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid" && this.isGiftName) {
        console.log('Welcome AddOnsPage Postpaid');
        console.log("Postpaid internet add-on ", this.subscriptionService.getUserInterSubscriptionInfoPostpaid());
        console.log("Postpaid add-on ", this.subscriptionService.getUserAddonSubscriptionInfoPostpaid());

        // only for subscription , not for ole ole till now
        var isBase = this.gatewayService.getBasePlanForSubscription();

        if (isBase && (giftType.p1 == '' || giftType.p1 == undefined)) {
          this.isBasePlanAvailable = true;

        }
        else {
          this.isBasePlanAvailable = false;
        }

        console.log("isBasePlanAvailable 1 ", this.isBasePlanAvailable, "\n isBase ", isBase);

        this.addOnceCategoryPlan = (this.subscriptionService.getUserAddonSubscriptionInfoPostpaid() == undefined ? [] : this.subscriptionService.getUserAddonSubscriptionInfoPostpaid()).concat(this.subscriptionService.getUserInterSubscriptionInfoPostpaid() == undefined ? [] : this.subscriptionService.getUserInterSubscriptionInfoPostpaid());

        console.log("this.addOnceCategoryPlan Postpaid ", JSON.stringify(this.addOnceCategoryPlan));
      }
      else {

        // only for subscription , not for ole ole till now
        var isBase = this.gatewayService.getBasePlanForSubscription();

        if (isBase && (giftType.p1 == '' || giftType.p1 == undefined)) {
          this.isBasePlanAvailable = true;
          // console.log("isBasePlanAvailable ",this.isBasePlanAvailable);
        }
        else {
          this.isBasePlanAvailable = false;
        }

        console.log("isBasePlanAvailable 2 ", this.isBasePlanAvailable, "\n isBase ", isBase);

        console.log('Welcome AddOnsPage Prepaid');

        if (this.isGiftName && giftType.p1 == 'POSTPAID') {
          this.addOnceCategoryPlan = (this.subscriptionService.getUserAddonSubscriptionInfoPostpaid() == undefined ? [] : this.subscriptionService.getUserAddonSubscriptionInfoPostpaid()).concat(this.subscriptionService.getUserInterSubscriptionInfoPostpaid() == undefined ? [] : this.subscriptionService.getUserInterSubscriptionInfoPostpaid());
        }
        else {
          this.addOnceCategoryPlan = this.subscriptionService.getUserAddonSubscriptionInfoPrepaid();
        }

        console.log("this.addOnceCategoryPlan Prepaid ", this.addOnceCategoryPlan);
      }
      // console.log("add-on ", JSON.stringify(this.addOnceCategoryPlan))
    });

    // buy now from dashboard
    if (this.subscriptionsTabService.GetTabInfo() != 1 && this.subscriptionsTabService.GetTabInfo() != undefined) {
      // user and time are the same arguments passed in `events.publish(user, time)`
      var userInfo = this.dashboardService.GetUserInfo();

      console.log("pre_pos_Indicator other ", userInfo.ListOfServices.Services.Pre_Pos_Indicator);

      if (userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid" && this.isGiftName) {
        console.log('Welcome AddOnsPage Postpaid other');
        console.log("Postpaid internet add-on other ", this.subscriptionService.getUserInterSubscriptionInfoPostpaid());
        console.log("Postpaid add-on other ", this.subscriptionService.getUserAddonSubscriptionInfoPostpaid());

        // only for subscription , not for ole ole till now
        var isBase = this.gatewayService.getBasePlanForSubscription();

        if (isBase && (giftType.p1 == '' || giftType.p1 == undefined)) {
          this.isBasePlanAvailable = true;
          // console.log("isBasePlanAvailable ",this.isBasePlanAvailable);
        }
        else {
          this.isBasePlanAvailable = false;
        }

        console.log("isBasePlanAvailable 3 ", this.isBasePlanAvailable, "\n isBase ", isBase);

        this.addOnceCategoryPlan = (this.subscriptionService.getUserAddonSubscriptionInfoPostpaid() == undefined ? [] : this.subscriptionService.getUserAddonSubscriptionInfoPostpaid()).concat(this.subscriptionService.getUserInterSubscriptionInfoPostpaid() == undefined ? [] : this.subscriptionService.getUserInterSubscriptionInfoPostpaid());
        // hege.concat(stale)
        console.log("this.addOnceCategoryPlan Postpaid other ", JSON.stringify(this.addOnceCategoryPlan));
      }
      else {

        // only for subscription , not for ole ole till now
        var isBase = this.gatewayService.getBasePlanForSubscription();

        if (isBase && (giftType.p1 == '' || giftType.p1 == undefined)) {
          this.isBasePlanAvailable = true;
          // console.log("isBasePlanAvailable ",this.isBasePlanAvailable);
        }
        else {
          this.isBasePlanAvailable = false;
        }

        console.log("isBasePlanAvailable 4 ", this.isBasePlanAvailable, "\n isBase ", isBase);

        if (this.isGiftName && giftType.p1 == 'POSTPAID') {
          this.addOnceCategoryPlan = (this.subscriptionService.getUserAddonSubscriptionInfoPostpaid() == undefined ? [] : this.subscriptionService.getUserAddonSubscriptionInfoPostpaid()).concat(this.subscriptionService.getUserInterSubscriptionInfoPostpaid() == undefined ? [] : this.subscriptionService.getUserInterSubscriptionInfoPostpaid());
        }
        else {
          console.log('Welcome AddOnsPage Prepaid other ');
          this.addOnceCategoryPlan = this.subscriptionService.getUserAddonSubscriptionInfoPrepaid();
          console.log("this.addOnceCategoryPlan Prepaid other ", this.addOnceCategoryPlan);
        }

      }
    }
  }
  ngOnDestroy() {
    this.indexValue = "";
  }

  goToDetailsPage(addOnDetails, header_title) {

    if (this.subscriptionService.isCapZone()) {
      this.subscriptionService.presentConfirm();
    } else {
      console.log("goToDetailsPage addOnDetails");

      console.log("AddOnsPage -> goToDetailsPage() -> GetIsOleoeAvailable() = ", this.subscriptionService.GetIsOleoeAvailable());

      console.log("AddOnsPage -> goToDetailsPage() -> this.navParams.get('typeOfGift') = ", this.navParams.get('typeOfGift'));

      // for header title if 'maxup' make it as 'Internet Quota Add-on'
      // header_title = (header_title == 'MAX UP' ? 'Internet Quota Add-on' : header_title)

      header_title = (header_title == 'MAX UP') ? 'Internet Quota Add-on' : (header_title == 'Late Night Internet' ? 'Late Night Internet (1am to 7am)' : header_title);

      this.indexValue = addOnDetails;


      console.log("AddOnsPage -> goToDetailsPage() -> header_title = ", header_title);

      // to check is user is gbshare admin or not
      var userInfo = this.dashboardService.GetUserInfo();

      var adminNumber = userInfo["ListOfServices"].Services.BillingType;

      console.log("AddOnsPage -> goToDetailsPage() -> goToDetailsPage = ", adminNumber);

      if (adminNumber == "Billable" && userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid" && userInfo.ListOfServices.Services.SmeGroupId != "") {
        // SubscriptionProductDetailsDesignGuidePage for GBshare admin
        // this.navCtrl.parent.parent.push(SubscriptionProductDetailsDesignGuidePage, { "details": addOnDetails, "header_title": header_title });
        if (this.subscriptionService.GetIsOleoeAvailable()) {
          this.navCtrl.parent.parent.push(ProductDetailsDesignGuidePage, { "subscription_gift_details": addOnDetails, "giftTypes": this.navParams.data, "header_title": header_title });
        }
        else {
          this.navCtrl.parent.parent.push(SubscriptionConfirmationPage, { "details": addOnDetails, "header_title": header_title });
        }


      }
      else if (this.subscriptionService.GetIsOleoeAvailable()) {
        // ProductDetailsDesignGuidePage
        this.navCtrl.parent.parent.push(ProductDetailsDesignGuidePage, { "subscription_gift_details": addOnDetails, "giftTypes": this.navParams.data, "header_title": header_title });
        // this.oleoleService.SetXpaxGiftListObj("PREPAID");
      }
      else {
        // for subscription confirmation
        this.navCtrl.parent.parent.push(SubscriptionConfirmationPage, { "details": addOnDetails, "header_title": header_title });
      }
    }

  }

  ionViewWillEnter() {
    console.log("ngAfterViewInit");
    this.indexValue = "";
    // this.addOnceCategoryPlan = this.subscriptionService.getUserAddonSubscriptionInfo();
  }

}
