import { Component, OnInit, Renderer2 } from "@angular/core";
import { NavController, NavParams, Events } from "ionic-angular";

import { SubInternetPage } from "../subscriptions/sub-internet/sub-internet";
import { AddOnsPage } from "../subscriptions/add-ons/add-ons";
import { IddPage } from "../subscriptions/idd/idd";
import { RoamingPage } from "../subscriptions/roaming/roaming";
import { SuperTabsController } from '../../components/custom-ionic-tabs';

import { SubscriptionService } from "../subscriptions/subscriptionService";
import { GatewayService } from '../../global/utilService';
import { ConstantData } from '../../global/constantService';
import { DashboardService } from '../../pages/home/homeService';
import { SubscriptionsTabService } from './subscriptionsTabService';
import { GlobalVars } from "../../providers/globalVars";

import { HandleError } from '../../global/handleerror';
import { AlertService } from '../../global/alert-service';

import { OleoleService } from '../../global/oleole-service';
import { ReloadPage } from '../reload/reload';
import { PayBillPage } from '../pay-bill/pay-bill';


/**
 * Generated class for the SubscriptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-subscriptions',
  templateUrl: 'subscriptions.html',
})
export class SubscriptionsPage implements OnInit {
  selectedTab: any;
  subscriptionTabs: any;
  tab1Page: any = SubInternetPage;
  tab2Page: any = AddOnsPage;
  tab3Page: any = IddPage;
  tab4Page: any = RoamingPage;
  // selectedTab:number = 0;
  loader: any;
  isOleoleGiftName: boolean = false;
  hideTabsForInternetAndAdon: boolean = false;
  hideTabsForInternetAndAdonForPreapid: boolean = false;
  userInfo: any;

  tabOleoleParm: any;
  titles: any;
  subtitles: any;

  count_to_push: number = 0;

  constructor(
    public events: Events,
    public globalVar: GlobalVars,
    public navCtrl: NavController,
    public navParams: NavParams,
    private tabsCtrl: SuperTabsController,
    public subscriptionsTabService: SubscriptionsTabService,
    public gatewayService: GatewayService,
    public subscriptionService: SubscriptionService,
    public dashboardService: DashboardService,
    public handleerror: HandleError,
    private alertCtrlInstance: AlertService,
    public oleoleService: OleoleService,
    private renderer: Renderer2
  ) {
    this.userInfo = this.dashboardService.GetUserInfo();
    console.log("On SubscriptionsPage");
    this.selectedTab = this.subscriptionsTabService.GetTabInfo();
    console.log("selectedTab ", this.selectedTab)

    if (this.selectedTab == undefined)
      this.selectedTab = 0;

    console.log("last page ", this.navCtrl.last())
    // console.log("last page ",JSON.stringify(this.navCtrl.last()))
    console.log("last page ", this.navCtrl.last().name)

    // title of page
    this.titles = (this.navCtrl.last().name == 'CataloguePage' || this.navCtrl.last().name == 'OleoleCatalogueListPage') ? 'OleOle' : 'Subscriptions';


    this.subtitles = (this.navCtrl.last().name == 'CataloguePage' || this.navCtrl.last().name == 'OleoleCatalogueListPage') ? 'Select your choice of e-gift' : 'Select Subcription Type';

    console.log("SubscriptionsPage -> selectedTab = ", this.selectedTab);
    // subscriptionService.registerObserver(this);
    // this.getSubscriptionInfos();
    events.subscribe('goToPayBillFromSubscription', (params) => {
      this.navCtrl.push(PayBillPage, {});
    });
    events.subscribe('GotoReload', (params) => {
      this.update();
    });

  }

  // ionViewDidEnter(){
  //   this.gatewayService.resetAllAnalyticData();
  //   this.gatewayService.setAnalyticPageNameProfileSegment("Subscriptions", this.renderer);
  // }

  ngOnDestroy() {
    this.events.unsubscribe('goToPayBillFromSubscription');
    this.events.unsubscribe('GotoReload');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubscriptionsPage');

    this.tabsCtrl.enableTabsSwipe(false, 'subscriptionTabs');
  }


  onTabSelect(ev: any) {
    // if(this.selectedTab==undefined)
    // this.selectedTab = ev.index;

    console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id + " this.selectedTab " + this.selectedTab);
  }

  ngOnInit() {

    this.isOleoleGiftName = (this.navParams.get('typeOfGift') == "POSTPAID" || this.navParams.get('typeOfGift') == "PREPAID") ? true : false;

    console.log("this.navParams.get('typeOfGift') ", this.navParams.get('typeOfGift'));

    this.subscriptionService.SetIsOleoeAvailable(this.isOleoleGiftName);

    // to disbale internet , idd and roaming tabs , in process
    if (this.navParams.get('typeOfGift') == "POSTPAID") {
      this.hideTabsForInternetAndAdon = true;
      this.hideTabsForInternetAndAdonForPreapid = false;
      // this.oleoleService.SetXpaxGiftListObj("POSTPAID");
      this.tabOleoleParm = {
        "p1": this.navParams.get('typeOfGift')
      }
    }
    else if (this.navParams.get('typeOfGift') == "PREPAID") {
      this.hideTabsForInternetAndAdon = true;
      this.hideTabsForInternetAndAdonForPreapid = true;
      // this.oleoleService.SetXpaxGiftListObj("PREPAID");  
      this.tabOleoleParm = {
        "p1": this.navParams.get('typeOfGift')
      }
    }

    var mcp_and_xpax = this.navParams.get('mcp_and_xpax');

    console.log("SubscriptionsPage -> oleoleGiftName -> ", this.isOleoleGiftName);

    console.log("SubscriptionsPage -> ngOnInit() -> ", mcp_and_xpax);

    var _that = this;
    _that.loader = _that.gatewayService.LoaderService();
    //loader.duration = 2000;
    _that.loader.present();

    console.log("Mobile ", this.gatewayService.GetMobileNumber());
    var params = {
      "msisdn": _that.gatewayService.GetMobileNumber(),//"60193522834"//this.localStorage.get("MobileNum","")
      "operation_id": "PROD_INQ",
      "txn_id": _that.gatewayService.generateRandomNum(16)
    };

    console.log("getSubscriptionInfos param ", params);

    if (!this.isOleoleGiftName) {
      // return new Promise((resolve, reject) => {
      _that.gatewayService.CallAdapter(ConstantData.adapterUrls.GetSubscriptionData, "POST", params).then(function (res) {
        console.log("********************************************** component success getSubscriptionInfos " + JSON.stringify(res));
        // alert("GetSubscriptionData "+JSON.stringify(res));
        var results: any = JSON.parse(JSON.stringify(res));
        // console.log("result ",result.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS)      
        try {
          _that.handleerror.handleErrorSubscriptionAddOnInfo(results, "SubscriptionAddOnInfo").then((result) => {
            if (result == true) {
              _that.loader.dismiss();
              console.log("subscription list ", result);
              // resolve(res);
              // for prepaid & post paid number
              _that.subscriptionService.setSubscriptionInfo(res);

              _that.events.publish('user:created');
              _that.getSubscriptionRoamingDataForPostAndPrepaid();
            }
            else {
              console.log("subscription list else ", result);
              _that.loader.dismiss();
              // reject("error : no data found.");
              _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
            }

          }, (err) => {
            console.log("subscription list err ", err);
            _that.loader.dismiss();
            // reject("error : no data found.");    
            _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
          });
        }
        catch (e) {
          console.log("getSubscriptionInfos ", e);
          _that.loader.dismiss();
          if (e == "No internet connection")
            return;
          _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
        }
      })
        .catch(function (e) {
          console.log("******************************************** component failure getSubscriptionInfos " + e);
          // alert("error user info getSubscriptionInfos");
          // reject(e);
          _that.loader.dismiss();
          if (e == "No internet connection")
            return;
          _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
        });
      // });
    }
    else {
      _that.loader.dismiss();
      console.log("for oleole gif mcp and xpax");


      _that.subscriptionService.setSubscriptionInfo(mcp_and_xpax);

      console.log("after set value for");
      setTimeout(() => {
        console.log("set event");
        _that.events.publish('user:created');
      }, 500);
    }
  }

  //  roaming 
  getSubscriptionRoamingDataForPostAndPrepaid() {
    var _that = this;

    console.log(" Sim type ", this.userInfo.ListOfServices.Services.ServiceType);

    var params = {
      "sref": this.gatewayService.GetMobileNumber(),//"60193522834"//this.localStorage.get("MobileNum","")
      "operation_id": "ROAMPROD_INQ",
      "txn_id": this.gatewayService.generateRandomNum(16),
      "vlrid": "LOCAL"
    };

    if (this.userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid") {

      console.log("getSubscriptionRoamingDataForPostAndPrepaid post param ", params);

      // return new Promise((resolve, reject) => {
      _that.gatewayService.CallAdapter(ConstantData.adapterUrls.SubscriptionRoamingListForPostpaid, "POST", params).then(function (res) {
        console.log("********************************************** component success getSubscriptionRoamingDataForPostAndPrepaid post " + JSON.stringify(res));
        // error handler
        _that.handleerror.handleErrorSubscriptionRoamingDataForPostAndPrepaid(res, "SubscriptionRoamingDataForPostAndPrepaid").then((result) => {
          if (result == true) {
            console.log("hai if SubscriptionRoamingDataForPostAndPrepaid ", result);
            try {
              _that.subscriptionService.setSubscriptionPostOrPrePlan(res);
              _that.loader.dismiss();
              // _that.events.publish('user:created');      
            }
            catch (e) {
              _that.loader.dismiss();
              console.log("getSubscriptionRoamingDataForPostAndPrepaid post ", e)
              if (e == "No internet connection")
                return;
              _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");

            }
          }
          else {
            console.log("hai else SubscriptionRoamingDataForPostAndPrepaid ", result);
            _that.loader.dismiss();
            _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
          }
        });

      })
        .catch(function (e) {
          console.log("******************************************** component failure getSubscriptionRoamingDataForPostAndPrepaid post " + e);
          // alert("error user info getSubscriptionRoamingDataForPostAndPrepaid post ");
          // reject(e);          
          _that.loader.dismiss();
          if (e == "No internet connection")
            return;
          _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
        });
      // });
    }
    else {
      console.log("getSubscriptionRoamingDataForPostAndPrepaid pre param ", params);
      // return new Promise((resolve, reject) => {
      _that.gatewayService.CallAdapter(ConstantData.adapterUrls.SubscriptionRoamingListForPrepaid, "POST", params).then(function (res) {
        console.log("********************************************** component success getSubscriptionRoamingDataForPostAndPrepaid pre " + JSON.stringify(res));

        var results: any = JSON.parse(JSON.stringify(res));

        _that.handleerror.handleErrorSubscriptionRoamingDataForPostAndPrepaid(results, "SubscriptionRoamingDataForPostAndPrepaid").then((result) => {
          if (result == true) {
            console.log("hai if SubscriptionRoamingDataForPostAndPrepaid pre ", result);
            try {
              _that.subscriptionService.setSubscriptionPostOrPrePlan(res);
              _that.loader.dismiss();
              // _that.events.publish('user:created');      
            }
            catch (e) {
              _that.loader.dismiss();
              console.log("getSubscriptionRoamingDataForPostAndPrepaid pre ", e);
              if (e == "No internet connection")
                return;
              _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");

            }
          }
          else {
            console.log("hai else SubscriptionRoamingDataForPostAndPrepaid pre", result);
            _that.loader.dismiss();
            _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
          }
        });
      })
        .catch(function (e) {
          console.log("******************************************** component failure getSubscriptionRoamingDataForPostAndPrepaid " + e);
          // alert("error user info getSubscriptionRoamingDataForPostAndPrepaid pre ");
          // reject(e);
          _that.loader.dismiss();
          if (e == "No internet connection")
            return;
          _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
        });
    }
  }




  update() {
    console.log("_____________________***************((((((((((((( ", this.count_to_push);

    if (this.userInfo.ListOfServices.Services.Pre_Pos_Indicator == 'Prepaid') {
      console.log("count_to_push ", this.count_to_push);

      this.navCtrl.push(ReloadPage);



    } else {
      this.navCtrl.push(PayBillPage);
    }
  }

}
