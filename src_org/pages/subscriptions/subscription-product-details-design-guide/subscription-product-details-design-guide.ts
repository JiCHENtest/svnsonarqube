import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';

// page
import { ConfirmationScreensPage } from '../../confirmation-screens/confirmation-screens';

// service
import { GatewayService } from '../../../global/utilService';

/**
 * Generated class for the ProductDetailsDesignGuidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'subscription-product-details-design-guide',
  templateUrl: 'subscription-product-details-design-guide.html',
})
export class SubscriptionProductDetailsDesignGuidePage {
  
  showGiftContainer: boolean = true;
  gift_request : any;
  numberToSend : any;
  isSelf : boolean = true;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public toastCtrl:ToastController,
    public gatewayService: GatewayService,
    private renderer: Renderer2) {

    // pre-selected value for gift type
    this.gift_request = 'buy_for_self';

    // for self gift number to be selected
    this.numberToSend = this.gatewayService.GetMobileNumber(); 

    // to disable input 
    this.isSelf = true;

    console.log("SubscriptionProductDetailsDesignGuidePage \n header_title = ",this.navParams.get('header_title'),"\n Confirm Details ",this.navParams.get('details'));
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Subscription product details design guide", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductDetailsDesignGuidePage');
  }

  SubscribeToGbShareAdminPostpaid() {
    
    this.navCtrl.push(ConfirmationScreensPage,{"confirmDetails":this.navParams.get('details'),"isRoaming":false,"isMaxUp":false,"islegacy":false,"header_title":this.navParams.get('header_title')});
  }

  copyVoucherCode () {
    this.toastCtrl.create({message:'Copied',duration:3000}).present();
  }

}
