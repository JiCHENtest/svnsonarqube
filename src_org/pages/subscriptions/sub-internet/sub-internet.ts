import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';

//page to navigate
import { SubscriptionConfirmationPage } from '../subscription-confirmation/subscription-confirmation';
import { ProductDetailsDesignGuidePage } from '../../product-details-design-guide/product-details-design-guide';

// service
import { SubscriptionService } from "../../subscriptions/subscriptionService";
import { DashboardService } from '../../../pages/home/homeService';
import { SubscriptionsTabService } from '../subscriptionsTabService';
import { GatewayService } from '../../../global/utilService';

/**
 * Generated class for the SubInternetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-sub-internet',
  templateUrl: 'sub-internet.html',
})
export class SubInternetPage {

  subInternetCategoryPlan: any;
  userInfo: any;
  isGiftName: any;
  isLegacy: any;
  indexValue: any;
  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public subscriptionService: SubscriptionService,
    public dashboardService: DashboardService,
    public subscriptionsTabService: SubscriptionsTabService,
    public gatewayService: GatewayService,
    private renderer: Renderer2
  ) {
    // 
    this.userInfo = this.dashboardService.GetUserInfo();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubInternetPage');
    this.indexValue = "";
    var _that = this;
    var userInfo = _that.dashboardService.GetUserInfo();

    var giftType: any = this.navParams.data;
    if (giftType.p1 == 'POSTPAID')  // oleole mcp
      this.isGiftName = true;
    else if (giftType.p1 == 'PREPAID')  // oleole xpax
      this.isGiftName = false;
    else if (giftType.p1 == '' || giftType.p1 == undefined) // subscription
      this.isGiftName = true;

    console.log("SubInternetPage -> ionViewDidLoad() -> isGiftName = ", this.isGiftName);
    console.log("SubInternetPage -> ionViewDidLoad() -> giftType = ", this.isGiftName);
    console.log("SubInternetPage -> ionViewDidLoad() -> giftType.p1 = ", this.isGiftName.p1);

    this.events.subscribe('user:created', () => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('Welcome sub-internet ', userInfo.ListOfServices.Services.ServiceType);
      if (userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid" && this.isGiftName) {
        // this.subInternetCategoryPlan = this.subscriptionService.getUserInterSubscriptionInfoPostpaid();
        // this.subInternetCategoryPlan = [];
        console.log("sun-internet legecy ", this.subscriptionService.getUserLegacySubscriptionInfoPostpaid());
        this.subInternetCategoryPlan = (this.subscriptionService.getUserLegacySubscriptionInfoPostpaid() == undefined ? [] : this.subscriptionService.getUserLegacySubscriptionInfoPostpaid());
        if (userInfo.ListOfServices.Services.PLAN == "P148 Plan" && (giftType.p1 == '' || giftType.p1 == undefined)) {
          this.subInternetCategoryPlan = [];
        }
        console.log("Length of postpaid legecy ", this.subInternetCategoryPlan.length);

        if (this.subInternetCategoryPlan.length == 0)
          this.isLegacy = false;
        else
          this.isLegacy = true;

        // aditi
        // if say plan p148

        console.log("isLegacy ", this.isLegacy);
        console.log("sub-internet postpaid ", JSON.stringify(this.subInternetCategoryPlan));
      }
      else {
        if (this.isGiftName && giftType.p1 == 'POSTPAID') {
          this.subInternetCategoryPlan = [];
        }
        else {
          // aditi
          if ((userInfo.ListOfServices.Services.PLAN == "First One Plan for Business" || userInfo.ListOfServices.Services.PLAN == "First One Plan" || userInfo.ListOfServices.Services.PLAN == "P148 Plan") && (giftType.p1 == '' || giftType.p1 == undefined))
            this.subInternetCategoryPlan = []
          else
            this.subInternetCategoryPlan = this.subscriptionService.getUserInterSubscriptionInfoPrePaid();


          // this.subInternetCategoryPlan = this.subscriptionService.getUserInterSubscriptionInfoPrePaid();
          console.log("sub-internet prepaid ", JSON.stringify(this.subInternetCategoryPlan));
        }
      }
    });
    // buy now from dashboard
    if (this.subscriptionsTabService.GetTabInfo() != 0 && this.subscriptionsTabService.GetTabInfo() != undefined) {
      console.log("Came by Buy now (Dashboard) ", this.subscriptionsTabService.GetTabInfo())
      console.log('Welcome sub-internet ', userInfo.ListOfServices.Services.ServiceType);

      if (userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid" && this.isGiftName) {
        // this.subInternetCategoryPlan = [];

        console.log("sun-internet legecy other ", this.subscriptionService.getUserLegacySubscriptionInfoPostpaid())
        this.subInternetCategoryPlan = (this.subscriptionService.getUserLegacySubscriptionInfoPostpaid() == undefined ? [] : this.subscriptionService.getUserLegacySubscriptionInfoPostpaid());
        if (userInfo.ListOfServices.Services.PLAN == "P148 Plan" && (giftType.p1 == '' || giftType.p1 == undefined)) {
          this.subInternetCategoryPlan = [];
        }
        console.log("Length of postpaid legecy other ", this.subInternetCategoryPlan.length);

        // islagacy for if some legacy number which has internet plans but for normal user there is no internet plan in case of postpaid
        if (this.subInternetCategoryPlan.length == 0)
          this.isLegacy = false;
        else
          this.isLegacy = true;

        // aditi
        // if say plan p148

        console.log("isLegacy other ", this.isLegacy);

        console.log("sub-internet postpaid other ", JSON.stringify(this.subInternetCategoryPlan));
      }
      else {
        if (this.isGiftName && giftType.p1 == 'POSTPAID') {
          this.subInternetCategoryPlan = [];
        }
        else {
          // aditi
          if (userInfo.ListOfServices.Services.PLAN == "First One Plan for Business" || userInfo.ListOfServices.Services.PLAN == "First One Plan" || userInfo.ListOfServices.Services.PLAN == "P148 Plan" && (giftType.p1 == '' || giftType.p1 == undefined))
            this.subInternetCategoryPlan = []
          else
            this.subInternetCategoryPlan = this.subscriptionService.getUserInterSubscriptionInfoPrePaid();

          // this.subInternetCategoryPlan = this.subscriptionService.getUserInterSubscriptionInfoPrePaid();
          console.log("sub-internet prepaid ", JSON.stringify(this.subInternetCategoryPlan));
        }

      }
    }

  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Internet plans subscription", this.renderer);
  }

  ngOnDestroy() {
    this.indexValue = "";
  }
  goToDetailsPage(subInternetDetails, header_title) {

    if (this.subscriptionService.isCapZone()) {
      this.subscriptionService.presentConfirm();
    } else {

      console.log("goToDetailsPage subInternetDetails");
      // islagacy for if some legacy number which has internet plans but for normal user there is no internet plan in case of postpaid
      var title_info: any = (subInternetDetails.HEADER_TITLE == 'MAX UP' && subInternetDetails.HEADER_TITLE == undefined) ? 'Internet Quota Add-on' : subInternetDetails.HEADER_TITLE;
      this.indexValue = subInternetDetails;
      var check_for_maxup_and_mi = false;

      if (this.userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid")
        check_for_maxup_and_mi = false;
      else
        check_for_maxup_and_mi = true;

      console.log("SubInternetPage -> goToDetailsPage() -> check_for_maxup_and_mi ", check_for_maxup_and_mi);

      console.log("SubInternetPage -> goToDetailsPage() -> GetIsOleoeAvailable() = ", this.subscriptionService.GetIsOleoeAvailable());

      if (this.subscriptionService.GetIsOleoeAvailable()) {
        // ProductDetailsDesignGuidePage
        this.navCtrl.parent.parent.push(ProductDetailsDesignGuidePage, { "subscription_gift_details": subInternetDetails, "isMaxUp": check_for_maxup_and_mi, "islegacy": this.isLegacy, "giftTypes": this.navParams.data, "header_title": header_title });
      }
      else {
        this.navCtrl.parent.parent.push(SubscriptionConfirmationPage, { "details": subInternetDetails, "isMaxUp": check_for_maxup_and_mi, "islegacy": this.isLegacy, "header_title": header_title });
      }
    }

    // ionViewWillLeave() {
    //   console.log('ionViewWillLeave SubInternetPage');
    // }

    // ionViewDidEnter() {
    //   console.log('ionViewDidEnter SubInternetPage');
    // }


  }
  ionViewWillEnter() {
    this.indexValue = "";
  }
}
