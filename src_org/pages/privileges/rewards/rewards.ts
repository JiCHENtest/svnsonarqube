import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { OfferingPage } from "./offering/offering";
import { RedeemedPage } from "./redeemed/redeemed";
import { ProductDetailsDesignGuidePage } from "../../product-details-design-guide/product-details-design-guide";
import { GatewayService } from '../../../global/utilService';

/**
 * Generated class for the RewardsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-rewards',
  templateUrl: 'rewards.html',
})
export class RewardsPage {
  tab1Page: any = OfferingPage;
  tab2Page: any = RedeemedPage;
  rewards:any;
  offrings:any;
  redeemed:any;
  constructor(public navCtrl: NavController, public events: Events, 
    public navParams: NavParams, public gatewayService: GatewayService, private renderer: Renderer2) {
    
  }
 
  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Rewards", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RewardsPage');
  }
  public openDetails() {
    this.navCtrl.push(ProductDetailsDesignGuidePage);
  }

}
