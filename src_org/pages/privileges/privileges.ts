import { Component, Renderer2 } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { ReloadBonusPage } from "../reload-bonus/reload-bonus";
import {FirstSytleOptinPage} from "./first-style/first-style-optin/first-style-optin";
import {FirstStyleSelectTypePage} from "./first-style/first-style-select-type/first-style-select-type";
import { PromotionalCampaignPage } from "../promotional-campaign/promotional-campaign-bonus";
import { GatewayService } from '../../global/utilService';
import { ConstantData } from '../../global/constantService';
import { ReloadService } from '../../global/reloadServices';
import { AlertService } from '../../global/alert-service';

import { PrivRewardsPage } from './first-style/priv-rewards/priv-rewards';
import { RewardsPage } from './rewards/rewards';
import { DashboardService } from "../home/homeService";
/**
 * Generated class for the PrivilegesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-privileges',
  templateUrl: 'privileges.html',
})
export class PrivilegesPage {
  loader: any; // loader
  isPrePaid:any;
  isNotCMPUser: boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public gatewayService: GatewayService,
    public reloadService: ReloadService,
    private alertCtrlInstance: AlertService,
    public dashboardService: DashboardService,
    private renderer: Renderer2) {
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Privileges", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrivilegesPage');
    this.checkPrePos();
    var userInfo = this.dashboardService.GetUserInfo();
    var prodPromeName = userInfo.ListOfServices.Services.ProdPromName;
    prodPromeName = prodPromeName.toLowerCase(); 
    this.isNotCMPUser = (prodPromeName.indexOf("cmp") != 0);
  }

  checkPrePos(){
    this.isPrePaid= this.gatewayService.isPrePaid();
  }
  goToPromotinalCampaign(){
    this.navCtrl.push(PromotionalCampaignPage);
  }

  goToRelaodBonusPage() {

    this.navCtrl.push(ReloadBonusPage);
  }

  goToRewards() {
    this.navCtrl.push(RewardsPage);
  }

  goToFirstStylePage (){
    //this.navCtrl.push(FirstStyleSelectTypePage);

    var _that = this;
    _that.loader = _that.gatewayService.LoaderService();
    _that.loader.present().then(() => {

      //query voucher service call
      _that.queryVoucherServiceCall().then(function (res) {
        console.log(res);
        

        if(res["isSuccessful"]){
          if(res["QueryVouchersResponse"].ResponseMessage =='Success'){
            console.log('If 1');
            console.log(res["QueryVouchersResponse"]);
            console.log(res["QueryVouchersResponse"].OPTIN);
            //res["QueryVouchersResponse"].OPTIN = "NO";
            if(res["QueryVouchersResponse"].OPTIN == "YES" && (res["QueryVouchersResponse"].PrivilegeType == 'MONTHLY' || res["QueryVouchersResponse"].PrivilegeType == 'ONETIME'))
            {
              //store privilege data
              _that.gatewayService.SetPrivilegeData(res);
              console.log('If 2');
              //go to offers page
              _that.navCtrl.push(PrivRewardsPage);

            }else{
              console.log('else');
              //go to opt-in page i.e. here only
              _that.navCtrl.push(FirstStyleSelectTypePage);
            }
          }
          else{
            _that.alertCtrlInstance.showAlert("Uh Oh.", "Plan is not eligible.", "OK");
          }
          _that.loader.dismiss();
        }
        else{
          _that.loader.dismiss();
          _that.alertCtrlInstance.showAlert("Uh Oh.", "Plan is not eligible.", "OK");
        }
      }).catch(function (e) {
        console.log("component  failure" + e);
        console.log("Error" + e);
        _that.loader.dismiss();
        _that.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
      });
    });
  }

  queryVoucherServiceCall() {
    var msisdn = this.gatewayService.GetMobileNumber();
    //msisdn = "0196017535";
    var params = { "msisdn": msisdn };
    console.log(params);
    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.QueryVoucher, "POST", params).then((res) => {
        console.log("query voucher response : " + JSON.stringify(res));
        resolve(res);

        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          console.log("******************************************** component failure query voucher response" + e);
          //alert("error in validating MobileConnectUser");   
          reject(e);
        });

    });

  }
}
