import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, ToastController, App } from 'ionic-angular';
import { GatewayService } from '../../../../global/utilService';
import { ConstantData } from '../../../../global/constantService';
import { ReloadService } from '../../../../global/reloadServices';
import { AlertService } from '../../../../global/alert-service';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { PrivRewardsPage } from '../../first-style/priv-rewards/priv-rewards';
import { isArray } from 'ionic-angular/util/util';
/**
 * Generated class for the ProductDetailsDesignGuidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-product-details',
  templateUrl: 'product-details.html',
})
export class ProductDetailsPage {
  showGiftContainer: boolean = false;
  offer_id: any;
  offer: any;
  type: any;
  rewards: any;
  loader: any;
  total_redeems:any;
  constructor(public navCtrl: NavController, public reloadService: ReloadService, 
    public appCtrl: App, public alertCtrl: AlertController, private alertCtrlInstance: AlertService, 
    public navParams: NavParams, public toastCtrl: ToastController, public gatewayService: GatewayService, private renderer: Renderer2) {
    var offer_id = this.navParams.get('offer_id');
    this.type = this.navParams.get('type');
    console.log('product details params offer_id' + offer_id);

    this.rewards = this.gatewayService.GetPrivilegeData();
    console.log(this.rewards);

    //if redeemed offer
    if (this.type == 'offer') {
      //var total_offers = this.rewards.QueryVouchersResponse.OFFERS.OFFER;
      var total_offers = [];
    var arr1 = isArray(this.rewards.QueryVouchersResponse.OFFERS.OFFER);
    if(!arr1){
      total_offers.push(this.rewards.QueryVouchersResponse.OFFERS.OFFER);
    }
    else{
      total_offers = this.rewards.QueryVouchersResponse.OFFERS.OFFER;
    }
      console.log(total_offers);
      this.offer = total_offers.filter(function (obj) {

        return obj.OfferId == offer_id;
      });
      console.log(this.offer);
      
    }
    else {
      //var total_redeems = this.rewards.QueryVouchersResponse.REDEEMES.RedeemedPrivilegesList.Redeem;
     this.total_redeems = [];
    var arr = isArray(this.rewards.QueryVouchersResponse.REDEEMES.RedeemedPrivilegesList.Redeem);
    if(!arr){
      this.total_redeems.push(this.rewards.QueryVouchersResponse.REDEEMES.RedeemedPrivilegesList.Redeem);
    }
    else{
      this.total_redeems = this.rewards.QueryVouchersResponse.REDEEMES.RedeemedPrivilegesList.Redeem;
    }
      this.offer = this.total_redeems.filter(function (obj) {
        console.log('filter offer id' + offer_id);
        console.log('filter offer id2' + obj.OfferId);
        return obj.OfferId == offer_id;
      });
    }
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Privilege product details", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductDetailsPage');
  }



  copyVoucherCode() {
    this.toastCtrl.create({ message: 'Copied', duration: 3000 }).present();
  }

  redeemVoucher() {
    var _that = this;
    _that.loader = _that.gatewayService.LoaderService();
    _that.loader.present().then(() => {

      //redeem voucher service call
      _that.redeemVoucherServiceCall().then(function (res) {
        console.log(res);


        if (res["isSuccessful"]) {
          //res["REDEEMPRIVILEGERESPONSE"].ResponseMessage = 'Success';
          if (res["REDEEMPRIVILEGERESPONSE"].ResponseMessage == 'Success') {
            console.log('If 1');
            //store privilege data
            //query voucher service call
            _that.queryVoucherServiceCall().then(function (res1) {
              console.log(res1);
              if (res1["isSuccessful"]) {
                if (res1["QueryVouchersResponse"].ResponseMessage == 'Success') {
                  //store privilege data
                  _that.gatewayService.setFlagForLifestylePageReload(true);
                  _that.gatewayService.SetPrivilegeData(res1);
                }
              }
            }).catch(function (e) {
              console.log("component  failure" + e);
              console.log("Error" + e);
              _that.loader.dismiss();
            });

            
            let alert = _that.alertCtrl.create({
              title: res["REDEEMPRIVILEGERESPONSE"].ResponseMessage,
              subTitle: "Voucher has been redemmed successfully.",
              buttons: [
                {
                  text: "Ok",
                  role: "Ok",
                  cssClass:"error-button-float-right submit-button",
                  handler: () => {
                    console.log('Call to Action Clicked');
                    _that.gatewayService.notifyObservers("updateLifestyle");
                    _that.navCtrl.pop();
                  }
                }],
              cssClass: 'success-message error-message',
              enableBackdropDismiss: false

            });
            alert.present();

          }
          else {
            //alert('else redeeem voucher');
            //_that.gatewayService.notifyObservers("updateLifestyle");
            _that.alertCtrlInstance.showAlert("Uh Oh.", res["REDEEMPRIVILEGERESPONSE"].ErrorDesc, "OK");
          }
          _that.loader.dismiss();
        }
        else {
          _that.loader.dismiss();
          _that.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
        }
      }).catch(function (e) {
        console.log("component  failure" + e);
        console.log("Error" + e);
        _that.loader.dismiss();
        _that.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
      });
    });
  }

  redeemVoucherServiceCall() {
    var msisdn = this.gatewayService.GetMobileNumber();
    //var msisdn = "0193225295";
    var offer_id = this.navParams.get('offer_id');
    var part_num = this.rewards.QueryVouchersResponse.LIFESTYLE_PARTNUM;
    console.log(part_num);
    //msisdn = "0196017535";
    
    var params = { "msisdn": msisdn, "offer_id": offer_id, "part_num": part_num };
    console.log(params);
    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.RedeemPrivilege, "POST", params).then((res) => {
        console.log("redeem voucher response : " + JSON.stringify(res));
        resolve(res);

        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          console.log("******************************************** component failure redeem voucher response" + e);
          //alert("error in validating MobileConnectUser");   
          reject(e);
        });

    });

  }

  queryVoucherServiceCall() {
    var msisdn = this.gatewayService.GetMobileNumber();
    //msisdn = "0196017535";
    var params = { "msisdn": msisdn };
    console.log(params);
    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.QueryVoucher, "POST", params).then((res) => {
        console.log("query voucher response : " + JSON.stringify(res));
        resolve(res);

        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          console.log("******************************************** component failure query voucher response" + e);
          //alert("error in validating MobileConnectUser");   
          reject(e);
        });

    });

  }
  DataAlignment(gettingDate){
    var day = gettingDate.slice(0, 8);
    var year = day.match(/.{1,4}/g);
    var date = year[1].match(/.{1,2}/g);
    var finalDate = date[1]+'/'+date[0]+'/'+year[0];
    return finalDate;
    }

}
