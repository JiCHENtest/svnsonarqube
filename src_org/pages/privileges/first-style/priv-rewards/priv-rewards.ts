import { Component, NgZone, Renderer2 } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';
import { PrivOfferingPage } from "./priv-offering/priv-offering";
import { PrivRedeemedPage } from "./priv-redeemed/priv-redeemed";
import { ProductDetailsPage } from "../product-details/product-details";
//import { ProductDetailsDesignGuidePage } from "../../product-details-design-guide/product-details-design-guide";

import { GatewayService } from '../../../../global/utilService';
import { ConstantData } from '../../../../global/constantService';
import { ReloadService } from '../../../../global/reloadServices';
import { AlertService } from '../../../../global/alert-service';


/**
 * Generated class for the RewardsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-priv-rewards',
  templateUrl: 'priv-rewards.html',
})
export class PrivRewardsPage {
  tab1Page: any = PrivOfferingPage;
  tab2Page: any = PrivRedeemedPage;
  loader:any;
  constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams,
    public gatewayService: GatewayService,
    public reloadService: ReloadService,
    private alertCtrlInstance: AlertService,
    private renderer: Renderer2,
    public zone: NgZone) {
      this.gatewayService.registerObserver(this);
    events.subscribe('goToOfferDetail',(params)=>{
      this.navCtrl.push(ProductDetailsPage,{"offer_id":params.offer_id,"type":params.type});

  });
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Priv rewards", this.renderer);
  }

  callingMethod() {

  }
  updateDashboard(){
    // this.refreshData();
   }
 
   update(){
 
   }
   updateLifestyle(){
     console.log('observer update lifestyle');
    this.goToFirstStylePage();
   }
  ionViewDidLoad() {
    console.log('ionViewDidLoad first style RewardsPage');
  }
  public openDetails() {
    //this.navCtrl.push(ProductDetailsDesignGuidePage);
  }
  ngOnDestroy(){
    this.events.unsubscribe('goToOfferDetail');
    }
    ionViewWillEnter(){
      console.log('ionViewWillEnter rewards page');
      if(this.gatewayService.getFlagForLifestylePageReload())
      {
        //this.goToFirstStylePage();
        this.gatewayService.setFlagForLifestylePageReload(false);
      }
      
    }

    goToFirstStylePage (){
      //this.navCtrl.push(FirstStyleSelectTypePage);
  
      var _that = this;
      _that.loader = _that.gatewayService.LoaderService();
      _that.loader.present().then(() => {
  
        //query voucher service call
        _that.queryVoucherServiceCall().then(function (res) {
          console.log(res);
          
  
          if(res["isSuccessful"]){
            if(res["QueryVouchersResponse"].ResponseMessage =='Success'){
              console.log('If 1');
              console.log(res["QueryVouchersResponse"]);
              console.log(res["QueryVouchersResponse"].OPTIN);
              //res["QueryVouchersResponse"].OPTIN = "NO";
              if(res["QueryVouchersResponse"].OPTIN == "YES" && (res["QueryVouchersResponse"].PrivilegeType == 'MONTHLY' || res["QueryVouchersResponse"].PrivilegeType == 'ONETIME'))
              {
                //store privilege data
                _that.zone.run(() => {
                _that.gatewayService.SetPrivilegeData(res);
                });
                
  
              }else{
                console.log('else');
                //go to opt-in page i.e. here only
                
              }
            }
            else{
              
            }
            _that.loader.dismiss();
          }
          else{
            _that.loader.dismiss();
            
          }
        }).catch(function (e) {
          console.log("component  failure" + e);
          console.log("Error" + e);
          _that.loader.dismiss();
          _that.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
        });
      });
    }
  
    queryVoucherServiceCall() {
      var msisdn = this.gatewayService.GetMobileNumber();
      //msisdn = "0196017535";
      var params = { "msisdn": msisdn };
      console.log(params);
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.QueryVoucher, "POST", params).then((res) => {
          console.log("query voucher response : " + JSON.stringify(res));
          resolve(res);
  
          //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
        })
          .catch(function (e) {
            console.log("******************************************** component failure query voucher response" + e);
            //alert("error in validating MobileConnectUser");   
            reject(e);
          });
  
      });
  
    }
}
