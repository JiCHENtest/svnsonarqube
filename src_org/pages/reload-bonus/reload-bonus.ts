import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductDetailsDesignGuidePage } from '../product-details-design-guide/product-details-design-guide';
import { ReloadService } from '../../global/reloadServices';
import { GatewayService } from '../../global/utilService';

import { AlertService } from '../../global/alert-service';

/**
 * Generated class for the ReloadBonusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reload-bonus',
  templateUrl: 'reload-bonus.html',
})
export class ReloadBonusPage {
  Ecem: any;
  RegisterFlag: any
  EcemCard: boolean;
  dec: any;
  loader: any;
  selectedNumber: any;
  expiryDate: any;
  PromoTitle: any;
  //Expiry dd/mm/yy hh:mm:ss
  numberofCards: any;
  pageName: any;
  number: any;
  expiry: string = "Expiry : ";
  onlyTime: any;
  EmptyStatus: boolean = false;
  NotEmpty: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public reloadService: ReloadService, public alertCtrlInstance: AlertService, 
    public gatewayService: GatewayService, private renderer: Renderer2) {
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Reload bonus privilege", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReloadBonusPage');
    this.dec = "Select one of the three available reload";
    this.Ecem = "ECEMB";

    //this.expiryDate = "dd/mm/yy hh:mm:ss";

    this.RegisterFlag = false;
    this.pageName = "privileges";

    this.checkEligblity();
  }
  checkEligblity() {

    this.loader = this.gatewayService.LoaderService();
    this.loader.present();

    this.selectedNumber = this.gatewayService.GetMobileNumber();



    var params = {
      "MobileNumber": this.selectedNumber
    }



    //
    this.reloadService.EcemEligible(params).then((res) => {

      console.log("*****EcemPassDetails*****" + JSON.stringify(res));

      if (res["FreebiesEligibilityResponse"].Eligible == "Y") {
        this.NotEmpty = true

          ;
        this.loader.dismiss();
        this.checkRedeemBonus();
      } else {
        this.EmptyStatus = true;
        this.loader.dismiss();
        //  this.checkRedeemBonus();
        // this.EcemCard = true;
      }
    }).catch(err => {
      this.loader.dismiss();
      console.log("*****reload*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }
  OnlyTimeReturn(Timeis) {
    var day = Timeis.slice(8, 14);
    var year = day.match(/.{1,2}/g);

    var finalDate = year[0] + ':' + year[1] + ':' + year[2];
    return finalDate;
  }
  checkRedeemBonus() {
    this.loader = this.gatewayService.LoaderService();
    this.loader.present();

    this.selectedNumber = this.gatewayService.GetMobileNumber();

    var saveVariable = this.selectedNumber.charAt(0);
    if (saveVariable != "6") {
      this.selectedNumber = "6" + this.selectedNumber;
    }
    //var testData = "60136430242";
    var params = {
      "MobileNumber": this.selectedNumber
    }

    this.reloadService.EcemPassDetails(params).then((res) => {

      console.log("*****EcemPassDetails*****" + JSON.stringify(res));

      if (res["FreebiesInquiryResponse"].NumberOfFreebies == "0") {
        this.loader.dismiss();
        this.EcemCard = false;
      } else {
        this.EcemCard = true;
        this.PromoTitle = "East Coast East Malaysia Bonus";
        var numberOfReloadCard = res["FreebiesInquiryResponse"].NumberOfFreebies;
        this.number = res["FreebiesInquiryResponse"].NumberOfFreebies;
        var ExpiryDateTime = res["FreebiesInquiryResponse"].PLAN_EXPIRY.EXPIRY_DATE;
        ExpiryDateTime = this.reloadService.DataAlignment(ExpiryDateTime);
        this.expiryDate = ExpiryDateTime;
        var Timeis = res["FreebiesInquiryResponse"].PLAN_EXPIRY.EXPIRY_DATE;
        this.onlyTime = this.OnlyTimeReturn(Timeis);
        this.numberofCards = [];
        for (var i = 0; i < numberOfReloadCard; i++) {
          this.numberofCards.push(i);
        }
        this.loader.dismiss();
      }

    }).catch(err => {
      this.loader.dismiss();
      console.log("*****reload*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }
  goToProductDetailsPage() {
    this.navCtrl.push(ProductDetailsDesignGuidePage, {
      param1: this.RegisterFlag, param2: this.Ecem, param3: this.pageName, param4: this.number
    });
  }

}
