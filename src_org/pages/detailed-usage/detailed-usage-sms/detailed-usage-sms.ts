import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DetailedUsageService } from '../../detailed-usage/detailedUsageService';
import { DashboardService } from '../../home/homeService';
import { GatewayService } from '../../../global/utilService';
import { ConstantData } from '../../../global/constantService';
import { GlobalVars } from "../../../providers/globalVars";
import _ from 'underscore';

/**
 * Generated class for the DetailedUsageSmsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detailed-usage-sms',
  templateUrl: 'detailed-usage-sms.html',
})
export class DetailedUsageSmsPage {
  public detailedUsageSMS:any={};
  cardArray:any = [];
  cardMapping:any;
  userInfo:any;
  isPrepaid:Boolean;
  usageSince:any;
  IsKADCeria:Boolean;
  internetPlans:any;
  Usage:any;
  isCapZon:boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public detailedUsageService: DetailedUsageService, public gatewayService:GatewayService, 
    public dashboardService: DashboardService, public globalVar: GlobalVars, private renderer: Renderer2) {
    console.log('ionViewDidLoad DetailedUsageSMSPage');
    let planName = this.gatewayService.getPlanName();
    console.log("**************** plan name****************"+planName);
    this.cardMapping = ConstantData.dialMapping.postpaid[planName];
    console.log("***************** Card mapping ****************"+JSON.stringify(this.cardMapping));
    //this.GBSharePlan = this.dashboardService.GetUnits();
    this.userInfo = this.dashboardService.GetUserInfo();
    this.isPrepaid = this.gatewayService.isPrePaid();
    this.usageSince = this.gatewayService.getUsageSince();
    this.IsKADCeria = this.dashboardService.IsKADCeria();
    this.internetPlans = this.dashboardService.GetAllDataPostpaidPrepaid();
    
    this.Usage = this.dashboardService.GetUsageSince();
    this.isCapZon = this.globalVar.getCapZoneStatus();
    this.generateSMSCards();
    // this.cardArray = [
    //   {
    //     "title":"Internet Pack 1",
    //     "subtitle":"3 days plan",
    //     "cardType":"Card CTA #1"
    //   },
    //   {
    //     "title":"Internet Pack 2",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Toggle/CTA #1"
    //   },
    //   {
    //     "title":"Internet Pack 3",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Usage with Limit #1"
    //   },
    //   {
    //     "title":"Internet Pack 4",
    //     "subtitle":"3 days plan",
    //     "cardType":"Card PPU #1"
    //   },
    //   {
    //     "title":"Internet Pack 5",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Description+ Toggle #1"
    //   },
    //   {
    //     "title":"Internet Pack 6",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Description #1"
    //   },
    //   {
    //     "title":"Internet Pack 7",
    //     "subtitle":"3 days plan",
    //     "cardType":"Card Usage with Limit (Kad Ceria) #1"
    //   },
    //   {
    //     "title":"Internet Pack 8",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Bundle (with Toggle/CTA) #1"
    //   },
    //   {
    //     "title":"Internet Pack 9",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Info (Roaming) #1"
    //   },
    //   {
    //     "title":"Internet Pack 10",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Info (Plan Activation) #1"
    //   },
    //   {
    //     "title":"Internet Pack 11",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Group Sharing Data #1"
    //   }
    // ]   
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("SMS detailed usage", this.renderer);
  }

  formatDate(val){
    if(val==""|| val==null || val==undefined)
    return;
    var dt = val.substring(6, 8)+'/'+val.substring(4, 6)+'/'+val.substring(2, 4);
    return dt;
  }

  generateSMSCards(){
    if(this.isCapZon){
      var data = {
        "title":"You are now in Capzone",
        "subtitle":"You have 10mins call time and 10 SMS to use within Celcom network",
        "date":"Valid for 35 days",
        "cardType":"Card Description #1"
      }
      this.cardArray.push(data);
    }
    if(this.IsKADCeria){
      var TalkPPUData = {
        "title":"Kad Ceria",
        "subtitle":"Enjoy 10% extra credit with every RM10 and above reload",
        "class":"kad-icon icon",
        "cardType":"Card Usage with Limit (Kad Ceria) #1"
      }
      this.cardArray.push(TalkPPUData);
      var obj = this.detailedUsageService.GetKadCeria();
      try{
        if(obj.BalanceDesc){
          var dataObj = {
            "title":"Kad Ceria Bonus",
            "key":"Balance Usage",
            "value":(obj.Balance/10000),
            "class":"kad-icon icon",
            "cardType":'Card PPU Kad Ceria #1'
          }
          this.cardArray.push(dataObj);
        }
      }catch(e){
        console.log("error"+e);
      }
    }
    
    // if(this.internetPlans.subscriberQuota.KadCeriaUsageDetail){
    //   this.generateKadCeriaUsageDetail();
    // }   
    if(this.isPrepaid){
      this.generatePPUSMSPrepaidCard();
    }else{      
      this.generateSMSPostpaidCards();
      this.generatePPUSMSPostpaidCard();
    }    
  }
  
  generateKadCeriaUsageDetail(){
    if(this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage){
      if(this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage.length>1){        
        for(let i in this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage){
          var pack = this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage[i];          
          var KadCeriaData = {
            "title":'Kad Ceria',
            "subtitle":'Enjoy 10% extra credit with every RM10 and above reload',
            "date":'Valid till '+this.formatDate(pack.KadCeriaSMSExpiryDate),
            "class":"kad-icon icon",
            "cardType":"Card Description #1"
          }
          this.cardArray.push(KadCeriaData);
        }
      }else{
        var pack = this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage;
        var KadCeriaData = {
          "title":'Kad Ceria',
          "subtitle":'Enjoy 10% extra credit with every RM10 and above reload',
          "date":'Valid till '+this.formatDate(pack.KadCeriaSMSExpiryDate),
          "class":"kad-icon icon",
          "cardType":"Card Description #1"
        }
        this.cardArray.push(KadCeriaData);
      }
    }
  }

  generatePPUSMSPrepaidCard(){
    try{
    this.detailedUsageSMS = this.detailedUsageService.GetDetailedUsagePrepaid();
    var SMSPPU = this.detailedUsageSMS.SMSCreditUsageDetails;
    let total: any = parseFloat(SMSPPU.TotalOnNetSMSFund)+parseFloat(SMSPPU.TotalOffNetSMSFund)+parseFloat(SMSPPU.TotalOffNetSMSFund);
      var PPUData = {
        "title":"Pay Per Use SMS",
        "subtitle":"",
        "data":[
          {
            "key":"Usage Since",
            "value":this.usageSince
          },
          {
            "key":"Same Network",
            "value":parseFloat(SMSPPU.TotalOnNetSMSFund).toFixed(2)
          },
          {
            "key":"Other Network",
            "value":parseFloat(SMSPPU.TotalOffNetSMSFund).toFixed(2)
          }],
        "TotalDataUsage":parseFloat(SMSPPU["TotalPayPerSMSFund"]).toFixed(2),
        "class":"icon_pay-per-use icon",
        "cardType":"Card PPU #1"
      }
      this.cardArray.push(PPUData); 
    }catch(e){
      console.log(e);
    }     
    //   if(SMSBonus){
    //   var PPUBonusData = {
    //     "title":"Talk",
    //     "subtitle":"",
    //     "data":[
    //       {
    //         "key":"Usage Since",
    //         "value":""
    //       },
    //       {
    //         "key":"Same Network",
    //         "value":SMSBonus.BonusTalkOnNetBal
    //       },
    //       {
    //         "key":"Other Network",
    //         "value":SMSBonus.BonusTalkOffNetBal
    //       }],
    //     "TotalDataUsage":SMSBonus["BonusTalkUsage"],
    //     "cardType":"Card PPU #1"
    //   }
    //   this.cardArray.push(PPUBonusData);
    // }
  }

  generatePPUSMSPostpaidCard(){
    try{
    this.detailedUsageSMS = this.detailedUsageService.GetDetailedusageSMSPostpaid();
    var formattedSMSObj = _.object(_.pluck(this.detailedUsageSMS, 'ParameterName'), _.pluck(this.detailedUsageSMS, 'ParameterValue'));
    var PPUData = {
      "title":"Pay Per Use SMS",
      "subtitle":"",
      "data":[
        {
          "key":"Usage Since",
          "value":this.Usage
        },
        {      
        "key":"Total SMS",
        "value":formattedSMSObj["SMSCount"]
        }],
      "TotalDataUsage":parseFloat(formattedSMSObj["TotalCharge"]).toFixed(2),
      "class":"icon_pay-per-use icon",
      "cardType":"Card PPU #1"
    }
    this.cardArray.push(PPUData);
  }catch(e){
    console.log(e);
  }
  }

  generateSMSPostpaidCards(){
    var smsPostpaid = this.dashboardService.GetSMSPostpaid();    
    if(smsPostpaid){
    if(smsPostpaid.TotalSMS=="Unlimited" || smsPostpaid.TotalSMS=="NA" || smsPostpaid.TotalSMS=="NaN"){
      var SMSPostpaidUnlimitedData = {
        "title":"Unlimited SMS",
        "subtitle":"You are entitled unlimited SMS to all Network",
        "date":"",
        "class":"sms-icon icon",
        "cardType":"Card Description #1"
      }
      this.cardArray.push(SMSPostpaidUnlimitedData);    
        }else{
          if(smsPostpaid.ExpireTime){
            var date = this.formatDate(smsPostpaid.ExpireTime)//this.getBillingDate(this.billDate);
          }else{
            var date = '';
          }
          var title;
            if(smsPostpaid.BalanceDesc=="" || smsPostpaid.BalanceDesc==null || smsPostpaid.BalanceDesc==undefined){
              title = "Postpaid SMS"
            }else{
              title = smsPostpaid.BalanceDesc;
            }
          var SMSPostpaidData = {
            "title":title,
            "subtitle":"",
            "dataUsed":smsPostpaid.UsageSMS,
            "dataTotal":smsPostpaid.TotalSMS,
            "dataBalance":smsPostpaid.Balance,
            "date":date,
            "autoRenew":0,
            "unit":"SMS",
            "class":"sms-icon icon",
            "cardType":"Card Usage with Limit #1"
          }
          this.cardArray.push(SMSPostpaidData);          
      }
    }
    }   

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailedUsageSmsPage');
  }
  cardTitleChange(titleString){
    try{
      return titleString.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }catch(e){
      return "";
    }
  }

}
