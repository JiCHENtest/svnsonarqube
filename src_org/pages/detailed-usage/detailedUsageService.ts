
/**
 * @author Capgemini 
 */

/**
 * @Module Name - detailedUsageService
 * @Description - Singleton class for detailed usage page, This is common gateway for all fetch/post calls 
 */




export class DetailedUsageService {

  detailedusageTalk;
  detailedusageSMS;
  detailedusageRoaming;
  cardsDetails:any= [];
  isGBAdmin:boolean = false;
  KadCeria:any = [];
  constructor(){
   
  };

GetDetailedusageSMSPostpaid(){
  try{
  return this.detailedusageSMS.Envelope.Body.UsageTalkSMSRoamingResponse.ResponseBody.ParameterList.Parameter;
  }catch(e){
    return '';
  }
}

SetDetailedUsageSMSPostpaid(value){
  this.detailedusageSMS = value;
}

GetDetailedusageTalkPostpaid(){
  try{
  console.log("********************************this.detailedusageTalk.Envelope.Body.UsageTalkSMSRoamingResponse.ResponseBody.ParameterList.Parameter"+this.detailedusageTalk.Envelope.Body.UsageTalkSMSRoamingResponse.ResponseBody.ParameterList.Parameter);
  return this.detailedusageTalk.Envelope.Body.UsageTalkSMSRoamingResponse.ResponseBody.ParameterList.Parameter;
  }catch(e){
    return '';
  }
}

SetDetailedUsageTalkPostpaid(value){
  this.detailedusageTalk = value;
}
GetDetailedusageRoamingPostpaid(){
  try{
  return this.detailedusageRoaming.Envelope.Body.UsageTalkSMSRoamingResponse.ResponseBody.ParameterList.Parameter;
  }catch(e){
    return '';
  }
}

SetDetailedUsageRoamingPostpaid(value){
  this.detailedusageRoaming = value;
}

GetDetailedUsagePrepaid(){
  try{
  return this.detailedusageRoaming.Envelope.Body.SubscriberUsageQueryResponse.ResponseBody;
  }catch(e){
    return '';
  }
}

SetDetailedUsagePrepaid(value){
  this.detailedusageRoaming = value;
}

SetcardsDetails(data){
  this.cardsDetails = data;
}

GetcardsDetails(){
  return this.cardsDetails;
}

SetKadCeria(data){
  this.KadCeria = data;
}

GetKadCeria(){
  return this.KadCeria;
}

SetISGBAdmin(str){
  this.isGBAdmin = str;
}

GetIsGBAdmin(){
  return this.isGBAdmin;
}

Initialize(){
  this.isGBAdmin = false;
}

}









         

