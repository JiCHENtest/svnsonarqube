import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {PlanDetailsTermsAndConditionsPage} from "./plan-details-terms-and-conditions/plan-details-terms-and-conditions";
import { ConstantData } from '../../global/constantService';
import { GatewayService } from '../../global/utilService';
import { AlertService } from '../../global/alert-service';
import { DashboardService } from '../../pages/home/homeService';
import { UpgradeLatestPlanPage } from "../services/upgrade-latest-plan/upgrade-latest-plan";
import _ from "underscore";

/**
 * Generated class for the PlanDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-plan-details',
  templateUrl: 'plan-details.html',
})
export class PlanDetailsPage {
  planDetails:any;
  plan_type:any;
  plan_name:any;
  outer_keys:any;
  plan_details:any;
  hideTermsAndConditions: boolean= false;
  check_plan_name:any;
  isPrepaid:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public gatewayService:GatewayService, public dashboardService:DashboardService, 
    public alertCtrlInstance:AlertService, private renderer: Renderer2) {
    
    this.isPrepaid = this.gatewayService.isPrePaid();
  
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Plan details", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlanDetailsPage');
    //call plan details adapter
    //web service call for Romaing details
    var _that = this;
    var loader = this.gatewayService.LoaderService();
    loader.present();
    
    
    this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetPlanDetails,"POST",{}).then((res)=> {
      
      console.log(res);
      var userInfo = _that.dashboardService.GetUserInfo();
      
      _that.plan_type = userInfo.ListOfServices.Services.Pre_Pos_Indicator.toLowerCase();
      _that.check_plan_name = userInfo.ListOfServices.Services.PLAN;
      if(_that.plan_type=='cnvrgtpostpaid')
      _that.plan_type = 'postpaid';
      
      //_that.plan_name = userInfo.ListOfServices.Services.PLAN;
      
      _that.planDetails = res[_that.plan_type][userInfo.ListOfServices.Services.ProdPromName];
      console.log(_that.planDetails);
      _that.plan_name = res[_that.plan_type][userInfo.ListOfServices.Services.ProdPromName]["Display Name"];
      
      _that.outer_keys = _.keys(_that.planDetails);
      
      //var plans = _that.outer_keys;
      if(_that.planDetails != undefined){
        var result = Object.keys(_that.planDetails).map(function(key) {
          return [key, _that.planDetails[key]];
        });
      }
      
      console.log(result);

      for(var j in result){
        //console.log(result[j]);
        if(_.isObject(result[j][1])){
          var result1 = Object.keys(result[j][1]).map(function(key1) {
            return [key1, result[j][1][key1]];
            
          });
          result[j][1] = result1;
          //console.log(result[j][1]);
          for(var k in result1)
          {
            if(_.isObject(result1[k][1])){
              Object.keys(result1[k][1]).map(function(key2) {
                return [key2, result1[k][1][key2]];
                
              });
              //result[j][1] = result1;
              //result[j][1][1] = result2;
            }

          }
          
        }
        else{

        }
      }

      
      
      console.log("plan_details are+++++++++++++++++"+JSON.stringify(result));
      _that.plan_details = result;
      
      //console.log(_.keys(_that.planDetails));
      console.log(_that.planDetails);
      loader.dismiss();
    })
    .catch(e => {
      loader.dismiss();
        console.log("component  failure"+e);
        if(e=="No internet connection")
        return;
        this.alertCtrlInstance.showAlert("Notification", "Plan not found.", "OK");
        
    });
  }

  goBack() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop({animate: false});
    }

  }
  upgrade(){
    this.navCtrl.push(UpgradeLatestPlanPage);
  }

  openTermsAndCondition() {
    this.navCtrl.push(PlanDetailsTermsAndConditionsPage);
  }
  isArray(obj : any ) {
    console.log(obj);
    return Array.isArray(obj)
 }
 isObject(obj : any) {
  var check = typeof(obj);
  console.log(check);
  if(check=="object"){
    return true;
  }
  else{
    return false;
  }
 }
}
