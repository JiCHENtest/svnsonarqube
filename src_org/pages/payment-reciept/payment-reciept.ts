import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { PayBillPage } from '../pay-bill/pay-bill';
import { PayBillService } from '../pay-bill/payBillService';
import { ReloadService } from '../../global/reloadServices';
import { GatewayService } from '../../global/utilService';
import { DashboardService } from '../home/homeService';
import { Events } from 'ionic-angular';
import { GlobalVars } from "../../providers/globalVars";

/**
 * Generated class for the PaymentRecieptPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-payment-reciept',
  templateUrl: 'payment-reciept.html',
})
export class PaymentRecieptPage {
  // XPAXTheme: boolean = this.globalVar.getXPAXTheme();
  selectedTab: any
  is_OleOle: any;
  transactionDate: any;
  transactionId: any;
  mobilePaybill: any;
  mobileReload: any;
  is_paybill: any;
  constructor(public viewCtrl: ViewController, public navCtrl: NavController, 
    public navParams: NavParams, public events: Events, public gatewayService: GatewayService, 
    public paybillService: PayBillService, public reloadService: ReloadService, 
    public dashboardService: DashboardService, public globalVar: GlobalVars, private renderer: Renderer2) {
    this.selectedTab = this.paybillService.GetTabInfo();
    this.is_paybill = this.gatewayService.isPrePaid();
    console.log(this.globalVar.getXPAXTheme);
    // this.XPAXTheme = this.gatewayService.isPrePaid();

    // var userInfo = this.dashboardService.GetUserInfo();
    // this.gatewayService.SetXPAX(userInfo.ListOfServices.Services.ServiceType);


  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Payment reciept", this.renderer);
  }

  ionViewDidLoad() {
    this.is_OleOle = false;
    console.log('ionViewDidLoad PaymentRecieptPage');
    if (this.gatewayService.isPrePaid()) {
      this.mobileReload = this.reloadService.GetSelectMobileNumber();
      this.transactionDate = this.reloadService.GetTransactionDate();
      this.transactionId = this.reloadService.GetTransactionId();
      console.log('Transaction date is::' + this.transactionDate);
      console.log('Transaction id is::' + this.transactionId);
      if (this.transactionId == null) {
        this.transactionId = "";
      }
    } else {
      this.mobilePaybill = this.paybillService.GetSelectMobileNumber();
      this.transactionDate = this.paybillService.GetTransactionDate();
      this.transactionId = this.paybillService.GetTransactionId();
      console.log('Transaction date is::' + this.transactionDate);
      console.log('Transaction id is::' + this.transactionId);
      if (this.transactionId == null) {
        this.transactionId = "";
      }
    }
  }


  goToConfirmation() {

    //this.navCtrl.push(PayBillPage, this.selectedTab);
    this.viewCtrl.dismiss().then(() => {
      this.is_paybill = "";
      // this.events.publish('AccountNumAdded', this.accountNumAdded);
      // selectedNumber = "";
      console.log("hiiiiii");
    });

  }
}
