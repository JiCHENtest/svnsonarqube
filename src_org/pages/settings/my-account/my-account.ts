import {Component, Renderer2} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {ResetPasswordPage} from "./rest-password/reset-password";
import { Keyboard } from '@ionic-native/keyboard';
import { GatewayService } from '../../../global/utilService';




/**
 * Generated class for the PageSettingsMyAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'my-account',
  templateUrl: 'my-account.html',
})
export class MyAccountPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private keyboard: Keyboard,
    private renderer: Renderer2, public gatewayService: GatewayService) {
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("My account", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyAccountPage');
  }

  editProfileClick() {
    console.log('editProfileClicked');
  }

  navToResetPassword() {
    this.navCtrl.push(ResetPasswordPage, {});
  }
  handleKeyDone(e){
    if(e.which === 13){
      this.keyboard.close();
    }
  }
}
