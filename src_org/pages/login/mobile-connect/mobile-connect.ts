import { Component, ApplicationRef, NgZone, Renderer2  } from '@angular/core';
import { NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { ConnectTacPage } from '../connect-tac/connect-tac';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController } from 'ionic-angular';
import { LocalStorage } from '../../../global/localStorage';
import { MobileConnectAccountPage } from '../mobile-connect/account-setup/account-setup';
import { JsonstoreService } from '../../../global/jsonstore';
import { GatewayService } from '../../../global/utilService';
import { ReloadService } from '../../../global/reloadServices';
import { ConstantData } from '../../../global/constantService';
import { MobileConnectService } from './mobileConnectService';
import { AlertService } from '../../../global/alert-service';
import { Keyboard } from '@ionic-native/keyboard';
/**
 * Generated class for the MobileConnectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var SMS: any;
@Component({
  selector: 'page-mobile-connect',
  templateUrl: 'mobile-connect.html',
})
export class MobileConnectPage {

  mobileNumber: any;

  isFirstTimeUser: any;
  SendTacForm: FormGroup;
  submitAttempt: boolean = false;
  state: any;
  validNumber: boolean = false;
  loader: any;
  serviceIndicator: any;
  constructor(private zone: NgZone,public navCtrl: NavController, public alertCtrl: AlertController,
     public navParams: NavParams, public platform: Platform, public androidPermissions: AndroidPermissions,
     public formBuilder: FormBuilder, public localStorage: LocalStorage, public jsonstoreService: JsonstoreService,
     public gatewayService: GatewayService, private alertCtrlInstance: AlertService,
     public mConnectService: MobileConnectService, public reloadService: ReloadService,
     private toastCtrl: ToastController,private keyboard: Keyboard, private ref: ApplicationRef, private renderer: Renderer2) {
    this.isFirstTimeUser = this.localStorage.get("IsFirstTimeUser", "true");


    this.SendTacForm = formBuilder.group({
      mobileNumber: ['', Validators.compose([Validators.maxLength(11), Validators.minLength(10), Validators.pattern('[0-9]*'), Validators.required])],

    });
  }

  ionViewWillEnter() {

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
      success => console.log('Permission granted'),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
    );

    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
  }
  
  keyUpChecker(eve, varname){ 
    let _thisRef = this;
    let returnVal = this.gatewayService.keyUpCheckerNumberOnly(eve,this.keyboard);      
    this.zone.run(()=>{
      _thisRef[varname] = returnVal;
    })
  }

  blurChecker(ev){
    if(ev.currentTarget.children[0])
    ev.currentTarget.children[0].value = this.gatewayService.keyUpCheckerNumberOnly(ev,this.keyboard);
  }    

  ReadListSMS() {

    this.platform.ready().then((readySource) => {

      let filter = {
        box: 'inbox', // 'inbox' (default), 'sent', 'draft'
        indexFrom: 0, // start from index 0
        maxCount: 10, // count of SMS to return each time
      };

      if (SMS) SMS.listSMS(filter, (ListSms) => {

        console.log("Sms", ListSms);
      },

        Error => {
          console.log('error list sms: ' + Error);
        });

    });
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageName("Login", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MobileConnectPage');
  }

  navigatetoTAC() {
    let toast = this.toastCtrl.create({
      message: 'OTP successfully sent',
      duration: 3000,
      cssClass: 'toast',
      position: 'top'
    });
    let Errortoast = this.toastCtrl.create({
      message: 'Unable to send OTP. Please retry!',
      duration: 3000,
      cssClass: 'toast',
      position: 'top'
    });

    this.submitAttempt = true;

    if (ConstantData.isLoginBypassed == false) {


      // alert(this.mobileNumber);
      // alert(this.mobileNumber.substring(0,3)=="011");
      // alert(this.mobileNumber.length==10);
      if ((this.mobileNumber.substring(0, 3) == "011" && this.mobileNumber.length == 10) || (this.mobileNumber.substring(0, 3) != "011" && this.mobileNumber.length == 11)) {

        this.alertCtrlInstance.showAlert("Uh Oh. Fingers Too Fast?", "Please enter a valid Celcom number (10 or 11 digits only)", "OK");

      }

      else {
        var _that = this;
        //check celcom number or not
        //check celcom number or not
        this.validNumber = false;
        var _that = this;
        var selectedNumber = this.mobileNumber;
        _that.loader = _that.gatewayService.LoaderService();
        _that.loader.present().then(() => {
          this.reloadService.checkAddedNewNumber(selectedNumber).then((res) => {
            console.log("inside checkAddedNewNumber " + JSON.stringify(res));
            this.serviceIndicator = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.ListOfServices.Services.ServiceType;


            _that.loader.dismiss();
            if (selectedNumber.substring(0, 3) == "011" && selectedNumber.length == 10) {
              this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid Number", "Please input a valid Celcom phone number. (If starting with 011 then put 11 digits).", "Try again");
            }
            else {
              this.validNumber = true;

              //if BRN number, don't login
              var checkBRN = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.CustomerIDType;

              if (checkBRN == "BRN") {
                _that.alertCtrlInstance.showAlert("Thanks for your interest in Celcom Life.", "At the moment, the app is for use by XPAX and postpaid customers only.", "OK");
              }
              else {
                // JSON store put data mobile number
                var mobile_number = { mobilenum: this.mobileNumber };
                this.jsonstoreService.putData(mobile_number, "mobilenumber"); // data { key : value , <collection_name>}
                this.gatewayService.SetMobileNumber(this.mobileNumber);
                this.reloadService.setingLoginNumber(this.mobileNumber);
                
                var loader = _that.gatewayService.LoaderService();
                loader.present();
                //code for calling register()from adapter
                _that.registerMobileConnectUser().then((res) => {
                  //alert("register function response"+JSON.stringify(res));
                  _that.mConnectService.SetMConnectInfo(res);
                  loader.dismiss();
                  
                  this.state = res["state"];
                  //var reponse=JSON.parse(res);
                  if (res["exists"] == 0) {

                      //_that.navCtrl.setRoot(MobileConnectAccountPage,{data:res["state"]});
                    _that.navCtrl.setRoot(MobileConnectAccountPage);
                  }
                  else {
                    toast.present();
                    _that.navCtrl.setRoot(ConnectTacPage);
                  }
                }).catch(function (e) {
                  console.log("component  failure" + e);
                  loader.dismiss();
                  if(e=="No internet connection")
                  return;
                  Errortoast.present();
                  _that.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
                });
              }

            }


          }).catch(err => {
            _that.loader.dismiss();
            
            console.log("*****mobile eer connect 2*******" + err);
            console.log(err);
            if(err=="No internet connection")
            return;
            
            if(err.indexOf("timed out") >= 0 || err.indexOf("Failed to connect") >= 0){
              this.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
            }
            else{
              this.alertCtrlInstance.showAlert("Notification", "Not a Celcom number.", "OK");
            }
            

            // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
          });
        });



      }

    } else {


      console.log("mobile number substring is ++++++++" + this.mobileNumber.substring(0, 3));
      if ((this.mobileNumber.substring(0, 3) == "011" && this.mobileNumber.length == 10) || (this.mobileNumber.substring(0, 3) != "011" && this.mobileNumber.length == 11)) {


        this.alertCtrlInstance.showAlert("Uh Oh. Fingers Too Fast?", "Please enter a valid Celcom number (10 or 11 digits only)", "OK");
      }

      else {
        // this.localStorage.set("MobileNum",this.mobileNumber);
        //check celcom number or not
        this.validNumber = false;
        var _that = this;
        var selectedNumber = this.mobileNumber;
        _that.loader = _that.gatewayService.LoaderService();
        _that.loader.present().then(() => {
          this.reloadService.checkAddedNewNumber(selectedNumber).then((res) => {
            console.log("inside checkAddedNewNumber " + JSON.stringify(res));
            this.serviceIndicator = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.ListOfServices.Services.ServiceType;


            _that.loader.dismiss();
            if (selectedNumber.substring(0, 3) == "011" && selectedNumber.length == 10) {
              this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid Number", "Please input a valid Celcom phone number. (If starting with 011 then put 11 digits).", "Try again");
            }
            else {
              this.validNumber = true;
              //if BRN number, don't login
              var checkBRN = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.CustomerIDType;

              if (checkBRN == "BRN") {
                _that.alertCtrlInstance.showAlert("Thanks for your interest in Celcom Life.", "At the moment, the app is for use by XPAX and postpaid customers only.", "OK");
              }
              else {
                // JSON store put data mobile number
                var mobile_number = { mobilenum: this.mobileNumber };
                this.jsonstoreService.putData(mobile_number, "mobilenumber"); // data { key : value , <collection_name>}
                this.gatewayService.SetMobileNumber(this.mobileNumber);
                console.log('1111 mobile number '+this.mobileNumber);
                this.reloadService.setingLoginNumber(this.mobileNumber);

                if (this.isFirstTimeUser == "true") {
                  this.navCtrl.setRoot(MobileConnectAccountPage);
                }
                else
                  this.navCtrl.setRoot(ConnectTacPage);
              }

            }


          }).catch(err => {
            _that.loader.dismiss();
            console.log("*****mobile connect*******" + JSON.stringify(err));
            if(err=="No internet connection")
              return;
              if(err.indexOf("timed out") >= 0 || err.indexOf("Failed to connect") >= 0){
                this.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
              }
              else{
                this.alertCtrlInstance.showAlert("Notification", "Not a Celcom number.", "OK");
              }
              // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
            
             
            
          });
        });


      }


    }
  }

  registerMobileConnectUser() {

    var msisdn = this.mobileNumber;
    var _that = this;
    var params_register = { "MSISDN": msisdn };

    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.register, "POST", params_register).then((res_register) => {
        console.log("register user response : " + JSON.stringify(res_register));
        resolve(res_register);

        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          console.log("******************************************** component failure registerMobileConnectUser" + e);
          //alert("error register MobileConnectUser");
          if(e=="No internet connection")
          return;
          _that.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later","OK");
          reject(e);
        });

    });


  }

  checkCelcomUser() {

  }
  handleLogin(e){
    if(e.which === 13){
      this.keyboard.close();
    }
  }
}