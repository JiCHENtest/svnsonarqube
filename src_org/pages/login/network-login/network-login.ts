import {Component, NgZone, Renderer2} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {OnBoardingGuidePage} from "../../on-boarding-guide/on-boarding-guide";
import {NetworkLoginLegalPage} from "./network-login-legal/network-login-legal";
import {NetworkLoginService} from "./network-loginService";
import { GatewayService } from '../../../global/utilService';
/**
 * Generated class for the NetworkLoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-network-login',
  templateUrl: 'network-login.html',
})
export class NetworkLoginPage {
ischecked: boolean = false;
 checkboxFlag: any;
 chkBox: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public zone : NgZone, public networkLoginService:NetworkLoginService, private renderer: Renderer2, public gatewayService: GatewayService) {
  }

  viewlegalPage(value) {
    this.networkLoginService.SetTabInfo(value);
    this.navCtrl.push( NetworkLoginLegalPage );
    console.log("value="+ value);
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageName("Network login", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NetworkLoginPage');
  }

  navigatetoMC() {

    this.navCtrl.setRoot(OnBoardingGuidePage);
  }

  enableLogin() {

    console.log("enableLogin ",this.checkboxFlag);
  
    this.chkBox = document.querySelectorAll(".loginChkBox") as HTMLCollectionOf<HTMLElement>;
    if (this.checkboxFlag == false) {
      console.log("enableLogin if");
      this.zone.run(()=>{
        this.ischecked = false;
        this.checkboxFlag = false;
       
        if(navigator.platform == "iPhone") {
          this.chkBox[0].children[0].style.setProperty('background-color', '', '');
          this.chkBox[0].children[0].style.setProperty('border-color', 'black', '');
        }  
        else
          this.chkBox[0].children[0].style = "";
        
      }); 
    }
    else {
      console.log("enableLogin else");
      this.zone.run(()=>{
        this.ischecked = true;
        this.checkboxFlag = true;
        if(navigator.platform == "iPhone") {
          this.chkBox[0].children[0].style.setProperty('background-color', '#009ade', 'important');
          this.chkBox[0].children[0].style.setProperty('border-color', '009ade', 'important');
        } 
        else
          this.chkBox[0].children[0].style = "background-color: #009ade !important; border-color: #009ade !important;";
      });
    } 


  }

}
