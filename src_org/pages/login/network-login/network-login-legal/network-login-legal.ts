import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {LoginLegalTermsPage} from "./login-legal-terms/login-legal-terms";
import {LoginLegalPrivacyPage} from "./login-legal-privacy/login-legal-privacy";
import {NetworkLoginService} from "../network-loginService";
import { SuperTabsController } from '../../../../components/custom-ionic-tabs';

/**
 * Generated class for the NetworkLoginLegalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-network-login-legal',
  templateUrl: 'network-login-legal.html',
})
export class NetworkLoginLegalPage {
  public selectedTab:any;
 public termsTab:any = LoginLegalTermsPage;
 public policyTab:any = LoginLegalPrivacyPage;
  constructor(public navCtrl: NavController, public navParams: NavParams,  public networkLoginService:NetworkLoginService,private tabsCtrl: SuperTabsController) {
    this.selectedTab = this.networkLoginService.GetTabInfo();
    
      }

      ngAfterViewInit() {
        // this.tabsCtrl.enableTabsSwipe(false, 'TandCTabs');
      }
      
 goBack(){
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop({animate: false});
    }
  }
  
 
}
