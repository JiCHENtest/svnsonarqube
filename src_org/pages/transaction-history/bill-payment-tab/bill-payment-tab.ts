import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TransactionHistoryDetailsPage} from "../history-details/history-details";
import { TransactionHistroyService } from '../../../pages/transaction-history/transactionHistroyService';
import { GatewayService } from '../../../global/utilService';
import { DashboardService } from '../../../pages/home/homeService';
import { TransactionHistoryDetailsPayBillPage } from "../../../pages/transaction-history/history-details-pay-bill/history-details-pay-bill";
import { OrderByFilterPipe } from '../../../pages/transaction-history/orderby_filter.pipe';
import { Pipe,PipeTransform } from '@angular/core';
import { orderBy } from 'lodash';
import { AlertService } from '../../../global/alert-service';
/**
 * Generated class for the TransactionHistoryBillPaymentTabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-transaction-history-transaction-bill-payment-tab',
  templateUrl: 'bill-payment-tab.html',
})
export class TransactionHistoryBillPaymentTabPage {
  MobileNumber :any;
  loader:any;
  rootNavCtrl : NavController;
  CurretMonth:any;
  NextMonth:any;
  LastMonth:any;
  CurrentMonthDetails:any;
  CurrentMonthDetail:any;
  NextMonthDetails:any;
  LastMonthDetails:any;
  TotalHistroyDetails:any;
  voucherReloads:any;
  onlineReloads:any;
  billPayment:any;
  PostPaid:boolean = false;
  Prepaid:boolean  = false;
  BillList:any;
  currentMonthFlag:boolean = false;
  NextMonthFlag:boolean = false;
  LastmonthMonthFlag:boolean = false;
  EmptyStatus:boolean = false;
  monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public transcationsHistorys:TransactionHistroyService, 
    public gatewayService: GatewayService, public dashboardService: DashboardService, 
    private alertCtrlInstance: AlertService, private renderer: Renderer2) {
    this. rootNavCtrl = navParams.get('rootNavCtrl');
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    if(this.gatewayService.isPrePaid()){
      this.gatewayService.setAnalyticPageNameProfileSegment("Reload transaction history", this.renderer);
    }else{
      this.gatewayService.setAnalyticPageNameProfileSegment("Bill payment transaction history", this.renderer);
    }
  }

  ionViewDidLoad() {
    this.loader = this.gatewayService.LoaderService();
    this.loader.present();
    this.MobileNumber =  this.gatewayService.GetMobileNumber();

    if(this.gatewayService.isPrePaid()){
      this.Prepaid = true;
      this.PostPaid = false;
      this.transcationsHistorys.FetchReloadTransactionHistory(this.MobileNumber).then((res) => {
        console.log("*****FetchPayBillReloadTransactionHistory*******" + JSON.stringify(res));
        //res = {"isSuccessful":true,"voucherReloads":{"isSuccessful":false,"warnings":[],"errors":["Unexpected error in server, see logs"],"info":[]},"onlineReloads":{"isSuccessful":true,"resultSet":[{"TOTALAMOUNT":"30.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180131102100","RESPONSE_TIME":"31-01-2018"},{"TOTALAMOUNT":"10.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180204102248","RESPONSE_TIME":"04-02-2018"},{"TOTALAMOUNT":"30.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180204102249","RESPONSE_TIME":"04-02-2018"},{"TOTALAMOUNT":"30.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180204102250","RESPONSE_TIME":"04-02-2018"},{"TOTALAMOUNT":"10.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180205102262","RESPONSE_TIME":"05-02-2018"},{"TOTALAMOUNT":"10.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180110100478","RESPONSE_TIME":"10-01-2018"},{"TOTALAMOUNT":"10.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180131102101","RESPONSE_TIME":"31-01-2018"},{"TOTALAMOUNT":"30.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180131102102","RESPONSE_TIME":"31-01-2018"},{"TOTALAMOUNT":"10.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180131102103","RESPONSE_TIME":"31-01-2018"},{"TOTALAMOUNT":"10.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180131102106","RESPONSE_TIME":"31-01-2018"},{"TOTALAMOUNT":"10.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180131102109","RESPONSE_TIME":"31-01-2018"},{"TOTALAMOUNT":"10.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180131102110","RESPONSE_TIME":"31-01-2018"},{"TOTALAMOUNT":"10.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180131102115","RESPONSE_TIME":"31-01-2018"},{"TOTALAMOUNT":"10.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180123101375","RESPONSE_TIME":"23-01-2018"},{"TOTALAMOUNT":"30.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180123101378","RESPONSE_TIME":"23-01-2018"},{"TOTALAMOUNT":"10.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180123101387","RESPONSE_TIME":"23-01-2018"},{"TOTALAMOUNT":"10.00","PAYMENT_TYPE":"Reload Via Credit Card","ORDERID":"C20180123101389","RESPONSE_TIME":"23-01-2018"}]}}
        var d = new Date();
        this.CurretMonth = this.monthNames[d.getMonth()];
        if(this.CurretMonth == "February"){
            this.NextMonth = "January" ;
            this.LastMonth = "December";
          }else if (this.CurretMonth == "January"){
            this.NextMonth = "December" ;
            this.LastMonth = "November";
          }else{
            this.NextMonth = this.monthNames[d.getMonth()-1] ;
            this.LastMonth = this.monthNames[d.getMonth()-2];
          }
       this.CurrentMonthDetails = [];
       this.onlineReloads = [];
       this.voucherReloads = [];

       if(res["isSuccessful"] == true){
         if(res["voucherReloads"].resultSet.length > 0 || res["onlineReloads"].resultSet.length > 0){
          this.EmptyStatus = false;
          if(res["voucherReloads"].resultSet !=undefined && res["onlineReloads"].resultSet !=undefined){
            this.CurrentMonthDetails = res["voucherReloads"].resultSet.concat(res["onlineReloads"].resultSet);
           }else if(res["voucherReloads"].resultSet !=undefined){
            this.CurrentMonthDetails = res["voucherReloads"].resultSet;
          }else if(res["onlineReloads"].resultSet !=undefined){
            this.CurrentMonthDetails = res["onlineReloads"].resultSet;
           }
           this.CurrentMonthDetails.forEach(element => {
            if(this.CurretMonth == this.monthNames[element.RESPONSE_TIME.split('-')[1]-1])
            this.currentMonthFlag = true;
          if(this.NextMonth == this.monthNames[element.RESPONSE_TIME.split('-')[1]-1])
            this.NextMonthFlag = true;
          if(this.LastMonth == this.monthNames[element.RESPONSE_TIME.split('-')[1]-1])
            this.LastmonthMonthFlag = true;

            if(this.currentMonthFlag ==false && this.NextMonthFlag == false && this.LastmonthMonthFlag == false)
            this.EmptyStatus = true;
            else
            this.EmptyStatus = false;

            if(typeof element.TOTALAMOUNT !='undefined' && element.TOTALAMOUNT !='NA'){
              element.TOTALAMOUNT = Number(element.TOTALAMOUNT).toFixed(2);
            }


            var params = {
                "TOTALAMOUNT":element.TOTALAMOUNT,
                "PAYMENT_TYPE":element.PAYMENT_TYPE,
                "RESPONSE_TIME":element.RESPONSE_TIME,
                "MONTHNAME":this.monthNames[element.RESPONSE_TIME.split('-')[1]-1]
              }
              this.CurrentMonthDetails.push(params);
            console.log("*****saveCards*******" + JSON.stringify(this.CurrentMonthDetails));
          });
         }else{
          this.EmptyStatus = true;
         }

          console.log("*****CurrentMonthDetails*******" + JSON.stringify(this.CurrentMonthDetails));
       }else{
        this.EmptyStatus = true;
        //this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
       }
        this.loader.dismiss();
      }).catch(err => {
        this.loader.dismiss();
        console.log("*****reload*******" + JSON.stringify(err));
        this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      });
    }else{
      this.Prepaid = false;
      this.PostPaid = true;
      var userInfo = this.dashboardService.GetUserInfo();
      var billingNumber = userInfo["ListOfServices"].Services.AssetBillingAccountNo;
      var ServiceType = userInfo["ListOfServices"].Services.ServiceType;

      // var billingNumber = "152940466";
      // var ServiceType = "Postpaid"



this.billPayment = "Bill Payment"

      this.transcationsHistorys.FetchPayBillTransactionHistory(billingNumber,ServiceType).then((res) => {
        console.log("*****FetchPayBillTransactionHistory*******" + JSON.stringify(res));
        this.loader.dismiss();
      var d = new Date();
      this.CurretMonth = this.monthNames[d.getMonth()];
      if(this.CurretMonth == "February"){
          this.NextMonth = "January" ;
          this.LastMonth = "December";
        }else if (this.CurretMonth == "January"){
          this.NextMonth = "December" ;
          this.LastMonth = "November";
        }else{
          this.NextMonth = this.monthNames[d.getMonth()-1] ;
          this.LastMonth = this.monthNames[d.getMonth()-2];
        }
        this.CurrentMonthDetails = [];
        this.CurrentMonthDetail = [];
        this.BillList = [];
        if(res["billList"]){
          this.BillList = res["billList"];
        }

          if(res["billAmountList"]){
            this.EmptyStatus = false;

            if(res["billAmountList"].billHistoryList.length > 0){
              this.CurrentMonthDetails = res["billAmountList"].billHistoryList;
              this.CurrentMonthDetails.forEach(element => {
                if(this.CurretMonth == this.monthNames[element.BillCycleEnd.split(' ')[0].split('/')[1]-1])
                this.currentMonthFlag = true;
              if(this.NextMonth == this.monthNames[element.BillCycleEnd.split(' ')[0].split('/')[1]-1])
                this.NextMonthFlag = true;
              if(this.LastMonth == this.monthNames[element.BillCycleEnd.split(' ')[0].split('/')[1]-1])
                this.LastmonthMonthFlag = true;

                if(this.currentMonthFlag ==false && this.NextMonthFlag == false && this.LastmonthMonthFlag == false)
                this.EmptyStatus = true;
                else
                this.EmptyStatus = false;
                if(typeof element.TotalDue !='undefined' && element.TotalDue !='NA'){
                  element.TotalDue = Number(element.TotalDue).toFixed(2);
                }
                var params = {
                    "TotalDue":element.TotalDue,
                    "BillCycleStart":element.BillCycleStart.split(' ')[0],
                    "BillCycleEnd":element.BillCycleEnd.split(' ')[0],
                    "BillingAccountNumber":element.BillingAccountNumber,
                    "InvoiceNumber":element.InvoiceNumber,
                    "MONTHNAME":this.monthNames[element.BillCycleEnd.split(' ')[0].split('/')[1]-1]
                  }
                  this.CurrentMonthDetail.push(params);

                console.log("*****saveCards*******" + JSON.stringify(this.CurrentMonthDetail));
              });
          }else{
            this.EmptyStatus = true;
          }
        }else{
          this.EmptyStatus = true;
          //this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
        }



      }).catch(err => {
        this.loader.dismiss();
        console.log("*****reload*******" + JSON.stringify(err));
        this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      });
    }


    console.log('ionViewDidLoad TransactionHistoryBillPaymentTabPage');
  }

  goToDetailsPage (item) {
    var objectidis;
    if(this.gatewayService.isPrePaid()){

    }else{
      var endDate  = item.BillCycleEnd.split('/')[1];
      this.BillList.forEach(element => {
        if(element.fileformattedDate.split('-')[1] == endDate){
          objectidis = element.objectId;
        }
      });
    }





    if(this.gatewayService.isPrePaid()){
      var TransationStatus = "Reload";
      this.rootNavCtrl.push(TransactionHistoryDetailsPage,{
        param1:item,param2:TransationStatus
      });

    }else{

      this.rootNavCtrl.push(TransactionHistoryDetailsPayBillPage,{
        param1:item,param2:objectidis
      });
    }



  }
}
