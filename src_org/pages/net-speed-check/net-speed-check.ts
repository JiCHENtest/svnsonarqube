import { Component, Renderer2 } from '@angular/core';
import { NavController } from 'ionic-angular';
// page
import { NetSpeedCheckInfoPage} from "../../pages/net-speed-check-info/net-speed-check-info";
// service
import { NetcheckerProvider } from '../../global/netchecker';
import { GatewayService } from '../../global/utilService';
/**
 * Generated class for the NetSpeedCheckPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-net-speed-check',
  templateUrl: 'net-speed-check.html',
})
export class NetSpeedCheckPage {

  // page
  msisdnText : any;
  registrationId : any;

  netSpeedCheckInfoPage : any = NetSpeedCheckInfoPage;
  constructor(public navCtrl: NavController, public netcheckerProvider : NetcheckerProvider, 
    public gatewayService:GatewayService, private renderer: Renderer2) {
    console.log('constructor NetSpeedCheckPage');
    this.netcheckerProvider.refreshMsisdn(0);
    this.msisdnText = this.netcheckerProvider.getMsisdnText();
    this.registrationId = this.netcheckerProvider.getRegistrationId();
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Internet speed check", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NetSpeedCheckPage');    
  }

  promptForMsisdn() {
    this.netcheckerProvider.promptForMsisdn();

    this.msisdnText = this.netcheckerProvider.getMsisdnText();
    this.registrationId = this.netcheckerProvider.getRegistrationId();
  }

  promptForMsisdnWithNotification() {
    this.netcheckerProvider.promptForMsisdnWithNotification();

    this.msisdnText = this.netcheckerProvider.getMsisdnText();
    this.registrationId = this.netcheckerProvider.getRegistrationId();
  }

  stopService() {
    this.netcheckerProvider.stopService();

    this.msisdnText = this.netcheckerProvider.getMsisdnText();
    this.registrationId = this.netcheckerProvider.getRegistrationId();
  }

  sendData() {
    this.netcheckerProvider.sendData();

    this.msisdnText = this.netcheckerProvider.getMsisdnText();
    this.registrationId = this.netcheckerProvider.getRegistrationId();
  }

  requestSettings() {
    this.netcheckerProvider.requestSettings();

    this.msisdnText = this.netcheckerProvider.getMsisdnText();
    this.registrationId = this.netcheckerProvider.getRegistrationId();
  }

}
