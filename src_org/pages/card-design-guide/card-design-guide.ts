import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { GatewayService } from '../../global/utilService';

/**
 * Generated class for the CardDesignGuidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-card-design-guide',
  templateUrl: 'card-design-guide.html',
})
export class CardDesignGuidePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private renderer: Renderer2, public gatewayService: GatewayService) {
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Card design guide", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CardDesignGuidePage');
  }

}
