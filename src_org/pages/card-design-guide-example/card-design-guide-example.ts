import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { GatewayService } from '../../global/utilService';

/**
 * Generated class for the CardDesignGuideExamplePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-card-design-guide-example',
  templateUrl: 'card-design-guide-example.html',
})
export class CardDesignGuideExamplePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private renderer: Renderer2, public gatewayService: GatewayService) {
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Card design guide example", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CardDesignGuideExamplePage');
  }

}
