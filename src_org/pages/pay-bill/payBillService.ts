import { ConstantData} from "../../global/constantService";
import { GatewayService } from '../../global/utilService';
import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { AlertController, LoadingController } from 'ionic-angular';
import { HandleError } from '../../global/handleerror';
import { AlertService } from '../../global/alert-service';
import { JsonstoreService } from '../../global/jsonstore';
import { ReloadService } from '../../global/reloadServices';


interface Subject{
  registerObserver(o: Observer);
  notifyObservers(val:string);
}

interface Observer{
  update();
  
}

@Injectable()
export class PayBillService {
dueBillAmount ;
selectedMobileNumber;
mobileNumber;
accountNumber;
PhonebookNumber;
isFullAmountPaid;
afterPaymentAmount;
afterPaymentDate;
orderId:any;
tabInfo:any;
loginMobileNumber:any;
transactionDate:any;
transactionId:any;
is_paybill:any;

constructor(private network: Network,public alertCtrl:AlertController, public loadingCtrl:LoadingController,public handleerror: HandleError, public gatewayService:GatewayService, public jsonstoreService: JsonstoreService, public reloadService:ReloadService){
// this.mobileNumber=this.gatewayService.GetMobileNumber();
 //this.selectedMobileNumber=gatewayService.GetSelectMobileNumber();

 this.loginMobileNumber=this.reloadService.gettingLoginNumber(); 
 console.log("login mobile number from reload service in paybill"+this.loginMobileNumber);
//  var mobile_number = {};
//  this.jsonstoreService.getData(mobile_number, "mobilenumber").then(
//    (result_mob) => {
//      console.log("mobile_number update() ", JSON.stringify(result_mob));

//      this.loginMobileNumber = result_mob.length > 0 ? result_mob[0].json.mobilenum : "";
//     });


};

private observers: Observer[] = [];
private amount: any;
private date:any;


registerObserver(o: Observer) {
    this.observers.push(o);
}

notifyObservers() {
 


    for (let observer of this.observers) {
      observer.update();
    }
  }
FeachAllSubMObileNumber(){

    try{
     var getAuthString= window.btoa('newCelcomApp:newce!c0m@99%');
     //var getMobileNumber = "60145960894";
    // alert("O*****K"+this.mobileNumber);
     var params = {
          "MobileNumber":this.loginMobileNumber,
          "AuthString": getAuthString
      }
      return new Promise((resolve, reject) => {
    this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetMobileNumber,"POST",params).then((res)=> {
      
      console.log("*********************************** component postpaid paybill GetMobileNumber FetchUserInfo"+JSON.stringify(res));
     //alert("AlertFine"+res);
      resolve(res);
    })
    .catch(function(e){
      console.log("******************************* component  postpaid paybill GetMobileNumber failure FetchUserInfo"+e);
     // alert("ERROR"+e);
      //alert("error user info");
      reject(e);
  });
  });
  }catch(e){
    //alert("error"+JSON.stringify(e));
  }
  
  
}

fetchBillDetails(selectedNumber,billingNumber){
  try{
  //alert("SelectedNumber"+selectedNumber)
  // var getMobileNumber = "0136426573";
  var params = {
        "MSISDN":selectedNumber,
        "billingAccountNumber":billingNumber
    }
    return new Promise((resolve, reject) => {
  this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetBillDueDetails,"POST",params).then((res)=> {
    
    console.log("*********************************** component GetBillDueDetails FetchUserInfo"+JSON.stringify(res));
   //alert("AlertFine"+res);
    resolve(res);
  })
  .catch(function(e){
    console.log("******************************* component  GetBillDueDetails failure FetchUserInfo"+e);
   // alert("ERROR"+e);
    //alert("error user info");
    reject(e);
});
});
}catch(e){
  //alert("error"+JSON.stringify(e));
}
}

PostpaidPaymentGateway(params){
  


try{

return new Promise((resolve, reject) => {

this.gatewayService.CallAdapter(ConstantData.adapterUrls.PostpaidPaymentURL, "POST", params).then(function (res) {
console.log("*********************************** component  PostPaymentGatway"+JSON.stringify(res));
resolve(res);
})
.catch(function(e){
  // this.alertCtrlInstance.showAlert("", " System down, cant call Payment Gateway", "Done");
 
console.log("******************************* component  failure PostPaymentGatway"+e);
//alert("error user info");
reject(e);
});
});
}catch(e){
//alert("error"+JSON.stringify(e));
}



}
checkAddedNewNumber(addedNumber) {
  var _that = this;
  //alert("fetch info mobile number"+this.mobileNumber);
  try{
      var params = {
        "SERIAL_NUM": addedNumber//this.localStorage.get("MobileNum","")
      }
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetUserInfo,"POST",params).then(function(res){
          console.log("*********************************** component success checkAddedNewNumber"+JSON.stringify(res));
          _that.handleerror.handleErrorFetchUserInfo(res,"FetchUserInfo").then((result) => {
            if(result == true) {
              console.log("hai if ",result);
              resolve(res);
            }
            else {
              console.log("hai else ",result);
              reject("error : no data found.");
            }

          },(err)=>{
            console.log("hai else err ",err);
            reject("error : no data found.");              
          });
        })
        .catch(function(e){
            console.log("******************************* component  failure checkAddedNewNumber"+e);
            //alert("error user info FetchUserInfo");
            
            reject(e);
        });
      });
  }catch(e){
    //alert("error"+JSON.stringify(e));
  }
}
//service for DigitalPIn
PostReloadPin(params) {
  
    var _that = this;
  
  try{
  
     return new Promise((resolve, reject) => {
   this.gatewayService.CallAdapter(ConstantData.adapterUrls.PostReloadPin,"POST",params).then((res)=> {
     console.log("*********************************** component  PostReloadPin"+JSON.stringify(res));
      resolve(res);
   })
   .catch(function(e){
     console.log("******************************* component  failure PostReloadPin"+e);
     //alert("error user info");
     reject(e);
  });
  });
  }catch(e){
   //alert("error"+JSON.stringify(e));
  }
  
  }
fetchSavedCards(param){
    try{
      //alert("SelectedNumber"+selectedNumber)
      // var getMobileNumber = "0136426573";
      var params = {
            "loginID":param,
           };
        return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.fetchSavedCards,"POST",params).then((res)=> {
        console.log("fetch saved cards params+++++++||++++"+JSON.stringify(params));
        console.log("*********************************** component fetchSavedCards"+JSON.stringify(res));

       //alert("AlertFine"+res);
        resolve(res);
        
      })
      .catch(function(e){
        console.log("******************************* component failure fetchSavedCards"+e);
       
        reject(e);
    });
    });
    }catch(e){
      //alert("error"+JSON.stringify(e));
    }
  }
  
  fetchTransactionDetails(loginNumber,amount){
    try{
      var params = {
            "loginID":loginNumber,
            "orderId":this.getOrderId(),
            "msisdn":this.GetSelectMobileNumber(),
            "totalAmount":amount,
            "isKadCeria":0,
            "credit":false
           };
        return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.fetchTransDetails,"POST",params).then((res)=> {
        console.log("transaction details params ::"+"loginid:"+params.loginID+" OrderId: "+params.orderId+ " msisdn: "+params.msisdn+" total amount: "+params.totalAmount+" isKadceria: "+params.isKadCeria+" credit: "+params.credit);
        console.log("*********************************** component fetchTransactionDetails"+JSON.stringify(res));

       //alert("AlertFine"+res);
        resolve(res);
        
      })
      .catch(function(e){
        console.log("******************************* component failure fetchTransactionDetails"+e);
       
        reject(e);
    });
    });
    }catch(e){
      //alert("error"+JSON.stringify(e));
    }
  }

  SetTransactionDate(obj){
    this.transactionDate = obj;
  }
  
  GetTransactionDate(){
    return this.transactionDate;
  }
  SetTransactionId(obj){
    this.transactionId = obj;
  }
  
  GetTransactionId(){
    return this.transactionId;
  }
SetDueBillAmount(obj){
    this.dueBillAmount = obj;
  }
  
  GetDueBillAmount(){
    return this.dueBillAmount;
  }
  getPhone_BookNumber(){
    return this.PhonebookNumber;
    }
    
    setPhone_BookNumber(value){
    this.PhonebookNumber = value;
    }

    GetSelectMobileNumber(){
      return this.selectedMobileNumber;
    }
    SetSelectMobileNumber(value){
     this.selectedMobileNumber =  value;
    }
    setAccountNumber(value){
      this.accountNumber= value;
    }
    getAccountNumber(){
      return this.accountNumber;
    }

    setFullAmountPaid(value){
      this.isFullAmountPaid= value;
    }
    getFullAmountPaid(){
      return this.isFullAmountPaid;
    }

    setAfterPaymentAmount(value){
      this.afterPaymentAmount = value;
    }
    getAfterPaymentAmount(){
      return this.afterPaymentAmount;
    }
   setAfterPaymentDate(value){
     this.afterPaymentDate=value;
   }
   getAfterPaymentDate(){
    return this.afterPaymentDate;
   }
   setOrderId(value){
     this.orderId=value;
   }
   getOrderId(){
     return this.orderId;
   }

   setIsPaybill(value){
     this.is_paybill=value;
   }
   getIsPaybill(){
    return this.is_paybill;
  }
 
  
  SetTabInfo(obj){
    this.tabInfo = obj;
  }
  
  GetTabInfo(){
    return this.tabInfo;
  }
  
  
  

}