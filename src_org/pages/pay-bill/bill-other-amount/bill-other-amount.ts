import { Component, NgZone, Renderer2 } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { ChangeCreditcardModalPage } from "../../change-creditcard-modal/change-creditcard-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import * as sha1 from "sha1";
import { DatePipe } from '@angular/common';
import { PayBillService } from '../payBillService';
import { GatewayService } from '../../../global/utilService';
import { AlertService } from '../../../global/alert-service';
import { Events } from 'ionic-angular';
import { PaymentRecieptPage } from '../../payment-reciept/payment-reciept';
import { AlertController, LoadingController } from 'ionic-angular';
import { ReloadService } from '../../../global/reloadServices';
import { Keyboard } from '@ionic-native/keyboard';
import { DashboardService } from '../../../pages/home/homeService';
import { analyticTransactionDetailsInterface } from '../../../global/interface/analytic-transaction-details';
/**
 * Generated class for the BillOtherAmountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-bill-other-amount',
  templateUrl: 'bill-other-amount.html',
})
export class BillOtherAmountPage {
  amountEntered: any;
  dueBillAmount: any;
  mobilenumber: any;
  nextDisable: any;
  cardType: string;
  indexValue: string;
  maxAmount: any;
  otherAmountForm: any;
  loader;
  accountNumber: any;
  selectedCard: string;
  Cardflag: any;
  loggedinNumber: any;

  arrayOfCards: any;
  isSaved: any;
  orderId: any;
  selectToken: any;
  isSavedCardUsed: any;
  payType: any;
  disable: boolean;
  cardsSaved: any;
  LoginbillingType: any;
  // carArray = ["12345678912345123","0981237891234345","98545678912345666","2299447891234745"];
  cardArray: any;
  finalFlag: any;
  transactionDetails: analyticTransactionDetailsInterface;
  paymentMethod = [
    {
      "type": "Credit/Debit Card",
      "icon": "custom-reload-card"
    },
    {
      "type": "Online Banking",
      "icon": "custom-reload-online"
    }

  ];

  constructor(public navCtrl: NavController, public events: Events, public navParams: NavParams,
    public formBuilder: FormBuilder, public modalCtrl: ModalController, private alertCtrlInstance: AlertService,
    public gatewayService: GatewayService, public theInAppBrowser: InAppBrowser, public datePipe: DatePipe,
    public paybillService: PayBillService, public alertCtrl: AlertController, public reloadService: ReloadService,
    private keyboard: Keyboard, public zone: NgZone, public dashboardService: DashboardService, private renderer: Renderer2) {
    console.log("index valie when nothing clicked is" + this.indexValue);
    this.isSavedCardUsed = false;
    // this.disable = true;
    this.loggedinNumber = this.reloadService.gettingLoginNumber();
    this.loader = this.gatewayService.LoaderService();
    //this.selectedCard = this.carArray[0];

    var _that = this;
    events.subscribe('Money', (dueAmount) => {

      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('Welcome::' + dueAmount);
      _that.dueBillAmount = dueAmount;
    });

    // this.dueBillAmount=  this.paybillService.GetDueBillAmount();
    // console.log("entered other amount page with bill due amount= "+this.dueBillAmount);

    _that.maxAmount = _that.dueBillAmount;

    events.subscribe('accountNumber', (accountNo) => {

      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('account number in other amount is::' + accountNo);
      _that.accountNumber = accountNo;
    });

    console.log("Account number::" + _that.accountNumber);
    //'(^[1-9]\d*$)(\.[0-9]{1,2})'-google
    //'^[1-9]\d*(\.[0-9]{1,2})?$' -vaibhav
    // ^[1-9][0-9]*$
    //value="{{ exampleNumber | number : '1.2-2' }}
    //^[1-9]([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/
    _that.otherAmountForm = formBuilder.group({
      amountEntered: ['', Validators.compose([Validators.pattern('[1-9][0-9]*(\.[0-9]{2})?'), Validators.required])],
      //^[0-9]*\.[0-9]{2}$
    });

  }

  ionViewDidEnter() {
    this.initiateAnalyticTransactionJourney();
  }

  initiateAnalyticTransactionJourney() {
    this.transactionDetails = {};
    this.gatewayService.resetAllAnalyticData();
    this.transactionDetails.jouneryDepth = "initiation";
    this.transactionDetails.transactProduct = "Other amount";
    this.gatewayService.setAnalyticTransactionJourney("Other amount", this.renderer, this.transactionDetails);
  }

  ionViewDidLoad() {
    this.paybillService.SetTabInfo(2);
    console.log('ionViewDidLoad BillOtherAmountPage');
    this.LoginbillingType = this.gatewayService.GetBillingType();
    this.nextDisable = true;


     this.events.subscribe('savedCards', (savedCards) => {

      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('savedCards in other amount is::' + JSON.stringify(savedCards));
      this.cardsSaved = savedCards;
     //this.getSavedCards();
      this.savedCards( this.cardsSaved);

     });
    

  }
  ngOnDestroy() {
    this.events.unsubscribe('Money');
    this.events.unsubscribe('accountNumber');
  }
 
  savedCards(res) {

    if (res["savedCards"] == undefined) {

      this.Cardflag = false;
      //this.disable = false;
    }
    else if (res["savedCards"].length >= 1) {

      this.Cardflag = true;

      this.nextDisable = false;
      this.isSavedCardUsed = true;
      // this.disable = true;
      this.cardArray = [];
      var allcards = res["savedCards"];
      allcards.forEach(element => {
        this.cardArray.push(element);
        this.selectedCard = res["savedCards"][0].masked;
        this.selectToken = res["savedCards"][0].TOKEN;
        console.log("*****saveCards*******" + JSON.stringify(element));
        // if (element.is_default == true) {
        //   this.selectedCard = element.masked;
        //   this.selectToken=element.TOKEN;
        // }

      });
      this.addCss(0);
    } else {

      // this.disable = false;
      this.Cardflag = false;
    }



  }
  addCss(index) {
    this.zone.run(() => {
      console.log("came into addCss function of other amounts and index is" + index);
      this.cardType = "";
      this.cardType = this.paymentMethod[index].type;
      this.indexValue = index;
      console.log("came into addCss function of other amounts and index is" + this.indexValue);
      //&& this.indexValue!=""
      if (this.amountEntered != "" && ((index != "") || (index != undefined)) || this.amountEntered != undefined) {
        console.log("next disabled is *********" + this.nextDisable);
        this.nextDisable = false;
      } else {
        this.nextDisable = true;
      }


    });
  }

  goToPay() {
    //Analytic Part
    this.initiateAnalyticTransactionJourney();

    if (this.cardType) {

      var paymentType = "";
      if (this.cardType == "Credit/Debit Card") {
        paymentType = "1";
      }
      else
        paymentType = "7";
      this.paybillService.setFullAmountPaid(false);
      this.openUrlPostpaid(this.amountEntered, paymentType);
    } else if (this.isSavedCardUsed) {
      this.paybillService.setFullAmountPaid(false);
      this.openUrlPostpaid(this.amountEntered, paymentType);
    } else {
      this.alertCtrlInstance.showAlert("Message", "Please select Payment type and Amount", "OK");
    }

  }
  cardShow(selectedCard) {
    var cardShow = selectedCard.replace(/.(?=.{4})/g, '*');
    return cardShow;
  }

  presentingChangeCreditModal() {
    let getCardArrayCard = this.cardArray;
    let selectedCard = this.selectedCard;

    let modal = this.modalCtrl.create(ChangeCreditcardModalPage, { selectedCard, getCardArrayCard });
    modal.present();

    modal.onWillDismiss(data => {
      console.log(data);
      if (data != null) {
        if (data["masked"]) {
          this.cardType = "";
          // this.disable = true;
          if (this.amountEntered != "" || this.amountEntered != undefined) {
            this.isSavedCardUsed = true;

            this.nextDisable = false;
            this.selectedCard = data.masked;
            this.selectToken = data.TOKEN;

          } else {

            this.nextDisable = true;
            this.isSavedCardUsed = true;
            //this.selectedCard = data;
            this.selectedCard = data.masked;
            this.selectToken = data.TOKEN;
          }
          this.addCss(0);
        } else {
          console.log("use new card");
          // this.disable = false;
          this.isSavedCardUsed = false;

          this.selectToken = "";
          this.selectedCard = "Use new card";
          this.addCss(0);


        }


      }
    })
  }

  openUrlPostpaid(value, type) {
    this.loader = this.gatewayService.LoaderService();
    //call payment adapter
    // "1" for carid card 
    //"7"  onlineback
    this.mobilenumber = this.paybillService.GetSelectMobileNumber();


    var _that = this;
    var date = this.datePipe.transform(new Date(), 'yyyyMMddHHmmssZ');
    var amount = parseFloat(value).toFixed(2);
    var accountNum = this.accountNumber;
    var recepient = this.mobilenumber;

    if (this.isSavedCardUsed == true) {

      this.payType = 4;
      if (type == 7) {
        this.payType = type;
      }
    } else {
      this.payType = type;
    }

    console.log("mobile number for paybill" + this.mobilenumber);
    var params = { "postpaidAccountNum": accountNum, "notifyRecipient": recepient , "paymentMethod": this.payType, "totalAmount": amount, "loginID": this.loggedinNumber, "isInternetReload": false };
    this.loader.present();
    console.log("*****Payment params in other amount*****" + JSON.stringify(params));
    this.paybillService.PostpaidPaymentGateway(params).then((res) => {

      //this.Cardflag = true;
      console.log("*********** component success PostpaidPaymentURL" + JSON.stringify(res));
      this.loader.dismiss();


      var payment_url = "";
      if (this.Cardflag) {
        payment_url = res["paymentURL"] + '&prePaymentToken=' + this.selectToken;
      } else {
        payment_url = res["paymentURL"];
      }
      console.log("*********** component success payment_url" + JSON.stringify(payment_url));

      var resUrl = res["responseUrl"];
      this.orderId = res["orderId"];
      var msgStatus = res["msgStatus"];
      this.paybillService.setOrderId(this.orderId);

      console.log("payment url for paybill" + payment_url);

      if (msgStatus == "Success" || payment_url != undefined || payment_url != null || payment_url != "") {

        //Analytic Part Start
        this.transactionDetails.jouneryDepth = "checkout";
        this.transactionDetails.transactValue = amount;
        if (this.cardType == "Credit/Debit Card") {
          this.transactionDetails.transactMethod = "Credit card";
        } else {
          this.transactionDetails.transactMethod = "Online banking";
        }
        //Analytic Part End

        const browser = _that.theInAppBrowser.create(payment_url, '_blank', 'location=no');
        browser.show();

        browser.on("exit").subscribe((e) => {
          console.log("exit" + e.url);
          //this.checkingKadCeriaBonus();
          this.getTransactionDetails();

          //this.alertCtrlInstance.showAlert("Notification", this.dueBillAmount + " has been settled bill for " + this.mobilenumber, "OK");
        }, err => {
          console.log("error" + err);
        });


        browser.on("loadstop").subscribe((e) => {

          console.log("loadstop" + e.url);
          if (e.url.startsWith(resUrl)) {
            browser.close();
          } else {

          }
        }, err => {
          console.log("error" + err);
        });

        browser.on("loaderror").subscribe((e) => {
          browser.close();
          console.log("loaderror" + e.url);
        }, err => {
          console.log("error" + err);
        });

      } else {
        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "failed";
        this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");
      }

      //Analytic Part
      this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);

    })
      .catch(err => {
        this.loader.dismiss();

        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "failed";
        this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);

        console.log("*************** component failure PostpaidPaymentURL" + err);
        if (err == "No internet connection")
          return;
        this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");

      });
  }

  DataAlignment(gettingDate) {
    var day = gettingDate.slice(0, 8);
    var year = day.match(/.{1,4}/g);
    var date = year[1].match(/.{1,2}/g);
    var finalDate = date[1] + '/' + date[0] + '/' + year[0];
    return finalDate;
  }
  getTransactionDetails() {
    // alert("Gettting "+selectedNumber);
    this.loader.present();
    this.paybillService.fetchTransactionDetails(this.loggedinNumber, this.amountEntered).then((res) => {

      console.log('fetchTransactionDetails ******' + JSON.stringify(res));
      var isSuccessful = res["isPaymentSuccess"];
      var transactionDate = res["resultSet"][0].TRANSDATE;
      if (transactionDate != null || transactionDate != "") {
        transactionDate = this.DataAlignment(transactionDate);
      }
      var transactionId = res["resultSet"][0].REFERID;
      this.paybillService.setIsPaybill(true);
      this.loader.dismiss();

      if (isSuccessful == true) {

        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "success";

        this.paybillService.SetTransactionDate(transactionDate);
        this.paybillService.SetTransactionId(transactionId);

        this.modal();

      } else {

        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "failed";

        //this.events.publish('updateNumber', this.mobilenumber, this.accountNumber);
        if (res["resultSet"][0].REASONCODE == "51011") {
          this.alertCtrlInstance.showAlert("Uh Oh. That's a Wrong CVC Number", "Please enter a valid 3-digit CVC number !!!", "OK");
        } else if (res["resultSet"][0].REASONCODE == "51012") {
          this.alertCtrlInstance.showAlert("Uh Oh. Got Another Credit Card?", "We prefer VISA or Mastercard.", "OK");
        } else if (res["resultSet"][0].REASONCODE == "51013") {
          this.alertCtrlInstance.showAlert("Uh Oh. Wrong Expiry Date?", "Please enter a valid expiry date", "OK");
        } else if (res["resultSet"][0].REASONCODE == "30000" || res["resultSet"][0].REASONCODE == "32001" || res["resultSet"][0].REASONCODE == "33001" || res["resultSet"][0].REASONCODE == "37036 ") {
          this.alertCtrlInstance.showAlert("Hang On. We're Working on Your Transaction", "Successful transaction will be reflected in transaction history", "OK");
        } else if (res["resultSet"][0].REASONCODE == "40000") {
          this.alertCtrlInstance.showAlert("Uh oh. There's a Problem With Your Card ", "Please try again later", "OK");
        } else {
          this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");
        }
      }

      //Analytic Part
      this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);

    }).catch(err => {
      console.log("***paybill*******" + JSON.stringify(err));
      this.loader.dismiss();

      //Analytic Part
      this.transactionDetails.jouneryDepth = "post";
      this.transactionDetails.transactResult = "failed";
      this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);
            
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");
      //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }
  modal() {
    let modal = this.modalCtrl.create(PaymentRecieptPage);
    modal.present();

    modal.onDidDismiss(data => {
      console.log("inside ondiddismissss");
      this.gatewayService.setFlagForDashboardReload(true);
      this.events.publish('updateNumber', this.mobilenumber, this.accountNumber);
      this.savedCards(this.cardsSaved);
      // if (this.Cardflag) {
      //   this.nextDisable = false;
      //  // this.disable = true;
      // } else this.nextDisable = true;
      var remaining = this.dueBillAmount - this.amountEntered;
      var staticNumber = this.gatewayService.GetMobileNumber();
      var userInfo = this.dashboardService.GetUserInfo();
      var prodPromeName = userInfo.ListOfServices.Services.ProdPromName;


      prodPromeName = prodPromeName.toLowerCase();
      if (this.mobilenumber == this.loggedinNumber && this.LoginbillingType == "Billable" && prodPromeName.indexOf("cmp") == -1) {
        if (this.amountEntered < this.dueBillAmount) {
          console.log("remaining amount in other amount is" + remaining);
          this.showAlertToHandleNavigate("You're Almost There!", "RM" + this.amountEntered + " partial settlement has been made for " + this.mobilenumber + ". Additional RM" + remaining + " is required for full bill settlement", ["OK", "Set up Auto Billing"]);
        } else {
          this.showAlertToHandleNavigate("Yay!", "RM" + this.amountEntered + " payment has been made for " + this.mobilenumber, ["OK", "Set up Auto Billing"]);
        }



      } else {
        if (this.amountEntered < this.dueBillAmount) {
          console.log("remaining amount in other amount is" + remaining);
          //this.showAlertOK("You're Almost There!", "RM" + this.amountEntered + " partial settlement has been made for" + this.mobilenumber + ". Additional RM" + remaining + " required for full bill settlement", "OK");
          this.showAlertOK("You're Almost There!", "RM" + this.amountEntered + " partial settlement has been made for " + this.mobilenumber + ".", "OK");
        } else
          //this.showAlertOK("Yay!", "RM" + this.amountEntered + " has made payment for " + this.mobilenumber, "OK");
          this.showAlertOK("Yay!", "RM" + this.amountEntered + " payment has been made for " + this.mobilenumber, "OK");
      }


    });
  }
  showAlertToHandleNavigate(title, msg, btn) {
    if (Array.isArray(btn)) {

      try {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          buttons: [{
            text: btn[0],
            cssClass: 'submit-button',
            handler: () => {
              //update Paybill
              console.log('update Paybill');

            }
          },
          {
            text: btn[1],
            cssClass: 'submit-button',
            handler: () => {

              //open autobilling
              console.log('open autobilling');
              //this.paybillService.notifyObservers();

              this.events.publish('SetUpAutobilling');
            }
          }],
          cssClass: 'success-message',
          enableBackdropDismiss: false
          // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
        });
        alert.present();

      }
      catch (e) {
        console.log("Exception : Alert open ", e);
        if (e == "No internet connection")
          return;
      }
    }
  }

  showAlertOK(title, msg, btn) {
    try {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: msg,
        buttons: [{
          text: btn,
          cssClass: 'error-button-float-right submit-button',
          handler: () => {
            //update Paybill
            console.log('update Paybill');

          }
        }
        ],
        cssClass: 'success-message',
        enableBackdropDismiss: false
        // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
      });
      alert.present();

    }
    catch (e) {
      console.log("Exception : Alert open ", e);
      if (e == "No internet connection")
        return;
    }
  }

  handlePin(e) {
    console.log("e.which" + e.which);
    if (e.which === 13) {
      this.keyboard.close();
    }
  }

}
