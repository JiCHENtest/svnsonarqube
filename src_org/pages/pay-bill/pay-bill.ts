import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { ReloadPopoverComponent } from '../../components/reload-popover/reload-popover';
import { SuperTabsController } from '../../components/custom-ionic-tabs';
import { BillFullAmountPage } from "./bill-full-amount/bill-full-amount";
import { BillOtherAmountPage } from "./bill-other-amount/bill-other-amount";
import { BillReloadCodePage } from "./bill-reload-code/bill-reload-code";
import { GatewayService } from '../../global/utilService';
import { AlertService } from '../../global/alert-service';
import { GlobalVars } from "../../providers/globalVars";
import { PayBillService } from './payBillService';
import { Events } from 'ionic-angular';
import { AutoBillingPage } from '../services/auto-billing/auto-billing';
import { DashboardService } from '../../pages/home/homeService';
import { ReloadService } from '../../global/reloadServices';


/**
 * Generated class for the PayBillPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pay-bill',
  templateUrl: 'pay-bill.html',
})
export class PayBillPage {
  allNumber;
  loader: any;
  selectOptions: any;
  accountNumber: any;
  accountInfo: any;
  totalDueAmount: any;
  billDueDate: any;
  billingDate: any;
  totalAmount: any;
  nextDisable: any;
  totalBill: any;
  tested: string[] = [];
  tab1Page: any = BillFullAmountPage;
  tab2Page: any = BillOtherAmountPage;
  tab3Page: any = BillReloadCodePage;
  selected: string = '';
  isCardSaved: any;
  cardMasked: any;
  cardName: any;
  cardExpiry: any;
  isDefault: any;
  cardToken: any;
  savedCards: any;
  otherNumbers: any;
  addedAccountNumber: any;
  loggedinNumber: any;
  billingType: any;
  LoginbillingType: any;


  constructor(public navCtrl: NavController, public events: Events, public navParams: NavParams, public globalVar: GlobalVars, public popoverCtrl: PopoverController, private tabsCtrl: SuperTabsController, private alertCtrlInstance: AlertService, public gatewayService: GatewayService, public paybillService: PayBillService, public reloadService: ReloadService, public dashboardService: DashboardService) {
    this.tabsCtrl.enableTabsSwipe(false, 'billTab');
    this.loader = this.gatewayService.LoaderService();
    this.loggedinNumber = this.reloadService.gettingLoginNumber();

    events.subscribe('updateNumber', (selected, accountInfo) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('Welcome::' + selected);
      console.log('this.accountInfo::' + accountInfo);
      this.getDueBillDetails(selected, accountInfo);

      // this.dueBillAmount = dueAmount;
    });
    events.subscribe('SetUpAutobilling', () => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.updates();

      // this.dueBillAmount = dueAmount;
    });
    //SetUpAutobilling

    events.subscribe('AccountNumAdded', (accountInfo, addedNum ) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
     this.selected=addedNum;
     this.addedAccountNumber=accountInfo;
     console.log("++++++++++++++++my selected numbers account number in eventss::" +this.addedAccountNumber);
     var accntNum= this.accountNumber.includes(this.addedAccountNumber);
     if(!accntNum){
       this.accountNumber.push(this.addedAccountNumber);
     }
     // this.dueBillAmount = dueAmount;
     this.paybillService.setAccountNumber(this.addedAccountNumber);

     
     this.paybillService.SetSelectMobileNumber(this.selected);
       var index = this.allNumber.indexOf(this.selected);
    
       this.accountInfo=this.accountNumber[index];
       this.events.publish('accountNumber', this.accountInfo);
       this.paybillService.setAccountNumber(this.accountInfo);
       console.log("onwillDismiss accountInfo"+ this.accountInfo);
   //  this.gatewayService.SetSelectMobileNumber(this.selected,this.accountNumber[index]);
  
 // this.loader.present();
   this.getDueBillDetails(this.selected,this.accountInfo);
   this.paybillService.SetDueBillAmount(this.totalDueAmount);
    });



    //
    //paybillService.registerObserver(this);

    this.selectOptions = {
      title: 'Phone Numbers',
      mode: 'ios',
      cssClass: this.globalVar.getCurrentTheme() + " mypopup"
    };
  }
  //  createUser(user) {
  // console.log('User created!')

  //  }
  ngOnDestroy() {
    //this.events.unsubscribe('callTransferAmount');
    console.log('ng destroy unsubscribe all events here');
    this.events.unsubscribe('updateNumber');
    this.events.unsubscribe('SetUpAutobilling');
    this.events.unsubscribe('AccountNumAdded');

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PayBillPage');
this.getSavedCards();
    var paybill = true;
    //  var bilingAccountNumber =  this.dashboardService.GetPostpaidBillingInfo();
    //console.log('*********Bill'+JSON.stringify(bilingAccountNumber));
    this.tabsCtrl.enableTabsSwipe(false, 'billTab');
  this.loader.present();
    var userInfo = this.dashboardService.GetUserInfo();
    console.log('*********Bill' + JSON.stringify(userInfo));
    var adminNumber = userInfo["ListOfServices"].Services.BillingType;
   
    if (adminNumber == "Billable") {
      this.gatewayService.FeachAllSubMObileNumber().then((res) => {
        console.log("Response from fetchsubmobile numbers into paybill is ********" + JSON.stringify(res));

        var getService = res["outputCPResp"].services;
        console.log("***OK paybill***");
        this.tested = [];
        this.allNumber = [];
        this.accountNumber = [];
        this.otherNumbers = [];
        
        if (res["outputCPResp"].services.length == undefined && res["outputCPResp"].services.mobileNumber && res["outputCPResp"].services.pre_Pos_Indicator == "Postpaid") {
          this.allNumber.push(res["outputCPResp"].services.mobileNumber);
          this.otherNumbers.push(res["outputCPResp"].services.mobileNumber);
          // this.selected = this.allNumber[0];
          this.selected = this.gatewayService.GetMobileNumber();
          console.log("MYYYYY mobile no." + this.selected);

          this.paybillService.SetSelectMobileNumber(this.selected);
          console.log("++++++++++++++++my selected number::" + this.selected);
          this.accountNumber.push(res["outputCPResp"].services.assetBillingAccountNo);
          console.log("+++++++++++++++all account numbers::" + this.accountNumber);
          var chooseValue = this.allNumber.indexOf(this.selected);

          //this.selected
          console.log("++++++++++++++++my choose value::" + chooseValue);
          this.accountInfo = this.accountNumber[chooseValue];
          this.paybillService.setAccountNumber(this.accountInfo);
          console.log("Billing account number:" + this.accountInfo);
          this.events.publish('accountNumber', this.accountInfo);
          //assetBillingAccountNo
          this.getDueBillDetails(this.selected, this.accountInfo);
          this.paybillService.SetDueBillAmount(this.totalDueAmount);
 this.loader.dismiss();
        } else {
          console.log("***OK2***");

          getService.forEach(element => {
            if (element.pre_Pos_Indicator == "Postpaid") {

              var  checkDoublication  =  this.allNumber.includes(element.mobileNumber);
              if (!checkDoublication) {
                //this.allNumber.push(getNewNumber);
                this.allNumber.push(element.mobileNumber);
                this.otherNumbers.push(element.mobileNumber);
              }


              this.accountNumber.push(element.assetBillingAccountNo);
              console.log("+++++++++++++++all account numbers in else::" + this.accountNumber);
            }

          });
          this.selected = this.gatewayService.GetMobileNumber();
          console.log("++++++++++++++++my selected number::" + this.selected);
          this.paybillService.SetSelectMobileNumber(this.selected);
          var chooseValue = this.allNumber.indexOf(this.selected);
          //this.selected
          this.accountInfo = this.accountNumber[chooseValue];
          console.log("++++++++++++++++my selected numbers account number::" + this.accountInfo);
          this.events.publish('accountNumber', this.accountInfo);
          this.paybillService.setAccountNumber(this.accountInfo);
          this.getDueBillDetails(this.selected, this.accountInfo);

  this.loader.dismiss();
          this.paybillService.SetDueBillAmount(this.totalDueAmount);
        }
      }).catch(err => {
        console.log("*****paybill*******" + JSON.stringify(err));
        if (err == "No internet connection") {
          this.loader.dismiss();
          return;
        } else {
          this.loader.dismiss();
        }
        this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");

        // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
      });
    } else {
    
      this.tested = [];
      this.allNumber = [];
      this.accountNumber = [];
      this.otherNumbers = [];
      var logginNumber = this.gatewayService.GetMobileNumber();
      this.allNumber.push(logginNumber);
      this.otherNumbers.push(logginNumber);
      // this.selected = this.allNumber[0];
      this.selected = this.gatewayService.GetMobileNumber();
      console.log("MYYYYY mobile no." + this.selected);

      this.paybillService.SetSelectMobileNumber(this.selected);
      console.log("++++++++++++++++my selected number::" + this.selected);

      var bilingAccountNumber = userInfo["ListOfServices"].Services.AssetBillingAccountNo;



      this.accountNumber.push(bilingAccountNumber);


      console.log("+++++++++++++++all account numbers::" + this.accountNumber);
      var chooseValue = this.allNumber.indexOf(this.selected);

      //this.selected
      console.log("++++++++++++++++my choose value::" + chooseValue);
      this.accountInfo = this.accountNumber[chooseValue];
      this.paybillService.setAccountNumber(this.accountInfo);
      console.log("Billing account number:" + this.accountInfo);
      this.events.publish('accountNumber', this.accountInfo);
      //assetBillingAccountNo
      this.getDueBillDetails(this.selected, this.accountInfo);
      this.paybillService.SetDueBillAmount(this.totalDueAmount);
  this.loader.dismiss();
    }

  }
  updateBalance(selectedNumber, accountInfo) {
    var removespecialCharacter = selectedNumber.replace(/[^0-9]/g, "")
    this.paybillService.fetchBillDetails(removespecialCharacter, accountInfo).then((res) => {

      console.log('updated balance getDueBillDetails ******' + JSON.stringify(res));

      this.totalBill = res["Envelope"].Body.FirstAppUsageResponse.ResponseBody.BalanceSummaryDetails.CmuBalanceSummaryVbc.LatestAmountDue;
      console.log("Checking total amount due " + this.totalBill);
      this.totalDueAmount = parseFloat(this.totalBill).toFixed(2);
      this.events.publish('Money', this.totalDueAmount);
      //this.dueAmountEmitter.emit(this.totalDueAmount);
      this.paybillService.SetDueBillAmount(this.totalDueAmount);
      this.loader.dismiss();
      // this.totalAmount="RM"+this.totalDueAmount;
      this.totalAmount = this.totalDueAmount;
      //if(this.totalDueAmount==0 ||this.totalDueAmount==negative){disable other amount too}

      this.billDueDate = res["Envelope"].Body.FirstAppUsageResponse.ResponseBody.BalanceSummaryDetails.CmuBalanceSummaryVbc.DueDate;
      if (this.billDueDate) {
        this.billingDate = this.gatewayService.DataAlignment(this.billDueDate);
        console.log("Billing Date" + this.billingDate);
      } else {
        this.billingDate = "_";
        console.log("Bill date not there");
      }


    }).catch(err => {
      this.loader.dismiss();
      console.log("***paybill*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }
  getDueBillDetails(selectedNumber, accountInfo) {
    // alert("Gettting "+selectedNumber);
    this.loader=this.gatewayService.LoaderService();
    this.loader.present();
    var checkDoublication = this.otherNumbers.includes(selectedNumber);
    if (checkDoublication) {
      var removespecialCharacter = selectedNumber.replace(/[^0-9]/g, "")
      this.paybillService.fetchBillDetails(removespecialCharacter, accountInfo).then((res) => {

        console.log('getDueBillDetails ******' + JSON.stringify(res));

        this.totalBill = res["Envelope"].Body.FirstAppUsageResponse.ResponseBody.BalanceSummaryDetails.CmuBalanceSummaryVbc.LatestAmountDue;
        console.log("Checking total amount due " + this.totalBill);
        this.totalDueAmount = parseFloat(this.totalBill).toFixed(2);
        this.events.publish('Money', this.totalDueAmount);
        //this.dueAmountEmitter.emit(this.totalDueAmount);
        this.paybillService.SetDueBillAmount(this.totalDueAmount);
        this.loader.dismiss();
        // this.totalAmount = "RM" + this.totalDueAmount;
        this.totalAmount = this.totalDueAmount;
        //if(this.totalDueAmount==0 ||this.totalDueAmount==negative){disable other amount too}

        this.billDueDate = res["Envelope"].Body.FirstAppUsageResponse.ResponseBody.BalanceSummaryDetails.CmuBalanceSummaryVbc.DueDate;
        if (this.billDueDate) {
          this.billingDate = this.gatewayService.DataAlignment(this.billDueDate);
          console.log("Billing Date" + this.billingDate);
        } else {
          this.billingDate = "_";
          console.log("Bill date not there");
        }


      }).catch(err => {
        this.loader.dismiss();
        console.log("***paybill*******" + JSON.stringify(err));
        if (err == "No internet connection")
          return;
        this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
        //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
      });
    }
    else {
      this.loader.dismiss();
      this.totalAmount = "_";
      this.events.publish('Money', this.totalAmount);
      this.billingDate = "_";
      console.log("billingdate is blank" + this.billingDate);
      console.log("account info " + this.addedAccountNumber);

      this.events.publish('accountNumber', this.addedAccountNumber);

    }
  }
 getSavedCards(){
    this.loader.present();
    this.paybillService.fetchSavedCards(this.loggedinNumber).then((res) => {
      this.loader.dismiss();
      console.log("response for saved cards in paybill amount" + JSON.stringify(res));
      this.events.publish('savedCards', res);
    //return res;
   
   }).catch(err => {
    console.log("*****saveCards Error*******" + JSON.stringify(err));
    this.loader.dismiss();
    if(err=="No internet connection")
    return;
    this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
        
       
   });
   }

  presentPopover(myEvent) {
    var getNewNumber = this.paybillService.getPhone_BookNumber();
    if (getNewNumber) {
      var checkDoublication = this.allNumber.includes(getNewNumber);
      if (!checkDoublication) {
        this.allNumber.push(getNewNumber);
      }
    }

    let selected = this.selected;
    let allNumber = this.allNumber;
    let popover = this.popoverCtrl.create(ReloadPopoverComponent, { selected, allNumber });
    popover.present({
      ev: myEvent
    });

    popover.onWillDismiss(data => {


      console.log("onWillDismiss+++++++" + data);
      if (data != null) {
        this.selected = data;
        this.paybillService.SetSelectMobileNumber(this.selected);
        var index = this.allNumber.indexOf(this.selected);

        this.accountInfo = this.accountNumber[index];
        this.events.publish('accountNumber', this.accountInfo);
        this.paybillService.setAccountNumber(this.accountInfo);
        console.log("onwillDismiss accountInfo" + this.accountInfo);
        //  this.gatewayService.SetSelectMobileNumber(this.selected,this.accountNumber[index]);

        //this.loader.present();
        this.getDueBillDetails(this.selected, this.accountInfo);
        this.paybillService.SetDueBillAmount(this.totalDueAmount);
        //alert('gowa l reload' +this.selected);
      }
    })
  }

  updatePaybill() {
    //   this.totalAmount= this.paybillService.getAfterPaymentAmount();
    //  this.billingDate= this.paybillService.getAfterPaymentDate();
    alert("ok");

  }
  updates() {
    this.navCtrl.push(AutoBillingPage);
  }

}
