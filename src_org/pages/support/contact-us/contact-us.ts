import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ContactUsHotlinePage } from './contact-us-hotline/contact-us-hotline';
import { ContactUsSocialPage } from './contact-us-social/contact-us-social';
import { GatewayService } from '../../../global/utilService';

/**
 * Generated class for the ContactUsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
})

export class ContactUsPage {
  hotlineTab = ContactUsHotlinePage;
  socialTab = ContactUsSocialPage;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public gatewayService: GatewayService,
    private renderer: Renderer2) {
  }

  // ionViewDidEnter() {
  //   this.gatewayService.resetAllAnalyticData();
  //   this.gatewayService.setAnalyticPageNameProfileSegment("Contact us", this.renderer);
  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactUsPage');
  }

}
