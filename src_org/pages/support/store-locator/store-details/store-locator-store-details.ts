import { Component, Renderer2 } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';
import { GoogleMap, GoogleMapOptions, GoogleMaps, GoogleMapsEvent } from "@ionic-native/google-maps";
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { GatewayService } from '../../../../global/utilService'; 

@Component({
  selector: 'page-store-locator-store-details',
  templateUrl: 'store-locator-store-details.html',
})
export class StoreLocatorStoreDetailsPage {

  serviceDetailsHidden: boolean = true;

  storeInfo: any;
  map: GoogleMap;
  operating_hours_info: string = "0";

  constructor(public modalCtrl: ModalController, 
    public navCtrl: NavController, 
    public navParams: NavParams,
    public launchNavigator: LaunchNavigator,
    private renderer: Renderer2,
    public gatewayService: GatewayService) {
    if (this.navParams.get('storeInfo')) {
      this.storeInfo = this.navParams.get('storeInfo');
    }
  }


  ionViewDidLoad() {
    if (this.navParams.get('storeInfo')) {
      this.storeInfo = this.navParams.get('storeInfo');
    }
    
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Store locator details", this.renderer);
    if (this.navParams.get('storeInfo')) {
      this.storeInfo = this.navParams.get('storeInfo');
    }
    this.loadMap();
  }


  toggleServiceDetails() {
    this.serviceDetailsHidden = !this.serviceDetailsHidden;
  }

  openMap() {
    // this.navCtrl.push(StoreLocatorStoreMapPage);

  }

  loadMap() {


    let mapOptions: GoogleMapOptions = {

      camera: {
        target: {
          lat: this.storeInfo.latitude,
          lng: this.storeInfo.longitude
        },
        tilt: 30,
        zoom: 15,
        bearing: 50
      },
      controls: {
        compass: true,
        myLocationButton: true,
        indoorPicker: true,
        zoom: true,
        mapToolbar: true
      },
      gestures: {
        scroll: true,
        tilt: true,
        rotate: true,
        zoom: true
      },
      preferences: {
        building: false
      }
    };

    this.map = GoogleMaps.create('store-map-modal', mapOptions);

    // Wait the MAP_READY before using any methods.
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('Map is ready!');

        // Now you can use all methods safely.
        this.map.addMarker({
          title: this.storeInfo.store_name,
          icon: 'red',
          position: {
            lat: this.storeInfo.latitude,
            lng: this.storeInfo.longitude
          }
        })
          .then(marker => {
            marker.showInfoWindow();

            marker.on(GoogleMapsEvent.MARKER_CLICK)
              .subscribe(() => {
                console.log(' map marker clicked');
              });
          });

      }).catch(error => {
        console.log(error);
      });
  }

  openNavigator() {
    this.launchNavigator.navigate([this.storeInfo.latitude, this.storeInfo.longitude])
      .then(
      success => console.log('Launched navigator'),
      error => console.log('Error launching navigator', error)
      );
  }


}
