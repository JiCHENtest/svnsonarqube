import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GatewayService } from '../../../../global/utilService';
/**
 * Generated class for the FaqCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-faq-category',
  templateUrl: 'faq-category.html',
})
export class FaqCategoryPage {
  category:any;
  userType:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public gatewayService:GatewayService, private renderer: Renderer2) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqCategoryPage');
  }
  
  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Faq category", this.renderer);
  }

  ionViewWillEnter(){
    this.category=this.navParams.get('category');
    //this.showFAQ();
  }



}
