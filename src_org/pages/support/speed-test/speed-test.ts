import { Component, NgZone, Renderer2 } from '@angular/core';
import { NavController, NavParams, Events, Platform } from 'ionic-angular';
import { GatewayService } from '../../../global/utilService';
import { SpeedTestProvider } from './speed-test-service';
import { AlertService } from '../../../global/alert-service';
declare var cordova;

@Component({
  selector: 'page-speed-test',
  templateUrl: 'speed-test.html',
})
export class SpeedTestPage {

  speedTestOj: any;
  pingSpeed: any;
  downloadSpeed: any;
  uploadSpeed: any;
  disableButton: any;
  buttonText: string;
  status: string;
  isScreenVisible: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public speedTest: SpeedTestProvider, public zone : NgZone,  
    public events: Events, public gatewayService: GatewayService, 
    public platform: Platform, private alertCtrlInstance: AlertService,
    private renderer: Renderer2) {
    var _this = this;
    this.speedTestOj = {
      dataUsed: 0,
      dataUsedVal: 0,
      dataTotal: 50,
      gaugeID:'gauge-container-speed',
      dataUsedUnit:'Mbps',
      speedTest:true
    };
	this.buttonText = "Start Speed Test";
	this.disableButton = false;
 	cordova.plugins.aptus.speedtestStop();
    this.events.subscribe('stoptest',()=>{
      //_this.noInternet();
      _this.buttonText = "Start Speed Test";
      _this.disableButton = false;
      _this.pingSpeed = 0;
      _this.uploadSpeed = 0;
      _this.downloadSpeed = 0;
      var unit = "Mbps";
      var value = 0;
      _this.events.publish("buttonpressed",value, unit);
    });
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Speed test", this.renderer);
  }

  startSpeedTest(){
     this.speedTest.btn_startSpeedtest(this.speedtestCallback, this.speedtestErrorCallback,this);
     this.pingSpeed = 0;
     this.uploadSpeed = 0;
     this.downloadSpeed = 0;
  }


    /*
      Speed Test Call-back :
      - The 'state' parameter is a JSON string containing information about the state of the current speedtest.
    */

     speedtestCallback(state,_this) {
     	var result = JSON.parse(state);
        _this.buttonText = "Speed Test Started";
        _this.disableButton = true;
     	
        _this.zone.run(()=>{
        var speed:any = "";
        
        if (result.status == "download_started") {

          _this.status = "Download Started";
          _this.speedTestOj.dataUsedVal = 0;
          
        } else if (result.status == "download_progress" || result.status == "download_finished") {

            speed = result.speed;
          _this.speedTestOj.dataUsedVal = ((speed * 8) / 1000000);
          
          	var unit = "Mbps";
		  	_this.events.publish("buttonpressed",_this.speedTestOj.dataUsedVal.toFixed(2), unit);
          
          if(typeof speed !='undefined')
          _this.downloadSpeed = ((speed * 8) / 1000000).toFixed(2);
          
          if(result.status == "download_finished") {
          		_this.status = "Download Finished"
          }
          

        } else if (result.status == "upload_started") {
        
            _this.status = "Upload Started";
            _this.speedTestOj.dataUsedVal = 0;
            
        } else if (result.status == "upload_progress" || result.status == "upload_finished") {

            speed = result.speed;

          _this.speedTestOj.dataUsedVal = ((speed * 8) / 1000000);
          
          	var unit = "Mbps";
          	_this.events.publish("buttonpressed",_this.speedTestOj.dataUsedVal.toFixed(2), unit);
          
          if(typeof speed !='undefined')
          _this.uploadSpeed = ((speed * 8) / 1000000).toFixed(2);
          
          if(result.status == "upload_finished"){
          		_this.status = "Upload Finished";
          }

        } else if (result.status == "ping_started") {
        
           //_this.status = "Ping Started";
           setTimeout(function(){
           		var unit = "Mbps";
           		var value = 0;
           		_this.events.publish("buttonpressed",value, unit);
           }, 4000);
           
        } else if (result.status == "ping_progress" || result.status == "ping_finished") {
          _this.pingSpeed = result.time;
        } else if (result.status == "finished") {
            if(result.download){
              _this.downloadSpeed = ((result.download.speed * 8) / 1000000).toFixed(2);
            }
            if(result.upload){
              _this.uploadSpeed = ((result.upload.speed * 8) / 1000000).toFixed(2);
            }
            if(result.ping){
              _this.pingSpeed = result.ping.time;
            }
            setTimeout(function(){
           		var unit = "Mbps";
           		var value = 0;
           		_this.events.publish("buttonpressed",value, unit);
           		_this.disableButton = false;
           		_this.buttonText = "Start Speed Test";
            }, 5000);
        }
      });
    }

      /*
        Speed Test error call-back :
        - The 'state' parameter is a JSON string containing information about the nature of the speed test error.
      */
       speedtestErrorCallback(state) {
       
          var result = JSON.parse(state);
          //alert("Error: " + result.status);
      }


  /*ionViewDidLoad() {
    console.log('ionViewDidLoad SpeedTestPage');
  }*/

}
