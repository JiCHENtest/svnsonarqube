import {Component, Renderer2} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import { DashboardService } from '../../../pages/home/homeService';
import { GatewayService } from '../../../global/utilService';

@Component({
  selector: 'page-device-self-help',
  templateUrl: 'device-self-help.html',
})
export class DeviceSelfHelpPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public dashboardService : DashboardService, public gatewayService: GatewayService,
    private renderer: Renderer2) {
  }

  type: string = "Prepaid";

  ionViewDidLoad() {
    var userInfo = this.dashboardService.GetUserInfo();
    if(userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid") {
        this.type = "Postpaid";
    }
    console.log('ionViewDidLoad DeviceSelfHelpPage');
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Device self help", this.renderer);
  }

}
