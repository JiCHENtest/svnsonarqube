import {Component, ChangeDetectorRef, Renderer2} from "@angular/core";
import {NavController, NavParams, Platform} from "ionic-angular";
import { SpeedTestPage } from "./speed-test/speed-test";
import { FaqPage } from "./faq/faq"
import { FeedbackPage } from "./feedback/feedback"
import { ContactUsPage } from "./contact-us/contact-us"
import { PukPage } from "./puk/puk"
import {StoreLocatorSelectStorePage} from "../support/store-locator/select-store/store-locator-select-store";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { GatewayService } from '../../global/utilService'; 
import { Uid } from '@ionic-native/uid';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import CryptoJS from 'crypto-js';

import { Sim } from '@ionic-native/sim';
/**
 * Generated class for the SupportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-support',
  templateUrl: 'support.html',
})
export class SupportPage {
  isPrePaid:any;
  selfCareUrl:any; 
  isAndroidDevice: boolean = false;
  imei: any;
  isImeiAvailable: boolean = false;
  errorConsole: any;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public inAppBrowser: InAppBrowser, 
    public gatewayService: GatewayService,
    private uid: Uid,
    private androidPermissions: AndroidPermissions,
    public ref:ChangeDetectorRef,
    public sim:Sim,
    private plt: Platform,
    private renderer: Renderer2) {

  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Support", this.renderer);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SupportPage');
    this.isPrePaid= this.gatewayService.isPrePaid();
    if (this.plt.is('android')) {
      // need to fetch IMEI only for android device
      console.log('DeviceSelfHelpPage -> constructor -> detected android device fetching IMEI!');
      this.isAndroidDevice = true;
    }
  }

  goToDeviceSelfHelp(){

    console.log("GET SIM INFO"+JSON.stringify(this.sim.getSimInfo()));

   // this.navCtrl.push( DeviceSelfHelpPage,{} );
   var _this = this;
   if(this.isPrePaid){
    this.selfCareUrl="http://13.54.99.117/SelfHelpPre";
 }else{
     this.selfCareUrl="http://13.54.99.117/SelfHelpPost"
 }  
  if (this.isAndroidDevice === true) {

    this.getImei(_this);
  }else{
   let browser = this.inAppBrowser.create(this.selfCareUrl, '_blank', 'location=no');
   browser.show();
  } 
  
  }

  async getImei(_this) {
    this.ref.detectChanges(); 
    const { hasPermission } = await this.androidPermissions.checkPermission(
      this.androidPermissions.PERMISSION.READ_PHONE_STATE
    );

    if (!hasPermission) {
      
      const result = await this.androidPermissions.requestPermission(
        this.androidPermissions.PERMISSION.READ_PHONE_STATE
      );
       
      if (!result.hasPermission) {
        //throw new Error('Permissions required');
        let browser = this.inAppBrowser.create(this.selfCareUrl, '_blank', 'location=no');
        browser.show();
      }


    }
    try{
      console.log(''+this.uid.IMEI);
    if (this.uid.IMEI) {
      this.imei = this.uid.IMEI;
      this.isImeiAvailable = true;
      CryptoJS.pad.NoPadding = {pad: function(){}, unpad: function(){}}; 
      var key = CryptoJS.enc.Hex.parse("Kipl~C^L-3$C!M}6"); 
      var iv = CryptoJS.enc.Hex.parse("0000000000000000"); 
      var encrypted = CryptoJS.AES.encrypt(this.imei, key, {iv: iv, padding: CryptoJS.pad.NoPadding}); 
      console.log(encrypted.toString()); 
	  
	     if(this.isPrePaid){
		       this.selfCareUrl = "http://13.54.99.117/SelfHelpPre_New/Devices/IMEIENC?imei=" + encrypted;

 }else{
	       this.selfCareUrl = "http://13.54.99.117/SelfHelpPost_New/Devices/IMEIENC?imei=" + encrypted;
 } 
      let browser = this.inAppBrowser.create(this.selfCareUrl, '_blank', 'location=no');
      browser.show();
    }else{
      let browser = this.inAppBrowser.create(this.selfCareUrl, '_blank', 'location=no');
      browser.show();
    }
  }catch(error){
    this.errorConsole = "Error "+error; 
  }
    return this.uid.IMEI
  }


  goToSpeedTest(){
    this.navCtrl.push( SpeedTestPage,{} );

  }
  goToFaq(){
    this.navCtrl.push( FaqPage,{} );

  }
  goToFeedback(){
    this.navCtrl.push( FeedbackPage,{} );

  }
  goContactUs(){
    this.navCtrl.push( ContactUsPage,{} );

  }
  goToPuk(){
    this.navCtrl.push( PukPage,{} );

  }

  goToStoreLocator() {
    this.navCtrl.push( StoreLocatorSelectStorePage,{} );

  }
}
