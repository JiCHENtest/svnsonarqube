import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

// service
import { DashboardService } from '../../pages/home/homeService';
import { GatewayService } from '../../global/utilService';
import { ConstantData } from '../../global/constantService';
import { HandleError } from '../../global/handleerror';
import { AlertService } from '../../global/alert-service';
import { ConfirmationScreensMydealsPage } from "../../pages/confirmation-screens-mydeals/confirmation-screens-mydeals";

/**
 * Generated class for the MyDealsModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-deals-modal',
  templateUrl: 'my-deals-modal.html',
})
export class MyDealsModalPage {

  ischecked: boolean = false;
  checkboxFlag: any;
  
  mydealsInfo : any;

  mobileNumber : any;

  constructor(
      public viewCtrl: ViewController,
      public navCtrl: NavController,
      public navParams: NavParams,
      public dashboardService : DashboardService,
      public gatewayService:GatewayService, 
      public handleerror: HandleError, 
      private alertCtrlInstance: AlertService,
      private renderer: Renderer2
    ) {

      this.mydealsInfo = this.navParams.get("mydealsInfo");
      console.log("this.mydealsInfo planDescription1 ",this.mydealsInfo.planDescription1);
      console.log("this.mydealsInfo costPerOffer ",this.mydealsInfo.costPerOffer);
      
      this.mobileNumber = this.gatewayService.GetMobileNumber()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyDealsModalPage');
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("My deals modal", this.renderer);
  }

  enableLogin() {

    if (this.checkboxFlag == "") {
      this.ischecked = false;
    }
    else {
      this.ischecked = true;
    }
  }

  buyMyDeals() {
    this.viewCtrl.dismiss();
    this.navCtrl.push(ConfirmationScreensMydealsPage,{"mydealPlanInfo":this.mydealsInfo});    
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
