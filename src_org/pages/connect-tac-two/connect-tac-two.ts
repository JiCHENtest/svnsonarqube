import { Component, ElementRef, ViewChild , NgZone} from "@angular/core";
import { NavController, NavParams, Platform } from "ionic-angular";
import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { LocalStorage } from '../../global/localStorage';
import { Keyboard } from '@ionic-native/keyboard';
// import { MobileConnectPage } from "../mobile-connect/mobile-connect";
// import { MobileConnectAccountPage } from "../mobile-connect/account-setup/account-setup";
// import { NetworkLoginPage } from "../network-login/network-login";


// oleole service
import { OleoleService } from '../../global/oleole-service';
import { GatewayService } from '../../global/utilService';
import { ConstantData } from '../../global/constantService';
import { AlertService } from '../../global/alert-service';
import { DashboardService } from '../home/homeService';

// page
import { OleoleShopPage } from "../services/oleole-shop/oleole-shop";

/**
 * Generated class for the ConnectTacTwoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
// declare for sms plugin 
declare var SMS: any;

@Component({
  selector: 'page-connect-tac-two',
  templateUrl: 'connect-tac-two.html',
})
export class ConnectTacTwoPage {

  isenabled: boolean = false;
  otpInput1: any;
  otpInput2: any;
  otpInput3: any;
  otpInput4: any;

  @ViewChild('focus2')
  focus2: ElementRef;

  @ViewChild('focus1')
  focus1: ElementRef;

  @ViewChild('focus3')
  focus3: ElementRef;

  @ViewChild('focus3')
  focus4: ElementRef;

  @ViewChild('connectSubmit')
  connectSubmit:ElementRef;

  loader:any; // loader

  // oleole
  isOleole : boolean = false;
  oleoleGiftParam : any;
  registerState : any;

  incorrectOTPCount = 0;
  resendCount = 0;
  alertResendOTP:any;
  otpStartTimer: any;
  otpInput: any;

  mobileNumber : any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public oleoleService : OleoleService,
    public gatewayService: GatewayService,
    private alertCtrlInstance: AlertService,
    public dashboardService: DashboardService,
    private toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public localStorage: LocalStorage,
    public platform: Platform,
    public zone: NgZone,
    private keyboard: Keyboard
  ) {

     this.mobileNumber  =  this.gatewayService.GetMobileNumber();

      console.log("ConnectTacTwoPage -> constructor() -> oleoleBuyFor ",this.navParams.get('oleoleBuyFor'));

      this.oleoleGiftParam = this.navParams.get('oleoleBuyFor');
      this.isOleole = this.navParams.get('oleoleBuyFor') != undefined ?  true: false;

      document.addEventListener('resume', () => {
        if(this.resendCount>4){
            var start_time = this.localStorage.get("otpStartTimer", '');
            var current_time:any = new Date().getTime();

            console.log(start_time);
            console.log(current_time);
            console.log(current_time-start_time);

          if(start_time == '' || start_time==undefined)
          {

          }
          else{
            if(((current_time - start_time) / 1000) > (10*60)){
              console.log('time up');
            
              this.alertResendOTP.dismiss();
              this.localStorage.set("otpStartTimer", '');
              this.resendCount = 0;
              //this.counter = 2*60;
              
            }
          }
        }
      });
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad ConnectTacTwoPage');

    this.platform.ready().then((readySource) => {

      if (SMS) SMS.startWatch(() => {
        console.log('watching started');
      }, Error => {
        console.log('failed to start watching');
      });

      document.addEventListener('onSMSArrive', (e: any) => {
        var sms = e.data.body;
        //var OTP=sms.substring(33,40);

        var OTP = sms.split("(OTP) is ");
        console.log("OTP is ::" + OTP[1]);
        this.zone.run(() => {
          this.otpInput = OTP[1];
          this.otpInput1 = this.otpInput.substring(0, 1);
          console.log("i/p1 :" + this.otpInput1);
          this.otpInput2 = this.otpInput.substring(1, 2);
          console.log("i/p2 :" + this.otpInput2);
          this.otpInput3 = this.otpInput.substring(2, 3);
          console.log("i/p3 :" + this.otpInput3);
          this.otpInput4 = this.otpInput.substring(3, 4);
          console.log("i/p4 :" + this.otpInput4);

          if (this.otpInput1 != "" && this.otpInput2 != "" && this.otpInput3 != "" && this.otpInput4 != "") {
            this.isenabled = true;
          }
        });


      });

    });
  }

  goBack() {
    // this.navCtrl.setRoot(MobileConnectPage);   
    console.log("ConnectTacTwoPage -> goBack() -> navCtrl.pop() ");
    this.navCtrl.pop();    
  }

  enableHome1(el) {

    if (this.otpInput1 == "") {
      this.isenabled = false;
    }
    else {
      //this.isenabled = true;
      el.focusNext();

    }

  }

  enableHome2(el, focus1) {

    if (this.otpInput2 == "") {
      this.isenabled = false;
      //focus1._native.nativeElement.focus();
      //focus1.setFocus();

      setTimeout(() => {
        focus1.setFocus();
      }, 50);
    }
    else {
      //this.isenabled = true;
      el.focusNext();

      //setTimeout(() => {
      // el.focusNext();

      // },150);
    }

  }

  enableHome3(el, focus2) {

    if (this.otpInput3 == "") {
      this.isenabled = false;


      setTimeout(() => {
        focus2._native.nativeElement.focus();
      }, 50);
    }
    else {
      //this.isenabled = true;
      el.focusNext();

    }

  }

  enableHome4(el, connectSubmit,focus3) {

    if (this.otpInput1 == "" || this.otpInput1==undefined|| this.otpInput2 == "" || this.otpInput2==undefined|| this.otpInput3 == "" || this.otpInput3==undefined|| this.otpInput4 == "" || this.otpInput4==undefined) {
      this.isenabled = false;
      // el.focusNext();
      focus3._native.nativeElement.focus();
     // focus3.setFocus();
      //focus3._native.nativeElement.focus();
    }
    else {
      this.isenabled = true;

    }

    // if (this.otpInput4 == "") {
    //   this.isenabled = false;

    //   setTimeout(() => {
    //     el.focusNext();
    //   }, 50);

    // }
    // else {
    //   this.isenabled = true;

    // }

  }

  handlePin(e,elementIndex,nextElement,prevElement){
    if(e.which === 13){
      //in case of enter button
      this.keyboard.close();
    }else if (e.which === 8){
      //in case of delete button need to move backward
      if(elementIndex!=1){
        prevElement._native.nativeElement.focus();
      }
    }else{
       //for all other buttons need to move forwards
       if(elementIndex!=4){
        nextElement._native.nativeElement.focus();
       }
    }

    if (this.otpInput1 == "" || this.otpInput1==undefined || this.otpInput2 == "" || this.otpInput2==undefined || this.otpInput3 == "" || this.otpInput3==undefined || this.otpInput4 == "" || this.otpInput4==undefined) {
      this.isenabled = false;
    }else{
      this.isenabled = true;
    }  
    
  
  }

  //incase of scucess
  gotoNetworkLogin() {
    // this.navCtrl.setRoot(NetworkLoginPage);
    var _that = this;
    _that.loader = _that.gatewayService.LoaderService();
    var a = false;
    // if(a) {
    if(ConstantData.isLoginBypassed==true) {
    
      _that.loader.dismiss();
      console.log("ConnectTacTwoPage -> gotoNetworkLogin() -> validateMobileConnectUser() -> oleoleGiftParam-(isLoginBypassed == true) = ",_that.oleoleGiftParam);
      _that.oleoleService.callGiftAdapter(_that.oleoleGiftParam).then((result) => {
        _that.loader.dismiss();
        console.log("gotoNetworkLogin send gift oleole success ",JSON.stringify(result));
        
        // _that.navCtrl.push(OleoleShopPage);
        // giftDetails
        var oleoleGiftDetails = _that.navParams.get('giftDetails');

        console.log("ConnectTacTwoPage -> gotoNetworkLogin() -> validateMobileConnectUser() -> oleoleGiftDetails ",oleoleGiftDetails);

        console.log("ConnectTacTwoPage -> gotoNetworkLogin() -> validateMobileConnectUser() -> mobileNumber ",_that.mobileNumber);
        _that.mobileNumber = _that.oleoleGiftParam.bnumber;

        console.log("ConnectTacTwoPage -> gotoNetworkLogin() -> validateMobileConnectUser() -> _that.oleoleGiftParam.bnumber 1 ",_that.mobileNumber);
        _that.oleoleService.alertShow(result,oleoleGiftDetails, _that.mobileNumber,'sending').then(resInfo => {
            console.log("ConnectTacTwoPage -> gotoNetworkLogin() -> validateMobileConnectUser() -> alertShow() ",resInfo);
            // if(resInfo) {
              // _that.navCtrl.popToRoot();
            // }
            // this.navCtrl.popToRoot();
            console.log("this.navCtrl.length() for connect two ",_that.navCtrl.length());
            console.log("this.navCtrl.getByIndex(this.navCtrl.length()-(1))  for connect two ",_that.navCtrl.getByIndex(_that.navCtrl.length()-(1)));
            _that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length()-(4+1)));
        });
        
        // goto mobile OTP page
        // _that.navCtrl.setRoot(ConnectTacTwoPage,{"oleoleBuyFor":params});            

      },(err)=>{
        _that.loader.dismiss();
        console.log("gotoNetworkLogin send gift oleole error ",err);
      });
    }
    else {
      if (this.incorrectOTPCount < 5) {
        
   

        _that.loader.present().then(() => {
           //code for calling register()from adapter
          _that.validateMobileConnectUser().then(function (res) {
            //console.log(res);
            console.log("ConnectTacTwoPage -> gotoNetworkLogin() -> validateMobileConnectUser() -> isOleole = ",_that.isOleole);
              try {

                
                // for oleole gift
                if(_that.isOleole && res["code"] == 0) {
                  _that.loader.dismiss();
                  console.log("ConnectTacTwoPage -> gotoNetworkLogin() -> validateMobileConnectUser() -> oleoleGiftParam = ",_that.oleoleGiftParam);
              
                  _that.oleoleService.callGiftAdapter(_that.oleoleGiftParam).then((result) => {
                    _that.loader.dismiss();
                    console.log("gotoNetworkLogin send gift oleole success ",JSON.stringify(result));
                    
                    // _that.navCtrl.push(OleoleShopPage);
                    // giftDetails
                    var oleoleGiftDetails = _that.navParams.get('giftDetails');

                    console.log("ConnectTacTwoPage -> gotoNetworkLogin() -> validateMobileConnectUser() -> oleoleGiftDetails ",oleoleGiftDetails);

                    console.log("ConnectTacTwoPage -> gotoNetworkLogin() -> validateMobileConnectUser() -> mobileNumber ",_that.mobileNumber);


                    _that.mobileNumber = _that.oleoleGiftParam.bnumber;

                    console.log("ConnectTacTwoPage -> gotoNetworkLogin() -> validateMobileConnectUser() -> _that.oleoleGiftParam.bnumber 2 ",_that.mobileNumber);
                    _that.oleoleService.alertShow(result,oleoleGiftDetails, _that.mobileNumber, 'sending').then(resInfo => {
                        console.log("ConnectTacTwoPage -> gotoNetworkLogin() -> validateMobileConnectUser() -> alertShow() ",resInfo);
                        // if(resInfo) {
                          // _that.navCtrl.popToRoot();
                        // }
                        // this.navCtrl.popToRoot();
                        console.log("this.navCtrl.length() for connect two ",_that.navCtrl.length());
                        console.log("this.navCtrl.getByIndex(this.navCtrl.length()-(1))  for connect two ",_that.navCtrl.getByIndex(_that.navCtrl.length()-(1)));
                        _that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length()-(4+1)));
                    });
                    
                    // goto mobile OTP page
                    // _that.navCtrl.setRoot(ConnectTacTwoPage,{"oleoleBuyFor":params});            

                  },(err)=>{
                    _that.loader.dismiss();
                    console.log("gotoNetworkLogin send gift oleole error ",err);
                  });
                } // oleoel gift else condition
                else if(_that.isOleole ) {
                  _that.loader.dismiss();
                  _that.incorrectOTPCount++;
                  //var data = { "title": "Uh oh, invalid OTP", "subtitle": "", "message": "Did you enter the wrong number?" };
                  _that.alertCtrlInstance.showAlert("Uh Oh. That's not a valid OTP", "Did you enter the wrong number? Please try again later.", "OK");
                  //_that.gatewayService.presentAlert(data);
                }
              }
              catch(e) {
                console.log("ConnectTacTwoPage -> gotoNetworkLogin() -> validateMobileConnectUser() -> err ",e);
              }

          }).catch(function (e) {
            console.log("component  failure" + e);
            console.log("Error" + e);
            _that.loader.dismiss();
            if(e=="No internet connection")
            return;
          
            _that.alertCtrlInstance.showAlert("Uh Oh. Can't seem to receive OTP", "Please try again later", "OK");
            
          });

        });
       
      }
      else {
        // let alert_incorrect = this.alertCtrl.create({
        //   title: "Uh Oh. That's Too Many Login Attempts",
        //   subTitle: "Uh oh. You've entered the incorrect PIN too many times. Please exit and try again later.",
        //   buttons: [],
        //   cssClass: 'error-message',
        //   enableBackdropDismiss: false
        //   // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
        // });
        // alert_incorrect.present();

        _that.loader.dismiss();

        let alert = this.alertCtrl.create({
          title: "Uh Oh. That's Too Many Login Attempts",
          subTitle: "You've entered the incorrect PIN too many times. Please exit and try again later.",
          buttons: [
          {
            text:"Ok",
            role:"Ok",
            cssClass:'error-button-float-right submit-button',
            handler: () => {
              console.log('Call to Action Clicked');
              // _that.navCtrl.setRoot(MobileConnectPage);
            }
          }],
          cssClass:'success-message error-message',
          enableBackdropDismiss: false
        // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
        });
        alert.present();
      }
    }
   
  }

  resendOTP() {
    var _that = this;
    // Do your operations
    //var currentResendDate   = new Date();
    //var seconds = (currentResendDate.getTime() - this.firstResendTime.getTime()) / 1000;
    console.log('Resend count ' + this.resendCount);
    //console.log(seconds);
    if (this.resendCount > 4) {

      //show max resend attemp msg
      this.alertResendOTP = this.alertCtrl.create({
        title: 'Uh oh',
        subTitle: 'Resend OTP requests limited to 5 attempts. Please try again in 10 minutes.',
        buttons: [],
        cssClass: 'success-message error-message',
        enableBackdropDismiss: false
        // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
      });
      this.alertResendOTP.present();
      this.otpStartTimer = this.localStorage.set("otpStartTimer", new Date().getTime());
      setTimeout(
        ()=>{
          this.alertResendOTP.dismiss();
          this.resendCount = 0;
          this.localStorage.set("otpStartTimer", '');
        }
              
        ,600000);
      // this.myTimer = Observable.timer(0, 1000)
      //   .take(this.counter)
      //   .map(() => {
      //     console.log(this.counter);
      //     if (this.counter == 1) {
      //       this.alertResendOTP.dismiss();
      //       this.resendCount = 0;
      //       this.counter = 2*60;
      //     } else {
      //       --this.counter;
      //     }
      //   }
      //   );
      // this.myTimer.subscribe();
      //_that.alertCtrlInstance.showAlert("", "", ["Try again", "Cancel"]);


    } else {
      this.sendOTP();
    }
  }
  sendOTP() {
        
    let toast = this.toastCtrl.create({
      message: 'OTP successfully sent',
      duration: 3000,
      cssClass: 'toast',
      position: 'top',
      
    });
    let Errortoast = this.toastCtrl.create({
      message: 'Unable to send OTP. Please retry!',
      duration: 3000,
      cssClass: 'toast',
      position: 'top'
    });
    var _that = this;
    this.resendCount++;
    //this.firstResendTime = new Date();
    //console.log('First date ' + this.firstResendTime);

    var loader = _that.gatewayService.LoaderService();
    loader.present();
    //code for calling register()from adapter
    _that.registerMobileConnectUser().then((res) => {
      //alert("register function response"+JSON.stringify(res));
      
      // latter
      // _that.mConnectService.SetMConnectInfo(res);

      loader.dismiss();
      toast.present();
    }).catch(function (e) {
      console.log("component  failure" + e);
      loader.dismiss();
      if(e=="No internet connection")
      return;
      Errortoast.present();
      //alert("Error"+e);

    });
  }

  validateMobileConnectUser() {
    //var _that = this;
    // this.state = this.navParams.get('data'); 
    // var mConnectData = this.mConnectService.GetMConnectInfo();
    var state = '';

    if(this.isOleole ) {
      this.registerState = this.navParams.get('register_state');
      state = this.registerState.state;
      console.log("validateMobileConnectUser() -> registerState = ",this.registerState);
      console.log("validateMobileConnectUser() -> isOleole = ",this.isOleole);
      console.log("validateMobileConnectUser() -> state = ",state);
    }
    else {
      // console.log("validateMobileConnectUser() -> else state ");
      // state = mConnectData["state"];
      // console.log("validateMobileConnectUser() -> else state = ",state);
    }

    var otp = this.otpInput1.toString() + this.otpInput2.toString() + this.otpInput3.toString() + this.otpInput4.toString();
    console.log(otp);
    console.log(state);
    var params_validate = { "state": state, "OTP": otp };

    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.validateMobileConnect2, "POST", params_validate).then((res_validate) => {
        console.log("validate user response : " + JSON.stringify(res_validate));
        resolve(res_validate);

        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          console.log("******************************************** component failure validateMobileConnectUser" + e);
          //alert("error in validating MobileConnectUser");   
          reject(e);
        });

    });


  }


  registerMobileConnectUser() {
    var _that = this;

    var msisdn = this.gatewayService.GetMobileNumber();
    var params_register = { "MSISDN": msisdn };

    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.registerMobileConnect2, "POST", params_register).then((res_register) => {
        console.log("register user response : " + JSON.stringify(res_register));
        resolve(res_register);

        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          console.log("******************************************** component failure registerMobileConnectUser" + e);
		  _that.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", ["OK"]);
          //alert("error register MobileConnectUser");   
          reject(e);
        });

    });


  }

  //incase of failure
  setupMobileConnectAccount() {
    // predefined code IBM
    // this.navCtrl.setRoot(MobileConnectAccountPage);
  }
}
