import { Component, Input } from '@angular/core';
import { LoadingController} from "ionic-angular";
import * as Raphael from "raphael";
import { GlobalVars } from "../../providers/globalVars";
import { isNumber } from 'ionic-angular/util/util';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { DetailedUsagePage } from "../../pages/detailed-usage/detailed-usage";
import { GatewayService } from '../../global/utilService';
/**
 * Generated class for the RaphaelDrawerComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'raphael-drawer',
  templateUrl: 'raphael-drawer.html',
  inputs: ['value']
})
export class RaphaelDrawerComponent {
  @Input("value") value;
  screenWidth: number = window.innerWidth;
  screenHeight: number = window.innerHeight;
  gaugeWidth: number;
  gaugeHeight: number;
  gaugeRadius: number;
  gaugeRadius10: number;
  borderWidth:number = 10;

  title: string;
  message: string;;

  dataUsed: any;
  dataUsedVal: any;
  unlimited:any;
  dataTotal: any;
  qouta: any;
  dataUsedUnit: any;
  dataTotalUnit: any;
  gaugeID: string;
  selectedTab: number;
  jsonString: string;
  capZone: boolean = false;
  callbarred: boolean = false;
  speedTest: boolean = false;
  capZoneCss: any = this.globalVar.getCapZoneStatus();
  callbarredCss: any = this.globalVar.getcallBarredStatus();
  speedTestList = [
    { min: 0, max: 1 }, { min: 1, max: 2 }, { min: 2, max: 3 }, { min: 3, max: 5 }, { min: 5, max: 10 }, { min: 10, max: 20 }, { min: 20, max: 30 }, { min: 30, max: 50 }
  ];

  constructor(public globalVar: GlobalVars, public navCtrl: NavController, public loadingCtrl: LoadingController, public gatewayService:GatewayService) {
      //alert("Constructor of component");
      gatewayService.registerObserver(this);   
  }

  update(){

  }
  callingMethod(){

  }

  updateDashboard(){

  }
  updateLifestyle(){
  }
updateDial(){
  //alert("value"+JSON.stringify(this.value));
  //@Input("value")this.value;
  this.screenWidth = window.innerWidth;
  this.screenHeight = window.innerHeight;
  this.gaugeWidth = 0;
  this.gaugeHeight = 0;
  this.gaugeRadius = 0;
  this.gaugeRadius10 = 0;

  this.title = '';
  this.message ='';

  this.dataUsed = 0;
  this.dataUsedVal= 0;
  this.dataTotal = 0;
  this.qouta = null;
  this.dataUsedUnit = null;
  this.dataTotalUnit = null;
  this.gaugeID = ''; 
  this.selectedTab = 0;
  this.jsonString = '';
  this.capZone = this.globalVar.getCapZoneStatus();
  this.callbarred = this.globalVar.getcallBarredStatus();
  this.speedTest = false;
  
  this.capZoneCss = this.globalVar.getCapZoneStatus();
  this.callbarredCss = this.globalVar.getcallBarredStatus();
  this.speedTestList = [
    { min: 0, max: 1 }, { min: 1, max: 2 }, { min: 2, max: 3 }, { min: 3, max: 5 }, { min: 5, max: 10 }, { min: 10, max: 20 }, { min: 20, max: 30 }, { min: 30, max: 50 }
  ];

    if (this.value.dataUsed) {
      this.dataUsed = this.value.dataUsed;
    }
    if (this.value.dataUsedVal || this.value.dataUsedVal == 0 ) {

      if (isNumber(this.value.dataUsedVal)) {
        this.dataUsedVal = Math.round(this.value.dataUsedVal * 10) / 10;
      } else {
        this.dataUsedVal = this.value.dataUsedVal;
      }
    }
    else{
      this.unlimited = true;
    }
    if (this.value.dataTotal) {
      if (isNumber(this.value.dataTotal)) {
        this.dataTotal = Math.round(this.value.dataTotal * 10) / 10;
      } else {
        this.dataTotal = this.value.dataTotal;
      }
    }
    if (this.value.capZone) {
      this.capZone = this.value.capZone;
    }
    if (this.value.speedTest) {
      this.speedTest = this.value.speedTest;
    }
    if (this.value.callbarred) {
      this.callbarred = this.value.callbarred;
    }
    if (this.value.title) {
      this.title = this.value.title;

    }
    if (this.value.message) {
      this.message = this.value.message;

    }
    if (this.value.gaugeID) {
      this.gaugeID = this.value.gaugeID;
    }
    if (this.value.dataUsedUnit) {
      this.dataUsedUnit = this.value.dataUsedUnit;
    }
    if (this.value.qouta) {
      this.qouta = this.value.qouta;
    }
    if (this.value.dataTotalUnit) {
      this.dataTotalUnit = this.value.dataTotalUnit;
    }

    if (this.value.tabNumber) {
      this.selectedTab = this.value.tabNumber;
    }

    this.jsonString = JSON.stringify(this.value);
    this.initialize();
}

  ngOnInit() {
    if (this.value.dataUsed) {
      this.dataUsed = this.value.dataUsed;
    }
    if (this.value.dataUsedVal || this.value.dataUsedVal == 0 ) {
      
      if (isNumber(this.value.dataUsedVal)) {
        this.dataUsedVal = Math.round(this.value.dataUsedVal * 10) / 10;
      } else {
        this.dataUsedVal = this.value.dataUsedVal;
      }
    }
    if (this.value.dataTotal) {
      this.dataTotal = this.value.dataTotal;
    }

    if (this.value.dataTotal || this.value.dataTotal == 0 ) {
      if (isNumber(this.value.dataTotal)) {
        this.dataTotal = Math.round(this.value.dataTotal * 10) / 10;
      } else {
        this.dataTotal = this.value.dataTotal;
      }
    }
    console.log("Dial Logs "+JSON.stringify(this.value));
    if(this.value.desc == 'Unlimited'){
      this.unlimited = true;
      console.log("Dial Logs true"+JSON.stringify(this.value));
    }else{
      this.unlimited = false;
      console.log("Dial Logs false"+JSON.stringify(this.value));
    }
    if (this.value.capZone) {
      this.capZone = this.value.capZone;
    }
    if (this.value.speedTest) {
      this.speedTest = this.value.speedTest;
    }
    if (this.value.callbarred) {
      this.callbarred = this.value.callbarred;
    }
    if (this.value.title) {
      this.title = this.value.title;

    }
    if (this.value.message) {
      this.message = this.value.message;

    }
    if (this.value.gaugeID) {
      this.gaugeID = this.value.gaugeID;
    }
    if (this.value.dataUsedUnit) {
      this.dataUsedUnit = this.value.dataUsedUnit;
    }
    if (this.value.qouta) {
      this.qouta = this.value.qouta;
    }
    if (this.value.dataTotalUnit) {
      this.dataTotalUnit = this.value.dataTotalUnit;
    }

    if (this.value.tabNumber) {
      this.selectedTab = this.value.tabNumber;
    }

    this.jsonString = JSON.stringify(this.value);
  }
  ngAfterViewInit() {
    this.initialize();
  }
  ngAfterContentChecked() {

    // setTimeout(()=>{
    //   alert("ThisValue=>"+this.value);
    // },5000);


  }
  
  updateValue(value, total, R, sec, time) {

      //  how to fill gauge for UI animation
      //  1- call function updateVal(value,total,R,hand,id,time)
      //  2- value  must be between 0 and 72   72 => full 0 => empty So developer need to calculate this value
      //  3- if 4.45 GB  consumed of 5 GB equation will be as follow
      //  4- 4.45 * 100 / 5 = 89
      //  5- 89 * 0.72  = 64.08    value = 64.08

    var capZoneActive = this.globalVar.getCapZoneStatus();
    var callBarredActive = this.globalVar.getcallBarredStatus();
    var capZone = this.capZone;
    var callbarred = this.callbarred;
    var borderWidth = this.borderWidth;
    var speedTest = this.speedTest;
    var unlimited = this.unlimited;
	var XPAXActive = this.globalVar.getXPAXTheme();	
    var newValue;

      if(unlimited){
        newValue = (total * 100 / total) * 0.72;
      }
      else{
        newValue = (value * 100 / total) * 0.72;
      }

      if (capZoneActive) {
        if (capZone) {
        	if(!speedTest){
          		sec.strokeLinearGradient("capZoneBG", borderWidth);
          	}
        }
        else {
        	if(!speedTest){
          		sec.strokeLinearGradient("capZone", borderWidth);
          	}
        }
      }
      else if (callBarredActive) {
        if(callbarred){
        	if(!speedTest){
          		sec.strokeLinearGradient("callBarred", borderWidth);
          	}
        }
        else{
        	if(!speedTest){
          		sec.strokeLinearGradient("callBarredBG", borderWidth);
          	}
        }
      }
      else if (XPAXActive) {
        if(!speedTest){
          sec.strokeLinearGradient("XpaxGrad", borderWidth);
        }
      }
      else {
        if(!speedTest){
          sec.strokeLinearGradient("celcomGrad", borderWidth);
        }
      }

      sec.animate({ arc: [newValue, 100, R] }, time, "easeInOut");

	}
  
  initialize() {
    //adding some features to raphael to apply gradient to stroke
    this.initRaphael();
    if(this.screenWidth > 700){
      this.screenWidth = window.innerWidth - 300;
      this.borderWidth = 20;
    }
    else{
      this.screenWidth = window.innerWidth - 100;
    }


    this.gaugeWidth = this.screenWidth;
    this.gaugeHeight = this.screenWidth;
    this.gaugeRadius = this.gaugeWidth / 2;
    this.gaugeRadius10 = this.gaugeRadius - 10;




    var XPAXActive = this.globalVar.getXPAXTheme();


    var capZoneActive = this.globalVar.getCapZoneStatus();
    var callBarredActive = this.globalVar.getcallBarredStatus();
    var capZone = this.capZone;
    var callbarred = this.callbarred;
    var borderWidth = this.borderWidth;
    var speedTest = this.speedTest;
    

    var unlimited = this.unlimited;

    // draw animate gouge  UI  Code

    if (this.value) {
      var r = Raphael(this.gaugeID, this.gaugeWidth, this.gaugeHeight);
    }

    //default configuration
    var R = this.gaugeRadius10; // raduis

    var GR = this.gaugeRadius;

    var param = { "stroke-width": 10, "stroke-linecap": "round" };

    // Custom Attribute
    r.customAttributes.arc = function (value, total, R) {
      var alpha = 360 / total * value,
        a = (90 - alpha) * Math.PI / 180,
        x = GR + R * Math.cos(a), // start point to x axis start draw from  130 half 260 total width of svg
        y = GR - R * Math.sin(a), // start point to y axis start draw from  130 half 260 total height of svg
        path;

      path = [["M", GR, GR - R], ["A", R, R, 0, +(alpha > 180), 1, x, y]];

      return { path: path };
    };

    r.defineLinearGradient("celcomGrad", [
      {
        "id": "s1",
        "offset": "0%",
        "style": "stop-color:rgb(0,173,179);stop-opacity:1"
      },
      {
        "id": "s2",
        "offset": "100%",
        "style": "stop-color:rgb(25,155,219);stop-opacity:1"
      }]);

    r.defineLinearGradient("XpaxGrad", [
      {
        "id": "s1",
        "offset": "0%",
        "style": "stop-color:rgb(25,155,219);stop-opacity:1"
      },
      {
        "id": "s2",
        "offset": "100%",
        "style": "stop-color:rgb(215,0,166);stop-opacity:1"
      }]);

    r.defineLinearGradient("capZone", [
      {
        "id": "s1",
        "offset": "0%",
        "style": "stop-color:rgb(255,112,0);stop-opacity:1"
      },
      {
        "id": "s2",
        "offset": "100%",
        "style": "stop-color:rgb(254,187,27);stop-opacity:1"
      }]);
    r.defineLinearGradient("capZoneBG", [
      {
        "id": "s1",
        "offset": "0%",
        "style": "stop-color:rgb(255,112,0);stop-opacity:.5"
      },
      {
        "id": "s2",
        "offset": "100%",
        "style": "stop-color:rgb(254,187,27);stop-opacity:.5"
      }]);

    r.defineLinearGradient("callBarred", [
      {
        "id": "s1",
        "offset": "0%",
        "style": "stop-color:rgb(239,23,21);stop-opacity:.5"
      },
      {
        "id": "s2",
        "offset": "100%",
        "style": "stop-color:rgb(239,23,21);stop-opacity:.5"
      }]);
      r.defineLinearGradient("callBarredBG", [
        {
          "id": "s1",
          "offset": "0%",
          "style": "stop-color:rgb(170,170,170);stop-opacity:.5"
        },
        {
          "id": "s2",
          "offset": "100%",
          "style": "stop-color:rgb(170,170,170);stop-opacity:.5"
        }]);

    //First path in SVG draw the gauge bg
    var bg = r.path().attr(param).attr({ arc: [72, 100, R] });
    bg.attr({
      'stroke': '#251e67',
      'fill': 'none',
      'stroke-width': borderWidth ,
      'stroke-opacity': 0.1,
      'stroke-linecap': 'round',
      'stroke-linejoin': 'round'
    });
    var sec;
    if (this.value.speedTest) {
      //Second path in SVG draw the gauge fill
      sec = r.path().attr(param).attr({ arc: [0, 100, R] });
      if(XPAXActive){
        sec.attr({
          'stroke': '#D1209B',
          'fill': 'none',
          'stroke-width': borderWidth ,
          'stroke-opacity': 1,
          'stroke-linecap': 'round',
          'stroke-linejoin': 'round'
        });
      }
      else{
        sec.attr({
          'stroke': '#009ade',
          'fill': 'none',
          'stroke-width': borderWidth ,
          'stroke-opacity': 1,
          'stroke-linecap': 'round',
          'stroke-linejoin': 'round'
        });
      }
      
    } else {
      //Second path in SVG draw the gauge fill
      sec = r.path().attr(param).attr({ arc: [72, 100, R] });
      sec.attr({
        'stroke': '#009ade',
        'fill': 'none',
        'stroke-width': borderWidth ,
        'stroke-opacity': 1,
        'stroke-linecap': 'round',
        'stroke-linejoin': 'round'
      });
    }



    function updateVal(value, total, R, hand, time) {

      //  how to fill gauge for UI animation
      //  1- call function updateVal(value,total,R,hand,id,time)
      //  2- value  must be between 0 and 72   72 => full 0 => empty So developer need to calculate this value
      //  3- if 4.45 GB  consumed of 5 GB equation will be as follow
      //  4- 4.45 * 100 / 5 = 89
      //  5- 89 * 0.72  = 64.08    value = 64.08

      var newValue;

      if(unlimited){
        newValue = (total * 100 / total) * 0.72;
      }
      else{
        newValue = (value * 100 / total) * 0.72;
      }
      

      if (capZoneActive) {
        if (capZone) {
        	if (!speedTest) {
          		sec.strokeLinearGradient("capZoneBG", borderWidth);
          	}
        }
        else {
        	if (!speedTest) {
          		sec.strokeLinearGradient("capZone", borderWidth);
          	}
        }
      }
      else if (callBarredActive) {
        if(callbarred){
        	if (!speedTest) {
          		sec.strokeLinearGradient("callBarred", borderWidth);
          	}
        }
        else{
        	if (!speedTest) {
          		sec.strokeLinearGradient("callBarredBG", borderWidth);
          	}
        }
      }
      else if (XPAXActive) {
        if(!speedTest){
          sec.strokeLinearGradient("XpaxGrad", borderWidth);
        }
      }
      else {
        if(!speedTest){
          sec.strokeLinearGradient("celcomGrad", borderWidth);
        }
      }

      hand.animate({ arc: [newValue, 100, R] }, time, "easeInOut");

    }

    if (this.speedTest) {
      var portion = 0;
      if (this.dataUsedVal > 50) {
        updateVal(50, 50, R, sec, 300);
      }
      for (var val of this.speedTestList) {
        portion++;
        if (this.dataUsedVal <= val.max && this.dataUsedVal >= val.min) {
          var newVal = (((this.dataUsedVal - val.min) / (val.max - val.min)) * 6.25) + ((portion - 1) * 6.25);
          updateVal(newVal, 50, R, sec, 300);
          break;
        }
      }

    }
    else if (this.dataUsedVal) {
      updateVal(this.dataUsedVal, this.dataTotal, R, sec, 3000);
    }

    // else if (this.dataUsedVal=='0' && this.dataTotal=='0') {
    //   updateVal(0, 100, R, sec, 3000);
    // }

    else {
      updateVal(0, 100, R, sec, 3000);
    }

  }

  viewDetailsPage(selectedTab:number) {
    //console.log(selectedTab);
    this.navCtrl.push(DetailedUsagePage, {"selectedTab":selectedTab});
  }


  initRaphael() {

    if (Raphael.vml) {
      Raphael.el.strokeLinearGradient = function () {
        // not supporting VML yet
        return this; // maintain chainability
      };
    } else {
      var setAttr = function (el, attr) {
        var key;
        if (attr) {
          for (key in attr) {
            if (attr.hasOwnProperty(key)) {
              el.setAttribute(key, attr[key]);
            }
          }
        } else {
          return document.createElementNS("http://www.w3.org/2000/svg", el);
        }

        return null;
      };

      var defLinearGrad = function (defId, stops) {
        var def = setAttr("linearGradient", null);
        var i, l;
        def.id = defId;

        for (i = 0, l = stops.length; i < l; i += 1) {
          var stopEle = setAttr("stop", null);
          var stop = stops[i];
          setAttr(stopEle, stop);

          def.appendChild(stopEle);
        }

        return def;
      };

      Raphael.el.strokeLinearGradient = function (defId, width, stops) {

        if (stops) {
          this.paper.defs.appendChild(defLinearGrad(defId, stops));
        }

        setAttr(this.node, {
          "stroke": "url(#" + defId + ")",
          "stroke-width": width
        });

        return this; // maintain chainability
      };

      Raphael.st.strokeLinearGradient = function (defId, width, stops) {
        return this.forEach(function (el) {
          el.strokeLinearGradient(defId, width, stops);
        });
      };

      Raphael.fn.defineLinearGradient = function (defId, stops) {

        this.defs.appendChild(defLinearGrad(defId, stops));
      };
    }
  }
}




