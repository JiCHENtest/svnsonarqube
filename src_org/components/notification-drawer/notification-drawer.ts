import { Component, Input, ElementRef, Renderer, Renderer2 } from '@angular/core';
import { Platform, DomController, Events, NavController } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { NotificationTab1Page } from "./notification-tab1/notification-tab1";
import { NotificationTab2Page } from "./notification-tab2/notification-tab2";
import { NotificationTab3Page } from "./notification-tab3/notification-tab3";
import { NotificationTab4Page } from "./notification-tab4/notification-tab4";
import { ProductDetailsDesignGuidePage } from '../../pages/product-details-design-guide/product-details-design-guide';
import { StatusBar } from "@ionic-native/status-bar";

// services
import { MyDealProvider } from '../../global/mydealservice';
import { GatewayService } from '../../global/utilService';
import { ConstantData } from '../../global/constantService';
import { HandleError } from '../../global/handleerror';
import { AlertService } from '../../global/alert-service';
import { SuperTabsController } from '../../components/custom-ionic-tabs';
import { PayBillPage } from "../../pages/pay-bill/pay-bill";
import { ReloadPage } from '../../pages/reload/reload';
import { CreditAdvancePage } from '../../pages/services/credit-manage/credit-advance/credit-advance';
import { ValidityExtensionPage } from '../../pages/services/credit-manage/validity-extension/validity-extension';
import { CreditManagePage } from "../../pages/services/credit-manage/credit-manage";


@Component({
  selector: 'notification-drawer',
  templateUrl: 'notification-drawer.html'
})
export class NotificationDrawer {

  @ViewChild('itemId') itemId;
  @ViewChild('arrowIcon') arrowIcon;
  @Input('options') options: any;
  handleHeight: number = 43;
  bounceBack: boolean = true;
  thresholdTop: number = 200;
  thresholdBottom: number = 200;
  tab1Page: any = NotificationTab1Page;
  tab2Page: any = NotificationTab2Page;
  tab3Page: any = NotificationTab3Page;
  tab4Page: any = NotificationTab4Page;
  isOpen: boolean = false;
  mobileNumber : any; // mobile number
  loaders : any; // loader 
  notificationCount:any;

  constructor(
    public events: Events,
    public element: ElementRef, 
    public renderer2: Renderer2, 
    public renderer: Renderer, 
    public domCtrl: DomController, 
    public platform: Platform,
    public mydealservice :MyDealProvider, 
    public gatewayService:GatewayService, 
    public handleerror: HandleError, 
    private alertCtrlInstance: AlertService,
    private tabsCtrl: SuperTabsController,
    public statusBar: StatusBar,
    public navCtrl: NavController
  ) {
    this.mobileNumber = this.gatewayService.GetMobileNumber();
     events.subscribe('navigateToReload', () => {
    // user and time are the same arguments passed in `events.publish(user, time)`
    //console.log('Welcome', user, 'at', time);
    this.navCtrl.push(ReloadPage);
  });
  events.subscribe('navigateToCreditAdvance',()=>{
    this.navCtrl.push( CreditManagePage,2 );
  });
  events.subscribe('navigateToValidityExtension',()=>{
    this.navCtrl.push( CreditManagePage,1);
  });
  events.subscribe('updateNotificationCount',(count)=>{
    this.notificationCount=count;
  })
  events.subscribe('goToPayBill',(params)=>{
      this.navCtrl.push(PayBillPage,{});
  });
  events.subscribe('redeemKawKaw',(params)=>{
       this.navCtrl.push(ProductDetailsDesignGuidePage,{
      param1:params.registerFlag,param2:params.kawkaw
    });
  });
  
    events.subscribe('updateMydeals', (val) => {
      // users get updated mydeals of selected msisdn number
      console.log('Welcome updateNotificationCount with selected number time ',val);
      this.mydealsList();
    });
  }

  ionViewWillEnter() {
    // console.log('ionViewWillEnter NotificationDrawer');
    
    // this.tabsCtrl.enableTabsSwipe(true, 'mydealsTabs');
    // var _that = this;
    // var loader = this.gatewayService.LoaderService();
    // //loader.duration = 2000;
    // loader.present();
    // // this.mydealservice.SetFetchMydeals();
     
    
    //   var params = {
    //     "msisdn":_that.mobileNumber,
    //   }      
    //   // return new Promise((resolve, reject) => {
    //     _that.gatewayService.CallAdapter(ConstantData.adapterUrls.FetchMydeals,"POST",params).then(function(res){
    //         console.log("************************************* component success FetchMydeals"+JSON.stringify(res));
    //       _that.handleerror.handleErrorFetchMydeals(res,"FetchMydeals").then((result) => {
    //           if(result == true) {
    //               console.log("hai if FetchMydeals ",result);
    //               // resolve(res);
    //               loader.dismiss();
    //               _that.mydealservice.SetFetchMydeals(res);
    //               // _that.events.publish('mydeal','hai');   
                  
                  
    //           }
    //           else {
    //               console.log("hai else FetchMydeals ",result);
    //               loader.dismiss();
    //               _that.mydealservice.SetFetchMydeals(res);
    //               // _that.events.publish('mydeal','hai');  
                  
                  
    //               // _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
    //               // reject("error : no data found.");
    //           }
    //       });
    //           // this.handleError(res,"FetchMydeals");
    //           // resolve(res);
    //     })
    //     .catch(function(e){
    //       console.log("****************************************** component failure FetchMydeals"+e);
    //       //alert("error user info FetchMydeals ");
    //       loader.dismiss();
    //       _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
    //       // reject(e);
    //     });
  }

  ngOnDestroy(){ 
      this.events.unsubscribe('navigateToReload');
      this.events.unsubscribe('navigateToCreditAdvance');
      this.events.unsubscribe('navigateToValidityExtension');
      this.events.unsubscribe('updateNotificationCount');
      this.events.unsubscribe('goToPayBill');
      this.events.unsubscribe('redeemKawKaw');
  }

  ngOnInit() {
    this.mydealsList();    
  }

  mydealsList() {
    console.log('ionViewWillEnter NotificationDrawer');
    
    this.tabsCtrl.enableTabsSwipe(true, 'mydealsTabs');
    var _that = this;
    var loader = this.gatewayService.LoaderService();
    //loader.duration = 2000;
    loader.present();
    // this.mydealservice.SetFetchMydeals();
     
    
      var params = {
        "msisdn":_that.gatewayService.GetMobileNumber(),
      }      

      console.log("_that.gatewayService.GetMobileNumber() ",_that.gatewayService.GetMobileNumber());
      // return new Promise((resolve, reject) => {
        _that.gatewayService.CallAdapter(ConstantData.adapterUrls.FetchMydeals,"POST",params).then(function(res){
            console.log("************************************* component success FetchMydeals"+JSON.stringify(res));
          _that.handleerror.handleErrorFetchMydeals(res,"FetchMydeals").then((result) => {
              if(result == true) {
                  console.log("hai if FetchMydeals ",result);
                  // resolve(res);
                  loader.dismiss();
                  _that.mydealservice.SetFetchMydeals(res);
                  // _that.events.publish('mydeal','hai');   
                  
                  
              }
              else {
                  console.log("hai else FetchMydeals ",result);
                  loader.dismiss();
                  _that.mydealservice.SetFetchMydeals(res);
                  // _that.events.publish('mydeal','hai');  
                  
                  
                  // _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
                  // reject("error : no data found.");
              }
          });
              // this.handleError(res,"FetchMydeals");
              // resolve(res);
        })
        .catch(function(e){
          console.log("****************************************** component failure FetchMydeals"+e);
          //alert("error user info FetchMydeals ");
          loader.dismiss();
          // _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
          // reject(e);
        });
  }

  ngAfterViewInit() {
    //disable swipe from tabs 
    // this.tabsCtrl.enableTabsSwipe(false, 'NotificationsTabs');

    if (this.options.handleHeight) {
      this.handleHeight = this.options.handleHeight;
    }

    if (this.options.bounceBack) {
      this.bounceBack = this.options.bounceBack;
    }

    if (this.options.thresholdFromBottom) {
      this.thresholdBottom = this.options.thresholdFromBottom;
    }

    if (this.options.thresholdFromTop) {
      this.thresholdTop = this.options.thresholdFromTop;
    }

    // this.renderer.setElementStyle(this.element.nativeElement, 'top', this.platform.height() - this.handleHeight + 'px');

    // this.renderer.setElementStyle(this.element.nativeElement, 'top', 91.5 + '%');
    this.renderer.setElementStyle(this.element.nativeElement, 'top', window.innerHeight - this.handleHeight + 'px');


    this.renderer.setElementStyle(this.element.nativeElement, 'padding-top', this.handleHeight + 'px');


    let hammer = new window['Hammer'](this.itemId.nativeElement);
    hammer.get('pan').set({ direction: window['Hammer'].DIRECTION_VERTICAL });


    hammer.on('pan', (ev) => {
      this.handlePan(ev);
    });

    hammer.on('tap', (ev) => {
      this.handelTap(ev);
    });

  }

  handlePan(ev) {

    let newTop = ev.center.y;
    let bounceToBottom = false;
    let bounceToTop = false;

    if (this.bounceBack && ev.isFinal) {

      let topDiff = newTop - this.thresholdTop;
      let bottomDiff = (this.platform.height() - this.thresholdBottom) - newTop;

      topDiff >= bottomDiff ? bounceToBottom = true : bounceToTop = true;
    }

    if ((newTop < this.thresholdTop && ev.additionalEvent === "panup") || bounceToTop) {

      this.openNotification();
      this.isOpen = true;

    } else if (((this.platform.height() - newTop) < this.thresholdBottom && ev.additionalEvent === "pandown") || bounceToBottom) {

      this.closeNotification();
      this.isOpen = false;

    } else {

      this.renderer.setElementStyle(this.element.nativeElement, 'transition', 'none');

      if (newTop > 0 && newTop < (this.platform.height() - this.handleHeight)) {

        if (ev.additionalEvent === "panup" || ev.additionalEvent === "pandown") {

          this.domCtrl.write(() => {
            var maxHeight = this.platform.height();
            var yRate = newTop / (maxHeight);
            var rotationAngle = yRate * 180;
            var rotateRatio = 180;

            this.renderer.setElementStyle(this.element.nativeElement, 'top', newTop + 'px');
            //this.renderer.setElementStyle(this.element.nativeElement, 'padding-top', this.handleHeight + 20  + 'px');

            if (ev.additionalEvent === "panup") {

              this.renderer.setElementStyle(this.arrowIcon.nativeElement, 'transform', 'rotate(' + (rotateRatio + rotationAngle) + 'deg)');

            } else if (ev.additionalEvent === "pandown") {

              this.renderer.setElementStyle(this.arrowIcon.nativeElement, 'transform', 'rotate(' + (rotateRatio + rotationAngle) + 'deg)');

            }

          });

        }

      }

    }

  }

  handelTap(ev) {
    if (!this.isOpen) {
      this.statusBar.styleDefault();
      this.openNotification();
      this.isOpen = true;
    }
    else {
      this.statusBar.styleLightContent();
      this.closeNotification();
      this.isOpen = false;
    }
  }


  openNotification() {
    this.domCtrl.write(() => {
      this.renderer.setElementStyle(this.element.nativeElement, 'transition', 'top 0.5s');
      this.renderer.setElementStyle(this.arrowIcon.nativeElement, 'transform', 'rotate(180deg)');
      this.renderer.setElementStyle(this.element.nativeElement, 'top', '0px');
      this.renderer.setElementStyle(this.element.nativeElement, 'padding-top', this.handleHeight + 20  + 'px');
      this.renderer.setElementClass(this.itemId.nativeElement, 'pan-up-top', true);

    });
  }
  closeNotification() {
    this.domCtrl.write(() => {
      this.renderer.setElementStyle(this.element.nativeElement, 'transition', 'top 0.5s');
      this.renderer.setElementStyle(this.arrowIcon.nativeElement, 'transform', 'rotate(360deg)');
      // this.renderer.setElementStyle(this.element.nativeElement, 'top', this.platform.height() - this.handleHeight + 'px');
      // this.renderer.setElementStyle(this.element.nativeElement, 'top', 91.5 + '%');
      this.renderer.setElementStyle(this.element.nativeElement, 'top', window.innerHeight - this.handleHeight + 'px');
      this.renderer.setElementStyle(this.element.nativeElement, 'padding-top', this.handleHeight  + 'px');
      this.renderer2.removeClass(this.itemId.nativeElement, 'pan-up-top');
    });
  }

 

}
