import { Component, NgZone } from "@angular/core";
import { NavController, NavParams, ModalController, Events } from "ionic-angular";
import {GlobalVars} from "../../../providers/globalVars";
import { ProductDetailsDesignGuidePage } from '../../../pages/product-details-design-guide/product-details-design-guide';
import { MyDealsPage } from '../../../pages/my-deals/my-deals';

// services
import { MyDealProvider } from '../../../global/mydealservice';
import { AlertService } from '../../../global/alert-service';
import { ConfirmationScreensMydealsPage } from "../../../pages/confirmation-screens-mydeals/confirmation-screens-mydeals";
/**
 * Generated class for the NotificationTab2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-notification-tab2',
  templateUrl: 'notification-tab2.html',
})
export class NotificationTab2Page {

  modalOptions: any;

  myDealsList : any;

  constructor( 
    public events: Events,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public globalVar:GlobalVars,
    public mydealservice : MyDealProvider,
    private alertCtrlInstance: AlertService,
    public zone : NgZone
  ) {
    // alert(3);
    this.modalOptions = {
      showBackdrop: false,
      enableBackdropDismiss: true ,
      cssClass:this.globalVar.getCurrentTheme()
    }

    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationTab2Page -*-*-');
    
  }

  ionViewWillEnter() {
    console.log(" ionViewWillEnter NotificationTab2Page");
    // alert(7)
    // this.events.subscribe('mydeal:created', () => {
      console.log("mydeal event ");
      try {
        this.zone.run(()=>{
          this.myDealsList = this.mydealservice.GetFetchMydeals();
        });
        console.log("myDealsList info ",JSON.stringify(this.myDealsList));
        if(this.myDealsList == undefined || this.myDealsList == "undefined" ){
          // this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
        }
        else {}

      }
      catch(e) {
        console.log("Exception mydeal ",e);
      }
    // });
    // _that.events.publish('mydeal:created');
    //   this.events.subscribe('mydeal:created', () => {
    //     console.log("mydeal event ");
    //     try {
    //       this.myDealsList = this.mydealservice.GetFetchMydeals();
    //       console.log("myDealsList info ",JSON.stringify(this.myDealsList));
    //     }
    //     catch(e) {
    //       console.log("Exception mydeal ",e);
    //     }
    // });
  }

  presenMydealspageModal(mydealdeatils) {
    let modal = this.modalCtrl.create(MyDealsPage, {"mydealsdetails" : mydealdeatils}, this.modalOptions );
    modal.present();
  }

  presenMydealspageCinfirmation(mydealdeatils) {

    // this.navCtrl.push(ConfirmationScreensMydealsPage,{"mydealPlanInfo":mydealdeatils});  
    // let modal = this.modalCtrl.create(MyDealsPage, {"mydealPlanInfo" : mydealdeatils}, this.modalOptions );
    // modal.present();
    // let modal = this.modalCtrl.create(ConfirmationScreensMydealsPage, {"mydealPlanInfo" : mydealdeatils}, this.modalOptions );
    // modal.present();

    this.navCtrl.parent.parent.push(ConfirmationScreensMydealsPage,{"mydealPlanInfo":mydealdeatils});
  }
  
  //  presenMydealspageModal(mydealdeatils) {
  //    console.log("presenMydealspageModal ",JSON.stringify(mydealdeatils));
  //    this.navCtrl.parent.parent.push(ProductDetailsDesignGuidePage,{"mydealsdetails" : mydealdeatils});
  //  }
}
