import {Component, Input, OnInit} from "@angular/core";
import { Events } from "ionic-angular";
/**
 * Generated class for the UsageIndicatorComponent component.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-usage-indicator',
  templateUrl: 'usage-indicator.html',
  inputs: ['value','hideProgressBar','expiryDate','minValue','maxValue']
})
export class UsageIndicatorComponent implements OnInit {

  progressBarValue: string = '100%'

  @Input("value") value: number;
  @Input("hideProgressBar") hideProgressBar: boolean = false;
  @Input("expiryDate") expiryDate: string;
  @Input("minValue") minValue: number;
  @Input("maxValue") maxValue: number;
  @Input("unit") unit: string;


  constructor(public events: Events) {
    events.subscribe('updatePool',(params)=>{
        this.updatePool(params);
    })
  }
  

  ngOnInit() {
    if(this.maxValue==0 && this.minValue==0){
        this.progressBarValue = 0+'%';
    }else if (this.value <= this.maxValue && this.value >= this.minValue) {
      setInterval(() => {
        let percentage = (this.value/(this.maxValue-this.minValue))*100;
         if((percentage < 5 && percentage > 0)) {
          percentage = 4;
        }
        this.progressBarValue = percentage +  "%";
      }, 500);
    }
  }
  ngOnDestroy() {
    //this.events.unsubscribe('callTransferAmount');
    console.log('ng destroy unsubscribe all events here');
   this.events.unsubscribe('updatePool');
    }
  updatePool(params){
    this.value = params.value;
    this.maxValue = params.max;
  this.minValue = params.min;
    //this.maxValue = numberis.substring(0,numberis.length-2)
    
    if(this.maxValue==0 && this.minValue==0){
        this.progressBarValue = 0+'%';
    }else if (this.value <= this.maxValue && this.value >= this.minValue) {
      setInterval(() => {
        let percentage = (this.value/(this.maxValue-this.minValue))*100;
         if((percentage < 5 && percentage > 0)) {
          percentage = 4;
        }
        this.progressBarValue = percentage +  "%";
      }, 500);
    }
  }



  isProgressBarHidden() {
    return this.hideProgressBar;
  }
}
