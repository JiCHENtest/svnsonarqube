import {Injectable, Renderer2} from "@angular/core";

@Injectable()

export class GlobalVars {

  public XPAXTheme: boolean = false; // default XPAXTheme not activated = false , activated = true

  public capZoneStatus: boolean = false; 

  public callBarredStatus: boolean = false;
  public currentTheme: String = '';



  constructor() {

  }

  private checkXPAXTheme(renderer:Renderer2) {

    if (this.XPAXTheme) {
      renderer.addClass(document.body, 'XPAX');
    } else {
      renderer.removeClass(document.body, 'XPAX');
    }
  }

  setXPAXTheme(value,  renderer: Renderer2) {
    this.XPAXTheme = value;
    this.checkXPAXTheme(renderer)
  }

  getXPAXTheme() {
    return this.XPAXTheme;
  }

  getCapZoneStatus(){
    return this.capZoneStatus;
  }

  getcallBarredStatus(){
    return this.callBarredStatus;
  }

  setCapZoneStatus(value){
    this.capZoneStatus = value;
  }

  setcallBarredStatus(value){
    this.callBarredStatus = value;
  }

  getCurrentTheme(){
    if(this.XPAXTheme==true){
      this.currentTheme = 'XPAX';
    }
    else{
      this.currentTheme = 'celcom';
    }
    return this.currentTheme;
  }

}
