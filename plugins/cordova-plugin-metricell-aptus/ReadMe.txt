Plugin Installation
-------------------
The plugin is installed using the cordova plugin manager.  To install follow these steps:

1. Copy the 'cordova-plugin-metricell-aptus-celcommy' folder alongside your Cordova project.

---+--cordova-plugin-metricell-aptus-celcommy
   |
   +--<your cordova project>
   
2. From a command window opened within your cordova project folder, install the plugin using the command:

	cordova plugin add cordova-plugin-metricell-aptus --searchpath ..
	
3. Finally build your cordova project to include the plugin code within the Android project:

	cordova build
	

Plugin Architecture
-------------------

The architecture of the plugin is quite simple.  The important files are:

/plugin.xml
- The configuration for the the plugin.  This file adds the required entries to the host project AndroidManifest.xml file to install the Aptus API service. It also specifies the plugin source
  files, JAR library and resources at will be included within the host project.  A description of the manifest configuration can be found in the standard Aptus installation guide.

/www/aptus.js
- The JS file provides convenience methods for calling the Aptus plugin actions.

/src/android/libs/MetricellAptusAPI-v1.0.22_170303.jar
- The latest Aptus API jar library file, this gets copied to the host project /lib/ folder when the plugin is installed.

/src/android/Aptus.java
- The CordovaPlugin wrapper for the Aptus API.  This provides all the native calls to manage the running of the background service.

/src/android/BootCompletedReceiver.java
- Ths broadcast receiver that gets called when the application is reinstalled or on rebooted to automatically restart the background service (if it has been previously started).

/src/android/res/
- Image resources used by the API.

	
Specifing MSISDN
----------------

Data send by the APTUS API can be associated with an individual MSISDN.  This must be specified manually before starting the service for the first time.
To set the MSISDN use the JavaScript function:

	cordova.plugins.aptus.setMsisdn("601234567890");
	
The value should be the full MSISDN including the country code.  If this is not set then all data sent by the API will not be associated with an MSISDN.
Any value set using this function is saved to persistent storage so only needs to be set once. The current MSISDN value set by the setMsisdn can also be
retrieved using the method:

	cordova.plugins.aptus.getMsisdn(function (msisdn) {
		alert("MSISDN:" + msisdn);
	});
	
Starting the background service
-------------------------------

The once the standard deviceReady callback has been called the Aptus plugin will be available. The background service can be started using the Javascript function:

	cordova.plugins.aptus.startService(successCallback, errorCallback);

Which in turn will call the execute method within the Aptus.java file in the native Android plugin with the action 'startService'.
The startService action will first check that the required permissions have been granted (prompting the user if neccessary) and starts
the Android background service. Once started successfully the successCallback will be called. If the required permissions are not granted
the service will not start and the errorCallback will be called.

Once the service has successfully started, it will also be re-started automatically whenever the handset is rebooted.  When running the
service will periodically check its registration state with the Metricell servers, request the latest server defined settings and send any data it has collected.
When the service has successfully registered the device with the server (which typically occurs shortly after starting the background service) it will be
assigned a registration ID.  This can be obtained using the Javascript method:

	cordova.plugins.aptus.getRegistrationId(function (regId) {
		alert("Registration ID:" + regId);
	});

The registration ID is useful for debugging and locating information sent by the background service to the Metricell servers and should be included with any support requests.
	
Stopping the background service
-------------------------------
	
To stop the running background service simply call:

	cordova.plugins.aptus.stopService();
	
This will also stop the service being started automatically when the handset is rebooted.

Sending data to server
----------------------

While the background service is running it will periodically send any queued data back to the Metricell servers.  If you wish to force it flush data immediately you can do so
using the method:

	cordova.plugins.aptus.sendData();
	
Providing there is a working data connection, any data will be sent immediately.

Requesting server settings
--------------------------

While the background service is running it will also periodically request settings from the Metricell servers.  If you wish to force it check immediately you can do so
using the method:

	cordova.plugins.aptus.requestSettings();
	
Providing there is a working data connection, the settings will be requested and updated immediately.

Debugging
---------

To enable logging of debug information first install the Metricell debugger APK contained within this package.  If the Aptus service is already running stop the service and restart it.
Once logging it will attempt to write log files to the /MCC folder on the phone storage. HTTP requests and other logging data is stored within this folder