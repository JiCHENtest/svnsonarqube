import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { EmailComposer } from '@ionic-native/email-composer';
import { GatewayService } from '../../../global/utilService';

@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
})
export class FeedbackPage {

  selected_category: any;
  feedback_text: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private emailComposer: EmailComposer, public gatewayService:GatewayService, private renderer: Renderer2) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackPage');
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Feedback", this.renderer);
  }

  submitFeedback(){

  let email = {
      app:'mailto',
      to: '',
      cc: '',
      bcc: [],
      attachments: [],
      subject: this.selected_category+" Error ",
      body: this.feedback_text,
      isHtml: true
    };
    this.emailComposer.open(email);
  }
}
