import { Injectable } from '@angular/core';
import { GatewayService } from '../../../global/utilService';
import { AlertService } from '../../../global/alert-service';
import { SpeedTestPage } from './speed-test';
import { Events } from 'ionic-angular';
declare var cordova;
declare var classObj;

@Injectable()
export class SpeedTestProvider {

  msisdnTextSpeed : any;
  registrationIdSpeed : any;
  downloadStatus : any;
  uploadStatus : any;
  pingStatus : any;

  constructor(public gatewayService:GatewayService, private alertCtrlInstance: AlertService, public events: Events) {
  }

  /*
    Start a speed test, it will first check that a data connection is available before attempting to start the test.
  */
   btn_startSpeedtest(speedtestCallback, speedtestErrorCallback,_thisParent) {
        var _this = this;
        if (cordova.plugins.aptus.hasDataConnection(function(msg) {
        _this.startTest(speedtestCallback, function() {
          _this.alertCtrlInstance.showAlert("Uh Oh. No Internet Detected", "Please turn on mobile data or connect to a wifi network to continue.", "OK");  
          _this.events.publish("stoptest");
        },_thisParent);
        },function(err) {
          _this.alertCtrlInstance.showAlert("Uh Oh. No Internet Detected", "Please turn on mobile data or connect to a wifi network to continue.", "OK");
        })){}
  }

  /*
    Start a speed test, called if a data connection is available and will prompt the user to enter their MSISDN,
    this is to associate the speedtest result data with a particular user.
  */
   startTest(speedtestCallback, speedtestErrorCallback,_this) {
          //let msisdn = "6"+this.gatewayService.GetMobileNumber();
          let msisdn = "6"+"0132069214";
          cordova.plugins.aptus.setMsisdn(msisdn);

           cordova.plugins.aptus.speedtestStart(function
          (state) {
            speedtestCallback(state,_this);
          },function(err) {
            speedtestErrorCallback(err);
          });
    }

  /*
    Stop the current speed test
  */
   btn_stopSpeedtest() {
      this.resetSpeedtest();
      cordova.plugins.aptus.speedtestStop();
  }



   resetSpeedtest(){
     this.downloadStatus = "Pending";
     this.uploadStatus = "Pending";
     this.pingStatus = "Pending";
  }

  getMsisdnTextSpeed() : any {
    return this.msisdnTextSpeed;
  }

  getRegistrationIdSpeed() : any {
    return this.registrationIdSpeed;
  }

  getUploadStatus() : any {
    return this.uploadStatus;
  }

  getDownloadStatus() : any {
    return this.downloadStatus;
  }

  getPingStatus() : any {
    return this.pingStatus;
  }
}
