import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GatewayService } from '../../../../global/utilService';
import { CallNumber } from '@ionic-native/call-number';

/**
 * Generated class for the ContactUsHotlinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-contact-us-hotline',
  templateUrl: 'contact-us-hotline.html',
})
export class ContactUsHotlinePage {
isPrepaid:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public gatewayService:GatewayService, private callNumber: CallNumber, private renderer: Renderer2) {
    
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Call us", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactUsHotlinePage');
   
  }
  ionViewWillEnter(){
    this.getPrePosInformation();
  }
  getPrePosInformation(){
    this.isPrepaid= this.gatewayService.isPrePaid();
  }

  launchDialer(numberToCall){
    this.callNumber.callNumber(numberToCall,false);
  }


}
