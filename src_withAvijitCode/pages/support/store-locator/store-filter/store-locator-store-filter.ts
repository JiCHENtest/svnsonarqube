import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { GatewayService } from '../../../../global/utilService'; 


@Component({
  selector: 'page-store-locator-store-filter',
  templateUrl: 'store-locator-store-filter.html',
})
export class StoreLocatorStoreFilterPage {

  storeType: any = [{ value: 'Blue Cube', label: 'Blue Cube', selected: false },
  { value: 'Celcom Centre', label: 'Celcom Centre', selected: false },
  { value: 'Celcom Telecommunication Special', label: 'Celcom Telecommunication Special', selected: false },
  { value: 'Celcom Xclusive', label: 'Celcom Xclusive', selected: false }];

  serviceType: any = [{ value: '', label: 'All', selected: false },
  { value: 'false', label: 'Without 24 hours Self Service Kiosk', selected: false },
  { value: 'true', label: 'With 24 hours Self Service Kiosk', selected: false },
  ];

  selected_filter: any = {
    store_type: [],
    is_open_24hr: []
  };
  
  storeTypeCheckbox: boolean[];
  serviceTypeCheckbox: boolean[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    private renderer: Renderer2,
    public gatewayService: GatewayService) {
    if (navParams.get('selectedFilter')) {
      this.selected_filter = navParams.get('selectedFilter');
    }

    let isAllStoreSelected = true; 
    for(let data of this.storeType) {      
      if(this.selected_filter.store_type.indexOf(data.value)!=-1){
        data.selected = true;
      }else{
        isAllStoreSelected = false;
      }
    }
    
    let isAllTypeSelected = true;
    for(let data of this.serviceType) {      
      if(this.selected_filter.is_open_24hr.indexOf(data.value)!=-1){
        data.selected = true;
      }else{
        isAllTypeSelected = false;
      }
    }

  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Store locator filter", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StoreLocatorStoreFilterPage');
  }

  resetForm() {
    for (let i = 0; i < this.storeType.length; i++) {
      this.storeType[i].selected = false;
    }
    for (let i = 0; i < this.serviceType.length; i++) {
      this.serviceType[i].selected = false;
    }
  }

  onChange(value, isChecked, type) {
    if (isChecked) {
      this.selected_filter[type].push(value);
    } else {
      this.selected_filter[type].splice(this.selected_filter[type].indexOf(value), 1);
    }
  }

  dismiss() {
    if (this.selected_filter.store_type.length == 0) {
      this.selected_filter.store_type = [""];
    }
    if (this.selected_filter.is_open_24hr.length == 0) {
      this.selected_filter.is_open_24hr = [""];
    }
    this.viewCtrl.dismiss(this.selected_filter);

  }
}
