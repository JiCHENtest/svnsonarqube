import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';
import { GatewayService } from '../../../../global/utilService'; 

/**
 * Generated class for the StoreLocatorStoreMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-store-locator-store-map',
  templateUrl: 'store-locator-store-map.html'
})
export class StoreLocatorStoreMapPage {

  


  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    private renderer: Renderer2,
    public gatewayService: GatewayService) {
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Store locator map", this.renderer);
  }


  ionViewDidLoad() {
  }


  dismiss() {
    this.viewCtrl.dismiss();

  }


}
