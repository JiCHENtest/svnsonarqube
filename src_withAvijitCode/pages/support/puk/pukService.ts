import { ConstantData} from "../../../global/constantService";
import { GatewayService } from '../../../global/utilService';
import { Injectable } from '@angular/core';
import { HandleError } from '../../../global/handleerror';

@Injectable()
export class PUKService {

constructor(public handleerror: HandleError, public gatewayService:GatewayService){

};


fetchPUKDetails(){
  try{
    var params = {
            "mobNumber": this.gatewayService.GetMobileNumber()
    }
    return new Promise((resolve, reject) => {
    this.gatewayService.CallAdapter(ConstantData.adapterUrls.PUKEnquiry,"POST",params).then((res)=> {
      var pukNumber = "Unable to fetch PUK";
      let response = res;
      if(response["statusCode"] == '200'){
        pukNumber = response["Envelope"].Body.PUKQueryCelResponse.PUKQueryCelResponseEntityType.X_PUK1;
      }
      resolve(pukNumber);
    }).catch(function(e){
      reject(e);
    });
    });
  }catch(e){

    }
  }
}
