import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { GatewayService } from '../../../global/utilService';

/**
 * Generated class for the PlanDetailsTermsAndConditionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-plan-details-terms-and-conditions',
  templateUrl: 'plan-details-terms-and-conditions.html',
})
export class PlanDetailsTermsAndConditionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private renderer: Renderer2, public gatewayService:GatewayService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlanDetailsTermsAndConditionsPage');
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Plan details terms and condition", this.renderer);
  }

  goBack() {
    if(this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    };
  }

}
