
/**
 * @author Capgemini 
 */

import { ConstantData} from "../../../global/constantService";
import { GatewayService } from '../../../global/utilService';
import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { AlertController, LoadingController } from 'ionic-angular';
import { HandleError } from '../../../global/handleerror';
import { AlertService } from '../../../global/alert-service';
import { JsonstoreService } from '../../../global/jsonstore';
import { ReloadService } from '../../../global/reloadServices'; 


@Injectable()
export class PaymentMethodsService {

mobileNumber:any;
cardDetails:any;
loginMobileNumber:any;

constructor(private network: Network,public alertCtrl:AlertController, public loadingCtrl:LoadingController,public handleerror: HandleError, public gatewayService:GatewayService,  public jsonstoreService: JsonstoreService,  public reloadService:ReloadService){
    this.loginMobileNumber=this.reloadService.gettingLoginNumber(); 
    // var mobile_number = {};
    // this.jsonstoreService.getData(mobile_number, "mobilenumber").then(
    //   (result_mob) => {
    //     console.log("mobile_number update() ", JSON.stringify(result_mob));
   
    //     this.loginMobileNumber = result_mob.length > 0 ? result_mob[0].json.mobilenum : "";
    //    });

    //this.mobileNumber=this.gatewayService.GetMobileNumber();
//this.selectedMobileNumber=gatewayService.GetSelectMobileNumber();
};
setCardDetails(value){
    this.cardDetails = value;
}
getCardDetails(){
    return this.cardDetails;
}
fetchSavedCards(param){
    try{
        //alert("SelectedNumber"+selectedNumber)
        // var getMobileNumber = "0136426573";
        var params = {
            "loginID":param,
            };
        return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.fetchSavedCards,"POST",params).then((res)=> {
        
        console.log("*********************************** component fetchSavedCards"+JSON.stringify(res));

        //alert("AlertFine"+res);
        resolve(res);
        
        })
        .catch(function(e){
        console.log("******************************* component failure fetchSavedCards"+e);
        
        reject(e);
    });
    });
    }catch(e){
        //alert("error"+JSON.stringify(e));
    }
    }

    deleteCards(loginId,token){
        try{
            //alert("SelectedNumber"+selectedNumber)
            // var getMobileNumber = "0136426573";
            var params = {
                "loginID":loginId,
                "TOKEN":token
                };
            return new Promise((resolve, reject) => {
            this.gatewayService.CallAdapter(ConstantData.adapterUrls.deleteSavedCard,"POST",params).then((res)=> {
            
            console.log("*********************************** component deleteSavedCard"+JSON.stringify(res));
    
            //alert("AlertFine"+res);
            resolve(res);
            
            })
            .catch(function(e){
            console.log("******************************* component failure deleteSavedCard"+e);
            
            reject(e);
        });
        });
        }catch(e){
            //alert("error"+JSON.stringify(e));
        } 
    }
    
    
    }
    
    


