import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PaymentMethodsService } from '../paymentMethodsService';
import { Keyboard } from '@ionic-native/keyboard';
import { GatewayService } from '../../../../global/utilService';


  

/**
 * Generated class for the ViewCardDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-view-card-details',
  templateUrl: 'view-card-details.html',
})
export class ViewCardDetailsPage {
  checkboxFlag: boolean;
  selectedCardDetails:any;
  name:any;
  expiryDate:any;
  cardNumber:any;
  cardType:any
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public paymentMethodsService: PaymentMethodsService, private keyboard: Keyboard,
    private renderer: Renderer2, public gatewayService: GatewayService) {
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Payment methods view card details", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewCardDetailsPage');
    this.selectedCardDetails=this.paymentMethodsService.getCardDetails();
    this.name=this.selectedCardDetails.name;
    this.expiryDate=this.selectedCardDetails.EXPIRYDATE;
    this.cardNumber=this.selectedCardDetails.masked;
    this.cardType=this.selectedCardDetails.cardtype;

  }
  handleKeyDone(e){
    if(e.which === 13){
      this.keyboard.close();
    }
  }
  //checkboxFlag

}
