import {Component, Renderer2} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {MyAccountPage} from "./my-account/my-account";
import { PaymentMethodsPage } from "./payment-methods/payment-methods";
import { GatewayService } from '../../global/utilService';


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private renderer: Renderer2, public gatewayService: GatewayService ) {
  }

  navToMyAccount(event): void {
    console.log(event);
    this.navCtrl.push(MyAccountPage, {});
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Settings", this.renderer);
  }
  
  goToPaymentMethods(){
    this.navCtrl.push( PaymentMethodsPage,{} );
  }
}
