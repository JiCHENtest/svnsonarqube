import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

// service 
import { GatewayService } from '../../global/utilService';
import { HandleError } from '../../global/handleerror';
import { ConstantData } from '../../global/constantService';
import { DashboardService } from '../../pages/home/homeService';

/**
 * Generated class for the ConfirmationScreensPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-confirmation-screens-mydeals',
  templateUrl: 'confirmation-screens-mydeals.html',
})
export class ConfirmationScreensMydealsPage {

  myVar: boolean = false;
  mydealPlanInfo : any;
  mobileNumber : any;
  userInfo : any;
  prebillAmountForMydeals : any;
  loader : any;
  cost: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public alertCtrl: AlertController,
    public gatewayService:GatewayService,
    public handleerror: HandleError, 
    public dashboardService : DashboardService,
    private renderer: Renderer2
  ) {

    this.mydealPlanInfo = this.navParams.get('mydealPlanInfo');
    this.cost = parseFloat(this.mydealPlanInfo.costPerOffer).toFixed(2);
    this.mobileNumber = this.gatewayService.GetMobileNumber();

    this.userInfo = this.dashboardService.GetUserInfo();

    console.log("ConfirmationScreensMydealsPage -> mydealPlanInfo = ",this.mydealPlanInfo);
    console.log("ConfirmationScreensMydealsPage -> mobileNumber = ",this.mobileNumber);
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Confirmation screen mydeals", this.renderer);
  }

  ionViewDidLoad() {
    
    console.log('ionViewDidLoad ConfirmationScreensMydealsPage');

    if(this.userInfo.ListOfServices.Services.ServiceType == "Prepaid" || this.userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Prepaid") { 

      var prepaidCurrentBalance = this.dashboardService.GetBalancePrepaidData();
      console.log("ConfirmationScreensMydealsPage -> prepaid Current Balance = ",prepaidCurrentBalance);
      var balanceRecord = prepaidCurrentBalance.BalanceRecordList.BalanceRecord;
      
      if(balanceRecord.length>1){
        for(let obj in balanceRecord){
          if(balanceRecord[obj].AccountType==2000){
            var amount = (balanceRecord[obj].Balance/10000).toString();
            this.prebillAmountForMydeals = parseFloat(amount).toFixed(2);
            //this.billingCycle = this.formatDate(balanceRecord[obj].ExpireTime);
          }
        }
      }else{
        var amount = (prepaidCurrentBalance.BalanceRecordList.BalanceRecord.Balance/10000).toString();
        this.prebillAmountForMydeals = parseFloat(amount).toFixed(2);
        console.log("ConfirmationScreensMydealsPage -> prebillAmountForMydeals = ",prepaidCurrentBalance);
       // this.billingCycle = this.formatDate(this.prepaidCurrentBalance.BalanceRecordList.BalanceRecord.ExpireTime);
      }

      this.prebillAmountForMydeals =" RM" +( parseFloat(this.prebillAmountForMydeals) - parseFloat(this.mydealPlanInfo["costPerOffer"])).toFixed(2); 
      console.log("ConfirmationScreensMydealsPage -> new balance = ",this.prebillAmountForMydeals);
    }
  }

  toggleList() {
    event.stopPropagation();
    this.myVar = !this.myVar;
  }

  presentsucess() {

    var _that = this;
    _that.loader = _that.gatewayService.LoaderService();
    //loader.duration = 2000;
    _that.loader.present();

    var _that = this;

    var params = {
      "msisdn":_that.mobileNumber,
      "treatmentCode": _that.mydealPlanInfo.treatmentCode,
      "productID": _that.mydealPlanInfo.productID,
      "eventValue": "GenMobAppEvent",
      "packFlag": "Mydeals Plan Purchase",
      "amount": this.mydealPlanInfo.costPerOffer,
      "title": this.mydealPlanInfo.planDescription1
    };

    console.log("ConfirmationScreensMydealsPage -> params = ",JSON.stringify(params));
        // "msisdn": "6" + this.mobileNo,//"60193522834"//this.localStorage.get("MobileNum","")
        // "bnumber": "6" + this.mobileNo, // self gift for gbshare number
        // "operation_id": "1", // old one "operation_id":"MAXUP",   
        // "txn_id": this.gatewayService.generateRandomNum(16),
        // "amount": details.PRODUCT_PRICE || details.PLAN_AMOUNT,
        // "product_id": details.PRODUCT_ID,
        // "isMaxUp": this.isMaxUp,
        // "isLegacy": isLegacy,
        // "packFlag": this.confirmDetails.PLAN_DESCRIPTION,
        // "title": this.navParams.get('header_title')

    // return new Promise((resolve, reject) => {
      _that.gatewayService.CallAdapter(ConstantData.adapterUrls.PurchaseMydeal,"POST",params).then(function(res){
        console.log("************************************* component success PurchaseMydeals"+JSON.stringify(res));
        _that.handleerror.handleErrorPurchaseMydeals(res,"PurchaseMydeals").then((result) => {
          if(result == true) {
              console.log("hai if PurchaseMydeals ",result);
              // _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
              _that.loader.dismiss();
              const alert_success = _that.alertCtrl.create({
                title: 'You Just Got a Great Deal',
                // subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
                message: "You have purchased "+ _that.mydealPlanInfo.planDescription1 +"",
                buttons: [
                  {
                    text: 'OK',
                    handler: () => {
                      console.log('Call to Action Clicked');
                    }
                  }],
                cssClass: 'success-message'
              });
              alert_success.present();
              // resolve(res);                    
          }
          else {
            _that.loader.dismiss();
              console.log("hai else PurchaseMydeals ",result);
              // _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
              const alert_failure = _that.alertCtrl.create({
                title: 'Uh Oh. MyDeals Is Busy',
                // subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
                message: "Please try again later",
                buttons: [
                  {
                    text: 'OK',
                    handler: () => {
                      console.log('PurchaseMydeals to Action Clicked ok');
                    }
                  }],
                cssClass: 'error-message'
              });
              alert_failure.present();
              // reject("error : no data found.");
          }
        });
            // this.handleError(res,"FetchMydeals");
            // resolve(res);
      })
      .catch(function(e){
        _that.loader.dismiss();
          console.log("****************************************** component failure FetchMydeals"+e);
          if(e=="No internet connection")
          return;
          // _that.alertCtrlInstance.showAlert("", "", "");

          const alert_catch = _that.alertCtrl.create({
            title: 'Uh Oh. Please be patient. System&#39;s busy.',
            // subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
            message: "Please try again later",
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  console.log('Call to Action Clicked');
                }
              }],
            cssClass: 'success-message'
          });
          alert_catch.present();
          //alert("error user info FetchMydeals ");
          // reject(e);
      });
    // });

    
  }
  
}
