import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams, Events} from 'ionic-angular';
import {OleoleCatalogueListPage} from "../oleole-catalogue-list/oleole-catalogue-list";

// to navigate
import { SubscriptionsPage } from '../../../../pages/subscriptions/subscriptions';

// service
import { GatewayService } from '../../../../global/utilService';
import { ConstantData } from '../../../../global/constantService';
import { HandleError } from '../../../../global/handleerror';
import { AlertService } from '../../../../global/alert-service';
import { OleoleService } from '../../../../global/oleole-service';
import { DashboardService } from '../../../../pages/home/homeService';
import { SubscriptionsTabService} from '../../../subscriptions/subscriptionsTabService';
import { SubscriptionService } from "../../../subscriptions/subscriptionService";
import { GlobalVars } from "../../../../providers/globalVars";
/**
 * Generated class for the CataloguePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-catalogue',
  templateUrl: 'catalogue.html',
})
export class CataloguePage {

  rootNavCtrl: NavController;

  giftList : any;
  isOleole : boolean;
  
  listOfCategories : any;
  mobileNumber : any;
  userInfo : any;

  isCMP : boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events : Events,
    public dashboardService : DashboardService,
    public oleoleService : OleoleService,
    public gatewayService:GatewayService, 
    public handleerror: HandleError, 
    private alertCtrlInstance: AlertService,
    public subscriptionsTabService: SubscriptionsTabService,
    public subscriptionService : SubscriptionService,
    public globalVar: GlobalVars,
    private renderer: Renderer2
  ) {
    
    
    // selected mobile number
    this.mobileNumber = this.gatewayService.GetMobileNumber();

    // type of msisdn number 
    this.userInfo = this.dashboardService.GetUserInfo();

    console.log("catalogue ole ",navParams.data);

    // need when get update for received gift api
    // this.rootNavCtrl = navParams.get('rootNavCtrl');
    this.rootNavCtrl = this.navCtrl;
    this.isOleole = navParams.data.isOleole;

    // this.oleoleVoucherImageSrc = {
    //   "Fashion_Valet": "./assets/img/oleole-product/2304x1296_fashionvalet.jpg",
    //   "Melissa_Shoes": "./assets/img/oleole-product/2304x1296_melissa.jpg",
    //   "Something_Borrowed": "./assets/img/oleole-product/2304x1296_something_borrowed.jpg",
    //   "ZALIA": "./assets/img/oleole-product/2304x1296_zelia.jpg",
    //   "ZALORA": "./assets/img/oleole-product/2304x1296_zalora.jpg",
    //   "Fave": "./assets/img/oleole-product/fave_1.jpeg",
    //   "Taobao_Collection": "./assets/img/oleole-product/taobao_collection.jpeg",
    //   "11street": "./assets/img/oleole-product/2304x1296_11_street.jpg",
    //   "LAZADA": "./assets/img/oleole-product/2304x1296_lazada.jpg",
    //   "Klook":"./assets/img/oleole-product/klook_2.jpeg",
    //   "Steam":"./assets/img/oleole-product/steam_1.jpeg"
    // };

      // {'name':'Prepaid E-Gifts','keyword':'PREPAID','imgsrc':'./assets/img/oleole-product/xpax_internet_add_ons.jpg'},
      // {'name':'Postpaid E-Gifts','keyword':'POSTPAID','imgsrc':'./assets/img/oleole-product/postpaid_int_addon.jpg'},

      this.userInfo = this.dashboardService.GetUserInfo();

      // to hide vouchers if number is not cmp
      var prodPromeName =this.userInfo.ListOfServices.Services.ProdPromName;
      var prodPromeName = prodPromeName.toLowerCase(); 
  
      var adminNumber = this.userInfo["ListOfServices"].Services.BillingType;
  
      if(adminNumber == "Billable" && prodPromeName.indexOf("cmp") != 0)
        this.isCMP = true;
      else
        this.isCMP = false;

    this.listOfCategories = [
      {'name':'Prepaid E-Gifts','keyword':'PREPAID','imgsrc':'./assets/img/oleole-product/prepaid_gifts.jpg','isCmp':true},
      {'name':'Postpaid E-Gifts','keyword':'POSTPAID','imgsrc':'./assets/img/oleole-product/celcom_gifts.jpg','isCmp':true},
      {'name':'Department Stores','keyword':'department_stores','imgsrc':'./assets/img/oleole-product/department_store.jpg','isCmp':this.isCMP },
      {'name':'Fashion & Apparel','keyword':'fashion','imgsrc':'./assets/img/oleole-product/fashion.jpg','isCmp':this.isCMP },
      {'name':'Travel, Beauty & Food','keyword':'beauty','imgsrc':'./assets/img/oleole-product/travel_beauty_food.jpg','isCmp':this.isCMP },
      {'name':'Games & Entertainment','keyword':'games','imgsrc':'./assets/img/oleole-product/game_entertainment.jpg','isCmp':this.isCMP },
      {'name':'Hot Picks & Promos','keyword':'hardcoded_hotpicks','imgsrc':'./assets/img/oleole-product/hotpicks_promos.jpg','isCmp':this.isCMP } // taken from above keyword
    ];

    console.log("CataloguePage -> prodPromeName  = ",prodPromeName ,"\n adminNumber = ",adminNumber,"\n isCMP = ",this.isCMP,"\n listOfCategories = ",this.listOfCategories);
    
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("OleOle catalogue", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CataloguePage');
    

    

    this.events.subscribe('oleole:created', () => {
      console.log("oleole event");
      if(this.userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid") {  
        // this.subInternetCategoryPlan = this.subscriptionService.getUserInterSubscriptionInfoPostpaid();
        this.giftList = this.oleoleService.GetGiftList();
        console.log("this.giftList ",JSON.stringify(this.giftList));
        // console.log("sub-internet postpaid ", JSON.stringify(this.subInternetCategoryPlan));
      }
      else {
        this.giftList = this.oleoleService.GetGiftList();
        console.log("this.giftList ",JSON.stringify(this.giftList));
        // this.subInternetCategoryPlan = this.subscriptionService.getUserInterSubscriptionInfoPrePaid();
        // console.log("sub-internet prepaid ", JSON.stringify(this.subInternetCategoryPlan));
      }
    });
  }


  openCatalogueListPage (giftName) {
    this.subscriptionsTabService.SetTabInfo(0); 
    console.log("CataloguePage -> openCatalogueListPage() -> giftName = ",giftName);

    // if (this.subscriptionService.isCapZone()) {
    //   this.subscriptionService.presentConfirm();
    // } else {
      var _that = this;
    
      var loader = _that.gatewayService.LoaderService();
    
      loader.present();

      var params = {
        "msisdn": _that.mobileNumber,
        "keyword":giftName,  //e.g. "fashion"
        "txn_id":_that.gatewayService.generateRandomNum(16)
      };

      var params_MCP_XPAX = {
        "msisdn": _that.mobileNumber,
        "operation_id":giftName,  //e.g. "PREPAID" , "POSTPAID"
        "txn_id":_that.gatewayService.generateRandomNum(16)
      };

      // var params_XPAX = {
      //   "msisdn": _that.mobileNumber,
      //   "operation_id":"PREPAID",  //e.g. "fashion"
      //   "txn_id":_that.gatewayService.generateRandomNum(16)
      // };

      if(giftName != 'PREPAID' && giftName != 'POSTPAID') {
        _that.gatewayService.CallAdapter(ConstantData.adapterUrls.GiftCat,"POST",params).then(function(res){
          console.log("************************************* component success oleole giftlist voucher "+JSON.stringify(res));
          _that.handleerror.handleErrorGiftListForCat(res,"GiftCat").then((result) => {
              if(result == true) {
                  console.log("hai if Oleole GiftList voucher for PostpaidAndPrepaid ",result);
                  // resolve(res);
                  
                  if(res['VOUCHERCATALOGUE']['ErrorCode'])
                    _that.alertCtrlInstance.showAlert("", "Voucher Out of stock.", "OK");
                  else {
                    _that.oleoleService.SetGiftList(res);
                    _that.rootNavCtrl.push(OleoleCatalogueListPage, {"giftName" : giftName,"rootNavCtrl":_that.rootNavCtrl});
                  }
                    
                  // _that.events.publish('oleole:created'); 
                  loader.dismiss();
              }
              else {
                  console.log("hai else Oleole GiftList voucher For PostpaidAndPrepaid ",result);
                  loader.dismiss();
                  if(res['VOUCHERCATALOGUE']['ErrorCode'])
                    _that.alertCtrlInstance.showAlert("", "Voucher Out of stock.", "OK");
                  else 
                  _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
                  // reject("error : no data found.");
              }
          });
          // this.handleError(res,"FetchMydeals");
          // resolve(res);
        })
        .catch(function(e){
          console.log("****************************************** component failure Oleole gift voucher  "+e);
          console.log("error user info Oleole gift voucher ");
          loader.dismiss();
          if(e=="No internet connection")
          return;
          _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
          // reject(e);
        });
      }
      else if(giftName == 'PREPAID' || giftName == 'POSTPAID') {

        _that.gatewayService.CallAdapter(ConstantData.adapterUrls.GiftListForPostpaidAndPrepaid,"POST",params_MCP_XPAX).then(function(res){
          console.log("************************************* component success oleole giftlist MCP & Xpax "+JSON.stringify(res));
          _that.handleerror.handleErrorGiftListForPostpaidAndPrepaid(res,"GiftListForPostpaidAndPrepaid").then((result) => {
              if(result == true) {
                  console.log("hai if Oleole GiftList MCP & Xpax  for PostpaidAndPrepaid ",result);
                  // resolve(res);

                  // var sim_type = (_that.userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid") ? "POSTPAID" : "PREPAID";
                  // _that.oleoleService.SetXpaxGiftListObj(giftName);
                  var choose_plan = giftName;
                  console.log("CataloguePage -> openCatalogueListPage() -> sim_type ",choose_plan);
                  // _that.events.publish('userOleole:created');
                  
                  _that.rootNavCtrl.push(SubscriptionsPage, {"typeOfGift" : choose_plan,"mcp_and_xpax":res,"giftName" : giftName});
                    
                  // _that.events.publish('oleole:created'); 
                  loader.dismiss();
              }
              else {
                  console.log("hai else Oleole GiftList MCP & Xpax For PostpaidAndPrepaid ",result);
                  loader.dismiss();
                  _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
                  // reject("error : no data found.");
              }
          });
          // this.handleError(res,"FetchMydeals");
          // resolve(res);
        })
        .catch(function(e){
          console.log("****************************************** component failure Oleole gift MCP & Xpax "+e);
          console.log("error user info Oleole gift MCP & Xpax");
          loader.dismiss();
          if(e=="No internet connection")
          return;
          _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
          // reject(e);
        });
      }
    // }


    // this.rootNavCtrl.push(OleoleCatalogueListPage, {"giftName" : giftName});
  }
}
