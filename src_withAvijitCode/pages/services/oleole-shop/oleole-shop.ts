import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { CataloguePage } from "./catalogue/catalogue";
import { GiftsRecievedPage } from "./gifts-recieved/gifts-recieved";
// service
import { GatewayService } from '../../../global/utilService';
import { ConstantData } from '../../../global/constantService';
import { HandleError } from '../../../global/handleerror';
import { AlertService } from '../../../global/alert-service';
import { OleoleService } from '../../../global/oleole-service';
import { DashboardService } from '../../../pages/home/homeService';

/**
 * Generated class for the OleoleShopPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-oleole-shop',
  templateUrl: 'oleole-shop.html',
})
export class OleoleShopPage {

    loader : any; // loader 
    mobileNumber :any; // mobile number

    tab1Page: any = CataloguePage;
    tab2Page: any = GiftsRecievedPage;
   
    ololeParams: any;
    userInfo : any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public gatewayService:GatewayService, 
    public handleerror: HandleError, 
    private alertCtrlInstance: AlertService,
    public oleoleService : OleoleService,
    public events : Events,
    public dashboardService : DashboardService,
    private renderer: Renderer2
  ) {
    this.mobileNumber = this.gatewayService.GetMobileNumber();
    console.log("oleole is ole ",this.navParams.get('isOleole'))
    this.ololeParams = {
      isOleole : this.navParams.get('isOleole')
    };
    this.userInfo = this.dashboardService.GetUserInfo();
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Oleole shop", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OleoleShopPage');
  }

   // service
  ngOnInit() {

    var _that = this;
   
    // _that.loader = _that.gatewayService.LoaderService();
    // //loader.duration = 2000;
    // _that.loader.present()

    

    // var params = {
    //   "msisdn": _that.mobileNumber,
    //   "operation_id":  (userInfo.ListOfServices.Services.ServiceType == "Postpaid" || userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid") ? "POSTPAID" : "PREPAID",
    //   "txn_id": _that.gatewayService.generateRandomNum(16)
    // };
    
    var params = {
      "msisdn": _that.mobileNumber,
      "keyword":"fashion",
      "txn_id":_that.gatewayService.generateRandomNum(16)
    };
   
    console.log("params ole giftlist ",JSON.stringify(params));

    // return new Promise((resolve, reject) => {
      // GiftListForPostpaidAndPrepaid

      // _that.gatewayService.CallAdapter(ConstantData.adapterUrls.GiftCat,"POST",params).then(function(res){
      //   console.log("************************************* component success oleole giftlist "+JSON.stringify(res));
      //   _that.handleerror.handleErrorGiftListForCat(res,"GiftCat").then((result) => {
      //       if(result == true) {
      //           console.log("hai if Oleole GiftList For PostpaidAndPrepaid ",result);
      //           // resolve(res);
                
      //           _that.oleoleService.SetGiftList(res);   
      //           _that.events.publish('oleole:created'); 
      //           _that.loader.dismiss();
      //       }
      //       else {
      //           console.log("hai else Oleole rGiftList For PostpaidAndPrepaid ",result);
      //           _that.loader.dismiss();
      //           _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
      //           // reject("error : no data found.");
      //       }
      //   });
      //       // this.handleError(res,"FetchMydeals");
      //       // resolve(res);
      // })
      // .catch(function(e){
      //   console.log("****************************************** component failure Oleole handleErrorGiftListForPostpaidAndPrepaid "+e);
      //   console.log("error user info Oleole handleErrorGiftListForPostpaidAndPrepaid ");
      //   _that.loader.dismiss();
      //   _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
      //   // reject(e);
      // });
    // });
  }

  // GiftListForPostpaidAndPrepaid() {

  //   var _that = this;

  //   var params = {
  //     "msisdn": _that.mobileNumber,
  //     "operation_id":  (_that.userInfo.ListOfServices.Services.ServiceType == "Postpaid" || userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid") ? "POSTPAID" : "PREPAID",
  //     "txn_id": _that.gatewayService.generateRandomNum(16)
  //   };

  //   _that.gatewayService.CallAdapter(ConstantData.adapterUrls.GiftListForPostpaidAndPrepaid,"POST",params).then(function(res){
  //     console.log("************************************* component success oleole addon giftlist "+JSON.stringify(res));
  //     _that.handleerror.handleErrorGiftListForPostpaidAndPrepaid(res,"GiftListForPostpaidAndPrepaid").then((result) => {
  //         if(result == true) {
  //             console.log("hai if Oleole GiftList For PostpaidAndPrepaid ",result);
  //             // resolve(res);
              
  //             _that.oleoleService.SetGiftList(res);   
  //             _that.events.publish('mydeal:created'); 
  //             _that.loader.dismiss();
  //         }
  //         else {
  //             console.log("hai else Oleole rGiftList For PostpaidAndPrepaid ",result);
  //             _that.loader.dismiss();
  //             _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
  //             // reject("error : no data found.");
  //         }
  //     });
  //         // this.handleError(res,"FetchMydeals");
  //         // resolve(res);
  //   })
  //   .catch(function(e){
  //     console.log("****************************************** component failure Oleole handleErrorGiftListForPostpaidAndPrepaid "+e);
  //     console.log("error user info Oleole handleErrorGiftListForPostpaidAndPrepaid ");
  //     _that.loader.dismiss();
  //     _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
  //     // reject(e);
  //   });
  // }


}
