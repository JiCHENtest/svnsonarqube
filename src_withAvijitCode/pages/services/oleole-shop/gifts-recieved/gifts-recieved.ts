import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {OleoleGiftsRecievedListPage} from "../oleole-gifts-recieved-list/oleole-gifts-received-list";
import { GatewayService } from '../../../../global/utilService';

/**
 * Generated class for the GiftsRecievedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gifts-recieved',
  templateUrl: 'gifts-recieved.html',
})
export class GiftsRecievedPage {

  rootNavCtrl: NavController;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    private renderer: Renderer2, public gatewayService: GatewayService) {
    this.rootNavCtrl = navParams.get('rootNavCtrl');

  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("OleOle gift recieved", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GiftsRecievedPage');
  }

  openGiftsRecievedListPage (data) {

    this.rootNavCtrl.push(OleoleGiftsRecievedListPage);
  }

}
