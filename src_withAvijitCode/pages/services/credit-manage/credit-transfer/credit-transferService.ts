import { GatewayService } from '../../../../global/utilService';
import { Injectable } from '@angular/core';
import { HandleError } from '../../../../global/handleerror';
import {Observable} from 'rxjs/Rx';

import { ConstantData } from '../../../../global/constantService';
import { CreditManageService } from "../CreditManageService";
import { ReloadService } from '../../../../global/reloadServices';

@Injectable()
export class CreditTransferService{

    constructor(public handleerror: HandleError, public gatewayService:GatewayService, public reloadService:ReloadService,  public creditManageService: CreditManageService){
    };
    
    getAccountStatus(){
      console.log('mobile number' + this.reloadService.gettingLoginNumber());
      return new Promise((resolve,reject)=>{ 
        var res = this.creditManageService.FeachValidityBlance(this.reloadService.gettingLoginNumber()).then((res)=>{
          console.log(res);
          var balanceArray = res["Envelope"].Body.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.BalanceRecordList.BalanceRecord;
          var creditBlance = 0;  
          console.log("Checking Data "+JSON.stringify(balanceArray));
           if(balanceArray.length == undefined){
             if(balanceArray.AccountType == "2000"){
             creditBlance =  Number(balanceArray.Balance) /10000;
          }
          } else{
          balanceArray.forEach(element => {
             if(element.AccountType == "2000"){
               creditBlance =  Number(element.Balance) /10000;               
             }
            });
           } 
           if(creditBlance>=2){
             resolve(creditBlance); 
           }else{
             reject("Minimum balance required is RM2.00");
           }
        });   
      }); 
    } 

    submitCreditTransfer(receiverMobNumber,amount){
      try{
        var params = {
            "MOBILENUM":this.creditManageService.GetSelectMobileNumber(),
            "receiverMobNumber":receiverMobNumber,
            "amount":amount
          };
        return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.SubmitCreditTransfer,"POST",params).then((res)=> {
          resolve(res);
        })
        .catch(function(e){
         reject(e);
      });
      });
      }catch(e){

      }
    } 

    getRoamingInfo(){
      return this.gatewayService.GetVLR();
    }

    getTotalBillAmount(value){
      return new Promise((resolve,reject)=>{
        var charge = "0.50";
        var val = parseFloat(value).toFixed(2) + parseFloat(charge).toFixed(2);
        console.log("val" + val);
        resolve(parseFloat(val).toFixed(2));
      });
    } 

    validate(phoneNumber){
      return new Promise((resolve,reject)=>{
        resolve(true);
      });
    }

    
}