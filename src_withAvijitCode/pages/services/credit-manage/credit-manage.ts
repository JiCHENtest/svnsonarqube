import { Component, ViewChild, Renderer2 } from '@angular/core';
import { NavController, NavParams, Events, LoadingController, Navbar } from 'ionic-angular';
import { ValidityExtensionPage } from './validity-extension/validity-extension';
import { CreditTransferPage } from './credit-transfer/credit-transfer';
import { CreditAdvancePage } from './credit-advance/credit-advance';
import { GlobalVars } from '../../../providers/globalVars';
import { SuperTabsController } from '../../../components/custom-ionic-tabs/index';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
import { ReloadPopoverComponent } from '../../../components/reload-popover/reload-popover';
import { GatewayService } from '../../../global/utilService';
import { CreditManageService } from './CreditManageService';
import { ConnectTacTransferPage } from '../../../pages/connect-tac-transfer/connect-tac-transfer';
import { ServicesPage } from '../services';


@Component({
  selector: 'page-credit-manage',
  templateUrl: 'credit-manage.html',
})
export class CreditManagePage {
  @ViewChild(Navbar) navBar: Navbar;
  tab1Page: any = CreditTransferPage;
  tab2Page: any = ValidityExtensionPage;
  tab3Page: any = CreditAdvancePage;
  tab1Params = { id: 0 };
  tab2Params = { id: 1 };
  tab3Params = { id: 2 };
  selectOptions: any;
  loader: any;
  creditBlance: any;
  lineValidity: any;
  phoneNumber: any;
  isCreditBalanceLoaded: boolean = false;
  isValidityExt: boolean;

  selectedTab: number = 0;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public globalVars: GlobalVars,
    private tabsCtrl: SuperTabsController,
    public popoverCtrl: PopoverController,
    public gatewayService: GatewayService,
    public creditMService: CreditManageService,
    public events: Events,
    public loadingCtrl: LoadingController,
    private renderer: Renderer2) {
    console.log("constructor called credit manage");
    //   var isConfirmed=true;

    this.selectOptions = {
      title: 'Phone Numbers',
      mode: 'ios',
      cssClass: this.globalVars.getCurrentTheme() + " mypopup"
    };
    this.selectedTab = this.navParams.data;
    console.log("this.selectedTab -->inside credit manage " + this.selectedTab);
    this.tab1Params = { id: this.selectedTab };
    this.tab2Params = { id: this.selectedTab };
    this.tab3Params = { id: this.selectedTab };

    this.loader = this.gatewayService.LoaderService();

    /*Subscribing to event to update balance validity*/
    events.subscribe('reloadHeaderManageCredit', () => {
      console.log('CreditManagePage -> constructor -> subscribe -> reloadingHeader');
      this.ionViewDidLoad();
    });
    events.subscribe('goToOTPPage', (params) => {
      this.navCtrl.push(ConnectTacTransferPage, { "params": params });
    });

    events.subscribe('goToServicePage', (params) => {
      //go to parent page
      this.navCtrl.pop();
    });
    events.subscribe('goToCreditTransfer', () => {
      this.navCtrl.push(CreditManagePage, 0);
    });
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Credit manage", this.renderer);
  }

  publishTabSelectedEvent(index){
    let eventname = "creditmanage"+index;
    this.events.publish(eventname);
  }

  backButtonClick() {
    console.log('// dos omething');
    this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3));
  }

  onTabSelect(ev: any) {
    console.log('Tab selected ', 'Index: ' + ev.index, 'Unique ID: ' + ev.id + " this.selectedTab " + this.selectedTab);
    this.publishTabSelectedEvent(ev.index);
  }


  ionViewDidLoad() {
    this.tabsCtrl.enableTabsSwipe(false, 'creditManage');
    this.getBalanceValidity();
  }

  ngOnDestroy() {
    this.events.unsubscribe('goToOTPPage');
    this.events.unsubscribe('reloadHeaderManageCredit');
    this.events.unsubscribe('goToServicePage');
    this.events.unsubscribe('goToCreditTransfer');
    this.events.unsubscribe('callTransferAmount');
  }

  getBalanceValidity() {
    this.creditBlance = "--";
    this.lineValidity = "--";
    this.phoneNumber = this.creditMService.GetSelectMobileNumber();
    this.loader = this.gatewayService.LoaderService();
    var _that = this;
    this.loader.present().then(() => {

      this.creditMService.getAccountStatus().then((res) => {
        // var balanaces = res["creditBlanceRMValue"];
        this.creditBlance = res["creditBlanceRMValue"];
      //  if(balanaces){
      //       if(balanaces.split('.')[1] == undefined){
      //       balanaces+".00";
      //     }else if(balanaces.split('.')[1].length <= 1){
            
      //     }else{

      //     }
      //  }
      // console.log('credit manage page balanaces', balanaces);
      //  if(balanaces.substring(0,2) == "RM"){
      //       this.creditBlance = "RM"+Number(balanaces.substring(2)).toFixed(2)
      //  }else{
      //    var balanaces = res["creditBlanceRMValue"];
      //  }
      

        var lineValidity_temp = res["lineValidity"];
        var year = lineValidity_temp.match(/.{1,4}/g);
        var date = year[1].match(/.{1,2}/g);
        this.lineValidity = date[1] + '/' + date[0] + '/' + year[0];
        this.isCreditBalanceLoaded = true;
        _that.loader.dismiss();
      }, (res) => {

        console.log("CreditManagePage -> getBalanceValidity -> res");
        _that.loader.dismiss();
        if (res == "No internet connection")
          return;
      });
    });
  }

}
