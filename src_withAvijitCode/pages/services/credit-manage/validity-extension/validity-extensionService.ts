import { GatewayService } from '../../../../global/utilService';
import { Injectable } from '@angular/core';
import { HandleError } from '../../../../global/handleerror';

import { ConstantData } from '../../../../global/constantService';
import { CreditManageService } from '../CreditManageService';


@Injectable()
export class ValidityExtensionService{

    constructor(public handleerror: HandleError, 
      public gatewayService:GatewayService, 
      public creditManageService: CreditManageService){
    };

    submitValidityExtension(amount){
      try{
        var params = {
            "mobNumber":this.creditManageService.GetSelectMobileNumber(),
            "amount":amount,
            "validityDate":this.creditManageService.getLineValidity()
          };
        return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.SubmitValidityExtension,"POST",params).then((res)=> {
          resolve(res);
        })
        .catch(function(e){
         reject(e);
      });
      });
      }catch(e){

      }
    } 
}