import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DeliveryInformationPage } from "../delivery-information/delivery-information";
import { SimReplacementProvider } from '../sim-replacementProvider';
import { StoreLocatorSelectStorePage } from "../../../support/store-locator/select-store/store-locator-select-store";
import { GatewayService } from '../../../../global/utilService';

@Component({
  selector: 'page-delivery-options',
  templateUrl: 'delivery-options.html',
})
export class DeliveryOptionsPage {

  selectedReason: string;
  deliveryOption: string;
  private SUCESS_TITLE:string = "Sim Replacement Success";
  private ERROR_TITLE:string = "Sim Replacement Error";

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public simReplacementProvider: SimReplacementProvider, 
    public alertCtrl: AlertController,
    private renderer: Renderer2,
    private gatewayService: GatewayService) {
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("SIM replacement delivery options", this.renderer);
  }

  selectDeliveryOption(option) {
    this.deliveryOption = option;
  }

  ionViewDidLoad() {
    this.selectedReason = this.navParams.get('reason');
    console.log('ionViewDidLoad DeliveryOptionsPage');
  }

  goToDeliveryInformation() {
    if (this.deliveryOption == 'selfcollect') {
      this.navCtrl.push(StoreLocatorSelectStorePage);
    } else if (this.deliveryOption == 'delivery') {
      this.navCtrl.push(DeliveryInformationPage,{reason:this.selectedReason});
    }
  }

  presentmessage(messageInput,title) {
    const alert = this.alertCtrl.create({
      title: title,
      message: messageInput,
      buttons: ['Ok'],
      cssClass: 'success-message error-message'
    });
    alert.present();
  }

  presentConfirm(title, message, btn1, btn2, callback1, callback2) {
    let alert = this.alertCtrl.create({
      title: title,
      message: message,
      cssClass:'success-message error-message',
      buttons: [
        {
          text: btn2,
          role: 'cancel',
          cssClass:'submit-button',
          handler: () => {
            callback2();
            console.log('Cancel clicked');
          }
        },
        {
          text: btn1,
          cssClass:'submit-button',
          handler: () => {
            callback1(this);
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }

}  


