import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GatewayService } from '../../../global/utilService';
import { ReloadService } from '../../../global/reloadServices';
import { AlertService } from '../../../global/alert-service';
//import { FormBuilder,FormBuilder,validators} from '@angular/forms'; 
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Keyboard } from '@ionic-native/keyboard';

import { Events } from "ionic-angular";
import { analyticTransactionDetailsInterface } from '../../../global/interface/analytic-transaction-details';
/**
 * Generated class for the ReloadCodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reload-code',
  templateUrl: 'reload-code.html',
})
export class ReloadCodePage {
  buttonDisable: boolean;

  loader;
  mobilenumber;
  digitalPinForm: FormGroup;
  digitPin: any;
  transactionDetails: analyticTransactionDetailsInterface;

  constructor(public navCtrl: NavController, public formBuilder: FormBuilder, public navParams: NavParams,
    private alertCtrlInstance: AlertService, public gatewayService: GatewayService,
    public reloadService: ReloadService, public events: Events, private keyboard: Keyboard,
    private renderer: Renderer2) {
    this.buttonDisable = true;
    this.digitalPinForm = formBuilder.group({
      digitPin: ['', Validators.compose([Validators.maxLength(16), Validators.minLength(16), Validators.pattern('[0-9]*'), Validators.required])],
    });
  }

  ionViewDidEnter() {
    this.initiateAnalyticTransactionJourney();
  }

  initiateAnalyticTransactionJourney(){
    this.transactionDetails = {};
    this.gatewayService.resetAllAnalyticData();
    this.transactionDetails.jouneryDepth = "initiation";
    this.transactionDetails.transactProduct = "Reload PIN";
    this.gatewayService.setAnalyticTransactionJourney("Reload PIN", this.renderer, this.transactionDetails);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReloadCodePage');
  }

  onChangeTime(pin) {
    if (pin.length == 16)
      this.buttonDisable = false;
    else
      this.buttonDisable = true;
  }
  onclicklog() {
    console.log('nader');
  }
  validationPin(pinnumber) {
    //Analytic Part
    this.initiateAnalyticTransactionJourney();

    this.loader = this.gatewayService.LoaderService();
    this.loader.present();
    this.mobilenumber = this.reloadService.GetSelectMobileNumber();

    var SelectedMobileNumber = this.mobilenumber.slice(-9);


    var params = {
      "MobileNumber": SelectedMobileNumber,
      "Pin": pinnumber
    }

    //Analytic Part Start
    this.transactionDetails.jouneryDepth = "checkout";
    this.transactionDetails.transactMethod = "PIN";
    //Analytic Part End

    this.reloadService.CheckDigtalPin(params).then((res) => {
      var AddAmount;
      var TotalBalace;
      console.log("**********succ******pin" + JSON.stringify(res));
      var getBalaceDetails = res["Envelope"].Body.VoucherRechargeResultMsg;
      this.loader.dismiss();
      if (getBalaceDetails.VoucherRechargeResult != undefined) {
        AddAmount = res["Envelope"].Body.VoucherRechargeResultMsg.VoucherRechargeResult.FaceValue.CDATA;
        AddAmount = Number(AddAmount) / 10000;
        TotalBalace = res["Envelope"].Body.VoucherRechargeResultMsg.VoucherRechargeResult.NewBalance.CDATA;
        TotalBalace = Number(TotalBalace) / 10000;
      }


      var showMesssage = res["Envelope"].Body.VoucherRechargeResultMsg.ResultHeader.ResultDesc;

      if (showMesssage) {
        if (AddAmount) {

          //Analytic Part
          this.transactionDetails.transactValue = AddAmount;
          this.transactionDetails.jouneryDepth = "post";
          this.transactionDetails.transactResult = "success";

          this.events.publish('updateBalance', SelectedMobileNumber);
          // this.events.publish('updateBalance',SelectedMobileNumber);
          this.gatewayService.setFlagForDashboardReload(true);
          this.alertCtrlInstance.showAlert("Hurray!", "RM" + AddAmount + " has been added to " + this.mobilenumber, "OK");
          // this.alertCtrlInstance.showAlert( "Total Balance to your account", "RM"+TotalBalace  , "OK");
        } else {

          //Analytic Part
          this.transactionDetails.jouneryDepth = "post";
          this.transactionDetails.transactResult = "failed";

          if (showMesssage.CDATA == "The voucher card does not exist or is invalid.") {
            //this.gatewayService.setFlagForDashboardReload(true);

            console.log('after reload flag set true');
            this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid PIN", "Not a PIN in the Celcom database", "OK");
          } else if (showMesssage.CDATA == "The voucher card has been already used.") {
            this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid PIN", "Already used", "OK");
          } else {
            this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid PIN", "Expired", "OK");
          }

        }
      } else {

        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "failed";

        this.alertCtrlInstance.showAlert("Uh Oh. Something's Not Right", "Please try again later", "OK");
      }

      //Analytic Part
      this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);
      // this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");

    }).catch(err => {
      this.loader.dismiss();

      //Analytic Part
      this.transactionDetails.jouneryDepth = "post";
      this.transactionDetails.transactResult = "failed";
      this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);

      console.log("*****reload*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });

  }

  handlePin(e) {
    if (e.which === 13) {
      this.keyboard.close();
    }
  }

}
