import { Component, NgZone, Renderer2 } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { ChangeCreditcardModalPage } from "../../change-creditcard-modal/change-creditcard-modal";
import { GatewayService } from '../../../global/utilService';
import { ReloadService } from '../../../global/reloadServices';
import { AlertService } from '../../../global/alert-service';
import { DashboardService } from '../../../pages/home/homeService';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import * as sha1 from "sha1";
import { DatePipe } from '@angular/common';
import { AlertController } from 'ionic-angular';
import { Events } from "ionic-angular";
import { PayBillService } from '../../pay-bill/payBillService';
import { PaymentRecieptPage } from '../../payment-reciept/payment-reciept';
import { analyticTransactionDetailsInterface } from '../../../global/interface/analytic-transaction-details';

/**
 * Generated class for the CreditReloadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-credit-reload',
  templateUrl: 'credit-reload.html',
})

export class CreditReloadPage {
  selectedCard: string;
  paymentMethod = [
    {
      "type": "Credit/Debit Card",
      "icon": "custom-reload-card"
    },
    {
      "type": "Online Banking",
      "icon": "custom-reload-online"
    }

  ];

  //carArray = ["12345678912345123","0981237891234345","98545678912345666","2299447891234745"];
  cardArray: any;

  subTitle;
  selectedItems = {}
  indexValue: number;
  subIndexValue: number;
  selectedDiv: boolean;

  cardValue: any;
  cardType: string;
  mobilenumber;
  loader;
  flag: boolean;
  flag2: boolean;
  Cardflag: boolean;
  openingMethod: string;
  kadCeria: number;
  tokentPassing: any;
  statusSaveCard: boolean;
  loginNumber: number;
  disable: boolean;
  IsKADCeria: boolean;
  transactionDetails: analyticTransactionDetailsInterface;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
    private alertCtrlInstance: AlertService, public gatewayService: GatewayService,
    public theInAppBrowser: InAppBrowser, public datePipe: DatePipe, public reloadService: ReloadService,
    public conformAlert: AlertController, public events: Events, public zone: NgZone,
    public paybillService: PayBillService, public DashboardService: DashboardService,
    private renderer: Renderer2 ) {
  }


  ionViewDidEnter() {
    this.initiateAnalyticTransactionJourney();
  }

  initiateAnalyticTransactionJourney() {
    this.transactionDetails = {};
    this.gatewayService.resetAllAnalyticData();
    this.transactionDetails.jouneryDepth = "initiation";
    this.transactionDetails.transactProduct = "Credit reload";
    this.gatewayService.setAnalyticTransactionJourney("Credit reload", this.renderer, this.transactionDetails);
  }

  ionViewDidLoad() {

    //  this.loader = this.gatewayService.LoaderService();

    //  this.loader.present();
    this.flag = false;
    this.flag2 = false;
    this.Cardflag = false;
    this.statusSaveCard = false;
    this.disable = false;
    console.log('ionViewDidLoad CreditReloadPage');
    this.reloadService.FeachingCreditReloadPlans().then((res) => {
      //  this.loader.dismiss();
      this.subTitle = res;
    }).catch(err => {
      //  this.loader.dismiss();
      console.log("*****reload*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;

      if (this.reloadService.GetErrorStatu() == false) {
        this.reloadService.SetErrorStatus(true);
        this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      } else {

      }
      //this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
      //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });


    this.newCards();
    //this.checkingKadCeriaBonus();

  }
  newCards() {
    var loginNumber = this.reloadService.gettingLoginNumber();
    //var loginNumber = "0196277991 ";
    this.reloadService.FetchSavedCards(loginNumber).then((res) => {
      //this.loader.dismiss();
      console.log("*****saveCards*******" + JSON.stringify(res));
      if (res["savedCards"] == undefined) {
        this.flag = false;
        this.flag2 = false;
        this.Cardflag = false;
        this.statusSaveCard = false;
        this.disable = false;
        this.cardType = "";
      } else if (res["savedCards"].length >= 1) {
        this.cardType = "Credit/Debit Card";
        this.Cardflag = true;
        this.flag = true;
        this.flag2 = true;
        this.statusSaveCard = true;
        this.cardArray = [];
        var allcards = res["savedCards"];
        this.disable = true;
        this.selectedCard = res["savedCards"][0].masked;
        this.tokentPassing = res["savedCards"][0].TOKEN;
        allcards.forEach(element => {
          this.cardArray.push(element);
          console.log("*****saveCards*******" + JSON.stringify(element));
          // if (element.is_default == true) {
          //   this.selectedCard = element.masked;
          //   this.tokentPassing = element.TOKEN;
          // }
        });
      } else {
        this.flag = false;
        this.flag2 = false;
        this.Cardflag = false;
        this.statusSaveCard = false;
        this.disable = false;
        this.cardType = "";
      }

    }).catch(err => {
      console.log("*****saveCards Error*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      if (this.reloadService.GetErrorStatu() == false) {
        this.reloadService.SetErrorStatus(true);
        this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      } else {

      }
    });
  }
  presenChangeCreditModal() {
    let getCardArrayCard = this.cardArray;
    let selectedCard = this.selectedCard;
    let modal = this.modalCtrl.create(ChangeCreditcardModalPage, { selectedCard, getCardArrayCard });
    modal.present();

    modal.onWillDismiss(data => {
      console.log('selectedArray' + data);
      if (data != null) {
        if (data["masked"]) {
          this.cardType = "Credit/Debit Card";
          this.disable = true;
          this.Cardflag = true;
          this.selectedCard = data["masked"];
          this.tokentPassing = data["TOKEN"];
          this.flag2 = true;
        } else {
          this.cardType = "";
          this.flag = false;
          this.flag2 = false;
          this.selectedCard = "";
          this.tokentPassing = "";
          this.disable = false;
          this.selectedCard = "Use new card";
          this.addCss(0);
          // this.Cardflag = false;  
        }
        // this.selectedCard = data["masked"];
        //  this.tokentPassing = data["TOKEN"];
      }
    })
  }

  goToPay() {
    //Analytic Part
    this.initiateAnalyticTransactionJourney();
    if (this.flag2 == true && this.flag == true) {
      this.cardValue = "30.00";
      this.cardType = "Credit/Debit Card";
    } else if (this.flag == true) {
      if (!this.cardValue)
        this.cardValue = "30.00";
    }
    if (this.cardValue && this.cardType) {

      var paymentType = "";
      if (this.cardType == "Credit/Debit Card") {
        paymentType = "1";
        this.openUrlPrepaid(this.cardValue, paymentType);
      } else {
        paymentType = "7";
        this.openUrlPrepaid(this.cardValue, paymentType);
      }
    } else {
      this.alertCtrlInstance.showAlert("Uh Oh. No details detected", "Please select reload amount and payment method.", "OK");
    }

  }
  cardShow(selectedCard) {
    var cardShow = selectedCard.replace(/.(?=.{4})/g, '*');
    return cardShow;
  }
  openUrlPrepaid(value, type) {
    this.loader = this.gatewayService.LoaderService();
    this.loader.present();
    this.mobilenumber = this.reloadService.GetSelectMobileNumber();
    this.loginNumber = this.reloadService.gettingLoginNumber();
    // var testMobilenumber = "0136430806"

    var _that = this;
    var amount = parseFloat(value).toFixed(2);
    var params = {};
    if (this.Cardflag && this.tokentPassing) {
      if (type == "7") {
        params = { "msisdn": this.mobilenumber, "paymentMethod": type, "totalAmount": amount, "loginID": this.loginNumber, "isInternetReload": false };
      } else {
        var typeis = "4"
        params = { "msisdn": this.mobilenumber, "paymentMethod": typeis, "totalAmount": amount, "loginID": this.loginNumber, "isInternetReload": false };

      }
    } else {
      params = { "msisdn": this.mobilenumber, "paymentMethod": type, "totalAmount": amount, "loginID": this.loginNumber, "isInternetReload": false };
    }



    console.log("*****Payment params*****" + JSON.stringify(params));
    this.reloadService.PrepaidPaymentGatway(params).then((res) => {

      //Analytic Part Start
      this.transactionDetails.jouneryDepth = "checkout";
      this.transactionDetails.transactValue = amount;
      if (this.cardType == "Credit/Debit Card") {
        this.transactionDetails.transactMethod = "Credit card";
      } else {
        this.transactionDetails.transactMethod = "Online banking";
      }
      //Analytic Part End

      if (res["msgStatus"] == "Success") {
        var statusVariable = "";
        if (this.tokentPassing && this.Cardflag) {
          statusVariable = res["paymentURL"] + '&prePaymentToken=' + this.tokentPassing;
        } else {
          statusVariable = res["paymentURL"];
        }
        console.log("*********** component success statusVariable" + JSON.stringify(statusVariable));
        this.loader.dismiss();
        console.log("*********** component success PostpaidPaymentURL" + JSON.stringify(res));
        const browser = _that.theInAppBrowser.create(statusVariable, '_blank', 'location=no');
        browser.show();
        browser.on("exit").subscribe((e) => {
          console.log("exit" + e.url);
          this.IsKADCeria = this.DashboardService.IsKADCeria();
          this.PaymentStatus(res["orderId"], this.IsKADCeria);
          //this.checkingKadCeriaBonus(res["orderId"]);
        }, err => {
          console.log("error" + err);
        });
        browser.on("loadstop").subscribe((e) => {

          console.log("loadstop" + e.url);
          if (e.url.startsWith(res["responseUrl"])) {
            browser.close();
          } else {

          }
        }, err => {
          console.log("error" + err);
        });
        browser.on("loaderror").subscribe((e) => {
          browser.close();
          console.log("loadstop" + e.url);
        }, err => {
          console.log("error" + err);
        });
      } else {
        this.loader.dismiss();

        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "failed";

        this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");
      }

      //Analytic Part
      this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);
    }).catch(err => {
      this.loader.dismiss();

      //Analytic Part
      this.transactionDetails.jouneryDepth = "post";
      this.transactionDetails.transactResult = "failed";
      this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);

      console.log("*************** component failure PostpaidPaymentURL" + err);
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");

    });
  }

  DataAlignment(gettingDate) {
    var day = gettingDate.slice(0, 8);
    var year = day.match(/.{1,4}/g);
    var date = year[1].match(/.{1,2}/g);
    var finalDate = date[1] + '/' + date[0] + '/' + year[0];
    return finalDate;
  }

  PaymentStatus(orderId, kadStatus) {
    this.loader = this.gatewayService.LoaderService();

    this.loader.present();
    var subNumber = this.reloadService.GetSelectMobileNumber();
    var loginNumber = this.reloadService.gettingLoginNumber();
    var checkingNumer = this.gatewayService.GetMobileNumber();
    //var amount = parseFloat(this.cardValue).toFixed(2);

    var params = { "loginID": loginNumber, "orderId": orderId, "msisdn": subNumber, "totalAmount": this.cardValue, "isKadCeria": kadStatus, "credit": true };

    //fetchTransactionDetails
    console.log("*****PaymentStatus params*****" + JSON.stringify(params));
    this.reloadService.fetchTransactionDetails(params).then((res) => {
      // this.loader.dismiss();


      var transactionDate = res["resultSet"][0].TRANSDATE;
      transactionDate = this.DataAlignment(transactionDate);
      var transactionId = res["resultSet"][0].REFERID;
      this.reloadService.setIsreload(true);
      console.log("*****PaymentStatus*****" + JSON.stringify(res));

      if (res["isPaymentSuccess"] == true && checkingNumer == res["resultSet"][0].MSISDN) {
        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "success";

        this.gatewayService.setFlagForDashboardReload(true);
        this.reloadService.setTransactionDate(transactionDate);
        this.reloadService.setTransactionId(transactionId);
        this.loader.dismiss();
        let modal = this.modalCtrl.create(PaymentRecieptPage);
        modal.present();

        modal.onDidDismiss(data => {
          this.newCards();
          console.log("inside ondiddismissss");
          this.loader = this.gatewayService.LoaderService();
          this.loader.present();
          this.cardType = "";
          this.flag2 = false;
          this.events.publish('updateBalance', res["resultSet"][0].MSISDN);

          if (res["hasFreebies"] == true) {
            var ECEM = "ECEMEligiblity";
            this.events.publish('CheckECEM', ECEM);
            this.EcemAlert(res["resultSet"][0].TOTALAMOUNT, res["resultSet"][0].MSISDN);
          } else if (res["kadCeriaMsg"]) {
            console.log("*****kadCeriaMsg*****");
            this.kadCeriaAlert(res["resultSet"][0].TOTALAMOUNT, res["resultSet"][0].MSISDN);
          } else {
            this.normalAlert(res["resultSet"][0].TOTALAMOUNT, res["resultSet"][0].MSISDN);
          }
        });
      } else if (res["isPaymentSuccess"] == true) {

        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "success";

        this.gatewayService.setFlagForDashboardReload(true);
        this.reloadService.setTransactionDate(transactionDate);
        this.reloadService.setTransactionId(transactionId);
        this.loader.dismiss();
        let modal = this.modalCtrl.create(PaymentRecieptPage);
        modal.present();

        modal.onDidDismiss(data => {
          this.newCards();
          this.loader = this.gatewayService.LoaderService();

          this.loader.present();

          this.cardType = "";
          this.flag2 = false;
          this.events.publish('updateBalance', res["resultSet"][0].MSISDN);
          this.normalAlert(res["resultSet"][0].TOTALAMOUNT, res["resultSet"][0].MSISDN);
        });

      } else {
        this.loader.dismiss();

        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "failed";

        if (res["resultSet"][0].REASONCODE == "51011") {
          this.alertCtrlInstance.showAlert("Uh Oh. That's a Wrong CVC Number", "Please enter a valid 3-digit CVC number !!!", "OK");
        } else if (res["resultSet"][0].REASONCODE == "51012") {
          this.alertCtrlInstance.showAlert("Uh Oh. Got Another Credit Card?", "We prefer VISA or Mastercard.", "OK");
        } else if (res["resultSet"][0].REASONCODE == "51013") {
          this.alertCtrlInstance.showAlert("Uh Oh. Wrong Expiry Date?", "Please enter a valid expiry date", "OK");
        } else if (res["resultSet"][0].REASONCODE == "30000" || res["resultSet"][0].REASONCODE == "32001" || res["resultSet"][0].REASONCODE == "33001" || res["resultSet"][0].REASONCODE == "37036 ") {
          this.alertCtrlInstance.showAlert("Hang On. We're Working on Your Transaction", "Successful transaction will be reflected in transaction history", "OK");
        } else {
          this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");
        }
      }

      //Analytic Part
      this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);

    }).catch(err => {
      this.loader.dismiss();

      //Analytic Part
      this.transactionDetails.jouneryDepth = "post";
      this.transactionDetails.transactResult = "failed";
      this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);

      console.log("*****PaymentStatus*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");
    });

    //{"loginID":"0138372028","orderId":"X20160903101006","msisdn":"60136430242","totalAmount":27,"isKadCeria":0,"credit":true}
  }


  EcemAlert(amount, number) {
    this.loader.dismiss();
    let ECEMAlert = this.conformAlert.create({
      title: "Yay! Reload and Get Bonus.",
      subTitle: "RM" + amount + " has been added to " + number + ". Enjoy 1 or 2 reload bonus for reloading RM10 or RM30",
      buttons: [
        {
          text: "DONE",
          cssClass: "submit-button",
          handler: () => {
            console.log("ALolow");
            //  this.openingMethod = "false";
            //  this.openTheContactConform();
          }
        }, {
          text: "CLAIM NOW",
          cssClass: "submit-button",
          role: "BUY ADD-ON",
          handler: () => {
            this.events.publish('openECEMNumber');
            //this.gatewayService.notifyObservers("Reload");

          }
        }
      ],
      cssClass: 'success-message',
      enableBackdropDismiss: false
    });
    ECEMAlert.present();
  }
  kadCeriaAlert(amount, number) {
    this.loader.dismiss();
    let alert = this.conformAlert.create({
      title: "Yay! Reload and Get Bonus.",
      subTitle: "RM" + amount + " has been added to " + number + " . Enjoy additional Call and SMS bonus within Celcom network",
      buttons: [
        {
          text: "DONE",
          cssClass: "submit-button",
          handler: () => {
            console.log("ALolow");
            // this.openingMethod = "false";
            //  this.openTheContactConform();
          }
        }, {
          text: "BUY ADD-ON",
          cssClass: "submit-button",
          role: "BUY ADD-ON",
          handler: () => {
            this.events.publish('openECEMNumber');
            //this.gatewayService.notifyObservers("Reload");
          }
        }
      ],
      cssClass: 'success-message',
      enableBackdropDismiss: false
    });
    alert.present();
  }
  normalAlert(amount, number) {
    this.loader.dismiss();
    let normalAlert = this.conformAlert.create({
      title: "Hurray!",
      subTitle: "RM" + amount + " has been added to " + number,
      buttons: [
        {
          text: "DONE",
          cssClass: "submit-button",
          handler: () => {
            //console.log("ALolow");
            //  this.openingMethod = "false";
            //  this.openTheContactConform();
          }
        }, {
          text: "BUY ADD-ON",
          cssClass: "submit-button",
          role: "BUY ADD-ON",
          handler: () => {
            this.events.publish('openECEMNumber');
            //  this.gatewayService.notifyObservers("Reload");
          }
        }
      ],
      cssClass: 'success-message'
    });
    normalAlert.present();
  }
  //Payment
  addCss(index) {
    this.zone.run(() => {
      console.log("index" + index);
      this.indexValue = index;
      console.log("index" + this.indexValue);
    });


    this.flag2 = false;
    console.log('/////' + index);
    this.cardType = "";
    this.cardType = this.paymentMethod[index].type;
    //this.addCssSub(-1);
  }
  //CreditReload 
  addCssSub(index) {
    this.zone.run(() => {
      console.log("index" + index);
      this.subIndexValue = index;
      console.log("index" + this.indexValue);
    });
    this.flag = false;
    console.log('/////' + index);
    this.cardValue = "";
    this.cardValue = this.subTitle[index].value;

  }
  checkingKadCeriaBonus(orderID) {

    this.loader = this.gatewayService.LoaderService();

    this.loader.present();
    var karCeriaChecking = this.reloadService.GetSelectMobileNumber();
    this.reloadService.checkAddedNewNumber(karCeriaChecking).then((res) => {
      this.loader.dismiss();
      console.log("*****Res*****" + JSON.stringify(res));
      var checkingKadCeria = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.ListOfServices.Services;
      if (checkingKadCeria.ProdPromName == "XPAX Kad Ceria Edition") {
        //    var kadCeria = "kadCeria"
        this.kadCeria = 1
        this.PaymentStatus(orderID, this.kadCeria);
      } else {
        this.kadCeria = 0;
        this.PaymentStatus(orderID, this.kadCeria)
      }
    }).catch(err => {
      this.loader.dismiss();
      console.log("*****reload*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
    });

  }
}
