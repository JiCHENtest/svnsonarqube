import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { GlobalVars } from "../../providers/globalVars";
import { GatewayService } from '../../global/utilService';



/**
 * Generated class for the StyleGuidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-style-guide',
  templateUrl: 'style-guide.html',
})
export class StyleGuidePage {

  selectOptions: any;
  centerSelectOptions: any;
  rightSelectOptions: any;
  constructor(public alertCtrl: AlertController,public navCtrl: NavController, 
    public navParams: NavParams, public globalVar: GlobalVars, public gatewayService: GatewayService, 
    private renderer: Renderer2) {

    this.selectOptions = {
      title: 'Phone Numbers',
      mode: 'ios',
      cssClass: this.globalVar.getCurrentTheme() + ' left-popover'
    };

    this.centerSelectOptions = {
      title: 'Phone Numbers',
      mode: 'ios',
      cssClass: this.globalVar.getCurrentTheme() + ' center-popover'
    };
    this.rightSelectOptions = {
      title: 'Phone Numbers',
      mode: 'ios',
      cssClass: this.globalVar.getCurrentTheme() + ' right-popover'
    };

  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Style guide", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StyleGuidePage');
  }
  presentAlert() {
    const alert = this.alertCtrl.create({
      title: 'Error Title',
      subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
     message:"do eiusmod tempor incididunt ut lab ore et dolore magna aliqua",
     buttons: [
      {
        text: 'Call to Action',
        cssClass:'submit-button',
        handler: () => {
          console.log('Call to Action Clicked');
        }

      },
      {
        text: 'Call to Action',
        cssClass:'submit-button',
        handler: () => {
          console.log('Call to Action Clicked');
        }

      }],
      cssClass: 'success-message error-message'
    });
    alert.present();
  }
  presentOneButtonAlert() {
    const alert = this.alertCtrl.create({
      title: 'Error Title',
      subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
     message:"do eiusmod tempor incididunt ut lab ore et dolore magna aliqua",
     buttons: [

      {
        text: 'Call to Action',
        cssClass:'error-button-float-right submit-button',
        handler: () => {
          console.log('Call to Action Clicked');
        }

      }],
      cssClass: 'success-message error-message'
    });
    alert.present();
  }
  presentsucess(){
    const alert = this.alertCtrl.create({
      title: 'Success Title',
      subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
     message:"do eiusmod tempor incididunt ut lab ore et dolore magna aliqua",
      buttons: [
      {
        text: 'Call to Action',
        cssClass:'submit-button ',
        handler: () => {
          console.log('Call to Action Clicked');
        }

      },
      {
        text: 'Call to Action',
        cssClass:'submit-button',
        handler: () => {
          console.log('Call to Action Clicked');
        }

      }],
      cssClass: 'success-message'
    });
    alert.present();
  }

  presentOneButtonSuccess() {
    const alert = this.alertCtrl.create({
      title: 'Success Title',
      subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
     message:"do eiusmod tempor incididunt ut lab ore et dolore magna aliqua",
     buttons: [

      {
        text: 'Call to Action',
        cssClass:'error-button-float-right submit-button',
        handler: () => {
          console.log('Call to Action Clicked');
        }

      }],
      cssClass: 'success-message'
    });
    alert.present();
  }

  present3ButtonSuccess() {
    const alert = this.alertCtrl.create({
      title: 'Success Title 3 buttons',
      subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
     message:"do eiusmod tempor incididunt ut lab ore et dolore magna aliqua",
     cssClass: 'success-message alert-3btn',
     buttons: [

      {
        text: 'Call to Action1',
        cssClass:'error-button-float-right submit-button',
        handler: () => {
          console.log('Call to Action Clicked');
        }

      },
      {
        text: 'Call to Action2',
        cssClass:'error-button-float-right submit-button',
        handler: () => {
          console.log('Call to Action Clicked');
        }

      },
      {
        text: 'Call to Action3',
        cssClass:'error-button-float-right submit-button',
        handler: () => {
          console.log('Call to Action Clicked');
        }

      }
    
    ]
    });
    alert.present();
  }

  present3ButtonAlert() {
    const alert = this.alertCtrl.create({
      title: 'Success Title 3 buttons',
      subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
     message:"do eiusmod tempor incididunt ut lab ore et dolore magna aliqua",
     cssClass: 'success-message error-message alert-3btn',
     buttons: [

      {
        text: 'Call to Action1',
        cssClass:'error-button-float-right submit-button',
        handler: () => {
          console.log('Call to Action Clicked');
        }

      },
      {
        text: 'Call to Action2',
        cssClass:'error-button-float-right submit-button',
        handler: () => {
          console.log('Call to Action Clicked');
        }

      },
      {
        text: 'Call to Action3',
        cssClass:'error-button-float-right submit-button',
        handler: () => {
          console.log('Call to Action Clicked');
        }

      }
    
    ]
    });
    alert.present();
  }

}
