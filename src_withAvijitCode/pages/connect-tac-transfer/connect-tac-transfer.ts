import { Component, ElementRef, ViewChild, NgZone } from "@angular/core";
import { NavController, NavParams, Platform, Events } from "ionic-angular";
import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { LocalStorage } from '../../global/localStorage';
import { Keyboard } from '@ionic-native/keyboard';
// import { MobileConnectPage } from "../mobile-connect/mobile-connect";
// import { MobileConnectAccountPage } from "../mobile-connect/account-setup/account-setup";
// import { NetworkLoginPage } from "../network-login/network-login";


// oleole service
import { OleoleService } from '../../global/oleole-service';
import { GatewayService } from '../../global/utilService';
import { ConstantData } from '../../global/constantService';
import { AlertService } from '../../global/alert-service';
import { DashboardService } from '../home/homeService';

import { CreditTransferPage } from '../services/credit-manage/credit-transfer/credit-transfer';
import { CreditManagePage } from '../services/credit-manage/credit-manage';
import { HomePage } from '../home/home';
/**
 * Generated class for the ConnectTacTwoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
// declare for sms plugin 
declare var SMS: any;

@Component({
  selector: 'page-connect-tac-transfer',
  templateUrl: 'connect-tac-transfer.html',
})
export class ConnectTacTransferPage {

  isenabled: boolean = false;
  otpInputt1: any;
  otpInputt2: any;
  otpInputt3: any;
  otpInputt4: any;

  @ViewChild('focus2')
  focus2: ElementRef;

  @ViewChild('focus1')
  focus1: ElementRef;

  @ViewChild('focus3')
  focus3: ElementRef;

  @ViewChild('focus3')
  focus4: ElementRef;

  @ViewChild('connectSubmit')
  connectSubmit:ElementRef;

  loader: any; // loader

  // oleole
  isOleole: boolean = false;
  params: any;
  registerState: any;
  state:any;

  incorrectOTPCount = 0;
  resendCount = 0;
  alertResendOTP: any;
  otpStartTimer: any;
  otpInput: any;

  mobileNumber: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public oleoleService: OleoleService,
    public gatewayService: GatewayService,
    private alertCtrlInstance: AlertService,
    public dashboardService: DashboardService,
    private toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public localStorage: LocalStorage,
    public platform: Platform,
    public zone: NgZone,
    public events: Events,
    private keyboard: Keyboard
  ) {

    this.mobileNumber = this.gatewayService.GetMobileNumber();

    console.log("ConnectTacTwoPage -> constructor() -> oleoleBuyFor ", this.navParams.get('params'));

    this.params = this.navParams.get('params');
    if(ConstantData.isLoginBypassed)
        {
          this.state = "";

        }
        else{
          this.state = this.params.register_state.state;
        }
    
  }

  ionViewWillEnter(){
    console.log('ionViewWillEnter ConnectTacTransferPage');
    
  }
  ngOnDestroy() {
    //this.events.unsubscribe('callTransferAmount');
    console.log('ng destroy connect tac transfer screen');
    //this.events.unsubscribe('goToOTPPage');
  }
  ionViewDidEnter() {
    console.log('ionViewDidLoad ConnectTacTransferPage');
    
      if (SMS) SMS.startWatch(() => {
          console.log('watching started');
        }, Error => {
          console.log('failed to start watching');
        });
  
        document.addEventListener('onSMSArrive', (e: any) => {
          var sms = e.data.body;
          //var OTP=sms.substring(33,40);
  
          var OTP = sms.split("(OTP) is ");
          console.log("OTP is ::" + OTP[1]);
          this.zone.run(() => {
            this.otpInput = OTP[1];
            this.otpInputt1 = this.otpInput.substring(0, 1);
            console.log("i/p1 :" + this.otpInputt1);
            this.otpInputt2 = this.otpInput.substring(1, 2);
            console.log("i/p2 :" + this.otpInputt2);
            this.otpInputt3 = this.otpInput.substring(2, 3);
            console.log("i/p3 :" + this.otpInputt3);
            this.otpInputt4 = this.otpInput.substring(3, 4);
            console.log("i/p4 :" + this.otpInputt4);
  
            if (this.otpInputt1 != "" && this.otpInputt2 != "" && this.otpInputt3 != "" && this.otpInputt4 != "") {
              this.isenabled = true;
            }
          });
  
        });
      
    
  }

  goBack() {
   
    console.log("ConnectTacTwoPage -> goBack() -> navCtrl.pop() ");
    this.navCtrl.pop();
  }

  enableHome1(el) {

    if (this.otpInputt1 == "") {
      this.isenabled = false;
    }
    else {
      //this.isenabled = true;
      el.focusNext();

    }

  }

  enableHome2(el, focus1) {

    if (this.otpInputt2 == "") {
      this.isenabled = false;
      //focus1._native.nativeElement.focus();
      //focus1.setFocus();

      setTimeout(() => {
        focus1.setFocus();
      }, 50);
    }
    else {
      //this.isenabled = true;
      el.focusNext();

      //setTimeout(() => {
      // el.focusNext();

      // },150);
    }

  }

  enableHome3(el, focus2) {

    if (this.otpInputt3 == "") {
      this.isenabled = false;


      setTimeout(() => {
        focus2._native.nativeElement.focus();
      }, 50);
    }
    else {
      //this.isenabled = true;
      el.focusNext();

    }

  }

  enableHome4(el) {

    if (this.otpInputt4 == "") {
      this.isenabled = false;

      setTimeout(() => {
        el.focusNext();
      }, 50);

    }
    else {
      this.isenabled = true;

    }

  }
  handlePin(e,elementIndex,nextElement,prevElement){
    if(e.which === 13){
      //in case of enter button
      this.keyboard.close();
    }else if (e.which === 8){
      //in case of delete button need to move backward
      if(elementIndex!=1){
        prevElement._native.nativeElement.focus();
      }
    }else{
       //for all other buttons need to move forwards
       if(elementIndex!=4){
        nextElement._native.nativeElement.focus();
       }
    }

    if (this.otpInputt1 == "" || this.otpInputt1==undefined || this.otpInputt2 == "" || this.otpInputt2==undefined || this.otpInputt3 == "" || this.otpInputt3==undefined || this.otpInputt4 == "" || this.otpInputt4==undefined) {
      this.isenabled = false;
    }else{
      this.isenabled = true;
    }  
    
  
  }
  //incase of scucess
  transferCredit() {
    var _that = this;
    if (ConstantData.isLoginBypassed == false) {

      if (this.incorrectOTPCount < 5) {
        
        _that.loader = _that.gatewayService.LoaderService();
        _that.loader.present().then(() => {
           //code for calling register()from adapter
        _that.validateMobileConnectUser().then(function (res) {
          //console.log(res);
          
          if (res["code"] == 0) {
            //transfer credit
            _that.transferAmountServiceCall().then(function (res) {
                  console.log(res);
                  
                  _that.loader.dismiss();
                  if(res["isSuccessful"]){
                    if(res["msgStatus"]=="Success"){
                      
                      _that.gatewayService.setFlagForDashboardReload(true); 
                      //_that.alertCtrlInstance.showAlert(res["msgStatus"], res["cswMessage"], "OK");
                      let alert = _that.alertCtrl.create({
                        title: res["cswStatus"],
                        subTitle: res["cswMessage"],
                        buttons: [
                        {
                          text:"OK",
                          role:"Ok",
                          cssClass:'error-button-float-right submit-button',
                          handler: () => {
                            console.log('Call to Action Clicked');
                            //_that.navCtrl.push(CreditManagePage,0);
                            _that.events.publish('reloadHeaderManageCredit');
                            _that.gatewayService.setFlagForDashboardReload(true);
                            
console.log("this.navCtrl.getByIndex(this.navCtrl.length()-(1)) ",_that.navCtrl.getByIndex(_that.navCtrl.length() - (1)));
_that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length() - (2 + 1))); 

                            //_that.navCtrl.setRoot(HomePage);
                            //_that.events.publish('goToCreditTransfer');
                          }
                        }],
                       cssClass: 'success-message',
                        enableBackdropDismiss: false
                      
                      });
                      alert.present();
                    }
                    else{
                      //_that.alertCtrlInstance.showAlert(res["msgStatus"], res["cswMessage"], "OK");
                      var alert_msg = '';
                      if(res["requestError"].serviceException.text == '' || res["requestError"].serviceException.text== undefined)
                      {
                        alert_msg = res["cswMessage"];
                      }
                      else{
                        alert_msg = res["requestError"].serviceException.text;
                      }
                      let alert = _that.alertCtrl.create({
                        title: res["msgStatus"],
                        subTitle: alert_msg,
                        buttons: [
                        {
                          text:"Ok",
                          role:"Ok",
                          cssClass:'error-button-float-right submit-button',
                          handler: () => {
                            console.log('Call to Action Clicked');
                            _that.events.publish('reloadHeaderManageCredit');
                            //_that.navCtrl.push(CreditManagePage,0);
                            
                            _that.navCtrl.setRoot(HomePage);

                          }
                        }],
                        cssClass:'success-message error-message',
                        enableBackdropDismiss: false
                      
                      });
                      alert.present();
                    }
                  }
                  else{
                    //_that.alertCtrlInstance.showAlert("Uh Oh. Can't transfer amount", "Please try again later", "OK");
                    let alert = _that.alertCtrl.create({
                      title: "Uh Oh. Can't transfer amount",
                      subTitle: "Please try again later",
                      buttons: [
                      {
                        text:"OK",
                        role:"Ok",
                        cssClass:'error-button-float-right submit-button',
                        handler: () => {
                          console.log('Call to Action Clicked');
                          //_that.navCtrl.push(CreditManagePage,0);
                          //_that.events.publish('goToCreditTransfer');
                          //_that.gatewayService.setFlagForDashboardReload(true);
                          _that.navCtrl.setRoot(HomePage);
                        }
                      }],
                      cssClass :'success-message error-message',
                      enableBackdropDismiss: false
                    
                    });
                    alert.present();
                  }
                  
                  
                }).catch(function (e) {

                  //_that.alertCtrlInstance.showAlert("Uh Oh. Can't transfer amount", "Please try again later", "OK");
                  let alert = _that.alertCtrl.create({
                    title: "Uh Oh. Can't transfer amount",
                    subTitle: "Please try again later",
                    buttons: [
                    {
                      text:"OK",
                      role:"Ok",
                      cssClass:'error-button-float-right submit-button',
                      handler: () => {
                        console.log('Call to Action Clicked');
                        //_that.navCtrl.push(CreditManagePage,0);
                        //_that.events.publish('goToCreditTransfer');
                        //_that.gatewayService.setFlagForDashboardReload(true);
                        _that.navCtrl.setRoot(HomePage);
                      }
                    }],
                    cssClass:'success-message error-message',
                    enableBackdropDismiss: false
                  
                  });
                  console.log("component  failure" + e);
                  console.log("Error" + e);
                  _that.loader.dismiss();
                  if(e=="No internet connection")
                  return;
                  alert.present();
                  
                 
                });
          }
          else {
            _that.loader.dismiss();
            _that.incorrectOTPCount++;
            //var data = { "title": "Uh oh, invalid OTP", "subtitle": "", "message": "Did you enter the wrong number?" };
            _that.alertCtrlInstance.showAlert("Uh Oh. That's not a valid OTP", "Did you enter the wrong number? Please try again later.", "OK");
            //_that.gatewayService.presentAlert(data);
          }
        }).catch(function (e) {
          _that.loader.dismiss();
          console.log("component  failure" + e);
          console.log("Error" + e);
          if(e=="No internet connection")
          return;
          _that.alertCtrlInstance.showAlert("Uh Oh. Can't seem to receive OTP", "Please try again later", "OK");
         
          
        });
        });
       
      }
      else {

        let alert = _that.alertCtrl.create({
          title: "Uh Oh. That's Too Many Login Attempts",
          subTitle: "You've entered the incorrect PIN too many times. Please exit and try again later.",
          buttons: [
          {
            text:"OK",
            role:"Ok",
            cssClass:'error-button-float-right submit-button',
            handler: () => {
              console.log('Call to Action Clicked');
              //_that.navCtrl.push(CreditManagePage,0);
              //_that.events.publish('goToCreditTransfer');
              //_that.gatewayService.setFlagForDashboardReload(true);
              _that.navCtrl.setRoot(HomePage);
            }
          }],
          cssClass:'success-message error-message',
          enableBackdropDismiss: false
        
        });
        alert.present();
      }

    } else {
      
      _that.loader = _that.gatewayService.LoaderService();
      _that.loader.present().then(() => {

        //transfer credit
        _that.transferAmountServiceCall().then(function (res) {
          console.log(res);
          
          _that.loader.dismiss();
          if(res["isSuccessful"]){
            if(res["msgStatus"]=="Success"){
              
              _that.gatewayService.setFlagForDashboardReload(true); 
              //_that.alertCtrlInstance.showAlert(res["msgStatus"], res["cswMessage"], "OK");
              let alert = _that.alertCtrl.create({
                title: res["cswStatus"],
                subTitle: res["cswMessage"],
                
                buttons: [
                {
                  text:"OK",
                  role:"Ok",
                  cssClass:"error-button-float-right submit-button",
                  handler: () => {
                    console.log('Call to Action Clicked');
                    
                    _that.events.publish('reloadHeaderManageCredit');
                    //_that.events.publish('goToCreditTransfer');
                    _that.gatewayService.setFlagForDashboardReload(true);
                    console.log("this.navCtrl.getByIndex(this.navCtrl.length()-(1)) ",_that.navCtrl.getByIndex(_that.navCtrl.length() - (1)));
_that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length() - (2 + 1))); 
                    //_that.navCtrl.setRoot(HomePage);
                    //_that.navCtrl.push(CreditManagePage,0);
                  }
                }],
                cssClass:'success-message',
                enableBackdropDismiss: false
              
              });
              alert.present();
            }
            else{
              //_that.alertCtrlInstance.showAlert(res["msgStatus"], res["cswMessage"], "OK");
              var alert_msg = '';
                      if(res["requestError"].serviceException.text == '' || res["requestError"].serviceException.text== undefined)
                      {
                        alert_msg = res["cswMessage"];
                      }
                      else{
                        alert_msg = res["requestError"].serviceException.text;
                      }
              let alert = _that.alertCtrl.create({
                title: res["msgStatus"],
                subTitle: alert_msg,
                buttons: [
                {
                  text:"OK",
                  role:"Ok",
                  cssClass:'error-button-float-right submit-button',
                  handler: () => {
                    console.log('Call to Action Clicked');
                    //_that.navCtrl.push(CreditManagePage,0);
                    _that.events.publish('goToCreditTransfer');
                  }
                }],
                cssClass:'success-message error-message',
                enableBackdropDismiss: false
              
              });
              alert.present();
            }
          }
          else{
            //_that.alertCtrlInstance.showAlert("Uh Oh. Can't transfer amount", "Please try again later", "OK");
            let alert = _that.alertCtrl.create({
              title: "Uh Oh. Can't transfer amount",
              subTitle: "Please try again later",
              buttons: [
              {
                text:"OK",
                role:"Ok",
                cssClass:'error-button-float-right submit-button',
                handler: () => {
                  console.log('Call to Action Clicked');
                  //_that.navCtrl.push(CreditManagePage,0);
                  _that.events.publish('goToCreditTransfer');
                }
              }],
              cssClass:'success-message error-message',
              enableBackdropDismiss: false
            
            });
            alert.present();
          }
          
          
        }).catch(function (e) {
          //_that.alertCtrlInstance.showAlert("Uh Oh. Can't transfer amount", "Please try again later", "OK");
          let alert = _that.alertCtrl.create({
            title: "Uh Oh. Can't transfer amount",
            subTitle: "Please try again later",
            buttons: [
            {
              text:"OK",
              role:"Ok",
              cssClass:'error-button-float-right submit-button',
              handler: () => {
                console.log('Call to Action Clicked');
                //_that.navCtrl.push(CreditManagePage,0);
                _that.events.publish('goToCreditTransfer');
              }
            }],
            cssClass:'success-message error-message',
            enableBackdropDismiss: false
          
          });
          console.log("component  failure" + e);
          console.log("Error" + e);
          _that.loader.dismiss();
          if(e=="No internet connection")
          return;
          alert.present();
          
         
        });
      });
      
    }

  }

  resendOTP() {
    var _that = this;
    
    console.log('Resend count ' + this.resendCount);
    //console.log(seconds);
    if (this.resendCount > 4) {

      //show max resend attemp msg
      this.alertResendOTP = _that.alertCtrl.create({
        title: 'Uh oh',
        subTitle: 'Resend OTP requests limited to 5 attempts. Please try again in 10 minutes.',
        buttons: [
          {
            text: "Ok",
            role: "Ok",
            cssClass:'error-button-float-right submit-button',
            handler: () => {
              console.log('Call to Action Clicked');

            }
          }],
        cssClass: 'success-message error-message',
        enableBackdropDismiss: true

      });
      this.alertResendOTP.present();
      this.otpStartTimer = this.localStorage.set("otpStartTimer", new Date().getTime());
      setTimeout(
        () => {
          this.alertResendOTP.dismiss();
          this.resendCount = 0;
          this.localStorage.set("otpStartTimer", '');
        }

        , 600000);
      // this.myTimer = Observable.timer(0, 1000)
      //   .take(this.counter)
      //   .map(() => {
      //     console.log(this.counter);
      //     if (this.counter == 1) {
      //       this.alertResendOTP.dismiss();
      //       this.resendCount = 0;
      //       this.counter = 2*60;
      //     } else {
      //       --this.counter;
      //     }
      //   }
      //   );
      // this.myTimer.subscribe();
      //_that.alertCtrlInstance.showAlert("", "", ["Try again", "Cancel"]);


    } else {
      this.sendOTP();
    }
  }
  sendOTP() {

    let toast = this.toastCtrl.create({
      message: 'OTP successfully sent',
      duration: 3000,
      position: 'top'
    });
    let Errortoast = this.toastCtrl.create({
      message: 'Unable to send OTP. Please retry!',
      duration: 3000,
      position: 'top'
    });
    var _that = this;
    this.resendCount++;
    //this.firstResendTime = new Date();
    //console.log('First date ' + this.firstResendTime);

    var loader = _that.gatewayService.LoaderService();
    loader.present();
    //code for calling register()from adapter
    _that.registerMobileConnectUser().then((res) => {
      console.log("resend otp state " + JSON.stringify(res));
     this.state = res["state"];
      loader.dismiss();
      toast.present();
    }).catch(function (e) {
      console.log("component  failure" + e);
      loader.dismiss();
      if(e=="No internet connection")
      return;
      Errortoast.present();
      //alert("Error"+e);

    });
  }

  validateMobileConnectUser() {
    
    console.log("validateMobileConnectUser() -> state = " + this.state);
    var otp = this.otpInputt1.toString() + this.otpInputt2.toString() + this.otpInputt3.toString() + this.otpInputt4.toString();
    console.log(otp);
    console.log(this.state);
    var params_validate = { "state": this.state, "OTP": otp };

    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.validateMobileConnect2, "POST", params_validate).then((res_validate) => {
        console.log("validate user response : " + JSON.stringify(res_validate));
        resolve(res_validate);

        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          console.log("******************************************** component failure validateMobileConnectUser" + e);
          //alert("error in validating MobileConnectUser");   
          reject(e);
        });

    });


  }


  registerMobileConnectUser() {
    var _that = this;

    var msisdn = this.gatewayService.GetMobileNumber();
    var params_register = { "MSISDN": msisdn };

    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.registerMobileConnect2, "POST", params_register).then((res_register) => {
        console.log("register user response : " + JSON.stringify(res_register));
        resolve(res_register);

        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          console.log("******************************************** component failure registerMobileConnectUser" + e);
          _that.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", ["OK"]);
          //alert("error register MobileConnectUser");   
          reject(e);
        });

    });


  }

  transferAmountServiceCall(){
    var transfereeNo = this.params.transfereeNo;
    //s.substr(1);
    var transferorNo = this.params.transferorNo;
    var currAcctChgAmt = this.params.currAcctChgAmt;
     var params = {"transfereeNo":transfereeNo, "transferorNo":transferorNo,"currAcctChgAmt":currAcctChgAmt};
 console.log(params);
     return new Promise((resolve, reject) => {
       this.gatewayService.CallAdapter(ConstantData.adapterUrls.CreditTransfer, "POST", params).then((res_validate) => {
         console.log("transfer amount response : " + JSON.stringify(res_validate));
         resolve(res_validate);
 
         //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
       })
         .catch(function (e) {
           console.log("******************************************** component failure transfer amount response" + e);
           //alert("error in validating MobileConnectUser");   
           reject(e);
         });
 
     });
 
   }
}
