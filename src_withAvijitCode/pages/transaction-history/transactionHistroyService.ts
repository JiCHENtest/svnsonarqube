import { ConstantData} from "../../global/constantService";
import { GatewayService } from '../../global/utilService';
import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { AlertController, LoadingController } from 'ionic-angular';
import { HandleError } from '../../global/handleerror';
import { AlertService } from '../../global/alert-service';
import { JsonstoreService } from '../../global/jsonstore';
import { ReloadService } from '../../global/reloadServices';


interface Subject{
  registerObserver(o: Observer);
  notifyObservers(val:string);
}

interface Observer{
  update();
  
}

@Injectable()
export class TransactionHistroyService {


constructor(private network: Network,public alertCtrl:AlertController, public loadingCtrl:LoadingController,public handleerror: HandleError, public gatewayService:GatewayService, public jsonstoreService: JsonstoreService, public reloadService:ReloadService){



};




FetchTransactionHistory(mobileNumber){


try{
  
  var params = {
    "MSISDN": mobileNumber//this.localStorage.get("MobileNum","")
  }
return new Promise((resolve, reject) => {

this.gatewayService.CallAdapter(ConstantData.adapterUrls.getTransactionHistory, "POST", params).then(function (res) {
console.log("*********************************** component  FetchTransactionHistory"+JSON.stringify(res));
resolve(res);
})
.catch(function(e){
  // this.alertCtrlInstance.showAlert("", " System down, cant call Payment Gateway", "Done");
 
console.log("******************************* component  failure FetchTransactionHistory"+e);
//alert("error user info");
reject(e);
});
});
}catch(e){
//alert("error"+JSON.stringify(e));
}



}
  
  
FetchReloadTransactionHistory(mobileNumber){
  
  
  try{
    
    var params = {
      "MSISDN": mobileNumber//this.localStorage.get("MobileNum","")
    }
  return new Promise((resolve, reject) => {
  
  this.gatewayService.CallAdapter(ConstantData.adapterUrls.getTransactionReloadPaybill, "POST", params).then(function (res) {
  console.log("*********************************** component  FetchReloadTransactionHistory"+JSON.stringify(res));
    resolve(res);
  })
  .catch(function(e){
  console.log("******************************* component  failure FetchReloadTransactionHistory"+e);
    reject(e);
  });
  });
  }catch(e){
  //alert("error"+JSON.stringify(e));
  }
  
  
  
  }
  FetchPayBillTransactionHistory(billingNumber,ServiceType){
   
    try{
      
      var params = {
        "billingAccountNum":billingNumber,
        "ServiceType":ServiceType
      }
      
    return new Promise((resolve, reject) => {
    
    this.gatewayService.CallAdapter(ConstantData.adapterUrls.fetchBillListRetrieve, "POST", params).then(function (res) {
    console.log("*********************************** component  FetchPayBillTransactionHistory"+JSON.stringify(res));
      resolve(res);
    })
    .catch(function(e){
    console.log("******************************* component  failure FetchPayBillTransactionHistory"+e);
      reject(e);
    });
    });
    }catch(e){
    //alert("error"+JSON.stringify(e));
    }
    
    
    
    }
    //BillDownload
    BillDownload(ObjectID){
      
       try{
         
         var params = {
           "objectId":ObjectID
         }
         
       return new Promise((resolve, reject) => {
       
       this.gatewayService.CallAdapter(ConstantData.adapterUrls.BillDownload, "POST", params).then(function (res) {
       console.log("*********************************** component  BillDownload"+JSON.stringify(res));
         resolve(res);
       })
       .catch(function(e){
       console.log("******************************* component  failure BillDownload"+e);
         reject(e);
       });
       });
       }catch(e){
       //alert("error"+JSON.stringify(e));
       }
       
       
       
       }


}