import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { GbShareAllocatePage } from '../gb-share-allocate/gb-share-allocate';
import { GbShareService } from '../../global/gbshareServices';
import { GatewayService } from '../../global/utilService';
import { AlertService } from '../../global/alert-service';
import { DashboardService } from '../home/homeService';
import { AlertController } from 'ionic-angular';
import { DatePipe } from '@angular/common';
import { SubscriptionsPage } from '../subscriptions/subscriptions';
import { GlobalVars } from "../../providers/globalVars";
import {SubscriptionsTabService } from '../../pages/subscriptions/subscriptionsTabService';
/**
 * Generated class for the GbSharePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gb-share',
  templateUrl: 'gb-share.html',
})
export class GbSharePage {

  value1: number = 0;
  value2: number = 0;
  value3: number = 0;
  loader:any;
  unitAvailable:any;
  totalInitialAmount:any;
  expairyDateTime:any;
  minValue: number;
  maxValue: number;
  count :number = 0;
  groupID:any;
  callBar:boolean=false;
  GB:string="GB";
  FreeUnitItem:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, 
    public gbshareService :GbShareService,private alertCtrlInstance: AlertService, 
    public gatewayService:GatewayService,public dashboardService: DashboardService, 
    public alertCtrl: AlertController, public datePipe: DatePipe,public globalVar: GlobalVars, 
    public subscriptionsTabService:SubscriptionsTabService, private renderer: Renderer2) {
    if (navParams.get('value1')) {
      this.value1 = navParams.get('value1');
    }
    if (navParams.get('value2')) {
      this.value2 = navParams.get('value2');
    }
    if (navParams.get('value3')) {
      this.value3 = navParams.get('value3');
    }
    this.loader = this.gatewayService.LoaderService();
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("GB share", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GbSharePage');
//var params 
  this.callBar = this.globalVar.getcallBarredStatus();
//  this.callBar = true
var _that = this;
var userInfo = _that.dashboardService.GetUserInfo();
console.log("*****userInfo*******"+JSON.stringify(userInfo));
this.groupID = userInfo.ListOfServices.Services.SmeGroupId;
this.FreeUnitItem = [];

var params = {
  "GroupID":this.groupID //665811549 //014831735
}  
this.loader = this.gatewayService.LoaderService();
this.loader.present();
this.gbshareService.GroupSharing(params).then((res)=> {
  this.loader.dismiss();   
  console.log("*****GroupSharing*******"+JSON.stringify(res));

if(res["Envelope"].Body.processResponse.ResponseHeader.Description == "Operation successfully."){
  if(res["Envelope"].Body.processResponse.QueryFreeUnitResult !="" || res["Envelope"].Body.processResponse.QueryFreeUnitResult != undefined){
    if(res["Envelope"].Body.processResponse.QueryFreeUnitResult.FreeUnitItem.length > 0){
      this.FreeUnitItem = res["Envelope"].Body.processResponse.QueryFreeUnitResult.FreeUnitItem
        this.FreeUnitItem.forEach((element,index) => {
        if(element.FreeUnitTypeName == "C_MI_Data_Monthly"){
          this.totalInitialAmount = element.TotalInitialAmount
          this.unitAvailable =  element.TotalUnusedAmount
          this.expairyDateTime = element.FreeUnitItemDetail.ExpireTime     
        }  
       });
    }else{
      this.totalInitialAmount = res["Envelope"].Body.processResponse.QueryFreeUnitResult.FreeUnitItem.TotalInitialAmount
      this.unitAvailable =  res["Envelope"].Body.processResponse.QueryFreeUnitResult.FreeUnitItem.TotalUnusedAmount
      this.expairyDateTime = res["Envelope"].Body.processResponse.QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.ExpireTime
    }
}else{
    this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
  }
 
}else{
  this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
}

 }).catch(err => {
  this.loader.dismiss();  
  console.log("*****reload*******"+JSON.stringify(err));
  if(err=="No internet connection")
  return;
  this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
  // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
});
  }

  pushPage() {
    //this.navCtrl.setRoot(GbShareAllocatePage);
    this.showAlertToHandleNavigate("Sharing is Caring","You are switching to allocated sharing. You decide how much data each of your GBshare lines receives monthly.", ["Switch","Cancel"]);
}
Rediretion(){
  var date = this.datePipe.transform(new Date(), 'yyyyMMddHHmmss');
  var params = {
    "GroupID":this.groupID,
    "time":date //665811549 //014831735//3775080302
  }  
  //E DashboardInfo{"GroupID":"3775080302","Time":"20180116143759"}
  this.loader = this.gatewayService.LoaderService();
  this.loader.present();
  this.gbshareService.GoingToAllocation(params).then((res)=> {
    this.loader.dismiss();   
    console.log("*****GBShareStatus*******"+JSON.stringify(res));
    if(res["Envelope"].Body.PurchaseProductCBSResponse.Response.ErrorMessage == "Operation successfully."){
      this.navCtrl.setRoot(GbShareAllocatePage);
    }else{
      this.alertCtrlInstance.showAlert("Uh Oh. GBshare is Busy","System is unable to toggle between group and allocated sharing", "OK");
    }
  //  this.showAlertToHandleNavigate("Success Title","Success Dec", ["CTA"]);
  }).catch(err => {
    this.loader.dismiss();  
    console.log("*****reload*******"+JSON.stringify(err));
    if(err=="No internet connection")
    return;
    this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
    // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
  });
}
  goToRoot() {
    this.navCtrl.popToRoot();
  }
  showAlertToHandleNavigate(title,  msg,  btn) {
    
    if (Array.isArray(btn)) {
      
            try {
              let alert = this.alertCtrl.create({
                title: title,
                subTitle: msg,
                buttons: [{
                  text: btn[0],
                  cssClass:'submit-button',
                  handler: () => {
                    console.log('open autobilling');
                    // if(title == "Success Title"){
                    //   this.navCtrl.setRoot(GbShareAllocatePage);
                    // }else if (title == "Error Title"){
                    //   this.count = this.count +1;
                    // }else{
                    //   this.count = 0
                    // }
                    this.Rediretion();
                  

                  }
                },
                {
                  text: btn[1],
                  cssClass:'submit-button',
                  handler: () => {
                     console.log('Second Button');
                     //this.count = this.count +1;
                  }
                }],
                cssClass: 'success-message'
                // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
              });
              alert.present();
      
            }
            catch (e) {
              console.log("Exception : Alert open ", e);
            }
          }
  }
  interNetAddOn() {
    this.subscriptionsTabService.SetTabInfo(1);
    this.navCtrl.push(SubscriptionsPage);
  }

  isCapZone(){
     return this.globalVar.getcallBarredStatus();
    //return true;
   }
}
