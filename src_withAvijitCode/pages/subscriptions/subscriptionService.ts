
/**
 * @author Capgemini 
 */

/**
 * @Module Name - utilService
 * @Description - Singleton class for webservices, This is common gateway for all fetch/post calls 
 */

import { Injectable } from '@angular/core';
import _ from 'underscore';
import { GlobalVars } from "../../providers/globalVars";
import { AlertController, Events } from 'ionic-angular';
import { DashboardService } from '../home/homeService';
import { GatewayService } from '../../global/utilService';

// interface Subject {
//     registerObserver(o: Observer);
//     notifyObservers(val: string);
// }
// interface Observer {
//     update();

// }

@Injectable()
export class SubscriptionService {

    userPlanInfo: any; // Internet category

    countryBtnInfo: any;

    // userAddonPlanInfo; // Add-on category

    // userIddPlanInfo; // IDD category

    // userRoamingPlanInfo; // Roaming category
    countryList: any;
    subcriptionPostPlan: any;

    isOleole: boolean = false;
    //private observers: Observer[] = [];

    constructor(public globalVar: GlobalVars,
        public alertCtrl: AlertController,
        public events: Events,
        private dashboardService: DashboardService,
        private gatewayService: GatewayService) { }

    // registerObserver(o: Observer) {
    //     this.observers.push(o);
    // }

    // notifyObservers() {

    //     for (let observer of this.observers) {
    //         observer.update();
    //     }
    // }

    isCapZone() {

        console.log("isCapZone() = ", this.globalVar.getCapZoneStatus());
        console.log("isCallBar() = ", this.globalVar.getcallBarredStatus());
        // return this.globalVar.getCapZoneStatus();
        return this.globalVar.getCapZoneStatus() || this.globalVar.getcallBarredStatus();
      
    }


    presentConfirm() {
        var title;
        if(this.globalVar.getCapZoneStatus()){
            title = "Remove CAPZONE";
        }else{
            title = "Remove CALL BARRED";
        }


       
       if (this.globalVar.getCapZoneStatus() || this.globalVar.getcallBarredStatus()) {
            this.PTPEligibility().then((res) => {
               
                let buttonsArray = [{
                    text: "PayBill >",
                    cssClass:'error-button-float-right submit-button',
                    handler: () => {
                        this.goToPayBill();
                        console.log('paybill clicked');
                    }
                },{
                    text: "Cancel >",
                    cssClass:'error-button-float-right submit-button',
                    handler: () => {
                       
                        console.log('Cancel clicked');
                    }
                }
            ];
                if (res == "promiseToPaySuccess") {
                    buttonsArray.push({
                        text: "Promise to Pay >",
                        cssClass:'error-button-float-right submit-button',
                        handler: () => {
                            this.updateCreditLimit()
                            console.log('promise to pay  clicked');
                        }
                    })
                }
                let alert = this.alertCtrl.create({
                    title: title,
                    message: "Kindly select your action",
                    buttons: buttonsArray,
                    cssClass: 'success-message alert-3btn',
                    enableBackdropDismiss: false
                });
                alert.present();
            }).catch(err => {
                let alert = this.alertCtrl.create({
                    title: title,
                    cssClass:'error-button-float-right submit-button',
                    message: "Kindly select your action",
                    buttons: [
                        {
                            text: "PayBill >",
                            handler: () => {
                                this.goToPayBill();
                                console.log('Cancel clicked');
                            }
                        }
                    ],
                    enableBackdropDismiss: false
                });
                alert.present();
            });
        }

    }

    presentsucess(title, messageInput) {
        const alert = this.alertCtrl.create({
            title: title,
            message: messageInput,
            buttons: ['Ok'],
            cssClass: 'error-message',
            enableBackdropDismiss: false
        });
        alert.present();
    }

    goToPayBill() {
        this.events.publish('goToPayBillFromSubscription');
    }

    enablePromiseToPay() {
        //call promiseToPay eligibility check if user is in call bar or capzone

    }

    PTPEligibility() {
        var  userInfo  = this.dashboardService.GetUserInfo();
        var billingAccountNumber  =  userInfo.ListOfServices.Services.AssetBillingAccountNo;
        var params = { "billingAccountNumber": billingAccountNumber };
        var _that = this;
        var loader = _that.gatewayService.LoaderService();
        loader.present();
        return new Promise((resolve, reject) => {

            _that.gatewayService.PromiseToPayCall(params).then((res) => {
                console.log(res);
                if (res["outputCreditCheckResp"] != undefined) {
                    if (res["outputCreditCheckResp"].statusId == 'S') {
                        // _that.globalVar.setCapZoneStatus(false);
                        // _that.globalVar.setcallBarredStatus(false);
                        // _that.gatewayService.setFlagForDashboardReload(true);
                        resolve("promiseToPaySuccess");
                    }
                }
                else {
                    console.log("promise to pay success else");
                    resolve("promiseToPayFailure");
                }
                loader.dismiss();
            }).catch(err => {
                console.log("promise to pay success catch");
                loader.dismiss();
                reject("promiseToPayFailure");//FetchUserInfo
            });
        });
    }

    updateCreditLimit() {
        var  userInfo  = this.dashboardService.GetUserInfo();
        var billingAccountNumber  =  userInfo.ListOfServices.Services.AssetBillingAccountNo;
        var params = { "billingAccountNumber": billingAccountNumber };
        var _that = this;
        var loader = _that.gatewayService.LoaderService();
        loader.present();
        return new Promise((resolve, reject) => {

            _that.gatewayService.updateCreditLimit(params).then((res) => {
                console.log(res);
                if (res["PTPCLModifyResponselist"] != undefined && res["PTPCLModifyResponselist"] != null) {
                    if (res["PTPCLModifyResponselist"].StatusId == 'S') {
                        console.log('yes');
                        _that.globalVar.setCapZoneStatus(false);
                        _that.globalVar.setcallBarredStatus(false);
                        _that.gatewayService.setFlagForDashboardReload(true);
                        _that.presentsucess("Keep Calm and Keep Talking", "Calls will not be barred for 5 days. Please pay the balance to avoid call-barring.");
                    }
                }
                else {
                }

                resolve();
                loader.dismiss();
            }).catch(err => {
                //alert("error FetchUserInfo"+JSON.stringify(err));
                loader.dismiss();
                reject(err);//FetchUserInfo
            });
        });
    }

    setSubscriptionInfo(obj) {
        console.log("setSubscriptionInfo ");
        //var result : any = JSON.parse(JSON.stringify(obj));
        console.log("setSubscriptionInfo " + obj)
        this.userPlanInfo = obj;
    }

    // Internet for postpaid
    getUserInterSubscriptionInfoPostpaid() {
        console.log("getUserInterSubscriptionInfoPostpaid Postpaid ", this.userPlanInfo);

        return [this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEMAXUPPLANLIST];

        // return _.values(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLETREMENDOUSPLANS);
        // _.values(result.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS);
    }

    // Internet for postpaid
    getUserLegacySubscriptionInfoPostpaid(): any {
        console.log("getUserLegacySubscriptionInfoPostpaid Postpaid ", this.userPlanInfo);
        var isLegacy: any = this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLELEGACYINTERNETPLANS;

        console.log("getUserLegacySubscriptionInfoPostpaid -> isLegacy = ", isLegacy);

        if (isLegacy != undefined)
            return [this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLELEGACYINTERNETPLANS];
        else
            return undefined;

        // return _.values(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLETREMENDOUSPLANS);
        // _.values(result.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS);
    }

    // Internet for prepaid
    getUserInterSubscriptionInfoPrePaid() {
        console.log("getUserInterSubscriptionInfoPrePaid Prepaid ", this.userPlanInfo);

        return (this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLETREMENDOUSPLANS == null) ? [] : ([this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLETREMENDOUSPLANS]);

        // return _.values(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLETREMENDOUSPLANS);
        // _.values(result.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS);
    }

    // setUserAddonSubscriptionInfo(obj){
    //     this.userAddonPlanInfo = obj;
    // }

    // product enqiry for prepaid 
    getUserAddonSubscriptionInfoPrepaid() {
        console.log("getUserAddonSubscriptionInfoPrepaid ", JSON.stringify(this.userPlanInfo));
        return _.values(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS);
    }

    // product enqiry for postpaid video amd music walla
    getUserAddonSubscriptionInfoPostpaid() {

        var handle_Plan_object: any = this.userPlanInfo;
        if (this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.VIDEOPACKS != undefined) {
            console.log("for subscription ");
            // for music & video pack check is Array
            if (Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.VIDEOPACKS.PLAN) && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.MUSICPACKS.PLAN)) {

                console.log("getUserAddonSubscriptionInfoPostpaid ", JSON.stringify(this.userPlanInfo));
                return _.values((_.pick(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS, 'MUSICPACKS', 'VIDEOPACKS')));

            } // for video pack check is Array 
            else if (Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.VIDEOPACKS.PLAN) === true && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.MUSICPACKS.PLAN) === false) {

                handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.MUSICPACKS.PLAN = [this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.MUSICPACKS.PLAN]

                return _.values((_.pick(handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS, 'MUSICPACKS', 'VIDEOPACKS')));

            } // for music pack check is Array
            else if (Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.VIDEOPACKS.PLAN) === false && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.MUSICPACKS.PLAN) === true) {

                handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.VIDEOPACKS.PLAN = [this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.VIDEOPACKS.PLAN]

                return _.values((_.pick(handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS, 'MUSICPACKS', 'VIDEOPACKS')));
            }
        }
        else {
            console.log("for oleole gift");
            // for music & YOUTUBEPACKS & ELIGIBLEMAXUPPLANLIST pack check is Array
            if (Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.YOUTUBEPACKS.PLAN) && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.MUSICPACKS.PLAN) && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.ELIGIBLEMAXUPPLANLIST.PLAN)) {

                console.log("getUserAddonSubscriptionInfoPostpaid oleole ", JSON.stringify(this.userPlanInfo));
                return _.values((_.pick(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS, 'YOUTUBEPACKS', 'MUSICPACKS', 'ELIGIBLEMAXUPPLANLIST')));

            } // for YOUTUBEPACKS pack check is Array 
            else if (Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.YOUTUBEPACKS.PLAN) === true && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.MUSICPACKS.PLAN) === false && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.ELIGIBLEMAXUPPLANLIST.PLAN) === false) {
                console.log("for oleole gift 1");
                handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.ELIGIBLEADDONPACKS.PLAN = [this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.ELIGIBLEADDONPACKS.PLAN];

                handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.MUSICPACKS.PLAN = [this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.MUSICPACKS.PLAN];

                return _.values((_.pick(handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS, 'YOUTUBEPACKS', 'MUSICPACKS', 'ELIGIBLEMAXUPPLANLIST')));

            } // for music pack check is Array
            else if (Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.YOUTUBEPACKS.PLAN) === false && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.MUSICPACKS.PLAN) === true && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.ELIGIBLEMAXUPPLANLIST.PLAN) === false) {
                console.log("for oleole gift 2");
                handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.YOUTUBEPACKS.PLAN = [this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.YOUTUBEPACKS.PLAN];

                handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.ELIGIBLEMAXUPPLANLIST.PLAN = [this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.ELIGIBLEMAXUPPLANLIST.PLAN];

                return _.values((_.pick(handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS, 'YOUTUBEPACKS', 'MUSICPACKS', 'ELIGIBLEMAXUPPLANLIST')));
            } // for ELIGIBLEMAXUPPLANLIST pack check is Array
            else if (Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.YOUTUBEPACKS.PLAN) === false && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.MUSICPACKS.PLAN) === false && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.ELIGIBLEMAXUPPLANLIST.PLAN) === true) {
                console.log("for oleole gift 3");
                handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.YOUTUBEPACKS.PLAN = [this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.YOUTUBEPACKS.PLAN]

                handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.MUSICPACKS.PLAN = [this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.MUSICPACKS.PLAN]

                return _.values((_.pick(handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS, 'YOUTUBEPACKS', 'MUSICPACKS', 'ELIGIBLEMAXUPPLANLIST')));
            } // for music & YOUTUBEPACKS pack check is Array
            else if (Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.YOUTUBEPACKS.PLAN) === true && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.MUSICPACKS.PLAN) === true && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.ELIGIBLEMAXUPPLANLIST.PLAN) === false) {
                console.log("for oleole gift 4");
                handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.ELIGIBLEMAXUPPLANLIST.PLAN = [this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.ELIGIBLEMAXUPPLANLIST.PLAN]

                return _.values((_.pick(handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS, 'YOUTUBEPACKS', 'MUSICPACKS', 'ELIGIBLEMAXUPPLANLIST')));
            }// for music & ELIGIBLEMAXUPPLANLIST pack check is Array
            else if (Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.YOUTUBEPACKS.PLAN) === false && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.MUSICPACKS.PLAN) === true && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.ELIGIBLEMAXUPPLANLIST.PLAN) === true) {
                console.log("for oleole gift 5");
                handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.YOUTUBEPACKS.PLAN = [this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.YOUTUBEPACKS.PLAN]

                return _.values((_.pick(handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS, 'YOUTUBEPACKS', 'MUSICPACKS', 'ELIGIBLEMAXUPPLANLIST')));
            }// for YOUTUBEPACKS & ELIGIBLEMAXUPPLANLIST pack check is Array
            else if (Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.YOUTUBEPACKS.PLAN) === true && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.MUSICPACKS.PLAN) === false && Array.isArray(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.ELIGIBLEMAXUPPLANLIST.PLAN) === true) {
                console.log("for oleole gift 6");
                handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.MUSICPACKS.PLAN = [this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS.MUSICPACKS.PLAN]

                return _.values((_.pick(handle_Plan_object.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS, 'YOUTUBEPACKS', 'MUSICPACKS', 'ELIGIBLEMAXUPPLANLIST')));
            }
        }
        // return _.values(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEADDONPACKS);
    }

    // setUserIddSubscriptionInfo(obj){
    //     this.userIddPlanInfo = obj;
    // }

    // IDD for prepaid
    getUserIddSubscriptionInfo() {
        console.log("getUserIddSubscriptionInfo service ", _.values(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEIDD))
        return _.values(this.userPlanInfo.ProductInquiryResponse.ELIGIBLE_PRODUCTS.ELIGIBLEIDD);
    }

    // setUserRoamingSubscriptionInfo(obj){
    //     this.userRoamingPlanInfo = obj;
    // }

    // getUserRoamingSubscriptionInfo(){
    //     return this.userRoamingPlanInfo;
    // }

    setCountryList(list) {
        console.log("list of country ", JSON.stringify(list));
        this.countryList = list;
    }

    getCountryList() {
        return this.countryList;
    }

    setSubscriptionPostOrPrePlan(plans) {
        console.log("setSubscriptionPostPlan ", JSON.stringify(plans));
        this.subcriptionPostPlan = plans;
    }

    getSubscriptionPostOrPrePlan() {
        var handle_Pass_object: any = this.subcriptionPostPlan;
        // var handle_Pass_object : any = this.staticJsonForRoaming();


        if (this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST != undefined && !Array.isArray(this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS)) {

            handle_Pass_object.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS = [this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS];

            console.log("getSubscriptionPostOrPrePlan 1 ",/*JSON.stringify(handle_Pass_object)*/);

            // return _.values((_.pick(this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST, '','')));
        }

        if (this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGBASICPASSLIST != undefined && !Array.isArray(this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGBASICPASSLIST.PASS)) {

            handle_Pass_object.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGBASICPASSLIST.PASS = [this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGBASICPASSLIST.PASS];

            console.log("getSubscriptionPostOrPrePlan 2 ",/*JSON.stringify(handle_Pass_object)*/);

            // return _.values((_.pick(this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST, '','')));
        }

        if (this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGSOCIALPASSLIST != undefined && !Array.isArray(this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGSOCIALPASSLIST.PASS)) {

            handle_Pass_object.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGSOCIALPASSLIST.PASS = [this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGSOCIALPASSLIST.PASS];

            console.log("getSubscriptionPostOrPrePlan 3 ",/*JSON.stringify(handle_Pass_object)*/);

            // return _.values((_.pick(this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST, '','')));
        }

        console.log("handle_Pass_object ",/*_.values(handle_Pass_object.ROAMPRODUCTINQUIRYRESPONSE)*/);

        return _.values(handle_Pass_object.ROAMPRODUCTINQUIRYRESPONSE);
    }

    getSubscriptionPostpaid() {
        var handle_Pass_object: any = this.subcriptionPostPlan;
        // var handle_Pass_object : any = this.staticJsonForRoaming();

        try {
            if (this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST != undefined && !Array.isArray(this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS) && this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS.length == 1) {

                handle_Pass_object.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS = [this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS];

                // dummy HEADER_TITLE if one object
                handle_Pass_object.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.HEADER_TITLE = this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS[0].HEADER_TITLE;

                console.log("getSubscriptionPostOrPrePlan 1 postpaid ",/*JSON.stringify(handle_Pass_object)*/);

                return _.values(handle_Pass_object.ROAMPRODUCTINQUIRYRESPONSE);
                // return _.values((_.pick(this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST, '','')));
            }
            else if (this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST != undefined && Array.isArray(this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS) == false) {

                // handle_Pass_object.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS = [this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS];

                var json_Roam_postpaid = [
                    {
                        "PASS": [this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS],
                        "HEADER_TITLE": this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS.PASS_NAME
                    }
                ]
                // dummy HEADER_TITLE if one object
                // handle_Pass_object.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.HEADER_TITLE = this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS.PASS_NAME;

                console.log("getSubscriptionPostOrPrePlan 2 postpaid ", JSON.stringify(handle_Pass_object));

                return _.values(json_Roam_postpaid);

                // return _.values(handle_Pass_object.ROAMPRODUCTINQUIRYRESPONSE);
                // return _.values((_.pick(this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST, '','')));
            }
            else {

                console.log("json_Roam_postpaid in");

                var json_Roam_postpaid = [
                    {
                        "PASS": [this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS[0]],
                        "HEADER_TITLE": this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS[0].PASS_NAME
                    },
                    {
                        "PASS": [this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS[1]],
                        "HEADER_TITLE": this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS[1].PASS_NAME
                    },
                    {
                        "PASS": [this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS[2]],
                        "HEADER_TITLE": this.subcriptionPostPlan.ROAMPRODUCTINQUIRYRESPONSE.ROAMINGPASSLIST.PASS[2].PASS_NAME
                    }
                ];
                console.log("json_Roam_postpaid out ", JSON.stringify(json_Roam_postpaid));

                return JSON.parse(JSON.stringify(json_Roam_postpaid));

            }
        }
        catch (e) {
            console.log("getSubscriptionPostpaid catch ", e);
        }

        // console.log("handle_Pass_object postpaid ",/*_.values(handle_Pass_object.ROAMPRODUCTINQUIRYRESPONSE)*/);

    }

    staticJson() {
        return {
            "statusReason": "OK",
            "responseHeaders": {
                "Server": "Proprietary",
                "Connection": "close",
                "Content-length": "30023",
                "Content-Type": "text/xml;charset=utf-8"
            },
            "isSuccessful": true,
            "responseTime": 2546,
            "totalTime": 2549,
            "warnings": [],
            "errors": [],
            "ProductInquiryResponse": {
                "MAIN_PRODUCT_TYPE": "NEW_XPAX",
                "ACTIVEMIPRODUCT": {
                    "SMS_CARRY_FORWARD": "",
                    "PRODUCTSTATUS": "1",
                    "BUSINESS_NAME": "NEW XPAX TURBO",
                    "CLASSIFICATIONCODE": "Broadband renewal pre ICC",
                    "MMS_CARRY_FORWARD": "",
                    "EXPIRYDATE": "",
                    "PRODUCT_ID": "",
                    "PRODUCT_NAME": "",
                    "EFFECTIVEDATE": "",
                    "PRODUCT_TYPE": "",
                    "VOICE_CARRY_FORWARD": "",
                    "DATA_CARRY_FORWARD": "",
                    "AUTORENEWAL_FLAG": "0"
                },
                "ResponseCode": "Xapx0000",
                "MSISDN": "0138372028",
                "ELIGIBLE_PRODUCTS": {
                    "ELIGIBLETREMENDOUSPLANS": {
                        "HEADER_TITLE": "Internet Plans",
                        "HEADER_SUBTITLE": "Promo!",
                        "PLAN": [
                            {
                                "CARRFORWARD_FLAG": "0",
                                "WEEKEND_INTERNET": "-",
                                "WIFI_QUOTA": "Unlimited",
                                "FREE_ONNET_CALLS": "-",
                                "FREE_OFFNET_SMS": "-",
                                "FREE_OFFNET_MMS": "-",
                                "PLAN_NAME": "Monthly",
                                "PRODUCT_PRICE_DISPLAY": "RM79",
                                "FREE_OFFNET_CALLS": "-",
                                "FREE_ONNET_MMS": "-",
                                "REGISTERED_ALRDY": "N",
                                "VALIDITY": "30 Days",
                                "YONDER_FLAG": "1",
                                "AUTORENEWAL_FLAG": "0",
                                "WIFI_VALIDITY": "30 Days",
                                "BUSINESS_NAME": "30DAYS 15GB",
                                "FREE_ONNET_SMS": "-",
                                "PRODUCT_ID": "40767",
                                "PLAN_DESCRIPTION": "RM79 for 15GB Monthly",
                                "OTHER": {
                                    "other_item": [
                                        "Video Walla - 15GB",
                                        "Music Walla - Unlimited",
                                        "Free Yonder Music - Unlimited"
                                    ]
                                },
                                "PRODUCT_TYPE": "NEW_XPAX",
                                "PRODUCT_KEY": "30DAYS 15GB",
                                "PRODUCT_PRICE": "79",
                                "PRODUCT_QUOTA": "15GB",
                                "FB_QUOTA": "-",
                                "FREEYONDERMUSIC": "Unlimited",
                                "YT_QUOTA": "-",
                                "OTHERDATA": {
                                    "INSTAGRAM_SUBTITLE": "Promo!",
                                    "OTHER_VALUE": "10GB",
                                    "OTHER_TAG": "Instagram Walla"
                                },
                                "FREE_INTERNET_FLAG": "1",
                                "PLAN_DESCRIPTION_SUBSCRIPT": "-"
                            },
                            {
                                "CARRFORWARD_FLAG": "0",
                                "WEEKEND_INTERNET": "-",
                                "WIFI_QUOTA": "Unlimited",
                                "FREE_ONNET_CALLS": "-",
                                "FREE_OFFNET_SMS": "-",
                                "FREE_OFFNET_MMS": "-",
                                "PLAN_NAME": "Monthly",
                                "PRODUCT_PRICE_DISPLAY": "RM50",
                                "FREE_OFFNET_CALLS": "-",
                                "FREE_ONNET_MMS": "-",
                                "REGISTERED_ALRDY": "N",
                                "VALIDITY": "30 Days",
                                "YONDER_FLAG": "1",
                                "AUTORENEWAL_FLAG": "0",
                                "WIFI_VALIDITY": "30 Days",
                                "BUSINESS_NAME": "30DAYS 10GB",
                                "FREE_ONNET_SMS": "-",
                                "PRODUCT_ID": "40565",
                                "PLAN_DESCRIPTION": "RM50 for 10GB Monthly",
                                "OTHER": {
                                    "other_item": [
                                        "Music Walla - Unlimited",
                                        "Free Yonder Music - Unlimited"
                                    ]
                                },
                                "PRODUCT_TYPE": "NEW_XPAX",
                                "PRODUCT_KEY": "30DAYS 10GB",
                                "PRODUCT_PRICE": "50",
                                "PRODUCT_QUOTA": "10GB",
                                "FB_QUOTA": "-",
                                "FREEYONDERMUSIC": "Unlimited",
                                "YT_QUOTA": "-",
                                "OTHERDATA": {
                                    "INSTAGRAM_SUBTITLE": "Promo!",
                                    "OTHER_VALUE": "10GB",
                                    "OTHER_TAG": "Instagram Walla"
                                },
                                "FREE_INTERNET_FLAG": "1",
                                "PLAN_DESCRIPTION_SUBSCRIPT": "-"
                            },
                            {
                                "CARRFORWARD_FLAG": "0",
                                "WEEKEND_INTERNET": "-",
                                "WIFI_QUOTA": "Unlimited",
                                "FREE_ONNET_CALLS": "-",
                                "FREE_OFFNET_SMS": "-",
                                "FREE_OFFNET_MMS": "-",
                                "PLAN_NAME": "Monthly",
                                "PRODUCT_PRICE_DISPLAY": "RM30",
                                "FREE_OFFNET_CALLS": "-",
                                "FREE_ONNET_MMS": "-",
                                "REGISTERED_ALRDY": "N",
                                "VALIDITY": "30 Days",
                                "YONDER_FLAG": "1",
                                "AUTORENEWAL_FLAG": "0",
                                "WIFI_VALIDITY": "30 Days",
                                "BUSINESS_NAME": "30DAYS 5GB",
                                "FREE_ONNET_SMS": "-",
                                "PRODUCT_ID": "40564",
                                "PLAN_DESCRIPTION": "RM30 for 5GB Monthly",
                                "OTHER": "Free Unlimited Yonder Music",
                                "PRODUCT_TYPE": "NEW_XPAX",
                                "PRODUCT_KEY": "30DAYS 5GB",
                                "PRODUCT_PRICE": "30",
                                "PRODUCT_QUOTA": "5GB",
                                "FB_QUOTA": "-",
                                "FREEYONDERMUSIC": "Unlimited",
                                "YT_QUOTA": "-",
                                "OTHERDATA": {
                                    "INSTAGRAM_SUBTITLE": "Promo!",
                                    "OTHER_VALUE": "10GB",
                                    "OTHER_TAG": "Instagram Walla"
                                },
                                "FREE_INTERNET_FLAG": "1",
                                "PLAN_DESCRIPTION_SUBSCRIPT": "-"
                            },
                            {
                                "CARRFORWARD_FLAG": "0",
                                "WEEKEND_INTERNET": "-",
                                "WIFI_QUOTA": "Unlimited",
                                "FREE_ONNET_CALLS": "-",
                                "FREE_OFFNET_SMS": "-",
                                "FREE_OFFNET_MMS": "-",
                                "PLAN_NAME": "Weekly",
                                "PRODUCT_PRICE_DISPLAY": "RM19",
                                "FREE_OFFNET_CALLS": "-",
                                "FREE_ONNET_MMS": "-",
                                "VALIDITY": "7 Days",
                                "YONDER_FLAG": "1",
                                "AUTORENEWAL_FLAG": "0",
                                "WIFI_VALIDITY": "7 Days",
                                "BUSINESS_NAME": "7DAYS 5GB",
                                "FREE_ONNET_SMS": "-",
                                "PRODUCT_ID": "40766",
                                "PLAN_DESCRIPTION": "RM19 for 5GB Weekly",
                                "OTHER": "",
                                "PRODUCT_TYPE": "NEW_XPAX",
                                "PRODUCT_KEY": "7DAYS 5GB",
                                "PRODUCT_PRICE": "19",
                                "PRODUCT_QUOTA": "5GB",
                                "FB_QUOTA": "-",
                                "FREEYONDERMUSIC": "Unlimited",
                                "YT_QUOTA": "-",
                                "OTHERDATA": {
                                    "INSTAGRAM_SUBTITLE": "Promo!",
                                    "OTHER_VALUE": "10GB",
                                    "OTHER_TAG": "Instagram Walla"
                                },
                                "FREE_INTERNET_FLAG": "1",
                                "PLAN_DESCRIPTION_SUBSCRIPT": "-"
                            },
                            {
                                "CARRFORWARD_FLAG": "0",
                                "WEEKEND_INTERNET": "-",
                                "WIFI_QUOTA": "Unlimited",
                                "FREE_ONNET_CALLS": "-",
                                "FREE_OFFNET_SMS": "-",
                                "FREE_OFFNET_MMS": "-",
                                "PLAN_NAME": "Weekly",
                                "PRODUCT_PRICE_DISPLAY": "RM10",
                                "FREE_OFFNET_CALLS": "-",
                                "FREE_ONNET_MMS": "-",
                                "VALIDITY": "7 Days",
                                "YONDER_FLAG": "1",
                                "AUTORENEWAL_FLAG": "0",
                                "WIFI_VALIDITY": "7 Days",
                                "BUSINESS_NAME": "7DAYS 2GB",
                                "FREE_ONNET_SMS": "-",
                                "PRODUCT_ID": "40562",
                                "PLAN_DESCRIPTION": "RM10 for 2GB Weekly",
                                "OTHER": "Free Unlimited Yonder Music",
                                "PRODUCT_TYPE": "NEW_XPAX",
                                "PRODUCT_KEY": "7DAYS 2GB",
                                "PRODUCT_PRICE": "10",
                                "PRODUCT_QUOTA": "2GB",
                                "FB_QUOTA": "-",
                                "FREEYONDERMUSIC": "Unlimited",
                                "YT_QUOTA": "-",
                                "OTHERDATA": {
                                    "INSTAGRAM_SUBTITLE": "Promo!",
                                    "OTHER_VALUE": "10GB",
                                    "OTHER_TAG": "Instagram Walla"
                                },
                                "FREE_INTERNET_FLAG": "1",
                                "PLAN_DESCRIPTION_SUBSCRIPT": "-"
                            },
                            {
                                "CARRFORWARD_FLAG": "0",
                                "WEEKEND_INTERNET": "-",
                                "WIFI_QUOTA": "Unlimited",
                                "FREE_ONNET_CALLS": "-",
                                "FREE_OFFNET_SMS": "-",
                                "FREE_OFFNET_MMS": "-",
                                "PLAN_NAME": "Weekly",
                                "PRODUCT_PRICE_DISPLAY": "RM6",
                                "FREE_OFFNET_CALLS": "-",
                                "FREE_ONNET_MMS": "-",
                                "VALIDITY": "7 Days",
                                "YONDER_FLAG": "1",
                                "AUTORENEWAL_FLAG": "0",
                                "WIFI_VALIDITY": "7 Days",
                                "BUSINESS_NAME": "7DAYS 500MB",
                                "FREE_ONNET_SMS": "-",
                                "PRODUCT_ID": "40765",
                                "PLAN_DESCRIPTION": "RM6 for 500MB Weekly",
                                "OTHER": "",
                                "PRODUCT_TYPE": "NEW_XPAX",
                                "PRODUCT_KEY": "7DAYS 500MB",
                                "PRODUCT_PRICE": "6",
                                "PRODUCT_QUOTA": "500MB",
                                "FB_QUOTA": "-",
                                "FREEYONDERMUSIC": "Unlimited",
                                "YT_QUOTA": "-",
                                "OTHERDATA": {
                                    "INSTAGRAM_SUBTITLE": "Promo!",
                                    "OTHER_VALUE": "10GB",
                                    "OTHER_TAG": "Instagram Walla"
                                },
                                "FREE_INTERNET_FLAG": "1",
                                "PLAN_DESCRIPTION_SUBSCRIPT": "-"
                            },
                            {
                                "CARRFORWARD_FLAG": "0",
                                "WEEKEND_INTERNET": "-",
                                "WIFI_QUOTA": "Unlimited",
                                "FREE_ONNET_CALLS": "-",
                                "FREE_OFFNET_SMS": "-",
                                "FREE_OFFNET_MMS": "-",
                                "PLAN_NAME": "Daily",
                                "PRODUCT_PRICE_DISPLAY": "RM5",
                                "FREE_OFFNET_CALLS": "-",
                                "FREE_ONNET_MMS": "-",
                                "VALIDITY": "24 Hours",
                                "YONDER_FLAG": "1",
                                "AUTORENEWAL_FLAG": "1",
                                "WIFI_VALIDITY": "24 Hours",
                                "BUSINESS_NAME": "1DAY 2GB",
                                "FREE_ONNET_SMS": "-",
                                "PRODUCT_ID": "40764",
                                "PLAN_DESCRIPTION": "RM5 for 2GB Daily",
                                "OTHER": "",
                                "PRODUCT_TYPE": "NEW_XPAX",
                                "PRODUCT_KEY": "1DAY 2GB",
                                "PRODUCT_PRICE": "5",
                                "PRODUCT_QUOTA": "2GB",
                                "FB_QUOTA": "-",
                                "FREEYONDERMUSIC": "Unlimited",
                                "YT_QUOTA": "-",
                                "OTHERDATA": {
                                    "INSTAGRAM_SUBTITLE": "Promo!",
                                    "OTHER_VALUE": "10GB",
                                    "OTHER_TAG": "Instagram Walla"
                                },
                                "FREE_INTERNET_FLAG": "1",
                                "PLAN_DESCRIPTION_SUBSCRIPT": "-"
                            },
                            {
                                "CARRFORWARD_FLAG": "0",
                                "WEEKEND_INTERNET": "-",
                                "WIFI_QUOTA": "Unlimited",
                                "FREE_ONNET_CALLS": "-",
                                "FREE_OFFNET_SMS": "-",
                                "FREE_OFFNET_MMS": "-",
                                "PLAN_NAME": "Daily",
                                "PRODUCT_PRICE_DISPLAY": "RM3",
                                "FREE_OFFNET_CALLS": "-",
                                "FREE_ONNET_MMS": "-",
                                "VALIDITY": "24 Hours",
                                "YONDER_FLAG": "1",
                                "AUTORENEWAL_FLAG": "1",
                                "WIFI_VALIDITY": "24 Hours",
                                "BUSINESS_NAME": "1DAY 1GB",
                                "FREE_ONNET_SMS": "-",
                                "PRODUCT_ID": "40561",
                                "PLAN_DESCRIPTION": "RM3 for 1GB Daily",
                                "OTHER": "Free Unlimited Yonder Music",
                                "PRODUCT_TYPE": "NEW_XPAX",
                                "PRODUCT_KEY": "1DAY 1GB",
                                "PRODUCT_PRICE": "3",
                                "PRODUCT_QUOTA": "1GB",
                                "FB_QUOTA": "-",
                                "FREEYONDERMUSIC": "Unlimited",
                                "YT_QUOTA": "-",
                                "OTHERDATA": {
                                    "INSTAGRAM_SUBTITLE": "Promo!",
                                    "OTHER_VALUE": "10GB",
                                    "OTHER_TAG": "Instagram Walla"
                                },
                                "FREE_INTERNET_FLAG": "1",
                                "PLAN_DESCRIPTION_SUBSCRIPT": "-"
                            }
                        ]
                    },
                    "ELIGIBLEADDONPACKS": {
                        "ELIGIBLEMAXUPPLANLIST": {
                            "HEADER_TITLE": "Internet Add-on",
                            "PLAN": [
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "3",
                                    "BUSINESS_NAME": "MAX100MB",
                                    "PLAN_NAME": "MAX100MB",
                                    "PRODUCT_ID": "45561",
                                    "PLAN_DESCRIPTION": "RM3 for 100MB",
                                    "PRODUCT_PRICE_DISPLAY": "RM3",
                                    "PRODUCT_VOLUME": "100MB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Follow Internet plan validity"
                                },
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "10",
                                    "BUSINESS_NAME": "MAX500MB",
                                    "PLAN_NAME": "MAX500MB",
                                    "PRODUCT_ID": "45562",
                                    "PLAN_DESCRIPTION": "RM10 for 500MB",
                                    "PRODUCT_PRICE_DISPLAY": "RM10",
                                    "PRODUCT_VOLUME": "500MB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Follow Internet plan validity"
                                },
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "15",
                                    "BUSINESS_NAME": "MAX1GB",
                                    "PLAN_NAME": "MAX1GB",
                                    "PRODUCT_ID": "45563",
                                    "PLAN_DESCRIPTION": "RM15 for 1GB",
                                    "PRODUCT_PRICE_DISPLAY": "RM15",
                                    "PRODUCT_VOLUME": "1GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Follow Internet plan validity"
                                },
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "50",
                                    "BUSINESS_NAME": "MAX5GB",
                                    "PLAN_NAME": "MAX5GB",
                                    "PRODUCT_ID": "45564",
                                    "PLAN_DESCRIPTION": "RM50 for 5GB",
                                    "PRODUCT_PRICE_DISPLAY": "RM50",
                                    "PRODUCT_VOLUME": "5GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Follow Internet plan validity"
                                }
                            ]
                        },
                        "INSTAGRAMPACKS": {
                            "HEADER_TITLE": "Instagram Walla",
                            "HEADER_SUBTITLE": "NEW",
                            "PLAN": [
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "1",
                                    "VALIDITY_DATE": "24 Hours",
                                    "BUSINESS_NAME": "Instagram Daily",
                                    "PLAN_NAME": "Daily",
                                    "PRODUCT_ID": "40876",
                                    "PLAN_DESCRIPTION": "RM1 for 1GB Daily",
                                    "PRODUCT_PRICE_DISPLAY": "RM1",
                                    "PRODUCT_VOLUME": "1GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 24 hours"
                                },
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "7",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "Instagram Weekly",
                                    "PLAN_NAME": "Weekly",
                                    "PRODUCT_ID": "40877",
                                    "PLAN_DESCRIPTION": "RM7 for 10GB Weekly",
                                    "PRODUCT_PRICE_DISPLAY": "RM7",
                                    "PRODUCT_VOLUME": "10GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                },
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "20",
                                    "VALIDITY_DATE": "30 Days",
                                    "BUSINESS_NAME": "Instagram Monthly",
                                    "PLAN_NAME": "Monthly",
                                    "PRODUCT_ID": "40878",
                                    "PLAN_DESCRIPTION": "RM20 for 15GB Monthly",
                                    "PRODUCT_PRICE_DISPLAY": "RM20",
                                    "PRODUCT_VOLUME": "15GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 30 days"
                                }
                            ]
                        },
                        "FBPACKS": {
                            "HEADER_TITLE": "Facebook Walla",
                            "PLAN": [
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "1",
                                    "VALIDITY_DATE": "1 Day",
                                    "BUSINESS_NAME": "FB Daily 1 GB",
                                    "PLAN_NAME": "Daily",
                                    "PRODUCT_ID": "40773",
                                    "PLAN_DESCRIPTION": "RM1 for 1GB Daily",
                                    "PRODUCT_PRICE_DISPLAY": "RM1",
                                    "PRODUCT_VOLUME": "1GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 24 hours"
                                },
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "7",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "FB Weekly 10 GB",
                                    "PLAN_NAME": "Weekly",
                                    "PRODUCT_ID": "40781",
                                    "PLAN_DESCRIPTION": "RM7 for 10GB Weekly",
                                    "PRODUCT_PRICE_DISPLAY": "RM7",
                                    "PRODUCT_VOLUME": "10GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                },
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "20",
                                    "VALIDITY_DATE": "30 Days",
                                    "BUSINESS_NAME": "FB Monthly 15 GB",
                                    "PLAN_NAME": "Monthly",
                                    "PRODUCT_ID": "40775",
                                    "PLAN_DESCRIPTION": "RM20 for 15GB Monthly",
                                    "PRODUCT_PRICE_DISPLAY": "RM20",
                                    "PRODUCT_VOLUME": "15GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 30 days"
                                }
                            ]
                        },
                        "YOUTUBEPACKS": {
                            "HEADER_TITLE": "VideoTube",
                            "PLAN": [
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "1",
                                    "VALIDITY_DATE": "24 Hours",
                                    "BUSINESS_NAME": "YouTube Daily",
                                    "PLAN_NAME": "Daily",
                                    "PRODUCT_ID": "40570",
                                    "PLAN_DESCRIPTION": "RM1 for 1GB Daily",
                                    "PRODUCT_PRICE_DISPLAY": "RM1",
                                    "PRODUCT_VOLUME": "1GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 24 hours"
                                },
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "7",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "YouTube Weekly",
                                    "PLAN_NAME": "Weekly",
                                    "PRODUCT_ID": "40884",
                                    "PLAN_DESCRIPTION": "RM7 for 10GB Weekly",
                                    "PRODUCT_PRICE_DISPLAY": "RM7",
                                    "PRODUCT_VOLUME": "10GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                },
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "20",
                                    "VALIDITY_DATE": "30 Days",
                                    "BUSINESS_NAME": "YouTube Monthly",
                                    "PLAN_NAME": "Monthly",
                                    "PRODUCT_ID": "40885",
                                    "PLAN_DESCRIPTION": "RM20 for 15GB Monthly",
                                    "PRODUCT_PRICE_DISPLAY": "RM20",
                                    "PRODUCT_VOLUME": "15GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 30 days"
                                }
                            ]
                        },
                        "BASICINTERNET": {
                            "HEADER_TITLE": "BASIC INTERNET",
                            "PLAN": [
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "1",
                                    "VALIDITY_DATE": "24 Hours",
                                    "BUSINESS_NAME": "BASIC_INTERNET1",
                                    "PLAN_NAME": "Daily",
                                    "PRODUCT_ID": "40572",
                                    "PLAN_DESCRIPTION": "Unlimited for RM1 Daily",
                                    "PRODUCT_PRICE_DISPLAY": "RM1",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 24 hours"
                                },
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "7",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "BASIC_INTERNET7",
                                    "PLAN_NAME": "Weekly",
                                    "PRODUCT_ID": "40573",
                                    "PLAN_DESCRIPTION": "Unlimited for RM7 Weekly",
                                    "PRODUCT_PRICE_DISPLAY": "RM7",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                },
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "20",
                                    "VALIDITY_DATE": "30 Days",
                                    "BUSINESS_NAME": "BASIC_INTERNET30",
                                    "PLAN_NAME": "Monthly",
                                    "PRODUCT_ID": "40776",
                                    "PLAN_DESCRIPTION": "Unlimited for RM20 Monthly",
                                    "PRODUCT_PRICE_DISPLAY": "RM20",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 30 days"
                                }
                            ]
                        },
                        "MUSICPACKS": {
                            "HEADER_TITLE": "Music Pack",
                            "PLAN": [
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "1",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "Music Weekly",
                                    "PLAN_NAME": "Weekly",
                                    "PRODUCT_ID": "41048",
                                    "PLAN_DESCRIPTION": "RM1 for Unlimited Weekly",
                                    "PRODUCT_PRICE_DISPLAY": "RM1",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                },
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "3",
                                    "VALIDITY_DATE": "30 Days",
                                    "BUSINESS_NAME": "Music Monthly",
                                    "PLAN_NAME": "Monthly",
                                    "PRODUCT_ID": "41049",
                                    "PLAN_DESCRIPTION": "RM3 for Unlimited Monthly",
                                    "PRODUCT_PRICE_DISPLAY": "RM3",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 30 days"
                                }
                            ]
                        },
                        "CALLPACKS": {
                            "HEADER_TITLE": "Call Add-on",
                            "PLAN": [
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "1",
                                    "VALIDITY_DATE": "24 Hours",
                                    "BUSINESS_NAME": "CALL PACK",
                                    "PLAN_NAME": "Daily",
                                    "PRODUCT_ID": "40695",
                                    "PLAN_DESCRIPTION": "Unlimited Calls within the Same Network RM1 Daily",
                                    "PRODUCT_PRICE_DISPLAY": "RM1",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 24 hours"
                                },
                                {
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "7",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "CALL PACK",
                                    "PLAN_NAME": "Weekly",
                                    "PRODUCT_ID": "40705",
                                    "PLAN_DESCRIPTION": "Unlimited Calls within the Same Network RM7 Weekly",
                                    "PRODUCT_PRICE_DISPLAY": "RM7",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                }
                            ]
                        },
                        "INTERNETBURUNGHANTU": {
                            "HEADER_TITLE": "Late Night Internet",
                            "PLAN": [
                                {
                                    "PLAN_DESCRIPTION_SUBSCRIPT1": "Valid for 1 day",
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "1",
                                    "VALIDITY_DATE": "1 Day",
                                    "BUSINESS_NAME": "1DAY NIGHT",
                                    "PLAN_NAME": "1DAY NIGHT",
                                    "PRODUCT_ID": "40566",
                                    "PLAN_DESCRIPTION": "RM1 for 1GB Daily",
                                    "PRODUCT_PRICE_DISPLAY": "RM1",
                                    "PRODUCT_VOLUME": "1GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid from 1am to 7am"
                                },
                                {
                                    "PLAN_DESCRIPTION_SUBSCRIPT1": "Valid for 7 days",
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "7",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "7DAYS NIGHT",
                                    "PLAN_NAME": "7DAYS NIGHT",
                                    "PRODUCT_ID": "40567",
                                    "PLAN_DESCRIPTION": "RM7 for 10GB Weekly",
                                    "PRODUCT_PRICE_DISPLAY": "RM7",
                                    "PRODUCT_VOLUME": "10GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid from 1am to 7am"
                                },
                                {
                                    "PLAN_DESCRIPTION_SUBSCRIPT1": "Valid for 30 days",
                                    "PRODUCT_TYPE": "NEW_XPAX",
                                    "PRODUCT_PRICE": "20",
                                    "VALIDITY_DATE": "30 Days",
                                    "BUSINESS_NAME": "30DAYS NIGHT",
                                    "PLAN_NAME": "30DAYS NIGHT",
                                    "PRODUCT_ID": "40774",
                                    "PLAN_DESCRIPTION": "RM20 for 15GB Monthly",
                                    "PRODUCT_PRICE_DISPLAY": "RM20",
                                    "PRODUCT_VOLUME": "15GB",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid from 1am to 7am"
                                }
                            ]
                        }
                    },
                    "ELIGIBLEIDD": {
                        "ASIAPASS": {
                            "HEADER_TITLE": "Asia Pass",
                            "PLAN": [
                                {
                                    "YOUTUBE_VOLUME": "1GB",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "IDD",
                                    "IDD_CALLS_TEXT": "IDD Calls to Indonesia",
                                    "IDD_CALLS_FLAG": "1",
                                    "PLAN_NAME": "WEEKLY",
                                    "PRODUCT_ID": "40745",
                                    "PLAN_DESCRIPTION": "RM9 for Indonesia",
                                    "YOUTUBE_FLAG": "1",
                                    "PRODUCT_PRICE_DISPLAY": "RM9",
                                    "INTERNET_VOLUME": "1GB",
                                    "PRODUCT_TYPE": "IDD",
                                    "INTERNET_FLAG": "1",
                                    "PRODUCT_PRICE": "9",
                                    "LOCAL_CALLS_FLAG": "1",
                                    "CALLS_VOLUME": "100 Mins",
                                    "IDD_CALLS_VOLUME": "200 Mins",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                },
                                {
                                    "YOUTUBE_VOLUME": "1GB",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "IDD",
                                    "IDD_CALLS_TEXT": "IDD Calls to Nepal",
                                    "IDD_CALLS_FLAG": "1",
                                    "PLAN_NAME": "WEEKLY",
                                    "PRODUCT_ID": "40746",
                                    "PLAN_DESCRIPTION": "RM9 for Nepal",
                                    "YOUTUBE_FLAG": "1",
                                    "PRODUCT_PRICE_DISPLAY": "RM9",
                                    "INTERNET_VOLUME": "1GB",
                                    "PRODUCT_TYPE": "IDD",
                                    "INTERNET_FLAG": "1",
                                    "PRODUCT_PRICE": "9",
                                    "LOCAL_CALLS_FLAG": "1",
                                    "CALLS_VOLUME": "100 Mins",
                                    "IDD_CALLS_VOLUME": "40 Mins",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                },
                                {
                                    "YOUTUBE_VOLUME": "1GB",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "IDD",
                                    "IDD_CALLS_TEXT": "IDD Calls to Bangladesh",
                                    "IDD_CALLS_FLAG": "1",
                                    "PLAN_NAME": "WEEKLY",
                                    "PRODUCT_ID": "40747",
                                    "PLAN_DESCRIPTION": "RM9 for Bangladesh",
                                    "YOUTUBE_FLAG": "1",
                                    "PRODUCT_PRICE_DISPLAY": "RM9",
                                    "INTERNET_VOLUME": "1GB",
                                    "PRODUCT_TYPE": "IDD",
                                    "INTERNET_FLAG": "1",
                                    "PRODUCT_PRICE": "9",
                                    "LOCAL_CALLS_FLAG": "1",
                                    "CALLS_VOLUME": "100 Mins",
                                    "IDD_CALLS_VOLUME": "120 Mins",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                },
                                {
                                    "YOUTUBE_VOLUME": "1GB",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "IDD",
                                    "IDD_CALLS_TEXT": "IDD Calls to Myanmar",
                                    "IDD_CALLS_FLAG": "1",
                                    "PLAN_NAME": "WEEKLY",
                                    "PRODUCT_ID": "40748",
                                    "PLAN_DESCRIPTION": "RM9 for Myanmar",
                                    "YOUTUBE_FLAG": "1",
                                    "PRODUCT_PRICE_DISPLAY": "RM9",
                                    "INTERNET_VOLUME": "1GB",
                                    "PRODUCT_TYPE": "IDD",
                                    "INTERNET_FLAG": "1",
                                    "PRODUCT_PRICE": "9",
                                    "LOCAL_CALLS_FLAG": "1",
                                    "CALLS_VOLUME": "100 Mins",
                                    "IDD_CALLS_VOLUME": "40 Mins",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                },
                                {
                                    "YOUTUBE_VOLUME": "1GB",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "IDD",
                                    "IDD_CALLS_TEXT": "IDD Calls to Vietnam",
                                    "IDD_CALLS_FLAG": "1",
                                    "PLAN_NAME": "WEEKLY",
                                    "PRODUCT_ID": "40749",
                                    "PLAN_DESCRIPTION": "RM9 for Vietnam",
                                    "YOUTUBE_FLAG": "1",
                                    "PRODUCT_PRICE_DISPLAY": "RM9",
                                    "INTERNET_VOLUME": "1GB",
                                    "PRODUCT_TYPE": "IDD",
                                    "INTERNET_FLAG": "1",
                                    "PRODUCT_PRICE": "9",
                                    "LOCAL_CALLS_FLAG": "1",
                                    "CALLS_VOLUME": "100 Mins",
                                    "IDD_CALLS_VOLUME": "40 Mins",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                },
                                {
                                    "YOUTUBE_VOLUME": "1GB",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "IDD",
                                    "IDD_CALLS_TEXT": "IDD Calls to China",
                                    "IDD_CALLS_FLAG": "1",
                                    "PLAN_NAME": "WEEKLY",
                                    "PRODUCT_ID": "40750",
                                    "PLAN_DESCRIPTION": "RM9 for China",
                                    "YOUTUBE_FLAG": "1",
                                    "PRODUCT_PRICE_DISPLAY": "RM9",
                                    "INTERNET_VOLUME": "1GB",
                                    "PRODUCT_TYPE": "IDD",
                                    "INTERNET_FLAG": "1",
                                    "PRODUCT_PRICE": "9",
                                    "LOCAL_CALLS_FLAG": "1",
                                    "CALLS_VOLUME": "100 Mins",
                                    "IDD_CALLS_VOLUME": "120 Mins",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                },
                                {
                                    "YOUTUBE_VOLUME": "1GB",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "IDD",
                                    "IDD_CALLS_TEXT": "IDD Calls to Thailand",
                                    "IDD_CALLS_FLAG": "1",
                                    "PLAN_NAME": "WEEKLY",
                                    "PRODUCT_ID": "40751",
                                    "PLAN_DESCRIPTION": "RM9 for Thailand",
                                    "YOUTUBE_FLAG": "1",
                                    "PRODUCT_PRICE_DISPLAY": "RM9",
                                    "INTERNET_VOLUME": "1GB",
                                    "PRODUCT_TYPE": "IDD",
                                    "INTERNET_FLAG": "1",
                                    "PRODUCT_PRICE": "9",
                                    "LOCAL_CALLS_FLAG": "1",
                                    "CALLS_VOLUME": "100 Mins",
                                    "IDD_CALLS_VOLUME": "40 Mins",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                },
                                {
                                    "YOUTUBE_VOLUME": "1GB",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "IDD",
                                    "IDD_CALLS_TEXT": "IDD Calls to Philippines",
                                    "IDD_CALLS_FLAG": "1",
                                    "PLAN_NAME": "WEEKLY",
                                    "PRODUCT_ID": "40752",
                                    "PLAN_DESCRIPTION": "RM9 for Philippines",
                                    "YOUTUBE_FLAG": "1",
                                    "PRODUCT_PRICE_DISPLAY": "RM9",
                                    "INTERNET_VOLUME": "1GB",
                                    "PRODUCT_TYPE": "IDD",
                                    "INTERNET_FLAG": "1",
                                    "PRODUCT_PRICE": "9",
                                    "LOCAL_CALLS_FLAG": "1",
                                    "CALLS_VOLUME": "100 Mins",
                                    "IDD_CALLS_VOLUME": "25 Mins",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                },
                                {
                                    "YOUTUBE_VOLUME": "1GB",
                                    "VALIDITY_DATE": "7 Days",
                                    "BUSINESS_NAME": "IDD",
                                    "IDD_CALLS_TEXT": "IDD Calls to India",
                                    "IDD_CALLS_FLAG": "1",
                                    "PLAN_NAME": "WEEKLY",
                                    "PRODUCT_ID": "40753",
                                    "PLAN_DESCRIPTION": "RM9 for India",
                                    "YOUTUBE_FLAG": "1",
                                    "PRODUCT_PRICE_DISPLAY": "RM9",
                                    "INTERNET_VOLUME": "1GB",
                                    "PRODUCT_TYPE": "IDD",
                                    "INTERNET_FLAG": "1",
                                    "PRODUCT_PRICE": "9",
                                    "LOCAL_CALLS_FLAG": "1",
                                    "CALLS_VOLUME": "100 Mins",
                                    "IDD_CALLS_VOLUME": "250 Mins",
                                    "PRODUCT_VOLUME": "Unlimited",
                                    "PLAN_DESCRIPTION_SUBSCRIPT": "Valid for 7 days"
                                }
                            ]
                        }
                    },
                    "ELIGIBLEMAINPLANLIST": {
                        "HEADER_TITLE": "Change Plan",
                        "PLAN": {
                            "PRODUCT_NAME": "NEW XPAX V2",
                            "PRODUCT_ID": "25127"
                        }
                    }
                },
                "Active_Subscription": "1",
                "ACTIVEMAINPRODUCT": {
                    "FREE_OFFNET_CALLS": "",
                    "FREE_ONNET_MMS": "",
                    "FREE_ALLNET_SMS": "",
                    "FREE_ONNET_CALLS": "",
                    "FREE_OFFNET_SMS": "",
                    "FREE_OFFNET_MMS": "",
                    "FREE_ALLNET_MMS": "",
                    "FREE_ONNET_SMS": ""
                },
                "ResponseMessage": "Success",
                "TRANSACTION_ID": "XNdU1nJFTPsXuHwN"
            },
            "info": [],
            "statusCode": 200
        };
    }

    staticJsonForRoaming() {
        return {
            "ROAMPRODUCTINQUIRYRESPONSE": {
                "ROAMINGPASSLIST": {
                    "PASS": [
                        {
                            "PASS_ISPROMOAVAILABLE": "True",
                            "PASS_NAME": "1-Day Internet Pass",
                            "PASS_WITHOUTPROMOPRICE": "",
                            "PASS_DESC": "RM4.99",
                            "PASS_VISITEDCOUNTRY_SUBSCRIPT": "",
                            "PASS_PRICE_DISPLAY": "RM4.99",
                            "PASS_VOLUME": "AUTO SUBSCRIPTION",
                            "PASS_VALIDITY": "Valid for 1 Day",
                            "PASS_PROMOPERIOD": "17 AUG 2017 - 16 NOV 2017",
                            "PASS_LOCAL_SUBSCRIPT": "Selected Countries",
                            "PASS_QUOTA": "500MB",
                            "PASS_VALIDITY_COUNTRIES": "Valid in 6 asia countries",
                            "IS_PURCHASE_ALLOWED": "N",
                            "PASS_ID": "40755",
                            "PASS_VALID": "1 Day",
                            "PASS_PRICE": "RM4.99/day"
                        },
                        {
                            "PASS_ISPROMOAVAILABLE": "False",
                            "PASS_NAME": "1-Day Internet Pass",
                            "PASS_WITHOUTPROMOPRICE": "",
                            "PASS_DESC": "RM38",
                            "PASS_VISITEDCOUNTRY_SUBSCRIPT": "",
                            "PASS_PRICE_DISPLAY": "RM38",
                            "PASS_VOLUME": "AUTO SUBSCRIPTION",
                            "PASS_VALIDITY": "Valid for 1 Day",
                            "PASS_PROMOPERIOD": "",
                            "PASS_LOCAL_SUBSCRIPT": "",
                            "PASS_QUOTA": "",
                            "PASS_VALIDITY_COUNTRIES": "Valid in more than 160 countries",
                            "IS_PURCHASE_ALLOWED": "Y",
                            "PASS_NEWPROMOPRICE": "",
                            "PASS_ID": "45687",
                            "PASS_VALID": "1 Day",
                            "PASS_PRICE": "RM38/day"
                        }
                    ],
                    "HEADER_TITLE": "1-Day Internet Pass"
                },
                "ROAMINGBASICPASSLIST": {
                    "PASS": {
                        "PASS_ISPROMOAVAILABLE": "False",
                        "PASS_NAME": "1-Day Basic Internet Pass",
                        "PASS_WITHOUTPROMOPRICE": "",
                        "PASS_DESC": "RM1",
                        "PASS_VISITEDCOUNTRY_SUBSCRIPT": "",
                        "PASS_PRICE_DISPLAY": "RM1",
                        "PASS_VOLUME": "AUTO SUBSCRIPTION",
                        "PASS_VALIDITY": "Valid for 1 Day",
                        "PASS_PROMOPERIOD": "",
                        "PASS_LOCAL_SUBSCRIPT": "",
                        "PASS_QUOTA": "",
                        "PASS_VALIDITY_COUNTRIES": "Valid In 9 ASEAN Countries",
                        "PASS_NEWPROMOPRICE": "",
                        "PASS_ID": "40867",
                        "PASS_VALID": "1 Day",
                        "PASS_PRICE": "RM1/day"
                    },
                    "HEADER_TITLE": "Basic Internet Pass"
                },
                "ROAMINGSOCIALPASSLIST": {
                    "PASS": {
                        "PASS_PROMO_PRODUCTID": "",
                        "PASS_ISPROMOAVAILABLE": "False",
                        "PASS_NAME": "1-Day Social Roam Pass",
                        "PASS_WITHOUTPROMOPRICE": "",
                        "PASS_DESC": "RM10",
                        "PASS_VISITEDCOUNTRY_SUBSCRIPT": "",
                        "PASS_PRICE_DISPLAY": "RM10",
                        "PASS_VOLUME": "AUTO SUBSCRIPTION",
                        "DATA_OPTIONS": [
                            {
                                "PERIOD": "1GB",
                                "label": "Social Media"
                            },
                            {
                                "PERIOD": "50MB",
                                "label": "High Speed Internet"
                            },
                            {
                                "PERIOD": "50MB",
                                "label": "Basic Internet"
                            }
                        ],
                        "PASS_VALIDITY": "Valid for 1 Day",
                        "PASS_PROMOPERIOD": "",
                        "PASS_LOCAL_SUBSCRIPT": "",
                        "PASS_QUOTA": "",
                        "PASS_VALIDITY_COUNTRIES": "Valid in 10 countries",
                        "IS_PURCHASE_ALLOWED": "Y",
                        "PASS_NEWPROMOPRICE": "",
                        "PASS_VALID": "1 Day",
                        "PASS_ID": "40554",
                        "PASS_PRICE": "RM10/day"
                    },
                    "HEADER_TITLE": "1-Day Social Roam Pass"
                },
                "WEEKLYROAMPASSLPASSLIST": {
                    "PASS": {
                        "PASS_PROMO_PRODUCTID": "",
                        "PASS_ISPROMOAVAILABLE": "False",
                        "PASS_NAME": "7-Day 3-IN-1 PASS",
                        "PASS_WITHOUTPROMOPRICE": "",
                        "PASS_DESC": "RM10",
                        "PASS_VISITEDCOUNTRY_SUBSCRIPT": "",
                        "PASS_PRICE_DISPLAY": "RM10",
                        "PASS_VOLUME": "AUTO SUBSCRIPTION",
                        "DATA_OPTIONS": [
                            {
                                "PERIOD": "1GB",
                                "label": "Social Media"
                            },
                            {
                                "PERIOD": "50MB",
                                "label": "High Speed Internet"
                            },
                            {
                                "PERIOD": "50MB",
                                "label": "Basic Internet"
                            }
                        ],
                        "PASS_VALIDITY": "Valid for 1 Day",
                        "PASS_PROMOPERIOD": "",
                        "PASS_LOCAL_SUBSCRIPT": "",
                        "PASS_QUOTA": "",
                        "PASS_VALIDITY_COUNTRIES": "Valid in 10 countries",
                        "IS_PURCHASE_ALLOWED": "Y",
                        "PASS_NEWPROMOPRICE": "",
                        "PASS_VALID": "1 Day",
                        "PASS_ID": "40554",
                        "PASS_PRICE": "RM10/day"
                    },
                    "HEADER_TITLE": "7-DAY 3-IN-1 PASS"
                },
                "DAILYROAMPASSLPASSLIST": {
                    "PASS": {
                        "PASS_PROMO_PRODUCTID": "",
                        "PASS_ISPROMOAVAILABLE": "False",
                        "PASS_NAME": "1-Day CALLS & SMS PASS",
                        "PASS_WITHOUTPROMOPRICE": "",
                        "PASS_DESC": "RM10",
                        "PASS_VISITEDCOUNTRY_SUBSCRIPT": "",
                        "PASS_PRICE_DISPLAY": "RM10",
                        "PASS_VOLUME": "AUTO SUBSCRIPTION",
                        "DATA_OPTIONS": [
                            {
                                "PERIOD": "1GB",
                                "label": "Social Media"
                            },
                            {
                                "PERIOD": "50MB",
                                "label": "High Speed Internet"
                            },
                            {
                                "PERIOD": "50MB",
                                "label": "Basic Internet"
                            }
                        ],
                        "PASS_VALIDITY": "Valid for 1 Day",
                        "PASS_PROMOPERIOD": "",
                        "PASS_LOCAL_SUBSCRIPT": "",
                        "PASS_QUOTA": "",
                        "PASS_VALIDITY_COUNTRIES": "Valid in 10 countries",
                        "IS_PURCHASE_ALLOWED": "Y",
                        "PASS_NEWPROMOPRICE": "",
                        "PASS_VALID": "1 Day",
                        "PASS_ID": "40554",
                        "PASS_PRICE": "RM10/day"
                    },
                    "HEADER_TITLE": "1-DAY CALLS & SMS PASS"
                }
            },
            "statusCode": 200
        }
    }

    planCompare(roaming, country) {
        console.log(JSON.stringify(roaming), " ---- ", JSON.stringify(country), " country name ", country.name)
        var main = [];
        var isObjectTo = [];

        _.each(roaming, function (p: any) {

            if (_.isObject(p) && p.PASS != undefined)
                isObjectTo.push(p);
        });

        var count = 0, counts = 0;
        var checkIsPromo = false;

        _.mapObject(_.values(isObjectTo), function (values, keys) {
            console.log("Parent key ", keys, " - val ", values.HEADER_TITLE);
            count = 0;
            counts = 0;


            _.mapObject(_.values(values.PASS), function (values1, keys1) {

                var stooge = values1;
                // console.log(stooge)  
                console.log("keys1 ", keys1);
                console.log(_.isMatch(stooge, { "PASS_PRICE_DISPLAY": "RM4.99" }));
                console.log(_.isMatch(stooge, { "PASS_ISPROMOAVAILABLE": "True" }));

                checkIsPromo = (_.isMatch(stooge, { "PASS_PRICE_DISPLAY": "RM4.99" }) && _.isMatch(stooge, { "PASS_ISPROMOAVAILABLE": "True" })) ? true : false;

                console.log("checkIsPromo is ", checkIsPromo);
            });

            _.mapObject(_.values(country.Plan), function (val, keyes) {
                console.log("key ", keyes, " - val ", val.display_name, "  ", val);
                var roamingInfo = values.HEADER_TITLE;

                if (roamingInfo.toLowerCase() == (val.display_name).toLowerCase() && (val.isavaliable == 'yes')) {
                    var countryInfo = val.display_name;
                    console.log(roamingInfo.toLowerCase(), "==", countryInfo.toLowerCase())
                    // values.PASS.splice(,);
                    console.log(values.HEADER_TITLE, "==========", val.display_name);
                    main.push(values);
                    //console.log();
                    //console.log(JSON.stringify(values)); 
                }

                // console.log("1 check ->",(val.isavaliable == 'yes' || val.isavaliable == 'no'));
                // console.log("1.1 check ->",(val.isavaliable == 'yes') );
                // console.log("1.2 check ->",(val.isavaliable == 'no'));

                // console.log("2 check ->",roamingInfo.toLowerCase() == ('1-Day Internet Pass').toLowerCase());
                // console.log("2.1 check ->",roamingInfo.toLowerCase() );
                // console.log("2.2 check ->",('1-Day Internet Pass').toLowerCase());

                // console.log("3 check -> ",(country.name == 'Bangladesh' || country.name == 'Cambodia' || country.name == 'Indonesia' || country.name == 'Nepal' || country.name == 'Singapore' || country.name == 'Sri Lanka'))
                // console.log("3.1 check -> ",(country.name == 'Bangladesh'));

                if (roamingInfo.toLowerCase() == ('1-Day Internet Pass').toLowerCase() &&
                    (val.isavaliable == 'yes' || val.isavaliable == 'no') &&
                    (country.name == 'Bangladesh' || country.name == 'Cambodia' || country.name == 'Indonesia' || country.name == 'Nepal' || country.name == 'Singapore' || country.name == 'Sri Lanka') &&
                    count == 0 && checkIsPromo) {
                    ++count;
                    console.log("4.99 promo before checkIsPromo is ", checkIsPromo);
                    if (checkIsPromo) {
                        console.log("4.99 promo checkIsPromo is ", checkIsPromo);

                        var countryInfo = val.display_name;
                        console.log(roamingInfo.toLowerCase(), "==", countryInfo.toLowerCase())
                        // values.PASS.splice(,);
                        console.log(values.HEADER_TITLE, "==========", val.display_name);
                        main.push(values);
                        //console.log();
                        //console.log(JSON.stringify(values)); 
                    }
                }
                if (roamingInfo.toLowerCase() == ('1-Day Internet Pass').toLowerCase() &&
                    (val.isavaliable == 'yes' || val.isavaliable == 'no') &&
                    (country.name == 'Bangladesh' || country.name == 'Cambodia' || country.name == 'Indonesia' || country.name == 'Nepal' || country.name == 'Singapore' || country.name == 'Sri Lanka') &&
                    count == 0 && !checkIsPromo) {
                    ++count;
                    console.log("4.99 promo before checkIsPromo is ", checkIsPromo);
                    if (!checkIsPromo) {
                        console.log("4.99 promo checkIsPromo is ", checkIsPromo);

                        var countryInfo = val.display_name;
                        console.log(roamingInfo.toLowerCase(), "==", countryInfo.toLowerCase())
                        // values.PASS.splice(,);
                        console.log(values.HEADER_TITLE, "==========", val.display_name);
                        main.push(values);
                        //console.log();
                        //console.log(JSON.stringify(values)); 
                    }
                }
                else if (roamingInfo.toLowerCase() == ('1-Day Internet Pass').toLowerCase() &&
                    (country.name != 'Bangladesh' && country.name != 'Cambodia' && country.name != 'Indonesia' && country.name != 'Nepal' && country.name != 'Singapore' && country.name != 'Sri Lanka') &&
                    (val.isavaliable == 'no') && counts == 0) {
                    ++counts;
                    console.log("4.99 not promo");
                    var countryInfo = val.display_name;
                    console.log(roamingInfo.toLowerCase(), "==", countryInfo.toLowerCase())
                    // values.PASS.splice(,);
                    console.log(values.HEADER_TITLE, "==========", val.display_name);
                    main.push(values);
                    //console.log();
                    //console.log(JSON.stringify(values)); 
                }

            });

        });
        // });

        console.log(main);
        return main;

    }

    planCompareForPostpaid(roaming, country) {

    }

    checkVlrId(countryInfo: any, vlrid: any): Promise<any> {

        var idCount = 0;

        console.log("countryInfo ", countryInfo, "vlrid ", vlrid);
        return new Promise((resolve, reject) => {
            // var vlr_id_check_one = false;
            // var vlr_id_check_two = false;
            idCount = 0;
            _.each(countryInfo, function (id: any) {
                ++idCount;

                // vlrid.substring(0, id.length) == id
                console.log("inside countryInfo ", countryInfo, "vlrid ", id);
                console.log("idCount ", idCount, " countryInfo.length ", countryInfo.length);

                // console.log(vlrid.substring(0, id.length) == id);
                console.log(vlrid.substring(0, 3) == id.substring(0, 3));

                if (vlrid.substring(0, 3) == id.substring(0, 3)) {
                    console.log("id ", id);

                    resolve(true);
                    // return true;

                }
                else if (countryInfo.length == idCount) {
                    console.log("nop ", id);
                    resolve(false);
                    // return false;
                }
            });
        });

    }

    SetCheckVlrIdForOtherCountry(countryName, isTrueToVisible) {

        console.log("country infos ", countryName, " isTrueToVisible ", isTrueToVisible);
        console.log("countryName  ", countryName["name"]);

        this.countryBtnInfo = {
            "country_name": countryName["name"],
            "isVlrId": isTrueToVisible
        };
    }

    GetCheckVlrIdForOtherCountry() {

        console.log("GetCheckVlrIdForOtherCountry() -> countryBtnInfo ", JSON.stringify(this.countryBtnInfo));

        return this.countryBtnInfo;
    }

    indexOfCountryPlans(passName): number {
        console.log("indexOfCountryPlans ", (passName.PASS_NAME).toUpperCase(), typeof (passName.PASS_NAME));

        return _.indexOf(["1-DAY INTERNET PASS", "7-DAY 3-IN-1 PASS"], (passName.PASS_NAME).toUpperCase());
    }

    // oleole 
    SetIsOleoeAvailable(isPreOrPost) {
        console.log("SubscriptionService -> SetIsOleoeAvailable() = ", isPreOrPost);
        this.isOleole = isPreOrPost;
    }

    GetIsOleoeAvailable() {
        console.log("SubscriptionService -> GetIsOleoeAvailable() = ", this.isOleole);
        return this.isOleole;
    }

}

