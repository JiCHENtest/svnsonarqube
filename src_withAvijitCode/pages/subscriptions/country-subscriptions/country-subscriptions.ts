import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { SubscriptionConfirmationPage } from '../subscription-confirmation/subscription-confirmation';


// service
import { DashboardService } from '../../../pages/home/homeService';
import { SubscriptionService } from '../subscriptionService';
import { GatewayService } from '../../../global/utilService';
/**
 * Generated class for the CountrySubscriptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-country-subscriptions',
  templateUrl: 'country-subscriptions.html',
})
export class CountrySubscriptionsPage {

  roamingCategoryPlan : any;
  selectedCountryName : any;
  isRoaming : boolean = false;
  userInfo : any;
  indexValue:any;
  isSixCountryPlan : boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public dashboardService : DashboardService,
    private subscriptionService: SubscriptionService,
    public gatewayService: GatewayService,
    private renderer: Renderer2) {

    this.roamingCategoryPlan =  this.navParams.get('plan_lists');

    console.log("roamingCategoryPlan ",this.roamingCategoryPlan);
    // this.roamingCategoryPlan = [
        // {
        //   "PASS": [
        //     {
        //       "PASS_ISPROMOAVAILABLE": "True",
        //       "PASS_NAME": "1-Day Internet Pass",
        //       "PASS_WITHOUTPROMOPRICE": "",
        //       "PASS_DESC": "RM4.99",
        //       "PASS_VISITEDCOUNTRY_SUBSCRIPT": "",
        //       "PASS_PRICE_DISPLAY": "RM4.99",
        //       "PASS_VOLUME": "AUTO SUBSCRIPTION",
        //       "PASS_VALIDITY": "Valid for 1 Day",
        //       "PASS_PROMOPERIOD": "17 AUG 2017 - 16 NOV 2017",
        //       "PASS_LOCAL_SUBSCRIPT": "Selected Countries",
        //       "PASS_QUOTA": "500MB",
        //       "PASS_VALIDITY_COUNTRIES": "Valid in 6 asia countries",
        //       "IS_PURCHASE_ALLOWED": "N",
        //       "PASS_ID": "40755",
        //       "PASS_VALID": "1 Day",
        //       "PASS_PRICE": "RM4.99/day"
        //     },
        //     {
        //       "PASS_ISPROMOAVAILABLE": "False",
        //       "PASS_NAME": "1-Day Internet Pass",
        //       "PASS_WITHOUTPROMOPRICE": "",
        //       "PASS_DESC": "RM38",
        //       "PASS_VISITEDCOUNTRY_SUBSCRIPT": "",
        //       "PASS_PRICE_DISPLAY": "RM38",
        //       "PASS_VOLUME": "AUTO SUBSCRIPTION",
        //       "PASS_VALIDITY": "Valid for 1 Day",
        //       "PASS_PROMOPERIOD": "",
        //       "PASS_LOCAL_SUBSCRIPT": "",
        //       "PASS_QUOTA": "",
        //       "PASS_VALIDITY_COUNTRIES": "Valid in more than 160 countries",
        //       "IS_PURCHASE_ALLOWED": "Y",
        //       "PASS_NEWPROMOPRICE": "",
        //       "PASS_ID": "45687",
        //       "PASS_VALID": "1 Day",
        //       "PASS_PRICE": "RM38/day"
        //     }
        //   ],
        //   "HEADER_TITLE": "1-Day Internet Pass"
        // },
        // "first0002",
        // "0138594037",
        // {
        //   "PASS": [
        //     {
        //       "PASS_ISPROMOAVAILABLE": "False",
        //       "PASS_NAME": "1-Day Basic Internet Pass",
        //       "PASS_WITHOUTPROMOPRICE": "",
        //       "PASS_DESC": "RM1",
        //       "PASS_VISITEDCOUNTRY_SUBSCRIPT": "",
        //       "PASS_PRICE_DISPLAY": "RM1",
        //       "PASS_VOLUME": "AUTO SUBSCRIPTION",
        //       "PASS_VALIDITY": "Valid for 1 Day",
        //       "PASS_PROMOPERIOD": "",
        //       "PASS_LOCAL_SUBSCRIPT": "",
        //       "PASS_QUOTA": "",
        //       "PASS_VALIDITY_COUNTRIES": "Valid In 9 ASEAN Countries",
        //       "PASS_NEWPROMOPRICE": "",
        //       "PASS_ID": "40867",
        //       "PASS_VALID": "1 Day",
        //       "PASS_PRICE": "RM1/day"
        //     }
        //   ],
        //   "HEADER_TITLE": "1-Day Basic Internet Pass"
        // },
        // "20180308140533",
        // {
        //   "PASS": [
        //     {
        //       "PASS_PROMO_PRODUCTID": "",
        //       "PASS_ISPROMOAVAILABLE": "False",
        //       "PASS_NAME": "1-Day Social Roam Pass",
        //       "PASS_WITHOUTPROMOPRICE": "",
        //       "PASS_DESC": "RM10",
        //       "PASS_VISITEDCOUNTRY_SUBSCRIPT": "",
        //       "PASS_PRICE_DISPLAY": "RM10",
        //       "PASS_VOLUME": "AUTO SUBSCRIPTION",
        //       "DATA_OPTIONS": [
        //         {
        //           "PERIOD": "1GB",
        //           "label": "Social Media"
        //         },
        //         {
        //           "PERIOD": "50MB",
        //           "label": "High Speed Internet"
        //         },
        //         {
        //           "PERIOD": "50MB",
        //           "label": "Basic Internet"
        //         }
        //       ],
        //       "PASS_VALIDITY": "Valid for 1 Day",
        //       "PASS_PROMOPERIOD": "",
        //       "PASS_LOCAL_SUBSCRIPT": "",
        //       "PASS_QUOTA": "",
        //       "PASS_VALIDITY_COUNTRIES": "Valid in 10 countries",
        //       "IS_PURCHASE_ALLOWED": "Y",
        //       "PASS_NEWPROMOPRICE": "",
        //       "PASS_VALID": "1 Day",
        //       "PASS_ID": "40554",
        //       "PASS_PRICE": "RM10/day"
        //     }
        //   ],
        //   "HEADER_TITLE": "1-Day Social Roam Pass"
        // },
        // "Success",
        // ""
    // ];

    this.selectedCountryName =  this.navParams.get('country_name');

    console.log("this.selectedCountryName ",this.selectedCountryName)

    var CountryForSix = ["Bangladesh", "Cambodia", "Indonesia", "Nepal","Sri Lanka","Singapore"];
    this.isSixCountryPlan = CountryForSix.indexOf(this.selectedCountryName.name) != -1 ? true : false;
    
    this.isRoaming =  this.navParams.get('isRoaming');

    this.userInfo = this.dashboardService.GetUserInfo();
    
    // if(this.planDetails.PASS_ISPROMOAVAILABLE.toLowerCase() == 'True' &&
    //     this.planDetails.PASS_DESC.toLowerCase() == 'RM4.99' &&
    //     (this.selectedCountryName.name == 'Bangladesh' || 
    //     this.selectedCountryName.name == 'Cambodia' || 
    //     this.selectedCountryName.name == 'Indonesia' || 
    //     this.selectedCountryName.name == 'Nepal' || 
    //     this.selectedCountryName.name == 'Singapore' || 
    //     this.selectedCountryName.name == 'Sri Lanka') ) {


    //   }
  
  }

  // ionViewDidEnter() {
  //   this.gatewayService.resetAllAnalyticData();
  //   this.gatewayService.setAnalyticPageNameProfileSegment("Country subscription", this.renderer);
  // }

  ionViewDidLoad() {
    
  }
  ionViewWillEnter(){
    this.indexValue="";
  }

  ngOnDestroy(){
    this.indexValue="";
  }
  pushBack(){
    this.navCtrl.pop();
  }

  goToDetailsPage (roamingDetails, header_title) {
    if (this.subscriptionService.isCapZone()) {
      this.subscriptionService.presentConfirm();
    } else { 
      this.indexValue=roamingDetails;
    this.navCtrl.parent.parent.push(SubscriptionConfirmationPage,{"details":roamingDetails,"isRoaming":true,"country_name":this.selectedCountryName, "header_title": header_title }); 
    }
  }
}
