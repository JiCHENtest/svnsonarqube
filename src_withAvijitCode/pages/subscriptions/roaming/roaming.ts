import { Component,NgZone, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SubscriptionConfirmationPage } from '../subscription-confirmation/subscription-confirmation';
import { CountrySubscriptionsPage } from '../country-subscriptions/country-subscriptions';

import { SubscriptionService } from "../../subscriptions/subscriptionService";
import { GatewayService } from '../../../global/utilService';
import { ConstantData } from '../../../global/constantService';
import { DashboardService } from '../../../pages/home/homeService';

/**
 * Generated class for the RoamingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-roaming',
  templateUrl: 'roaming.html',
})
export class RoamingPage {

  countries: any;
  loader : any;
  checkStatus : boolean = true;
  roamingCategoryPlan : any;
  selectedCountryName : any = '';
  isOleole : boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public subscriptionService : SubscriptionService, public gatewayService:GatewayService, 
    public dashboardService : DashboardService,public zone:NgZone, private renderer: Renderer2) {
    this.roamingCategoryPlan = '';
    this.initializeItems();
    var giftType: any = this.navParams.data;
    if ( giftType.p1 != undefined)    
      this.isOleole = true
  }

  ionViewDidLoad() {
    this.roamingCategoryPlan = '';
    console.log('ionViewDidLoad RoamingPage ',this.navCtrl.last()," next ",this.navParams.data);
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Roaming subscription", this.renderer);
  }

  initializeItems() {

    var _that = this;

    _that.loader = _that.gatewayService.LoaderService();
    _that.loader.present();
        
    var params = [];
   
    console.log("countryList param ",params);
      
    // return new Promise((resolve, reject) => {
    _that.gatewayService.CallAdapter(ConstantData.adapterUrls.GetCountryListName,"POST",params).then(function(res){
      console.log("********************************************** component success countryList "+JSON.stringify(res));
         
      try {          
        _that.countries = res;
        console.log("contry set ",_that.countries);
        _that.subscriptionService.setCountryList(res);
        _that.loader.dismiss();
        // _that.events.publish('user:created');      
      }
      catch(e) {
        console.log("countryList error ",e);
      }      
    })
    .catch(function(e){
        console.log("******************************************** component failure countryList "+e);
        console.log("error user info countryList ");
        _that.loader.dismiss();
        if(e=="No internet connection")
        return;
        // reject(e);
        // _that.loader.dismiss();
    });
    // });
  }

  initializeItemsCountry() {
    this.countries = this.subscriptionService.getCountryList();
  }


  getItems(ev: any) {

    this.zone.run(() => {
    this.selectedCountryName = '';
    this.checkStatus = true;
    // Reset items back to all of the items
    this.initializeItemsCountry();

    // set val to the value of the searchbar
    let val = ev.target.value;
    console.log("val ",val);
    
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.countries = this.countries.filter((item) => {
        return ((item.name.toLowerCase()).indexOf(val.toLowerCase()) > -1);
      })
    }
  });
  }

  goToCountryDetailsPage(countryPlan) {
    this.checkStatus = false;
    // console.log("Roaming ",JSON.stringify(countryPlan));

    // selected country plan
    this.selectedCountryName = countryPlan;

    
    // to show all roam plan
    // this.roamingCategoryPlan = this.subscriptionService.getSubscriptionPostOrPrePlan();

    // console.log("this.roamingCategoryPlan ",JSON.stringify(this.roamingCategoryPlan));
    // userInfo.ListOfServices.Services.ServiceType == "Postpaid" || userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid"
    // console.log("yeswe   ",JSON.stringify(this.subscriptionService.getSubscriptionPostpaid()));
    // this.roamingCategoryPlan = this.subscriptionService.planCompare(this.subscriptionService.getSubscriptionPostOrPrePlan(), this.selectedCountryName);
    // console.log("yes   ",JSON.stringify(this.subscriptionService.getSubscriptionPostpaid()));
    var userInfo = this.dashboardService.GetUserInfo();

    // compare country plan name with response api pass name
    this.roamingCategoryPlan = (userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Prepaid")  ? this.subscriptionService.planCompare(this.subscriptionService.getSubscriptionPostOrPrePlan(), this.selectedCountryName) : this.subscriptionService.planCompare(this.subscriptionService.getSubscriptionPostpaid(), this.selectedCountryName);
    
    // show cards for roaming
    this.navCtrl.push(CountrySubscriptionsPage,{"plan_lists":this.roamingCategoryPlan,"isRoaming":true,"country_name":this.selectedCountryName});
  }

  // add later
  goToDetailsPage(roamingDetails) { 
    if (this.subscriptionService.isCapZone()) {
      this.subscriptionService.presentConfirm();
    } else {   
      this.navCtrl.parent.parent.push(SubscriptionConfirmationPage,{"details":roamingDetails,"isRoaming":true,"country_name":this.selectedCountryName});  
    }
  }

  
}
