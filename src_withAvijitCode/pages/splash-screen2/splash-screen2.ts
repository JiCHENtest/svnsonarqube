import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';
/**
 * Generated class for the SplashScreen2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-splash-screen2',
  templateUrl: 'splash-screen2.html',
})
export class SplashScreen2Page {

  constructor(public navCtrl: NavController, public navParams: NavParams, public inAppBrowser: InAppBrowser) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SplashScreen2Page');
  }

  startApp(){
    const browser = this.inAppBrowser.create('https://beta.celcom.com.my/', '_blank', 'location=no');
    browser.show();
  }
}
