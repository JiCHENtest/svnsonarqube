import { Component, ElementRef, ViewChild, NgZone, Renderer2 } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { HomePage } from '../../home/home';
import { OnBoardingGuidePage } from '../../on-boarding-guide/on-boarding-guide';
//import { TermsConditionsPage } from '../../terms-conditions/terms-conditions';
import { MobileConnectPage } from '../mobile-connect/mobile-connect';
import { NetworkLoginPage } from '../network-login/network-login';
import { GatewayService } from '../../../global/utilService';
import { ConstantData } from '../../../global/constantService';
import { LocalStorage } from '../../../global/localStorage';
import { AlertService } from '../../../global/alert-service';
import { ReloadService } from '../../../global/reloadServices';
import { DashboardService } from '../../home/homeService';
import { ToastController } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';

import { MobileConnectService } from '../mobile-connect/mobileConnectService';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
/**
 * Generated class for the ConnectTacPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var SMS: any;
@Component({
  selector: 'page-connect-tac',
  templateUrl: 'connect-tac.html',
})
export class ConnectTacPage {
  mobileNumber: any;
  isFirstTimeUser: any;
  isenabled: boolean = false;
  otpInput: any;
  otp: any;
  otpInput1: any;
  otpInput2: any;
  otpInput3: any;
  otpInput4: any;
  loader: any;

  incorrectOTPCount = 0;
  resendCount = 0;
  firstResendTime = new Date();
  counter: number = 2 * 60;
  otpStartTimer: any;
  alertResendOTP: any;
  myTimer: Observable<void>;
  @ViewChild('focus2')
  focus2: ElementRef;

  @ViewChild('focus1')
  focus1: ElementRef;

  @ViewChild('focus3')
  focus3: ElementRef;

  @ViewChild('focus3')
  focus4: ElementRef;

  @ViewChild('connectSubmit')
  connectSubmit: ElementRef;

  constructor(public navCtrl: NavController, public reloadService: ReloadService, public navParams: NavParams,
    public loadingCtrl: LoadingController, public gatewayService: GatewayService, public platform: Platform,
    public zone: NgZone, public localStorage: LocalStorage, public dashboardService: DashboardService,
    private alertCtrlInstance: AlertService, public mConnectService: MobileConnectService,
    public alertCtrl: AlertController, private toastCtrl: ToastController,
    private keyboard: Keyboard, private renderer: Renderer2) {
    this.isFirstTimeUser = this.localStorage.get("IsFirstTimeUser", "true");
    this.mobileNumber = this.gatewayService.GetMobileNumber();//this.localStorage.get("MobileNum","");
    //alert("Mobile number entered is::"+ this.mobileNumber ); 
    console.log('constructor called');
    // var sms_test = "The one time pin you requested is 123456. It will expire in 10 minutes. Call 1111 if you did not request any pin number from the Celcom Mobile application.";

    //   document.addEventListener('resume', () => {
    //     if(this.resendCount>4){
    //     var start_time = this.localStorage.get("otpStartTimer", '');
    //     var current_time:any = new Date().getTime();

    //     console.log(start_time);
    //     console.log(current_time);
    //     console.log(current_time-start_time);

    //     if(start_time == '' || start_time==undefined)
    //     {

    //     }
    //     else{
    //       if(((current_time - start_time) / 1000) > (10*60)){
    //         console.log('time up');

    //         this.alertResendOTP.dismiss();
    //         this.localStorage.set("otpStartTimer", '');
    //         this.resendCount = 0;
    //         //this.counter = 2*60;

    //       }
    //     }
    //   }
    // });
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageName("OTP", this.renderer);

    this.platform.ready().then((readySource) => {

      if (SMS) SMS.startWatch(() => {
        console.log('watching started');
      }, Error => {
        console.log('failed to start watching');
      });

      document.addEventListener('onSMSArrive', (e: any) => {
        var sms = e.data.body;
        //var OTP=sms.substring(33,40);

        var OTP = sms.split("(OTP) is ");
        console.log("OTP is ::" + OTP[1]);
        this.zone.run(() => {
          this.otpInput = OTP[1];
          this.otpInput1 = this.otpInput.substring(0, 1);
          console.log("i/p1 :" + this.otpInput1);
          this.otpInput2 = this.otpInput.substring(1, 2);
          console.log("i/p2 :" + this.otpInput2);
          this.otpInput3 = this.otpInput.substring(2, 3);
          console.log("i/p3 :" + this.otpInput3);
          this.otpInput4 = this.otpInput.substring(3, 4);
          console.log("i/p4 :" + this.otpInput4);

          if (this.otpInput1 != "" && this.otpInput2 != "" && this.otpInput3 != "" && this.otpInput4 != "") {
            this.isenabled = true;
          }
        });


      });

    });
  }

  goBack() {

    this.navCtrl.setRoot(MobileConnectPage);
  }

  // enableHome() {

  // if (this.otpInput == "1234") {
  //   this.isenabled = true;
  //  }
  //  else {
  //   this.isenabled = false;
  //  }

  //  }

  //incase of failure
  // setupMobileConnectAccount() {
  //   this.navCtrl.setRoot(MobileConnectAccountPage);

  // }







  gotoNetworkLogin() {
    var _that = this;
    if (ConstantData.isLoginBypassed == false) {

      if (this.incorrectOTPCount < 5) {

        _that.loader = _that.gatewayService.LoaderService();
        _that.loader.present().then(() => {
          //code for calling register()from adapter
          _that.validateMobileConnectUser().then(function (res) {
            //console.log(res);

            if (res["code"] == 0) {
              _that.gatewayService.ReloadDashboard().then((res) => {
                _that.loader.dismiss();
                var userInfo = _that.dashboardService.GetUserInfo();
                _that.gatewayService.SetXPAX(userInfo.ListOfServices.Services.ServiceType);
                //_that.reloadService.setingLoginNumberServiceType(userInfo.ListOfServices.Services.ServiceType);

                _that.gatewayService.notifyObservers("Menu");
                _that.localStorage.set("IsUserLoggedIn", "true");
                if (_that.isFirstTimeUser == "true") {

                  _that.navCtrl.setRoot(NetworkLoginPage);
                } else {

                  _that.navCtrl.setRoot(HomePage);

                }
              }).catch(err => {
                _that.loader.dismiss();
                console.log("component  failure" + err);
                if (err == "No internet connection")
                  return;
                _that.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
                //alert("Fetch Dashboard Info "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid

              });

            }
            else {
              _that.loader.dismiss();
              _that.incorrectOTPCount++;
              if (res["code"] == 77) {
                //Check for Invalid OTP
                _that.alertCtrlInstance.showAlert("Uh Oh. That's not a valid OTP", "Did you enter the wrong number? Please try again later.", "OK");
              } else if (res["code"] == 78) {
                _that.alertCtrlInstance.showAlert("Uh Oh. OTP has expired", "Please request for new OTP.", "OK");
              }
              //var data = { "title": "Uh oh, invalid OTP", "subtitle": "", "message": "Did you enter the wrong number?" };

              //_that.gatewayService.presentAlert(data);
            }
          }).catch(function (e) {
            console.log("component  failure" + e);
            console.log("Error" + e);
            _that.loader.dismiss();
            if (e == "No internet connection")
              return;
            _that.alertCtrlInstance.showAlert("Uh Oh. Can't seem to receive OTP", "Please try again later", "OK");

          });
        });

      }
      else {




        let alert = this.alertCtrl.create({
          title: "Uh Oh. That's Too Many Login Attempts",
          subTitle: "You've entered the incorrect PIN too many times. Please exit and try again later.",
          buttons: [
            {
              text: "Ok",
              role: "Ok",
              cssClass: 'error-button-float-right submit-button',
              handler: () => {
                console.log('Call to Action Clicked');
                _that.navCtrl.setRoot(MobileConnectPage);
              }
            }],
          cssClass: 'success-message error-message',
          enableBackdropDismiss: false

        });
        alert.present();
      }

    } else {

      _that.loader = _that.gatewayService.LoaderService();
      _that.loader.present().then(() => {

        _that.gatewayService.ReloadDashboard().then((res) => {
          _that.loader.dismiss();
          var userInfo = _that.dashboardService.GetUserInfo();
          _that.gatewayService.SetXPAX(userInfo.ListOfServices.Services.ServiceType);
          _that.gatewayService.notifyObservers("Menu");
          _that.localStorage.set("IsUserLoggedIn", "true");
          if (_that.isFirstTimeUser == "true") {

            _that.navCtrl.setRoot(NetworkLoginPage);
          } else {

            _that.navCtrl.setRoot(HomePage);

          }
        }).catch(err => {
          _that.loader.dismiss();
          if (err == "No internet connection")
            return;
          _that.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
          //alert("Fetch Dashboard Info "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
        });
      });

    }
  }
  resendOTP() {
    var _that = this;
    // Do your operations
    //var currentResendDate   = new Date();
    //var seconds = (currentResendDate.getTime() - this.firstResendTime.getTime()) / 1000;
    console.log('Resend count ' + this.resendCount);
    //console.log(seconds);
    if (this.resendCount > 4) {

      //show max resend attemp msg
      this.alertResendOTP = this.alertCtrl.create({
        title: 'Uh oh',
        subTitle: 'Resend OTP requests limited to 5 attempts. Please try again in 10 minutes.',
        buttons: [
          {
            text: "Ok",
            role: "Ok",
            cssClass: 'error-button-float-right submit-button',
            handler: () => {
              console.log('Call to Action Clicked');

            }
          }],
        cssClass: 'success-message error-message',
        enableBackdropDismiss: true

      });
      this.alertResendOTP.present();
      this.otpStartTimer = this.localStorage.set("otpStartTimer", new Date().getTime());
      setTimeout(
        () => {
          this.alertResendOTP.dismiss();
          this.resendCount = 0;
          this.localStorage.set("otpStartTimer", '');
        }

        , 600000);
      // this.myTimer = Observable.timer(0, 1000)
      //   .take(this.counter)
      //   .map(() => {
      //     console.log(this.counter);
      //     if (this.counter == 1) {
      //       this.alertResendOTP.dismiss();
      //       this.resendCount = 0;
      //       this.counter = 2*60;
      //     } else {
      //       --this.counter;
      //     }
      //   }
      //   );
      // this.myTimer.subscribe();
      //_that.alertCtrlInstance.showAlert("", "", ["Try again", "Cancel"]);


    } else {
      this.sendOTP();
    }
  }
  sendOTP() {
    //clear old otp
    this.otpInput1 = "";
    this.otpInput2 = "";
    this.otpInput3 = "";
    this.otpInput4 = "";


    let toast = this.toastCtrl.create({
      message: 'OTP successfully sent',
      duration: 3000,
      cssClass: 'toast',
      position: 'top',

    });
    let Errortoast = this.toastCtrl.create({
      message: 'Unable to send OTP. Please retry!',
      duration: 3000,
      cssClass: 'toast',
      position: 'top'
    });
    var _that = this;
    this.resendCount++;
    //this.firstResendTime = new Date();
    //console.log('First date ' + this.firstResendTime);

    var loader = _that.gatewayService.LoaderService();
    loader.present();
    //code for calling register()from adapter
    _that.registerMobileConnectUser().then((res) => {
      //alert("register function response"+JSON.stringify(res));
      _that.mConnectService.SetMConnectInfo(res);
      loader.dismiss();
      toast.present();
    }).catch(function (e) {
      console.log("component  failure" + e);
      loader.dismiss();
      if (e == "No internet connection")
        return;
      Errortoast.present();
      //alert("Error"+e);

    });
  }
  validateMobileConnectUser() {
    //var _that = this;
    // this.state = this.navParams.get('data'); 
    var mConnectData = this.mConnectService.GetMConnectInfo();
    var state = mConnectData["state"];
    var otp = this.otpInput1.toString() + this.otpInput2.toString() + this.otpInput3.toString() + this.otpInput4.toString();
    console.log(otp);
    console.log(state);
    var params_validate = { "state": state, "OTP": otp };

    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.validate, "POST", params_validate).then((res_validate) => {
        console.log("validate user response : " + JSON.stringify(res_validate));
        resolve(res_validate);

        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          console.log("******************************************** component failure validateMobileConnectUser" + e);
          //alert("error in validating MobileConnectUser");   
          reject(e);
        });

    });


  }
  registerMobileConnectUser() {
    var _that = this;

    var msisdn = this.mobileNumber;
    var params_register = { "MSISDN": msisdn };

    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.register, "POST", params_register).then((res_register) => {
        console.log("register user response : " + JSON.stringify(res_register));
        resolve(res_register);

        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          console.log("******************************************** component failure registerMobileConnectUser" + e);
          _that.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
          //alert("error register MobileConnectUser");   
          reject(e);
        });

    });


  }

  handlePin(e, elementIndex, nextElement, prevElement) {
    // debugger;
    // let _thisRef = this;
    // let returnVal = this.gatewayService.keyUpCheckerNumberOnlys(e,this.keyboard);      
    // this.zone.run(()=>{
    //   _thisRef[varname] = returnVal;
    // })



    if (e.which === 13) {
      //in case of enter button
      this.keyboard.close();
    } else if (e.which === 8) {
      //in case of delete button need to move backward
      if (elementIndex != 1) {
        prevElement._native.nativeElement.focus();
        prevElement.value = this.gatewayService.keyUpCheckOtpOnlys(prevElement, this.keyboard);
      }
    } else {
      //for all other buttons need to move forwards
      if (elementIndex != 4) {
        nextElement._native.nativeElement.focus();
      }
    }
    if (e.currentTarget.children[0])
      e.currentTarget.children[0].value = this.gatewayService.keyUpCheckOtpOnlys(e, this.keyboard);
    if (this.otpInput1 == "" || this.otpInput1 == undefined || this.otpInput2 == "" || this.otpInput2 == undefined || this.otpInput3 == "" || this.otpInput3 == undefined || this.otpInput4 == "" || this.otpInput4 == undefined) {
      this.isenabled = false;
    } else {
      this.isenabled = true;
    }


  }



}







