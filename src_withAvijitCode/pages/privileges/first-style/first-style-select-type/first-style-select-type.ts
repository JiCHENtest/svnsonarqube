import { Component, NgZone, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { FirstStyleOneoffPage } from "../first-style-one-off/first-style-one-off";
import { GatewayService } from '../../../../global/utilService';
import { ConstantData } from '../../../../global/constantService';
import { ReloadService } from '../../../../global/reloadServices';
import { PrivilegesPage } from '../../privileges';
import { PrivRewardsPage } from '../priv-rewards/priv-rewards';
import { AlertService } from '../../../../global/alert-service';
/**
 * Generated class for the first-style-select-type-page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-first-style-select-type',
  templateUrl: 'first-style-select-type.html',
})
export class FirstStyleSelectTypePage {
  loader: any; // loader
  priv_type: any;
  indexValue = 0;
  privilegeCards = [
    {
      "type": "One-Off Privileges",
      "icon": "present-icon",
      "card_desc":"Enjoy this FREE One-Off Privilege when you subscribe to a 12-month contract."
    },
    {
      "type": "Monthly Privileges",
      "icon": "calendar-icon",
      "card_desc":"Enjoy monthly rewards from our merchant partners throughout your 12-month contract."
    }

  ];
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public gatewayService: GatewayService,
    public reloadService: ReloadService,
    public zone:NgZone,
    private alertCtrlInstance: AlertService,
    public alertCtrl: AlertController,
    private renderer: Renderer2
  ) {
    
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("First style select type privilege", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad first-style-select-type-page');
  }

  openOneOffPage() {
    this.navCtrl.push(FirstStyleOneoffPage);
  }
  addCss(index) {
    this.zone.run(() => {
      console.log("index"+index);
      this.indexValue = index;
      console.log("index"+this.indexValue);
    });
    

    console.log('/////' + index);
    this.priv_type = "";
    this.priv_type = this.privilegeCards[index].type;
    
  }

  goToOptIn(){
    var _that = this;
    _that.loader = _that.gatewayService.LoaderService();
    _that.loader.present().then(() => {

      //transfer credit
      _that.optInServiceCall().then(function (res) {
        console.log(res);
        

        if(res["isSuccessful"]){
          if(res["OPTINENABLERESPONSSE"].ResponseMessage =='Success'){
            console.log('Isuccess');
            //_that.alertCtrlInstance.showAlert("Success", "You have successfully opt-in", "OK");
            let alert = _that.alertCtrl.create({
              title: "Success",
              subTitle: "You have successfully opt-in",
              buttons: [
              {
                text:"Ok",
                role:"Ok",
                cssClass:"error-button-float-right submit-button",
                handler: () => {
                  console.log('Call to Action Clicked');
                  //_that.navCtrl.push(PrivRewardsPage,0);
                }
              }],
              cssClass:'success-message error-message',
              enableBackdropDismiss: false
            
            });
            alert.present();

            _that.queryVoucherServiceCall().then(function (res) {
              console.log(res);
              
              
              if(res["isSuccessful"]){
                if(res["QueryVouchersResponse"].ResponseMessage =='Success'){
                  console.log('If 1');
                  console.log(res["QueryVouchersResponse"]);
                  console.log(res["QueryVouchersResponse"].OPTIN);
                  //res["QueryVouchersResponse"].OPTIN = "NO";
                  if(res["QueryVouchersResponse"].OPTIN == "YES" && (res["QueryVouchersResponse"].PrivilegeType == 'MONTHLY' || res["QueryVouchersResponse"].PrivilegeType == 'ONETIME'))
                  {
                    //store privilege data
                    _that.gatewayService.SetPrivilegeData(res);
                    console.log('If 2');
                    //go to offers page
                    _that.navCtrl.push(PrivRewardsPage,0);
      
                  }else{
                    console.log('else');
                    //go to opt-in page i.e. here only
                    _that.navCtrl.push(FirstStyleSelectTypePage);
                  }
                }
                else{
                  _that.alertCtrlInstance.showAlert("Uh Oh.", "Plan is not eligible.", "OK");
                }
                _that.loader.dismiss();
              }
              else{
                _that.loader.dismiss();
                _that.alertCtrlInstance.showAlert("Uh Oh.", "Plan is not eligible.", "OK");
              }
            }).catch(function (e) {
              console.log("component  failure" + e);
              console.log("Error" + e);
              _that.loader.dismiss();
              
            });
          }
          else{
            _that.alertCtrlInstance.showAlert("Uh Oh.", res["OPTINENABLERESPONSSE"].ErrorDesc, "OK");
            _that.loader.dismiss();
          }
          
        }
        else{
          _that.loader.dismiss();
          _that.alertCtrlInstance.showAlert("Uh Oh.", "Failed", "OK");
        }
      }).catch(function (e) {
        console.log("component  failure" + e);
        console.log("Error" + e);
        _that.loader.dismiss();
        _that.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
      });
    });
  }

  optInServiceCall() {
    var msisdn = this.gatewayService.GetMobileNumber();
    var priv_type = "";
    if(this.priv_type == "One Off Privileges"){
      priv_type = "ONETIME";
    }
    else{
      priv_type = "MONTHLY";
    }
    var params = { "msisdn": msisdn, "priv_type": priv_type };
    console.log(params);
    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.EnableOptIn, "POST", params).then((res) => {
        console.log("enable opt in response : " + JSON.stringify(res));
        resolve(res);

        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          console.log("******************************************** component failure enable opt in response" + e);
          //alert("error in validating MobileConnectUser");   
          reject(e);
        });

    });

  }

  queryVoucherServiceCall() {
    var msisdn = this.gatewayService.GetMobileNumber();
    var params = { "msisdn": msisdn };
    console.log(params);
    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.QueryVoucher, "POST", params).then((res) => {
        console.log("query voucher response : " + JSON.stringify(res));
        resolve(res);

        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          console.log("******************************************** component failure query voucher response" + e);
          //alert("error in validating MobileConnectUser");   
          reject(e);
        });

    });

  }
  
}
