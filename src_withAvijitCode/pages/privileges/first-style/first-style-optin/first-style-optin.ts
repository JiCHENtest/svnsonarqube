import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {FirstStyleSelectTypePage} from "../first-style-select-type/first-style-select-type";
import { GatewayService } from '../../../../global/utilService';

/**
 * Generated class for the FirstSytleOptinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-first-style-optin',
  templateUrl: 'first-style-optin.html',
})
export class FirstSytleOptinPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private renderer: Renderer2, public gatewayService: GatewayService) {
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("First style option privilege", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FirstSytleOptinPage');
  }

  goToPrivilegeSelectTypePage() {
    this.navCtrl.push(FirstStyleSelectTypePage);
  }

}
