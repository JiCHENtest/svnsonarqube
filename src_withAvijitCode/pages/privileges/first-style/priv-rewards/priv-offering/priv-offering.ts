import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, App, Events } from 'ionic-angular';
//import { ProductDetailsDesignGuidePage } from "../../../product-details-design-guide/product-details-design-guide";
import { GatewayService } from '../../../../../global/utilService';
import { ProductDetailsPage } from "../../product-details/product-details";
import { isArray } from 'ionic-angular/util/util';
/**
 * Generated class for the OfferingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-priv-offering',
  templateUrl: 'priv-offering.html',
})
export class PrivOfferingPage {
  rewards:any;
  offrings:any;
  redeemed:any;
  available_offers:any;
  available_offerings_rows:any;
  available_offerings:any;
  numbers:any;
  images:any;
  isPrepaid:any;
  constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams, 
    public appCtrl: App, public gatewayService: GatewayService, private renderer: Renderer2) {
    this.rewards = this.gatewayService.GetPrivilegeData();
    
     this.images = {
       "218" : "tealive.jpg",
       "219" : "11street.jpg",
       "221" : "grab.jpg",
       "204" : "playstore.jpg",
       "209" : "apple.jpg",
       "198" : "video_walla.png",
       "220" : "gsc.jpg",
       "223" : "flight.jpg",
       "222" : "flight.jpg"
       };
    
     
    
    console.log(this.rewards);
    //var total_offers = this.rewards.QueryVouchersResponse.OFFERS.OFFER;
    var total_offers = [];
    var arr = isArray(this.rewards.QueryVouchersResponse.OFFERS.OFFER);
    if(!arr){
      total_offers.push(this.rewards.QueryVouchersResponse.OFFERS.OFFER);
    }
    else{
      total_offers = this.rewards.QueryVouchersResponse.OFFERS.OFFER;
    }
    this.available_offers = total_offers.filter(function( obj ) {
      return obj.OfferStatus == 'AVAILABLE';
  });

  console.log(this.available_offers);

  this.available_offerings_rows = Math.ceil(parseInt(this.available_offers.length)/2);
  console.log('available offers rows '+ this.available_offerings_rows);
  
  let res = [];
    for (let i = 0; i < this.available_offerings_rows; i++) {
        res.push(i);
      }

      this.available_offerings = res;
  
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Offering privilege", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrivOfferingPage');
  }
  openDetails(id) {
    console.log(id);
    var offer_id = id;
    this.events.publish("goToOfferDetail",{"offer_id":offer_id, "type":"offer"});
    //this.navCtrl.push(ProductDetailsPage,{"offer_id":offer_id, "type":"offer"});
    
  }
  ionViewWillEnter(){
    console.log('ionViewWillEnter PrivOfferingPage');
  }
}
