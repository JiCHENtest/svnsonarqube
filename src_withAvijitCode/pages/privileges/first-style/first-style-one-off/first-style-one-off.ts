import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';
import { GatewayService } from '../../../../global/utilService';

/**
 * Generated class for the FirstStyleOneoffPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-first-style-one-off',
  templateUrl: 'first-style-one-off.html',
})
export class FirstStyleOneoffPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,
    private renderer: Renderer2, public gatewayService: GatewayService) {
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("First style one off privilege", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FirstStyleOneoffPage');
  }


  dismiss() {
    this.viewCtrl.dismiss();

  }

}
