import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';

import { DetailedUsageService } from '../../detailed-usage/detailedUsageService';
import { DashboardService } from '../../home/homeService';
import { GatewayService } from '../../../global/utilService';
import { ConstantData } from '../../../global/constantService';
import _ from 'underscore';

/**
 * Generated class for the DetailedUsageRoamingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detailed-usage-roaming',
  templateUrl: 'detailed-usage-roaming.html',
})
export class DetailedUsageRoamingPage {  
  public detailedUsageRoaming:any={};
  bundleUsagePercent: number = 90;
  detailedInternetUsage:any;
  cardArray:any = [];
  cardMapping:any;
  internetPlans:any;
  userInfo:any;
  isPrepaid:Boolean;
  PPUPrepaid:any;
  loader:any;
  roamingCountry:any = '';
  list;
  str;
  SMSCall:boolean = false;
  cards:boolean = false;
  usageSince:any

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public detailedUsageService: DetailedUsageService, public gatewayService:GatewayService, 
    public dashboardService: DashboardService,public events:Events, private renderer: Renderer2) {
    let planName = this.gatewayService.getPlanName();
    console.log("**************** plan name****************"+planName);
    this.cardMapping = ConstantData.dialMapping.postpaid[planName];
    console.log("***************** Card mapping ****************"+JSON.stringify(this.cardMapping));
    //this.GBSharePlan = this.dashboardService.GetUnits();
    this.usageSince = this.gatewayService.getUsageSince();
    this.userInfo = this.dashboardService.GetUserInfo();
    this.isPrepaid = this.gatewayService.isPrePaid();
    try{
      this.str = this.gatewayService.GetVLR();
      if(this.str!="" || this.str!=null || this.str!=undefined) {
        this.initializeItems();
      }      
    }catch(e){
      console.log("Error "+JSON.stringify(e));
      if(e=="No internet connection")
      return;
      this.generateRoamingInternetCards();
    }
    
    // this.cardArray = [
    //   {
    //     "title":"Internet Pack 1",
    //     "subtitle":"3 days plan",
    //     "cardType":"Card CTA #1"
    //   },
    //   {
    //     "title":"Internet Pack 2",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Toggle/CTA #1"
    //   },
    //   {
    //     "title":"Internet Pack 3",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Usage with Limit #1"
    //   },
    //   {
    //     "title":"Internet Pack 4",
    //     "subtitle":"3 days plan",
    //     "cardType":"Card PPU #1"
    //   },
    //   {
    //     "title":"Internet Pack 5",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Description+ Toggle #1"
    //   },
    //   {
    //     "title":"Internet Pack 6",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Description #1"
    //   },
    //   {
    //     "title":"Internet Pack 7",
    //     "subtitle":"3 days plan",
    //     "cardType":"Card Usage with Limit (Kad Ceria) #1"
    //   },
    //   {
    //     "title":"Internet Pack 8",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Bundle (with Toggle/CTA) #1"
    //   },
    //   {
    //     "title":"Internet Pack 9",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Info (Roaming) #1"
    //   },
    //   {
    //     "title":"Internet Pack 10",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Info (Plan Activation) #1"
    //   },
    //   {
    //     "title":"Internet Pack 11",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Group Sharing Data #1"
    //   }
    // ]
  }


  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Roaming detailed usage", this.renderer);
  }

  initializeItems(){    
        var _that = this;    
        _that.loader = _that.gatewayService.LoaderService();
        _that.loader.present();            
        var params = [];       
        console.log("countryList param ",params);
          
        // return new Promise((resolve, reject) => {
        _that.gatewayService.CallAdapter(ConstantData.adapterUrls.GetCountryListName,"POST",params).then(function(res){
          console.log("********************************************** component success countryList "+JSON.stringify(res));
          try {          
            _that.list = res;
            _that.list = _.find(_that.list, function (product) {
              // underscore walks through each product and returns
              // the first one found.              
              _that.str = _that.str.substr(0,4);
              var ids = product["Vlrids"][0]["id"];
              return ids.indexOf(_that.str) !== -1
           });           
          //   var out = _.filter(_that.list, function(item) {
          //     return _.contains(item.Vlrids.id, '35567');
          //  });
           //console.log("contry set ",_that.countries);
           // _that.subscriptionService.setCountryList(res);
            _that.loader.dismiss();
            _that.roamingCountry = "Roaming";
            _that.generateRoamingInternetCards();
            _that.generateRoamingCard();
            // _that.events.publish('user:created');      
          }
          catch(e) {
            console.log("countryList error ",e);
            _that.loader.dismiss();
            if(e=="No internet connection")
            return;
          }      
        })
        .catch(function(e){
            console.log("******************************************** component failure countryList "+e);
            //alert("error user info countryList ");
            _that.loader.dismiss();
            if(e=="No internet connection")
            return;
            // reject(e);
            // _that.loader.dismiss();
        });
        // });
      }

  generateRoamingInternetCards(){
    if(this.isPrepaid){
      //this.generateRoamingInternetPrepaidCards();
      this.generateRoamingInternetPostpaidCards();
      this.generateRoamingPPUPrepaidCards();      
    }else{
      this.generateRoamingInternetPostpaidCards(); 
      this.generateRoamingPPUPostpaidCards();           
    }    
  }

  generateRoamingCard(){
    var internetData = {
      "country":this.list.name,
      "operator":this.list.Mobile_Display,
      "class":"roaming-icon icon",     
      "cardType":"Card Info (Roaming) #1"
    }
    this.cardArray.push(internetData);    
  }

  formatMBValue(val){    
    var str = val.substring(0, val.length-2);
    return str;
  }

  formatValue(val,val1){    
    var str = val.substring(0, val.length-2);
    if(str.indexOf('0.0')>-1){
      str = val1.substring(0, val1.length-2);
    }
    return str;
  }

  formatUnit(val,val1){
    var unit = 'GB';    
    var str = val.substring(0, val.length-2);
    if(str.indexOf('0.0')>-1){
      unit = 'MB';
    }
    return unit;
  }

  formatDate(val){
    if(val==""|| val==null || val==undefined)
    return;
    var dt = val.substring(6, 8)+'/'+val.substring(4, 6)+'/'+val.substring(0, 4);
    return dt;
  }



  generateRoamingInternetPostpaidCards(){   
    if(!this.isPrepaid){
      this.internetPlans = this.dashboardService.GetRoamStatus();
    }else{
      this.internetPlans = this.dashboardService.GetRoamStatusPre();
    }   
    console.log("Roaming Plans "+JSON.stringify(this.internetPlans));
    //alert("this.internetPlans"+this.internetPlans);
    //alert("this.roamingCountry "+this.roamingCountry);
    if(this.internetPlans==""|| this.internetPlans==null || this.internetPlans==undefined){
      //alert("no plans");
      var NoInternet = {
        "title":"No Roaming Plan",
        "subtitle":"You are not subscribed to any Roaming plan",
        "date":"",
        "action":"Subscribe to Roaming Pass Now",
        "class":"roaming-icon icon",
        "cardType":"Card CTA #1"
      }    
      this.cardArray.push(NoInternet);
      return;
    }    
    var data = [];
    //var nos = this.internetPlans["PASS"];
    if(this.internetPlans["PASS"]){
    if(this.internetPlans["PASS"].length>1)
    data = this.internetPlans["PASS"];
    else
    data.push(this.internetPlans["PASS"]);
    }
    if(this.internetPlans["BASICINTERNETPASS"]){
    data.push(this.internetPlans["BASICINTERNETPASS"]);
    }
    try{
    for(var obj in data){
      //if(data[obj]["PASS_ISDATAUSED"] == true){     
      var romTitle = data[obj]["PASS_NAME"];
      //alert("romTitle"+romTitle);
      if(romTitle=='7 DAY 3 IN 1 PASS'){

        var listObj = [];
        //alert("I am in 7 day pass");
        var romUsedVal = this.formatMBValue(data[obj]["USAGE"]);
        var romTotal = this.formatMBValue(data[obj]["QUOTA"]);
        var romDate = this.formatDate(data[obj]["PASS_VALIDUNTILL"]);
        var romBalance = romTotal - romUsedVal;
        var internetData = {
          "title":'Roaming Internet',
          "subtitle":"",
          "dataUsed":romUsedVal,
          "dataTotal":romTotal,
          "dataBalance":romBalance,
          "date":romDate,
          "unit":'GB',
          "autoRenew":0,
          "class":"roaming-icon icon",
          "cardType":"Card Usage with Limit #1"
        }
        listObj.push(internetData);
        this.cards = true;
        //if(this.SMSCall==false){
          var CallsPostpaidUnlimitedData = {
            "title":"Unlimited Calls",
            "subtitle":"Unlimited Calls in visiting country and Malaysia",
            "date":this.formatDate(data[obj]["PASS_VALIDUNTILL"]),
            "icon":"call-icon icon",
            "cardType":"Call Card Description #1"
          }
          listObj.push(CallsPostpaidUnlimitedData);
          var SMSPostpaidUnlimitedData = {
            "title":"Unlimited SMS",
            "subtitle":"Unlimited SMS in visiting country and Malaysia",
            "date":this.formatDate(data[obj]["PASS_VALIDUNTILL"]),
            "icon":"sms-icon icon",
            "cardType":"SMS Card Description #1"
          }
          this.cards = true;   
          listObj.push(SMSPostpaidUnlimitedData);
        //}
        this.SMSCall = true;
        this.cardArray.push({"title":romTitle,"data":listObj,"cardType":"Card Bundle (with Toggle/CTA) 7 Day Internet #1"});        
      }else if((romTitle=="1 DAY Internet PASS") && this.roamingCountry!=""){
            //alert("I am in 1 day pass");
            var index = _.findIndex(this.list.Plan, { display_name: "1 DAY Internet PASS" });
            var quotaUsage = this.list.Plan[index];
            var internetRoaming = {
              "title":romTitle,
              "quota":quotaUsage.quota,
              "class":"roaming-icon icon",         
              "cardType":"Card Description #1"
            }
            this.cards = true;
            this.cardArray.push(internetRoaming);
        //}
          }else if((romTitle=="1-Day INTERNET PASS") && this.roamingCountry!=""){
          //alert("I am in 1 day pass");
          var index = _.findIndex(this.list.Plan, { display_name: "1-Day INTERNET PASS" });
          var quotaUsage = this.list.Plan[index];
          var romDesc = romTitle;
          try{
            var country = this.list.name;
            var countryList = ["Bangladesh","Cambodia","Indonesia","Nepal","Singapore","Sri Lanka"];
            if(countryList.indexOf(country)>-1){
              romDesc = "1-Day INTERNET PASS RM4.99"
            }
          }catch(e){
            console.log(e);
          }
          var internetRoaming = {
            "title":romDesc,
            "quota":quotaUsage.quota,
            "class":"roaming-icon icon",         
            "cardType":"Card Description #1"
          }
          this.cards = true;
          this.cardArray.push(internetRoaming);
      //}
    
      }else if(romTitle=="1-Day BASIC INTERNET PASS"){
        //alert("1-Day BASIC INTERNET PASS")
        var BasicData = {
          "title":romTitle,
          "subtitle":"",
          "dataUsed":this.formatMBValue(data[obj]["USEDDATA"]),
          "dataTotal":this.formatMBValue(data[obj]["TOTALDATA"]),
          "dataBalance":this.formatMBValue(data[obj]["BALANCEDATA"]),
          "date":this.formatDate(data[obj]["EXPIRYDATE"]),
          "unit":'MB',
          "autoRenew":0,
          "class":"roaming-icon icon",
          "cardType":"Card Usage with Limit #1"
        }
        this.cards = true;
        this.cardArray.push(BasicData);
    //}
  }else if(romTitle=="1 DAY CALLS AND SMS PASS" && this.SMSCall==false){
        this.SMSCall = true;
        var listObj = [];
        //alert("I am in 1 DAY CALLS AND SMS PASS");
        var CallsPostpaidUnlimitedData = {
          "title":"Unlimited Calls",
          "subtitle":"Unlimited Calls in visiting country and Malaysia",
          "date":this.formatDate(data[obj]["PASS_VALIDUNTILL"]),
          "icon":"call-icon icon",
          "cardType":"Call Card Description #1"
        }
        this.cards = true;
        listObj.push(CallsPostpaidUnlimitedData);
        var SMSPostpaidUnlimitedData = {
          "title":"Unlimited SMS",
          "subtitle":"Unlimited SMS in visiting country and Malaysia",
          "date":this.formatDate(data[obj]["PASS_VALIDUNTILL"]),
          "icon":"sms-icon icon",
          "cardType":"SMS Card Description #1"
        }
        this.cards = true;  
        listObj.push(SMSPostpaidUnlimitedData);
        this.cardArray.push({"title":romTitle,"data":listObj,"class":"roaming-icon icon","cardType":"Card Bundle (with Toggle/CTA) call & SMS Internet #1"});
      }else if(romTitle=="1-Day SOCIAL ROAM PASS"){
        //alert("in social pass"); 

        var dataPre = [];
        var listObj = [];
        var title;
        var socialPass = data[obj]["PACK_LIST"]["PACK"];
        if(socialPass.length>1)
          dataPre = socialPass;
        else
          dataPre.push(socialPass);
          for(var obj in dataPre){
            var romUsedValPre = this.formatValue(dataPre[obj]["QUOTAUSAGE_GIGABYTES"],dataPre[obj]["QUOTAUSAGE_MEGABYTES"]);
            var romTotalPre = this.formatValue(dataPre[obj]["TOTALQUOTAVALUE_GIGABYTES"],dataPre[obj]["TOTALQUOTAVALUE_MEGABYTES"]);
            var romBalancePre = this.formatValue(dataPre[obj]["QUOTABALANCE_GIGABYTES"],dataPre[obj]["QUOTABALANCE_MEGABYTES"]);
            var romDatePre = this.formatDate(dataPre[obj]["PASS_VALIDUNTILL"]);
           // var romBalancePre = romTotal - romUsedVal;
            title = romTitle;
            var internetDataPre = {
              "title":dataPre[obj].BUSINESSNAME,
              "subtitle":"",
              "dataUsed":romUsedValPre,
              "dataTotal":romTotalPre,
              "dataBalance":romBalancePre,
              "date":romDatePre,
              "unit":this.formatUnit(dataPre[obj]["TOTALQUOTAVALUE_GIGABYTES"],dataPre[obj]["TOTALQUOTAVALUE_MEGABYTES"]),
              "autoRenew":0,
              "class":"roaming-icon icon",
              "cardType":"Card Usage with Limit #1"
            }
            this.cards = true;
            listObj.push(internetDataPre);
          }
          this.cardArray.push({"title":title,"data":listObj,"cardType":"Card Bundle (with Toggle/CTA) #1"});
      }
         
    }
    if(this.cards==false){
      var NoInternet = {
        "title":"No Roaming Plan",
        "subtitle":"You are not subscribed to any Roaming plan",
        "date":"",
        "action":"Subscribe to Roaming Pass Now",
        "class":"roaming-icon icon",
        "cardType":"Card CTA #1"
      }    
      this.cardArray.push(NoInternet);
    }
   // }
  }catch(e){
    console.log("Error"+e);
  }
  }

  generateRoamingInternetPrepaidCards(){
    this.internetPlans = this.dashboardService.GetRoamStatusPre();
    console.log("Roaming Plans "+JSON.stringify(this.internetPlans));
    //alert("this.internetPlans"+this.internetPlans);
    if(this.internetPlans==""|| this.internetPlans==null || this.internetPlans==undefined)
    return;
    var data = [];
    //var nos = this.internetPlans["PASS"];
    if(this.internetPlans["PASS"].length>1)
    data = this.internetPlans["PASS"];
    else
    data.push(this.internetPlans["PASS"]);
    try{
    for(var obj in data){
      if(data[obj]["PASS_ISDATAUSED"] == true){
      var romUsedVal = this.formatMBValue(data[obj]["USAGE"]);
      var romTotal = this.formatMBValue(data[obj]["QUOTA"]);
      var romTitle = data[obj]["PASS_NAME"];
      var romDate = this.formatDate(data[obj]["PASS_VALIDUNTILL"]);
      var romBalance = romTotal - romUsedVal;
      var internetData = {
        "title":romTitle,
        "subtitle":"",
        "dataUsed":romUsedVal,
        "dataTotal":romTotal,
        "dataBalance":romBalance,
        "date":romDate,
        "unit":'GB',
        "autoRenew":0,
        "class":"roaming-icon icon",
        "cardType":"Card Usage with Limit #1"
      }
      this.cardArray.push(internetData);
    }else{
        var index = _.findIndex(this.list.Plan, { display_name: "1-Day INTERNET PASS" });
        var quotaUsage = this.list.Plan[index];
        if(data[obj]["PASS_NAME"]=="1 DAY Internet PASS"){
          var internetRoaming = {
            "title":data[obj]["PASS_NAME"],
            "quota":quotaUsage.quota, 
            "class":"roaming-icon icon",        
            "cardType":"Card Description #1"
          }
          this.cardArray.push(internetRoaming);
      }
    }
    }
  }catch(e){
    console.log("Error "+JSON.stringify(e));
    if(e=="No internet connection")
    return;
  }
  }

  generateRoamingPPUPostpaidCards(){
    var roamingDetails = this.detailedUsageService.GetDetailedusageRoamingPostpaid();
    var formattedRoamingObj = _.object(_.pluck(roamingDetails, 'ParameterName'), _.pluck(roamingDetails, 'ParameterValue'));
    //   this.talkUsedVal = parseFloat(formattedTalkObj["TotalUsage"]).toFixed(2);
    var roamingSMSPPUData = {
      "title":"Pay Per Use SMS",
      "subtitle":"",
      "data":[        
        {
          "key":"Total SMS sent",
          "value":formattedRoamingObj["Outgoing SMS count"]
        }],
      "TotalDataUsage":formattedRoamingObj["Outgoing SMS charge"],
      "class":"sms-icon icon",
      "cardType":"Card PPU #1"
    }
    this.cardArray.push(roamingSMSPPUData);

    var roamingCallsPPUData = {
      "title":"Pay Per Use Calls",
      "subtitle":"",
      "data":[        
        {
          "key":"Total call usage",
          "value":formattedRoamingObj["Standard roaming voice calls"]
        }],
      "TotalDataUsage":formattedRoamingObj["Standard roaming voice calls"],
      "class":"call-icon icon",
      "cardType":"Card PPU #1"
    }
    this.cardArray.push(roamingCallsPPUData); 
  }

  generateRoamingPPUPrepaidCards(){
    try{
    var detailedUsageSMS = this.detailedUsageService.GetDetailedUsagePrepaid();
    var SMSPPU = detailedUsageSMS.SMSCreditUsageDetails;
      var PPUData = {
        "title":"Pay Per Use SMS",
        "subtitle":"",
        "data":[
          {
            "key":"Usage Since",
            "value":this.usageSince
          },
          {
            "key":"Total SMS sent",
            "value":parseFloat(SMSPPU.TotalRoamingSMSCount).toFixed(2)
          }],
        "TotalDataUsage":parseFloat(SMSPPU.TotalRoamingSMSUsage).toFixed(2),
        "class":"sms-icon icon",
        "cardType":"Card PPU #1"
      }
      this.cardArray.push(PPUData);

    var detailedUsageTalk = this.detailedUsageService.GetDetailedUsagePrepaid();
    var talkPPU = detailedUsageTalk.TalkCreditUsageDetails;
    var TalkPPUData = {
        "title":"Pay Per Use Calls",
        "subtitle":"",
        "data":[
          {
            "key":"Usage Since",
            "value":this.usageSince
          },
          {
            "key":"Total call usage",
            "value":parseFloat(talkPPU.TotalRoamingCallDuration).toFixed(2)
          }],
        "TotalDataUsage":parseFloat(talkPPU.TotalRoamingCallUsage).toFixed(2),
        "class":"call-icon icon",
        "cardType":"Card PPU #1"
      }
      this.cardArray.push(TalkPPUData);  
    }catch(e){
      console.log(e);
    }
  }

  goToSubscriptions(value){   
    this.events.publish("goToRoamingTab",value);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailedUsageRoamingPage');
    //this.detailedUsageRoaming = this.detailedUsageService.GetDetailedusageRoamingPostpaid();
    //console.log(this.detailedUsageRoaming);
  }
  cardTitleChange(titleString){
    try{
      return titleString.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }catch(e){
      console.log(e);
    }
  }

}
