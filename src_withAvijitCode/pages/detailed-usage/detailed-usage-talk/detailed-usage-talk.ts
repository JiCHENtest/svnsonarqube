import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DetailedUsageService } from '../../detailed-usage/detailedUsageService';
import { DashboardService } from '../../home/homeService';
import { GatewayService } from '../../../global/utilService';
import { ConstantData } from '../../../global/constantService';
import { GlobalVars } from "../../../providers/globalVars";
import _ from 'underscore';

/**
 * Generated class for the DetailedUsageTalkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detailed-usage-talk',
  templateUrl: 'detailed-usage-talk.html',
})
export class DetailedUsageTalkPage {
  detailedUsageTalk:any={};
  detailedInternetUsage:any;
  cardArray:any = [];
  cardMapping:any;
  userInfo:any;
  isPrepaid:Boolean;
  PPUPrepaid:any;
  internetPlans:any;
  bundleUsagePercent: number = 100;
  usageSince:any;
  isKADCeria:Boolean; 
  isCapZone:boolean = false; 
  freeTalk:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public detailedUsageService: DetailedUsageService, public gatewayService:GatewayService, 
    public dashboardService: DashboardService, public globalVar: GlobalVars, private renderer: Renderer2) {
    console.log('ionViewDidLoad DetailedUsageTalkPage');
    let planName = this.gatewayService.getPlanName();
    console.log("**************** plan name****************"+planName);
    this.cardMapping = ConstantData.dialMapping.postpaid[planName];
    console.log("***************** Card mapping ****************"+JSON.stringify(this.cardMapping));
    //this.GBSharePlan = this.dashboardService.GetUnits();
    this.userInfo = this.dashboardService.GetUserInfo();
    this.isPrepaid = this.gatewayService.isPrePaid();
    this.usageSince = this.gatewayService.getUsageSince();
    this.internetPlans = this.dashboardService.GetAllDataPostpaidPrepaid();
    this.isKADCeria = this.dashboardService.IsKADCeria();
    this.isCapZone = this.globalVar.getCapZoneStatus()
    //console.log(this.detailedUsageTalk);
    this.generateTalkCards();
    // this.cardArray = [
    //   {
    //     "title":"Internet Pack 1",
    //     "subtitle":"3 days plan",
    //     "cardType":"Card CTA #1"
    //   },
    //   {
    //     "title":"Internet Pack 2",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Toggle/CTA #1"
    //   },
    //   {
    //     "title":"Internet Pack 3",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Usage with Limit #1"
    //   },
    //   {
    //     "title":"Internet Pack 4",
    //     "subtitle":"3 days plan",
    //     "cardType":"Card PPU #1"
    //   },
    //   {
    //     "title":"Internet Pack 5",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Description+ Toggle #1"
    //   },
    //   {
    //     "title":"Internet Pack 6",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Description #1"
    //   },
    //   {
    //     "title":"Internet Pack 7",
    //     "subtitle":"3 days plan",
    //     "cardType":"Card Usage with Limit (Kad Ceria) #1"
    //   },
    //   {
    //     "title":"Internet Pack 8",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Bundle (with Toggle/CTA) #1"
    //   },
    //   {
    //     "title":"Internet Pack 9",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Info (Roaming) #1"
    //   },
    //   {
    //     "title":"Internet Pack 10",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Card Info (Plan Activation) #1"
    //   },
    //   {
    //     "title":"Internet Pack 11",
    //     "subtitle":"3 days plan 2",
    //     "cardType":"Group Sharing Data #1"
    //   }
    // ]
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Talk detailed usage", this.renderer);
  }

  formatMinsValue(val){
    var formattedStr = val.substr(0,val.indexOf(' '));
    formattedStr = Number(formattedStr);
    return formattedStr;
  }

  formatDate(val){
    if(val==""|| val==null || val==undefined)
    return;
    var dt = val.substring(6, 8)+'/'+val.substring(4, 6)+'/'+val.substring(2, 4);
    return dt;
  }

  generateAsiaPacks(){
    var data = [];
    let title;
    if(this.internetPlans.subscriberQuota.ASIAPASS.ASIAPASSTALK.length>1){
      for(let i in this.internetPlans.subscriberQuota.ASIAPASS.ASIAPASSTALK){
        var pack = this.internetPlans.subscriberQuota.ASIAPASS.ASIAPASSTALK[i];
        title = pack.BUSINESSNAME;
        var TalkPostpaidData = {
          "title":pack.PACKNAME,
          "subtitle":"",
          "dataUsed":this.formatMinsValue(pack.USAGECALLS),
          "dataTotal":this.formatMinsValue(pack.TOTALCALLS),
          "dataBalance":this.formatMinsValue(pack.BALANCECALLS),
          "date":this.formatDate(pack.EXPIRYDATE),
          "unit":"Minutes",
          "autoRenew":0,
          "cardType":"Card Usage with Limit #1"
        }
       
        data.push(TalkPostpaidData);
       // this.generateCardUsageWithLimit(pack);
      }
    }else{
      var pack = this.internetPlans.subscriberQuota.ASIAPASS.ASIAPASSINTERNET;
      title = pack.BUSINESSNAME;
      var TalkPostpaidData = {
        "title":pack.PACKNAME,
        "subtitle":"",
        "dataUsed":this.formatMinsValue(pack.USAGECALLS),
        "dataTotal":this.formatMinsValue(pack.TOTALCALLS),
        "dataBalance":this.formatMinsValue(pack.BALANCECALLS),
        "date":this.formatDate(pack.EXPIRYDATE),
        "unit":"Minutes",
        "autoRenew":0,
        "cardType":"Card Usage with Limit #1"
      }
      data.push(TalkPostpaidData);
      //this.generateCardUsageWithLimit(pack);
    }
    this.cardArray.push({"title":title,"data":data,"class":"call-icon icon","cardType":"Card Bundle (with Toggle/CTA) #1"});
  }

  generateTalkCards(){ 
    if(this.isCapZone){
      var data = {
        "title":"You are now in Capzone",
        "subtitle":"You have 10mins call time and 10 SMS to use within Celcom network",
        "date":"Valid for 35 days",
        "cardType":"Card Description #1"
      }
      this.cardArray.push(data);
    }
    if(this.isKADCeria){
      var TalkPPUData = {
        "title":"Kad Ceria",
        "subtitle":"Enjoy 10% extra credit with every RM10 and above reload",
        "class":"kad-icon icon",
        "cardType":"Card Usage with Limit (Kad Ceria) #1"
      }
      this.cardArray.push(TalkPPUData);
      var obj = this.detailedUsageService.GetKadCeria();
      try{
        if(obj.BalanceDesc){
          console.log("Kad Ceria Bonus");
          var dataObj = {
            "title":"Kad Ceria Bonus",
            "key":"Balance Usage",
            "value":(obj.Balance/10000),
            "class":"kad-icon icon",
            "cardType":'Card PPU Kad Ceria #1'
          }
          this.cardArray.push(dataObj);
        }
      }catch(e){
        console.log("error"+e);
      }
      
    }    
    //this.generateKadCeriaUsageDetail();
    if(this.internetPlans.subscriberQuota.UNLIMITEDTALKLIST){
        this.generateUnlimitedCall();
    }   
    if(this.isPrepaid){      
      try{
      if(this.internetPlans.subscriberQuota.ASIAPASS.ASIAPASSTALK){
        this.generateAsiaPacks();
      }
    }catch(e){
      console.log("No Asia Packs");
      if(e=="No internet connection")
      return;
    }
    this.generatePPUTalkPrepaidCard();
    }else{      
      this.generateTalkPostpaidCards();
      this.generatePPUTalkPostpaidCard();
    }
     // if(this.internetPlans.subscriberQuota.KadCeriaUsageDetail){
    //   this.generateKadCeriaUsageDetail();
    // }
  }

  generateUnlimitedCall(){
    var pack = this.internetPlans.subscriberQuota.UNLIMITEDTALKLIST.UNLIMITEDTALK;
    var CallData = {
      "title":'Call Add-on',
      "subtitle":pack.DETAIL,
      "date":'Valid till '+this.formatDate(pack.EXPIRYDATE),
      "class":"call-icon icon",
      "cardType":"Card Description #1"
    }
    this.cardArray.push(CallData);
    this.freeTalk = true;
  }

  generateKadCeriaUsageDetail(){
    if(this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage){
      if(this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage.length>1){        
        for(let i in this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage){
          var pack = this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage[i];          
          var KadCeriaData = {
            "title":'Kad Ceria',
            "subtitle":'Enjoy 10% extra credit with every RM10 and above reload',
            "date":this.formatDate(pack.KadCeriaSMSExpiryDate),
            "class":"kad-icon icon",
            "cardType":"Card Description #1"
          }
          this.cardArray.push(KadCeriaData);
        }
      }else{
        var pack = this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage;
        var KadCeriaData = {
          "title":'Kad Ceria',
          "subtitle":'Enjoy 10% extra credit with every RM10 and above reload',
          "date":this.formatDate(pack.KadCeriaSMSExpiryDate),
          "class":"kad-icon icon",
          "cardType":"Card Description #1"
        }
        this.cardArray.push(KadCeriaData);
      }
    }
  }

  generatePPUTalkPrepaidCard(){
    try{
    this.detailedUsageTalk = this.detailedUsageService.GetDetailedUsagePrepaid();
    var talkPPU = this.detailedUsageTalk.TalkCreditUsageDetails;
    var talkBonus = this.detailedUsageTalk.BonusTalkDetails.BonusTalkDetail;
    var offnet:any = parseFloat(talkPPU.TotalTalkCreditUsage)-parseFloat(talkPPU.TotalOnNetCallUsage);
    let total: any = parseFloat(talkPPU.TotalOnNetCallUsage)+parseFloat(offnet)+parseFloat(talkPPU.TotalIDDCallUsage);
    //alert("total "+total);    
    var TalkPPUData = {
        "title":"Pay Per Use Calls",
        "subtitle":"",
        "data":[
          {
            "key":"Usage Since",
            "value":this.usageSince
          },
          {
            "key":"Same Network",
            "value":parseFloat(talkPPU.TotalOnNetCallUsage).toFixed(2)
          },
          {
            "key":"Other Network",
            "value":parseFloat(offnet).toFixed(2)
          },         
          {
            "key":"IDD",
            "value":parseFloat(talkPPU.TotalIDDCallUsage).toFixed(2)
          }],
        "TotalDataUsage":total.toFixed(2),
        "class":"icon_pay-per-use icon",
        "cardType":"Card PPU #1"
      }
      this.cardArray.push(TalkPPUData);
    }catch(e){
      console.log(e);
    }      
    //   if(talkBonus){
    //     let totalcharge: any = parseFloat(talkBonus.BonusTalkOnNetBal)+parseFloat(talkBonus.BonusTalkOffNetBal);
    //     var TalkPPUBonusData = {
    //       "title":"Pay Per Use Bonus",
    //       "subtitle":"",
    //       "data":[
    //         {
    //           "key":"Usage Since",
    //           "value":this.usageSince
    //         },
    //         {
    //           "key":"Same Network",
    //           "value":parseFloat(talkBonus.BonusTalkOnNetBal).toFixed(2)
    //         },
    //         {
    //           "key":"Other Network",
    //           "value":parseFloat(talkBonus.BonusTalkOffNetBal).toFixed(2)
    //         }],
    //       "TotalDataUsage":totalcharge.toFixed(2),
    //       "class":"icon_pay-per-use icon",
    //       "cardType":"Card PPU #1"
    //     }
    //     this.cardArray.push(TalkPPUBonusData);
    // }
  }

  generatePPUTalkPostpaidCard(){
    try{
    this.detailedUsageTalk = this.detailedUsageService.GetDetailedusageTalkPostpaid();
    var formattedTalkObj = _.object(_.pluck(this.detailedUsageTalk, 'ParameterName'), _.pluck(this.detailedUsageTalk, 'ParameterValue'));
    let total: any = parseFloat(formattedTalkObj["Allnet"])+parseFloat(formattedTalkObj["IDD"])+parseFloat(formattedTalkObj["SpecialNumbers"]);
    var TalkPPUData = {
      "title":"Pay Per Use Calls",
      "subtitle":"",
      "data":[{
        "key":"All net",
        "value":parseFloat(formattedTalkObj["Allnet"]).toFixed(2)
        },
        {
        "key":"IDD",
        "value":parseFloat(formattedTalkObj["IDD"]).toFixed(2)
        },
        {
        "key":"Special Numbers",
        "value":parseFloat(formattedTalkObj["SpecialNumbers"]).toFixed(2)
        }],
      "TotalDataUsage":total.toFixed(2),
      "class":"icon_pay-per-use icon",
      "cardType":"Card PPU #1"
    }
    this.cardArray.push(TalkPPUData);
  }catch(e){
    console.log(e);
  }
  }

  generateTalkPostpaidCards(){
    var talkPostpaid = this.dashboardService.GetTalkPostpaid();
    //alert(JSON.stringify(talkPostpaid));
    try{
    if(talkPostpaid.TotalTalk){
    //alert(JSON.stringify(talkPostpaid.TotalTalk));
    if(talkPostpaid.TotalTalk=="Unlimited" || talkPostpaid.TotalTalk=="NA"|| talkPostpaid.TotalTalk=="NaN"){
          var TalkPostpaidUnlimitedData = {
            "title":"Unlimited Calls",
            "subtitle":"You are entitled unlimited calls to all Networks",
            "date":"",
            "class":"call-icon icon",
            "cardType":"Card Description #1"
          }
          this.cardArray.push(TalkPostpaidUnlimitedData);        
        }else{
          var date = ''; 
          if(talkPostpaid.ExpireTime){
            date = this.formatDate(talkPostpaid.ExpireTime)//this.getBillingDate(this.billDate);
          }else{
            date = '';
          }
          var title;
          if(talkPostpaid.BalanceDesc=="" || talkPostpaid.BalanceDesc==null || talkPostpaid.BalanceDesc==undefined){
            title = "Postpaid Calls"
          }else{
            title = talkPostpaid.BalanceDesc;
          }
          var TalkPostpaidData = {
            "title":title,
            "subtitle":"",
            "dataUsed":talkPostpaid.UsageTalk,
            "dataTotal":talkPostpaid.TotalTalk,
            "dataBalance":talkPostpaid.Balance,
            "date":date,
            "unit":"Minutes",
            "autoRenew":0,
            "class":"call-icon icon",
            "cardType":"Card Usage with Limit #1"
          }
          this.cardArray.push(TalkPostpaidData);
      }
    }
  }catch(e){
    console.log("************** talk issues ");
    if(e=="No internet connection")
    return;
  }
  }   

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailedUsageTalkPage');
  }
  cardTitleChange(titleString){
    try{
      return titleString.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }catch(e){
      return "";
    }
  }


}
