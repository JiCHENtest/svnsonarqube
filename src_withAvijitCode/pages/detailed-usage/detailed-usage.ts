import {Component, Renderer2} from "@angular/core";
import {NavController, NavParams, Events} from "ionic-angular";
import {DetailedUsageInternetPage} from "./detailed-usage-internet/detailed-usage-internet";
import {DetailedUsageSmsPage} from "./detailed-usage-sms/detailed-usage-sms";
import {DetailedUsageRoamingPage} from "./detailed-usage-roaming/detailed-usage-roaming";
import {DetailedUsageTalkPage} from "./detailed-usage-talk/detailed-usage-talk";
import {SubscriptionsTabService } from '../../pages/subscriptions/subscriptionsTabService';
import { SubscriptionsPage } from '../../pages/subscriptions/subscriptions';
import { SuperTabsController } from '../../components/custom-ionic-tabs';
import { GatewayService } from '../../global/utilService';

/**
 * Generated class for the DetailedUsagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detailed-usage',
  templateUrl: 'detailed-usage.html',
})
export class DetailedUsagePage {


  internetTab = DetailedUsageInternetPage;
  talkTab = DetailedUsageTalkPage;
  smsTab = DetailedUsageSmsPage;
  roamingTab = DetailedUsageRoamingPage;


  selectedTab:number = 0;

  constructor( public navCtrl: NavController, public navParams: NavParams, 
    public events:Events, public subscriptionsTabService:SubscriptionsTabService, 
    private tabsCtrl: SuperTabsController, private renderer: Renderer2, public gatewayService:GatewayService) {
    const selectedTab = this.navParams.get("selectedTab");
    if(selectedTab) {
      this.selectedTab = selectedTab;
    }
    this.events.unsubscribe("goToRoamingTab");
    this.events.subscribe("goToRoamingTab",(value)=>{
    this.subscriptionsTabService.SetTabInfo(value);
    // change for CLU-50
    //old   // this.navCtrl.setRoot(SubscriptionsPage);
    // new 
    this.navCtrl.push(SubscriptionsPage);  
    }
  );

  }

  // ionViewDidEnter(){
  //   this.gatewayService.resetAllAnalyticData();
  //   this.gatewayService.setAnalyticPageNameProfileSegment("Detailed usage", this.renderer);
  // }

  ngAfterViewInit() {
    // this.tabsCtrl.enableTabsSwipe(false, 'detailedUsageTabs');
  }


  goBack() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    }
  }


}
