import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { MyDealsModalPage } from '../my-deals-modal/my-deals-modal';
import { GlobalVars } from "../../providers/globalVars";
import { GatewayService } from '../../global/utilService';

// page
import { ConfirmationScreensMydealsPage } from "../../pages/confirmation-screens-mydeals/confirmation-screens-mydeals";
/**
 * Generated class for the MyDealsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-deals',
  templateUrl: 'my-deals.html',
})
export class MyDealsPage {

  // mydeals 
  mydealsInfo : any;

  constructor(public viewCtrl: ViewController, public modalCtrl: ModalController, 
    public navCtrl: NavController, public navParams: NavParams, 
    public globalVar :GlobalVars, private renderer: Renderer2, public gatewayService:GatewayService) {
    // mydeals
    console.log("this.mydealsInfo param ", JSON.stringify(this.navParams.get("mydealsdetails")));
    
    this.mydealsInfo = this.navParams.get("mydealsdetails");
    console.log("this.mydealsInfo ",this.mydealsInfo.planDescription1);
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("My deals", this.renderer);
  }
  
  presenMydealsModal(mydealsInfo) {

    // ConfirmationScreensMydealsPage reference
    this.navCtrl.push(ConfirmationScreensMydealsPage,{"mydealPlanInfo":this.mydealsInfo});    

    // ConfirmationScreensMydealsPage

    // let modal = this.modalCtrl.create(ConfirmationScreensMydealsPage, {'mydealPlanInfo':mydealsInfo}, { showBackdrop: false, enableBackdropDismiss: true ,cssClass: this.globalVar.getCurrentTheme() + ' mydeal-modal' });

    // ole one
    // let modal = this.modalCtrl.create(MyDealsModalPage, {'mydealsInfo':mydealsInfo}, { showBackdrop: false, enableBackdropDismiss: true ,cssClass: this.globalVar.getCurrentTheme() + ' mydeal-modal' });

    // modal.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
