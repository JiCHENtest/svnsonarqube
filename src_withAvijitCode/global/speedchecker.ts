declare var cordova;
/*
  Generated class for the SpeedcheckerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
// @Injectable()
export class SpeedcheckerProvider {

  // provider
  msisdnTextSpeed : any;
  registrationIdSpeed : any;
  downloadStatus : any;
  uploadStatus : any;
  pingStatus : any;

  constructor() {
    console.log('Hello SpeedcheckerProvider Provider');
  }


  registeredMsisdn : any = null;
  registeredId : any = null;
  
   refreshMsisdn(num : any) : any {
    console.log("SpeedcheckerProvider refreshMsisdn:");
     var _this = this;
        return new Promise((resolve, reject) => {
            return cordova.plugins.aptus.getMsisdn(function(msisdn) {
                console.log("SpeedcheckerProvider getMsisdn:" + msisdn);
                _this.registeredMsisdn = msisdn;
                if (_this.registeredMsisdn == "") {
                    //   document.getElementById('msisdnTextSpeed').textContent = "MSISDN:- Undefined";
                    _this.msisdnTextSpeed = "Undefined";
                } else {
                    //   document.getElementById('msisdnTextSpeed').textContent = "MSISDN:- " + _this.registeredMsisdn;
                    if( num == 1 )
                        _this.resetSpeedtest();

                    _this.msisdnTextSpeed = _this.registeredMsisdn;
                }


                cordova.plugins.aptus.getRegistrationId(function(registrationId)  {
                    console.log("SpeedcheckerProvider getRegistrationId:" + registrationId);
                    _this.registeredId = registrationId;
                    if (_this.registeredId == null || _this.registeredId == "") {
                        //   document.getElementById('registrationIdSpeed').textContent = "Registration ID: Not Registered ++";
                        _this.registrationIdSpeed = "Not Registered";
                        resolve(registrationId);
                    } else {
                        //   document.getElementById('registrationIdSpeed').textContent = "Registration ID: ++" + _this.registeredId;
                        _this.registrationIdSpeed = _this.registeredId;
                        resolve(registrationId);
                    }
                });

            });
  
            
        });
  
  }
  
  /*
    Start a speed test, it will first check that a data connection is available before attempting to start the test.
  */
   btn_startSpeedtest() {
        var _this = this;
        console.log("btn_startSpeedtest: in" );

        if (cordova.plugins.aptus.hasDataConnection(function(msg) {
        _this.startTest();
        console.log("btn_startSpeedtest: success cb" + msg);
        },function(err) {
        alert("Cannot start speed test, no data connection");
        })){}
  }
  
  /*
    Start a speed test, called if a data connection is available and will prompt the user to enter their MSISDN,
    this is to associate the speedtest result data with a particular user.
  */
   startTest() {
    console.log("startTest");
     var _that = this;
      if (_that.registeredMsisdn == null || _that.registeredMsisdn == "") {
        console.log("startTest In");
          var msisdn = prompt("Please enter your phone number", "");
        return new Promise((resolve, reject) => {
            if (msisdn != null && msisdn.length > 0) {
                console.log("startTest if");
              cordova.plugins.aptus.setMsisdn(msisdn);
              return this.refreshMsisdn(1).then(res =>{
                    console.log("startTest in ",res);

                    cordova.plugins.aptus.speedtestStart(
                    (state)=>{ 
                        console.log("speedtestStart in");
                        _that.speedtestCallback(state);
                    },(err)=>  {
                        console.log("speedtestStart err ",err);
                        _that.speedtestErrorCallback(err);                
                    });

                    resolve(res);
                },err =>{
                    console.log("startTest in err ",err);
                    reject();
                });
            //   _that.refreshMsisdn();
  
            //   _that.resetSpeedtest();
              
             }
        });
  
      } else {
          cordova.plugins.aptus.speedtestStart(function
          (state) { 
            console.log("startTest else ",state);
            _that.speedtestCallback(state);
          },function(err) {
            console.log("startTest else errv",err);
            _that.speedtestErrorCallback(err);                
          });
      }
  }
  
  /*
    Stop the current speed test
  */
   btn_stopSpeedtest() {
    console.log("btn_stopSpeedtest");
    this.resetSpeedtest();
      cordova.plugins.aptus.speedtestStop();
  }
  
  /*
    Speed Test Call-back :
    - The 'state' parameter is a JSON string containing information about the state of the current speedtest.
  */
   speedtestCallback(state) {
    
      var result = JSON.parse(state);

      // clean to get new value
      this.downloadStatus = "";
      this.uploadStatus =  "";
      this.pingStatus = "";
    var speed:any = "";
      console.log("speedtestCallback ",state)
      if (result.status == "download_started") {
          
        //   document.getElementById('downloadStatus').textContent = "Download: Started";
        this.downloadStatus = "Started";
  
      } else if (result.status == "download_progress" || result.status == "download_finished") {
          
          speed = result.speed;
          
        //   document.getElementById('downloadStatus').textContent = "Download: " + ((speed * 8) / 1000000) + " Mbps";
        this.downloadStatus = ((speed * 8) / 1000000) + " Mbps";
  
      } else if (result.status == "upload_started") {
        //   document.getElementById('uploadStatus').textContent = "Upload: Started";
          this.uploadStatus = "Started";
  
      } else if (result.status == "upload_progress" || result.status == "upload_finished") {
          
          speed = result.speed;
         
        //   document.getElementById('uploadStatus').textContent = "Upload: " + ((speed * 8) / 1000000) + " Mbps";
        this.uploadStatus = ((speed * 8) / 1000000) + " Mbps";
  
      } else if (result.status == "ping_started") {
        //   document.getElementById('pingStatus').textContent = "Ping: Started";
        this.pingStatus = "Started";
  
      } else if (result.status == "ping_progress" || result.status == "ping_finished") {
        this.pingStatus = (result.time + " ms");
  
      } else if (result.status == "finished") {
  
          var downloadResults = result.download;
          var uploadResults = result.upload;
          var pingResults = result.ping;
  
        //   document.getElementById('downloadStatus').textContent = "Download: " + ((downloadResults.speed * 8) / 1000000) + " Mbps";
        //   document.getElementById('uploadStatus').textContent = "Upload: " + ((uploadResults.speed * 8) / 1000000) + " Mbps";
        //   document.getElementById('pingStatus').textContent = "Ping: " + pingResults.time + " ms";
          
          this.downloadStatus = (this.downloadStatus + ((downloadResults.speed * 8) / 1000000) + " Mbps");
          this.uploadStatus = (this.uploadStatus + ((uploadResults.speed * 8) / 1000000) + " Mbps");
          this.pingStatus = (this.pingStatus + pingResults.time + " ms");

          console.log("this.downloadStatus "+this.downloadStatus);
          console.log("this.uploadStatus "+this.uploadStatus);
          console.log("this.pingStatus "+this.pingStatus);
      }
  }
  
   resetSpeedtest(){

     console.log("SpeedcheckerProvider ");
    // document.getElementById('downloadStatus').textContent = "Download: Pending";
    // document.getElementById('uploadStatus').textContent = "Upload: Pending";
    // document.getElementById('pingStatus').textContent = "Ping: Pending";	

    // this.msisdnTextSpeed : any;
    // this.registrationIdSpeed : any;
    this.downloadStatus = "Pending";
    this.uploadStatus = "Pending";
    this.pingStatus = "Pending";

  }
  
  /*
    Speed Test error call-back :
    - The 'state' parameter is a JSON string containing information about the nature of the speed test error.
  */
   speedtestErrorCallback(state) {
    console.log("speedtestErrorCallback ",state)
      var result = JSON.parse(state);
  
      alert("Error: " + result.status);
  }

  getMsisdnTextSpeed() : any {
    return this.msisdnTextSpeed;
  }

  getRegistrationIdSpeed() : any {
    return this.registrationIdSpeed;
  }

  getUploadStatus() : any {
    return this.uploadStatus;
  }

  getDownloadStatus() : any {
    return this.downloadStatus;
  }

  getPingStatus() : any {
    return this.pingStatus;
  }
}
