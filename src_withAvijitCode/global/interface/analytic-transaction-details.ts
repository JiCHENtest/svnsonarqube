export interface analyticTransactionDetailsInterface {
  jouneryDepth?: string,
  transactProduct?: string,
  transactValue?: string,
  transactMethod?: string,
  transactResult?: string
}