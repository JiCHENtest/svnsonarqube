
/**
 * @author Capgemini 
 */

/**
 * @Module Name - utilService
 * @Description - Singleton class for webservices, This is common gateway for all fetch/post calls 
 */

import { Injectable, Input } from '@angular/core';
import { Network } from '@ionic-native/network';
import { AlertController, LoadingController } from 'ionic-angular';
import { JsonstoreService } from './jsonstore';
import { LocalStorage } from './localStorage';
import { DashboardService } from '../pages/home/homeService';
import { DetailedUsageService } from '../pages/detailed-usage/detailedUsageService'
import { HandleError } from './handleerror';
// import { Headers, Http, Response } from '@angular/http';
import { ConstantData } from "./constantService";
import { Platform } from 'ionic-angular';
import { AlertService } from '../global/alert-service';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { GlobalVars } from "../providers/globalVars";

import { AnalyticsService } from './analytic.service';
import { RendererService } from './renderer.service';

declare var _satellite: any;

declare var WLResourceRequest;
//declare var WL;
//declare var WLAuthorizationManager;
interface Subject {
  registerObserver(o: Observer);
  notifyObservers(val: string);
}

interface Observer {
  update(theme: boolean);
  updateDashboard();
  callingMethod();
  updateLifestyle();//updating data on lifestyle page
  //updateDial();
}



@Injectable()
export class GatewayService {

  public isUserLoggedIn: Boolean = true;
  public isFirstTimeUser: Boolean = true;
  loader;
  public isLoaderOn: Boolean = false;
  public flagForDashboardReload: Boolean = false;

  XPAX: any;
  isPrepaidVal: boolean;
  mobileNumber: any;
  BillingAccountNo: any;
  customerInfo: any;
  selectedMobileNumber: any;
  VLRDetails;
  isCBS: Boolean = false;
  PhonebookNumber: any;
  firstResendTime: any;
  networkStatus: any;
  startTime: any;
  endTime: any;
  onChangeCalled: boolean = false;
  customerRetriveResponse: any;
  internetAlert: number = 0;
  user_priv_data: any;
  isPriviledgeReload: boolean;
  PTPEligibilityData: any;
  billingType: any;
  PreviousNumber: any;
  basePlanForSubscription: boolean = false;
  constructor(private network: Network, private openNativeSettings: OpenNativeSettings, 
    private alertCtrlInstance: AlertService, public alertCtrl: AlertController, 
    public loadingCtrl: LoadingController, public jsonstoreService: JsonstoreService, 
    public localStorage: LocalStorage, public dashboardService: DashboardService, 
    public detailedUsageService: DetailedUsageService, public handleerror: HandleError, 
    private platform: Platform, public globalVar: GlobalVars, 
    public _analyticsService: AnalyticsService, public _rendererService: RendererService) {
    //this.MfpInit();

  };

  private observers: Observer[] = [];
  //private theme: boolean;

  registerObserver(o: Observer) {
    this.observers.push(o);
  }

  showAlertToHandleNavigate(title, msg, btn) {
    if (Array.isArray(btn)) {

      try {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          buttons: [{
            text: btn[1],
            role: 'cancel',
            cssClass: 'submit-button',
            handler: () => {
              this.setInterentFlag(false);
              console.log('Cancel clicked');
            }
          },
          {
            text: btn[0],
            cssClass: 'submit-button',
            handler: () => {
              this.setInterentFlag(false);
              this.openNativeSettings.open("wifi");
              console.log('Call to Action Clicked');
            }
          }],
          cssClass: 'success-message error-message',
          enableBackdropDismiss: false
          // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
        });
        alert.present();

      }
      catch (e) {
        console.log("Exception : Alert open ", e);
      }
    }
  }

  setInterentFlag(status) {
    if (status) {
      this.internetAlert++;
    } else {
      this.internetAlert--;
    }

  }


  getInternetFlag() {
    return this.internetAlert;
  }
  notifyObservers(val: string) {
    if (val == "Menu") {
      for (let observer of this.observers) {
        observer.update(this.isPrepaidVal);
      }
    } else if (val == "Dashboard") {
      for (let observer of this.observers) {
        observer.updateDashboard();
      }
      // } else if (val == "Dial") {
      //   for (let observer of this.observers) {
      //     observer.updateDial();
      //   }
    } else if (val == "updateLifestyle") {
      //updateLifestyle
      for (let observer of this.observers) {
        observer.updateLifestyle();
      }
    } else {
      for (let observer of this.observers) {
        observer.callingMethod();
      }
    }
  }

  MfpInit() {
    console.debug('-- trying to init WL client');
  }

  CallAdapter(url, reqType, params) {

    if (this.platform.is('ios')) {

      this.networkStatus = this.network.type;

    }
    else {

      this.networkStatus = this.network.type;

    }
    if (this.networkStatus == null || this.networkStatus == 'none') {

      if (this.getInternetFlag() == 0) {
        //yourcode
        console.log("Internet alert is hit");

        this.showAlertToHandleNavigate("Uh Oh. No Internet Detected ", "Please turn on mobile data or connect to a wifi network to continue.", ["Settings", "Close"]);
        this.setInterentFlag(true);
      }


      return new Promise((resolve, reject) => {
        var response = "No internet connection";

        reject(response);

      });

    } else {
      var resourceRequest;

      resourceRequest = new WLResourceRequest(url, reqType);

      let arr = JSON.stringify(params);
      resourceRequest.setHeader('Content-Type', 'application/x-www-form-urlencoded');
      var data = { "params": "['" + arr + "']" };


      return new Promise((resolve, reject) => {

        var req = resourceRequest.sendFormParameters(data).then(
          function (response) {
            console.log("-- success from the adapter: " + JSON.stringify(response));
            resolve(response.responseJSON);
          },
          function (response) {
            console.log("-- failure: " + JSON.stringify(response));
            reject(response.errorMsg);
          }
        );
        console.log("************************************** resourceRequest " + req);
        console.log("************************************** resourceRequest " + JSON.stringify(req));
      });
    }

  }

  GetNetworkStatus() {
    var type = '';
    if (this.platform.is('ios')) {
      type = this.network.type;
      this.networkStatus = this.network.type;
      return type;
    }
    else {
      type = this.network.type;
      this.networkStatus = this.network.type;
      return type;
    }
  }
  DataAlignment(gettingDate) {
    var day = gettingDate.slice(0, 8);
    var year = day.match(/.{1,4}/g);
    var date = year[1].match(/.{1,2}/g);
    var finalDate = date[1] + '/' + date[0] + '/' + year[0];
    return finalDate;
  }
  GetSelectMobileNumber() {
    return this.selectedMobileNumber;
  }
  SetSelectMobileNumber(value) {
    this.selectedMobileNumber = value;
  }
  GetIsUserLoggedIn() {
    return this.isUserLoggedIn;
  }

  SetUserLoggedFlag(value) {
    this.isUserLoggedIn = value;
  }
  GetIsFirstTimeUser() {
    return this.isFirstTimeUser;
  }

  SetcustomerRetriveResponse(res) {
    this.customerRetriveResponse = res;
  }

  GetcustomerRetriveResponse() {
    return this.customerRetriveResponse;
  }


  SetFirstTimeUser(value) {
    this.isFirstTimeUser = value;
  }

  GetMobileNumber() {
    return this.mobileNumber;
  }

  SetMobileNumber(value) {
    this.mobileNumber = value;
  }

  SetVLRDetailsData(obj) {
    this.VLRDetails = obj;
  }

  GetVLRDetailsData() {
    return this.VLRDetails.Envelope.Body.ActQuotaUsageQueryFUPResponse;
  }

  GetVLR() {
    return this.VLRDetails.Envelope.Body.ActQuotaUsageQueryFUPResponse.RoamingFlag.CDATA;
  }

  GetCBSValue() {
    return this.isCBS;
  }

  getPreviousNumber() {
    return this.PreviousNumber;
  }

  setPreviousNumber(MobileNumber) {
    this.PreviousNumber = MobileNumber;
  }

  setOnChangeCalled(flag) {
    this.onChangeCalled = flag;
  }

  getOnChangeCalled() {
    return this.onChangeCalled;
  }

  SetBillingType(value) {
    this.billingType = value;
  }

  GetBillingType() {
    return this.billingType;
  }
  getPlanName() {
    var planName = this.customerInfo.ListOfServices.Services.PLAN;
    // alert("this.planName"+this.planName);
    if (planName.indexOf('Plan') > -1)
      planName = planName.replace(/ Plan$/, "");
    return planName;
  }

  LoaderService() {
    this.loader = this.loadingCtrl.create({
      content: "Loading",
      //duration: 2000,
    });
    return this.loader;
  }

  SetXPAX(status) {
    this.XPAX = status;
    if (this.XPAX == "Prepaid")
      this.isPrepaidVal = true;
    else
      this.isPrepaidVal = false;
  }



  isPrePaid() {
    //alert(this.XPAX);
    if (this.XPAX == "Prepaid")
      return true;
    else
      return false;

  }

  generateRandomNum(digits) {
    var text = '';
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < digits; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  FetchUserInfo() {
    var _that = this;
    //alert("fetch info mobile number"+this.mobileNumber);
    try {
      var params = {
        "SERIAL_NUM": this.mobileNumber//this.localStorage.get("MobileNum","")
      }
      return new Promise((resolve, reject) => {
        this.CallAdapter(ConstantData.adapterUrls.GetUserInfo, "POST", params).then(function (res) {
          console.log("*********************************** component success FetchUserInfo" + JSON.stringify(res));
          _that.handleerror.handleErrorFetchUserInfo(res, "FetchUserInfo").then((result) => {
            if (result == true) {
              console.log("hai if ", result);
              // _that.dashboardService.SetUserInfo(res);
              //  var userInfo = _that.dashboardService.GetUserInfo();
              // _that.customerInfo = userInfo;
              // _that.SetXPAX(userInfo.ListOfServices.Services.ServiceType);
              // _that.BillingAccountNo = userInfo.ListOfServices.Services.AssetBillingAccountNo;
              resolve(res);
            }
            else {
              console.log("hai else ", result);
              reject("error : Unable to .");
            }

          }, (err) => {
            console.log("hai else err ", err);
            reject("error : no data found.");
          });
        })
          .catch(function (e) {
            console.log("******************************* component  failure FetchUserInfo" + e);
            //alert("error user info FetchUserInfo");

            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }
  }





  FetchPlanDetails(serviceType, plan) {
    //alert("fetch info mobile number"+this.mobileNumber);
    try {
      var params = {
        "service": serviceType,//this.localStorage.get("MobileNum","")
        "plan": plan
      }
      return new Promise((resolve, reject) => {
        this.CallAdapter(ConstantData.adapterUrls.GetCardMapping, "POST", params).then(function (res) {
          console.log("*********************************** component success FetchUserInfo" + JSON.stringify(res));
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  failure FetchUserInfo" + e);
            //alert("error user info");
            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }
  }

  FetchVLRDetails() {
    //alert("fetch FetchVLRDetails mobile number"+this.mobileNumber);
    var _that = this;
    try {
      var params = {
        "MSISDN": this.mobileNumber,//this.localStorage.get("MobileNum","")
        "PlanType": this.customerInfo.ListOfServices.Services.ServiceType
      }
      return new Promise((resolve, reject) => {
        this.CallAdapter(ConstantData.adapterUrls.QueryQuotaUsageDetailsProcess, "POST", params).then(function (res) {
          console.log("*********************************** component success FetchVLRDetails" + JSON.stringify(res));
          //alert("*********************************** component success FetchVLRDetails"+JSON.stringify(res));
          _that.SetVLRDetailsData(res);
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  failure FetchVLRDetails" + e);
            //alert("error user info");
            reject(e);
          });
      });
    } catch (e) {
      //alert(e);
    }
  }

  //Postpaid
  FetchPostpaidBillingInfo() {
    //alert("fetch FetchPostpaidBillingInfo mobile number"+this.mobileNumber);
    var _that = this;
    console.log(this.customerInfo.ListOfServices.Services.AssetBillingAccountNo);
    var params = {
      "MOBILENUM": this.mobileNumber,//this.localStorage.get("MobileNum",""),
      "BillingAccountNumber": this.customerInfo.ListOfServices.Services.AssetBillingAccountNo //this.BillingAccountNo;
    }
    return new Promise((resolve, reject) => {
      this.CallAdapter(ConstantData.adapterUrls.GetPostpaidBillingInfo, "POST", params).then(function (res) {
        console.log("************************************* component success FetchPostpaidBillingInfo" + JSON.stringify(res));
        _that.handleerror.handleErrorFetchPostpaidBillingInfo(res, "FetchPostpaidBillingInfo").then((result) => {
          if (result == true) {
            console.log("hai if FetchPostpaidBillingInfo ", result);
            _that.dashboardService.SetPostpaidBillingInfo(res);
            //strat checking for cap zone and call bar

            //console.log(responseBillingInfo);
            var postpaidBillingInfo = _that.dashboardService.GetPostpaidBillingInfo();
            var callBarDetails = postpaidBillingInfo.CallBarDetails;
            //callBarDetails.ElementId = "10005";
            //if user has entered capzone
            console.log('Element ID ' + callBarDetails.ElementId);
            if (callBarDetails.ElementId == "45760") {
              console.log('cap zone element id' + callBarDetails.ElementId);
              _that.globalVar.setCapZoneStatus(true);
              if (_that.globalVar.getcallBarredStatus()) {
                _that.globalVar.setcallBarredStatus(!_that.globalVar.getcallBarredStatus());
              }
            }
            else {
              _that.globalVar.setCapZoneStatus(false);
            }
            //if user has entered call bar or manual call bar(10006)

            if (callBarDetails.ElementId == "10005" || callBarDetails.ElementId == "10006" || callBarDetails.ElementId == "100000" || callBarDetails.ElementId == "000001" || callBarDetails.ElementId == "000002" || callBarDetails.ElementId == "000200" || callBarDetails.ElementId == "002000" || callBarDetails.ElementId == "000100" || callBarDetails.ElementId == "001000" || callBarDetails.ElementId == "200000") {
              console.log('cap zone element id' + callBarDetails.ElementId);
              _that.globalVar.setcallBarredStatus(true);
              if (_that.globalVar.getCapZoneStatus()) {
                _that.globalVar.setCapZoneStatus(!_that.globalVar.getCapZoneStatus());
              }
            }
            else {
              _that.globalVar.setcallBarredStatus(false);
            }
            resolve(res);
          }
          else {
            console.log("hai else FetchPostpaidBillingInfo ", result);
            reject("error : no data found.");
          }
        });
        // this.handleError(res,"FetchPostpaidBillingInfo");
        // resolve(res);
      })
        .catch(function (e) {
          console.log("****************************************** component failure FetchPostpaidBillingInfo" + e);
          //alert("error user info FetchPostpaidBillingInfo ");
          reject(e);
        });
    });
  }

  //Get ALL sub Mobile Number 
  FeachAllSubMObileNumber() {
    //alert("fetch FeachAllSubMObileNumber mobile number"+this.mobileNumber);
    try {
      var _that = this;
      // var getAuthString = window.btoa('newCelcomApp:newce!c0m@99%');
      // "AuthString": getAuthString
      //var getMobileNumber = "60145960894";
      // alert("O*****K"+this.mobileNumber);
      var params = {
        "MobileNumber": this.mobileNumber

      }
      return new Promise((resolve, reject) => {
        this.CallAdapter(ConstantData.adapterUrls.GetMobileNumber, "POST", params).then((res) => {

          console.log("*********************************** component  FeachAllSubMObileNumber" + JSON.stringify(res));
          _that.SetcustomerRetriveResponse(res);
          //alert("AlertFine"+res);
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  failure FeachAllSubMObileNumber" + e);
            // alert("ERROR"+e);
            //alert("error user info");
            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }

  }


  FetchTalkPostpaid() {
    //alert("fetch FetchTalkPostpaid mobile number"+this.mobileNumber);
    var _that = this;
    var params = {
      "MOBILENUM": this.mobileNumber,//"0196752391",//"0193401465",//this.mobileNumber,//this.localStorage.get("MobileNum",""),
      "BillingProfileId": this.customerInfo.ListOfServices.Services.BillingProfileId,//"152962080"//this.BillingAccountNo;SmeGroupId
      "BillingAccountNumber": this.customerInfo.ListOfServices.Services.AssetBillingAccountNo,
      "ServiceType": this.customerInfo.ListOfServices.Services.ServiceType
    }
    return new Promise((resolve, reject) => {
      this.CallAdapter(ConstantData.adapterUrls.GetTalkPostpaid, "POST", params).then(function (res) {
        console.log("************************************* component success FetchTalkPostpaid" + JSON.stringify(res));
        _that.dashboardService.SetTalkPostpaid(res);
        // _that.handleerror.FetchFreeUnitDetailsProcess(res,"FetchTalkPostpaid").then((result) => {
        //   if(result == true) {
        //     console.log("hai if FetchTalkPostpaid ",result);
        //     resolve(res);
        //   }
        //   else {
        //     console.log("hai else FetchTalkPostpaid ",result);
        //     reject("error : no data found.");
        //   }
        // });
        //this.handleError(res,"FetchTalkPostpaid");
        resolve(res);
      })
        .catch(function (e) {
          console.log("****************************************** component failure FetchPostpaidBillingInfo" + e);
          //alert("error user info FetchPostpaidBillingInfo ");
          reject(e);
        });
    });
  }

  FetchFreeUnitDetailsProcess() {
    //alert("fetch FetchFreeUnitDetailsProcess mobile number"+this.mobileNumber);
    var _that = this;
    var params = {
      "MOBILENUM": this.mobileNumber,//this.localStorage.get("MobileNum",""),
      "GroupID": this.customerInfo.ListOfServices.Services.SmeGroupId//"152962080"//this.BillingAccountNo;SmeGroupId
    }
    return new Promise((resolve, reject) => {
      if (this.customerInfo.ListOfServices.Services.SmeGroupId == "") {
        resolve();
      } else {
        this.CallAdapter(ConstantData.adapterUrls.GetFreeUnitDetailsProcess, "POST", params).then(function (res) {
          console.log("************************************* component success GetFreeUnitDetailsProcess" + JSON.stringify(res));
          _that.isCBS = true;
          _that.dashboardService.SetUnits(res);
          resolve(res);
        })
          .catch(function (e) {
            console.log("****************************************** component failure FetchPostpaidBillingInfo" + e);
            //alert("error user info FetchPostpaidBillingInfo ");
            reject(e);
          });
      }
    });
  }

  //common service for postpaid prepaid
  FetchAllDataPostpaidPrepaid() {
    //alert("fetch FetchAllDataPostpaidPrepaid mobile number"+this.mobileNumber);
    var _that = this;
    var params = {
      "MSISDN": this.mobileNumber//"60193522834"//this.localStorage.get("MobileNum","")
    }
    return new Promise((resolve, reject) => {
      this.CallAdapter(ConstantData.adapterUrls.GetAllDataPostpaidPrepaid, "POST", params).then(function (res) {
        console.log("********************************************** component success FetchAllDataPostpaidPrepaid" + JSON.stringify(res));
        _that.customerRetriveResponse
        _that.handleerror.handleErrorFetchAllDataPostpaidPrepaid(res, "FetchAllDataPostpaidPrepaid").then((result) => {
          if (result == true) {
            _that.dashboardService.SetAllDataPostpaidPrepaid(res);
            console.log("hai if FetchAllDataPostpaidPrepaid ", result);
            resolve(res);
          }
          else {
            console.log("hai else FetchAllDataPostpaidPrepaid ", result);
            reject("error : no data found.");
          }
        });

        // resolve(res);
      })
        .catch(function (e) {
          console.log("******************************************** component failure FetchAllDataPostpaidPrepaid" + e);
          //alert("error user info");
          reject(e);
        });
    });
  }



  //
  //common service for postpaid prepaid
  FetchRoamStatus() {
    //alert("fetch FetchRoamStatus mobile number"+this.mobileNumber);
    var _that = this;
    var params = {
      "MSISDN": this.mobileNumber//"60193522834"//this.localStorage.get("MobileNum","")
    }
    return new Promise((resolve, reject) => {
      this.CallAdapter(ConstantData.adapterUrls.GetRoamStatus, "POST", params).then(function (res) {
        console.log("********************************************** component success FetchRoamStatus" + JSON.stringify(res));
        _that.dashboardService.SetRoamStatus(res);

        // _that.handleerror.handleErrorFetchRoamStatus(res,"FetchRoamStatus").then((result) => {
        //   if(result == true) {
        //     console.log("hai if FetchRoamStatus ",result);
        //     resolve(res);
        //   }
        //   else {
        //     console.log("hai else FetchRoamStatus ",result);
        //     reject("error : no data found.");
        //   }
        // });

        resolve(res);
      })
        .catch(function (e) {
          console.log("******************************************** component failure FetchRoamStatus" + e);
          //alert("error user info");
          reject(e);
        });
    });
  }

  FetchRoamStatusPre() {
    //alert("GetVLR"+this.GetVLR());
    var _that = this;
    var params = {
      "MSISDN": this.mobileNumber,//"60193522834"//this.localStorage.get("MobileNum","")
      "VLR": this.GetVLR()
    }
    return new Promise((resolve, reject) => {
      this.CallAdapter(ConstantData.adapterUrls.GetRoamStatusPre, "POST", params).then(function (res) {
        console.log("********************************************** component success FetchRoamStatusPre" + JSON.stringify(res));
        _that.dashboardService.SetRoamStatusPre(res);
        //alert("component success FetchRoamStatusPre" + JSON.stringify(res));
        // _that.handleerror.handleErrorFetchRoamStatus(res,"FetchRoamStatusPre").then((result) => {
        //   if(result == true) {
        //     console.log("hai if FetchRoamStatusPre ",result);
        //     resolve(res);
        //   }
        //   else {
        //     console.log("hai else FetchRoamStatusPre ",result);
        //     reject("error : no data found.");
        //   }
        // });

        resolve(res);
      })
        .catch(function (e) {
          console.log("******************************************** component failure FetchRoamStatusPre" + e);
          //alert("error user info"+e);
          reject(e);
        });
    });
  }


  //Prepaid
  FetchOverAllPrepaidData() {
    //alert("fetch FetchOverAllPrepaidData mobile number"+this.mobileNumber);
    var _that = this;
    var params = {
      "MOBILENUM": this.mobileNumber//this.localStorage.get("MobileNum","")
    }
    return new Promise((resolve, reject) => {
      this.CallAdapter(ConstantData.adapterUrls.GetOverAllPrepaidData, "POST", params).then(function (res) {
        console.log("***************************************************** component success FetchOverAllPrepaidData" + JSON.stringify(res));
        _that.handleerror.handleErrorFetchOverAllPrepaidData(res, "FetchOverAllPrepaidData").then((result) => {
          if (result == true) {
            console.log("hai if FetchOverAllPrepaidData ", result);
            resolve(res);
          }
          else {
            console.log("hai else FetchOverAllPrepaidData ", result);
            reject("error : no data found FetchOverAllPrepaidData.");
          }
        });
        //resolve(res);
      })
        .catch(function (e) {
          console.log("****************************************** component failure FetchOverAllPrepaidData" + e);
          //alert("error user info");
          reject(e);
        });
    });
  }


  //Prepaid
  FetchBalancePrepaidData() {
    //alert("fetch FetchBalancePrepaidData mobile number"+this.mobileNumber);
    var _that = this;
    var params = {
      "MOBILENUM": this.mobileNumber//this.localStorage.get("MobileNum","")
    }
    return new Promise((resolve, reject) => {
      this.CallAdapter(ConstantData.adapterUrls.GetBalancePrepaidData, "POST", params).then(function (res) {
        console.log("************************************************** component success FetchBalancePrepaidData" + JSON.stringify(res));
        _that.handleerror.handleErrorFetchBalancePrepaidData(res, "FetchBalancePrepaidData").then((result) => {
          if (result == true) {
            console.log("hai if FetchBalancePrepaidData ", result);
            _that.dashboardService.SetBalancePrepaidData(res);
            resolve(res);
          }
          else {
            console.log("hai else FetchBalancePrepaidData ", result);
            reject("error : no data found FetchBalancePrepaidData.");
          }
        });
        //resolve(res);
      })
        .catch(function (e) {
          console.log("********************************************** component failure FetchBalancePrepaidData" + e);
          //alert("error user info FetchBalancePrepaidData");
          reject(e);
        });
    });
  }

  FetchStopAutoRenew(productID) {
    //alert("productID"+productID);
    var _that = this;
    var loader = _that.LoaderService();
    loader.present();
    var params = {
      "MSISDN": this.mobileNumber,//"60193522834"//this.localStorage.get("MobileNum","")
      "productID": productID
    }
    return new Promise((resolve, reject) => {
      this.CallAdapter(ConstantData.adapterUrls.GetStopAutorenew, "POST", params).then(function (res) {
        console.log("********************************************** component success GetStopAutorenew" + JSON.stringify(res));
        loader.dismiss();
        resolve(res);
      })
        .catch(function (e) {
          console.log("******************************************** component failure GetStopAutorenew" + e);
          loader.dismiss();
          reject(e);
        });
    });
  }

  //service for youthQRY
  CheckyouthQRY() {

    // var testMobilenumber = "60138371348";
    var params = {
      "MobileNumber": this.mobileNumber
    }

    try {

      return new Promise((resolve, reject) => {
        this.CallAdapter(ConstantData.adapterUrls.youthQRYKawKaw, "POST", params).then((res) => {
          console.log("*********************************** component  youthQRYKawKaw" + JSON.stringify(res));
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  failure youthQRYKawKaw" + e);
            //alert("error user info");
            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }



  }
  CheckyouthSUBs() {

    var params = {
      "MobileNumber": this.mobileNumber
    }

    try {

      return new Promise((resolve, reject) => {
        this.CallAdapter(ConstantData.adapterUrls.youthSubsKawKaw, "POST", params).then((res) => {
          console.log("*********************************** component  youthSubsKawKaw" + JSON.stringify(res));
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  failure youthSubsKawKaw" + e);
            //alert("error user info");
            reject(e);
          });
      });
    } catch (e) {
      //alert("error"+JSON.stringify(e));
    }



  }

  FetchDetailUsageTalkSMSInternet(category) {
    //alert("fetch FetchDetailUsageTalkSMSInternet mobile number"+this.mobileNumber);
    var _that = this;
    var params = {
      "MSISDN": this.BillingAccountNo,//this.localStorage.get("MobileNum","")
      "category": category
    }
    return new Promise((resolve, reject) => {
      this.CallAdapter(ConstantData.adapterUrls.DashboardDetailPostpaidUrl, "POST", params).then((res) => {
        console.log("dashboard detail roaming usage : " + JSON.stringify(res));
        _that.handleerror.handleErrorFetchDetailUsageTalkSMSInternet(res, category).then((result) => {
          if (result == true) {
            console.log("hai if FetchDetailUsageTalkSMSInternet ", result);
            if (category == "Roaming") {
              _that.detailedUsageService.SetDetailedUsageRoamingPostpaid(res);
            } else if (category == "Talk") {
              _that.detailedUsageService.SetDetailedUsageTalkPostpaid(res);
            } else if (category == "SMS") {
              _that.detailedUsageService.SetDetailedUsageSMSPostpaid(res);
            }
            resolve(res);
          }
          else {
            console.log("hai else FetchDetailUsageTalkSMSInternet ", result);
            reject("error : no data found FetchDetailUsageTalkSMSInternet.");
          }
        });
        // resolve(res);  
      })
        .catch(e => {
          console.log("component  failure FetchDetailUsageTalkSMSInternet " + e);
          reject(e);
        });
    });
  }


  FetchDetailedUsagePrepaid() {
    var _that = this;
    var PastDate = new Date(new Date().setDate(new Date().getDate() - 30));
    var myStartDate = new Date(PastDate);
    var startYear = myStartDate.getFullYear();
    var startMonth = (1 + myStartDate.getMonth()).toString();
    startMonth = startMonth.length > 1 ? startMonth : '0' + startMonth;
    var startDay = myStartDate.getDate().toString();
    startDay = startDay.length > 1 ? startDay : '0' + startDay;
    var startTimeStamp = startMonth + '/' + startDay + '/' + startYear + " " + "00:00:00";
    var year = startYear.toString();
    year = year.substring(2, 4);
    //this.startTime = startDay + '/' + startMonth + '/' + year;
    var myEndDate = new Date();
    //alert((mynewdate.getMonth() + 1) + '/' + mynewdate.getDate() + '/' +  mynewdate.getFullYear());
    var endYear = myEndDate.getFullYear();
    var endMonth = (1 + myEndDate.getMonth()).toString();
    endMonth = endMonth.length > 1 ? endMonth : '0' + endMonth;
    var endDay = myEndDate.getDate().toString();
    endDay = endDay.length > 1 ? endDay : '0' + endDay;
    var endTimeStamp = endMonth + '/' + endDay + '/' + endYear + " " + "23:59:59";
    this.endTime = endMonth + '/' + endDay + '/' + endYear;
    var params = {
      "MOBILENUM": this.mobileNumber,//this.localStorage.get("MobileNum","")
      "startTimestamp": startTimeStamp,
      "endTimestamp": endTimeStamp
    }
    return new Promise((resolve, reject) => {
      this.CallAdapter(ConstantData.adapterUrls.DashboardDetailPrepaidUrl, "POST", params).then(function (res) {
        console.log("************************************************** component success DashboardDetailPrepaidUrl" + JSON.stringify(res));
        _that.detailedUsageService.SetDetailedUsagePrepaid(res);
        resolve(res);
      })
        .catch(function (e) {
          console.log("********************************************** component failure DashboardDetailPrepaidUrl" + e);
          //alert("error user info");
          reject(e);
        });
    });
  }

  getUsageSince() {
    var _that = this;
    var PastDate = new Date(new Date().setDate(new Date().getDate() - 30));
    var myStartDate = new Date(PastDate);
    var startYear = myStartDate.getFullYear();
    var startMonth = (1 + myStartDate.getMonth()).toString();
    startMonth = startMonth.length > 1 ? startMonth : '0' + startMonth;
    var startDay = myStartDate.getDate().toString();
    startDay = startDay.length > 1 ? startDay : '0' + startDay;
    var startTimeStamp = startMonth + '/' + startDay + '/' + startYear + " " + "00:00:00";
    var year = startYear.toString();
    year = year.substring(2, 4);
    this.startTime = startDay + '/' + startMonth + '/' + year;
    return this.startTime;
  }

  ReloadDashboard1() {
    //alert("Reload Dashboard");
    var _that = this;

    return new Promise((resolve, reject) => {
      //var loader = _that.LoaderService();
      this.loader.present();
      _that.FetchUserInfo().then((res) => {
        _that.dashboardService.SetUserInfo(res);
        var userInfo = _that.dashboardService.GetUserInfo();
        _that.customerInfo = userInfo;
        _that.SetXPAX(userInfo.ListOfServices.Services.ServiceType);
        _that.BillingAccountNo = userInfo.ListOfServices.Services.AssetBillingAccountNo;
        if (ConstantData.isDashboardBypassed == false) {
          //_that.FetchVLRDetails().then((responseVLRDetails) => {
          //alert("FetchVLRDetails"+JSON.stringify(responseVLRDetails));
          //_that.SetVLRDetailsData(responseVLRDetails);
          if (userInfo.ListOfServices.Services.ServiceType == "Postpaid" || userInfo.ListOfServices.Services.ServiceType == "CNVRGTPostpaid") {
            //alert("inside Postpaid "+userInfo.ListOfServices.Services.ServiceType);
            _that.FetchPostpaidBillingInfo().then((responseBillingInfo) => {
              _that.dashboardService.SetPostpaidBillingInfo(responseBillingInfo);
              //strat checking for cap zone and call bar

              //console.log(responseBillingInfo);
              var postpaidBillingInfo = _that.dashboardService.GetPostpaidBillingInfo();
              var callBarDetails = postpaidBillingInfo.CallBarDetails;
              //callBarDetails.ElementId = "10005";
              //if user has entered capzone
              console.log('Element ID ' + callBarDetails.ElementId);
              if (callBarDetails.ElementId == "45760") {
                console.log('cap zone element id' + callBarDetails.ElementId);
                _that.globalVar.setCapZoneStatus(true);
                if (_that.globalVar.getcallBarredStatus()) {
                  _that.globalVar.setcallBarredStatus(!_that.globalVar.getcallBarredStatus());
                }
              }
              else {
                _that.globalVar.setCapZoneStatus(false);
              }
              //if user has entered call bar or manual call bar(10006)
              if (callBarDetails.ElementId == "10004" || callBarDetails.ElementId == "10005" || callBarDetails.ElementId == "10006") {
                console.log('cap zone element id' + callBarDetails.ElementId);
                _that.globalVar.setcallBarredStatus(true);
                if (_that.globalVar.getCapZoneStatus()) {
                  _that.globalVar.setCapZoneStatus(!_that.globalVar.getCapZoneStatus());
                }
              }
              else {
                _that.globalVar.setcallBarredStatus(false);
              }
              // if (page.title == "Toggle CAP Zone") {
              //   this.globalVar.setCapZoneStatus(!this.globalVar.getCapZoneStatus());
              //   if (this.globalVar.getcallBarredStatus()) {
              //     this.globalVar.setcallBarredStatus(!this.globalVar.getcallBarredStatus());
              //   }
              // }


              // if (page.title == "Toggle CAll Barred") {
              //   this.globalVar.setcallBarredStatus(!this.globalVar.getcallBarredStatus());
              //   if (this.globalVar.getCapZoneStatus()) {
              //     this.globalVar.setCapZoneStatus(!this.globalVar.getCapZoneStatus());
              //   }
              // }






              //end checking for cap zone and call bar

              _that.FetchAllDataPostpaidPrepaid().then((responsePostpaidPrepaid) => {
                _that.dashboardService.SetAllDataPostpaidPrepaid(responsePostpaidPrepaid);
                _that.FetchDetailUsageTalkSMSInternet("Roaming").then((responseRoaming) => {
                  console.log("*************************************** roaming response" + JSON.stringify(responseRoaming));
                  _that.detailedUsageService.SetDetailedUsageRoamingPostpaid(responseRoaming);
                  _that.FetchDetailUsageTalkSMSInternet("Talk").then((responseTalk) => {
                    _that.detailedUsageService.SetDetailedUsageTalkPostpaid(responseTalk);
                    _that.FetchDetailUsageTalkSMSInternet("SMS").then((responseSMS) => {
                      _that.detailedUsageService.SetDetailedUsageSMSPostpaid(responseSMS);
                      _that.FetchTalkPostpaid().then((responseTalkPostpaid) => {
                        _that.dashboardService.SetTalkPostpaid(responseTalkPostpaid);
                        _that.FetchRoamStatus().then((responseRoamStatus) => {
                          _that.dashboardService.SetRoamStatus(responseRoamStatus);
                          var groupId = this.customerInfo.ListOfServices.Services.SmeGroupId;
                          //alert("groupId"+groupId);
                          if (groupId == "") {
                            this.loader.dismiss();
                            resolve();
                          } else {
                            _that.FetchFreeUnitDetailsProcess().then((responseFreeUnitDetailsProcess) => {
                              //alert("Inside free");                               
                              this.loader.dismiss();
                              _that.isCBS = true;
                              _that.dashboardService.SetUnits(responseFreeUnitDetailsProcess);
                              resolve();
                            }).catch(err => {
                              //alert("FetchFreeUnitDetailsProcess error"+JSON.stringify(err));//FetchFreeUnitDetailsProcess
                              this.loader.dismiss();
                              reject(err);
                            });
                          }
                        }).catch(err => {
                          //alert("FetchRoamStatus error"+JSON.stringify(err));//RoamStatus
                          this.loader.dismiss();
                          reject(err);
                        });
                        // this.loader.dismiss();
                        // resolve();
                      }).catch(err => {
                        //alert("FetchTalkPostpaid error"+JSON.stringify(err));//FetchTalkPostpaid
                        this.loader.dismiss();
                        reject(err);
                      });
                    }).catch(err => {
                      //alert("FetchDetailUsageSMS error"+JSON.stringify(err));//DetailedUsageSMSPostpaid
                      this.loader.dismiss();
                      reject(err);
                    });
                  }).catch(err => {
                    //alert("FetchDetailUsageTalk error"+JSON.stringify(err));//DetailedUsageTalkPostpaid
                    this.loader.dismiss();
                    reject(err);
                  });
                }).catch(err => {
                  //alert("FetchDetailUsageInternet error"+JSON.stringify(err));//DetailedUsageInternetPostpaid
                  this.loader.dismiss();
                  reject(err);
                });
              }).catch(err => {
                //alert("FetchAllDataPostpaidPrepaid error"+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
                this.loader.dismiss();
                reject(err);
              });
            }).catch(err => {
              //alert("FetchPostpaidBillingInfo error"+JSON.stringify(err));//FetchPostpaidBillingInfo
              this.loader.dismiss();
              reject(err);
            });
          } else {
            //alert("Inside Prepaid "+userInfo.ListOfServices.Services.ServiceType);
            //to disable capzone and call bar
            _that.globalVar.setCapZoneStatus(false);
            _that.globalVar.setcallBarredStatus(false);
            _that.FetchAllDataPostpaidPrepaid().then((responsePostpaidPrepaid) => {
              _that.dashboardService.SetAllDataPostpaidPrepaid(responsePostpaidPrepaid);
              _that.FetchBalancePrepaidData().then((responseBalancePrepaid) => {
                _that.dashboardService.SetBalancePrepaidData(responseBalancePrepaid);
                _that.FetchDetailedUsagePrepaid().then((responseDetailUsagePrepaid) => {
                  _that.detailedUsageService.SetDetailedUsagePrepaid(responseDetailUsagePrepaid);
                  //  if(_that.GetVLR()){
                  //   _that.FetchRoamStatusPre().then((responseRoamStatusPre)=> { 
                  //     alert(JSON.stringify("RoamStatusPre Response"+responseRoamStatusPre));
                  //     this.loader.dismiss();
                  //     resolve();
                  //   }).catch(err => {
                  //       //alert("FetchRoamStatusPre error"+JSON.stringify(err));//RoamStatusPre
                  //       this.loader.dismiss();
                  //       reject(err);
                  //   }); 
                  // }else{  
                  this.loader.dismiss();
                  resolve();
                  // }
                }).catch(err => {
                  //alert("FetchBalancePrepaidData error"+JSON.stringify(err));
                  this.loader.dismiss();
                  reject(err);//FetchAllDataPostpaidPrepaid
                });
              }).catch(err => {
                //alert("FetchBalancePrepaidData error"+JSON.stringify(err));
                this.loader.dismiss();
                reject(err);//FetchAllDataPostpaidPrepaid
              });
            }).catch(err => {
              //alert("FetchAllDataPostpaidPrepaid error"+JSON.stringify(err));
              this.loader.dismiss();
              reject(err);//FetchAllDataPostpaidPrepaid
            });
          }
          // }).catch(err => {
          //   //alert("FetchVLRDetails error"+JSON.stringify(err));
          //   this.loader.dismiss();
          //   reject(err);//FetchVLRDetails
          // });
        } else {
          //alert("Dashboard is bypassed");
          this.loader.dismiss();
          resolve();
        }
      }).catch(err => {
        //alert("error FetchUserInfo"+JSON.stringify(err));
        this.loader.dismiss();
        reject(err);//FetchUserInfo
      });

    });
  }

  ReloadDashboard() {
    //alert("Reload Dashboard");
    this.loader.present();
    var _that = this;
    return new Promise((resolve, reject) => {
      Promise.all([
        _that.FetchUserInfo().then((res) => {
          _that.dashboardService.SetUserInfo(res);
          var userInfo = _that.dashboardService.GetUserInfo();
          _that.customerInfo = userInfo;
          _that.SetXPAX(userInfo.ListOfServices.Services.ServiceType);
          _that.BillingAccountNo = userInfo.ListOfServices.Services.AssetBillingAccountNo;
          if (userInfo.ListOfServices.Services.ServiceType == "Postpaid" || userInfo.ListOfServices.Services.ServiceType == "CNVRGTPostpaid") {
            Promise.all([
              _that.FetchPostpaidBillingInfo(),
              _that.FetchDetailUsageTalkSMSInternet("Roaming"),
              _that.FetchDetailUsageTalkSMSInternet("Talk"),
              _that.FetchDetailUsageTalkSMSInternet("SMS"),
              _that.FetchTalkPostpaid(),
              _that.FetchRoamStatus(),
              _that.FetchFreeUnitDetailsProcess(),
              //_that.FeachAllSubMObileNumber(),
              _that.FetchVLRDetails()
            ]).then(value => {
              //alert("user info resolve");
            }).catch(error => {
              _that.loader.dismiss();
              reject(error);
            })
          } else {
            _that.globalVar.setCapZoneStatus(false);
            _that.globalVar.setcallBarredStatus(false);
            Promise.all([
              _that.FetchBalancePrepaidData(),
              _that.FetchDetailedUsagePrepaid(),
              //_that.FeachAllSubMObileNumber(),
              //_that.FetchVLRDetails(),
              _that.FetchVLRDetails().then((responseVLRDetails) => {

                //alert("FetchVLRDetails"+JSON.stringify(responseVLRDetails));
                _that.SetVLRDetailsData(responseVLRDetails);
                try {
                  var vlr = _that.GetVLR();
                  if (vlr) {
                    _that.FetchRoamStatusPre();
                  }
                } catch (e) {
                  //alert('e'+e);
                }
              }).catch(error => {
                reject(error);
              })
            ]).then(value => {
              //alert("user info resolve");
            }).catch(error => {
              _that.loader.dismiss();
              reject(error);
            })
          }
        }),
        _that.FeachAllSubMObileNumber(),
        _that.FetchAllDataPostpaidPrepaid()
        //_that.FetchBalancePrepaidData(),
        //_that.FetchDetailedUsagePrepaid()
      ]).then(value => {
        //alert("in resolve");
        _that.loader.dismiss();
        _that.dashboardService.SetIsReloadDashboardCalled(true);
        resolve();
      }).catch(error => {
        _that.loader.dismiss();
        reject(error);
      })
      // }).catch(err => {
      //   //alert("error FetchUserInfo"+JSON.stringify(err));
      //   //this.loader.dismiss();
      //   reject(err);//FetchUserInfo
      // });
      // }).catch(err => {
      //   //alert("error FetchUserInfo"+JSON.stringify(err));
      //   //this.loader.dismiss();
      //   reject(err);//FetchUserInfo
      // });
    })
    // Promise.all([
    //   _that.FetchUserInfo(),
    //   _that.FetchAllDataPostpaidPrepaid(),
    //   _that.FetchBalancePrepaidData(),
    //   _that.FetchDetailedUsagePrepaid()
    // ]).then(value => {


    //})


  }


  // ReloadDashboard() {
  //   //alert("Reload Dashboard");
  //   this.loader.present();
  //   var _that = this;
  //   return new Promise((resolve, reject) => {
  //     //Promise.all([
  //       _that.FetchUserInfo().then((res) => {
  //         _that.dashboardService.SetUserInfo(res);
  //         var userInfo = _that.dashboardService.GetUserInfo();
  //         _that.customerInfo = userInfo;
  //         _that.SetXPAX(userInfo.ListOfServices.Services.ServiceType);
  //         _that.BillingAccountNo = userInfo.ListOfServices.Services.AssetBillingAccountNo;
  //         if (userInfo.ListOfServices.Services.ServiceType == "Postpaid" || userInfo.ListOfServices.Services.ServiceType == "CNVRGTPostpaid") {
  //           Promise.all([
  //             _that.FetchPostpaidBillingInfo(),
  //             _that.FetchAllDataPostpaidPrepaid(),
  //             _that.FetchDetailUsageTalkSMSInternet("Roaming"),
  //             _that.FetchDetailUsageTalkSMSInternet("Talk"),
  //             _that.FetchDetailUsageTalkSMSInternet("SMS"),
  //             _that.FetchTalkPostpaid(),
  //             _that.FetchRoamStatus(),
  //             _that.FetchFreeUnitDetailsProcess(),
  //             _that.FeachAllSubMObileNumber(),
  //             _that.FetchVLRDetails()
  //           ]).then(value => {
  //                alert("user info resolve");
  //               _that.loader.dismiss();
  //               _that.dashboardService.SetIsReloadDashboardCalled(true);
  //               resolve();
  //           }).catch(error => {
  //             alert("in catch");
  //             _that.loader.dismiss();
  //             reject(error);
  //           })
  //         } else {
  //           _that.globalVar.setCapZoneStatus(false);
  //           _that.globalVar.setcallBarredStatus(false);
  //           Promise.all([
  //             _that.FetchBalancePrepaidData(),
  //             _that.FetchDetailedUsagePrepaid(),
  //             _that.FeachAllSubMObileNumber(),
  //             _that.FetchAllDataPostpaidPrepaid(),
  //             //_that.FetchVLRDetails(),
  //             _that.FetchVLRDetails().then((responseVLRDetails) => {

  //               //alert("FetchVLRDetails"+JSON.stringify(responseVLRDetails));               
  //               try {
  //                 var vlr = _that.GetVLR();
  //                 _that.SetVLRDetailsData(responseVLRDetails);
  //                 if (vlr) {
  //                   _that.FetchRoamStatusPre();
  //                 }
  //               } catch (e) {
  //                 alert('prepaid catch'+e);
  //               }
  //             }).catch(error => {
  //               reject(error);
  //             })
  //           ]).then(value => {
  //               alert("user info resolve");
  //               _that.loader.dismiss();
  //               _that.dashboardService.SetIsReloadDashboardCalled(true);
  //               resolve();
  //           }).catch(error => {
  //             alert("inner catch");
  //             _that.loader.dismiss();
  //             reject(error);
  //           })
  //         }
  //       }).catch(error => {
  //       alert("user info catch");
  //       _that.loader.dismiss();
  //       reject(error);
  //     });

  //       //_that.FeachAllSubMObileNumber()
  //       //_that.FetchBalancePrepaidData(),
  //       //_that.FetchDetailedUsagePrepaid()
  //     // ]).then(value => {
  //     //   alert("user info resolve");
  //     //   _that.loader.dismiss();
  //     //   _that.dashboardService.SetIsReloadDashboardCalled(true);
  //     //   resolve();
  //     // }).catch(error => {
  //     //   alert("user info catch");
  //     //   _that.loader.dismiss();
  //     //   reject(error);
  //     // })
  //     // }).catch(err => {
  //     //   //alert("error FetchUserInfo"+JSON.stringify(err));
  //     //   //this.loader.dismiss();
  //     //   reject(err);//FetchUserInfo
  //     // });
  //     // }).catch(err => {
  //     //   //alert("error FetchUserInfo"+JSON.stringify(err));
  //     //   //this.loader.dismiss();
  //     //   reject(err);//FetchUserInfo
  //     // });
  //   });
  //   // Promise.all([
  //   //   _that.FetchUserInfo(),
  //   //   _that.FetchAllDataPostpaidPrepaid(),
  //   //   _that.FetchBalancePrepaidData(),
  //   //   _that.FetchDetailedUsagePrepaid()
  //   // ]).then(value => {


  //   //})


  // }
  presentAlert(data) {
    const alert = this.alertCtrl.create({
      title: data.title,
      subTitle: data.subtitle,
      message: data.message,
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'submit-button',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Call to Action',
        cssClass: 'submit-button',
        handler: () => {
          console.log('Call to Action Clicked');
        }
      }],
      cssClass: 'success-message error-message'
    });
    alert.present();
  }

  reset() {
    this.isCBS = false;
  }

  getFlagForDashboardReload() {
    return this.flagForDashboardReload;
  }

  setFlagForDashboardReload(obj) {
    this.flagForDashboardReload = obj;
  }



  PromiseToPayCall(params) {
    var _that = this;

    try {

      return new Promise((resolve, reject) => {
        this.CallAdapter(ConstantData.adapterUrls.PromiseToPay, "POST", params).then(function (res) {
          console.log("*********************************** component success PromiseToPayCall" + JSON.stringify(res));
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  failure PromiseToPayCall" + e);


            reject(e);
          });
      });
    } catch (e) {

    }
  }

  updateCreditLimit(params) {
    var _that = this;

    try {

      return new Promise((resolve, reject) => {
        this.CallAdapter(ConstantData.adapterUrls.updatePTPProfile, "POST", params).then(function (res) {
          console.log("*********************************** component success updateCreditLimit" + JSON.stringify(res));
          resolve(res);
        })
          .catch(function (e) {
            console.log("******************************* component  failure updateCreditLimit" + e);


            reject(e);
          });
      });
    } catch (e) {

    }
  }

  GetPrivilegeData() {
    return this.user_priv_data;
  }

  SetPrivilegeData(value) {
    this.user_priv_data = value;
  }

  //function to handle keyup event on the text boxes to restrict numbers only
  keyUpCheckerNumberOnly(ev, keyborad) {
    let elementChecker: string;
    let format = /[a-z]|[A-Z]|#|!|@|\$|%|\^|\&|\*|\)|\(|\+|=|\.|_|-|;|:|\"|\'|\/|\\|\s|\,/g;
    elementChecker = ev.target.value;
    if (elementChecker.match(format) && elementChecker.match(format).length > 0) {
      ev.target.value = elementChecker.replace(format, '');
    }
    this.handleKeyDone(ev, keyborad);
    return ev.target.value;
  }
  keyUpCheckOtpOnlys(ev, keyborad) {
    let elementChecker: string;
    let format = /[a-z]|[A-Z]|#|!|@|\$|%|\^|\&|\*|\)|\(|\+|=|\.|_|-|;|:|\"|\'|\/|\\|\s|\,/g;
    if (ev.target == undefined) {
      elementChecker = ev.value
    } else {
      elementChecker = ev.target.value
    }

    if (elementChecker.match(format) && elementChecker.match(format).length > 0) {

      if (ev.target == undefined) {
        ev.value = elementChecker.replace(format, '');
      } else {
        ev.target.value = elementChecker.replace(format, '');
      }




    }
    if (elementChecker.length > 1) {

      if (ev.target == undefined) {
        ev.value = elementChecker.substring(0, 1);
      } else {
        ev.target.value = elementChecker.substring(0, 1);
      }




    }
    this.handleKeyDone(ev, keyborad);
    return ev.target.value;
  }
  //handle the keyborad event
  handleKeyDone(e, keyborad) {
    if (e.which === 13) {
      keyborad.close();
    }
  }

  setFlagForLifestylePageReload(value) {
    this.isPriviledgeReload = value;
  }
  getFlagForLifestylePageReload() {
    return this.isPriviledgeReload;
  }

  setPTPEligibilityData(value) {
    this.PTPEligibilityData = value;
  }
  getPTPEligibilityData() {
    return this.PTPEligibilityData
  }


  setBasePlanForSubscription(value) {
    this.basePlanForSubscription = value;
  }
  getBasePlanForSubscription() {
    return this.basePlanForSubscription;
  }

  getAnalyticProfileSegment() {
    console.log("XPAX", this.XPAX);
    if (this.XPAX == "Prepaid") {
      return "Prepaid";
    } else  if (this.XPAX == "Postpaid") {
      return "Postpaid";
    } else {
      return "Xpax Postpaid";
    }
  }

  getAnalyticTransactionJourneyName(){
    if(this.isPrePaid()){
      return "Prepaid reload";
    }else{
      return "Postpaid bill payment";
    }
  }

  setAnalyticPageName(pageName, renderer){
    this._analyticsService.pageName = pageName;
    this.updateAnalyticJSON(renderer);
  }

  setAnalyticPageNameProfileSegment(pageName, renderer){
    this._analyticsService.profileSegement = this.getAnalyticProfileSegment();
    this.setAnalyticPageName(pageName, renderer);
  }

  /* 
  TransactMethod Values: 
        "Credit card", 
        "Online banking",  
        "PIN" 
  */
  /*
    JouneryDepth Values: 
        "initiation", // pre-checkout pages
        "checkout",  // all pg pages / checkout pages
        "post" // after checkout / confirmation / leave out
  */
 /* 
    TransactProduct Values: 
        "Credit reload",
        "Internet reload",  
        "Reload PIN" 
  */
 /* 
    TransactResult Values: 
        "success",
        "failed"
 */

  setAnalyticTransactionJourney(pageName, renderer, transactionDetails){
    this._analyticsService.journeyName = this.getAnalyticTransactionJourneyName();
    this._analyticsService.jouneryDepth = transactionDetails.jouneryDepth;
    this._analyticsService.transactProduct = transactionDetails.transactProduct;
    this._analyticsService.transactValue = transactionDetails.transactValue;
    this._analyticsService.transactMethod = transactionDetails.transactMethod;
    this._analyticsService.transactResult = transactionDetails.transactResult;
    this.setAnalyticPageNameProfileSegment(pageName, renderer);
  }

  updateAnalyticTransactionJourney(renderer, transactionDetails){
    this._analyticsService.journeyName = this.getAnalyticTransactionJourneyName();
    this._analyticsService.jouneryDepth = transactionDetails.jouneryDepth;
    this._analyticsService.transactProduct = transactionDetails.transactProduct;
    this._analyticsService.transactValue = transactionDetails.transactValue;
    this._analyticsService.transactMethod = transactionDetails.transactMethod;
    this._analyticsService.transactResult = transactionDetails.transactResult;
    this.updateAnalyticJSON(renderer);
  }
  
  resetAllAnalyticData(){
    this._analyticsService.resetAllAnalyticData();
  }

  updateAnalyticJSON(renderer){
    (<any>window).contextData = this._analyticsService.getAvailableAnalyticsData();
  
            var result = this._analyticsService.getAvailableAnalyticsData();
             console.log("*****result*******" + JSON.stringify(result));
            //remove the old json data and add the new page level details
            this._rendererService.updateDataLayerScript(document, renderer, result);
            _satellite.pageBottom();
  }

}


