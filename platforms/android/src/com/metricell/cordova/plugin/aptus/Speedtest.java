package com.metricell.cordova.plugin.aptus;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.telephony.TelephonyManager;
import android.view.WindowManager;

import com.metricell.mcc.api.DataCollector;
import com.metricell.mcc.api.DataCollectorStrings;
import com.metricell.mcc.api.DataCollectorSystemListener;
import com.metricell.mcc.api.queue.EventQueue;
import com.metricell.mcc.api.scriptprocessor.tasks.TestResult;
import com.metricell.mcc.api.scriptprocessor.tasks.download.DownloadTestResult;
import com.metricell.mcc.api.scriptprocessor.tasks.ping.PingTestResult;
import com.metricell.mcc.api.scriptprocessor.tasks.upload.UploadTestResult;
import com.metricell.mcc.api.speedtest.SpeedTestManager;
import com.metricell.mcc.api.speedtest.SpeedTestManagerListerner2;
import com.metricell.mcc.api.speedtest.SpeedTestStats;
import com.metricell.mcc.api.tools.MetricellLocationManager;
import com.metricell.mcc.api.tools.MetricellLocationManagerListener;
import com.metricell.mcc.api.tools.MetricellLogger;
import com.metricell.mcc.api.tools.MetricellNetworkTools;
import com.metricell.mcc.api.tools.MetricellTools;
import com.metricell.mcc.api.types.DataCollection;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class Speedtest implements MetricellLocationManagerListener, SpeedTestManagerListerner2 {

	private static final int TEST_TIMEOUT = 15000;

    private Context mContext;
	private WeakReference<Activity> mActivity;
	private CallbackContext mSpeedtestCallbackContext = null;

	private SpeedTestManager mSpeedTestManager = null;
	private MetricellLocationManager mBackgroundLocationManager;
	private DataCollector mDataCollector;
	private DataCollectorSystemListener mDataCollectorSystemListener;
	
	private boolean mSpeedTestCancelled = false;
	
	private long mDownloadSpeedAvgFinal = 0;
	private long mDownloadSpeedMaxFinal = 0;
	private long mDownloadSize = 0;
	private long mDownloadDuration = 0;
	private int mDownloadNetworkType = 0;
	private boolean mDownloadMultithreaded = false;
	
	private long mUploadSpeedAvgFinal = 0;
	private long mUploadSpeedMaxFinal = 0;
	private long mUploadSize = 0;
	private long mUploadDuration = 0;

	private long mPingSpeedFinal = 0;
	private long mPingJitter = 0;
	
	private String mUploadUrl;
	private String mDownloadUrl;
	private String mPingUrl;
	
	private String mDownloadErrorCode = SpeedTestManager.ERROR_CODES[SpeedTestManager.NOT_EXECUTED];
	private String mUploadErrorCode = SpeedTestManager.ERROR_CODES[SpeedTestManager.NOT_EXECUTED];
	private String mPingErrorCode = SpeedTestManager.ERROR_CODES[SpeedTestManager.NOT_EXECUTED];
	
	private JSONArray mDownloadSamplePoints = null;
	private JSONArray mUploadSamplePoints = null;
	
	public Speedtest(Context context) {
        mContext = context;

		mDataCollector = new DataCollector(context);
		mDataCollectorSystemListener = new DataCollectorSystemListener(context, mDataCollector);
		mBackgroundLocationManager = new MetricellLocationManager(context);
		mBackgroundLocationManager.setListener(this);
	}

	/*
     * This function starts a speedtest
     */
	void startSpeedtest(Activity activity, CallbackContext callback) {
		mSpeedtestCallbackContext = callback;
        mActivity = new WeakReference<Activity>(activity);

        // Set the KEEP_SCREEN_ON flag to keep the screen on whilst the speedtest is in progress
        activity.runOnUiThread(new Runnable() {
            public void run() {
                Activity a = mActivity.get();
                if (a != null) {
                    a.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
            }
        });

        if (MetricellNetworkTools.hasDataConnection(activity)) {

			if (mSpeedTestManager == null || !mSpeedTestManager.isSpeedTestRunning()) {
				mDataCollectorSystemListener.init();
				enableBackgroundLocationRefresh();

				mSpeedTestCancelled = false;
			
				mDownloadErrorCode = SpeedTestManager.ERROR_CODES[SpeedTestManager.NOT_EXECUTED];
				mUploadErrorCode = SpeedTestManager.ERROR_CODES[SpeedTestManager.NOT_EXECUTED];
				mPingErrorCode = SpeedTestManager.ERROR_CODES[SpeedTestManager.NOT_EXECUTED];
					
				mDownloadSpeedAvgFinal = mDownloadSpeedMaxFinal = 0;
				mUploadSpeedAvgFinal = mUploadSpeedMaxFinal = 0;
				mPingSpeedFinal = 0;			

				mDownloadSize = mUploadSize = 0;
				mDownloadDuration = mUploadDuration = 0;
				mDownloadSamplePoints = null;
				mUploadSamplePoints = null;
				
				mDownloadMultithreaded = false;

				mUploadUrl = mDownloadUrl = mPingUrl = null;
				
				// Create a new SpeedTestManager object
				mSpeedTestManager = new SpeedTestManager(activity, this, mDataCollector);

				// Start a speedtest with the specified timeout
				mSpeedTestManager.startSpeedTest(TEST_TIMEOUT);

                try {
                    JSONObject o = new JSONObject();
                    o.put("status", "init");

                    PluginResult result = new PluginResult(PluginResult.Status.OK, o.toString());
                    result.setKeepCallback(true);
                    mSpeedtestCallbackContext.sendPluginResult(result);
                } catch (Exception e) {
					MetricellLogger.getInstance().logException(Aptus.TAG, e);
				}

			} else {

                try {
                    JSONObject o = new JSONObject();
                    o.put("status", "already_running");

                    PluginResult result = new PluginResult(PluginResult.Status.ERROR, o.toString());
                    mSpeedtestCallbackContext.sendPluginResult(result);
                } catch (Exception e) {
					MetricellLogger.getInstance().logException(Aptus.TAG, e);
				}

			}

		} else {

            try {
                JSONObject o = new JSONObject();
                o.put("status", "no_data_connection");

                PluginResult result = new PluginResult(PluginResult.Status.ERROR, o.toString());
                mSpeedtestCallbackContext.sendPluginResult(result);
            } catch (Exception e) {
				MetricellLogger.getInstance().logException(Aptus.TAG, e);
			}

		}
	}

	/*
	 * This function cancels the current speedtest
	 */
	void cancelSpeedtest() {

		mSpeedTestCancelled = true;
	
        // Clear the KEEP_SCREEN_ON flag to let the screen go to sleep
        Activity a = mActivity.get();
        if (a != null) {
            a.runOnUiThread(new Runnable() {
                public void run() {
                    Activity a = mActivity.get();
                    if (a != null) {
                        a.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    }
                }
            });
        }

        if (mSpeedTestManager != null) {
			mSpeedTestManager.stopSpeedTest();

			mDataCollectorSystemListener.shutdown();
			disableBackgroundLocationRefresh();
		}
	}

	/*
     * This function enables the background location listener
     */
	private void enableBackgroundLocationRefresh() {
		if (mBackgroundLocationManager != null) mBackgroundLocationManager.refreshLocation(30000, MetricellLocationManager.LOCATION_REFRESH_GPS|MetricellLocationManager.LOCATION_REFRESH_NETWORK);
	}

	/*
	 * This function disables the background location listener
	 */
	private void disableBackgroundLocationRefresh() {
		if (mBackgroundLocationManager != null) mBackgroundLocationManager.stopLocationRefresh();
	}

	/*
	 * MetricellLocationManagerListener
	 * This listener receives location call backs which are then passed onto the background service to update the location.
	 */
	public void locationManagerLocationUpdated(MetricellLocationManager manager, Location l) {
		// Inform the background service of our new location
		if (mDataCollector != null) mDataCollector.callbackLocationChanged(l);
	}

	@Override
	public void locationManagerTimedOut(MetricellLocationManager manager) {
	}

	@Override
	public void locationManagerProviderStateChanged(MetricellLocationManager manager, String provider, boolean enabled) {
		enableBackgroundLocationRefresh();		// refresh location providers
	}
	
	/**
	 *
	 * SpeedTestManagerListener implementation
	 *
	 * */

	@Override
	public void setupStarted(String url) {
	}

	@Override
	public void setupFinished(String wifiDlUrl, String wifiUlUrl,
							  String mobileDlUrl, String mobileUlUrl, String mobilePingUrl,
							  String wifiPingUrl) {
        try {
			mUploadUrl = mobileUlUrl;
			mDownloadUrl = mobileDlUrl;
			mPingUrl = mobilePingUrl;
			
            JSONObject o = new JSONObject();
            o.put("status", "started");

            PluginResult result = new PluginResult(PluginResult.Status.OK, o.toString());
            result.setKeepCallback(true);
            mSpeedtestCallbackContext.sendPluginResult(result);
        } catch (Exception e) {
			MetricellTools.logException(Aptus.TAG, e);
		}
	}

	/*
	 * Callback for when the download portion of the test starts
	 */
	@Override
	public void downloadTestStarted(String url) {
        try {
			mDownloadErrorCode = SpeedTestManager.ERROR_CODES[SpeedTestManager.NO_ERROR];
			
            JSONObject o = new JSONObject();
            o.put("status", "download_started");
            o.put("url", url);

            PluginResult result = new PluginResult(PluginResult.Status.OK, o.toString());
            result.setKeepCallback(true);
            mSpeedtestCallbackContext.sendPluginResult(result);
        } catch (Exception e) {
			MetricellLogger.getInstance().logException(Aptus.TAG, e);
		}
	}

	/*
	 * Callback for when the download progress updates
	 */
	@Override
	public void downloadTestUpdate(SpeedTestStats dlStats) {
        try {
			mDownloadNetworkType = dlStats.technologyType;
			
			long speed, speedMax;
			
			// 2G
			if (dlStats.technologyType == TelephonyManager.NETWORK_TYPE_EDGE || dlStats.technologyType == TelephonyManager.NETWORK_TYPE_GPRS) {
				double basicSpeed = 0;
				if (dlStats.duration >= 1000) {
					basicSpeed = (double)dlStats.size / ((double)dlStats.duration/1000);
				}

				speed = (long)basicSpeed;
				speedMax = speed;
				
			// All other data types
			} else {
				speed = dlStats.speedAvg;
				speedMax = dlStats.speedMax;
			}
			
            JSONObject o = new JSONObject();
            o.put("status", "download_progress");
            o.put("elapsed", dlStats.duration);
            o.put("transferred", dlStats.size);
            o.put("speed", speed);
			o.put("speed_max", speedMax);
            o.put("url", dlStats.url);

            PluginResult result = new PluginResult(PluginResult.Status.OK, o.toString());
            result.setKeepCallback(true);
            mSpeedtestCallbackContext.sendPluginResult(result);
        } catch (Exception e) {
			MetricellLogger.getInstance().logException(Aptus.TAG, e);
		}
	}

	/*
	 * Callback for when the download portion finishes, returns the following JSON to the registered callback
	 */
	@Override
	public void downloadTestFinished(SpeedTestStats dlStats) {
        try {

			// 2G
			if (dlStats.technologyType == TelephonyManager.NETWORK_TYPE_EDGE || dlStats.technologyType == TelephonyManager.NETWORK_TYPE_GPRS) {
				double basicSpeed = 0;
				if (dlStats.duration >= 1000) {
					basicSpeed = (double)dlStats.size / ((double)dlStats.duration/1000);
				}
				
				mDownloadSpeedAvgFinal = (long)basicSpeed;
				mDownloadSpeedMaxFinal = (long)basicSpeed;
				
			// All other data types
			} else {
				
				mDownloadSpeedAvgFinal = dlStats.speedAvg;
				mDownloadSpeedMaxFinal = dlStats.speedMax;
			}
				
			mDownloadDuration = dlStats.duration;
			mDownloadSize = dlStats.size;
			mDownloadNetworkType = dlStats.technologyType;			
			mDownloadUrl = dlStats.url;
			mDownloadMultithreaded = dlStats.multithreaded;
			mDownloadSamplePoints = dlStats.speedSamples;
				
            JSONObject o = new JSONObject();
            o.put("status", "download_finished");
            o.put("elapsed", mDownloadDuration);
            o.put("transferred", mDownloadSize);
            o.put("speed", mDownloadSpeedAvgFinal);
			o.put("speed_max", mDownloadSpeedMaxFinal);
            o.put("url", mDownloadUrl);

            PluginResult result = new PluginResult(PluginResult.Status.OK, o.toString());
            result.setKeepCallback(true);
            mSpeedtestCallbackContext.sendPluginResult(result);
        } catch (Exception e) {
			MetricellLogger.getInstance().logException(Aptus.TAG, e);
		}
	}

	/*
	 * Callback for when the upload portion starts
	 */
	@Override
	public void uploadTestStarted(String url) {
        try {
			mUploadErrorCode = SpeedTestManager.ERROR_CODES[SpeedTestManager.NO_ERROR];
			
            JSONObject o = new JSONObject();
            o.put("status", "upload_started");
            o.put("url", url);

            PluginResult result = new PluginResult(PluginResult.Status.OK, o.toString());
            result.setKeepCallback(true);
            mSpeedtestCallbackContext.sendPluginResult(result);
        } catch (Exception e) {
			MetricellLogger.getInstance().logException(Aptus.TAG, e);
		}
	}

	/*
	 * Callback for when the upload progress updates
	 */
	@Override
	public void uploadTestUpdate(SpeedTestStats ulStats) {
        try {
			long speed, speedMax;
			
			// 2G
			if (ulStats.technologyType == TelephonyManager.NETWORK_TYPE_EDGE || ulStats.technologyType == TelephonyManager.NETWORK_TYPE_GPRS) {
				double basicSpeed = 0;
				if (ulStats.duration >= 1000) {
					basicSpeed = (double)ulStats.size / ((double)ulStats.duration/1000);
				}

				speed = (long)basicSpeed;
				speedMax = (long)basicSpeed;
				
			// All other data types
			} else {					
				speed = ulStats.speedAvg;
				speedMax = ulStats.speedMax;
			}
			
            JSONObject o = new JSONObject();
            o.put("status", "upload_progress");
            o.put("elapsed", ulStats.duration);
            o.put("transferred", ulStats.size);
            o.put("speed", speed);
			o.put("speed_max", speedMax);
            o.put("url", ulStats.url);

            PluginResult result = new PluginResult(PluginResult.Status.OK, o.toString());
            result.setKeepCallback(true);
            mSpeedtestCallbackContext.sendPluginResult(result);
        } catch (Exception e) {
			MetricellLogger.getInstance().logException(Aptus.TAG, e);
		}
	}

	/*
	 * Callback when the upload test finishes
	 */
	@Override
	public void uploadTestFinished(SpeedTestStats ulStats) {
        try {
			
			// 2G
			if (ulStats.technologyType == TelephonyManager.NETWORK_TYPE_EDGE || ulStats.technologyType == TelephonyManager.NETWORK_TYPE_GPRS) {
				double basicSpeed = 0;
				if (ulStats.duration >= 1000) {
					basicSpeed = (double)ulStats.size / ((double)ulStats.duration/1000);
				}
				
				mUploadSpeedAvgFinal = (long)basicSpeed;
				mUploadSpeedMaxFinal = (long)basicSpeed;
				
			// All other data types
			} else {
				mUploadSpeedAvgFinal = ulStats.speedAvg;
				mUploadSpeedMaxFinal = ulStats.speedMax;			
			}	
			
			mUploadSize = ulStats.size;
			mUploadDuration = ulStats.duration;
			mUploadUrl = ulStats.url;
			mUploadSamplePoints = ulStats.speedSamples;
			
            JSONObject o = new JSONObject();
            o.put("status", "upload_finished");
            o.put("elapsed", mUploadDuration);
            o.put("transferred", mUploadSize);
            o.put("speed", mUploadSpeedAvgFinal);
			o.put("speed_max", mUploadSpeedMaxFinal);
            o.put("url", mUploadUrl);

            PluginResult result = new PluginResult(PluginResult.Status.OK, o.toString());
            result.setKeepCallback(true);
            mSpeedtestCallbackContext.sendPluginResult(result);
        } catch (Exception e) {
			MetricellLogger.getInstance().logException(Aptus.TAG, e);
		}
	}

	/*
	 * Callback when the ping test portion starts
	 */
	@Override
	public void pingTestStarted(String url) {
        try {
			mPingErrorCode = SpeedTestManager.ERROR_CODES[SpeedTestManager.NO_ERROR];
			
            JSONObject o = new JSONObject();
            o.put("status", "ping_started");
            o.put("url", url);

            PluginResult result = new PluginResult(PluginResult.Status.OK, o.toString());
            result.setKeepCallback(true);
            mSpeedtestCallbackContext.sendPluginResult(result);
        } catch (Exception e) {
			MetricellLogger.getInstance().logException(Aptus.TAG, e);
		}
	}

	/*
	 * Callback when the ping test progress updates
	 */
	@Override
	public void pingTestUpdate(double ping) {
        try {
            JSONObject o = new JSONObject();
            o.put("status", "ping_progress");
            o.put("time", ping);

            PluginResult result = new PluginResult(PluginResult.Status.OK, o.toString());
            result.setKeepCallback(true);
            mSpeedtestCallbackContext.sendPluginResult(result);
        } catch (Exception e) {
			MetricellLogger.getInstance().logException(Aptus.TAG, e);
		}
	}

	/*
	 * Callback when the upload test finishes
	 */
	@Override
	public void pingTestFinished(double ping, double jitter, String url) {
        try {
			mPingSpeedFinal = (long)ping;
			mPingJitter = (long)jitter;
			mPingUrl = url;
		
            JSONObject o = new JSONObject();
            o.put("status", "ping_finished");
            o.put("time", mPingSpeedFinal);
            o.put("jitter", mPingJitter);
            o.put("url", mPingUrl);

            PluginResult result = new PluginResult(PluginResult.Status.OK, o.toString());
            result.setKeepCallback(true);
            mSpeedtestCallbackContext.sendPluginResult(result);
        } catch (Exception e) {
			MetricellLogger.getInstance().logException(Aptus.TAG, e);
		}
	}

	
	/*
	 * Callback when the test experiences an error.  The errorMessage describes the error.
	 */
	public void testError(String errorMessage) {
        testError(null, errorMessage);
	}
	
	/*
	 * Callback when the test experiences an error.  The errorMessage describes the error.
	 */
	@Override
	public void testError(TestResult result, String errorMessage) {
        try {
			
			if (result != null) {
				if (result instanceof UploadTestResult) {
					mUploadErrorCode = SpeedTestManager.ERROR_CODES[result.getErrorCode()];
				} else if (result instanceof DownloadTestResult) {
					mDownloadErrorCode = SpeedTestManager.ERROR_CODES[result.getErrorCode()];
				} else if (result instanceof PingTestResult) {
					mPingErrorCode = SpeedTestManager.ERROR_CODES[result.getErrorCode()];
				}
			}
			
            cancelSpeedtest();
			prepareSpeedtestReport();

            JSONObject o = new JSONObject();
            o.put("status", "network_error");
            o.put("message", errorMessage);

            PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, o.toString());
            mSpeedtestCallbackContext.sendPluginResult(pluginResult);
        } catch (Exception e) {
			MetricellLogger.getInstance().logException(Aptus.TAG, e);
		}
	}

	/**
	 * Callback when the speedtest finishes
	 *
	 * The SpeedTestStats object contains the following properties:
	 * speedAvg - The average test speed in bytes/sec
	 * speedMax - The maximum test speed in bytes/sec
	 * size - Transfer size in bytes
	 * duration - Transfer duration in ms
	 * url - The test url
	 *
	 * @param dlStats - the SpeedTestStats for the download portion of the test
	 * @param ulStats - The SpeedTestStats for the upload portion of the test
	 * @param ping - The ping time in ms
	 * @param jitter - The ping jitter in ms
	 * @param pingUrl - The ping url
	 */
	@Override
	public void speedTestFinished(SpeedTestStats dlStats, SpeedTestStats ulStats,
								  long ping, long jitter, String pingUrl) {

		if (!mSpeedTestCancelled) {
			prepareSpeedtestReport();
			
			try {
				JSONObject o = new JSONObject();
				o.put("status", "finished");

				JSONObject dlObject = new JSONObject();
				dlObject.put("url", mDownloadUrl);
				dlObject.put("transferred", mDownloadSize);
				dlObject.put("duration", mDownloadDuration);
				dlObject.put("speed", mDownloadSpeedAvgFinal);
				dlObject.put("speed_max", mDownloadSpeedMaxFinal);

				JSONObject ulObject = new JSONObject();
				ulObject.put("url", mUploadUrl);
				ulObject.put("transferred", mUploadSize);
				ulObject.put("duration", mUploadDuration);
				ulObject.put("speed", mUploadSpeedAvgFinal);
				ulObject.put("speed_max", mUploadSpeedMaxFinal);

				JSONObject pingObject = new JSONObject();
				pingObject.put("url", mPingUrl);
				pingObject.put("time", mPingSpeedFinal);
				pingObject.put("jitter", mPingJitter);

				o.put("download", dlObject);
				o.put("upload", ulObject);
				o.put("ping", pingObject);

				PluginResult result = new PluginResult(PluginResult.Status.OK, o.toString());
				mSpeedtestCallbackContext.sendPluginResult(result);
			} catch (Exception e) {
				MetricellLogger.getInstance().logException(Aptus.TAG, e);
			}
		}
	}
	
	private void prepareSpeedtestReport() {
		// Add results to DataCollection
		DataCollection dc = mDataCollector.getCurrentStateSnapshot();
		
		// This will reset the speedtest button
		cancelSpeedtest();

		dc.putInt(DataCollection.DATA_DOWNLOAD_AVG, (int)mDownloadSpeedAvgFinal);
		dc.putInt(DataCollection.DATA_DOWNLOAD_MAX, (int)mDownloadSpeedMaxFinal);

		dc.putInt(DataCollection.DATA_DOWNLOAD_SIZE, (int)mDownloadSize);
		dc.putInt(DataCollection.DATA_DOWNLOAD_DURATION, (int)mDownloadDuration);

		if (mDownloadErrorCode != null) dc.putString(DataCollection.DATA_DOWNLOAD_ERROR_CODE, mDownloadErrorCode);

		dc.putInt(DataCollection.DATA_UPLOAD_AVG, (int)mUploadSpeedAvgFinal);
		dc.putInt(DataCollection.DATA_UPLOAD_MAX, (int)mUploadSpeedMaxFinal);

		dc.putInt(DataCollection.DATA_UPLOAD_SIZE, (int)mUploadSize);
		dc.putInt(DataCollection.DATA_UPLOAD_DURATION, (int)mUploadDuration);

		if (mUploadErrorCode != null) dc.putString(DataCollection.DATA_UPLOAD_ERROR_CODE, mUploadErrorCode);

		dc.putInt(DataCollection.DATA_PING_TIME, (int)mPingSpeedFinal);
		dc.putInt(DataCollection.DATA_PING_JITTER, (int)mPingJitter);

		if (mDownloadUrl != null) dc.putString(DataCollection.DATA_DOWNLOAD_URL, mDownloadUrl);
		if (mUploadUrl != null) dc.putString(DataCollection.DATA_UPLOAD_URL, mUploadUrl);
		if (mPingUrl != null) dc.putString(DataCollection.DATA_PING_URL, mPingUrl);

		if (mPingErrorCode != null) dc.putString(DataCollection.DATA_PING_ERROR_CODE, mPingErrorCode);

		dc.putBoolean(DataCollection.DATA_DOWNLOAD_MULTITHREADED, mDownloadMultithreaded);

		if (mDownloadSamplePoints != null) dc.putString(DataCollection.JSON_DATA_DOWNLOAD_POINTS, mDownloadSamplePoints.toString());
		if (mUploadSamplePoints != null) dc.putString(DataCollection.JSON_DATA_UPLOAD_POINTS, mUploadSamplePoints.toString());

		// Override network type with what we received whilst we were doing the speed test
		dc.putString(DataCollection.NETWORK_TYPE, DataCollectorStrings.getNetworkTypeString(mDownloadNetworkType));
		dc.putString(DataCollection.MOBILE_DATA_SUBTYPE, DataCollectorStrings.getNetworkTypeString(mDownloadNetworkType));

		// Set the event type/subtype
		dc.setEventType(DataCollection.EVENT_TYPE_MANUAL_REPORT, DataCollection.EVENT_SUBTYPE_SPEED_TEST);

		// Save test and add to the
		EventQueue eq = EventQueue.getInstance(mContext);
		eq.add(mContext, dc);
		eq.saveQueue(mContext);

		// Send Data (Flush queue)
		Aptus.sendData(mContext);
		Aptus.getSettings(mContext);
	}
}

    