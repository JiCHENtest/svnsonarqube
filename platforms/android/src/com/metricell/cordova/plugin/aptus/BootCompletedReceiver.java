package com.metricell.cordova.plugin.aptus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
 
public class BootCompletedReceiver extends BroadcastReceiver {
    
    @Override
    public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		String dataString = intent.getDataString();
		if (action == null) return;
		if (action.equals("android.intent.action.MY_PACKAGE_REPLACED") ||
			action.equals("android.intent.action.BOOT_COMPLETED") ||
			(action.equals("android.intent.action.PACKAGE_REPLACED") && dataString != null && dataString.contains(context.getPackageName())) ) {
		
			if (Aptus.isBackgroundServiceEnabled(context)) {
				boolean displayServiceNotification = Aptus.isBackgroundServiceNotificationEnabled(context);
				
				Aptus.startAptusService(context, displayServiceNotification);
			}
		}
    }
}