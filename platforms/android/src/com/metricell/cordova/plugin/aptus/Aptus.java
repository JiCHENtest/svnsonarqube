package com.metricell.cordova.plugin.aptus;

import android.Manifest;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import com.metricell.mcc.api.MccService;
import com.metricell.mcc.api.MccServiceSettings;
import com.metricell.mcc.api.http.MccServiceHttpIntentService;
import com.metricell.mcc.api.registration.RegistrationDetails;
import com.metricell.mcc.api.registration.RegistrationManager;
import com.metricell.mcc.api.remotesettings.MccServiceRemoteSettingsHttpIntentService;
import com.metricell.mcc.api.tools.MetricellLogger;
import com.metricell.mcc.api.tools.MetricellNetworkTools;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Aptus extends CordovaPlugin {

	static final String TAG = "AptusCordovaPlugin";

    private static final String PREFS = "aptus";
	private static final String[] APTUS_SERVICE_PERMSSIONS = { Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE };

	private CallbackContext mPermissionCallbackContext = null;
	private boolean mDisplayServiceNotification = false;
	
	private Speedtest mSpeedTest;

	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);

		mSpeedTest = new Speedtest(cordova.getActivity());
	}

	/**
     * Executes the request.
     *
     * @param action The action to execute.
     * @param args The exec() arguments.
     * @param callback The callback context used when calling back into
     * JavaScript.
     * @return Returning false results in a "MethodNotFound" error.
     *
     * @throws JSONException
     */
    @Override
    public boolean execute(String action, JSONArray args,
            CallbackContext callback) throws JSONException {

		Context context = cordova.getActivity().getApplicationContext();

		MccServiceSettings.updateSettings(context);
		
		//
		// Called by cordova.plugins.aptus.startService(successCallback, errorCallback) in JS.
		// It will start the background service after checking for the required permissions, prompting the user if neccessary.
		// If the required permissions are not granted then it will send an error to the error callback.
		//
		if (action.equalsIgnoreCase("startService")) {

			mDisplayServiceNotification = false;
			try {
				if (args.length() > 0) {
					String serviceSettingsJson = args.getString(0);
					if (serviceSettingsJson != null) {
						JSONObject o = new JSONObject(serviceSettingsJson);
						if (o.has("display_notification")) {
							mDisplayServiceNotification = o.optBoolean("display_notification", false);
						}
					}
				}
			} catch (Exception e) {
				MetricellLogger.getInstance().logException(TAG, e);
			}
		
			// If permissions have already been granted start the service immediately
			if (hasRequiredPermissions()) {
				setBackgroundServiceEnabled(context, true, mDisplayServiceNotification);
				callback.success();
			
			// Otherwise request the permissions
			} else {
				mPermissionCallbackContext = callback;
				cordova.requestPermissions(this, 0, APTUS_SERVICE_PERMSSIONS);
			}
			
            return true;
        
		//
		// Called by cordova.plugins.aptus.stopService() in JS
		//
		} else if (action.equalsIgnoreCase("stopService")) {
            setBackgroundServiceEnabled(context, false, false);
			callback.success();
            return true;
			
		// Called by cordova.plugins.aptus.setMsisdn(msisdn) in JS
		} else if (action.equalsIgnoreCase("setMsisdn")) {
			String msisdn = null;
			try {
				if (args.length() > 0) {
					msisdn = args.getString(0);
				}
			} catch (Exception e) {
				MetricellLogger.getInstance().logException(TAG, e);
			}
			
			setMsisdn(context, msisdn);
			callback.success();
			return true;
			
		//
		// Called by cordova.plugins.aptus.getMsisdn(callback) in JS, the value is returned as the callback string
		//
		} else if (action.equalsIgnoreCase("getMsisdn")) {
			String msisdn = getMsisdn(context);
			callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, msisdn));
			return true;	
			
		//
		// Called by cordova.plugins.aptus.getRegistrationId(callback) in JS, the value is returned as the callback string
        } else if (action.equalsIgnoreCase("getRegistrationId")) {
			String registrationId = getRegistrationId(context);
			callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, registrationId));
			return true;	
        
		//
		// Called by cordova.plugins.aptus.requestSettings() in JS
		//
		} else if (action.equalsIgnoreCase("requestSettings")) {
			getSettings(context);
			callback.success();
			return true;
			
		//
		// Called by cordova.plugins.aptus.sendData() in JS
		//
		} else if (action.equalsIgnoreCase("sendData")) {
			sendData(context);
			callback.success();
			return true;

		//
		// Called by cordova.plugins.aptus.speedtestStart() in JS
		//
		} else if (action.equalsIgnoreCase("speedtestStart")) {

			// If permissions have already been granted start the speedtest immediately
			if (hasRequiredPermissions()) {
				mSpeedTest.startSpeedtest(cordova.getActivity(), callback);

				// Otherwise request the permissions
			} else {
				mPermissionCallbackContext = callback;
				cordova.requestPermissions(this, 1, APTUS_SERVICE_PERMSSIONS);
			}

			return true;

		//
		// Called by cordova.plugins.aptus.speedtestStop() in JS
		//
		} else if (action.equalsIgnoreCase("speedtestStop")) {
			mSpeedTest.cancelSpeedtest();
			return true;

		//
		// Called by cordova.plugins.aptus.isWifiConnected() in JS
		//
		} else if (action.equalsIgnoreCase("isWifiConnected")) {
			if (MetricellNetworkTools.isWifiConnected(cordova.getActivity())) {
				callback.success();
			} else {
				callback.error("false");
			}
			return true;

		//
		// Called by cordova.plugins.aptus.hasDataConnection() in JS
		//
		} else if (action.equalsIgnoreCase("hasDataConnection")) {
			if (MetricellNetworkTools.hasDataConnection(cordova.getActivity())) {
				callback.success();
			} else {
				callback.error("false");
			}
			return true;
		}
		
        return false;
    }
	
	/**
	*/
	public void onRequestPermissionResult(int requestCode, String[] permissions,
                                         int[] grantResults) throws JSONException {
		
		// If the permissions are not granted, report back an error to the the registered callback
		for (int r:grantResults) {
			if (r == PackageManager.PERMISSION_DENIED) {
				mPermissionCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "Permissions denied"));
				return;
			}
		}
		
		// Permissions have been granted so start the background service
		Context context = cordova.getActivity().getApplicationContext();
		switch(requestCode) {
			case 0:
				setBackgroundServiceEnabled(context, true, mDisplayServiceNotification);
				mPermissionCallbackContext.success();
				break;

			case 1:
				mSpeedTest.startSpeedtest(cordova.getActivity(), mPermissionCallbackContext);
				break;
		}
	}

    private void setBackgroundServiceEnabled(Context context, boolean enabled, boolean displayNotification) {
        SharedPreferences sp = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
		editor.putBoolean("background_service_enabled", enabled);
		editor.putBoolean("background_service_notification", displayNotification);
		editor.commit();

		if (enabled) {	
			startAptusService(context, displayNotification);
		} else {
			stopAptusService(context);
		}
    }

	static boolean isBackgroundServiceEnabled(Context c) {
		SharedPreferences sp = c.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		return sp.getBoolean("background_service_enabled", false);
	}

	static boolean isBackgroundServiceNotificationEnabled(Context c) {
		SharedPreferences sp = c.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		return sp.getBoolean("background_service_notification", false);
	}

	private static void setMsisdn(Context c, String msisdn) {
		// You should start the MSISDN with your local country code (e.g. 447938748374 for the UK)
		MccServiceSettings.setMsisdn(c, msisdn);
	}

	private static String getMsisdn(Context c) {
		return MccServiceSettings.getMsisdn(c);
	}

	private static String getRegistrationId(Context c) {
		RegistrationManager rm = RegistrationManager.getInstance(c);
		RegistrationDetails regDetails = rm.getRegistrationDetails();
		return regDetails.getRegistrationId();
	}

	static void startAptusService(Context c, boolean displayNotification) {

		try {
			if (MccService.hasRequiredPermissions(c)) {
				
				Intent serviceIntent = new Intent(c, MccService.class);
	
				if (displayNotification) {
					// Optional - provide extras to display the ongoing
					// foreground service notification
					Intent launchIntent = null;
					try {
						launchIntent = c.getPackageManager().getLaunchIntentForPackage(c.getPackageName());
						if (launchIntent != null) {
							launchIntent.setAction(Intent.ACTION_MAIN);
							launchIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
						}
					} catch (Exception e) {
					}
			
					if (launchIntent != null)
						serviceIntent.putExtra(MccService.EXTRA_NOTIFICATION_INTENT, launchIntent);

					int titleResId = c.getResources().getIdentifier("aptus_notification_title", "string", c.getPackageName());
					int messageResId = c.getResources().getIdentifier("aptus_notification_text", "string", c.getPackageName());

					String title = null;
					if (titleResId != 0) {
						title = c.getResources().getString(titleResId);
					}
					if (title == null) {
						title = getApplicationName(c);
					}
					serviceIntent.putExtra(MccService.EXTRA_NOTIFICATION_TITLE, title);

					String msg = null;
					if (messageResId != 0) {
						msg = c.getResources().getString(messageResId);
					}
					if (msg == null) {
						msg = getApplicationName(c) + " is running ...";
					}
					serviceIntent.putExtra(MccService.EXTRA_NOTIFICATION_TEXT, msg);

					int iconResId = c.getResources().getIdentifier("aptus_notification_icon", "drawable", c.getPackageName());
					if (iconResId != 0)
						serviceIntent.putExtra(MccService.EXTRA_NOTIFICATION_ICON_RESOURCE, iconResId);

					if (Build.VERSION.SDK_INT >= 16) {
						serviceIntent.putExtra(MccService.EXTRA_NOTIFICATION_PRIORITY, Notification.PRIORITY_DEFAULT);
					}
				}
				
				c.startService(serviceIntent);
			}

		} catch (Exception e) {
			MetricellLogger.getInstance().logException(TAG, e);
		}
	}

	private static void stopAptusService(Context c) {
		try {
			c.stopService(new Intent(c, MccService.class));
		} catch (Exception e) {
			MetricellLogger.getInstance().logException(TAG, e);
		}
	}

	static void getSettings(Context c) {
		try {
			c.startService(new Intent(c, MccServiceRemoteSettingsHttpIntentService.class));
		} catch (Exception e) {
			MetricellLogger.getInstance().logException(TAG, e);
		}
	}

	static void sendData(Context c) {
		try {
			c.startService(new Intent(c, MccServiceHttpIntentService.class));
		} catch (Exception e) {
			MetricellLogger.getInstance().logException(TAG, e);
		}
	}

	private boolean hasRequiredPermissions() {
		boolean hasAllRequiredPermissions = true;
		for (String perm : APTUS_SERVICE_PERMSSIONS) {
			if (!cordova.hasPermission(perm)) {
				hasAllRequiredPermissions = false;
				break;
			}
		}
		
		return hasAllRequiredPermissions;
	}
	
	private static String getApplicationName(Context context) {
		ApplicationInfo applicationInfo = context.getApplicationInfo();
		int stringId = applicationInfo.labelRes;
		return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
	}

}
