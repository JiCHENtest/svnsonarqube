// This is a generated file. Do not edit. See application-descriptor.xml.
// WLClient configuration variables.
console.log("Running static_app_props.js...");
var WL = WL ? WL : {};
WL.StaticAppProps = { APP_ID: 'com.celcom.mycelcom',
  APP_VERSION: '1.0',
  WORKLIGHT_PLATFORM_VERSION: '8.0.0.00-20171219-120008',
  WORKLIGHT_NATIVE_VERSION: '2224806268',
  LANGUAGE_PREFERENCES: 'en',
  ENVIRONMENT: 'android',
  WORKLIGHT_ROOT_URL: '/apps/services/api/com.celcom.mycelcom/android/',
  APP_SERVICES_URL: '/apps/services/',
  APP_DISPLAY_NAME: 'Celcom Life',
  LOGIN_DISPLAY_TYPE: 'embedded',
  mfpClientCustomInit: false,
  MESSAGES_DIR: 'plugins\\cordova-plugin-mfp\\worklight\\messages' };