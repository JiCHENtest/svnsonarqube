cordova.define("cordova-plugin-metricell-aptus.Aptus", function(require, exports, module) {
var exec = require('cordova/exec');

/**
 * Activates the automatic startup of the Aptus background service
 * after the reboot of the device, and starts the service immediately if it is not already running
 */
exports.startService = function (successCallback, errorCallback, settings) {
	cordova.exec(successCallback, errorCallback, 'Aptus', 'startService', [ settings ]);
};

/**
 * Deactivates the automatic start of the Aptus background service 
 * after the reboot of the device, and stops any services already running
 */
exports.stopService = function () {
	cordova.exec(function(msg){}, function(msg){}, 'Aptus', 'stopService', []);
};

/**
 * Requests that any queued data is sent to the server
 */
exports.setMsisdn = function (msisdn) {
	cordova.exec(function(msg){}, function(msg){}, 'Aptus', 'setMsisdn', [msisdn]);
};

/**
 * Returns the MSISDN used to identify Aptus data on the server side.  The value is registered with setMsisdn.
 */
exports.getMsisdn = function (callback) {
	cordova.exec(callback, function(msg){}, 'Aptus', 'getMsisdn', []);
};

/**
 * Returns the current Metricell registration ID.
 */
exports.getRegistrationId = function (callback) {
	cordova.exec(callback, function(msg){}, 'Aptus', 'getRegistrationId', []);
};

/**
 * Requests a settings refresh from the server
 */
exports.requestSettings = function () {
    cordova.exec(function(msg){}, function(msg){}, 'Aptus', 'requestSettings', []);
};

/**
 * Requests that any queued data is sent to the server
 */
exports.sendData = function () {
    cordova.exec(function(msg){}, function(msg){}, 'Aptus', 'sendData', []);
};

/**
 * Requests that any queued data is sent to the server
 */
exports.isWifiConnected = function (trueCallback, falseCallback) {
    cordova.exec(trueCallback, falseCallback, 'Aptus', 'isWifiConnected', []);
};

/**
 * Requests that any queued data is sent to the server
 */
exports.hasDataConnection = function (trueCallback, falseCallback) {
    cordova.exec(trueCallback, falseCallback, 'Aptus', 'hasDataConnection', []);
};

// Speedtest Additions

/**
 * Start a speedtest
 */
exports.speedtestStart = function (successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, 'Aptus', 'speedtestStart', []);
};

/**
 * Stop any currently running speedtests
 */
exports.speedtestStop = function () {
    cordova.exec(null, null, 'Aptus', 'speedtestStop', []);
};


});
