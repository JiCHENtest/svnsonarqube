import { Component, NgZone, Renderer2 } from '@angular/core';
import { NavController, NavParams, ViewController ,Platform, Events } from 'ionic-angular';
import { Contacts, Contact } from '@ionic-native/contacts';
import { PayBillService } from '../pay-bill/payBillService';
import { AlertService } from '../../global/alert-service';
import { GatewayService } from '../../global/utilService';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ReloadService} from '../../global/reloadServices';
import { AlertController } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';



  


/**
 * Generated class for the AddNumberModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-number-modal',
  templateUrl: 'add-number-modal.html',
})
export class AddNumberModalPage {
 
  userSelectedMobileNumber:any;
  loader:any;
  addNewNumberForm:FormGroup;
  serviceIndicator:any;
  accountNumAdded:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, 
    public viewCtrl: ViewController,public contact:Contacts,public platform:Platform, 
    public paybillService:PayBillService, public gatewayService: GatewayService, 
    public reloadService:ReloadService,public conformAlert:AlertController, 
    public alertCtrlInstance: AlertService,  public events: Events,private keyboard: Keyboard, 
    public zone:NgZone, private renderer: Renderer2) {
    this.loader = this.gatewayService.LoaderService();

    this.addNewNumberForm = formBuilder.group({
      userSelectedMobileNumber: ['', Validators.compose([Validators.maxLength(11),Validators.minLength(10), Validators.pattern('[0-9]*'), Validators.required])],
     
  });
 
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Add number", this.renderer);
  }

  ionViewDidLoad() {
    this.userSelectedMobileNumber = "";
    console.log('ionViewDidLoad AddNumberModalPage');
  }
  cancel(){
    this.viewCtrl.dismiss();
  }


 checkNumberPattern(number) { 

// = "+601123693858";

            var phoneNumber = number; // => "Tabs1"

//console.log("complete:"+phoneNumber);

    var finalRes;

    if (phoneNumber.substr(0,3) == "+60"){

         finalRes = phoneNumber.substr(2);

console.log("finalRes1:"+finalRes);

        }

else if (phoneNumber.substr(0,2) == "60"){

        finalRes = phoneNumber.substr(1);

console.log("finalRes2:"+finalRes);

        }

else{

        finalRes = phoneNumber;

console.log("finalRes3:"+finalRes);

        }

 

return finalRes;

}



  PrepaidMethod(){
    //var selectedNumber = this.userSelectedMobileNumber.replace(/[^0-9]/g, "")
     var selectedNumber=this.checkNumberPattern(this.userSelectedMobileNumber);
      this.reloadService.checkAddedNewNumber(selectedNumber).then((res)=> {
        console.log("inside checkAddedNewNumber "+JSON.stringify(res));
        this.serviceIndicator = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.ListOfServices.Services.ServiceType;
        
           if(this.serviceIndicator== "Prepaid" || this.serviceIndicator=="CNVRGTPrepaid"){
            this.loader.dismiss();
            if(selectedNumber.substring(0,3)=="011" && selectedNumber.length==10){
              this.alertCtrlInstance.showAlert("Uh Oh. Fingers Too Fast ?", "Please input a valid Celcom phone number. (If starting with 011 then put 11 digits).", "Try again");
            }
            else{
              this.viewCtrl.dismiss(selectedNumber).then(()=>{
                this.events.publish('selectedMsisdn', selectedNumber);
                selectedNumber = "";
              });
            }
          }else{
            this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid Number", "Please enter a valid Celcom prepaid number (10 or 11 digits only).", "OK");
            this.loader.dismiss();
          }
   
       }).catch(err => {
        this.loader.dismiss();
        console.log("*****paybill*******"+JSON.stringify(err));
        if(err=="No internet connection")
        return;
        //  //this.alertCtrlInstance.showAlert("Notification", "Not a Celcom number.", "OK");
        //  this.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
        // // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
        if(err.indexOf("timed out") >= 0 || err.indexOf("Failed to connect") >= 0){
          this.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
        }
        else{
          this.alertCtrlInstance.showAlert("Notification", "Not a Celcom number.", "OK");
        }
        
    });

  }
  dismiss() {
    this.loader = this.gatewayService.LoaderService();

    this.loader.present();
    if(this.gatewayService.isPrePaid()){

      this.PrepaidMethod();

    }else{
      
    
      //var selectedNumber = this.userSelectedMobileNumber.replace(/[^0-9]/g, "");
       var selectedNumber=this.checkNumberPattern(this.userSelectedMobileNumber);
      this.paybillService.checkAddedNewNumber(selectedNumber).then((res)=> {
        console.log("inside checkAddedNewNumber "+JSON.stringify(res));
        this.serviceIndicator=res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.ListOfServices.Services.ServiceType;
        //AssetBillingAccountNo : "300098241"
        this.accountNumAdded=res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.ListOfServices.Services.AssetBillingAccountNo;
          if(this.serviceIndicator=="Postpaid" || this.serviceIndicator=="CNVRGTPostpaid"){
            this.loader.dismiss();
            if(selectedNumber.substring(0,3)=="011" && selectedNumber.length==10){
              this.alertCtrlInstance.showAlert("Uh Oh. Fingers Too Fast?", "Please enter a valid Celcom number (10 or 11 digits only)", "Try again");
            }
            
            else{
              this.viewCtrl.dismiss(selectedNumber).then(()=>{
                this.events.publish('AccountNumAdded', this.accountNumAdded,selectedNumber);
                selectedNumber ="";
              });
            }
           
          }else{
            this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid Number", "Please enter a valid Celcom postpaid number (10 or 11 digits only)", "OK");
            this.loader.dismiss();
          }
  
       }).catch(err => {
        this.loader.dismiss();
        console.log("*****paybill*******"+JSON.stringify(err));
        if(err=="No internet connection")
        return;
        // this.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
        
        // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
        if(err.indexOf("timed out") >= 0 || err.indexOf("Failed to connect") >= 0){
          this.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
        }
        else{
          this.alertCtrlInstance.showAlert("Notification", "Not a Celcom number.", "OK");
        }
    });
    }
  
   
  }
  
  openTheContactConform(){
    this.userSelectedMobileNumber = "";
        this.zone.run(() => {

              this.contact.pickContact().then((response: Contact) => { 
              //var checkLenght = response["_objectInstance"].phoneNumbers[0].value;
              var chooseMobileNumber = response["_objectInstance"].phoneNumbers[0].value;
                  chooseMobileNumber = chooseMobileNumber.replace(/[^0-9]/g, "");
              this.userSelectedMobileNumber =  chooseMobileNumber;
      
                       console.log("Open the contacts response "+response)
                    }); 
        })
     
  }

// openTheContact(){

//  let alert = this.conformAlert.create({
//         title:"CONTACT LIST",
//         subTitle:"Allow CelcomLife to access your contacts?",
//         buttons:[
//           {
//             text:"DON'T ALLOW",
//             role:"DON'T ALLOW",
//             handler:()=>{
//               console.log("ALolow");
//             }
//           },{
//               text:"ALLOW",
//               handler:()=>{
//                 console.log("ALolow");
//                 this.openTheContactConform();
//               }
//           }
//         ],
//         cssClass:'error-message'
//       });

// alert.present();

 
// }
handleKeyDone(e){
  if(e.which === 13){
    e.preventDefault();
    this.keyboard.close();
  }
}
}
