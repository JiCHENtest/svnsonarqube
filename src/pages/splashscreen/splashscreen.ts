import { Component, ViewChild, Renderer2 } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { OnBoardingGuidePage } from "../../pages/on-boarding-guide/on-boarding-guide";
import { NetworkLoginLegalPage } from "../login/network-login/network-login-legal/network-login-legal";
import { SplashScreen2Page } from '../splash-screen2/splash-screen2';
import { SplashScreen } from '@ionic-native/splash-screen';
/**
 * Generated class for the SplashscreenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-splashscreen',
  templateUrl: 'splashscreen.html',
})
export class SplashscreenPage {

  @ViewChild('btnCon') btnCon;
  handleHeight = - 125;
  ischecked: boolean = false;
  checkboxFlag: any;
  constructor(public navCtrl: NavController,public viewCtrl: ViewController, public navParams: NavParams, public renderer2: Renderer2, public splashScreen: SplashScreen) {
  }

  ionViewDidEnter() {

    this.splashScreen.hide();
 
    setTimeout(() => {
      this.viewCtrl.dismiss();
      let interval = setInterval(()=>{
        if(this.handleHeight < 0)
        {
          this.handleHeight = this.handleHeight + 1;
          this.renderer2.setStyle(this.btnCon.nativeElement, 'bottom', this.handleHeight+ 'px');
        }
        else
        {
          clearInterval(interval);
        }
      },4)
  
    }, 4000);


    // setTimeout(() => {
    //   let interval = setInterval(()=>{
    //     if(this.handleHeight < 0)
    //     {
    //       this.handleHeight = this.handleHeight + 1;
    //       this.renderer2.setStyle(this.btnCon.nativeElement, 'bottom', this.handleHeight+ 'px');
    //     }
    //     else
    //     {
    //       clearInterval(interval);
    //     }
    //   },4)
  
      
    // }, 4500);


  }

  viewlegalPage() {
    this.navCtrl.push(NetworkLoginLegalPage);
  }



  navigatetoMC() {

    this.navCtrl.setRoot(SplashScreen2Page);
  }

  enableLogin() {

    if (this.checkboxFlag == "") {
      this.ischecked = false;
    }
    else {
      this.ischecked = true;
    }


  }
}
