import { Component, ViewChild, Renderer2, ElementRef, NgZone } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { ViewCardDetailsPage } from './view-card-details/view-card-details';
import { GatewayService } from '../../../global/utilService';
import { PaymentMethodsService } from './paymentMethodsService';
import { AlertService } from '../../../global/alert-service';
import { ReloadService } from '../../../global/reloadServices';

/**
 * Generated class for the PaymentMethodsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-payment-methods',
  templateUrl: 'payment-methods.html',
})
export class PaymentMethodsPage {
  @ViewChild('editButton') editButton;
  @ViewChild('doneButton') doneButton;
  @ViewChild('content') content;

  loader: any;
  //isCardSaved:any;
  Cardflag: any;
  selectedCard: string;
  cardArray: any;
  cardToken: any;
  loginNum: any;
  second: string = "(Default)";
  isDeleted: any;
  deleteMessage: any;
  //staticNumber = "0132772997";
  cards = [];
  cardType: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public renderer2: Renderer2, public element: ElementRef, public alertCtrl: AlertController, public gatewayService: GatewayService, public paymentMethodsService: PaymentMethodsService, public alertCtrlInstance: AlertService, public reloadService: ReloadService, public zone: NgZone) {
    this.loginNum = this.reloadService.gettingLoginNumber();
    this.loader = this.gatewayService.LoaderService();
    this.getSavedCards();
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Payment methods setting", this.renderer2);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentMethodsPage');
  }

  edit() {
    console.log(this.editButton.nativeElement);
    this.renderer2.addClass(this.editButton.nativeElement, 'hide-class');
    this.renderer2.addClass(this.doneButton.nativeElement, 'show-class');
    this.renderer2.addClass(this.content.nativeElement, 'show-buttons');

  }
  done() {
    this.renderer2.removeClass(this.editButton.nativeElement, 'hide-class');
    this.renderer2.removeClass(this.doneButton.nativeElement, 'show-class');
    this.renderer2.removeClass(this.content.nativeElement, 'show-buttons');

  }
  openCard(cardDetails) {
    this.paymentMethodsService.setCardDetails(cardDetails);
    this.navCtrl.push(ViewCardDetailsPage);
  }

  presentsucess(ev) {

    const alert = this.alertCtrl.create({
      title: 'Bye Bye Credit Card',
      subTitle: ' ',
      message: "Are you sure you want to delete this credit card?",
      buttons: [
        {
          text: 'Delete',
          cssClass: 'submit-button',
          handler: () => {
            console.log('Call to Action Clicked');
            //call to delete

            //let index = this.cards.indexOf(ev);
            this.cardToken = this.cards[ev].TOKEN;
            if (ev > -1) {
              this.zone.run(() => {
                this.deleteSavedCards(ev);
              });



            }

          }

        },
        {
          text: 'Cancel',
          cssClass: 'submit-button',
          handler: () => {
            console.log('Cancel Clicked');
          }

        }],
      cssClass: 'success-message',
      enableBackdropDismiss: false
    });

    alert.present();
  }

  getSavedCards() {
    this.loader.present();

    this.paymentMethodsService.fetchSavedCards(this.reloadService.gettingLoginNumber()).then((res) => {
      this.loader.dismiss();

      //  this.isCardSaved= ;
      if (res["savedCards"] == undefined) {
        this.Cardflag = false;
      } else if (res["savedCards"].length >= 1) {
        this.Cardflag = true;
        this.cardArray = [];
        var allcards = res["savedCards"];

        for (var i = 0; i < allcards.length; i++) {
          var elem = {};
          var re = /X/gi;
          var allCreditCards = allcards[i].masked;
          allcards[i].masked = allcards[i].masked.replace(re, "-");
          var u = /-/gi;
          var cardTypeNumber = allCreditCards.replace(u, "");
          var t = / /gi;
          cardTypeNumber = cardTypeNumber.replace(t, "");
          var h = /X/gi;
          cardTypeNumber = cardTypeNumber.replace(h, "0");

          console.log('my cards length' + cardTypeNumber.length);
          this.cardType = this.GetCardType(cardTypeNumber);
          allcards[i].cardtype = this.cardType;
          //this.cards.push('');
        }
        this.cards = allcards;
      } else {
        this.Cardflag = false;
      }



      // allcards.forEach(element => {

      //     this.cardArray.push(element);
      //   console.log("*****saveCards*******"+JSON.stringify(element));  
      //  this.cards.push(element.masked.replace("x","-"));    
      //   // if(element.is_default == true){
      //   //   this.selectedCard = element.masked ;
      //   // }

      // });
    }).catch(err => {
      //this.loader.dismiss();  
      console.log("*****saveCards Error*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Fetch saved cards for payment Methods", "No data found.", "OK");
      //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }
  deleteSavedCards(ev) {
    this.loader = this.gatewayService.LoaderService();
    this.loader.present();

    this.paymentMethodsService.deleteCards(this.reloadService.gettingLoginNumber(), this.cardToken).then((res) => {
      console.log("response from delete cards in payment methods " + JSON.stringify(res));
      this.loader.dismiss();
      this.isDeleted = res["isSuccessful"];
      this.deleteMessage = res["cswMessage"];

      if (this.isDeleted == true) {
        this.alertCtrlInstance.showAlert("Notification", this.deleteMessage, "OK");
        this.cards.splice(ev, 1);
      } else {
        this.alertCtrlInstance.showAlert("Notification", "Failed to delete", "OK");
      }

    }).catch(err => {
      //this.loader.dismiss();  
      console.log("*****saveCards Error*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Delete saved cards", "No data found.", "OK");
      //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }

  GetCardType(number) {
    // visa
    var re = new RegExp("^4");
    if (number.match(re) != null) {
      return "Visa";
    }
    // Mastercard 
    // Updated for Mastercard 2017 BINs expansion
    else if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number)) {
      return "Mastercard";
    }
    else
      return "";
  }

}
