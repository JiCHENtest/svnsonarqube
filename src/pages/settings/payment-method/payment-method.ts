import {Component, Renderer2} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import { GatewayService } from '../../../global/utilService';

/**
 * Generated class for the PaymentMethodPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'payment-method',
  templateUrl: 'payment-method.html',
})
export class PaymentMethodPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private renderer: Renderer2, public gatewayService: GatewayService) {
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Payment method setting", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentMethodPage');
  }

}
