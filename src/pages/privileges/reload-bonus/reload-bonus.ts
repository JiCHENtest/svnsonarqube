import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductDetailsDesignGuidePage } from '../../product-details-design-guide/product-details-design-guide';
import { GatewayService } from '../../../global/utilService';

/**
 * Generated class for the ReloadBonusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reload-bonus',
  templateUrl: 'reload-bonus.html',
})
export class ReloadBonusPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private renderer: Renderer2, public gatewayService: GatewayService) {
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Reload bonus privilege", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReloadBonusPage');
  }
  
  goToProductDetails(){
    this.navCtrl.push(ProductDetailsDesignGuidePage);
  }

}
