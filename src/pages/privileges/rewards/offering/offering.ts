import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
import { ProductDetailsDesignGuidePage } from "../../../product-details-design-guide/product-details-design-guide";
import { GatewayService } from '../../../../global/utilService';

/**
 * Generated class for the OfferingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-offering',
  templateUrl: 'offering.html',
})
export class OfferingPage {
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public appCtrl: App,
    public gatewayService: GatewayService, private renderer: Renderer2) {
    
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Rewards offering privilege", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfferingPage');
  }
  openDetails() {
    this.appCtrl.getRootNav().push(ProductDetailsDesignGuidePage);
  }
}
