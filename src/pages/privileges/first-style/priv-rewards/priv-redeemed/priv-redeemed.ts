import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, App, Events } from 'ionic-angular';
import { ProductDetailsPage } from "../../product-details/product-details";
import { GatewayService } from '../../../../../global/utilService';
import { isArray } from 'ionic-angular/util/util';

/**
 * Generated class for the RedeemedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-priv-redeemed',
  templateUrl: 'priv-redeemed.html',
})
export class PrivRedeemedPage {
  rewards:any;
  offrings:any;
  redeemed:any;
  total_redeems:any;
  images:any;
  isPrepaid:any;
  constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams, 
    public appCtrl: App, public gatewayService: GatewayService, private renderer: Renderer2) {
    this.rewards = this.gatewayService.GetPrivilegeData();
    console.log(this.rewards);
    console.log('redeemed offers below');
    this.total_redeems = [];
    if(this.rewards.QueryVouchersResponse.REDEEMES.RedeemedPrivilegesList.Redeem != undefined)
    {
    var arr = isArray(this.rewards.QueryVouchersResponse.REDEEMES.RedeemedPrivilegesList.Redeem);
    if(!arr){
      this.total_redeems.push(this.rewards.QueryVouchersResponse.REDEEMES.RedeemedPrivilegesList.Redeem);
    }
    else{
      this.total_redeems = this.rewards.QueryVouchersResponse.REDEEMES.RedeemedPrivilegesList.Redeem;
    }
  }
 console.log(this.total_redeems);
 this.images = {
  "218" : "tealive.jpg",
  "219" : "11street.jpg",
  "221" : "grab.jpg",
  "204" : "playstore.jpg",
  "209" : "apple.jpg",
  "198" : "video_walla.png",
  "220" : "gsc.jpg",
  "223" : "flight.jpg",
  "222" : "flight.jpg"
  };
 
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Redeemed privilege", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad priv RedeemedPage');
  }

  openDetails(id) {
    console.log(id);
    var offer_id = id;
    this.events.publish("goToOfferDetail",{"offer_id":offer_id, "type":"redeem"});
    //this.navCtrl.push(ProductDetailsPage,{"offer_id":offer_id, "type":"offer"});
  }

  ionViewWillEnter(){
    console.log('ionViewWillEnter priv RedeemedPage');
  }
  DataAlignment(gettingDate){
    var day = gettingDate.slice(0, 8) ;
    var year = day.match(/.{1,4}/g);
    var date = year[1].match(/.{1,2}/g);
    var finalDate = date[1]+'/'+date[0]+'/'+year[0];
    return finalDate;
    }
}
