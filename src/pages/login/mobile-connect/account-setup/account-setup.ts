import { Component, Renderer2 } from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import {MobileConnectPage} from "../mobile-connect";
import {NetworkLoginLegalPage} from "../../network-login/network-login-legal/network-login-legal";
import { ConnectTacPage } from '../../connect-tac/connect-tac';
import { GatewayService } from '../../../../global/utilService';
import { ConstantData } from '../../../../global/constantService';
import { LocalStorage } from '../../../../global/localStorage';
import { MobileConnectService } from '../mobileConnectService';
import {NetworkLoginService} from "../../network-login/network-loginService";
import { AlertService } from '../../../../global/alert-service';
import { HomePage } from '../../../home/home';
/**
 * Generated class for the MobileConnectAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mobile-connect-account',
  templateUrl: 'account-setup.html',
})
export class MobileConnectAccountPage {
  state:any;
  constructor(public navCtrl: NavController,private alertCtrlInstance: AlertService, 
    public navParams: NavParams,  public networkLoginService:NetworkLoginService, 
    public loadingCtrl: LoadingController, public gatewayService:GatewayService, 
    public localStorage:LocalStorage, public mConnectService:MobileConnectService, private renderer: Renderer2) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MobileConnectAccountPage');
  }

 gotoHome() {
  if(ConstantData.isLoginBypassed==false){
    var _that=this;
    var loader = _that.gatewayService.LoaderService();
    loader.present();
    //code for calling registernew()
    _that.registerNewMobileConnectUser().then(function(res){
      _that.mConnectService.SetMConnectInfo(res);
      _that.navCtrl.setRoot(ConnectTacPage);
      loader.dismiss();
    }).catch(function(e){
      console.log("component  failure"+e);
      alert("Error"+e);
      loader.dismiss();
      if(e=="No internet connection")
      return;
    });
   
  } else{
    this.navCtrl.setRoot(ConnectTacPage);
  }
}

ionViewDidEnter(){
  this.gatewayService.resetAllAnalyticData();
  this.gatewayService.setAnalyticPageName("Mobile connect account setup", this.renderer);
}

viewlegalPage(value) {
  this.networkLoginService.SetTabInfo(value);
  this.navCtrl.push( NetworkLoginLegalPage );
}
 registerNewMobileConnectUser(){
   var _that = this;
    //this.state = this.navParams.get('data'); 
    //console.log(this.state);
     var msisdn = this.gatewayService.GetMobileNumber();
    console.log('mobile number on account' + msisdn);
    var params_register={"MSISDN":msisdn};
    
    return new Promise((resolve, reject) => {
    this.gatewayService.CallAdapter(ConstantData.adapterUrls.registerNew,"POST",params_register).then((res_register)=> {
      console.log("register user response : "+JSON.stringify(res_register));
     resolve(res_register);
      
      //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
    })
    .catch(function(e){
      console.log("******************************************** component failure registerMobileConnectUser"+e);
   //   alert("error register MobileConnectUser");
   if(e=="No internet connection")
   return;
   _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK"); 
      reject(e);
    });
      
  });
   
     
  }
  goBack(){
    this.navCtrl.setRoot(MobileConnectPage);
  }
}
