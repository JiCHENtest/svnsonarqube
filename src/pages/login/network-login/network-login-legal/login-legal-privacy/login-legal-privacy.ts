import {Component, Renderer2} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import { GatewayService } from '../../../../../global/utilService';
/**
 * Generated class for the LoginLegalPrivacyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login-legal-privacy',
  templateUrl: 'login-legal-privacy.html',
})
export class LoginLegalPrivacyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private renderer: Renderer2, public gatewayService: GatewayService) {
    
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageName("Network login privacy", this.renderer);
  }

}
