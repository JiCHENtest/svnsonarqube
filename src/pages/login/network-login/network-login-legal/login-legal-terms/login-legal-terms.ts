import {Component, Renderer2} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import { GatewayService } from '../../../../../global/utilService';
/**
 * Generated class for the LoginLegalTermsPage .
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login-legal-terms',
  templateUrl: 'login-legal-terms.html',
})
export class LoginLegalTermsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private renderer: Renderer2, public gatewayService: GatewayService) {
   
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageName("Network login terms and condition", this.renderer);
  }

}
