import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, ModalController, Events } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
// pages
import { ConfirmationScreensPage } from '../../pages/confirmation-screens/confirmation-screens';

// models
import { AddNumberModalPage } from "../../pages/add-number-modal/add-number-modal";

import { ReloadService } from '../../global/reloadServices';
import { GatewayService } from '../../global/utilService';
import { OleoleService } from '../../global/oleole-service';
import { DashboardService } from '../../pages/home/homeService';
import { SubscriptionService } from "../subscriptions/subscriptionService";
import { GlobalVars } from "../../providers/globalVars";

import { Keyboard } from '@ionic-native/keyboard';
import { Contacts, Contact } from '@ionic-native/contacts';

import { AlertService } from '../../global/alert-service';
import { AlertController } from 'ionic-angular';
import { PrivilegesPage } from '../privileges/privileges';
import { ReloadPage } from "../reload/reload";
import { retry } from 'rxjs/operator/retry';

import _ from 'underscore';

/**
 * Generated class for the ProductDetailsDesignGuidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-product-details-design-guide',
  templateUrl: 'product-details-design-guide.html',
})
export class ProductDetailsDesignGuidePage {
  checkKawKaw: any;
  kawkawVariable: any;
  subIndexValue: any;
  showGiftContainer: boolean = true;
  loader;
  planDetails;
  Ecem: any;
  planDetailsName = [];
  productID = [];
  selectedProductID: any;
  selectedNumber;
  chooseButton: boolean;
  pageName: string;
  number: any;
  alertNumber: any;
  //oleole
  oleoleGiftPartnerList: any;
  oleoleVoucherImageSrc: any;
  isOleole: boolean = false;
  isGiftTyped: boolean = false;

  // subscription gift
  subscriptionGift: any;
  isOleoleSuscription: boolean = false;

  personalizedMessage: any;
  numberToSendAndGet: any;
  isOleoleProductEnable: boolean = true;

  isSelf: boolean = true;

  gift_request: any;
  WhenShowOption: boolean = true;
  isVoucher: boolean = false;
  reloadbouns: boolean = true;
  isNumberNotValid: any;

  countNum: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public reloadService: ReloadService, public alertCtrlInstance: AlertService, 
    public gatewayService: GatewayService, public conformAlert: AlertController, 
    public dashboardService: DashboardService, private keyboard: Keyboard, 
    public modalCtrl: ModalController, public contact: Contacts, public oleoleService: OleoleService, 
    public events: Events,public subscriptionService: SubscriptionService, 
    public globalVar: GlobalVars, private renderer: Renderer2) {

    // title of  plan
    console.log("ProductDetailsDesignGuidePage -> constructor() -> Header titile = ", this.navParams.get('header_title'));

    // oleole vouchar    
    this.oleoleGiftPartnerList = this.navParams.get('oleoleGiftPartner');

    // isvoucher
    this.isVoucher = (this.navParams.get('isvoucher') != undefined && this.navParams.get('isvoucher') == true) ? true : false;

    console.log("ProductDetailsDesignGuidePage -> gift_request ", this.gift_request);
    console.log("ProductDetailsDesignGuidePage -> isVoucher ", this.isVoucher);

    this.oleoleVoucherImageSrc = {
      "Fashion_Valet": "./assets/img/oleole-product/2304x1296_fashionvalet.jpg",
      "Melissa_Shoes": "./assets/img/oleole-product/2304x1296_melissa.jpg",
      "Something_Borrowed": "./assets/img/oleole-product/2304x1296_something_borrowed.jpg",
      "ZALIA": "./assets/img/oleole-product/2304x1296_zelia.jpg",
      "ZALORA": "./assets/img/oleole-product/2304x1296_zalora.jpg",
      "Fave": "./assets/img/oleole-product/fave_1.jpg",
      "Taobao_Collection": "./assets/img/oleole-product/taobao_collection.jpeg",
      "11street": "./assets/img/oleole-product/2304x1296_11_street.jpg",
      "LAZADA": "./assets/img/oleole-product/2304x1296_lazada.jpg",
      "Klook":"./assets/img/oleole-product/klook_2.jpeg",
      "Steam":"./assets/img/oleole-product/steam_1.jpeg"
    };


    if (this.oleoleGiftPartnerList != undefined) {
      this.isOleole = true; 
      console.log("ProductDetailsDesignGuidePage constructor() if  isOleole ", this.isOleole);
    }
    else {
      this.isOleole = false;
      console.log("ProductDetailsDesignGuidePage constructor() else isOleole ", this.isOleole);
    }

    console.log("ProductDetailsDesignGuidePage -> oleoleGiftPartnerList = ", this.oleoleGiftPartnerList);
    console.log("ProductDetailsDesignGuidePage -> isOleole = ", this.isOleole);

    // oleole subscription gift
    this.subscriptionGift = this.navParams.get('subscription_gift_details');
    console.log("ProductDetailsDesignGuidePage -> subscriptionGift = ", this.subscriptionGift);

    if (this.subscriptionGift != undefined) {
      this.isOleoleSuscription = true;
    }
    else {
      this.isOleoleSuscription = false;
    }

    console.log("ProductDetailsDesignGuidePage -> subscriptionGift = ", this.subscriptionGift);
    console.log("ProductDetailsDesignGuidePage -> isOleoleSuscription = ", this.isOleoleSuscription);
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Product details", this.renderer);
  }

  ionViewWillEnter() {
    this.numberToSendAndGet = '';
    this.personalizedMessage = '';
    this.countNum = 0;
    if(this.gift_request != 'buy_for_self')
      this.isOleoleProductEnable = true;

  }

  ionViewDidLoad() {

    this.chooseButton = true;
    this.checkKawKaw = this.navParams.get("param1");
    this.kawkawVariable = this.navParams.get("param2");
    this.pageName = this.navParams.get("param3");
    this.number = this.navParams.get("param4");

    if (this.kawkawVariable == "ECEMB")
      this.EcemPassDetails();
    console.log('ionViewDidLoad ProductDetailsDesignGuidePage');
  }

  addCssSub(index) {
    this.chooseButton = false;
    //this.cardValue = this.subTitle[index].value;
    this.subIndexValue = index;
    this.selectedProductID = this.productID[index];
    if (this.selectedProductID) {
      this.reloadbouns = false;
    }
  }

  EcemPassDetails() {
    this.loader = this.gatewayService.LoaderService();
    this.loader.present();

    this.selectedNumber = this.gatewayService.GetMobileNumber();
    var saveVariable = this.selectedNumber.charAt(0);
    if (saveVariable != "6") {
      this.selectedNumber = "6" + this.selectedNumber;
    }

    // var params = {
    //   "MobileNumber":this.selectedNumber
    // }

    //var testData = "60136430242";
    var params = {
      "MobileNumber": this.selectedNumber
    }

    this.reloadService.EcemPassDetails(params).then((res) => {
      this.loader.dismiss();
      console.log("*****EcemPassDetails*****" + JSON.stringify(res));
      this.planDetails = res["FreebiesInquiryResponse"].FREEBIES.PLAN;
      this.planDetails.forEach(element => {
        this.planDetailsName.push(element.PLAN_NAME);
        this.productID.push(element.PRODUCT_ID);
      });
    }).catch(err => {
      this.loader.dismiss();
      console.log("*****reload*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }
  checkRedeemBonus() {
    this.loader = this.gatewayService.LoaderService();
    this.loader.present();

    this.selectedNumber = this.gatewayService.GetMobileNumber();

    var saveVariable = this.selectedNumber.charAt(0);
    if (saveVariable != "6") {
      this.selectedNumber = "6" + this.selectedNumber;
    }
    //var testData = "60136430242";
    var params = {
      "MobileNumber": this.selectedNumber
    }

    this.reloadService.EcemPassDetails(params).then((res) => {

      console.log("*****EcemPassDetails*****" + JSON.stringify(res));
      this.loader.dismiss();
      if (res["FreebiesInquiryResponse"].NumberOfFreebies == "0") {
        this.alertNumber = "zero";
      } else if (res["FreebiesInquiryResponse"].NumberOfFreebies == "1") {
        this.alertNumber = "one";
      } else {
        this.alertNumber = "two";
      }
      this.ECEMAlert();

    }).catch(err => {
      this.loader.dismiss();
      this.ECEMAlert();
      // console.log("*****reload*******"+JSON.stringify(err));
      // if(err=="No internet connection")
      // return;
      //  this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }
  SubScribe() {

    let KawKawAlert = this.conformAlert.create({
      title: "YAY!",
      subTitle: "You have successfully joined Kaw Kaw Squad.",
      buttons: [
        {
          text: "DONE",
          role: "DONE",
          handler: () => {
            console.log("ALolow");
            this.navCtrl.setRoot(PrivilegesPage);
          }
        }
      ],
      cssClass: 'error-message'
    });

    console.log('ionViewDidLoad ReloadBonusPage');
    this.loader = this.gatewayService.LoaderService();
    //var testMobilenumber = "60138371348"
    this.loader.present();
    this.gatewayService.CheckyouthSUBs().then((res) => {
      this.loader.dismiss();
      console.log("*****Res*****" + JSON.stringify(res));
      if (res["STDNT_OPTIN"].ResponseMessage == "The proper route cannot be found.") {
        //   this.checkKawKaw = false;
        this.alertCtrlInstance.showAlert("Uh Oh. Kaw Kaw Squad Is Busy", "System fails to opt user in to kaw kaw squd.", "OK");
      } else if (res["STDNT_OPTIN"].ResponseMessage == "The product 40699 can not be subscribed to repeatedly.") {
        // this.checkKawKaw = true;                        
        this.alertCtrlInstance.showAlert("Uh Oh. Kaw Kaw Squad Is Busy", "The Product can not be subscribed to repleatedly.", "OK");
      } else if (res["STDNT_OPTIN"].ResponseMessage == "Success") {
        KawKawAlert.present();
        //   this.alertCtrlInstance.showAlert("YAY!", "You have successfully joined Kaw Kaw Squad.", "DONE");
        // this.checkKawKaw = true;
      } else {
        this.alertCtrlInstance.showAlert("Uh Oh. Kaw Kaw Squad Is Busy", "Please try again later", "OK");
      }


    }).catch(err => {
      this.loader.dismiss();
      console.log("*****reload*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Kaw Kaw Squad Is Busy", "Please try again later", "OK");
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }

  Claim() {
    this.checkRedeemBonus();
  }
  ECEMAlert() {

    this.loader = this.gatewayService.LoaderService();
    this.loader.present();

    this.selectedNumber = this.gatewayService.GetMobileNumber();
    var saveVariable = this.selectedNumber.charAt(0);
    if (saveVariable != "6") {
      this.selectedNumber = "6" + this.selectedNumber;
    }

    // var params = {
    //   "MobileNumber":this.selectedNumber,
    //   "productID":this.selectedProductID
    // }
    // var testData = "60136430242";
    var params = {
      "MobileNumber": this.selectedNumber,
      "productID": this.selectedProductID
    }
    this.reloadService.EcemSubmission(params).then((res) => {
      this.loader.dismiss();
      console.log("*****EcemPassDetails*****" + JSON.stringify(res));
      let alert = this.conformAlert.create({
        title: "Hurray! You Got Rewarded.",
        subTitle: "East Coast East Malaysia Bonus has been claimed " + res["MAXUPPurchaseResponse"].MSISDN,
        buttons: [
          {
            text: "Redeem another",
            role: "Redeem another",
            handler: () => {
              console.log("ALolow");
              //this.navCtrl.setRoot(ReloadBonusPage);
              this.navCtrl.setRoot(PrivilegesPage);
            }
          }, {
            text: "DONE",
            handler: () => {
              console.log("ALolow");
              // if( this.pageName = "privileges"){
              //   this.navCtrl.setRoot(PrivilegesPage);
              // }else if(this.pageName = "reloadPage"){
              //   this.navCtrl.setRoot(ReloadPage);
              // }
              // this.openTheContactConform();
              this.navCtrl.setRoot(HomePage);
            }
          }
        ],
        cssClass: 'error-message',
        enableBackdropDismiss: false
      });
      let singlealert = this.conformAlert.create({
        title: "Hurray! You Got Rewarded.",
        subTitle: "East Coast East Malaysia Bonus has been claimed " + res["MAXUPPurchaseResponse"].MSISDN,
        buttons: [
          {
            text: "DONE",
            role: "DONE",
            handler: () => {
              console.log("ALolow");
              //this.navCtrl.setRoot(ReloadBonusPage);
              // if( this.pageName = "privileges"){
              //   this.navCtrl.setRoot(PrivilegesPage);
              // }else if(this.pageName = "reloadPage"){
              //   this.navCtrl.setRoot(ReloadPage);
              // }
              this.navCtrl.setRoot(HomePage);
            }
          }
        ],
        cssClass: 'error-message',
        enableBackdropDismiss: false
      });

      if (res["MAXUPPurchaseResponse"].ResponseMessage == "Success") {

        if (this.alertNumber == "one") {
          singlealert.present();
        } else {
          alert.present();
        }

        // this.alertCtrlInstance.showAlert("BONUS REDEEMED", "East Coast East Malaysia Bonus has been claimed by"+res["MAXUPPurchaseResponse"].MSISDN, "DONE");
      } else {
        //alert.present();
        this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");
      }


    }).catch(err => {
      this.loader.dismiss();
      console.log("*****reload*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }

  // oleole buy gift
  requestGift() {

    this.isOleoleProductEnable = true;
    var loader = this.gatewayService.LoaderService();
    loader.present();

    var giftTypes: any = this.navParams.get('giftTypes');

    console.log("ProductDetailsDesignGuidePage -> requestGift() -> gift_request = ", this.gift_request);
    console.log("ProductDetailsDesignGuidePage -> requestGift() -> giftTypes = ", giftTypes);


    // console.log("ProductDetailsDesignGuidePage -> requestGift() -> numberToSendAndGet length =",islength.toString().length);

    // console.log("ProductDetailsDesignGuidePage -> requestGift() -> numberToSendAndGet length =" ,(this.numberToSendAndGet.toString()).length == 11);

    if(this.isSelf == false ){
      this.numberToSendAndGet = this.gatewayService.GetMobileNumber();
    }

    console.log("Check whether it is number valid or nor. = ", (/^[0-9]{11}$/.test(this.numberToSendAndGet)));



    if (this.gift_request == undefined || this.gift_request == '') {
      this.alertCtrlInstance.showAlert("", "Please select gift type.", "OK");
      return false;
    }
    if ((this.numberToSendAndGet == undefined || !(/^[0-9]{10,11}$/.test(this.numberToSendAndGet)))) {
      // // !(_.isNumber(this.numberToSendAndGet))
      this.alertCtrlInstance.showAlert("", "Number is not valid.", "OK");
      return false;
    }

    if ((this.numberToSendAndGet.substring(0, 3) == "011" && this.numberToSendAndGet.length == 10)) {

      this.alertCtrlInstance.showAlert("Uh Oh. Fingers Too Fast?", "Please enter a valid Celcom number (10 or 11 digits only)", "OK");
      return false;
    }

    var giftTypeHandle: any = '';
    var userInfo = this.dashboardService.GetUserInfo();

    // to remove 6 to validate number
    if (this.numberToSendAndGet.indexOf("6") != 0 && this.numberToSendAndGet.length == 10)
      this.numberToSendAndGet = this.numberToSendAndGet;
    else if (this.numberToSendAndGet.indexOf("6") == 0 && this.numberToSendAndGet.length == 11)
      this.numberToSendAndGet = this.numberToSendAndGet.substr(1);

    console.log("requestGift() -> isMaxUp =", this.navParams.get('isMaxUp'), "\n islegacy = ",this.navParams.get('islegacy') );

    // for inactive number
    if (this.gift_request != 'buy_for_self' && this.gift_request != undefined) {
      loader.dismiss();
      this.oleoleService.isInactiveNumber(this.numberToSendAndGet)
      .then((res) => {
        console.log(this.numberToSendAndGet," is ",res);

        // if active number
        if(res == true) {
          console.log("going forward ",res);

          // giftType
          if (giftTypes == 'undefined' || giftTypes == undefined) {
            console.log("ProductDetailsDesignGuidePage undefined -> requestGift() -> giftTypeHandle = ", giftTypeHandle);
            giftTypeHandle = (userInfo.ListOfServices.Services.Pre_Pos_Indicator).toUpperCase();
          }
          else {
            console.log("ProductDetailsDesignGuidePage not undefined-> requestGift() -> giftTypeHandle = ", giftTypeHandle);
            giftTypeHandle = giftTypes.p1;
          }

          console.log("ProductDetailsDesignGuidePage -> requestGift() -> giftTypeHandle = ", giftTypeHandle);


          // oleole MCP & Xpax
          console.log("ProductDetailsDesignGuidePage -> requestGift() =", this.subscriptionGift);

          console.log("ProductDetailsDesignGuidePage -> requestGift() -> giftTypes =", giftTypes);
          console.log("ProductDetailsDesignGuidePage -> requestGift() -> this.navParams.get('giftTypes') =", this.navParams.get('typeOfGift'));


          console.log("requestGift() -> Header titile = ", this.navParams.get('header_title'));
          
          console.log("requestGift() -> num 1 else = ", this.reloadService.gettingLoginNumber());
          console.log("requestGift() -> num 2 = else = ", this.gatewayService.GetMobileNumber());

          if (this.numberToSendAndGet.slice(-10) == this.gatewayService.GetMobileNumber() && (this.gift_request == 'buy_gift_for' || this.gift_request == 'request_gift_from')) {
            this.isOleoleProductEnable = false;
            this.alertCtrlInstance.showAlert("Uh Oh. Sorry, you cannot send an e-Gift to yourself.", "Thank you.", "OK");
          } else {
            this.isOleoleProductEnable = false;
            this.navCtrl.push(ConfirmationScreensPage, { "confirmDetails": this.subscriptionGift, "isRoaming": undefined, "isMaxUp": null, "numberToSendAndGet": this.numberToSendAndGet, "personalizedMessage": this.personalizedMessage, "gift_request": this.gift_request, "giftTypes": giftTypeHandle, "isVoucher": this.isVoucher, "header_title": this.navParams.get('header_title'),"msg":this.personalizedMessage});
          }

        }
      },(err) => {
        loader.dismiss();
        console.log("Error isInactiveNumber -> ",err);
      });
    }
    else {
      loader.dismiss();
      // var giftTypeHandle: any = '';
      // var userInfo = this.dashboardService.GetUserInfo();

      // giftType
      if (giftTypes == 'undefined' || giftTypes == undefined) {
        console.log("ProductDetailsDesignGuidePage undefined -> requestGift() -> giftTypeHandle = ", giftTypeHandle);
        giftTypeHandle = (userInfo.ListOfServices.Services.Pre_Pos_Indicator).toUpperCase();
      }
      else {
        console.log("ProductDetailsDesignGuidePage not undefined-> requestGift() -> giftTypeHandle = ", giftTypeHandle);
        giftTypeHandle = giftTypes.p1;
      }

      console.log("ProductDetailsDesignGuidePage -> requestGift() -> giftTypeHandle = ", giftTypeHandle);


      // oleole MCP & Xpax
      console.log("ProductDetailsDesignGuidePage -> requestGift() =", this.subscriptionGift);

      console.log("ProductDetailsDesignGuidePage -> requestGift() -> giftTypes =", giftTypes);
      console.log("ProductDetailsDesignGuidePage -> requestGift() -> this.navParams.get('giftTypes') =", this.navParams.get('typeOfGift'));

      console.log("requestGift() -> Header titile = ", this.navParams.get('header_title'));
      console.log("requestGift() -> num 1 else = ", this.reloadService.gettingLoginNumber());
      console.log("requestGift() -> num 2 = else = ", this.gatewayService.GetMobileNumber());

      if (this.numberToSendAndGet.slice(-10) == this.gatewayService.GetMobileNumber() && (this.gift_request == 'buy_gift_for' || this.gift_request == 'request_gift_from')) {
        this.alertCtrlInstance.showAlert("Uh Oh. Sorry, you cannot send an e-Gift to yourself.", "Thank you.", "OK");
        this.isOleoleProductEnable = false;
      } else {
        // this.navCtrl.push(ConfirmationScreensPage, { "confirmDetails": this.subscriptionGift, "isRoaming": undefined, "isMaxUp": null, "numberToSendAndGet": this.numberToSendAndGet, "personalizedMessage": this.personalizedMessage, "gift_request": this.gift_request, "giftTypes": giftTypeHandle, "isVoucher": this.isVoucher, "header_title": this.navParams.get('header_title') });
        this.isOleoleProductEnable = false;
        this.navCtrl.push(ConfirmationScreensPage, { "confirmDetails": this.subscriptionGift, "isRoaming": undefined, "isMaxUp": this.navParams.get('isMaxUp'), "islegacy":this.navParams.get('islegacy'),"numberToSendAndGet": this.numberToSendAndGet, "personalizedMessage": this.personalizedMessage, "gift_request": this.gift_request, "giftTypes": giftTypeHandle, "isVoucher": this.isVoucher, "header_title": this.navParams.get('header_title') });
      }
    }

    // this.navCtrl.parent.parent.push(ConfirmationScreensPage,{"confirmDetails":this.planDetails,"isRoaming":this.isRoaming,"isMaxUp":this.isMaxUp});    
  }

  voucherPlan(voucherPlanInfo) {

    // this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");

    // let smartPhonealert = this.conformAlert.create({
    //   // title: "Ole for Smartphones!",
    //   // message: "Only recipients using smartphones can receive and  enjoy their e-gifts.",
    //   title: "Awesome Choice.",
    //   message: "These e-Gifts can only be used by your family and friends who are using smartphone.",
    //   buttons: [
    //     {
    //       text: 'Cancel',
    //       handler: () => {
    //         console.log('Cancel clicked for smartphone');
    //         // return false;
    //       }
    //     },
    //     {
    //       text: 'OK Proceed',
    //       handler: () => {
    //         console.log('OK Proceed clicked for smartphone');
    //         this.smartPhoneProcessed(voucherPlanInfo);
    //       }
    //     }
    //   ]
    // });
    // smartPhonealert.present();

    if(voucherPlanInfo.PLAN_AMOUNT != "Out of Stock")
      this.smartPhoneProcessed(voucherPlanInfo);
  }

  smartPhoneProcessed(voucherPlanInfo) {


    if (this.subscriptionService.isCapZone()) {
      this.subscriptionService.presentConfirm();
    } else {

        console.log("ProductDetailsDesignGuidePage -> voucherPlan() -> voucherPlanInfo ", JSON.stringify(voucherPlanInfo));

        if (voucherPlanInfo.PRODUCT_ID != undefined) {
          this.isOleole = false;
          this.WhenShowOption = false;
          this.subscriptionGift = voucherPlanInfo;
          this.isOleoleSuscription = true;
        }
        else {
          this.isOleole = true;
          this.isOleoleSuscription = false;
        }

        console.log("ProductDetailsDesignGuidePage -> voucherPlan() -> isOleole", this.isOleole);
      }
  }

  handleKeyDone(e, giftInfo, msisdnInfo,isfromIonChange) {
    console.log("handleKeyDone giftInfo = ", giftInfo, " -> msisdnInfo = ", msisdnInfo);

    this.isGiftTyped = giftInfo;

    if(isfromIonChange == 'yes')
      this.numberToSendAndGet = '';

    // if(giftInfo == true && msisdnInfo == true)
    if (this.gift_request == 'buy_for_self') {
      this.isSelf = false;
      this.numberToSendAndGet = this.gatewayService.GetMobileNumber();
      this.isOleoleProductEnable = false;
    }
    else
      this.isSelf = true;

    if (giftInfo == true && msisdnInfo == true && (this.numberToSendAndGet != undefined && (/^[0-9]{10,11}$/.test(this.numberToSendAndGet))))
      this.isOleoleProductEnable = false;
    else if (giftInfo == true && msisdnInfo == false && (this.numberToSendAndGet != undefined && (/^[0-9]{10,11}$/.test(this.numberToSendAndGet))))
      this.isOleoleProductEnable = false;
    else
      this.isOleoleProductEnable = true;



    console.log("ProductDetailsDesignGuidePage -> handleKeyDone() -> (for selft)numberToSendAndGet =", this.numberToSendAndGet);

    console.log("handleKeyDone isOleoleProductEnable --", this.isOleoleProductEnable);
    console.log("handleKeyDone isGiftTyped --", this.isGiftTyped);


    if (e.which === 13) {
      this.keyboard.close();
    }

  }

  // Oleole Gift phone number add to send and request gift
  pickUpNumber(e, giftInfo, msisdnInfo) {
    // let modal = this.modalCtrl.create(AddNumberModalPage);
    // modal.present();
    // modal.onDidDismiss(data => { 
    //   console.log("ProductDetailsDesignGuidePage -> voucherPlan() -> pickUpNumber = ",JSON.stringify(data));
    //   this.numberToSendAndGet = data;
    // });

    console.log("ProductDetailsDesignGuidePage -> pickUpNumber() -> isSelf =", this.isSelf);

    // if (this.isSelf) {
    //   return false;
    // }

    this.numberToSendAndGet = "";
    this.contact.pickContact().then((response: Contact) => {

      //var checkLenght = response["_objectInstance"].phoneNumbers[0].value;

      var chooseMobileNumber: any = response["_objectInstance"].phoneNumbers[0].value;
      chooseMobileNumber = chooseMobileNumber.replace(/[^0-9]/g, "");
      this.numberToSendAndGet = chooseMobileNumber;
   
      console.log("Open the contacts response " + response);
      console.log("Open the contacts response chooseMobileNumber " + chooseMobileNumber);
      console.log("Open the contacts response chooseMobileNumber length " + chooseMobileNumber.length);
      console.log("Open the contacts response chooseMobileNumber isGiftTyped " + this.isGiftTyped);

      console.log("Open the contacts response chooseMobileNumber giftInfo " + giftInfo);
      console.log("Open the contacts response chooseMobileNumber msisdnInfo " + msisdnInfo);
      console.log("Open the contacts response chooseMobileNumber is numberToSendAndGet ",(this.numberToSendAndGet != undefined && (/^[0-9]{10,11}$/.test(this.numberToSendAndGet)))," number is ",this.numberToSendAndGet);

      
      // if((chooseMobileNumber.length == 10 || chooseMobileNumber.length == '10') || (chooseMobileNumber.length == 11 || chooseMobileNumber.length == '11') && this.isGiftTyped) {
      if (giftInfo == true && (this.numberToSendAndGet != undefined && (/^[0-9]{10,11}$/.test(this.numberToSendAndGet)))) {
        console.log("if ")
        this.isOleoleProductEnable = false;
      }
      else {
        console.log("else ")
        this.isOleoleProductEnable = true;
      }

      // if((chooseMobileNumber.length == 10 || chooseMobileNumber.length == '10') || (chooseMobileNumber.length == 11 || chooseMobileNumber.length == '11')) {
      //   console.log("if ")
      //   this.isOleoleProductEnable = false;
      // } 
      // else {
      //   console.log("else ")
      //   this.isOleoleProductEnable = true;
      // }

      console.log("isOleoleProductEnable ", this.isOleoleProductEnable);

    }, (err) => {
      console.log("Error Open the contacts response = ", JSON.stringify(err));
      if (err == 6 || err == '6') {
        this.isOleoleProductEnable = true;
      }
    });
  }

}
