import  {  DashboardService  }  from  '../../../pages/home/homeService';
import { Injectable } from '@angular/core';
import { HandleError } from '../../../global/handleerror';
import { GatewayService } from '../../../global/utilService';
import { ConstantData } from '../../../global/constantService';
import _ from "underscore";

@Injectable()
export class UpgardeLatestPlanService {

  planDetails: any;
  plan_type: any;
  plan_name: any = "";
  outer_keys: any;
  plan_details: any;
  check_plan_name:any;
  constructor(public handleerror: HandleError,
    public gatewayService: GatewayService,
    public dashboardService: DashboardService) {
    
  };
  getLatestPlanDetails(){
    
    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetLatestPlanDetails, "POST", {}).then((res) => {
       var latest_plans=res;
       console.log("latest plans:"+JSON.stringify(latest_plans) );
       delete latest_plans['isSuccessful']; 
        
        return resolve({
         latest_plan: latest_plans,
         plan_type: this.plan_type
        });
      })
        .catch(e => {
          console.log("component  failure" + e);
          reject("Latest plan not found.");
        });
    });
  }
  getPlanDetails() {
    var _that = this;
    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetPlanDetails, "POST", {}).then((res) => {
        var userInfo = _that.dashboardService.GetUserInfo();
        _that.plan_type = userInfo.ListOfServices.Services.ServiceType.toLowerCase();
        if (_that.plan_type == 'cnvrgtpostpaid')
          _that.plan_type = 'postpaid';
        _that.planDetails = res[_that.plan_type][userInfo.ListOfServices.Services.ProdPromName];
        _that.plan_name = res[_that.plan_type][userInfo.ListOfServices.Services.ProdPromName]["Display Name"];
        _that.check_plan_name = userInfo.ListOfServices.Services.PLAN;
        if (typeof _that.planDetails != "undefined") {
          _that.outer_keys = _.keys(_that.planDetails);
          var result = Object.keys(_that.planDetails).map(function (key) {
            return [key, _that.planDetails[key]];
          });
          for (var j in result) {
            if (_.isObject(result[j][1])) {
              var result1 = Object.keys(result[j][1]).map(function (key1) {
                return [key1, result[j][1][key1]];
              });
              result[j][1] = result1;
              for (var k in result1) {
                if (_.isObject(result1[k][1])) {
                  Object.keys(result1[k][1]).map(function (key2) {
                    return [key2, result1[k][1][key2]];
                  });
                }
              }
            }
            else {

            }
          }
          _that.plan_details = result;
        }
        return resolve({
          plan_details: _that.plan_details,
          plan_name: _that.plan_name,
          plan_type: _that.plan_type,
          check_plan_name: _that.check_plan_name
        });
      })
        .catch(e => {
          console.log("component  failure" + e);
          reject("Plan not found.");
        });
    });


  }


  submitUpgradeRequest() {
    try {
      var params = {
        "mobNumber":  this.gatewayService.GetMobileNumber()
      }
      return new Promise((resolve, reject) => {
        this.gatewayService.CallAdapter(ConstantData.adapterUrls.UpgradePlanDetail, "POST", params).then((res) => {
          resolve(res);
        }).catch(function (e) {
          reject(e);
        });
      });
    } catch (e) {
      console.log("UpgardeLatestPlanService -> submitUpgradeRequest : Error occured");
    }
  }




}   