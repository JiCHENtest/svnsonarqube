import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UpgardeLatestPlanService } from './upgrade-latest-planService';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import _ from "underscore";
import { DashboardService } from '../../home/homeService';
import { GatewayService } from '../../../global/utilService';


@Component({
  selector: 'page-upgrade-latest-plan',
  templateUrl: 'upgrade-latest-plan.html',
})
export class UpgradeLatestPlanPage {

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public upgradeService: UpgardeLatestPlanService,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public dashboardService: DashboardService,
    private renderer: Renderer2,
    public gatewayService: GatewayService) {
    var res = this.navParams.data;
    this.plan_details = res["plan_details"];
    this.plan_name = res["plan_name"];
    this.plan_type = res["plan_type"];

  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Upgrade latest plan service", this.renderer);
  }

  plan_details: any;
  errorDetail: string = "";
  plan_name: any;
  plan_type: any;
  planDetails:any;
  outer_keys:any;

  ionViewDidLoad() {
    var userInfo = this.dashboardService.GetUserInfo();
    this.plan_type = userInfo.ListOfServices.Services.ServiceType.toLowerCase();
    if (this.plan_type == 'cnvrgtpostpaid')
    this.plan_type = 'postpaid';

    var loader = this.loadingCtrl.create({
      content: "Loading"
    });
    var _that = this;
    loader.present().then(() => {
    //getLatestPlanDetails
    _that.upgradeService.getLatestPlanDetails().then((res) => {
      console.log("upgrade latest plan page latest plans"+JSON.stringify(res));
      _that.planDetails=res["latest_plan"];
      console.log(_that.planDetails);
      _that.plan_name = _that.planDetails["Display Name"];
      
      _that.outer_keys = _.keys(_that.planDetails);
      
      //var plans = _that.outer_keys;
      if(_that.planDetails != undefined){
        var result = Object.keys(_that.planDetails).map(function(key) {
          return [key, _that.planDetails[key]];
        });
      }
      
      console.log(result);

      for(var j in result){
        //console.log(result[j]);
        if(_.isObject(result[j][1])){
          var result1 = Object.keys(result[j][1]).map(function(key1) {
            return [key1, result[j][1][key1]];
            
          });
          result[j][1] = result1;
          //console.log(result[j][1]);
          for(var k in result1)
          {
            if(_.isObject(result1[k][1])){
              Object.keys(result1[k][1]).map(function(key2) {
                return [key2, result1[k][1][key2]];
                
              });
              //result[j][1] = result1;
              //result[j][1][1] = result2;
            }

          }
          
        }
        else{

        }
      }

      console.log("latest plan_details are+++++++++++++++++"+JSON.stringify(result));
      _that.plan_details = result;
      
      //console.log(_.keys(_that.planDetails));
      console.log(_that.planDetails);
      loader.dismiss();
     
    }, (err) => {
      console.log("UpgradeLatestPlanPage -> latest plans: Error occured")
      loader.dismiss();
    });
    
  });
  }

  upgrade() {
    var loader = this.loadingCtrl.create({
      content: "Loading"
    });
    loader.present().then(() => {
      this.upgradeService.submitUpgradeRequest().then((res) => {
        console.log(res);
        loader.dismiss();
        if (res["msgStatus"] == "Success") {
          var msg = res["msgText"];
          this.presentsucess(msg);
        } else {
          this.presenterror("Please try again later");
          // : "You have already migrated to PB10980 plan. [ErrCR00029]")
        }
      }, (err) => {
        console.log("UpgradeLatestPlanPage -> upgrade : Error occured")
        loader.dismiss();
      });
    });
    //this.navCtrl.push(ProductDetailsDesignGuidePage);  
  }

  presentsucess(msg) {
    const alert = this.alertCtrl.create({
      title: 'Plan Update Success',
      message: msg,
      buttons: ['Ok'],
      cssClass: 'success-message error-message'
    });
    alert.present();
  }

  presenterror(messageInput) {
    const alert = this.alertCtrl.create({
      title: 'Uh Oh. Xpax is Busy',
      message: messageInput,
      buttons: ['Ok'],
      cssClass: 'success-message error-message'
    });
    alert.present();
  }

  isArray(obj: any) {
    console.log(obj);
    return Array.isArray(obj)
  }

}
