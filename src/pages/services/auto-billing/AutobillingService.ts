import { ConstantData} from "../../../global/constantService";
import { GatewayService } from '../../../global/utilService';
import { Injectable ,Input} from '@angular/core';
import { Network } from '@ionic-native/network';
import { AlertController, LoadingController } from 'ionic-angular';
import { HandleError } from '../../../global/handleerror';
import { Events } from 'ionic-angular';
import { AlertService } from '../../../global/alert-service';
import { JsonstoreService } from '../../../global/jsonstore';
import { ReloadService } from '../../../global/reloadServices';

@Injectable()
export class AutoBillingService{
  mobileNumber:any;
  customerRowId:any;
  contactRowID:any;
  billingAccountRowId:any;
  billingAccountNo:any;
  creditCardToken:any;
  creditCardTokenType:any;
  celCreditCardType:any;
  celPaymentMethod:any;
  loginMobileNumber:any;
  BillingProfileId:any;
  dropdownNumber:any;

  constructor(private network: Network,public alertCtrl:AlertController,private alertCtrlInstance: AlertService, public events: Events,public loadingCtrl:LoadingController,public handleerror: HandleError, public gatewayService:GatewayService, public jsonstoreService: JsonstoreService, public reloadService:ReloadService){
    this.loginMobileNumber=this.reloadService.gettingLoginNumber(); 
    
    console.log("dropdown number in autobilling service:"+this.dropdownNumber );
    // var mobile_number = {};
    // this.jsonstoreService.getData(mobile_number, "mobilenumber").then(
    //   (result_mob) => {
    //     console.log("mobile_number update() ", JSON.stringify(result_mob));
   
    //     this.loginMobileNumber = result_mob.length > 0 ? result_mob[0].json.mobilenum : "";
    //    });
   
    //this.mobileNumber=this.gatewayService.GetMobileNumber();
    //this.selectedMobileNumber=gatewayService.GetSelectMobileNumber();
    events.subscribe('customerRowId', (customerRowId) => {
      console.log('In service::'+ customerRowId);
      this.customerRowId=customerRowId;
    });
    events.subscribe('contactRowID', (contactRowID) => {
      console.log('In service::'+ contactRowID);
      this.contactRowID=contactRowID;
    });
    events.subscribe('billingAccountRowId', (billingAccountRowId) => {
      console.log('In service billingAccountRowId::'+ billingAccountRowId);
      this.billingAccountRowId=billingAccountRowId;
    });
    events.subscribe('billingAccountNo', (billingAccountNo) => {
      console.log('In service billingAccountNo::'+ billingAccountNo);
      this.billingAccountNo=billingAccountNo;
    });
    events.subscribe('creditCardToken', (creditCardToken) => {
      console.log('In service creditCardToken::'+ creditCardToken);
      this.creditCardToken=creditCardToken;
    });
    events.subscribe('creditCardTokenType', (creditCardTokenType) => {
      console.log('In service creditCardTokenType::'+ creditCardTokenType);
      this.creditCardTokenType=creditCardTokenType;
    });
    events.subscribe('celCreditCardType', (celCreditCardType) => {
      console.log('In service celCreditCardType::'+ celCreditCardType);
      this.celCreditCardType=celCreditCardType;
    });
    events.subscribe('celPaymentMethod', (celPaymentMethod) => {
      console.log('In service celPaymentMethod::'+ celPaymentMethod);
      this.celPaymentMethod=celPaymentMethod;
    });
    events.subscribe('BillingProfileId', (BillingProfileId)=> {
      console.log('In service BillingProfileId::'+ BillingProfileId);
      this.BillingProfileId=BillingProfileId;
    });
   };


   customerDetails_AutoBilling(addedNumber) {
    var _that = this;
    //alert("fetch info mobile number"+this.mobileNumber);
    try{
        var params = {
          "SERIAL_NUM": addedNumber//this.localStorage.get("MobileNum","")
        }
        return new Promise((resolve, reject) => {
          this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetUserInfo,"POST",params).then(function(res){
            console.log("*********************************** component success customerDetails_AutoBilling"+JSON.stringify(res));
            _that.handleerror.handleErrorFetchUserInfo(res,"FetchUserInfo").then((result) => {
              if(result == true) {
                console.log("hai if ",result);
                resolve(res);
              }
              else {
                console.log("hai else ",result);
                reject("error : no data found.");
              }
  
            },(err)=>{
              console.log("hai else err ",err);
              reject("error : no data found.");              
            });
          })
          .catch(function(e){
        
              console.log("******************************* component  failure customerDetails_AutoBilling"+e);
              //alert("error user info FetchUserInfo");
              
              reject(e);
          });
        });
    }catch(e){
      //alert("error"+JSON.stringify(e));
    }
  }

  billingTokenizer(param){
    var _that = this;
  try{
    var params = {"loginID":param.MSISDN};
    
  return new Promise((resolve, reject) => {
  
  this.gatewayService.CallAdapter(ConstantData.adapterUrls.autobillingTokenizerURL, "POST", params).then(function (res) {
  console.log("*********************************** component  success autobillingTokenizerURL"+JSON.stringify(res));
  resolve(res);
  })
  .catch(function(e){
    //this.alertCtrlInstance.showAlert("", " System down, cant call autobilling Tokenizer", "Done");
   
  console.log("******************************* component  failure autobillingTokenizerURL"+e);
  //alert("error user info");
  reject(e);
  });
  });
  }catch(e){
  //alert("error"+JSON.stringify(e));
  }
  }
 
  fetchTransactionDetails(loginNumber,amount, orderID){
    try{
      var params = {
            "loginID":loginNumber,
            "orderId":orderID,
            "msisdn":loginNumber,
            "totalAmount":amount,
            "isKadCeria":0,
            "credit":false
           };
        return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.fetchTransDetails,"POST",params).then((res)=> {
        console.log("transaction details params ::"+"loginid:"+params.loginID+" OrderId: "+params.orderId+ " msisdn: "+params.msisdn+" total amount: "+params.totalAmount+" isKadceria: "+params.isKadCeria+" credit: "+params.credit);
        console.log("*********************************** component fetchTransactionDetails"+JSON.stringify(res));

       //alert("AlertFine"+res);
        resolve(res);
        
      })
      .catch(function(e){
        console.log("******************************* component failure fetchTransactionDetails"+e);
       
        reject(e);
    });
    });
    }catch(e){
      //alert("error"+JSON.stringify(e));
    }
  }

autobillingUpdateProfile(selectedNumber){
    try{
      //alert("SelectedNumber"+selectedNumber)
      // var getMobileNumber = "0136426573";
      var params = {
        "createdBy":this.customerRowId,
        "accountId":this.customerRowId,
        "area":"Billing Profile",
        "contactId":this.contactRowID,
        "srType": "Account Profile Management",
        "status": "Unassigned",
        "subArea": "Update Payment Method",
        "ttSource": "First",
        "serviceNumber":selectedNumber,
        "billingAccountId": this.billingAccountRowId,
        "celPaymentMethod": this.celPaymentMethod,
        "creditCardToken":this.creditCardToken,
        "creditCardTokenType":this.creditCardTokenType,
        "celCreditCardType":this.celCreditCardType
        };
      
        return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.updateProfile,"POST",params).then((res)=> {
        console.log("Params for autobilling updateProfile is"+ JSON.stringify(params));
        console.log("*********************************** component autobillingUpdateProfile autobillingToggle"+JSON.stringify(res));
       //alert("AlertFine"+res);
        resolve(res);
      })
      .catch(function(e){
        console.log("******************************* component  autobillingUpdateProfile failure autobillingToggle"+e);
       // alert("ERROR"+e);
        //alert("error user info");
        reject(e);
    });
    });
    }catch(e){
      //alert("error"+JSON.stringify(e));
    }
  }

  fetchSavedCards(param){
    try{
      //alert("SelectedNumber"+selectedNumber)
      // var getMobileNumber = "0136426573";
      var params = {
            "loginID":param,
           };
           //fetchSavedCards
        return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.fetchSavedCardsAutoBilling,"POST",params).then((res)=> {
        
        console.log("*********************************** component fetchSavedCards"+JSON.stringify(res));

       //alert("AlertFine"+res);
        resolve(res);
        
      })
      .catch(function(e){
        console.log("******************************* component failure fetchSavedCards"+e);
       
        reject(e);
    });
    });
    }catch(e){
      //alert("error"+JSON.stringify(e));
    }
  }

  fetchBillDetails(selectedNumber){
    try{
      //,this.BillingProfileId, this.billingAccountNo,this.billingAccountRowId
    //alert("SelectedNumber"+selectedNumber)
    // var getMobileNumber = "0136426573";
    var params = {
          "MSISDN":selectedNumber,
          "billingAccountNumber":this.billingAccountNo,
          "BillingProfileId":this.BillingProfileId,
          "BARowID":this.billingAccountRowId
      };
      return new Promise((resolve, reject) => {
    this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetBillDueDetails,"POST",params).then((res)=> {
      console.log("params in autobilling for getting autobilling flag"+JSON.stringify(params));
      console.log("*********************************** component in autobilling GetBillDueDetails FetchUserInfo"+JSON.stringify(res));
     //alert("AlertFine"+res);
      resolve(res);
    })
    .catch(function(e){
      console.log("******************************* component  in autobilling GetBillDueDetails failure FetchUserInfo"+e);
     // alert("ERROR"+e);
      //alert("error user info");
      reject(e);
  });
  });
  }catch(e){
    //alert("error"+JSON.stringify(e));
  }
  }

}
