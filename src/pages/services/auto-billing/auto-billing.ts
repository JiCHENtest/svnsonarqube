import { Component, Input, NgZone, Renderer2 } from '@angular/core';
import { NavController, NavParams, ModalController, Platform } from 'ionic-angular';
import { GatewayService } from '../../../global/utilService';
import { AlertService } from '../../../global/alert-service';
import { ChangeCreditcardModalPage } from "../../change-creditcard-modal/change-creditcard-modal";

import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ConstantData } from "../../../global/constantService";
import { HandleError } from "../../../global/handleerror";
import * as sha1 from "sha1";
import { DatePipe } from '@angular/common';
import { Events } from 'ionic-angular';
import { AutoBillingService } from './AutobillingService';
import { PayBillService } from '../../pay-bill/payBillService';
import { AlertController, LoadingController } from 'ionic-angular';
import { ReloadService } from '../../../global/reloadServices';


/**
 * Generated class for the AutoBillingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-auto-billing',
  templateUrl: 'auto-billing.html',
})
export class AutoBillingPage {

  selectedCard: string;
  selectedNumber: any;
  loader: any;
  customerRowId: any;
  contactRowID: any;
  billingAccountRowId: any;
  billingAccountNo: any;
  creditCardToken: any;
  creditCardTokenType: any;
  celCreditCardType: any;
  celPaymentMethod: any;
  BillingProfileId: any;
  isToggled: any;
  //isCardSaved;
  cards: any;
  cardType: any;
  second: string = "(Default)";
  myVar: boolean = true;
  Cardflag: boolean = true;
  cardArray: any;
  myCard: any;
  selectedCard1: any;
  MaskedStr: any;
  autobillingFlag: any;
  isAddedNow: any;
  myflag: any;
  do_deactivate: any;
  noAction: any;
  orderId: any;
  anyvar:boolean=false;
  dropdownNumber:any;

  //carArray = ["413718xxxxxx5918","0981237891234345","98545678912345666","2299447891234745"];


  constructor(public navCtrl: NavController, public paybillService: PayBillService, public zone: NgZone, 
    public theInAppBrowser: InAppBrowser, public events: Events, public navParams: NavParams, 
    public modalCtrl: ModalController, public autobillingService: AutoBillingService, 
    private alertCtrlInstance: AlertService, public gatewayService: GatewayService, 
    public alertCtrl: AlertController, public reloadService: ReloadService,
    public platform: Platform, private renderer: Renderer2) {
    //this.selectedCard = this.carArray[0];
    this.loader = this.gatewayService.LoaderService();
    this.dropdownNumber = this.gatewayService.GetMobileNumber();
    this.selectedNumber = this.reloadService.gettingLoginNumber();
    this.isAddedNow = false;
    this.myflag = false;
    this.do_deactivate = true;
  

    this.events.publish('isAutobilling', true);

  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Auto billing service", this.renderer);
  }

  ionViewDidLoad() {
    // this.Cardflag = false;
    console.log('ionViewDidLoad AutoBillingPage');
    var _that = this;
    //var staticNumber = "0132772997";
    //this.staticNumber=this.gatewayService.GetMobileNumber();
    this.customerDetails().then((res) => {
      this.getAutobillingFlag().then((autobillingFlag) => {
        if (autobillingFlag == "Cash") {
          this.isToggled = false;

        } else {
          this.isToggled = true;

        }
      }).catch(err => {

        console.log("*****In flag toggle*******" + JSON.stringify(err));
        this.loader.dismiss();

        if (err == "No internet connection")
          return;
        //this.alertCtrlInstance.showAlert("Autobilling info", "No data found", "ok");
        this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
        // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
      });

    }).catch(err => {

      console.log("*****Customer details*******" + JSON.stringify(err));
      this.loader.dismiss();

      if (err == "No internet connection")
        return;
      //this.alertCtrlInstance.showAlert("Autobilling info", "No data found", "ok");
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");

      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });

    this.saveCardsfetch();



  }


  saveCardsfetch() {
    this.loader.present();
    this.autobillingService.fetchSavedCards(this.selectedNumber).then((res) => {
      console.log("*************** response from fetch saved cards *********** " + JSON.stringify(res));
      this.loader.dismiss();
      //this.isCardSaved = ;
      if (res["savedCards"] == undefined) {
        this.Cardflag = true;
      }
      else if (res["savedCards"].length >= 1) {
        this.Cardflag = false;
        var allcards = res["savedCards"];
        console.log("*************** all cards *********** " + JSON.stringify(allcards));
        try {
          for (var i = 0; i < allcards.length; i++) {
            var elem = {};
            //var re = /X/gi;
            //allcards[i].masked = allcards[i].masked.replace(re,"-");
            console.log('this.GetCardType(allcards[i].masked)' + this.GetCardType(allcards[i].masked));
            this.cardType = this.GetCardType(allcards[i].masked);
            allcards[i].cardtype = this.cardType;
            console.log('added card type' + JSON.stringify(allcards[i]));
            //alert("allcards[i].is_default "+allcards[i].is_default);
            if (allcards[i]) {
              // alert("selected card "+JSON.stringify(allcards[i]));
              // _that.selectedCard1 = allcards[i];
              this.MaskedStr = allcards[0].masked;
              this.creditCardToken = allcards[0].TOKEN;
              console.log("creditCardToken" + this.creditCardToken);
              this.events.publish('creditCardToken', this.creditCardToken);
              this.creditCardTokenType = "Credit Card";

              this.events.publish('creditCardTokenType', this.creditCardTokenType);
              this.celCreditCardType = allcards[i].cardtype;
              this.events.publish('celCreditCardType', this.celCreditCardType);
              console.log("celCreditCardType" + this.celCreditCardType);
            }
            //this.cards.push('');
          }
        } catch (e) {
          console.log("*****saveCards Error*******" + JSON.stringify(e));
          if (e == "No internet connection")
            return;
        //  this.alertCtrlInstance.showAlert("Fetch saved cards for Autobilling", "No data found.", "OK");
        this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
          //alert(e);
        }
        this.cards = allcards;
      } else {
        this.Cardflag = true;
      }



    }).catch(err => {
      this.loader.dismiss();
      console.log("*****saveCards Error*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      // this.alertCtrlInstance.showAlert("Fetch saved cards for autobilling", "No data found.", "OK");
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }
  cardShow(selectedCard) {
    var cardShow = selectedCard.replace(/.(?=.{4})/g, '*');
    return cardShow;
  }


  notify(event) {
    console.log("anyvar: " + event);
  
  
    
      if (this.Cardflag == false) {
        if (this.isToggled == true) {
          this.celPaymentMethod = "Credit Card";
          this.events.publish('celPaymentMethod', this.celPaymentMethod);
          this.showAlertToActivateDeactivate1("Yay! You've Chosen the Best Way to Pay", "Activate Auto Billing and you'll never miss a bill payment again.", ["Cancel", "Activate"]);
          // this.autobillingUpdate();
          //To activate auto-billing, please add a valid credit card.
        } else if (this.isToggled == true && this.isAddedNow == true) {
          this.celPaymentMethod = "Credit Card";
          this.events.publish('celPaymentMethod', this.celPaymentMethod);
          this.showAlertToActivateDeactivate("Yay! You've Chosen the Best Way to Pay", "Credit Card Added.Activate Auto Billing to avoid missed bill payments.", ["Activate", "Cancel"]);

        }
        else {
          if (this.isToggled == false) {
            this.celPaymentMethod = "Cash";
            this.events.publish('celPaymentMethod', this.celPaymentMethod);
            this.showAlertToActivateDeactivate("Giving Auto Billing a Break?", " Sure you want to deactivate Auto Billing? You might miss bill payments", ["Deactivate", "Cancel"]);
            // this.autobillingUpdate();
            this.isToggled = false;

          }

        }
      }
      else {
        if (this.isToggled == true) {

          console.log("Toggled: " + this.isToggled);
          this.showAlertToHandleNavigate("Yay! You've Chosen the Best Way to Pay", "To activate auto-billing, please add a valid credit card.", ["Add Card", "Cancel"]);

        } else if (this.isToggled == false) {
          this.celPaymentMethod = "Cash";
          this.events.publish('celPaymentMethod', this.celPaymentMethod);

          this.showAlertToActivateDeactivate("Giving Auto Billing a Break?", " Sure you want to deactivate Auto Billing? You might miss bill payments", ["Deactivate", "Cancel"]);
          // this.autobillingUpdate();
          this.isToggled = false;


        }

      }

  

  }

  toggleList() {
    event.stopPropagation();
    this.myVar = !this.myVar;
  }

  presenChangeCreditModal() {
    let getCardArrayCard = this.cards;
    let myCard = this.selectedCard1;

    let modal = this.modalCtrl.create(ChangeCreditcardModalPage, { myCard, getCardArrayCard });
    modal.present();

    modal.onWillDismiss(data => {
      console.log("data from change credit card modal" + JSON.stringify(data));
      if (data != null) {
        if (data["masked"]) {
          this.MaskedStr = data.masked;
          this.creditCardToken = data.TOKEN;
          this.events.publish('creditCardToken', this.creditCardToken);
          this.celCreditCardType = data.cardtype;
          this.events.publish('celCreditCardType', this.celCreditCardType);
          this.creditCardTokenType = "Credit Card";
          this.events.publish('creditCardTokenType', this.creditCardTokenType);
        } else {
          this.Cardflag = true;
          this.myflag = true;
          this.creditCardToken = "";
          this.MaskedStr = "Use new card";

        }


      }
    })
  }
  GetCardType(number) {
    // visa
    var re = new RegExp("^4");
    if (number.match(re) != null) {
      return "Visa";
    }
    // Mastercard 
    // Updated for Mastercard 2017 BINs expansion
    else if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number)) {
      return "Mastercard";
    }

    return "";
  }
  getAutobillingFlag() {
    this.loader.present();
    return new Promise((resolve, reject) => {
      this.autobillingService.fetchBillDetails(this.selectedNumber).then((res) => {

        console.log('get Autobilling Flag ******' + JSON.stringify(res));
        this.loader.dismiss();

        var autobillingFlag = res["Envelope"].Body.FirstAppUsageResponse.ResponseBody.BADetailsRetrieve.PaymentMethod;

        resolve(autobillingFlag);

      }).catch(err => {
        this.loader.dismiss();
        console.log("***autobilling *******" + JSON.stringify(err));
        if (err == "No internet connection")
          return;
          this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
        reject(err);
        //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
      });
    });

  }



  customerDetails() {

    this.loader.present();

    return new Promise((resolve, reject) => {
      this.autobillingService.customerDetails_AutoBilling(this.selectedNumber).then((res) => {
        this.loader.dismiss();
        console.log("inside customerDetails_AutoBilling " + JSON.stringify(res));

        this.customerRowId = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.CustomerRowId;
        console.log("customerRowId::" + this.customerRowId);
        this.events.publish('customerRowId', this.customerRowId);
        this.contactRowID = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.ContactRowID;
        console.log("contactRowID::" + this.contactRowID);
        this.events.publish('contactRowID', this.contactRowID);
        this.billingAccountRowId = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.ListOfServices.Services.AssetBillingAccountRowId;
        console.log("billingAccountRowId::" + this.billingAccountRowId);
        this.events.publish('billingAccountRowId', this.billingAccountRowId);
        this.billingAccountNo = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.ListOfServices.Services.AssetBillingAccountNo;
        console.log("billingAccountNo::" + this.billingAccountNo);
        this.events.publish('billingAccountNo', this.billingAccountNo);

        //SBLCustomerRetrieveENTOutput=ListOfServices=Services= BillingProfileId : "1-2DCKRY0"
        this.BillingProfileId = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.ListOfServices.Services.BillingProfileId;
        console.log("BillingProfileId::" + this.BillingProfileId);
        this.events.publish('BillingProfileId', this.BillingProfileId);


        resolve(res);

      }).catch(err => {
        this.loader.dismiss();
        console.log("*****Autobilling details fetch*******" + JSON.stringify(err));
        if (err == "No internet connection")
          return;
        //this.alertCtrlInstance.showAlert("Autobilling info", "No data found", "ok");
        this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
        reject(err);
        // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
      });
    });
  }

  autobillingUpdate() {

    // if(this.Cardflag==true){

    //  var loader =this.gatewayService.LoaderService();
    // loader.present().then(()=>{
    this.loader = this.gatewayService.LoaderService();
    this.loader.present();
    this.autobillingService.autobillingUpdateProfile(this.selectedNumber).then((res) => {
      this.loader.dismiss();
      console.log("inside autobillingUpdateProfile " + JSON.stringify(res));
      var serialNo = res["outputUpdateProfileResp"].srNumber;
      var status = res["outputUpdateProfileResp"].status;
      this.getAutobillingFlag().then((autobillingFlag) => {
        if (this.isToggled == false) {
          //  loader.dismiss();
          if (autobillingFlag == "Cash") {
            this.isToggled = false;

            this.showAlertOK("Autobilling info", "Auto billing deactivated successfully!!", "OK");
          } else {
         
            this.anyvar=false;
            this.isToggled = true;
            this.alertCtrlInstance.showAlert("Autobilling info", "Sorry!! Failed to deactivate auto billing", "OK");
          }

        } else {
          //  loader.dismiss();
          if (autobillingFlag == "Cash") {
      
            this.anyvar=false;
            this.isToggled = false;

            this.alertCtrlInstance.showAlert("Autobilling info", "Sorry!! Failed to activate auto billing !!", "OK");
            //   this.methodOne();
          } else {
            this.isToggled = true;

            this.showAlertOK("Autobilling info", "Auto billing activated successfully!!", "OK");


          }

        }

      }).catch(err => {
        this.loader.dismiss();
        console.log("***autobilling *******" + JSON.stringify(err));
        if (err == "No internet connection")
          return;
       // this.alertCtrlInstance.showAlert("Autobilling Info", "No data found.", "OK");
       this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
        //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
      });


    }).catch(err => {

      console.log("*****Autobilling toggle*******" + JSON.stringify(err));
      this.loader.dismiss();
      this.isToggled = false;

      if (err == "No internet connection")
        return;
      //this.alertCtrlInstance.showAlert("Autobilling info", "No data found", "ok");
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");

      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
    // });


  }
  getPaymentTokenizerData() {
    this.loader = this.gatewayService.LoaderService();
    var params = {
      "MSISDN": this.reloadService.gettingLoginNumber()
    };
    this.loader.present();
    this.autobillingService.billingTokenizer(params).then((res) => {
      this.loader.dismiss();
      this.Cardflag = true;
      console.log("*********** component success billingTokenizer" + JSON.stringify(res));

      this.orderId = res["orderId"];
      console.log("order id is::" + this.orderId);
      var paymentURL = res["paymentURL"];
      console.log("paymentURL in autobilling" + paymentURL);
      //var str = 'celcomFirstcelcomFirstCA201711162323343056UJctp1100';
      var resUrl = res["responseURL"];




      if (paymentURL == undefined || paymentURL == "" || paymentURL == null) {

        this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");
        return;
      } else {
        const browser = this.theInAppBrowser.create(paymentURL, '_blank', 'location=no');
        browser.show();



        browser.on("exit").subscribe((e) => {
          console.log("exit" + e.url);

          this.getTransactionDetails();

          //this.alertCtrlInstance.showAlert("Notification", this.dueBillAmount + " has been settled bill for " + this.mobilenumber, "OK");
        }, err => {
          console.log("error" + err);
        });


        browser.on("loadstop").subscribe((e) => {

          console.log("loadstop" + e.url);
          if (e.url.startsWith(resUrl)) {
            browser.close();
          } else {

          }
        }, err => {
          console.log("error" + err);
        });

        browser.on("loaderror").subscribe((e) => {
          browser.close();
          console.log("loaderror" + e.url);
        }, err => {
          console.log("error" + err);
        });

      }
    })
      .catch(err => {
        this.loader.dismiss();
        console.log("*************** component failure getPaymentTokenizerData" + err);
        if (err == "No internet connection")
          return;
        this.alertCtrlInstance.showAlert("Uh Oh. We Couldn't Save Your Card.", "Please try again later", "OK");


      });
  }

  getTransactionDetails() {
    // alert("Gettting "+selectedNumber);

    var dueBillAmount = 0;
    var _that = this;
    _that.loader.present();
    this.autobillingService.fetchTransactionDetails(this.selectedNumber, dueBillAmount, this.orderId).then((res) => {

      console.log('fetchTransactionDetails ******' + JSON.stringify(res));
      var isSuccessful = res["isPaymentSuccess"];
      var reasonCode = res["resultSet"][0].REASONCODE;

      _that.loader.dismiss();
      _that.saveCardsfetch();
      if (reasonCode == 14000) {

        this.isAddedNow = true;
        //this.events.publish('updateBalance', this.mobilenumber);
        _that.showAlertOK("Yay", "Card Saved!! ", "OK");
      } else if (reasonCode == 50003) {
        _that.alertCtrlInstance.showAlert("Notification", "Field is missing ", "OK");
      } else if (reasonCode == 50004) {
        _that.alertCtrlInstance.showAlert("Notification", "Invalid store ", "OK");
      } else if (reasonCode == 50009) {
        _that.alertCtrlInstance.showAlert("Notification", "Signature not match ", "OK");
      } else if (reasonCode == 51007) {
        _that.alertCtrlInstance.showAlert("Notification", "Credit card number is blacklisted ", "OK");
      } else if (reasonCode == 54002) {
        _that.alertCtrlInstance.showAlert("Notification", "Store do not have tokenizer as their payment method ", "OK");
      } else if (reasonCode == 54003) {
        _that.alertCtrlInstance.showAlert("Notification", "Token not exist ", "OK");
      } else if (reasonCode == 54004) {
        _that.alertCtrlInstance.showAlert("Notification", "Token have invalid expiry date ", "OK");
      } else if (reasonCode == 54005) {
        _that.alertCtrlInstance.showAlert("Notification", "Credit card number invalid", "OK");
      } else if (reasonCode == 54006) {
        _that.alertCtrlInstance.showAlert("Notification", "Credit card expiry date invalid", "OK");
      } else if (reasonCode == 54009) {
        _that.alertCtrlInstance.showAlert("Notification", "Token is blacklist", "OK");
      } else if (reasonCode == 54010) {
        _that.alertCtrlInstance.showAlert("Notification", "Credit card type not supported", "OK");
      }
      else {
        _that.alertCtrlInstance.showAlert("Uh Oh. We Couldn't Save Your Card.", "Please try again later", "OK");
        // this.alertCtrlInstance.showAlert("Notification", "TRANSACTION FAILED !!!", "OK");
      }


    }).catch(err => {
      console.log("***paybill*******" + JSON.stringify(err));
      _that.loader.dismiss();
      if (err == "No internet connection")
        return;
      _that.alertCtrlInstance.showAlert("Uh Oh. We Couldn't Save Your Card.", "Please try again later", "OK");
      //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }

  showAlertToHandleNavigate(title,  msg,  btn) {
    if (Array.isArray(btn)) {

      try {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          buttons: [{
            text: btn[0],
           cssClass:'submit-button',
            handler: () => {
              if (this.Cardflag == true) {
                this.isToggled = false;

                this.getPaymentTokenizerData();
              }
              console.log('Adding a card');
            }
          },
          {
            text: btn[1],
            cssClass:'submit-button',
            handler: () => {
              if (this.Cardflag == true) {
                this.isToggled = false;


              }
              console.log('toggle to false');
            }
          }],
         cssClass: 'success-message error-message',
          enableBackdropDismiss: false
          // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
        });
        alert.present();

      }
      catch (e) {
        console.log("Exception : Alert open ", e);
      }
    }
  }
  showAlertToActivateDeactivate(title,  msg,  btn) {

    if (Array.isArray(btn)) {

      try {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          buttons: [
            {
              text: btn[0],
              cssClass:'submit-button',
              handler: () => {
                if (this.celPaymentMethod == "Credit Card") {
                  // Activate
                  this.zone.run(() => {
                    this.autobillingUpdate();
                  });

                } else if (this.celPaymentMethod == "Cash") {
                  // Deactivate
                  this.zone.run(() => {
                    this.autobillingUpdate();
                  });
                }

              }
            },
            {
              text: btn[1],
              cssClass:'submit-button',
              handler: () => {
                if (this.celPaymentMethod == "Credit Card") {
                  // Activate to deactivate
                
                  this.anyvar=false;
                  this.isToggled = false;

                  

                } else if (this.celPaymentMethod == "Cash") {
                  // Deactivate to activate
                
                  this.anyvar=false;
                  this.isToggled = true;
                }

              }
            }],
          cssClass: 'success-message error-message',
          enableBackdropDismiss: false
          // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
        });
        alert.present();

      }
      catch (e) {
        console.log("Exception : Alert open ", e);
      }
    }
  }

  showAlertToActivateDeactivate1(title,  msg,  btn) {

    if (Array.isArray(btn)) {

      try {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          buttons: [
            {
              text: btn[0],
              cssClass:'submit-button submit-button-normal',
              handler: () => {
                if (this.celPaymentMethod == "Credit Card") {
                  // Activate to deactivate
                
                  this.anyvar=false;
                  this.isToggled = false;

                  

                } else if (this.celPaymentMethod == "Cash") {
                  // Deactivate to activate
                
                  this.anyvar=false;
                  this.isToggled = true;
                }
              }
            },
            {
              text: btn[1],
              cssClass:'submit-button',
              handler: () => {
                if (this.celPaymentMethod == "Credit Card") {
                  // Activate
                  this.zone.run(() => {
                    this.autobillingUpdate();
                  });

                } else if (this.celPaymentMethod == "Cash") {
                  // Deactivate
                  this.zone.run(() => {
                    this.autobillingUpdate();
                  });
                }
              }
            }],
          cssClass: 'success-message',
          enableBackdropDismiss: false
          // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
        });
        alert.present();

      }
      catch (e) {
        console.log("Exception : Alert open ", e);
      }
    }
  }

  showAlertOK(title, msg, btn){
    try {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: msg,
        buttons: [{
          text: btn,
          cssClass:'error-button-float-right submit-button',
          handler: () => {
            //update Paybill
            console.log('update AUTOBILLING');

          }
        }
       ],
        cssClass: 'success-message',
        enableBackdropDismiss: false
        // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
      });
      alert.present();

    }
    catch (e) {
      console.log("Exception : Alert open ", e);
      if (e == "No internet connection")
        return;
    }
  }

}
