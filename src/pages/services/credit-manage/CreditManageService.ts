import { ConstantData } from "../../../global/constantService";
import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { AlertController, LoadingController } from 'ionic-angular';
import { HandleError } from '../../../global/handleerror';
import { JsonstoreService } from '../../../global/jsonstore';
import { GatewayService } from '../../../global/utilService';
import { ReloadService } from '../../../global/reloadServices';
import { DashboardService } from '../../../pages/home/homeService';
import { JsonPipe } from "@angular/common/src/pipes/json_pipe";


@Injectable()
export class CreditManageService {

  selectedMobileNumber: any;
  loginMobileNumber: any;
  PhonebookNumber: any;
  lineValidity: any;
  creditBalance: any;
  creditBlanceRMValue: any;
  prepaidCurrentBalance: any;
  constructor(private network: Network, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public handleerror: HandleError, public gatewayService: GatewayService, public jsonstoreService: JsonstoreService, public reloadService: ReloadService ,public dashboardService:DashboardService) {
    this.loginMobileNumber = this.gatewayService.GetMobileNumber();
    this.selectedMobileNumber = this.gatewayService.GetMobileNumber();

    console.log('number from reload service ' + this.reloadService.gettingLoginNumber());
  };

  GetSelectMobileNumber() {
    return this.gatewayService.GetMobileNumber();
  }

  getLineValidity(){
    return this.lineValidity;
  }

  getCreditBalance(){
    return this.creditBalance;
  }

  getCreditBlanceRMValue(){
    return this.creditBlanceRMValue;
  }

  SetSelectMobileNumber(value) {
    this.selectedMobileNumber = value;
  }

  getPhoneBookNumber() {
    return this.PhonebookNumber;
  }

  setPhonebookNumber(value) {
    this.PhonebookNumber = value;
  }

  DataAlignment(gettingDate) {
    var day = gettingDate.slice(0, 8);
    var year = day.match(/.{1,4}/g);
    var date = year[1].match(/.{1,2}/g);
    var finalDate = date[1] + '/' + date[0] + '/' + year[0];
    return finalDate;
  }

  getAccountStatus(){
    return new Promise((resolve,reject)=>{
      var params = {
        "MobileNumber":this.GetSelectMobileNumber()
      };
      console.log('credit manage mobile number '+JSON.stringify(params));
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetPrepaidBalance,"POST",params).then((res)=> {
        var creditBalance = res["outputPrepaidAccStatusResp"].message.prepaidBalance;
        var lineValidity = res["outputPrepaidAccStatusResp"].message.validityDate;
        var creditBalanceRMValue="RM"+creditBalance;
        console.log("Checking Data credit "+creditBalance+"lineValidity "+lineValidity+"creditBalanceRMValue "+creditBalanceRMValue);

        var creditManageObj = {
          "creditBlance": creditBalance,
           "lineValidity": lineValidity,
           "creditBlanceRMValue": creditBalanceRMValue
        };
        this.lineValidity = lineValidity;
        this.creditBalance = creditBalance;
        this.creditBlanceRMValue = creditBalanceRMValue;
        resolve(creditManageObj);
      },(res)=>{
        reject("Unable to fetch data");
      });
    });
  }

  getCreditAdvanceEligibility(){
    return new Promise((resolve,reject)=>{
      var params = {
        "MSISDN":this.gatewayService.GetMobileNumber(),
        "TransDateTime":this.getFormattedDate(),
        "CurrentBalance":Math.ceil(this.creditBalance*100),
        "suspend_date":this.lineValidity,
        "plan":this.gatewayService.getPlanName()
      }

      //alert(JSON.stringify(params));
      
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetCreditAdvanceEligibility,"POST",params).then((res)=> {
        console.log("getCreditAdvanceEligibility"+JSON.stringify(res));
        resolve(res);
      },(res)=>{
        reject("Unable to fetch data");
      });
    });
  }

  creditAdvanceDenominations(denomAmount){
    return new Promise((resolve,reject)=>{
      var params = {
        "MSISDN":this.gatewayService.GetMobileNumber(),
        "TransDateTime":this.getFormattedDate(),
        "CurrentBalance":Math.ceil(this.creditBalance*100),
        "suspend_date":this.lineValidity,
        "DenomationAmount":denomAmount,
        "plan":this.gatewayService.getPlanName()
      }

      //alert(JSON.stringify(params));
      
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.creditAdvanceDenominations,"POST",params).then((res)=> {
        console.log("params for creditAdvanceDenominations+++++++++"+ JSON.stringify(params));
        console.log("creditAdvanceDenominations"+JSON.stringify(res));
        resolve(res);
      },(res)=>{
        reject("Unable to fetch data");
      });
    });
  }

  creditAdvanceTransaction(){
    return new Promise((resolve,reject)=>{
      var params = {
        "MSISDN":this.gatewayService.GetMobileNumber(),
        "TransDateTime":this.getFormattedDate(),
        "CurrentBalance":Math.ceil(this.creditBalance*100),
        "suspend_date":this.lineValidity,
        "plan":this.gatewayService.getPlanName()
      }

      //alert(JSON.stringify(params));
      
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.creditAdvanceTransaction,"POST",params).then((res)=> {
        console.log("creditAdvanceTransaction"+JSON.stringify(res));
        resolve(res);
      },(res)=>{
        reject("Unable to fetch data");
      });
    });
  }

  getFormattedDate(){
    var tempDate = new Date();
			let dd:any = tempDate.getDate();
			let MM:any =  (tempDate.getMonth()+1).toString();
			let yyyy:any = (tempDate.getFullYear()).toString();
			let hh:any = tempDate.getHours();
			let mm: any = tempDate.getMinutes();
			let ss:any = tempDate.getSeconds();
			if(dd<10) {
			    dd='0'+dd
			} 
			if(MM<10) {
			    MM='0'+MM
			} 
			if(hh<10) {
			    hh='0'+hh
			} 
			if(mm<10) {
			    mm='0'+mm
			} 
			if(ss<10) {
			    ss='0'+ss
      }
      return yyyy+MM+dd+hh+mm+ss;
  }
  
  
  FeachValidityBlance(selectedNumber){
    try{
    //alert("SelectedNumber"+selectedNumber)
    // var getMobileNumber = "0136426573";
    var params = {
          "MobileNumber":selectedNumber
      }
      return new Promise((resolve, reject) => {
    this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetValidityBalance,"POST",params).then((res)=> {
      
      console.log("*********************************** component GetValidityBalance FeachValidityBlance"+JSON.stringify(res));
     //alert("AlertFine"+res);
      resolve(res);
    })
    .catch(function(e){
      console.log("******************************* component  GetValidityBalance failure FeachValidityBlance"+e);
     // alert("ERROR"+e);
      //alert("error user info");
      reject(e);
  });
  });
  }catch(e){
    //alert("error"+JSON.stringify(e));
  }
  }
  formatDate(val){
    if(val==""|| val==null || val==undefined)
    return;
    var dt = val.substring(6, 8)+'/'+val.substring(4, 6)+'/'+val.substring(2, 4);
    return dt;
  }
}

