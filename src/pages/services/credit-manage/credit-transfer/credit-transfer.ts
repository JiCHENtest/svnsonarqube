import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { CreditTransferService } from './credit-transferService';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import {ReloadService} from '../../../../global/reloadServices';
import { AlertService } from '../../../../global/alert-service';
import { GatewayService } from '../../../../global/utilService';
import { ServicesPage } from '../../../../pages/services/services';
import { ConstantData } from '../../../../global/constantService';
import { CreditManageService } from "../CreditManageService";
import { Events } from "ionic-angular";
import { Keyboard } from '@ionic-native/keyboard';
import { ConfirmationScreensPage } from '../../../confirmation-screens/confirmation-screens';
import { ConnectTacTransferPage } from '../../../../pages/connect-tac-transfer/connect-tac-transfer';

@Component({
  selector: 'page-credit-transfer',
  templateUrl: 'credit-transfer.html',
})
export class CreditTransferPage {

  totalBillAmount: any;
  phoneNumber: any;
  serviceIndicator:any;
  loader:any;
  validNumber: boolean = false;
  validAmount: boolean = false;
  amount:any;
  currentBalance:any;
  transferParams:any;
  id: number;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public creditTransfer: CreditTransferService,
    public alertCtrl: AlertController,
    public reloadService:ReloadService,
    public alertCtrlInstance: AlertService,
    public gatewayService: GatewayService,
    public creditManageService: CreditManageService,
    public events:Events,private keyboard: Keyboard,
    private renderer: Renderer2
  ) {
    console.log('credit transfer constructor called');
      this.loader = this.gatewayService.LoaderService();
      this.events.unsubscribe('callTransferAmount');
      events.subscribe('callTransferAmount',(params)=>{
        console.log('event subscribed callTransferAmount' + JSON.stringify(params));
        this.transferAmount(params);
      });

      // this.events.unsubscribe('callTransferAmount');
      // events.subscribe('callTransferAmount',(params)=>{
      //   console.log('event subscribed callTransferAmount' + JSON.stringify(params));
      //   this.transferAmount(params);
      // });

      this.events.unsubscribe('creditmanage0');
      events.subscribe('creditmanage0',(params)=>{
        console.log('event subscribed creditmanage0 -> Credit Transfer');
        this.getAccountInfo();
      });

      this.id = navParams.get('id');
      if(this.id==0){
        this.getAccountInfo();
      }


  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Credit transfer service", this.renderer);
  }

  ngOnDestroy() {
    this.events.unsubscribe('callTransferAmount');
    this.events.unsubscribe('creditmanage0');
  }

  ionViewDidLoad() {
    console.log('credit transfer ion view did load called');
   }

  getAccountInfo(){

    var _that = this;

    _that.loader = _that.gatewayService.LoaderService();
    _that.loader.present().then(() => {

      this.creditTransfer.getAccountStatus().then((res)=>{
        console.log("CreditTransferPage -> getAccountInfo -> service response "+ res);
        this.currentBalance=res;
        _that.loader.dismiss();
        try{
           var roamingInfo = this.creditTransfer.getRoamingInfo();
           console.log(roamingInfo);
           this.showAlert("Uh Oh", "User must be in Malaysia to perform credit transfer.", "OK");

         }catch(err){
           console.log("User is in Malaysia can do credit transfer No prob!!");
         }
        },
       (res)=>{
         _that.loader.dismiss();
         console.log("CreditTransferPage -> getAccountInfo -> service response -> Error "+ res);
         if(res=="No internet connection")
         return;
         this.showAlert("Uh Oh", res, "OK");
       });
    });


  }

  validatePhoneNumberAndAmount(){
    this.creditTransfer.validate(this.phoneNumber).then((res)=>{

    },(err)=>{

    });
  }

 calculateTotalBillAmount(val){
  console.log("after event" + JSON.stringify(val));
    this.getTotalBillAmount(val).then((res)=>{
      this.totalBillAmount = res;
    });
  }

  getTotalBillAmount(value){
    return new Promise((resolve,reject)=>{
      var charge = "0.50";
      var val = parseFloat(value) + parseFloat(charge);
      console.log("val" + val);

      resolve(parseFloat(val.toString()).toFixed(2));
    });
  }

  checkAmount(event:any){
    this.validAmount = false;
    this.totalBillAmount = "";
    var msg = '';
    if(this.amount == '' || this.amount == undefined){

    }
    else{
    if(this.amount <= 0){
      msg = 'Please enter an valid amount.';
    }
    else if(isNaN(this.amount)){
      msg = 'Please enter an valid amount.';
    }
    else if(this.amount<1){
      msg = 'Amount should be greater than RM1.00';
    }
    else if(this.amount>20){
      msg = 'Amount should not be greater than RM20.00';
    }
    else{
      this.validAmount = true;
      var value = this.amount;
      console.log("before event");
      this.calculateTotalBillAmount(value);
    }

    if(msg != ''){
      this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid Amount", msg, "Ok");
    }
  }

  }
  checkMobileNumber(){
    this.validNumber = false;
    var _that = this;
    var selectedNumber = this.phoneNumber;
    if(this.phoneNumber == '' || this.phoneNumber == undefined){
      //nothing to do
    }
    else{
    _that.loader = _that.gatewayService.LoaderService();
    _that.loader.present().then(() => {
      this.reloadService.checkAddedNewNumber(selectedNumber).then((res)=> {
        console.log("inside checkAddedNewNumber "+JSON.stringify(res));
        this.serviceIndicator = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.ListOfServices.Services.ServiceType;

           if(this.serviceIndicator== "Prepaid" || this.serviceIndicator=="CNVRGTPrepaid"){
            _that.loader.dismiss();
            if(selectedNumber.substring(0,3)=="011" && selectedNumber.length==10){
              this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid Number", "Please enter a valid Celcom prepaid number (10 or 11 digits only).", "Try again");
            }
            else{
              this.validNumber = true;
            }
          }else{
            this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid Number", "Not a Prepaid number.", "OK");
            _that.loader.dismiss();
          }

       }).catch(err => {
        _that.loader.dismiss();
        console.log("*****paybill*******"+JSON.stringify(err));
        if(err=="No internet connection")
        return;
        if(err.indexOf("timed out") >= 0 || err.indexOf("Failed to connect") >= 0){
          this.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", "OK");
        }
        else{
          this.alertCtrlInstance.showAlert("Notification", "Not a Celcom number.", "OK");
        }
        // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
    });
  }

  }

  showAlert(title, msg, btn,cssClassVal?,dismissOnPageChange?,duration?) {
    console.log("Alert open");
    var _that = this;
      try {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          buttons: [
          {
            text: btn,
            cssClass:'error-button-float-right submit-button',
            handler: () => {
              console.log('Call to Action Clicked');
              //_that.navCtrl.setRoot(ServicesPage);

              _that.events.publish('goToServicePage');
            }
          }],
          cssClass:'success-message error-message'
        // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
        });
        alert.present();
        if(duration!=null && alert._state==1){
          setTimeout(()=>{
            alert.dismiss().then(succ=>{
            });
          },2000);
        }
      }
      catch(e) {
        console.log("Exception : Alert open ",e);
      }



  }
  confirmAmount(){
    var transferorNo = this.gatewayService.GetMobileNumber();
    var transfereeNo = this.phoneNumber;
    var currAcctChgAmt = this.amount * 10000;
    console.log(this.currentBalance);
    console.log(this.totalBillAmount);
    var newCreditBal=this.currentBalance-this.totalBillAmount;
    var creditDetails={
      "debitedFrom":transferorNo,
      "creditedTo":transfereeNo,
      "amountTransfer":"RM "+ this.amount,
      "amount":this.amount,
      "totalChrge":"RM "+this.totalBillAmount,
      // "newCreditBal":"RM "+newCreditBal.toFixed(2)
      "newCreditBal":newCreditBal
    }
    this.navCtrl.parent.parent.push(ConfirmationScreensPage, {"isCreditTransfer":true, "creditTransferDetails":creditDetails});
  }
  transferAmount(params) {
    console.log("transfer amount called");

    console.log(params);
    this.transferParams = params;
    var _that = this;
    _that.loader = _that.gatewayService.LoaderService();
    _that.loader.present().then(() => {

      var transfereeNo = _that.transferParams.creditedTo.substr(1);
   //s.substr(1);
   var transferorNo = _that.transferParams.debitedFrom.substr(1);
   var currAcctChgAmt = _that.transferParams.amount * 10000;
   console.log(currAcctChgAmt)
   //alert(currAcctChgAmt);
    var params = {"transfereeNo":transfereeNo, "transferorNo":transferorNo,"currAcctChgAmt":currAcctChgAmt};
        if(ConstantData.isLoginBypassed)
        {
          //_that.navCtrl.push(ConnectTacTransferPage,{"params":params});
          this.events.publish('goToOTPPage',params);
          _that.loader.dismiss();
          // _that.transferAmountServiceCall().then(function (res) {
          //         console.log(res);

          //         _that.loader.dismiss();
          //         if(res["isSuccessful"]){
          //           if(res["msgStatus"]=="Success"){
          //             _that.alertCtrlInstance.showAlert(res["msgStatus"], res["cswMessage"], "OK");
          //           }
          //           else{
          //             _that.alertCtrlInstance.showAlert(res["msgStatus"], res["cswMessage"], "OK");
          //           }
          //         }
          //         else{
          //           _that.alertCtrlInstance.showAlert("Uh Oh. Can't transfer amount", "Please try again later", "OK");
          //         }


          //       }).catch(function (e) {
          //         _that.alertCtrlInstance.showAlert("Uh Oh. Can't transfer amount", "Please try again later", "OK");
          //         console.log("component  failure" + e);
          //         console.log("Error" + e);
          //         _that.loader.dismiss();
          //       });

        }
        else{

    //code for calling register()from adapter
    this.sendOTP(params).then((res) => {
      //alert("register function response"+JSON.stringify(res));
    //_that.navCtrl.push(ConnectTacTransferPage,{"params":params,"register_state":res});
    params["register_state"] = res;
    _that.loader.dismiss();
      this.events.publish('goToOTPPage',params);

    }).catch(function (e) {
      console.log("component  failure" + e);
      _that.loader.dismiss();


    });

        }


                   //code for calling register()from adapter
                // _that.transferAmountServiceCall().then(function (res) {
                //   console.log(res);

                //   _that.loader.dismiss();
                //   if(res["isSuccessful"]){
                //     if(res["msgStatus"]=="Success"){
                //       _that.alertCtrlInstance.showAlert(res["msgStatus"], res["cswMessage"], "OK");
                //     }
                //     else{
                //       _that.alertCtrlInstance.showAlert(res["msgStatus"], res["cswMessage"], "OK");
                //     }
                //   }
                //   else{
                //     _that.alertCtrlInstance.showAlert("Uh Oh. Can't transfer amount", "Please try again later", "OK");
                //   }


                // }).catch(function (e) {
                //   _that.alertCtrlInstance.showAlert("Uh Oh. Can't transfer amount", "Please try again later", "OK");
                //   console.log("component  failure" + e);
                //   console.log("Error" + e);
                //   _that.loader.dismiss();
                // });


    });

  }
  transferAmountServiceCall(){
   var transfereeNo = this.phoneNumber.substr(1);
   //s.substr(1);
   var transferorNo = this.gatewayService.GetMobileNumber().substr(1);
   var currAcctChgAmt = this.amount;
    var params = {"transfereeNo":transfereeNo, "transferorNo":transferorNo,"currAcctChgAmt":currAcctChgAmt};

    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.CreditTransfer, "POST", params).then((res_validate) => {
        console.log("transfer amount response : " + JSON.stringify(res_validate));
        resolve(res_validate);

        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          console.log("******************************************** component failure transfer amount response" + e);
          //alert("error in validating MobileConnectUser");
          reject(e);
        });

    });

  }

  sendOTP(params){
    var _that = this;
    console.log("ConfirmationScreensPage -> registerMobileConnectUser() -> params ",params);


    var msisdn = this.gatewayService.GetMobileNumber();

    var params_register={"MSISDN":msisdn};

    console.log("ConfirmationScreensPage -> registerMobileConnectUser() -> msisdn = ",msisdn);
    console.log("ConfirmationScreensPage -> registerMobileConnectUser() -> params_register = ",params_register);

     return new Promise((resolve, reject) => {
    this.gatewayService.CallAdapter(ConstantData.adapterUrls.registerMobileConnect2,"POST",params_register).then((res_register)=> {
      console.log("register OTP response : "+JSON.stringify(res_register));
      resolve(res_register);

    })
    .catch(function(e){
      console.log("******************************************** component failure registerMobileConnectUser "+e);
      console.log("error register  oleole gift");
      if(e=="No internet connection")
      return;
      _that.alertCtrlInstance.showAlert("Uh Oh. System's a Little Busy", "Please try again later", ["OK"]);
      reject(e);
    });

    });


  }

  handleKeyDone(e){
    if(e.which === 13){
      this.keyboard.close();
    }
  }
}
