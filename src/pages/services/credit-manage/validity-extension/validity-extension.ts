import { Component, ApplicationRef, NgZone, Renderer2 } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Events } from 'ionic-angular';

import { ValidityExtensionService } from "./validity-extensionService";
import { GatewayService } from '../../../../global/utilService';
import { CreditManageService } from '../CreditManageService';
import { Keyboard } from '@ionic-native/keyboard';
import { ConfirmationScreensPage } from '../../../confirmation-screens/confirmation-screens';



@Component({
  selector: 'page-validity-extension',
  templateUrl: 'validity-extension.html'
})
export class ValidityExtensionPage {

  noOfDays: string = "0";
  noOfDays_parsed: number = 0;
  newCreditBalance: number;
  newValidityDate: any;
  totalValidityDays: any;
  totalAmount: any;
  creditBalance: any;
  isValidityExt: boolean = true;
  id: number;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public validityExtensionService: ValidityExtensionService,
    public gatewayService: GatewayService,
    public creditManageService: CreditManageService,
    public loadingCtrl: LoadingController,
    public events: Events,
    private keyboard: Keyboard,
    private ref: ApplicationRef,
    private zone: NgZone,
    private renderer: Renderer2
  ) {
    this.ionViewDidEnter();
    console.log('validity constructor called');
    this.events.unsubscribe('callSubmitValidityConfirmed');
    events.subscribe('callSubmitValidityConfirmed', (params) => {
      this.submitVlaidityExtensionConfirmed();
    });

    this.events.unsubscribe('creditmanage1');
    events.subscribe('creditmanage1', (params) => {
      console.log('event subscribed creditmanage1 -> validity extension');
      this.getAccountInfo();
    });

    this.id = navParams.get('id');
    if (this.id == 1) {
      this.getAccountInfo();
    }
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Validity extension service", this.renderer);
  }

  ngOnDestroy() {
    this.events.unsubscribe('callSubmitValidityConfirmed');
    this.events.unsubscribe('creditmanage1');
  }

  keyUpChecker(eve, varname) {
    let _thisRef = this;
    let returnVal = this.gatewayService.keyUpCheckerNumberOnly(eve, this.keyboard);
    this.zone.run(() => {
      _thisRef[varname] = returnVal;
    })
  }

  blurChecker(ev) {
    if (ev.currentTarget.children[0])
      ev.currentTarget.children[0].value = this.gatewayService.keyUpCheckerNumberOnly(ev, this.keyboard);
  }

  somethingChanged(refrence) {
    this.noOfDays = refrence.value;
  }

  submitValidyExtension() {

    this.creditBalance = this.creditManageService.getCreditBalance();
    var title = "Confirmation";
    var errorMessage;
    this.noOfDays_parsed = Number(this.noOfDays);
    this.newCreditBalance = this.creditBalance - this.noOfDays_parsed;
    var newcreditBalanceString = "" + this.newCreditBalance;
    var newcreditBlanceRMValue = "RM" + parseFloat(newcreditBalanceString).toFixed(2);
    var lineValidity = this.creditManageService.getLineValidity();
    this.newValidityDate = new Date(new Date(lineValidity.substr(0, 4), lineValidity.substr(4, 2) - 1, lineValidity.substr(6, 2)).getTime() + (1000 * 60 * 60 * 24 * (this.noOfDays_parsed+1)));
    this.totalAmount = this.noOfDays;

    if (this.newCreditBalance > 0 && this.noOfDays_parsed > 0) {
      var validityDetails = {
        "PaymentMethod": "deduct from credit",
        // "NewCreditBalance": newcreditBlanceRMValue,
        "NewCreditBalance": this.newCreditBalance,
        "TotalValidityDaysExtended": this.noOfDays,
        "AmountDeducted": this.totalAmount,
        "MobileNumber": this.creditManageService.GetSelectMobileNumber(),
        "NewValidityDate": this.newValidityDate.toISOString().substring(0, 10)
      }
      this.navCtrl.parent.parent.push(ConfirmationScreensPage, { "validityDetails": validityDetails, "isValidityExt": this.isValidityExt });
    } else if (this.noOfDays_parsed <= 0) {
      errorMessage = "Invalid number of days";
      this.presenterror(errorMessage);
    } else {
      errorMessage = "Request can not be processed, unsufficient credit balance";
      console.log("ValidityExtensionPage -> submitValidyExtension -> service response " + errorMessage);
      this.presenterror(errorMessage);/*commenting this code for confirmation*/
    }
  }

  submitVlaidityExtensionConfirmed() {

    var loader = this.loadingCtrl.create({
      content: "Loading"
    });
    loader.present().then(() => {
      this.validityExtensionService.submitValidityExtension(this.noOfDays).then((res) => {
        console.log("ValidityExtensionPage -> submitVlaidityExtensionConfirmed -> service response " + res);
        if (res["msgStatus"] == "Success") {
          var dateDisplay = new Date(res["updatedExpDt"].substr(0, 4), res["updatedExpDt"].substr(4, 2) - 1, res["updatedExpDt"].substr(6, 2));
          var messageInput = "Validity extended: <br>" + this.noOfDays + " day(s) has been added to mobile " + this.creditManageService.GetSelectMobileNumber();
          this.presentsucess(messageInput);
          this.noOfDays = "0";
          this.events.publish('reloadHeaderManageCredit');
          this.gatewayService.setFlagForDashboardReload(true);
        } else if (res["msgStatus"] == "Exception") {
          this.presenterror(res["errorMessage"]);
        } else {
          this.presenterror("Validity Extension transaction failed");
        }
        loader.dismiss();
      },
        (err) => {
          loader.dismiss();
          console.log("ValidityExtensionPage -> submitVlaidityExtensionConfirmed -> service response -> Error " + err);
          if (err == "No internet connection")
            return;
          this.presenterror("Validity Extension transaction failed");

        });
    });
  }

  submitCancel() {

  }

  getAccountInfo() {
    this.creditBalance = this.creditManageService.getCreditBalance();
  }

  presenterror(messageInput) {
    const alert = this.alertCtrl.create({
      title: 'Validity Extension Error',
      message: messageInput,
      buttons: [{
        text: "OK",
        role: "Ok",
        cssClass: "error-button-float-right submit-button"
      }],
      cssClass: 'success-message error-message'
    });
    alert.present();
  }

  presentsucess(messageInput) {
    const alert = this.alertCtrl.create({
      title: 'Congratulations!',
      message: messageInput,
      buttons: [{
        text: "OK",
        role: "Ok",
        cssClass: "error-button-float-right submit-button"
      }],
      cssClass: 'success-message'
    });
    alert.present();
  }

  presentConfirm(title, message, btn1, btn2, callback1, callback2) {
    let alert = this.alertCtrl.create({
      title: title,
      message: message,
      cssClass: 'success-message error-message',
      buttons: [
        {
          text: btn2,
          role: 'cancel',
          cssClass: 'submit-button',
          handler: () => {
            callback2();
            console.log('Cancel clicked');
          }
        },
        {
          text: btn1,
          cssClass: 'submit-button',
          handler: () => {
            callback1(this);
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }

}
