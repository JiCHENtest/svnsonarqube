import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams, Events, AlertController} from 'ionic-angular';
import { ProductDetailsDesignGuidePage } from '../../../product-details-design-guide/product-details-design-guide';

// service
import { GatewayService } from '../../../../global/utilService';
import { ConstantData } from '../../../../global/constantService';
import { HandleError } from '../../../../global/handleerror';
import { AlertService } from '../../../../global/alert-service';
import { OleoleService } from '../../../../global/oleole-service';
import { DashboardService } from '../../../../pages/home/homeService';


// to navigate
import { SubscriptionsPage } from '../../../../pages/subscriptions/subscriptions';

/**
 * Generated class for the OleoleCatalogueListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-oleole-catalogue-list',
  templateUrl: 'oleole-catalogue-list.html',
})
export class OleoleCatalogueListPage {

  rootNavCtrl: NavController;

  listOfPlanCatalogue : any; // list of plan of voucher
  giftName : any; // category name
  userInfo : any; // type of msisdn number
  mobileNumber :any; // mobile number

  hotpickesObj : any;
  oleoleVoucherImageSrc : any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public gatewayService:GatewayService, 
    public handleerror: HandleError, 
    private alertCtrlInstance: AlertService,
    public oleoleService : OleoleService,
    public events : Events,
    public dashboardService : DashboardService,
    private renderer: Renderer2, 
    public conformAlert: AlertController){
    

    this.hotpickesObj = [
              {
                  "HEADER_TITLE": "Fashion Valet",
                  "plan": []
              },
              {
                  "HEADER_TITLE": "11street",
                  "plan": []
              },
              {
                  "HEADER_TITLE": "LAZADA",
                  "plan": []
              },
              {
                  "HEADER_TITLE": "Xpax E-Gifts",
                  "plan": []
              },
              {
                  "HEADER_TITLE": "Celcom E-Gifts",
                  "plan": []
              }
          ];
          
    this.oleoleVoucherImageSrc = [
      {
        "name": "Fashion Valet",
        "imgSrc": "./assets/img/oleole-product/2304x1296_fashionvalet.jpg",
        "voucher_description": "Fashion Valet e-Gift card can only be used by the recipient on Android and iOS phone and only available for redemption on www.fashionvalet.com only.",
        "merchant_description":"The online shopping website that features an array of Asia’s chic designers and boutiques. Under this fabulous roof, you’ll be able to shop the latest trends even while curling up in bed. Let us do all the work as we bring you the hottest looks, delivered to your doorstep in time to spruce up that wardrobe of yours. Free shipping across Malaysia."
      },
      {
        "name": "Melissa Shoes",
        "imgSrc": "./assets/img/oleole-product/2304x1296_melissa.jpg",
        "voucher_description": "Melissa Shoes e-Gift card can only be used by the recipient on Android and iOS phone and only available for redemption on www.mdreams.com.my only.",
        "merchant_description":"Melissa is an Iconic, Pop-luxury, Eco-friendly brand. Each pair of Melissa shoe is covetable piece of art; they are easily recognisable from their modern designs and rich colours. The shoes are comfortable, fashionable, express individuality and are eco-friendly that many stylish women and celebrities have even become huge fans and advocates of the brand – from the likes of Victoria’s Secret supermodel Alessandra Ambròsio with whom Melissa has recently collaborated with and launched a capsule collection to Hollywood starts such as Katie Holmes, Vanessa Hudgens and Anne Hathaway."
      },
      {
        "name": "Something Borrowed",
        "imgSrc": "./assets/img/oleole-product/2304x1296_something_borrowed.jpg",
        "voucher_description": "Something Borrowed e-Gift card can only be used by the recipient on Android and iOS phone and only available for redemption on www.zalora.com.my only.",
        "merchant_description":"Something Borrowed is an exclusive brand by ZALORA. We provide the chic-street look through an extensive collection."
      },
      {
        "name": "ZALIA",
        "imgSrc": "./assets/img/oleole-product/2304x1296_zelia.jpg",
        "voucher_description": "ZALIA  e-Gift card can only be used by the recipient on Android and iOS phone and only available for redemption on www.zalora.com.my only.",
        "merchant_description":"Zalia is an exclusive brand by ZALORA. We provide the chic-street look through an exclusive collection. ZALORA is Asia’s online fashion destination. We provide the region’s style enthusiasts with the latest high-street looks through an extensive collection of global and local fashion and beauty brands made available on one awesome website. We bring the latest trends from the fashion runways to your computer screens and have them speedily delivered straight to your doorstep."
      },
      {
        "name": "ZALORA",
        "imgSrc": "./assets/img/oleole-product/2304x1296_zalora.jpg",
        "voucher_description": "ZALORA  e-Gift card can only be used by the recipient on Android and iOS phone and only available for redemption on www.zalora.com.my only.",
        "merchant_description":"ZALORA is Asia’s online fashion destination. We provide the region’s style enthusiasts with the latest high-street looks through an extensive collection of global and local fashion and beauty brands made available on one awesome website. We bring the latest trends from the fashion runways to your computer screens and have them speedily delivered straight to your doorstep."
      },
      {
        "name": "Fave",
        "imgSrc": "./assets/img/oleole-product/fave_1.jpg",
        "voucher_description": "Fave e-Gift card can only be used by the recipient on Android and iOS phone and only available for redemption on www.myfave.com only.",
        "merchant_description":"Fave offers the best deals from travel, food, beauty and services up to 70% discount."
      },
      {
        "name": "Taobao Collection",
        "imgSrc": "./assets/img/oleole-product/taobao_collection.jpg",
        "voucher_description": "Taobao Collection e-Gift card can only be used by the recipient on Android and iOS phone and only available for redemption on www.lazada.com.my/taobao only.",
        "merchant_description":"Shop from a Wide Variety of Taobao products. From Mommy Market to Home and Living products. Taobao, operated by Alibaba Group, is the largest Chinese shopping website. Taobao shops have over 10 million products."
      },
      {
        "name": "11street",
        "imgSrc": "./assets/img/oleole-product/2304x1296_11_street.jpg",
        "voucher_description": "11street e-Gift card can only be used by the recipient on Android and iOS phone and only available for redemption on www.11street.my only.",
        "merchant_description":"11Street offers hundreds and thousands of products across all categories including health and beauty, home and living, fashion, mobile and tablets, consumer electronics, home appliance among others. 11Street is the place to visit for all your shopping needs."
        // "merchant_description":"11Street offers a wide range of products with hundreds of thousands of products across categories including health and beauty, home and living, fashion, mobiles and tablets, consumer electronics and home appliances among others. Lazada is the place to visit for all your shopping needs."
      },
      {
        "name": "LAZADA",
        "imgSrc": "./assets/img/oleole-product/2304x1296_lazada.jpg",
        "voucher_description": "LAZADA e-Gift card can only be used by the recipient on Android and iOS phone and only available for redemption on www.lazada.com.my only.",
        "merchant_description":"Lazada offers a wide range of products with hundreds of thousands of products across categories including health and beauty, home and living, fashion, mobiles and tablets, consumer electronics and home appliances among others. Lazada is the place to visit for all your shopping needs."
      },
      {
        "name": "Klook",
        "imgSrc": "./assets/img/oleole-product/klook_2.jpg",
        "voucher_description": "KLOOK e-Gift card can only be used by the recipient on Android and iOS phone and only available for redemption on www.klook.com only.",
        "merchant_description":"Klook offers a simple way to discover activities, attractions and things to do wherever you travel. Discover destination services at the best prices. Set your inner child free at Tokyo Disneyland or Marvel at the breathtaking world under the waves in Bali."
      },
      {
        "name": "Steam",
        "imgSrc": "./assets/img/oleole-product/steam_1.jpg",
        "voucher_description": "",
        "merchant_description":""
      },
      {
        "name":"MOL Points",// or in excel sheet zGold-MOLPoints
        "imgSrc": "./assets/img/oleole-product/2304x1296_z_gold_mol_point.jpg",
        "voucher_description": "MOL Points e-Gift card can only be used by the recipient on Android and iOS phone and only available for redemption on www.mol.com only.",
        "merchant_description":"zGold-MOLPoints is a virtual currency which you can use to purchase over 10,000+ games, Digital Content and Services. From game credits to Digital Content, MOL’s goal is to provide you with the widest range of products available on the Internet through zGold-MOLPoints. Steam Wallet Code - Battle.Net Balance – Bigo LIVE – PlayCash (Blackshot, Echo of Soul ) – Wargaming (World of Tanks, World of Warships) – Efun Games – Gm99, UJoy – R2Games – Facebook Game Credits – Spotify"
      }
    ];
      
    // selected mobile number
    this.mobileNumber = this.gatewayService.GetMobileNumber();

    // type of msisdn number 
    this.userInfo = this.dashboardService.GetUserInfo();
    
    // this.listOfPlanCatalogue = this.navParams.get("giftName");
    this.giftName = this.navParams.get("giftName");
    console.log("OleoleCatalogueListPage -> giftName = ",this.giftName);

    var responseForGiftIsArray = Array.isArray(this.oleoleService.GetGiftList());
    var Response_Gift = this.oleoleService.GetGiftList();
 
    if(this.giftName != 'hardcoded_hotpicks') {
     
      this.listOfPlanCatalogue = responseForGiftIsArray ? Response_Gift : [Response_Gift];
      console.log("OleoleCatalogueListPage -> (if) listOfPlanCatalogue = ",this.listOfPlanCatalogue,"\n responseForGift = ",responseForGiftIsArray);
    }
    else if(this.giftName == 'hardcoded_hotpicks') {
      // this.listOfPlanCatalogue = this.hotpickesObj;
      // this.listOfVouchers(this.giftName);
      this.listOfPlanCatalogue = responseForGiftIsArray ? Response_Gift : [Response_Gift];
      // Array.isArray
      console.log("OleoleCatalogueListPage -> (else) listOfPlanCatalogue = ",this.listOfPlanCatalogue);
      console.log("OleoleCatalogueListPage -> (else) hotpickesObj = ",this.hotpickesObj,"\n responseForGift = ",responseForGiftIsArray);
    }

    
    var count = 0;
    this.listOfPlanCatalogue.forEach(element => {
      console.log("element 1 ",element.HEADER_TITLE);

      if(element.HEADER_TITLE.toLowerCase() == ("Xpax E-Gifts").toLowerCase() ) {
        this.listOfPlanCatalogue[3].imgsrc = "./assets/img/oleole-product/xpax_internet_add_ons.jpg";
        count++;
      }
      else if(element.HEADER_TITLE.toLowerCase() == ("Celcom E-Gifts").toLowerCase() ) {
        this.listOfPlanCatalogue[4].imgsrc = "./assets/img/oleole-product/postpaid_int_addon.jpg";
        count++;
      }

      this.oleoleVoucherImageSrc.forEach(elements => {

        console.log("elements 2 ",elements.name," count ", count);
        console.log(element.HEADER_TITLE.toLowerCase() == elements.name.toLowerCase());

        if(element.HEADER_TITLE.toLowerCase() == elements.name.toLowerCase()){
          this.listOfPlanCatalogue[count].imgsrc = elements.imgSrc;
          this.listOfPlanCatalogue[count].merchant_description = elements.merchant_description;
          this.listOfPlanCatalogue[count].voucher_description = elements.voucher_description;
          count++;
        }        
      });
    });
    console.log("OleoleCatalogueListPage -> listOfPlanCatalogue = ",this.listOfPlanCatalogue);
    // this.listOfVouchers(this.giftName);

    this.rootNavCtrl = navParams.get('rootNavCtrl');
  }


  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("OleOle catalogue list", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OleoleCatalogueListPage');
  }

  goToProductDetails(partnerPlanList){

    let smartPhonealert = this.conformAlert.create({
      // title: "Ole for Smartphones!",
      // message: "Only recipients using smartphones can receive and  enjoy their e-gifts.",
      title: "Awesome Choice.",
      message: "These e-Gifts can only be used by your family and friends who are using smartphone.",
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel clicked for smartphone');
            // return false;
          }
        },
        {
          text: 'OK Proceed',
          handler: () => {
            console.log('OK Proceed clicked for smartphone');
            this.smartPhoneProcessed(partnerPlanList);
          }
        }
      ]
    });
    smartPhonealert.present();



    
    
  }

  listOfVouchers(keywords) {

    console.log("OleoleCatalogueListPage -> listOfVouchers() -> keywords = ",keywords);
    
  }

  smartPhoneProcessed(partnerPlanList)  {
    console.log("HEADER_TITLE ",partnerPlanList.HEADER_TITLE)
    var _that = this;
    var giftName;
    var giftNameForHotpickes  = this.giftName;

    partnerPlanList.plan = Array.isArray(partnerPlanList.plan) ? partnerPlanList.plan : [partnerPlanList.plan]

    if(this.giftName == "hardcoded_hotpicks" && partnerPlanList.HEADER_TITLE == "Xpax E-Gifts") 
      giftName  = "PREPAID";
    else if(this.giftName == "hardcoded_hotpicks" && partnerPlanList.HEADER_TITLE == "Celcom E-Gifts") 
      giftName  = "POSTPAID";
    else if(this.giftName == "hardcoded_hotpicks" && partnerPlanList.HEADER_TITLE == "11street") 
      giftName  = "hardcoded_hotpicks";
    else if(this.giftName == "hardcoded_hotpicks" && partnerPlanList.HEADER_TITLE == "Fashion Valet") 
      giftName  = "hardcoded_hotpicks";
    else if(this.giftName == "hardcoded_hotpicks" && partnerPlanList.HEADER_TITLE == "LAZADA") 
      giftName  = "hardcoded_hotpicks";

    console.log("OleoleCatalogueListPage -> goToProductDetails() -> partnerPlanList = ",partnerPlanList);

    var loader = _that.gatewayService.LoaderService();
   
    loader.present();

    var params = {
      "msisdn": _that.mobileNumber,
      "keyword":giftName,  //e.g. "fashion"
      "txn_id":_that.gatewayService.generateRandomNum(16)
    };

    var params_MCP_XPAX = {
      "msisdn": _that.mobileNumber,
      "operation_id":giftName,  //e.g. "PREPAID" , "POSTPAID"
      "txn_id":_that.gatewayService.generateRandomNum(16)
    };

    console.log("OleoleCatalogueListPage -> goToProductDetails() -> giftName = ",giftName);
    console.log("OleoleCatalogueListPage -> goToProductDetails() -> giftNameForHotpickes = ",giftNameForHotpickes);

    
    console.log(" 1 ",giftNameForHotpickes == "hardcoded_hotpicks" && (giftName != "PREPAID" || giftName != "POSTPAID"));
    console.log(" 2 ",giftNameForHotpickes == "hardcoded_hotpicks" && (giftName == "PREPAID" || giftName == "POSTPAID"));

    if(giftNameForHotpickes == "hardcoded_hotpicks" && (giftName != "PREPAID" && giftName != "POSTPAID") ) {

      loader.dismiss();

      var choose_plan = giftName;
      
      this.navCtrl.push(ProductDetailsDesignGuidePage,{"oleoleGiftPartner":partnerPlanList,"isvoucher":true});
      
        /*_that.gatewayService.CallAdapter(ConstantData.adapterUrls.GiftCat,"POST",params).then(function(res){
        console.log("************************************* component success oleole giftlist voucher "+JSON.stringify(res));
        _that.handleerror.handleErrorGiftListForCat(res,"GiftCat").then((result) => {
            if(result == true) {
                console.log("hai if Oleole GiftList voucher for PostpaidAndPrepaid ",result);
                // resolve(res);
                
                if(res['VOUCHERCATALOGUE']['ErrorCode'])
                  _that.alertCtrlInstance.showAlert("", "Voucher Out of stock.", "OK");
                else {
                  _that.oleoleService.SetGiftList(res);
                  // for a moment
                  // _that.rootNavCtrl.push(OleoleCatalogueListPage, {"giftName" : giftName});
                }
                  
                // _that.events.publish('oleole:created'); 
                loader.dismiss();
            }
            else {
                console.log("hai else Oleole GiftList voucher For PostpaidAndPrepaid ",result);
                loader.dismiss();
                if(res['VOUCHERCATALOGUE']['ErrorCode'])
                  _that.alertCtrlInstance.showAlert("", "Voucher Out of stock.", "OK");
                else 
                _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
                // reject("error : no data found.");
            }
        });
        // this.handleError(res,"FetchMydeals");
        // resolve(res);
      })
      .catch(function(e){
        console.log("****************************************** component failure Oleole gift voucher  "+e);
        console.log("error user info Oleole gift voucher ");
        loader.dismiss();
        _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
        // reject(e);
      }); */
    }
    else if(giftNameForHotpickes == "hardcoded_hotpicks" && (giftName == "PREPAID" || giftName == "POSTPAID") ) {

      _that.gatewayService.CallAdapter(ConstantData.adapterUrls.GiftListForPostpaidAndPrepaid,"POST",params_MCP_XPAX).then(function(res){
        console.log(" ************************************* component success oleole giftlist MCP & Xpax "+JSON.stringify(res));
        _that.handleerror.handleErrorGiftListForPostpaidAndPrepaid(res,"GiftListForPostpaidAndPrepaid").then((result) => {
            if(result == true) {
                console.log("hai if Oleole GiftList MCP & Xpax  for PostpaidAndPrepaid ",result);
                // resolve(res);

                // var sim_type = (_that.userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid") ? "POSTPAID" : "PREPAID";
                // _that.oleoleService.SetXpaxGiftListObj(giftName);
                var choose_plan = giftName;
                console.log("CataloguePage -> openCatalogueListPage() -> sim_type ",choose_plan);
                // _that.events.publish('userOleole:created');
                
                _that.rootNavCtrl.push(SubscriptionsPage, {"typeOfGift" : choose_plan,"mcp_and_xpax":res,"giftName" : giftName});
                  
                // _that.events.publish('oleole:created'); 
                loader.dismiss();
            }
            else {
                console.log("hai else Oleole GiftList MCP & Xpax For PostpaidAndPrepaid ",result);
                loader.dismiss();
                _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
                // reject("error : no data found.");
            }
        });
        // this.handleError(res,"FetchMydeals");
        // resolve(res);
      })
      .catch(function(e){
        console.log("****************************************** component failure Oleole gift MCP & Xpax "+e);
        console.log("error user info Oleole gift MCP & Xpax");
        // loader.dismiss();
        if(e=="No internet connection")
        return;
        _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
        // reject(e);
      });
    }
    else {
      // navigate to ProductDetailsDesignGuidePage
      loader.dismiss();
      console.log("OleoleCatalogueListPage -> goToProductDetails() ");
      if(giftNameForHotpickes != 'hardcoded_hotpicks') {
        console.log("OleoleCatalogueListPage -> Navigate(ProductDetailsDesignGuidePage) ");
        this.navCtrl.push(ProductDetailsDesignGuidePage,{"oleoleGiftPartner":partnerPlanList,"isvoucher":true,"header_title":partnerPlanList.HEADER_TITLE});
      }
      else  {
        console.log("OleoleCatalogueListPage -> Navigate(none) ");
      }

    }
  }
}
