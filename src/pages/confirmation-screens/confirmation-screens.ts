import { Component, ViewChild, Renderer2 } from '@angular/core';
import { NavController, NavParams, Nav, AlertController } from 'ionic-angular';

// pages
// import { ConnectTacPage } from '../../pages/login/connect-tac/connect-tac';
import { ConnectTacTwoPage } from '../../pages/connect-tac-two/connect-tac-two';

// service
import { ConstantData } from '../../global/constantService';
import { GatewayService } from '../../global/utilService';
import { DashboardService } from '../../pages/home/homeService';
import { OleoleService } from '../../global/oleole-service';
import { SubscriptionService } from '../../pages/subscriptions/subscriptionService';
//import { TermsConditionsGiftingPage } from '../../pages/terms-conditions-gifting/terms-conditions-gifting';

import { HandleError } from '../../global/handleerror';
import { AlertService } from '../../global/alert-service';
import { ReloadPage } from '../reload/reload';
import { Events } from "ionic-angular";
import { CreditManagePage } from "../services/credit-manage/credit-manage";
import { OleOleTermsConditionsPage } from "../../pages/services/oleole-shop/oleole-terms-conditions/oleole-terms-conditions"
/**
 * Generated class for the ConfirmationScreensPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-confirmation-screens',
  templateUrl: 'confirmation-screens.html',
})
export class ConfirmationScreensPage {
  @ViewChild(Nav) nav: Nav;
  myVar: boolean = false;
  mobileNo: any;
  confirmDetails: any;
  loader: any;
  userInfo: any;
  isRoaming: boolean;
  isMaxUp: boolean = false;

  personalizedMessage: any;
  numberToSendAndGet: any;
  isNumberAvaliableToGift: boolean = false;
  gift_request: any;
  giftTypes: any;
  isVoucher: boolean = false;
  isEnabledOleoleGift: boolean = false;

  prebillAmount: any = '';

  validityDetails: any;
  isValidityExt: boolean = false;
  newValidityDate: any;
  AmountDeducted: any;
  newCreditBalance: any;
  isCreditTransfer: boolean = false;
  creditTransferDetails: any;
  debitedFrom: any;
  creditedTo: any;
  amountTransfer: any;
  totalChrge: any;
  newCreditBal: any;

  common_number: any;
  valueForTotal: any;
  isCreditAdv: any;
  creditAdvanceDetails: any;
  cardDenom: any;
  finalDenom: any;
  remainingBal: any;
  partOfTitle: any;


  title: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public gatewayService: GatewayService,
    public dashboardService: DashboardService,
    public oleoleService: OleoleService,
    public handleerror: HandleError,
    private alertCtrlInstance: AlertService,
    public events: Events,
    public subscriptionService: SubscriptionService,
    private renderer: Renderer2) {

    this.partOfTitle = this.navParams.get('header_title')
    this.title = this.partOfTitle;
    // this.isMaxUp = this.navParams.get('isMaxUp');
    this.isMaxUp = (this.navParams.get('isMaxUp') != undefined && this.navParams.get('isMaxUp') == true) ? true : false;

    this.userInfo = this.dashboardService.GetUserInfo();
    this.isRoaming = this.navParams.get('isRoaming');

    this.confirmDetails = this.navParams.get('confirmDetails');
    this.mobileNo = this.gatewayService.GetMobileNumber();
    this.gift_request = this.navParams.get('gift_request');

    this.isVoucher = this.navParams.get('isVoucher');
    this.validityDetails = this.navParams.get('validityDetails');
    var validDet = JSON.stringify(this.validityDetails);
    console.log("ConfirmationScreensPage -> validityDetails =", validDet);
    this.isValidityExt = this.navParams.get('isValidityExt');
    console.log("ConfirmationScreensPage -> isValidityExt =", this.isValidityExt);

    this.isCreditTransfer = this.navParams.get('isCreditTransfer');
    console.log("ConfirmationScreensPage -> isCreditTransfer =", this.isCreditTransfer);
    this.creditTransferDetails = this.navParams.get('creditTransferDetails');
    var creditDet = JSON.stringify(this.creditTransferDetails);
    console.log("ConfirmationScreensPage -> creditTransferDetails =", creditDet);

    this.isCreditAdv = this.navParams.get('isCreditAdv');
    console.log("ConfirmationScreensPage -> isCreditAdv =", this.isCreditAdv);
    this.creditAdvanceDetails = this.navParams.get('creditAdvanceDetails');
    var creditAdv = JSON.stringify(this.creditAdvanceDetails);
    console.log("ConfirmationScreensPage -> creditAdvanceDetails =", creditAdv);

    console.log("ConfirmationScreensPage -> partOfTitle =", this.partOfTitle);
    console.log("ConfirmationScreensPage -> isMaxUp =", this.isMaxUp);
    console.log("ConfirmationScreensPage -> isRoaming ", this.isRoaming);
    console.log("ConfirmationScreensPage -> isVoucher ", this.isVoucher);

    // oleole MCP and Xpax
    this.personalizedMessage = this.navParams.get('personalizedMessage');
    this.numberToSendAndGet = this.navParams.get('numberToSendAndGet');
    this.isNumberAvaliableToGift = this.navParams.get('numberToSendAndGet') != undefined ? true : false;

    this.giftTypes = this.navParams.get('giftTypes');

    console.log("ConfirmationScreensPage -> personalizedMessage ", this.personalizedMessage);
    console.log("ConfirmationScreensPage -> numberToSendAndGet ", this.numberToSendAndGet);
    console.log("ConfirmationScreensPage -> isNumberAvaliableToGift ", this.isNumberAvaliableToGift);
    console.log("ConfirmationScreensPage -> gift_request ", this.gift_request);
    console.log("ConfirmationScreensPage -> giftTypes ", this.giftTypes);
    console.log("ConfirmationScreensPage -> valueForTotal ", this.valueForTotal);

  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Confirmation screen", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmationScreensPage');
    if (this.isValidityExt == true) {
      this.mobileNo = this.validityDetails.MobileNumber;
      this.newValidityDate = this.validityDetails.NewValidityDate;
      this.AmountDeducted = this.validityDetails.AmountDeducted;
      this.newCreditBalance = this.validityDetails.NewCreditBalance;
    }
    else if (this.isCreditTransfer == true) {
      this.debitedFrom = this.creditTransferDetails.debitedFrom;
      this.creditedTo = this.creditTransferDetails.creditedTo;
      this.amountTransfer = this.creditTransferDetails.amountTransfer;
      this.totalChrge = this.creditTransferDetails.totalChrge;
      this.newCreditBal = this.creditTransferDetails.newCreditBal;

    }
    else if (this.isCreditAdv == true) {
      this.mobileNo = this.creditAdvanceDetails.mobileNumber;
      this.cardDenom = this.creditAdvanceDetails.cardDenom;
      this.finalDenom = this.creditAdvanceDetails.finalDenom;
      this.remainingBal = this.creditAdvanceDetails.remainingBal;
    }


    if (this.userInfo.ListOfServices.Services.ServiceType == "Prepaid" || this.userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Prepaid") {

      var prepaidCurrentBalance = this.dashboardService.GetBalancePrepaidData();
      console.log("prepaidCurrentBalance ", prepaidCurrentBalance);
      var balanceRecord = prepaidCurrentBalance.BalanceRecordList.BalanceRecord;

      if (balanceRecord.length > 1) {
        for (let obj in balanceRecord) {
          if (balanceRecord[obj].AccountType == 2000) {
            var amount = (balanceRecord[obj].Balance / 10000).toString();
            this.prebillAmount = parseFloat(amount).toFixed(2);
            //this.billingCycle = this.formatDate(balanceRecord[obj].ExpireTime);
          }
        }
      } else {
        var amount = (prepaidCurrentBalance.BalanceRecordList.BalanceRecord.Balance / 10000).toString();
        this.prebillAmount = parseFloat(amount).toFixed(2);
        // this.billingCycle = this.formatDate(this.prepaidCurrentBalance.BalanceRecordList.BalanceRecord.ExpireTime);
      }

      this.prebillAmount = parseFloat(amount).toFixed(2);
      console.log("prebillAmount ", this.prebillAmount);

      // this.prebillAmount = "RM" + (parseFloat(this.prebillAmount) - parseFloat((this.valueForTotal ? this.valueForTotal : (this.confirmDetails["PLAN_AMOUNT"] != undefined ? this.confirmDetails["PLAN_AMOUNT"] : (this.confirmDetails["PRODUCT_PRICE"] != undefined ? ((this.confirmDetails["PRODUCT_PRICE"]).search("RM") != -1 ? '' : 'RM') : ((this.confirmDetails["PASS_PRICE_DISPLAY"]).search("RM") != -1 ? '' : 'RM'))) + " " + (this.confirmDetails["PRODUCT_PRICE"] != undefined ? this.confirmDetails["PRODUCT_PRICE"] : this.confirmDetails["PASS_PRICE_DISPLAY"])).trim().substr(2))).toFixed(2);
      this.prebillAmount = (parseFloat(this.prebillAmount) - parseFloat((this.valueForTotal ? this.valueForTotal : (this.confirmDetails["PLAN_AMOUNT"] != undefined ? this.confirmDetails["PLAN_AMOUNT"] : (this.confirmDetails["PRODUCT_PRICE"] != undefined ? ((this.confirmDetails["PRODUCT_PRICE"]).search("RM") != -1 ? '' : 'RM') : ((this.confirmDetails["PASS_PRICE_DISPLAY"]).search("RM") != -1 ? '' : 'RM'))) + " " + (this.confirmDetails["PRODUCT_PRICE"] != undefined ? this.confirmDetails["PRODUCT_PRICE"] : this.confirmDetails["PASS_PRICE_DISPLAY"])).trim().substr(2))).toFixed(2);

      console.log("After calulation prebillAmount = ", this.prebillAmount);

      // if(this.prebillAmount < 1) {
      //   const alerts = this.alertCtrl.create({
      //     title: 'Uh Oh. Your Credit Balance is a Bit Low',
      //     // subTitle: ,
      //     message: "Please top-up your credit balance before proceeding",
      //     // (this.confirmDetails.PLAN_DESCRIPTION != undefined? this.confirmDetails.PLAN_DESCRIPTION : this.confirmDetails.PASS_NAME)+" "+"has not been added to mobile "+this.mobileNo,
      //     buttons: [
      //         {

      //             text: "Reload",
      //             handler: () => {
      //                 console.log('Confirmation Page Navigate to reload');
      //                 this.subscriptionService.notifyObservers();
      //                   // this.navCtrl.setRoot(ReloadPage);
      //                   // this.navCtrl.popToRoot();
      //                 // resolve(false);
      //             }
      //         },{
      //             text: 'Cancel',
      //             handler: () => {
      //                 console.log('Confirmation Page Cancel clicked');
      //                 // this.navCtrl.popToRoot();
      //                 // resolve(false);
      //             }
      //         }
      //     ],
      //     cssClass: 'error-message',
      //     enableBackdropDismiss: false
      //   });
      //   alerts.present();
      // }

    }

    console.log(" Total Value ", this.valueForTotal ? this.valueForTotal : (this.confirmDetails["PLAN_AMOUNT"] != undefined ? this.confirmDetails["PLAN_AMOUNT"] : (this.confirmDetails["PRODUCT_PRICE"] != undefined ? ((this.confirmDetails["PRODUCT_PRICE"]).search("RM") != -1 ? '' : 'RM') : ((this.confirmDetails["PASS_PRICE_DISPLAY"]).search("RM") != -1 ? '' : 'RM'))) + " " + (this.confirmDetails["PRODUCT_PRICE"] != undefined ? this.confirmDetails["PRODUCT_PRICE"] : this.confirmDetails["PASS_PRICE_DISPLAY"]))

  }


  openGiftingTnC() {
    this.navCtrl.push(OleOleTermsConditionsPage);
  }

  toggleList() {
    event.stopPropagation();

    this.myVar = !this.myVar;
    console.log("toggleList ", this.myVar);
  }

  presentsucess(details, isGbShare, isForOleoleSelf) {
    var _that = this;
    _that.loader = _that.gatewayService.LoaderService();
    //loader.duration = 2000;
    _that.loader.present();

    this.common_number = this.mobileNo;

    // to check is user is gbshare admin or not
    var adminNumber = this.userInfo["ListOfServices"].Services.BillingType;

    console.log("ConfirmationScreensPage -> presentsucess -> adminNumber = ", adminNumber);

    if (adminNumber == "Billable" && this.userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid" && this.userInfo.ListOfServices.Services.SmeGroupId != "" && isGbShare) {

      var isLegacy = '';
      if (this.navParams.get('islegacy') && this.navParams.get('islegacy') != undefined) {
        isLegacy = 'yes';
      }
      else {
        isLegacy = 'no';
      }

      var params_gbshareAdmin = {
        "msisdn": "6" + this.mobileNo,//"60193522834"//this.localStorage.get("MobileNum","")
        "bnumber": "6" + this.mobileNo, // self gift for gbshare number
        "operation_id": "1", // old one "operation_id":"MAXUP",
        "txn_id": this.gatewayService.generateRandomNum(16),
        "amount": details.PRODUCT_PRICE || details.PLAN_AMOUNT,
        "product_id": details.PRODUCT_ID,
        "isMaxUp": this.isMaxUp,
        "isLegacy": isLegacy,
        "packFlag": this.confirmDetails.PLAN_DESCRIPTION,
        "title": this.navParams.get('header_title')
      };

      console.log("ConfirmationScreensPage -> presentsucess -> (gbshare adminnumber)params = ", params_gbshareAdmin);

      _that.gatewayService.CallAdapter(ConstantData.adapterUrls.GbShareAdmin, "POST", params_gbshareAdmin).then(function (res) {
        console.log("********************************************** component success GbShareAdmin SubscribeTo Internet AddOn " + JSON.stringify(res));
        _that.loader.dismiss();

        var results_json = JSON.parse(JSON.stringify(res));
        // _that.alertToShow(result);
        var funcName = "SubscriptionAddOnInfoSubscribe";
        _that.handleerror.handleErrorSubscriptionAddOnInfoSubscribe(results_json, funcName).then((result) => {
          if (result == true) {
            console.log("GbShareAdmin subscription subscribe list ", result);
            _that.loader.dismiss();
            _that.alertToShow(results_json);

          }
          else {
            console.log("GbShareAdmin subscription subscribe list else ", result);
            _that.loader.dismiss();
            _that.alertToShow(results_json);
            // reject("error : no data found.");
            // _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
          }
        }, (err) => {
          console.log("GbShareAdmin subscription subscribe list err ", err);
          _that.loader.dismiss();
          // reject("error : no data found.");
          // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "You'll receive an SMS confirmation shortly.", "OK");
          // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.", "OK");
          _that.showAlertForHangOne();
          // _that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length() - (2 + 1)));
        });
      });

    }
    else if (this.isRoaming != true && this.isValidityExt != true) {

      var isLegacy = '';
      if (this.navParams.get('islegacy') && this.navParams.get('islegacy') != undefined) {
        isLegacy = 'yes';
      }
      else {
        isLegacy = 'no';
      }

      console.log("ConfirmationScreensPage -> presentsucess -> isLegacy = ", isLegacy);
      console.log("ConfirmationScreensPage -> presentsucess -> navParams.get('islegacy')  = ", this.navParams.get('islegacy'));
      console.log("ConfirmationScreensPage -> presentsucess -> navParams.get('header_title')  = ", this.navParams.get('header_title'));

      console.log("ConfirmationScreensPage -> presentsucess -> isForOleoleSelf = ", isForOleoleSelf);

      var params = {
        "msisdn": "6" + this.mobileNo,//"60193522834"//this.localStorage.get("MobileNum","")
        "operation_id": (this.isMaxUp ? "MI_PUR" : "MAXUP"), // old one "operation_id":"MAXUP",
        "txn_id": this.gatewayService.generateRandomNum(16),
        "amount": details.PRODUCT_PRICE || details.PLAN_AMOUNT,
        "product_id": (isForOleoleSelf && details.PRODUCT_ID_NORMAL != undefined) ? details.PRODUCT_ID_NORMAL : details.PRODUCT_ID,//Product_ID_Normal for ole ole buy_for_self categories and PRODUCT_ID for subscription
        "isMaxUp": this.isMaxUp,
        "isLegacy": isLegacy,
        "packFlag": this.confirmDetails.PLAN_DESCRIPTION,
        "title": this.navParams.get('header_title')
      };

      console.log("ConfirmationScreensPage -> presentsucess -> isMaxUp = ", this.isMaxUp);

      console.log("ConfirmationScreensPage -> presentsucess -> params = ", params);
      var funcName = (isLegacy == 'yes') ? "SubscriptionInternetLegacyInfoSubscribeForMI" : ((this.isMaxUp) ? "SubscriptionAddOnInfoSubscribeForMI" : "SubscriptionAddOnInfoSubscribe");

      console.log("ConfirmationScreensPage -> presentsucess -> funcName = ", funcName);

      _that.gatewayService.CallAdapter(ConstantData.adapterUrls.SubscribeToAddOn, "POST", params).then(function (res) {
        console.log("********************************************** component success SubscribeToAddOn" + JSON.stringify(res));
        _that.loader.dismiss();

        var results_json = JSON.parse(JSON.stringify(res));
        // _that.alertToShow(result);

        _that.handleerror.handleErrorSubscriptionAddOnInfoSubscribe(results_json, funcName).then((result) => {
          if (result == true) {
            console.log("subscription subscribe list ", result);
            _that.loader.dismiss();
            _that.alertToShow(results_json);

          }
          else {
            console.log("subscription subscribe list else ", result);
            _that.loader.dismiss();
            _that.alertToShow(results_json);
            // reject("error : no data found.");
            // _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
          }

        }, (err) => {
          console.log("subscription subscribe list err ", err);
          _that.loader.dismiss();
          // reject("error : no data found.");
          // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "You'll receive an SMS confirmation shortly.", "OK");
          // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.", "OK");

          // _that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length() - (2 + 1)));
          _that.showAlertForHangOne();
        });

      })
        .catch(function (e) {
          console.log("******************************************** component failure SubscribeToAddOn " + e);
          console.log("error user info SubscribeToAddOn ConfirmationScreensPage");
          // reject(e);
          _that.loader.dismiss();
          if (e == "No internet connection")
            return;
          // _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
          // for timeout and incorrect response
          // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "You'll receive an SMS confirmation shortly.", "OK");
          // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.", "OK");

          // _that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length() - (2 + 1)));
          _that.showAlertForHangOne();
        });
    }
    else if (this.isRoaming == true && (this.userInfo.ListOfServices.Services.ServiceType == "Postpaid" || this.userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Postpaid")) {

      var param_post = {
        "sref": "6" + this.mobileNo,//"60193522834"//this.localStorage.get("MobileNum","")
        "product_id": details.PASS_ID, // pass from SubscriptionRoamingListForPostpaid (Product id will be the pass id from ROAMQRY). using 60104060032 with pass id MI02210
        "txn_id": this.gatewayService.generateRandomNum(16),
        "packFlag": this.confirmDetails.PASS_NAME,
        "amount": details.PASS_PRICE_DISPLAY,
        "title": this.navParams.get('header_title')
      };

      // "title": this.navParams.get('header_title')

      console.log("param_post roaming ", JSON.stringify(param_post));

      this.gatewayService.CallAdapter(ConstantData.adapterUrls.SubscribeRoamingPlanForPostpaid, "POST", param_post).then(function (res) {
        console.log("********************************************** component success Subscribing Roaming Plan For Postpaid" + JSON.stringify(res));
        // alert("SubscribingRoamingPlanFor Postpaid "+JSON.stringify(res));

        var results_json = JSON.parse(JSON.stringify(res));
        _that.handleerror.handleErrorRoamingSubscriptionSubscribe(results_json, "RoamingSubscriptionSubscribe").then((result) => {
          if (result == true) {
            console.log("subscription  roaming postpaid subscribe list ", result);

            _that.loader.dismiss();
            _that.alertToShow(results_json);
          }
          else {
            console.log("subscription  roaming postpaid subscribe list else ", result);
            _that.loader.dismiss();
            if (results_json.MAXUPPurchaseResponse != undefined) {

              if (results_json.MAXUPPurchaseResponse.ErrorCode == "Er400001" || results_json.MAXUPPurchaseResponse.ErrorCode == "Err40000") {
                // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "You'll receive an SMS confirmation shortly.", "OK");
                // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.", "OK");

                // _that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length() - (2 + 1)));
                _that.showAlertForHangOne();
              }
              else
                _that.alertToShow(results_json);
            } else {
              if (results_json.ROAMPASSPURCHASERESPONSE.ResponseCode == "Err40105") {
                //Err40105
                // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "You'll receive an SMS confirmation shortly.", "OK");
                // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.", "OK");
                // _that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length() - (2 + 1)));
                _that.showAlertForHangOne();
              } else
                _that.alertToShow(results_json);
            }


            // reject("error : no data found.");
            // _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
          }

        }, (err) => {
          console.log("subscription roaming postpaid subscribe list err ", err);
          _that.loader.dismiss();
          // reject("error : no data found.");
          _that.alertToShow(results_json);
          // _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
        });

      })
        .catch(function (e) {
          console.log("******************************************** component failure SubscribingRoamingPlanForPostpaid " + e);
          // alert("error user info SubscribingRoamingPlanFor Postpaid");
          // reject(e);
          _that.loader.dismiss();
          if (e == "No internet connection")
            return;
          // _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
          // for timeout and incorrect response
          // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "You'll receive an SMS confirmation shortly.", "OK");
          // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.", "OK");
          // _that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length() - (2 + 1)));
          _that.showAlertForHangOne();
        });

    }
    else if (this.isRoaming == true && (this.userInfo.ListOfServices.Services.ServiceType == "Prepaid" || this.userInfo.ListOfServices.Services.Pre_Pos_Indicator == "Prepaid")) {

      var param_pre = {
        "sref": "6" + this.gatewayService.GetMobileNumber(),//"60193522834"//this.localStorage.get("MobileNum","")
        "product_id": details.PASS_ID, // pass from SubscriptionRoamingListForPostpaid (Product id will be the pass id from ROAMQRY). using 60104060032 with pass id MI02210
        "txn_id": this.gatewayService.generateRandomNum(16),
        "packFlag": this.confirmDetails.PASS_NAME,
        "amount": details.PASS_PRICE_DISPLAY,
        "title": this.navParams.get('header_title')
      };

      console.log("param_pre roaming ", JSON.stringify(param_pre));

      this.gatewayService.CallAdapter(ConstantData.adapterUrls.SubscribeRoamingPlanForPrepaid, "POST", param_pre).then(function (res) {
        console.log("********************************************** component success Subscribing Roaming Plan For prepaid" + JSON.stringify(res));

        _that.loader.dismiss();

        var results_json = JSON.parse(JSON.stringify(res));
        // _that.alertToShow(result);

        _that.handleerror.handleErrorRoamingSubscriptionSubscribe(results_json, "RoamingSubscriptionSubscribe").then((result) => {
          if (result == true) {
            console.log("subscription  roaming prepaid subscribe list ", result);
            _that.loader.dismiss();
            _that.alertToShow(results_json);
          }
          else {
            console.log("subscription  roaming prepaid subscribe list else ", result);
            _that.loader.dismiss();
            // reject("error : no data found.");

            if (results_json.MAXUPPurchaseResponse != undefined) {

              if (results_json.MAXUPPurchaseResponse.ErrorCode == "Er400001" || results_json.MAXUPPurchaseResponse.ErrorCode == "Err40000") {
                // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "You'll receive an SMS confirmation shortly.", "OK");
                // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.", "OK");
                // _that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length() - (2 + 1)));
                _that.showAlertForHangOne();
              }
              else
                _that.alertToShow(results_json);
            } else {
              if (results_json.ROAMPASSPURCHASERESPONSE.ResponseCode == "Err40105") {
                //Err40105
                // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "You'll receive an SMS confirmation shortly.", "OK");
                // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.", "OK");
                // _that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length() - (2 + 1)));
                _that.showAlertForHangOne();
              } else
                _that.alertToShow(results_json);
            }


            // _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
          }

        }, (err) => {
          console.log("subscription roaming prepaid subscribe list err ", err);
          _that.loader.dismiss();
          // reject("error : no data found.");
          // _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
          // for timeout and incorrect response
          // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "You'll receive an SMS confirmation shortly.", "OK");
          // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.", "OK");
          // _that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length() - (2 + 1)));
          _that.showAlertForHangOne();
        });

      })
        .catch(function (e) {
          console.log("******************************************** component failure SubscribingRoamingPlanForPrepaid " + e);
          // alert("error user info SubscribingRoamingPlanFor prepaid");
          // reject(e);
          _that.loader.dismiss();
          if (e == "No internet connection")
            return;
          // _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
          // for timeout and incorrect response
          // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "You'll receive an SMS confirmation shortly.", "OK");
          // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.", "OK");

          // _that.navCtrl.popTo(_that.navCtrl.getByIndex(_that.navCtrl.length() - (2 + 1)));
          _that.showAlertForHangOne();
        });
    }
  }
  // oleole
  presentSucessForGift(details) {

    this.common_number = this.numberToSendAndGet;

    var loader = this.gatewayService.LoaderService();
    loader.present();



    this.isEnabledOleoleGift = true;
    var userInfo = this.dashboardService.GetUserInfo();
    var params = {};
    console.log("ConfirmationScreensPage -> presentSucessForGift() -> details = ", JSON.stringify(details));
    console.log("ConfirmationScreensPage -> presentSucessForGift() -> this.giftTypes = ", this.giftTypes);
    console.log("ConfirmationScreensPage -> presentSucessForGift() -> this.gift_request = ", this.gift_request);
    console.log("ConfirmationScreensPage -> presentSucessForGift -> navParams.get('header_title')  = ", this.navParams.get('header_title'));
    console.log("ConfirmationScreensPage -> presentSucessForGift -> navParams.get('msg')  = ", this.navParams.get('msg'));
    // var _that = this;
    // _that.loader = _that.gatewayService.LoaderService();

    // _that.loader.present();

    //http://10.1.62.78:9006/tmf?action=mfp2/GIFTREQ2&sref=60136429564&bnumber=60134355979&keyword=send_gift&product_id=45903&trxid=eDgUqDrpgO&msg=&brand_type=PREPAID



    if (this.isVoucher) {
      console.log("ConfirmationScreensPage -> presentSucessForGift() -> (if) isVoucher = ", this.isVoucher);

      params = {
        "msisdn": "6" + this.mobileNo, //"60193522834"
        "bnumber": "6" + this.numberToSendAndGet, // number to send , receive & gift for vouchers
        "keyword": (this.gift_request == 'buy_gift_for' || this.gift_request == 'buy_for_self') ? 'send_gift_voucher' : 'request_gift_voucher',
        "product_id": details.PRODUCT_ID,
        "trxid": this.gatewayService.generateRandomNum(16),
        "brand_type": (userInfo.ListOfServices.Services.Pre_Pos_Indicator).toUpperCase(),
        "amount": (details.PLAN_AMOUNT).substr(2),
        "title": this.navParams.get('header_title') + " OleOle",
        "msg": this.navParams.get('msg')
      };
    }
    else {

      console.log("ConfirmationScreensPage -> presentSucessForGift() -> (else) isVoucher = ", this.isVoucher);

      params = {
        "msisdn": "6" + this.mobileNo, //"60193522834"
        "bnumber": "6" + this.numberToSendAndGet, // number to send , receive & self gift
        "keyword": (this.gift_request == 'buy_gift_for' || this.gift_request == 'buy_for_self') ? 'send_gift' : 'request_gift',
        "product_id": details.PRODUCT_ID,
        "trxid": this.gatewayService.generateRandomNum(16),
        "brand_type": (userInfo.ListOfServices.Services.Pre_Pos_Indicator).toUpperCase(),
        "amount": details.PRODUCT_PRICE,
        "title": this.navParams.get('header_title') + " OleOle",
        "msg": this.navParams.get('msg')
      };
    }

    console.log("ConfirmationScreensPage -> yy presentSucessForGift() -> params = ", params);

    // Request : no OTP
    if (this.gift_request != 'buy_gift_for' && this.gift_request != 'buy_for_self' && this.gift_request != undefined) {
      loader.dismiss();
      console.log("Requesting.................");
      // call gift request vourchase
      this.oleoleService.callGiftAdapter(params).then((result) => {
        this.isEnabledOleoleGift = false;
        loader.dismiss();
        console.log("presentSucessForGift request gift success ", JSON.stringify(result));
        this.alertToShow(result);

      }, (err) => {
        
        loader.dismiss();
        this.isEnabledOleoleGift = false;
        console.log("presentSucessForGift request gift error ", err);

        this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - (2 + 1)));
      });
    } // send gift
    else if (this.gift_request != undefined && this.gift_request == 'buy_gift_for') {
      console.log("Sending.......................... ");
      loader.dismiss();
      this.registerMobileConnectUser(params);

      // call gift send vourchase
      // this.oleoleService.callGiftAdapter(params).then((result) => {

      // console.log("presentSucessForGift send gift oleole success ",JSON.stringify(result));

      // goto mobile OTP page latter
      // this.navCtrl.setRoot(ConnectTacTwoPage,{"oleoleBuyFor":params});

      // this.alertToShow(result);

      // },(err)=>{

      //   console.log("presentSucessForGift send gift oleole error ",err);
      // });
    } // for self
    else if (this.gift_request != undefined && this.gift_request == 'buy_for_self') {
      this.isEnabledOleoleGift = false;
      console.log("When buying for self.................. ", JSON.stringify(details));
      this.presentsucess(details, false, true);
      loader.dismiss();
    }
  }



  registerMobileConnectUser(params) {

    var loader = this.gatewayService.LoaderService();
    loader.present();
    console.log("ConfirmationScreensPage -> registerMobileConnectUser() -> params ", params);

    var _this = this;
    var msisdn = this.mobileNo;

    var params_register = { "MSISDN": msisdn };

    console.log("ConfirmationScreensPage -> registerMobileConnectUser() -> msisdn = ", msisdn);
    console.log("ConfirmationScreensPage -> registerMobileConnectUser() -> params_register = ", params_register);
    var a = false;
    // if( a) {
    if (ConstantData.isLoginBypassed == true) {
      loader.dismiss();
      _this.bypass(params);
    }
    else {
      // return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.registerMobileConnect2, "POST", params_register).then((res_register) => {
        _this.isEnabledOleoleGift = false;
        console.log("register send olole gift OTP response : " + JSON.stringify(res_register));
        //  resolve(res_register);
        loader.dismiss();
        _this.navCtrl.push(ConnectTacTwoPage, { "oleoleBuyFor": params, "register_state": res_register, "giftDetails": _this.confirmDetails });


        //_that.detailedUsageService.SetDetailedUsageSMSPostpaid(res_register);
      })
        .catch(function (e) {
          _this.isEnabledOleoleGift = false;
          loader.dismiss();
          console.log("******************************************** component oleole gift failure registerMobileConnectUser " + e);
          console.log("error register  oleole gift");
          // reject(e);
        });
      // });
    }
  }

  //  for messages
  alertToShow(result) {
    var send_or_req = '';

    if (this.gift_request != 'buy_gift_for' && this.gift_request != 'request_gift_from' && this.gift_request != undefined) {
      send_or_req = 'self';
    }
    else if (this.gift_request != 'buy_gift_for' && this.gift_request != 'buy_for_self' && this.gift_request != undefined) {
      send_or_req = 'requesting';
    }
    else if (this.gift_request != 'request_gift_from' && this.gift_request != undefined) {
      send_or_req = 'sending';
    }
    else if ((this.gift_request != 'request_gift_from' || this.gift_request != 'buy_gift_for' || this.gift_request != 'buy_for_self') && this.gift_request == undefined) {
      send_or_req = "subscription";
    }
    console.log(" common_number ", this.common_number);
    console.log("ConfirmationScreensPage -> send_or_req ", send_or_req);
    
    this.events.publish("tittle", this.title);
    this.oleoleService.alertShow(result, this.confirmDetails, this.common_number, send_or_req).then(res => {
      console.log("res TRY alertToShow ", res)
      // if (res) {
        // this.navCtrl.popToRoot();
        console.log("this.navCtrl.length() ", this.navCtrl.length());
        console.log("this.navCtrl.getByIndex(this.navCtrl.length()-(1)) ", this.navCtrl.getByIndex(this.navCtrl.length() - (1)));
        this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - (2 + 1)));
      // }
    });
  }
  goToValidity() {
    var isConfirmed = true;
    this.navCtrl.pop();
    this.events.publish('isValidity', isConfirmed);
    this.events.publish('callSubmitValidityConfirmed', isConfirmed);

  }
  goToCreditTransfer() {
    var isConfirmed = true;
    console.log(this.creditTransferDetails);
    this.events.publish('callTransferAmount', this.creditTransferDetails);
  }
  goToCreditAdvance() {
    var isConfirmed = true;
    this.navCtrl.pop();
    this.events.publish('isCreditAdv', isConfirmed);
    this.events.publish('callAdvanceDenominations', isConfirmed);

  }

  bypass(params) {
    this.navCtrl.push(ConnectTacTwoPage, { "oleoleBuyFor": params, "register_state": "res_register", "giftDetails": this.confirmDetails });
  }

  // total for gift + txn
  getTotal(val) {
    var item_charge: any = (this.isNumberAvaliableToGift && (this.gift_request != 'buy_gift_for' && this.gift_request != 'undefined')) ? '0.20' : '0.50';

    console.log(
      "item_charge => ", item_charge,
      "\n this.isNumberAvaliableToGift => ", this.isNumberAvaliableToGift,
      "\n this.gift_request => ", this.gift_request,
      "\n val => ", val,
      "\n parseFloat(item_charge) => ", parseFloat(item_charge),
      "\n parseFloat(val.substr(2)) => ", parseFloat(val.substr(2)),
      "\n Total ", parseFloat(item_charge) + parseFloat(val.substr(2))
    );

    return (parseFloat(item_charge) + parseFloat(val.substr(2)));
  }

  theTotal(val, type, plan, voucher) {


    if (type == 'request_gift_from')
      this.valueForTotal = "RM " + (val).toFixed(2);
    else if (type == 'buy_gift_for' && plan != undefined)
      this.valueForTotal = "RM " + (parseFloat(val) + parseFloat(plan)).toFixed(2);
    else if (type == 'buy_gift_for' && voucher != undefined)
      this.valueForTotal = "RM " + (parseFloat(val) + parseFloat(voucher.substr(2))).toFixed(2);

    if (this.gift_request == 'buy_for_self' && type == 'buy_for_self' && plan != undefined)
      this.valueForTotal = "RM " + (val).toFixed(2);

    if (this.gift_request == 'buy_for_self' && type == 'buy_for_self' && voucher != undefined)
      this.valueForTotal = "RM " + (val).toFixed(2);

    console.log("Hia ", val, " -> ", type, " -->", plan, " -->", voucher);
    console.log("valueForTotal ", this.valueForTotal);
  }

  showAlertForHangOne(){

    // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.", "OK");
    
    const alert_success = this.alertCtrl.create({
      title: 'Hang on. We are working on your request.',
      // subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
      message: "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.",
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Call to showAlertForHangOnes Action Clicked');
            this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - (2 + 1)));
          }
        }],
      cssClass: 'success-message'
    });
    alert_success.present();
  }

}
