import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { GatewayService } from '../../../global/utilService';
import { TransactionHistroyService } from '../../../pages/transaction-history/transactionHistroyService';
import { FileOpener } from '@ionic-native/file-opener';
import { File } from '@ionic-native/file';
import { AlertService } from '../../../global/alert-service';
/**
 * Generated class for the TransactionHistoryDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-transaction-history-details-pay-bill',
  templateUrl: 'history-details-pay-bill.html',
})
export class TransactionHistoryDetailsPayBillPage {
  Details: any;
  StartDate: any;
  EndDate: any;
  MobileNumber: any;
  AccountNumber: any;
  InvoiceID: any;
  objiectidforPdf: any;
  loader: any;
  blobPdf: any;
  amountDue: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public gatewayService: GatewayService, public transcationsHistorys: TransactionHistroyService,
    private fileOpener: FileOpener, public file: File, public platform: Platform,
    private alertCtrlInstance: AlertService, private renderer: Renderer2) {
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Transaction history details pay bill", this.renderer);
  }

  ionViewDidLoad() {
    this.Details = this.navParams.get("param1");
    this.objiectidforPdf = this.navParams.get("param2");
    this.MobileNumber = this.gatewayService.GetMobileNumber();
    this.StartDate = this.Details.BillCycleStart;
    this.EndDate = this.Details.BillCycleEnd;
    this.AccountNumber = this.Details.BillingAccountNumber;
    this.InvoiceID = this.Details.InvoiceNumber;
    this.amountDue = this.Details.TotalDue;
    if (typeof this.amountDue != 'undefined' && this.amountDue != 'NA') {
      this.amountDue = Number(this.amountDue).toFixed(2);
    }

    console.log('ionViewDidLoad TransactionHistoryDetailsPage' + this.objiectidforPdf);
  }

  showingPDF() {
    this.loader = this.gatewayService.LoaderService();
    this.loader.present();
    console.log("*****objiectidforPdf*******" + JSON.stringify(this.objiectidforPdf));
    this.transcationsHistorys.BillDownload(this.objiectidforPdf).then((res) => {
      console.log("*****BillDownload*******" + JSON.stringify(res));
      var encodedString;
      if (res["Envelope"].Body.getResponse.return.DataObjects.Contents.Value) {
        this.blobPdf = res["Envelope"].Body.getResponse.return.DataObjects.Contents.Value;
        var arrayBuffer = this._base64ToArrayBuffer(this.blobPdf);
        if (this.platform.is('android')) {
          this.file.writeFile(this.file.externalApplicationStorageDirectory, 'Bill.pdf', arrayBuffer, { replace: true }).then(res => {
            this.fileOpener.open(
              res.toInternalURL(),
              'application/pdf'
            ).then((res) => {

            }).catch(err => {
              console.log('open error');
            });
          }).catch(err => {
            console.log('save error');
          });
        } else {
          window.open("data:application/pdf;base64," + this.blobPdf, "_blank", "location=no,enableviewportscale=yes");
        }
      } else {
        this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      }


      this.loader.dismiss();




    }).catch(err => {
      this.loader.dismiss();
      console.log("*****reload*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
    });
  }

  _base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
  }

}
