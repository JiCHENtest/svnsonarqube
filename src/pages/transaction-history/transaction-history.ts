import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TransactionHistoryTransactionTabPage} from "./transaction-tab/transaction-tab";
import {TransactionHistoryBillPaymentTabPage} from "./bill-payment-tab/bill-payment-tab";
import { GatewayService } from '../../global/utilService';
/**
 * Generated class for the TransactionHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-transaction-history',
  templateUrl: 'transaction-history.html',
})
export class TransactionHistoryPage {

  transactionTab = TransactionHistoryTransactionTabPage;
  billPaymentTab = TransactionHistoryBillPaymentTabPage;
  Transaction:any;
  BillPayment:any;
  PostPaid:boolean = false;
  Prepaid:boolean  = false;
  MobileNumber:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public gatewayService: GatewayService, private renderer: Renderer2) {
  }

  // ionViewDidEnter() {
  //   this.gatewayService.resetAllAnalyticData();
  //   this.gatewayService.setAnalyticPageNameProfileSegment("Transaction history", this.renderer);
  // }

  ionViewDidLoad() {
    this.MobileNumber =  this.gatewayService.GetMobileNumber();
    if(this.gatewayService.isPrePaid()){
      this.Prepaid = true;
      this.PostPaid = false;
    }else{
      this.PostPaid = true;
      this.Prepaid = false;
    }
    console.log('ionViewDidLoad TransactionHistoryPage');
  }

}
