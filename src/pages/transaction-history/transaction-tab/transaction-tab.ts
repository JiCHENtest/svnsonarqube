import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TransactionHistoryDetailsPage} from "../history-details/history-details";
import { TransactionHistroyService } from '../../../pages/transaction-history/transactionHistroyService';
import { CreditManageService } from '../../../pages/services/credit-manage/CreditManageService';
import { GatewayService } from '../../../global/utilService';
import { OrderByFilterPipe } from '../../../pages/transaction-history/orderby_filter.pipe';
import { Pipe,PipeTransform } from '@angular/core';
import { orderBy } from 'lodash';
import { AlertService } from '../../../global/alert-service';
/**
 * Generated class for the TransactionHistoryTransactionTabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-transaction-history-transaction-tab',
  templateUrl: 'transaction-tab.html',
})
export class TransactionHistoryTransactionTabPage {
  MobileNumber :any;
  loader:any;
  rootNavCtrl : NavController;
  CurretMonth:any;
  NextMonth:any;
  LastMonth:any;
  CurrentMonthDetails:any;
  NextMonthDetails:any;
  LastMonthDetails:any;
  TotalHistroyDetails:any;
  currentMonthFlag:boolean = false;
  NextMonthFlag:boolean = false;
  LastmonthMonthFlag:boolean = false;
  creditA:boolean = false;
  EmptyStatus:boolean = false;
  CreditAdvanceObj:any;
  monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public transcationsHistorys:TransactionHistroyService, 
    public gatewayService: GatewayService, public creditManageService: CreditManageService,
    private alertCtrlInstance: AlertService, private renderer: Renderer2) {
    this. rootNavCtrl = navParams.get('rootNavCtrl');
    this.creditA = false;
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    if(this.gatewayService.isPrePaid()){
      this.gatewayService.setAnalyticPageNameProfileSegment("Credit transaction history", this.renderer);
    }else{
      this.gatewayService.setAnalyticPageNameProfileSegment("Transactions history", this.renderer);
    }
  }

  formatDate(val){
    if(val==""|| val==null || val==undefined)
    return;
    var dt = val.substring(6, 8)+'/'+val.substring(4, 6)+'/'+val.substring(0, 4);
    return dt;
  }

  formatTime(val){
    if(val==""|| val==null || val==undefined)
    return;
    var dt = val.substring(6, 8)+'/'+val.substring(4, 6)+'/'+val.substring(0, 4);
    return new Date(dt);
  }

  formatMonthName(val){
    if(val==""|| val==null || val==undefined)
    return;
    var Month = val.substring(4, 6);
    var MonthData = [{
      "01":"January",
      "02":"February",
      "03":"March",
      "04":"April",
      "05":"May",
      "06":"June",
      "07":"July",
      "08":"August",
      "09":"September",
      "10":"October",
      "11":"November",
      "12":"December"
    }]
    return MonthData[0][Month];
  }

  ionViewDidLoad() {
    this.loader = this.gatewayService.LoaderService();
    this.loader.present();
    this.MobileNumber =  this.gatewayService.GetMobileNumber();
    this.creditAdvanceTransaction();
    this.transcationsHistorys.FetchTransactionHistory(this.MobileNumber).then((res) => {
      console.log("*****transcationsHistorys*******" + JSON.stringify(res));
      this.TotalHistroyDetails = [];
      this.CurrentMonthDetails = [];
      this.NextMonthDetails = [];
      this.LastMonthDetails = [];
      this.CreditAdvanceObj = [];
      var d = new Date();

      if(res["resultSet"].length > 0){
      this.TotalHistroyDetails = res["resultSet"]

      this.CurretMonth = this.monthNames[d.getMonth()];

      if(this.CurretMonth == "February"){
        this.NextMonth = "January" ;
        this.LastMonth = "December";
      }else if (this.CurretMonth == "January"){
        this.NextMonth = "December" ;
        this.LastMonth = "November";
      }else{
        this.NextMonth = this.monthNames[d.getMonth()-1];
        this.LastMonth = this.monthNames[d.getMonth()-2];
      }
      this.TotalHistroyDetails.forEach(element => {
        if(this.CurretMonth == this.monthNames[element.RESPONSE_TIME.split('-')[1]-1])
          this.currentMonthFlag = true;
        if(this.NextMonth == this.monthNames[element.RESPONSE_TIME.split('-')[1]-1])
          this.NextMonthFlag = true;
        if(this.LastMonth == this.monthNames[element.RESPONSE_TIME.split('-')[1]-1])
          this.LastmonthMonthFlag = true;

          if(this.currentMonthFlag ==false && this.NextMonthFlag == false && this.LastmonthMonthFlag == false)
          this.EmptyStatus = true;
          else
          this.EmptyStatus = false;


          if(typeof element.CHARGES !='undefined' && element.CHARGES !='NA'){
            element.CHARGES = Number(element.CHARGES).toFixed(2);
          }
        var params = {
            "TransactionTitle":element.TRANSACTION_NAME,
            "CHARGES":element.CHARGES,
            "RESPONSE_TIME":element.RESPONSE_TIME.split('-')[2].substring(0,2)+'/'+element.RESPONSE_TIME.split('-')[1]+'/'+element.RESPONSE_TIME.split('-')[0],
            "MONTHNAME":this.monthNames[element.RESPONSE_TIME.split('-')[1]-1],
            "TYPE_TRANS":element.TYPE_TRANS,
            "TIME":element.RESPONSE_TIME

          }
          this.CurrentMonthDetails.push(params);


        //this.CurrentMonthDetails.push();
        console.log("*****saveCards*******" + JSON.stringify(params));
      });
    }else{
      this.EmptyStatus = true;
    }
   //   document.write("The current month is " + this.monthNames[d.getMonth()]);
      //str.split('-')[1];
      this.loader.dismiss();

    }).catch(err => {
      this.loader.dismiss();
      console.log("*****reload*******" + JSON.stringify(err));
      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      //this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
      //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });

    // this.creditManageService.creditAdvanceTransaction().then((res) => {
    // }).catch(err => {
    //   this.loader.dismiss();
    //   console.log("*****reload*******" + JSON.stringify(err));
    // });
    console.log('ionViewDidLoad TransactionHistoryTransactionTabPage');
  }




  creditAdvanceTransaction() {
    // var loader = this.gatewayService.LoaderService();
    // loader.present();
    try{
      this.creditManageService.creditAdvanceTransaction().then((res) => {
        console.log("creditAdvanceTransaction -> " + JSON.stringify(res));
        let obj:any = res;
        var data = obj.Envelope.Body.CreditAdvanceResponse.ResponseBody.AdvanceHistoryList.AdvanceHistory;
        if(data.length>0){
          for(var i in data){
              this.creditA = true;
              //alert(JSON.stringify(obj[i]))


              

              
              var params = {
                "TransactionTitle":"Credit Advance",
                "CHARGES":Number(data[i].AdvanceAmount/100).toFixed(2),
                "RESPONSE_TIME":this.formatDate(data[i].DateTaken),
                "MONTHNAME":this.formatMonthName(data[i].DateTaken),
                "TYPE_TRANS":"Credit Advance",
                "TIME":this.formatTime(data[i].DateTaken)
              }
              //alert(JSON.stringify(params));
              this.CreditAdvanceObj.push(params);
              //this.CurrentMonthDetails.push(params);
              this.EmptyStatus = false;
              console.log("*****Credit Advance*******" + JSON.stringify(params));
            }
        }
          //loader.dismiss();

      }).catch(err => {
        //loader.dismiss();
        console.log("creditAdvanceTransaction Error " + err);
        if (err == "No internet connection")
          return;
      });
          //this.presenterror("Please try again later");
        //});
      }catch(e){
        //loader.dismiss();
        console.log("catch"+e);
      }
  }


  goToDetailsPage (item) {
   var TransationStatus = "CreditTransactions";
    this.rootNavCtrl.push(TransactionHistoryDetailsPage,{
      param1:item,param2:TransationStatus
    });

  }



}
