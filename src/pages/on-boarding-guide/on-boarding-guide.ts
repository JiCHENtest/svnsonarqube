import { Component, Renderer2 } from "@angular/core";
import {NavController, NavParams,LoadingController} from "ionic-angular";
import {HomePage} from "../home/home";
import { GatewayService } from '../../global/utilService';
import { LocalStorage } from '../../global/localStorage';
/**
 * Generated class for the OnBoardingGuidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-on-boarding-guide',
  templateUrl: 'on-boarding-guide.html',
})
export class OnBoardingGuidePage {
  public isXPAX:boolean;
  constructor(public navCtrl: NavController, public localStorage:LocalStorage, public navParams: NavParams, 
    public loadingCtrl: LoadingController, public gatewayService:GatewayService,
    private renderer: Renderer2 ) {
      this.gatewayService.isPrePaid();
      this.localStorage.set("IsUserLoggedIn","true");
      this.localStorage.set("IsFirstTimeUser","false");
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("On boarding guide", this.renderer);
  }

  ionViewDidLoad() {

  }

  skipOnBoarding() {
    var loader = this.gatewayService.LoaderService();
    loader.present();
    this.navCtrl.setRoot(HomePage);
    loader.dismiss();
  }
}
