import { Component, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams, Events, ModalController } from 'ionic-angular';

import { GatewayService } from '../../../global/utilService';
import { PayBillService } from '../payBillService';
import { AlertService } from '../../../global/alert-service';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ReloadService } from '../../../global/reloadServices';
import { PaymentRecieptPage } from '../../payment-reciept/payment-reciept';
import { Keyboard } from '@ionic-native/keyboard';
import { DashboardService } from '../../../pages/home/homeService';
import { analyticTransactionDetailsInterface } from '../../../global/interface/analytic-transaction-details';


@Component({
  selector: 'page-bill-reload-code',
  templateUrl: 'bill-reload-code.html',
})
export class BillReloadCodePage {
  buttonDisable: boolean;
  loader:any;
  mobilenumber:any;
  digitalPinForm:any;
  loggedinNumber:any;
  dueBillAmount:any;
  facevalue:any;
  LoginbillingType:any;
  transactionDetails: analyticTransactionDetailsInterface;

  constructor(public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public navParams: NavParams,
    public alertCtrlInstance: AlertService,
    public gatewayService: GatewayService,
    public paybillService: PayBillService,
    public events: Events,
    public alertCtrl: AlertController,
  public reloadService:ReloadService,
  private keyboard: Keyboard,
  public modalCtrl: ModalController,
  public dashboardService:DashboardService,
  private renderer: Renderer2) {
      this.loggedinNumber = this.reloadService.gettingLoginNumber();
    this.buttonDisable = true;
    this.digitalPinForm = formBuilder.group({
      digitPin: ['', Validators.compose([Validators.maxLength(16), Validators.minLength(16), Validators.pattern('[0-9]*'), Validators.required])],
    });
    events.subscribe('Money', (dueAmount) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('Welcome::' + dueAmount);
      this.dueBillAmount = dueAmount;
    });
  }

  ionViewDidEnter() {
    this.initiateAnalyticTransactionJourney();
  }

  initiateAnalyticTransactionJourney(){
    this.transactionDetails = {};
    this.gatewayService.resetAllAnalyticData();
    this.transactionDetails.jouneryDepth = "initiation";
    this.transactionDetails.transactProduct = "Reload PIN";
    this.gatewayService.setAnalyticTransactionJourney("Reload PIN", this.renderer, this.transactionDetails);
  }

  ionViewDidLoad() {
    this.paybillService.SetTabInfo(3);
    this.LoginbillingType=this.gatewayService.GetBillingType();
    console.log('ionViewDidLoad BillReloadCodePage');
  }

  onChangeTime(pin) {
    if (pin.length == 16)
      this.buttonDisable = false;
    else
      this.buttonDisable = true;
  }

  validationPin(pinnumber) {
    //Analytic Part
    this.initiateAnalyticTransactionJourney();

    this.loader = this.gatewayService.LoaderService();
    this.loader.present();
    this.mobilenumber = this.paybillService.GetSelectMobileNumber();
   

    var SelectedMobileNumber = this.mobilenumber.slice(-9);

    var params = {
      "MobileNumber": SelectedMobileNumber,
      "Pin": pinnumber
    }

    this.paybillService.PostReloadPin(params).then((res) => {
      var AddAmount;
      var TotalBalance;
      console.log("Paybill -> postReload pin" + JSON.stringify(res));
      var responseCode = res["SOFTPIN_Response"];
      this.facevalue = res["SOFTPIN_Response"].FaceValue;
      var resResponseMessage = res["SOFTPIN_Response"].ResponseMessage;
      if(responseCode && resResponseMessage == "Success"){

        //Analytic Part
        this.transactionDetails.transactValue = this.facevalue;
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "success";

        this.gatewayService.setFlagForDashboardReload(true);
        this.events.publish('updateBalance', this.mobilenumber);
        var staticNumber=this.gatewayService.GetMobileNumber();
        var userInfo = this.dashboardService.GetUserInfo();
        var prodPromeName = userInfo.ListOfServices.Services.ProdPromName;
            prodPromeName = prodPromeName.toLowerCase();
       
          this.loader.dismiss();
          if (this.mobilenumber==this.loggedinNumber && this.LoginbillingType=="Billable" && prodPromeName.indexOf("cmp") == -1) {
          if(this.facevalue < this.dueBillAmount){
            var remaining= this.dueBillAmount-this.facevalue;
            this.showAlertToHandleNavigate(" You're Almost There!", "RM"+ this.facevalue+" partial payment has been made for "+ this.mobilenumber+". Additional RM"+remaining+" is required for full bill payment ", ["OK","Set up Auto Billing"]);
           
          }else{
            this.showAlertToHandleNavigate("Yay!", "RM"+ this.facevalue + " payment has been made for " + this.mobilenumber, ["OK","Set up Auto Billing"]);
          }
        
         
        }else{
          this.loader.dismiss(); 
          if(this.facevalue < this.dueBillAmount){
            var remaining= this.dueBillAmount-this.facevalue;
            this.showAlertOK(" You're Almost There!", "RM"+ this.facevalue+" partial payment has been made for "+ this.mobilenumber+". ", "OK");
           
          }else{
            this.showAlertOK("Yay!", "RM"+ this.facevalue + " payment has been made for " + this.mobilenumber, "OK");
          }
         
         
        }
   
       
      } else {
     
        this.loader.dismiss();

        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "failed";

        this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid PIN", "Please enter a valid 16-digit PIN", "OK");
      }
      // }else{
      //   this.loader.dismiss();
      //   this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
      // }
    }).catch(err => {
      this.loader.dismiss();

      //Analytic Part
      this.transactionDetails.jouneryDepth = "post";
      this.transactionDetails.transactResult = "failed";
      this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);

      console.log("Paybill -> postReload pin error " + JSON.stringify(err));
      if(err=="No internet connection")
      return;
      this.alertCtrlInstance.showAlert("Uh Oh. Something's Not Right", "Please try again later", "OK");

    });
  }
 
  // getTransactionDetails(loggedinNumber, facevalue) {

  //   this.loader.present();
  //   this.paybillService.fetchTransactionDetails(loggedinNumber, facevalue).then((res) => {
  //     console.log('paybill reload -> fetchTransactionDetails' + JSON.stringify(res));
  //     var isSuccessful = res["isPaymentSuccess"];
  //     this.loader.dismiss();
  //     if (isSuccessful == true) {
  //       this.gatewayService.setFlagForDashboardReload(true);
  //       this.events.publish('updateBalance', this.mobilenumber);
  //       var staticNumber=this.gatewayService.GetMobileNumber();
  //       if(this.mobilenumber==staticNumber){
  //         this.showAlertToHandleNavigate("Notification", facevalue + " has settled bill for " + this.mobilenumber, ["DONE", "SETUP AUTO-BILLING"]);
  //       }else{
  //         this.showAlertToHandleNavigate("Notification", facevalue + " has settled bill for " + this.mobilenumber, ["DONE"]);
  //       }
       
  //     } else {
  //       this.alertCtrlInstance.showAlert("Notification", "TRANSACTION FAILED !!!", "OK");
  //     }
  //   }).catch(err => {
  //     console.log("paybill reload -> Error " + JSON.stringify(err));
  //     this.loader.dismiss();
  //     this.alertCtrlInstance.showAlert("Transaction details", "No data found.", "OK");
  //   });
  // }

  showAlertToHandleNavigate(title, msg, btn) {
    if (Array.isArray(btn)) {

      try {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          buttons: [{
            text: btn[0],
            cssClass:'submit-button',
            handler: () => {
              console.log('update Paybill');
            }
          },
          {
            text: btn[1],
            cssClass:'submit-button',
            handler: () => {
            
              console.log('open autobilling');
              //this.paybillService.notifyObservers();
              this.events.publish('SetUpAutobilling');
            }
          }],
          cssClass: 'success-message',
          enableBackdropDismiss: false
        });
        alert.present();

      }
      catch (e) {
        console.log("Exception : Alert open ", e);
        if(e=="No internet connection")
        return;
      }
    }
    
  }
  showAlertOK(title, msg, btn){
    try {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: msg,
        buttons: [{
          text: btn,
          cssClass:'error-button-float-right submit-button',
          handler: () => {
            //update Paybill
            console.log('update Paybill');

          }
        }
       ],
        cssClass: 'success-message',
        enableBackdropDismiss: false
        // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
      });
      alert.present();

    }
    catch (e) {
      console.log("Exception : Alert open ", e);
      if (e == "No internet connection")
        return;
    }
  }
  handlePin(e){
    if(e.which === 13){
      this.keyboard.close();
    }
  }




}
