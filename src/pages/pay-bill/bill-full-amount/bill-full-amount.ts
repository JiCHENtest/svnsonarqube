import { Component, NgZone, Renderer2 } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { ChangeCreditcardModalPage } from "../../change-creditcard-modal/change-creditcard-modal";
import { GatewayService } from '../../../global/utilService';
import { AlertService } from '../../../global/alert-service';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import * as sha1 from "sha1";
import { DatePipe } from '@angular/common';
import { PayBillService } from '../payBillService';
import { Events } from 'ionic-angular';
import { AlertController, LoadingController } from 'ionic-angular';
import { PayBillPage } from '../pay-bill';
import { ReloadService } from '../../../global/reloadServices';
import { PaymentRecieptPage } from '../../payment-reciept/payment-reciept';
import { Keyboard } from '@ionic-native/keyboard';
import { DashboardService } from '../../../pages/home/homeService';
import { analyticTransactionDetailsInterface } from '../../../global/interface/analytic-transaction-details';





/**
 * Generated class for the BillFullAmountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-bill-full-amount',
  templateUrl: 'bill-full-amount.html',
})
export class BillFullAmountPage {
  indexValue: string;
  cardType: string;
  mobilenumber: any;
  loader: any;
  accountNumber: any;
  selectedCard: string;
  // paybillAmount:any;
  // @Input() 
  dueBillAmount: any;
  amountShow: any;
  nextDisable: boolean;
  Cardflag: any;
  loggedinNumber: any;

  arrayOfCards: any;
  isSaved: any;
  orderId: any;
  finalFlag: any;
  selectToken: any;
  isSavedCardUsed: any;
  payType: any;
  disable: any;
  cardsSaved: any;
  LoginbillingType: any;
  //staticNumber:any;
  //carArray = ["12345678912345123","0981237891234345","98545678912345666","2299447891234745"];
  cardArray: any;
  fullAmountDisable: any;
  transactionDetails: analyticTransactionDetailsInterface;

  paymentMethod = [
    {
      "type": "Credit/Debit Card",
      "icon": "custom-reload-card"
    },
    {
      "type": "Online Banking",
      "icon": "custom-reload-online"
    }

  ];

  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public events: Events,
    public navParams: NavParams, public modalCtrl: ModalController, public paybillService: PayBillService,
    private alertCtrlInstance: AlertService, public gatewayService: GatewayService,
    public theInAppBrowser: InAppBrowser, public datePipe: DatePipe, public alertCtrl: AlertController,
    public reloadService: ReloadService, private keyboard: Keyboard, public zone: NgZone,
    public dashboardService: DashboardService, private renderer: Renderer2) {
    //this.selectedCard = this.carArray[0];
    //this.disable = true;
    this.isSavedCardUsed = false;
    this.loggedinNumber = this.reloadService.gettingLoginNumber();
    this.loader = this.gatewayService.LoaderService();
    this.amountShow = 0;

    var _that = this;
    events.subscribe('Money', (dueAmount) => {

      // user and time are the same arguments passed in `events.publish(user, time)`
      _that.dueBillAmount = dueAmount;
      console.log('Welcome::' + dueAmount);

      // if (_that.dueBillAmount.substring(0, 1) == "_") {

      //   _that.amountShow = _that.dueBillAmount;
      // } else {

      //   _that.amountShow = "RM" + _that.dueBillAmount;
      // }
      _that.amountShow = _that.dueBillAmount;

    });
    //this.dueBillAmount=  this.paybillService.GetDueBillAmount();
    //console.log("entered full amount page with bill due amount= "+this.dueBillAmount);

    events.subscribe('accountNumber', (accountNo) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('account number in full amount is::' + accountNo);
      _that.accountNumber = accountNo;
    });

    console.log("Account number::" + _that.accountNumber);

  }
  ngOnDestroy() {
    this.events.unsubscribe('Money');
    this.events.unsubscribe('accountNumber');
  }

  ionViewDidEnter() {
    this.initiateAnalyticTransactionJourney();
  }

  initiateAnalyticTransactionJourney() {
    this.transactionDetails = {};
    this.gatewayService.resetAllAnalyticData();
    this.transactionDetails.jouneryDepth = "initiation";
    this.transactionDetails.transactProduct = "Full amount";
    this.gatewayService.setAnalyticTransactionJourney("Full amount", this.renderer, this.transactionDetails);
  }


  ionViewDidLoad() {
    this.fullAmountDisable = true;
    this.paybillService.SetTabInfo(1);
    console.log('ionViewDidLoad BillFullAmountPage');
    this.LoginbillingType = this.gatewayService.GetBillingType();
    this.nextDisable = true;

    this.events.subscribe('savedCards', (savedCards) => {

      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('savedCards in full amount is::' + JSON.stringify(savedCards));
      this.cardsSaved = savedCards;
      // this.getSavedCards();
      this.savedCards(this.cardsSaved);

    });


  }

  savedCards(res) {

    if (res["savedCards"] == undefined) {

      this.Cardflag = false;
      //this.disable = false;
    }
    else if (res["savedCards"].length >= 1) {



      this.isSavedCardUsed = true;
      //this.disable = true;
      this.Cardflag = true;
      //this.nextDisable = false;
      this.cardArray = [];
      var allcards = res["savedCards"];
      allcards.forEach(element => {
        this.selectedCard = res["savedCards"][0].masked;
        this.selectToken = res["savedCards"][0].TOKEN;
        this.cardArray.push(element);
        console.log("*****saveCards*******" + JSON.stringify(element));
        // if (element.is_default == true) {
        //   this.selectedCard = element.masked;
        //   this.selectToken = element.TOKEN;
        // }

      });
      this.addCss(0);
    } else {

      // this.disable = false;
      this.Cardflag = false;
    }



  }
  cardShow(selectedCard) {
    var cardShow = selectedCard.replace(/.(?=.{4})/g, '*');
    return cardShow;
  }
  addCss(index) {
    this.zone.run(() => {
      console.log("came into addCss function and index is" + index);
      this.cardType = "";

      this.cardType = this.paymentMethod[index].type;
      this.indexValue = index;
      console.log("came into addCss function and indexValue is" + this.indexValue);

      if (this.dueBillAmount == undefined || this.dueBillAmount == 0.00 || this.dueBillAmount.substring(0, 1) == "-") {
        this.nextDisable = true;
      } else {
        this.nextDisable = false;
      }

    })
  }

  goToPay() {

    //Analytic Part
    this.initiateAnalyticTransactionJourney();

    if (this.cardType) {

      var paymentType = "";
      if (this.cardType == "Credit/Debit Card") {
        paymentType = "1";
      }
      else
        paymentType = "7";
      this.paybillService.setFullAmountPaid(true);
      this.openUrlPostpaid(this.dueBillAmount, paymentType);

    } else if (this.isSavedCardUsed) {
      this.paybillService.setFullAmountPaid(false);
      this.openUrlPostpaid(this.dueBillAmount, paymentType);
    }
    else {
      this.alertCtrlInstance.showAlert("Message", "Please select Payment type and Amount", "OK");
    }

  }


  presentingChangeCreditModal() {
    let getCardArrayCard = this.cardArray;
    let selectedCard = this.selectedCard;

    let modal = this.modalCtrl.create(ChangeCreditcardModalPage, { selectedCard, getCardArrayCard });
    modal.present();

    modal.onWillDismiss(data => {
      console.log(data);
      if (data != null) {

        if (data["masked"]) {
          this.cardType = "";
          if (this.dueBillAmount == undefined || this.dueBillAmount == 0.00 || this.dueBillAmount.substring(0, 1) == "-") {
            // this.disable = true;
            this.nextDisable = true;
            this.isSavedCardUsed = true;
            this.selectedCard = data.masked;
            this.selectToken = data.TOKEN;

          } else {
            // this.disable = true;
            this.nextDisable = false;
            this.isSavedCardUsed = true;
            this.selectedCard = data.masked;
            this.selectToken = data.TOKEN;
          }

        } else {
          // this.disable = false;
          this.isSavedCardUsed = false;


          this.selectToken = "";
          this.selectedCard = "Use new card";
          this.addCss(0);
        }


      }
    })
  }


  openUrlPostpaid(value, type) {
    this.loader = this.gatewayService.LoaderService();

    //call payment adapter
    // "1" for carid card 
    //"7"  onlineback

    this.mobilenumber = this.paybillService.GetSelectMobileNumber();

    var _that = this;
    var date = this.datePipe.transform(new Date(), 'yyyyMMddHHmmssZ');
    var amount = parseFloat(value).toFixed(2);
    var accountNum = this.accountNumber;
    var recepient = this.mobilenumber;
    if (this.isSavedCardUsed == true) {
      this.payType = 4;
      if (type == 7) {
        this.payType = type;
      }
    } else {
      this.payType = type;
    }

    var params = { "postpaidAccountNum": accountNum, "notifyRecipient": recepient , "paymentMethod": this.payType, "totalAmount": amount, "loginID": this.loggedinNumber, "isInternetReload": false };
    this.loader.present();
    console.log("*****Payment params in full amount*****" + JSON.stringify(params));

    this.paybillService.PostpaidPaymentGateway(params).then((res) => {

      //Analytic Part Start
      this.transactionDetails.jouneryDepth = "checkout";
      this.transactionDetails.transactValue = amount;
      if (this.cardType == "Credit/Debit Card") {
        this.transactionDetails.transactMethod = "Credit card";
      } else {
        this.transactionDetails.transactMethod = "Online banking";
      }
      //Analytic Part End

      //this.Cardflag = true;
      console.log("*********** component success PostpaidPaymentURL" + JSON.stringify(res));
      this.loader.dismiss();
      var payment_url = "";
      if (this.Cardflag) {
        payment_url = res["paymentURL"] + '&prePaymentToken=' + this.selectToken;
      } else {
        payment_url = res["paymentURL"];
      }
      console.log("*********** component success payment_url" + JSON.stringify(payment_url));

      var resUrl = res["responseUrl"];
      this.orderId = res["orderId"];
      var msgStatus = res["msgStatus"];
      this.paybillService.setOrderId(this.orderId);

      console.log(payment_url);

      //selectToken

      if (msgStatus == "Success" || payment_url != undefined || payment_url != null || payment_url != "") {
        const browser = _that.theInAppBrowser.create(payment_url, '_blank', 'location=no');
        browser.show();

        browser.on("exit").subscribe((e) => {
          console.log("exit" + e.url);
          //this.checkingKadCeriaBonus();
          this.getTransactionDetails();

          //this.alertCtrlInstance.showAlert("Notification", this.dueBillAmount + " has been settled bill for " + this.mobilenumber, "OK");
        }, err => {
          console.log("error" + err);
        });

        browser.on("loadstop").subscribe((e) => {

          console.log("loadstop" + e.url);
          if (e.url.startsWith(resUrl)) {
            browser.close();
          } else {

          }
        }, err => {
          console.log("error" + err);
        });

        browser.on("loaderror").subscribe((e) => {
          browser.close();
          console.log("loaderror" + e.url);
        }, err => {
          console.log("error" + err);
        });

      } else {
        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "failed";
        this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");
      }


      //Analytic Part
      this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);
      //this.navCtrl.setRoot(PayBillPage);
    })
      .catch(err => {
        this.loader.dismiss();

        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "failed";
        this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);

        console.log("*************** component failure PostpaidPaymentURL" + err);
        if (err == "No internet connection")
          return;
        this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");

      });

  }

  DataAlignment(gettingDate) {
    var day = gettingDate.slice(0, 8);
    var year = day.match(/.{1,4}/g);
    var date = year[1].match(/.{1,2}/g);
    var finalDate = date[1] + '/' + date[0] + '/' + year[0];
    return finalDate;
  }

  getTransactionDetails() {
    // alert("Gettting "+selectedNumber);
    this.loader.present();
    this.paybillService.fetchTransactionDetails(this.loggedinNumber, this.dueBillAmount).then((res) => {

      console.log('fetchTransactionDetails ******' + JSON.stringify(res));
      var isSuccessful = res["isPaymentSuccess"];
      var transactionDate = res["resultSet"][0].TRANSDATE;
      if (transactionDate != null || transactionDate != "") {
        transactionDate = this.DataAlignment(transactionDate);
      }

      var transactionId = res["resultSet"][0].REFERID;
      this.paybillService.setIsPaybill(true);

      this.loader.dismiss();
      if (isSuccessful == true) {

        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "success";

        // this.navCtrl.setRoot(PaymentRecieptPage);
        this.paybillService.SetTransactionDate(transactionDate);
        this.paybillService.SetTransactionId(transactionId);


        this.modal();


      } else {

        //Analytic Part
        this.transactionDetails.jouneryDepth = "post";
        this.transactionDetails.transactResult = "failed";

        // this.navCtrl.setRoot(PaymentRecieptPage);
        // this.events.publish('updateNumber', this.mobilenumber, this.accountNumber);
        if (res["resultSet"][0].REASONCODE == "51011") {
          this.alertCtrlInstance.showAlert("Uh Oh. That's a Wrong CVC Number", "Please enter a valid 3-digit CVC number !!!", "OK");
        } else if (res["resultSet"][0].REASONCODE == "51012") {
          this.alertCtrlInstance.showAlert("Uh Oh. Got Another Credit Card?", "We prefer VISA or Mastercard.", "OK");
        } else if (res["resultSet"][0].REASONCODE == "51013") {
          this.alertCtrlInstance.showAlert("Uh Oh. Wrong Expiry Date?", "Please enter a valid expiry date", "OK");
        } else if (res["resultSet"][0].REASONCODE == "30000" || res["resultSet"][0].REASONCODE == "32001" || res["resultSet"][0].REASONCODE == "33001" || res["resultSet"][0].REASONCODE == "37036 ") {
          this.alertCtrlInstance.showAlert("Hang On. We're Working on Your Transaction", "Successful transaction will be reflected in transaction history", "OK");
        } else if (res["resultSet"][0].REASONCODE == "40000") {
          this.alertCtrlInstance.showAlert("Uh oh. There's a Problem With Your Card ", "Please try again later", "OK");
        }
        else {
          this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");
        }
      }

      //Analytic Part
      this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);

    }).catch(err => {
      console.log("***paybill*******" + JSON.stringify(err));
      this.loader.dismiss();

      //Analytic Part
      this.transactionDetails.jouneryDepth = "post";
      this.transactionDetails.transactResult = "failed";
      this.gatewayService.updateAnalyticTransactionJourney(this.renderer, this.transactionDetails);

      if (err == "No internet connection")
        return;
      this.alertCtrlInstance.showAlert("Uh Oh. Payment System is Busy", "Please try again later", "OK");
      //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }
  modal() {
    let modal = this.modalCtrl.create(PaymentRecieptPage);
    modal.present();
    modal.onDidDismiss(data => {
      console.log("inside ondiddismissss");
      this.gatewayService.setFlagForDashboardReload(true);
      this.events.publish('updateNumber', this.mobilenumber, this.accountNumber);
      this.savedCards(this.cardsSaved);

      // if (this.Cardflag) {
      //   this.nextDisable = false;
      //   //this.disable = true;
      // } else this.nextDisable = true;

      var dropDownNumner = this.gatewayService.GetMobileNumber();
      var userInfo = this.dashboardService.GetUserInfo();
      var prodPromeName = userInfo.ListOfServices.Services.ProdPromName;

      prodPromeName = prodPromeName.toLowerCase();


      if (this.mobilenumber == this.loggedinNumber && this.LoginbillingType == "Billable" && prodPromeName.indexOf("cmp") == -1) {

        this.showAlertToHandleNavigate("Yay!", this.dueBillAmount + " payment has been made for " + this.mobilenumber, ["OK", "Set up Auto Billing"]);
      } else {

        this.showAlertOK("Yay!", this.dueBillAmount + " payment has been made for " + this.mobilenumber, "OK");
      }


    });


  }

  showAlertToHandleNavigate(title, msg, btn) {
    if (Array.isArray(btn)) {

      try {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          buttons: [{
            text: btn[0],
            cssClass: 'submit-button',
            handler: () => {
              //update Paybill
              console.log('update Paybill');
            }
          },
          {
            text: btn[1],
            cssClass: 'submit-button',
            handler: () => {
              //open autobilling
              console.log('open autobilling');
              //  this.paybillService.notifyObservers();

              this.events.publish('SetUpAutobilling');
            }
          }],
          cssClass: 'success-message',
          enableBackdropDismiss: false
          // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
        });
        alert.present();

      }
      catch (e) {
        console.log("Exception : Alert open ", e);
        if (e == "No internet connection")
          return;
      }
    }
  }

  showAlertOK(title, msg, btn) {
    try {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: msg,
        buttons: [{
          text: btn,
          cssClass: 'error-button-float-right submit-button',
          handler: () => {
            //update Paybill
            console.log('update Paybill');

          }
        }
        ],
        cssClass: 'success-message',
        enableBackdropDismiss: false
        // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
      });
      alert.present();

    }
    catch (e) {
      console.log("Exception : Alert open ", e);
      if (e == "No internet connection")
        return;
    }
  }
  handlePin(e) {
    if (e.which === 13) {
      this.keyboard.close();
    }
  }
}
