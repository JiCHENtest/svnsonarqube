import { Component, Renderer2, ElementRef, ViewChild,NgZone } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { GbSharePage } from '../gb-share/gb-share';
import { GatewayService } from '../../global/utilService';
import { AlertService } from '../../global/alert-service';
import { DashboardService } from '../home/homeService';
import { GbShareService } from '../../global/gbshareServices';
import { AlertController } from 'ionic-angular';
import { DatePipe } from '@angular/common';
import { SubscriptionsPage } from '../subscriptions/subscriptions';
import { GlobalVars } from "../../providers/globalVars";
import {SubscriptionsTabService } from '../../pages/subscriptions/subscriptionsTabService';
import { Events } from "ionic-angular";
/**
 * Generated class for the GbShareAllocatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gb-share-allocate',
  templateUrl: 'gb-share-allocate.html',
})
export class GbShareAllocatePage {

  @ViewChild('overlay') overlay;
  sliderModel1: any = {};
  sliderModel2: any = {};
  sliderModel3: any = {};
  changeFlag: boolean = false;
  cancelFlag: boolean = false;
  selctedElement: any;
  gettingValue:number = 5;

  loader:any;
  unitAvailable:number;
  totalInitialAmount:any;
  expairyDateTime:any;
  minValue: number;
  maxValue: number;
  individualUsage;
  count :number = 0;
  billingAccount:any;
  accountID:any;
  groupID:any;
  AllocatedPage:any;
  AvailablelityGB:number = 0;
  finalValue:number;
  addedSubline:any;
  FetchNumbers:any;
  ArrayObject:any;
  sliders:any;
  AlertFlag:boolean=false;
  savedGB:any;
  subLineNumber:any;
  disableValue:boolean=true;
  callBar:boolean=false;
  GB:string="GB";
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, 
    public renderer2: Renderer2, public element: ElementRef,private alertCtrlInstance: AlertService, 
    public gatewayService:GatewayService,public dashboardService: DashboardService, 
    public gbshareService :GbShareService,public alertCtrl: AlertController, public datePipe: DatePipe, 
    public globalVar: GlobalVars,public subscriptionsTabService:SubscriptionsTabService, 
    public events: Events,public zone:NgZone) {

    this.loader = this.gatewayService.LoaderService();
     this.AllSublines();
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("GB share allocate", this.renderer2);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GbShareAllocatePage');
    console.log('ionViewDidLoad GbShareAllocatePage');
    console.log('ionViewDidLoad GbSharePage');
    //var params 
    this.callBar = this.globalVar.getcallBarredStatus();
    //this.callBar = true
   
    
  }
  getSavedGBData(){
    var _that = this;
    var userInfo = _that.dashboardService.GetUserInfo();
    console.log("*****userInfo Allocation*******"+JSON.stringify(userInfo));
    this.groupID = userInfo.ListOfServices.Services.SmeGroupId;
   // this.groupID = "665811549";
    
    this.accountID = userInfo.CustomerRowId;
    this.billingAccount = userInfo.ListOfServices.Services.AssetBillingAccountRowId;
   //this.accountID = "1-VI3NG9";
   //this.billingAccount = "1-VI4I4E";
    console.log("*****accountID accountID*******"+JSON.stringify(this.billingAccount));
    console.log("*****billingAccount billingAccount*******"+JSON.stringify(this.billingAccount));

    var params = {
      "GroupID":this.groupID //this.groupID //665811549:0 //014831735:1
    }  
   // this.loader = this.gatewayService.LoaderService();
    //this.loader.present();
    this.gbshareService.GroupSharing(params).then((res)=> {
      this.loader.dismiss();   
      console.log("*****GroupSharing*******"+JSON.stringify(res));
      this.individualUsage = [];
    if(res["Envelope"].Body.processResponse.ResponseHeader.Description == "Operation successfully."){
      var FreeUnitItems = res["Envelope"].Body.processResponse.QueryFreeUnitResult.FreeUnitItem;

      this.AllocatedPage = [];
      this.addedSubline = [];
      this.ArrayObject = [];
      this.AvailablelityGB = 0;
      this.finalValue = 0;

      if(FreeUnitItems.length > 0){
  
        FreeUnitItems.forEach((element,index) => {
          if(element.FreeUnitTypeName == "C_MI_Data_Monthly"){
            this.totalInitialAmount = element.TotalInitialAmount
            this.unitAvailable =  element.TotalUnusedAmount
            this.expairyDateTime = element.FreeUnitItemDetail.ExpireTime  
            this.individualUsage = element.MemberUsageList;
          }  
         });
      }else{
        this.totalInitialAmount = FreeUnitItems.TotalInitialAmount
        this.unitAvailable =  FreeUnitItems.TotalUnusedAmount
        this.expairyDateTime = FreeUnitItems.FreeUnitItemDetail.ExpireTime;
        this.individualUsage = FreeUnitItems.MemberUsageList;
      }













   
    
     if(this.individualUsage == undefined){
        this.finalValue = this.unitAvailable;
        var params = {
          "value":this.finalValue,
          "min":0,//this.groupID
          "max":this.totalInitialAmount,  //665811549 //014831735//3775080302
         }
         this.events.publish('updatePool',params);
        this.FetchNumbers.forEach((element,index) => {
          this.AllocatedPage.push(0);
          this.addedSubline.push(this.FetchNumbers[index]);
          var sharedVar="0";
           let lg = {
           UsedAmount: "0",
           SubscriberKey:"0",
           ShareLimit:sharedVar,
           PrimaryIdentity:this.addedSubline[index]
         };
          this.ArrayObject.push(lg);
         console.log("*****ArrayObject*******" + JSON.stringify(this.ArrayObject));
         });
     }else if(this.individualUsage.PrimaryIdentity){
     // this.finalValue = FreeUnitItems.TotalUnusedAmount;
     this.finalValue = parseInt(this.totalInitialAmount) - this.individualUsage.ShareLimit;
   
      let lg = {}
              this.FetchNumbers.forEach((element,index) => {
                
                  if("0"+this.individualUsage.PrimaryIdentity == this.FetchNumbers[index]){
                    this.AllocatedPage.push(this.individualUsage.ShareLimit);
                    this.addedSubline.push(this.individualUsage.PrimaryIdentity);
                    var sharedVar="0";
                    let lg = {
                    UsedAmount: "0",
                    SubscriberKey:"0",
                    ShareLimit:this.individualUsage.ShareLimit,
                    PrimaryIdentity:this.individualUsage.PrimaryIdentity
                  };
                  this.ArrayObject.push(lg);
                  }else{
                    this.AllocatedPage.push(0);
                    
                      this.addedSubline.push(this.FetchNumbers[index]);
                      var sharedVar="0";
                       let lg = {
                       UsedAmount: "0",
                       SubscriberKey:"0",
                       ShareLimit:sharedVar,
                       PrimaryIdentity:this.addedSubline[index]
                     };
                     this.ArrayObject.push(lg);
                  }


                
             
               console.log("*****ArrayObject*******" + JSON.stringify(this.ArrayObject));
               });
               var params = {
                "value":this.finalValue,
                "min":0,//this.groupID
                "max":this.totalInitialAmount,  //665811549 //014831735//3775080302
               }
               this.events.publish('updatePool',params);

               if(this.AlertFlag && this.savedGB && this.subLineNumber){
                this.alertCtrlInstance.showAlert("Somebody's Gonna be Happy", ""+this.savedGB+"GB allocated to "+this.subLineNumber+"", "OK");   
               }
                 
    }else{
      this.individualUsage.forEach((element,index) => {
        this.AllocatedPage.push(element.ShareLimit);
        this.AvailablelityGB += parseInt(element.ShareLimit);
        this.addedSubline.push("0"+element.PrimaryIdentity);
        console.log("*****Shared*******" + JSON.stringify(this.individualUsage));
      });
      this.FetchNumbers.forEach((element,index) => {
       var checkDoublication =  this.addedSubline.includes(this.FetchNumbers[index]);
       if(!checkDoublication){
        this.addedSubline.push(this.FetchNumbers[index]);
       }

     
        var sharedVar = this.AllocatedPage[index];
        if(!sharedVar){
          sharedVar = "0";
          this.AllocatedPage[index] = "0";
        }
      let lg = {
        UsedAmount: "0",
        SubscriberKey:"0",
        ShareLimit:sharedVar,
        PrimaryIdentity:this.addedSubline[index]
      };
       this.ArrayObject.push(lg);
      console.log("*****ArrayObject*******" + JSON.stringify(this.ArrayObject));
      });
      this.finalValue = parseInt(this.totalInitialAmount) - this.AvailablelityGB
      var params = {
        "value":this.finalValue,
        "min":0,//this.groupID
        "max":this.totalInitialAmount,  //665811549 //014831735//3775080302
       }
      this.events.publish('updatePool',params);
      if(this.AlertFlag && this.savedGB && this.subLineNumber){
        this.alertCtrlInstance.showAlert("Somebody's Gonna be Happy", ""+this.savedGB+"GB allocated to "+this.subLineNumber+"", "OK");   
       }
     }  
       
    }else{
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
    }
    }).catch(err => {
      this.loader.dismiss();  
      console.log("*****reload*******"+JSON.stringify(err));
      if(err=="No internet connection")
      return;
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }
  numberMethod(number){
    return Number(number);
  }
  goToRoot() {
    this.navCtrl.popToRoot();
  }
  AllSublines(){
    var ServiceNumber = this.gatewayService.GetMobileNumber();
    var params = {
      "MobileNumber": ServiceNumber
    }  

if(!this.AlertFlag){
  this.loader = this.gatewayService.LoaderService();
    this.loader.present();
  
}
    this.gbshareService.GetSubLines(params).then((res)=> {
     // this.loader.dismiss();   
          console.log("*****GetSubLines*******"+JSON.stringify(res));
          this.FetchNumbers = [];
          var savedArray = res["Envelope"].Body.SMEGroupDetailsOutput.SMEGroupDetailsResponse.ResponseBody.ListOfMemberDetails;
        
          if(savedArray.SERIAL_NUMBER){
            this.FetchNumbers.push(savedArray.SERIAL_NUMBER);
          }else{
            savedArray.forEach(element => {
              this.FetchNumbers.push(element.SERIAL_NUMBER);
            })
          }

          
        
          
          this.getSavedGBData();
    }).catch(err => {
          this.loader.dismiss();  
          console.log("*****reload*******"+JSON.stringify(err));
          if(err=="No internet connection")
          return;
          this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }
  gotoGroupShare(){

    //var date = this.datePipe.transform(new Date(), 'yyyyMMddHHmmss');
    var params = {
      "GroupID": this.groupID //665811549 //014831735
    }  


    this.loader = this.gatewayService.LoaderService();
    this.loader.present();
    this.gbshareService.GoingToGroup(params).then((res)=> {
      this.loader.dismiss();   
      console.log("*****GoingToGroup*******"+JSON.stringify(res));
      if(res["Envelope"].Body.PurchaseProductCBSResponse.Response.ErrorMessage == "Operation successfully."){
        this.navCtrl.setRoot(GbSharePage);
      }else{
        this.alertCtrlInstance.showAlert("Uh Oh. GBshare is Busy","System is unable to toggle between group and allocated sharing", "OK");
      }
      //this.showAlertToHandleNavigate("Success Title","Success Dec", ["CTA"]);
    }).catch(err => {
      this.loader.dismiss();  
      console.log("*****reload*******"+JSON.stringify(err));
      if(err=="No internet connection")
      return;
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }
  pushPage() {
    //this.navCtrl.setRoot(GbSharePage);

    this.showAlertToHandleNavigate("Sharing is Caring","You are switching to group sharing. You and your GB Share lines will enjoy data from a shared pool.", ["Switch","Cancel"]);
  


  }
  Redirection(){
    var date = this.datePipe.transform(new Date(), 'yyyyMMdd_HHmmss');
    //date = date.substring(0,8)+'_'+date.substring(8,16);
        

    var ServiceNumber = this.gatewayService.GetMobileNumber();
     var _that = this;
    var userInfo = _that.dashboardService.GetUserInfo();
    this.accountID = userInfo.CustomerRowId;
    this.billingAccount = userInfo.ListOfServices.Services.AssetBillingAccountRowId;
    console.log("*****GoingToGroup*******"+JSON.stringify(userInfo));
    //ServiceNumber = "0196868767";
    var params = {
      "time":date,
      "GroupID":this.groupID,//this.groupID
      "AccountID":this.accountID,  //665811549 //014831735//3775080302
      "ServiceNumber":ServiceNumber,
      "BillingAccountID":this.billingAccount
    }  
    this.loader = this.gatewayService.LoaderService();
    this.loader.present();
    this.gbshareService.DeletedGBShare(params).then((res)=> {
      this.loader.dismiss();   
      if(res["isSuccessful"]==true){
        this.gotoGroupShare();
      }else{
        this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");  
      }
      console.log("*****GoingToGroup*******"+JSON.stringify(res));
     // this.showAlertToHandleNavigate("Success Title","Success Dec", ["CTA"]);
    }).catch(err => {
      this.loader.dismiss();  
      console.log("*****reload*******"+JSON.stringify(err));
      if(err=="No internet connection")
      return;
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
    
  }
  save(slider: any,index) {

   this.savedGB = slider.ShareLimit - this.AllocatedPage[index];
   this.subLineNumber = this.addedSubline[index]

if(this.finalValue >= this.savedGB){
    console.log("Saved"+index);
    console.log("savedGB"+this.savedGB);
    slider.savedValue = slider.ShareLimit;
    this.AllocatedPage[index] = slider.ShareLimit;
    this.changeFlag = false;
    this.renderer2.removeClass(this.overlay.nativeElement, 'gb-overlay');
    this.renderer2.removeClass(this.selctedElement, 'gb-selected');
    this.allocationGBServerCall(this.savedGB,this.subLineNumber);
    }else{
        this.alertCtrlInstance.showAlert("Notification", "you can't add more than Avaliable.", "OK");
    }
  }
  allocationGBServerCall(savedGB,subLineNumber){
    var date = this.datePipe.transform(new Date(), 'yyyyMMddHHmmss');
    var ServiceNumber = this.gatewayService.GetMobileNumber();
    //['{"GroupID":"665811549","AccountID":"1-VI3NG9","MobileNumber":"0196472007","GroupID":"665811549","allocatedGB":"4.00","ServiceNumber":"0196868767","BillingAccountID":"1-VI4I4E"}']
  //  ServiceNumber = "0196868767";
   // subLineNumber = "0196472007";
    var params = {
      "GroupID":this.groupID,//this.groupID
      "AccountID":this.accountID,  //665811549 //014831735//3775080302
      "MobileNumber":subLineNumber,
      "allocatedGB":savedGB+'.00',
      "ServiceNumber":ServiceNumber,
      "BillingAccountID":this.billingAccount
    }  

    
    this.loader = this.gatewayService.LoaderService();
    this.loader.present();
    this.gbshareService.AllocatedGBShare(params).then((res)=> {
      this.loader.dismiss();   
      if(res["Envelope"].Body.SRCreate_Output.ListOfCelservicerequestthinout.CelServiceRequestThin.Status == "Closed"){
       //
       this.AlertFlag = true;
      // this.alertCtrlInstance.showAlert("Somebody's Gonna be Happy", ""+savedGB+" allocated to "+subLineNumber+"", "OK");  
        this.AllSublines();
      }else{
        this.AlertFlag = false;
        this.AllSublines();
        this.alertCtrlInstance.showAlert("Uh Oh. GBshare is Busy","System is unable to allocate data to the users", "OK");
      //  var chooseValue = this.addedSubline.indexOf(subLineNumber);
        
        // this.cancel(this.ArrayObject[chooseValue],chooseValue)
      }
  
      console.log("*****GoingToGroup*******"+JSON.stringify(res));
     // this.showAlertToHandleNavigate("Success Title","Success Dec", ["CTA"]);
    }).catch(err => {
      this.loader.dismiss();  
      console.log("*****reload*******"+JSON.stringify(err));
      if(err=="No internet connection")
      return;
      this.AlertFlag = false;
      this.AllSublines();
     
     
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
    
  }
  cancel(slider: any,index) {
    console.log("Cancel Saved"+index);
    slider.ShareLimit =  this.AllocatedPage[index];
    this.cancelFlag = true;
    this.changeFlag = false;
    this.renderer2.removeClass(this.overlay.nativeElement, 'gb-overlay');
    this.renderer2.removeClass(this.selctedElement, 'gb-selected');
  }

  

  onClick(slider: any) {
    if (!this.changeFlag && !this.cancelFlag) {
      console.log(slider.PrimaryIdentity);
      console.log(this.element.nativeElement);
      this.selctedElement = this.element.nativeElement.querySelector("#a" + slider.PrimaryIdentity);
      this.renderer2.addClass(this.overlay.nativeElement, 'gb-overlay');
      this.renderer2.addClass(this.selctedElement, 'gb-selected');
      // var thisParent = el._elementRef.nativeElement.offsetParent.offsetParent.childNodes[7].childNodes[3].childNodes[3];
      // console.log(thisParent)
      // this.renderer2.removeClass(thisParent, 'halsbalas');
      // // _elementRef.nativeElement
      console.log('halsbala');

      this.changeFlag = true;
    }
    this.cancelFlag = false;
  }
  showAlertToHandleNavigate(title,  msg,  btn) {
    
    if (Array.isArray(btn)) {
      
            try {
              let alert = this.alertCtrl.create({
                title: title,
                subTitle: msg,
                buttons: [{
                  text: btn[0],
                  cssClass:'submit-button',
                  handler: () => {
                    // console.log('open autobilling');
                    // if(title == "Success Title"){
                    //   this.navCtrl.setRoot(GbShareAllocatePage);
                    // }else if (title == "Error Title"){
                    //   this.count = this.count +1;
                    // }else{
                    //   this.count = 0
                    // }
                    this.Redirection();
                  
                  }
                },
                {
                  text: btn[1],
                  cssClass:'submit-button',
                  handler: () => {
                     console.log('Second Button');
                     //this.count = this.count +1;
                     //this.navCtrl.setRoot(GbSharePage);
                  }
                }],
                cssClass:  'success-message'
                // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
              });
              alert.present();
      
            }
            catch (e) {
              console.log("Exception : Alert open ", e);
            }
          }
  }
  interNetAddOn(){
    this.subscriptionsTabService.SetTabInfo(1);
    this.navCtrl.push(SubscriptionsPage);
  }
  isCapZone(){
    return this.globalVar.getcallBarredStatus();
   //return true;
  }
}
