import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GlobalVars } from "../../providers/globalVars";
import { CreditReloadPage } from "./credit-reload/credit-reload";
import { InternetPlansPage } from "./internet-plans/internet-plans";
import { ReloadCodePage } from "./reload-code/reload-code"
import { SuperTabsController } from '../../components/custom-ionic-tabs';
import { PopoverController } from 'ionic-angular';
import { ReloadPopoverComponent } from '../../components/reload-popover/reload-popover';
import { GatewayService } from '../../global/utilService';
import { ReloadService } from '../../global/reloadServices';
import { AlertService } from '../../global/alert-service';
import {ProductDetailsDesignGuidePage} from "../product-details-design-guide/product-details-design-guide";
import { Events } from "ionic-angular";
import { SubscriptionsPage } from '../subscriptions/subscriptions';
import { DashboardService } from '../../pages/home/homeService';


/**
 * Generated class for the ReloadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reload',
  templateUrl: 'reload.html',
})
export class ReloadPage {
  tab1Page: any = CreditReloadPage;
  tab2Page: any = InternetPlansPage;
  tab3Page: any = ReloadCodePage;
  selectOptions: any;
  selected : string ='';
  tested:string[]=[];
  allNumber;
  otherNumbers;
  lineValidity;
  creditBlance;
  balanceArray;
  loader:any;
  Ecem:any;
  pageName:string = "reloadPage";
  RegisterFlag:any;
  CheckECEM:any;
  prepaidCurrentBalance:any;
  count:number;
  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVar: GlobalVars,
    private tabsCtrl: SuperTabsController, public popoverCtrl: PopoverController, 
    private alertCtrlInstance: AlertService,public gatewayService:GatewayService, 
    public reloadService:ReloadService,public events:Events,public dashboardService:DashboardService) {
    this.tabsCtrl.enableTabsSwipe(false,'internetTab');
    this.Ecem = "ECEMB";
    this.RegisterFlag = "";
    
    //this.tested = ["123","123243","13324"];
  
    this.selectOptions = {
      title: 'Phone Numbers',
      mode: 'ios',
      cssClass: this.globalVar.getCurrentTheme() + " mypopup"
    };
    events.subscribe('updateBalance',(mobileNumber)=>{
      if(mobileNumber){
        this.getBalanceValidity(mobileNumber);
      }
    });
    events.subscribe('openECEMNumber',()=>{
        this.callingMethod();
    });
    //openECEMNumber
    events.subscribe('CheckECEM',(status)=>{
      this.CheckECEM = status;
    });
  //CheckECEM
  events.subscribe('selectedMsisdn', (addedNum)=>{
    this.selected=addedNum;
   console.log("New added number in reload is:"+ this.selected);
    this.reloadService.SetSelectMobileNumber(this.selected);
    this.loader = this.gatewayService.LoaderService();
    if(this.reloadService.getshowingHidenLoader() == 0){
     this.loader.present();
     this.reloadService.showingHidenLoader(true);
   }
    this.events.publish('CheckKawKaw',this.selected);
    this.getBalanceValidity(this.selected);
    //alert('gowa l reload' +this.selected);

  });
  }
  
  ngOnDestroy() {
    //this.events.unsubscribe('callTransferAmount');
    console.log('ng destroy unsubscribe all events here');
    this.events.unsubscribe('updateBalance');
    this.events.unsubscribe('openECEMNumber');
    } 
  callingMethod(){
    if(this.CheckECEM == "ECEMEligiblity" ){
      if(this.count == 0){
        ++this.count;
        this.navCtrl.push(ProductDetailsDesignGuidePage,{
          param1:this.RegisterFlag,param2:this.Ecem,param3:this.pageName
        });
        
      }
    }else{
      if(this.count == 0){
        ++this.count;
        this.navCtrl.push(SubscriptionsPage);
      }
    }
  }

  updateDial(){
    
  }
  ionViewWillEnter(){
    this.count = 0;
  }
  ionViewDidLoad() {
    
    console.log('ionViewDidLoad ReloadPage');
    this.tabsCtrl.enableTabsSwipe(false, 'internetTab');
    this.pageName = "reloadPage"
    this.loader = this.gatewayService.LoaderService();
   if(this.reloadService.getshowingHidenLoader() == 0){
    this.loader.present();
    this.reloadService.showingHidenLoader(true);
   }
   var userInfo = this.dashboardService.GetUserInfo();
   var adminNumber = userInfo["ListOfServices"].Services.BillingType;
   if (adminNumber == "Billable") {
   
    this.gatewayService.FeachAllSubMObileNumber().then((res)=> { 
     
      var getService = res["outputCPResp"].services
      console.log("***OK***");
       this.tested = [];
       this.allNumber = [];
       this.otherNumbers = [];
       if(res["outputCPResp"].services.length==undefined && res["outputCPResp"].services.mobileNumber && res["outputCPResp"].services.pre_Pos_Indicator == "Prepaid"){
          //    this.loader.dismiss();  
           this.allNumber.push(res["outputCPResp"].services.mobileNumber);
           this.otherNumbers.push(res["outputCPResp"].services.mobileNumber);
           this.selected =  this.gatewayService.GetMobileNumber();
           this.events.publish('CheckKawKaw',this.selected);
           this.reloadService.SetSelectMobileNumber(this.selected);
           this.getBalanceValidity(this.selected);
           
       }else{
      //  this.loader.dismiss();  
        console.log("***OK2***");
        getService.forEach(element => {
          if(element.pre_Pos_Indicator == "Prepaid"){
            var checkDoublication =  this.allNumber.includes(element.mobileNumber);
            if(!checkDoublication){
             this.allNumber.push(element.mobileNumber);
             this.otherNumbers.push(element.mobileNumber);
            }
          // this.allNumber.push(element.mobileNumber);
         // this.otherNumbers.push(element.mobileNumber);
          }
         });
         this.selected = this.gatewayService.GetMobileNumber(); 
         this.events.publish('CheckKawKaw',this.selected);
         this.reloadService.SetSelectMobileNumber(this.selected);
         this.getBalanceValidity(this.selected);
        }
       }).catch(err => {

        if(this.reloadService.getshowingHidenLoader() != 0){
          this.loader.dismiss();  
            this.reloadService.showingHidenLoader(false);
         }

        
      console.log("*****reload*******"+JSON.stringify(err));
      if(err=="No internet connection")
      return;
      if(this.reloadService.GetErrorStatu() == false){
        this.reloadService.SetErrorStatus(true);
        this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      }else{

      }

       
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
  });

   }else{
    this.tested = [];
    this.allNumber = [];
    this.otherNumbers = [];

    var loginNumber = this.gatewayService.GetMobileNumber();
    this.allNumber.push(loginNumber);
    this.otherNumbers.push(loginNumber);
    this.selected =  this.gatewayService.GetMobileNumber();
    this.events.publish('CheckKawKaw',this.selected);
    this.reloadService.SetSelectMobileNumber(this.selected);
    this.getBalanceValidity(this.selected);




   }



  }

  getBalanceValidity(selectedNumber){
   // alert("Gettting "+selectedNumber);

   var checkDoublication =  this.otherNumbers.includes(selectedNumber);
   if(checkDoublication){
    var removespecialCharacter =   selectedNumber.replace(/[^0-9]/g, "")
    
      
        this.reloadService.FeachValidityBlance(removespecialCharacter).then((res)=> { 
        
          console.log('itok ******'+JSON.stringify(res));
          this.creditBlance = "";
          this.lineValidity = "";
        
    var checkNewlyAddNumber =  res["Envelope"].Body.IntegrationEnquiryResultMsg.ResultHeader.ResultDesc.CDATA;
      if(checkNewlyAddNumber == "The proper route cannot be found."){
        if(this.reloadService.getshowingHidenLoader() != 0){
          this.loader.dismiss();  
            this.reloadService.showingHidenLoader(false);
         }
        this.creditBlance = "--";
        this.lineValidity = "--";
      } else{
       // if(this.reloadService.getshowingHidenLoader() != 0){
          this.loader.dismiss();  
       ///     this.reloadService.showingHidenLoader(false);
         //}

        this.balanceArray = res["Envelope"].Body.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.BalanceRecordList.BalanceRecord;
        this.prepaidCurrentBalance = res["Envelope"].Body.IntegrationEnquiryResultMsg.IntegrationEnquiryResult;
        console.log("Checking Data "+JSON.stringify(this.balanceArray));
        if (this.prepaidCurrentBalance.SubscriberState.ActiveStop.CDATA != undefined) {
          var tempExp = this.prepaidCurrentBalance.SubscriberState.ActiveStop.CDATA;
        } else {
          var tempExp = this.prepaidCurrentBalance.SubscriberState.ActiveStop;
        }
        console.log(tempExp);
        this.lineValidity = this.formatDate(tempExp);
          if(this.balanceArray.length == undefined){
          
            if(this.balanceArray.AccountType == "2000"){
            //this.lineValidity = this.reloadService.DataAlignment(this.balanceArray.ExpireTime);
            this.creditBlance =  Number(this.balanceArray.Balance) /10000;
            // this.creditBlance = "RM"+parseFloat(this.creditBlance).toFixed(2);
            this.creditBlance = parseFloat(this.creditBlance).toFixed(2);
         }
         } else{
         this.balanceArray.forEach(element => {
         
            if(element.AccountType == "2000"){
               //this.lineValidity = this.reloadService.DataAlignment(element.ExpireTime);
               this.creditBlance =  Number(element.Balance) /10000;
              // this.creditBlance = "RM"+parseFloat(this.creditBlance).toFixed(2);
               this.creditBlance = parseFloat(this.creditBlance).toFixed(2);
            }
           });
          }
      }
      }).catch(err => {
        this.loader.dismiss();  
          console.log("*****reload*******"+JSON.stringify(err));
          if(err=="No internet connection")
          return;
          if(this.reloadService.GetErrorStatu() == false){
            this.reloadService.SetErrorStatus(true);
            this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
          }else{
          
          }
         
           //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
      });
   }else{
    if(this.reloadService.getshowingHidenLoader() != 0){
      this.loader.dismiss();  
        this.reloadService.showingHidenLoader(false);
     }
    this.creditBlance = "--";
    this.lineValidity = "--";
   }


  
  }
  afterUpdateReload(selectedNumber){
    // alert("Gettting "+selectedNumber);
 
    var checkDoublication =  this.otherNumbers.includes(selectedNumber);
    
     var removespecialCharacter =   selectedNumber.replace(/[^0-9]/g, "")
     
       
         this.reloadService.FeachValidityBlance(removespecialCharacter).then((res)=> { 
         
           console.log('itok ******'+JSON.stringify(res));
           
         
     var checkNewlyAddNumber =  res["Envelope"].Body.IntegrationEnquiryResultMsg.ResultHeader.ResultDesc.CDATA;
       if(checkNewlyAddNumber == "The proper route cannot be found."){
         if(this.reloadService.getshowingHidenLoader() != 0){
           this.loader.dismiss();  
             this.reloadService.showingHidenLoader(false);
          }
         this.creditBlance = "--";
         this.lineValidity = "--";
       } else{
        // if(this.reloadService.getshowingHidenLoader() != 0){
           this.loader.dismiss();  
        ///     this.reloadService.showingHidenLoader(false);
          //}
          this.creditBlance = "";
          this.lineValidity = "";
         this.balanceArray = res["Envelope"].Body.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.BalanceRecordList.BalanceRecord;
        this.prepaidCurrentBalance = res["Envelope"].Body.IntegrationEnquiryResultMsg.IntegrationEnquiryResult;
         console.log("Checking Data "+JSON.stringify(this.balanceArray));
         if (this.prepaidCurrentBalance.SubscriberState.ActiveStop.CDATA != undefined) {
           var tempExp = this.prepaidCurrentBalance.SubscriberState.ActiveStop.CDATA;
         } else {
           var tempExp = this.prepaidCurrentBalance.SubscriberState.ActiveStop;
         }
         console.log(tempExp);
         this.lineValidity = this.formatDate(tempExp);





         console.log("Checking Data "+JSON.stringify(this.balanceArray));
           if(this.balanceArray.length == undefined){
           
             if(this.balanceArray.AccountType == "2000"){
          //   this.lineValidity = this.reloadService.DataAlignment(this.balanceArray.ExpireTime);
             this.creditBlance =  Number(this.balanceArray.Balance) /10000;
            //  this.creditBlance = "RM"+parseFloat(this.creditBlance).toFixed(2);
             this.creditBlance = parseFloat(this.creditBlance).toFixed(2);
          }
          } else{
          this.balanceArray.forEach(element => {
          
             if(element.AccountType == "2000"){
               // this.lineValidity = this.reloadService.DataAlignment(element.ExpireTime);
                this.creditBlance =  Number(element.Balance) /10000;
                // this.creditBlance = "RM"+parseFloat(this.creditBlance).toFixed(2);
                this.creditBlance = parseFloat(this.creditBlance).toFixed(2);
             }
            });
           }
       }
       }).catch(err => {
         this.loader.dismiss();  
           console.log("*****reload*******"+JSON.stringify(err));
           if(err=="No internet connection")
           return;
           if(this.reloadService.GetErrorStatu() == false){
             this.reloadService.SetErrorStatus(true);
             this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
           }else{
           
           }
          
            //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
       });
    
 
 
   
   }

  presentPopover(myEvent) {
   var getNewNumber =  this.reloadService.getPhoneBookNumber();
   
   if(getNewNumber){
    var checkDoublication =  this.allNumber.includes(getNewNumber);
     if(!checkDoublication){
      this.allNumber.push(getNewNumber);
     }
   }
      

    let selected = this.selected;
    let allNumber = this.allNumber;
    let popover = this.popoverCtrl.create(ReloadPopoverComponent ,  {selected,allNumber}  );
    popover.present({
      ev: myEvent
    });
    
    popover.onWillDismiss(data => {
      console.log(data);
      if(data!=null){
         this.selected=data;
         this.reloadService.SetSelectMobileNumber(this.selected);
         this.loader = this.gatewayService.LoaderService();
         if(this.reloadService.getshowingHidenLoader() == 0){
          this.loader.present();
          this.reloadService.showingHidenLoader(true);
        }
         this.events.publish('CheckKawKaw',this.selected);
         this.getBalanceValidity(this.selected);
         //alert('gowa l reload' +this.selected);
      }
    })
  }


  formatDate(val){
    if(val==""|| val==null || val==undefined)
    return;
    var dt = val.substring(6, 8)+'/'+val.substring(4, 6)+'/'+val.substring(2, 4);
    return dt;
  }

}
