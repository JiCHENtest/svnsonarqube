import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';

// service
import { SubscriptionService } from "../../subscriptions/subscriptionService";
import { GatewayService } from '../../../global/utilService';

/**
 * Generated class for the SubscriptionSelectCountryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-subscription-select-country',
  templateUrl: 'subscription-select-country.html',
})
export class SubscriptionSelectCountryPage {

  countries: any;
  callback: any;
  plan_name: any;
  plan_type: any;
  is_plan_price_promo: boolean = false;;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public subscriptionService: SubscriptionService,
    private viewCtrl: ViewController,
    public gatewayService: GatewayService,
    private renderer: Renderer2
  ) {
    this.callback = this.navParams.get("callback")

    // plan name
    this.plan_name = (this.navParams.get('plan_name')['PASS_NAME'] != undefined && this.navParams.get('plan_name')['PASS_NAME'] == "1-Day Basic Internet Pass") ? "Basic Internet Pass" : this.navParams.get('plan_name')['PASS_NAME'];

    this.plan_type = this.navParams.get('plan_name');

    // 15-Mar-2018
    // this.is_plan_price_promo = (this.navParams.get('plan_name')['PASS_PRICE_DISPLAY'] != undefined && this.navParams.get('plan_name')['PASS_PRICE_DISPLAY'] == 'RM4.99' && this.navParams.get('plan_name')['PASS_ISPROMOAVAILABLE'] == 'True') ? true : false;

    this.is_plan_price_promo = (this.navParams.get('plan_name')['PASS_PRICE_DISPLAY'] != undefined && this.navParams.get('plan_name')['PASS_PRICE_DISPLAY'] == 'RM4.99') ? true : false;

    console.log("SubscriptionSelectCountryPage -> constructor() -> callback = ", this.callback);
    console.log("SubscriptionSelectCountryPage -> constructor() -> plan_name = ", this.plan_name);
    console.log("SubscriptionSelectCountryPage -> constructor() -> navParams.get('plan_name') = ", this.navParams.get('plan_name'));
    console.log("SubscriptionSelectCountryPage -> constructor() -> plan_type = ", this.plan_type);


    this.initializeCountriesItems();
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Subscription select country", this.renderer);
  }

  dismiss() {
    this.viewCtrl.dismiss();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubscriptionSelectCountryPage');
  }

  initializeCountriesItems() {
    var _this = this;

    var loader = this.gatewayService.LoaderService();
    loader.present();

    this.countries = this.subscriptionService.getCountryList();

    console.log("SubscriptionSelectCountryPage -> initializeCountriesItems() -> plan_name ", this.plan_name);
    try {
      this.countries = this.countries.filter((item) => {

        var isPromoCountry = (item.name == 'Bangladesh' || item.name == 'Cambodia' || item.name == 'Indonesia' || item.name == 'Nepal' || item.name == 'Singapore' || item.name == 'Sri Lanka') ? true : false;

        return (item.Plan).some(function (b) {
          console.log("SubscriptionSelectCountryPage -> initializeCountriesItems() -> filter ", JSON.stringify(b));
          console.log("SubscriptionSelectCountryPage -> initializeCountriesItems() -> *inside filter * plan_name ", _this.plan_name);
          console.log("SubscriptionSelectCountryPage -> initializeCountriesItems() -> isPromoCountry ", isPromoCountry);
          console.log("SubscriptionSelectCountryPage -> initializeCountriesItems() -> is_plan_price_promo ", _this.is_plan_price_promo);


          if ((b.display_name.toLowerCase()).indexOf(("1-DAY INTERNET PASS").toLowerCase()) > -1 && _this.is_plan_price_promo && isPromoCountry) {
            console.log("SubscriptionSelectCountryPage 1")
            return ((b.display_name.toLowerCase()).indexOf((_this.plan_name).toLowerCase()) > -1 && b.isavaliable == "no");
          }
          else if ((b.display_name.toLowerCase()).indexOf(("1-DAY INTERNET PASS").toLowerCase()) > -1 && !_this.is_plan_price_promo && !isPromoCountry) {
            console.log("SubscriptionSelectCountryPage 2")
            return ((b.display_name.toLowerCase()).indexOf((_this.plan_name).toLowerCase()) > -1 && b.isavaliable == "no");
          }
          else if (_this.is_plan_price_promo == false) {
            console.log("SubscriptionSelectCountryPage 3")
            return ((b.display_name.toLowerCase()).indexOf((_this.plan_name).toLowerCase()) > -1 && b.isavaliable == "yes");
          }
          //return selected[b.item_id];
        });
      });

      setTimeout(() => {
        loader.dismiss();
      }, 900);
    }
    catch (e) {
      loader.dismiss();
      console.log("catch ", e);
    }
  }

  getItems(ev: any) {

    // Reset items back to all of the items
    this.initializeCountriesItems();

    // set val to the value of the searchbar
    let val = ev.target.value;
    console.log("val ", val);

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.countries = this.countries.filter((item) => {
        return ((item.name.toLowerCase()).indexOf(val.toLowerCase()) > -1);
      })
    }
  }



  goToDetailsPage(countryDetails) {

    var loader = this.gatewayService.LoaderService();
    loader.present();

    // var selected_country_vlrids : any = countryDetails.Vlrids[0].id.split("/") 
    // var vlrid_to_match : any = this.gatewayService.GetVLRDetailsData();


    // console.log("SubscriptionSelectCountryPage -> goToDetailsPage() -> selected_country_vlrids = ",selected_country_vlrids);
    // console.log("SubscriptionSelectCountryPage -> goToDetailsPage() -> vlrid_to_match = ",vlrid_to_match);

    // try {
    //   if(vlrid_to_match != undefined) {

    //     if(vlrid_to_match.RoamingFlag != undefined && vlrid_to_match.RoamingFlag.CDATA != undefined) {


    //       console.log("SubscriptionSelectCountryPage -> goToDetailsPage() -> vlrid_to_match.RoamingFlag.CDATA = ",vlrid_to_match.RoamingFlag.CDATA);
    //       this.subscriptionService.checkVlrId(selected_country_vlrids,vlrid_to_match.RoamingFlag.CDATA).then((vlr) => {
    //         console.log("SubscriptionSelectCountryPage -> goToDetailsPage() -> isvlr = ",vlr);

    //         if(vlr)
    //           this.subscriptionService.SetCheckVlrIdForOtherCountry(countryDetails, vlr);
    //         else
    //           this.subscriptionService.SetCheckVlrIdForOtherCountry(countryDetails, vlr);

    //           this.callback(countryDetails).then(()=>{
    //             loader.dismiss();
    //             this.navCtrl.pop();      
    //           });

    //         console.log("SubscriptionSelectCountryPage -> goToDetailsPage() -> countryDetails.name, vlr ",countryDetails.name, vlr);
    //       });
    //     }
    //     else {

    //       console.log("SubscriptionSelectCountryPage -> goToDetailsPage() -> else 1");
    //       this.subscriptionService.SetCheckVlrIdForOtherCountry(countryDetails, false);

    //       this.callback(countryDetails).then(()=>{
    //         loader.dismiss();
    //         this.navCtrl.pop();      
    //       });
    //     }
    //   }
    //   else {
    //     console.log("SubscriptionSelectCountryPage -> goToDetailsPage() -> else 2");
    //     this.subscriptionService.SetCheckVlrIdForOtherCountry(countryDetails, false);

    //     this.callback(countryDetails).then(()=>{
    //       loader.dismiss();
    //       this.navCtrl.pop();      
    //     });
    //   }
    // }
    // catch(e) {
    //   console.log("SubscriptionSelectCountryPage -> goToDetailsPage() -> Other catch goToDetailsPage ",e); 
    //   loader.dismiss();
    // }
    console.log("Other ", JSON.stringify(countryDetails));

    // console.log("SubscriptionSelectCountryPage -> goToDetailsPage() -> vlrid data = ",this.gatewayService.GetVLRDetailsData());


    this.callback(countryDetails).then(() => {
      loader.dismiss();
      this.navCtrl.pop();
    });


    // this.navCtrl.parent.parent.push(SubscriptionConfirmationPage,{"details":countryDetails,"isRoaming":true,"country_details":this.selectedCountryName});
  }

}
