import { Component, NgZone, Renderer2 } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';

//page to navigate
import { SubscriptionConfirmationPage } from '../subscription-confirmation/subscription-confirmation';

// service
import { SubscriptionService } from "../../subscriptions/subscriptionService";
import { DashboardService } from '../../../pages/home/homeService'
import { GatewayService } from '../../../global/utilService';
/**
 * Generated class for the IddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-idd',
  templateUrl: 'idd.html',
})
export class IddPage {

  iddCategoryPlan : any;
  isIDD : boolean =false;
  indexValue:any;
  constructor(
    public events: Events,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public subscriptionService : SubscriptionService, 
    public dashboardService : DashboardService,
    public zone : NgZone,
    private renderer: Renderer2,
    public gatewayService: GatewayService) {
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Idd subscription", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IddPage');
    this.indexValue="";
    this.events.publish('user:created'); 
    this.zone.run(()=>{
      console.log('ionViewDidLoad zone IddPage');
      this.iddCategoryPlan = this.subscriptionService.getUserIddSubscriptionInfo();
      
     
    });
  }

  ionViewWillEnter() {
    this.indexValue="";
    this.isIDD = true;
    console.log('ionViewWillEnter IddPage');
    this.zone.run(()=>{
      console.log('ionViewWillEnter zone IddPage');
      
      this.iddCategoryPlan = this.subscriptionService.getUserIddSubscriptionInfo();
    });
  }
  ngOnDestroy(){
    this.indexValue="";
  }
  goToDetailsPage(iddDetails, header_title) {
    console.log("goToDetailsPage iddDetails");
    if (this.subscriptionService.isCapZone()) {
      this.subscriptionService.presentConfirm();
    } else {
      this.indexValue = iddDetails;
    this.navCtrl.parent.parent.push(SubscriptionConfirmationPage,{"details":iddDetails,"isIDD":this.isIDD,"header_title":header_title});
    }
  }


}
