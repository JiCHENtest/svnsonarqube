import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import { GatewayService } from '../../global/utilService';
import {ConstantData} from '../../global/constantService';
/**
 * Generated class for the SubscriptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-subscriptions',
  templateUrl: 'subscriptions.html',
})
export class SubscriptionsPage {

  mobileNumber : any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public gatewayService:GatewayService) {
    console.log("On SubscriptionsPage");
    this.getSubscriptionInfos();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubscriptionsPage');
  }

  getSubscriptionInfos() {
    
    console.log("Mobile ",this.gatewayService.GetMobileNumber());
    var params = {
      "msisdn":this.gatewayService.GetMobileNumber(),//"60193522834"//this.localStorage.get("MobileNum","")
      "operation_id":"PROD_INQ",
      "txn_id":this.gatewayService.generateRandomNum(16)
    };
    
    console.log("getSubscriptionInfos param ",params);
    
    // return new Promise((resolve, reject) => {
    this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetSubscriptionData,"POST",params).then(function(res){
      console.log("********************************************** component success getSubscriptionInfos"+JSON.stringify(res));
      alert("GetSubscriptionData "+JSON.stringify(res));
      // _that.handleerror.handleErrorFetchAllDataPostpaidPrepaid(res,"FetchAllDataPostpaidPrepaid").then((result) => {
      //   if(result == true) {
      //     console.log("hai if FetchAllDataPostpaidPrepaid ",result);
          // resolve(res);
        // }
        // else {
          // console.log("hai else FetchAllDataPostpaidPrepaid ",result);
          // reject("error : no data found.");
      //   }
      // });

      // resolve(res);
    })
    .catch(function(e){
        console.log("******************************************** component failure getSubscriptionInfos "+e);
        if(e=="No internet connection")
        return;
        alert("error user info getSubscriptionInfos");
        // reject(e);
    });
  // });
  }

  SubscribeToAddOn(confirmDetails) {
    // for success
    // http://10.1.62.78:9006/tmf?action=maxuppurchaserequest&msisdn=60104060032&operation_id=MAXUP&transaction_id=1234&amount=20&product_id=45892
    var params = {
      "msisdn":this.gatewayService.GetMobileNumber(),//"60193522834"//this.localStorage.get("MobileNum","")
      "operation_id":"MAXUP",
      "txn_id":this.gatewayService.generateRandomNum(16),
      "amount":"20",
      "product_id" : "45892"
    };

    this.gatewayService.CallAdapter(ConstantData.adapterUrls.SubscribeToAddOn,"POST",params).then(function(res){
      console.log("********************************************** component success SubscribeToAddOn"+JSON.stringify(res));
      alert("SubscribeToAddOn "+JSON.stringify(res));      
    })
    .catch(function(e){
        console.log("******************************************** component failure SubscribeToAddOn "+e);
        if(e=="No internet connection")
        return;
        alert("error user info SubscribeToAddOn");
        // reject(e);
    });
  }

  SubscriptionRoamingListForPostpaid() {
    // for success
    // http://10.1.62.78:9006/tmf?action=mfp2/ROAMQRY&sref=60133743604&operation_id=ROAMPROD_INQ&transaction_id=3MEnJpTZMn&vlrid=LOCAL
    var params = {
      "sref":this.gatewayService.GetMobileNumber(),//"60193522834"//this.localStorage.get("MobileNum","")
      "operation_id":"ROAMPROD_INQ",
      "txn_id":this.gatewayService.generateRandomNum(16),
      "vlrid":"LOCAL"
    };

    this.gatewayService.CallAdapter(ConstantData.adapterUrls.SubscriptionRoamingListForPostpaid,"POST",params).then(function(res){
      console.log("********************************************** component success SubscriptionRoamingListForPostpaid"+JSON.stringify(res));
      alert("SubscriptionRoamingListForPostpaid "+JSON.stringify(res));      
    })
    .catch(function(e){
        console.log("******************************************** component failure SubscriptionRoamingListForPostpaid "+e);
        if(e=="No internet connection")
        return;
        alert("error user info SubscriptionRoamingListForPostpaid");
        // reject(e);
    });
  }

  SubscribeRoamingPlanForPostpaid() {
    // For Roaming subscribe, endpoint URL:
	  // http://10.1.62.78:9006/tmf?action=mfp2/ROAMPASS&sref=60133743604&product_id=CPT13750&trxid=vmHmVTTn1T
	
    // var responses = {
    // "sref":this.gatewayService.GetMobileNumber(),//"60193522834"//this.localStorage.get("MobileNum","")
    // "product_id":"ROAMPROD_INQ", // taken from 
    // "txn_id":this.gatewayService.generateRandomNum(16)
    // };
    var params = {
      "sref":this.gatewayService.GetMobileNumber(),//"60193522834"//this.localStorage.get("MobileNum","")
      "product_id":"MI02210", // pass from SubscriptionRoamingListForPostpaid (Product id will be the pass id from ROAMQRY). using 60104060032 with pass id MI02210
      "txn_id":this.gatewayService.generateRandomNum(16)
    };

    this.gatewayService.CallAdapter(ConstantData.adapterUrls.SubscribeRoamingPlanForPostpaid,"POST",params).then(function(res){
      console.log("********************************************** component success Subscribing Roaming Plan For Postpaid"+JSON.stringify(res));
      alert("SubscribingRoamingPlanFor Postpaid "+JSON.stringify(res));      
    })
    .catch(function(e){
        console.log("******************************************** component failure SubscribingRoamingPlanForPostpaid "+e);
        if(e=="No internet connection")
        return;
        alert("error user info SubscribingRoamingPlanFor Postpaid");
        // reject(e);
    });
  }

  SubscriptionRoamingListForPrepaid() {
    // for success
    // http://10.1.62.78:9006/tmf?action=mfp2/ROAMPREQRY&sref=601140799647&operation_id=ROAMPROD_INQ&transaction_id=hfhb&vlrid=LOCAL
    var params = {
      "sref":this.gatewayService.GetMobileNumber(),//"60193522834"//this.localStorage.get("MobileNum","")
      "operation_id":"ROAMPROD_INQ",
      "txn_id":this.gatewayService.generateRandomNum(16),
      "vlrid":"LOCAL"
    };

    this.gatewayService.CallAdapter(ConstantData.adapterUrls.SubscriptionRoamingListForPrepaid,"POST",params).then(function(res){
      console.log("********************************************** component success SubscriptionRoamingListForPrepaid"+JSON.stringify(res));
      alert("SubscriptionRoamingListForPrepaid "+JSON.stringify(res));      
    })
    .catch(function(e){
        console.log("******************************************** component failure SubscriptionRoamingListForPrepaid "+e);
        if(e=="No internet connection")
        return;
        alert("error user info SubscriptionRoamingListForPrepaid");
        // reject(e);
    });
  }

  SubscribeRoamingPlanForPrepaid() {
    // For Roaming subscribe, endpoint URL:
    // http://10.1.62.78:9006/tmf?action=mfp2/ROAMPASSPRE&sref=60136430806&product_id=45687&trxid=7899
    	
    // var responses = {
    // "sref":this.gatewayService.GetMobileNumber(),//"60193522834"//this.localStorage.get("MobileNum","")
    // "product_id":"ROAMPROD_INQ", // taken from 
    // "txn_id":this.gatewayService.generateRandomNum(16)
    // };
    var params = {
      "sref":this.gatewayService.GetMobileNumber(),//"60193522834"//this.localStorage.get("MobileNum","")
      "product_id":"45687", // pass from SubscriptionRoamingListForPostpaid (Product id will be the pass id from ROAMQRY). using 60104060032 with pass id MI02210
      "txn_id":this.gatewayService.generateRandomNum(16)
    };

    this.gatewayService.CallAdapter(ConstantData.adapterUrls.SubscribeRoamingPlanForPrepaid,"POST",params).then(function(res){
      console.log("********************************************** component success Subscribing Roaming Plan For prepaid"+JSON.stringify(res));
      alert("SubscribeRoamingPlanFor Prepaid "+JSON.stringify(res));      
    })
    .catch(function(e){
        console.log("******************************************** component failure SubscribingRoamingPlanForPrepaid "+e);
        if(e=="No internet connection")
        return;
        alert("error user info SubscribingRoamingPlanFor prepaid");
        // reject(e);
    });
  }
}
