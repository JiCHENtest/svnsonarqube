import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// service
import { SpeedcheckerProvider } from '../../global/speedchecker';
import { GatewayService } from '../../global/utilService';
/**
 * Generated class for the NetSpeedCheckInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-net-speed-check-info',
  templateUrl: 'net-speed-check-info.html',
})
export class NetSpeedCheckInfoPage {

  // page
  msisdnTextSpeed : any;
  registrationIdSpeed : any;
  downloadStatus : any;
  uploadStatus : any;
  pingStatus : any;

  temp: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public speedcheckerProvider : SpeedcheckerProvider, public gatewayService:GatewayService, private renderer: Renderer2) {
    this.speedcheckerProvider.resetSpeedtest();
    
    
    this.speedcheckerProvider.refreshMsisdn(0).then(data =>{

      this.msisdnTextSpeed = this.speedcheckerProvider.getMsisdnTextSpeed(); 
      this.registrationIdSpeed = this.speedcheckerProvider.getRegistrationIdSpeed(); 
      this.downloadStatus = this.speedcheckerProvider.getDownloadStatus(); 
      this.uploadStatus = this.speedcheckerProvider.getUploadStatus();
      this.pingStatus = this.speedcheckerProvider.getPingStatus();

      console.log("constructor refreshMsisdn NetSpeedCheckInfoPage ",data);
    }, err => {
      console.log("constructor refreshMsisdn NetSpeedCheckInfoPage err ",err);
    });

    // this.msisdnTextSpeed = this.speedcheckerProvider.getMsisdnTextSpeed(); 
    // this.registrationIdSpeed = this.speedcheckerProvider.getRegistrationIdSpeed(); 
    // this.downloadStatus = this.speedcheckerProvider.getDownloadStatus(); 
    // this.uploadStatus = this.speedcheckerProvider.getUploadStatus();
    // this.pingStatus = this.speedcheckerProvider.getPingStatus();
  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Internet speed check info", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NetSpeedCheckInfoPage');
  }

  btn_startSpeedtest() {
    this.speedcheckerProvider.btn_startSpeedtest();
    
    this.msisdnTextSpeed = this.speedcheckerProvider.getMsisdnTextSpeed(); 
    this.registrationIdSpeed = this.speedcheckerProvider.getRegistrationIdSpeed(); 
    this.downloadStatus = this.speedcheckerProvider.getDownloadStatus(); 
    this.uploadStatus = this.speedcheckerProvider.getUploadStatus();
    this.pingStatus = this.speedcheckerProvider.getPingStatus();
  }

  btn_stopSpeedtest() {
    this.speedcheckerProvider.btn_stopSpeedtest();

    this.msisdnTextSpeed = this.speedcheckerProvider.getMsisdnTextSpeed(); 
    this.registrationIdSpeed = this.speedcheckerProvider.getRegistrationIdSpeed(); 
    this.downloadStatus = this.speedcheckerProvider.getDownloadStatus(); 
    this.uploadStatus = this.speedcheckerProvider.getUploadStatus();
    this.pingStatus = this.speedcheckerProvider.getPingStatus();
  }

  ngOnInit() {
    
    this.temp = setInterval(() => {
      // this.battleInit(); 
      console.log("ngOnInit");
      this.msisdnTextSpeed = this.speedcheckerProvider.getMsisdnTextSpeed(); 
      this.registrationIdSpeed = this.speedcheckerProvider.getRegistrationIdSpeed(); 
      this.downloadStatus = this.speedcheckerProvider.getDownloadStatus(); 
      this.uploadStatus = this.speedcheckerProvider.getUploadStatus();
      this.pingStatus = this.speedcheckerProvider.getPingStatus();
    }, 500);
  }
  
  ngOnDestroy() {
    console.log("ngOnDestroy in");
    if (this.temp) {
      console.log("ngOnDestroy out");
      clearInterval(this.temp);
    }
  }

}
