import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';

import { DetailedUsageService } from '../../detailed-usage/detailedUsageService';
import { DashboardService } from '../../home/homeService';
import { GatewayService } from '../../../global/utilService';
import { ConstantData } from '../../../global/constantService';

/**
 * Generated class for the DetailedUsageInternetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// Card CTA
// Card Toggle/CTA
// Card Usage with Limit
// Card PPU
// Card Description+ Toggle
// Card Description
// Card Usage with Limit (Kad Ceria)
// Card Bundle (with Toggle/CTA)
// Card Info (Roaming)
// Card Info (Plan Activation)

@Component({
  selector: 'page-detailed-usage-internet',
  templateUrl: 'detailed-usage-internet.html',
})
export class DetailedUsageInternetPage {
  bundleUsagePercent: number = 70;
  detailedInternetUsage:any;
  cardArray:any = [];
  cardMapping:any;
  internetPlans:any;
  GBSharePlan:any;
  userInfo:any;
  isPrepaid:Boolean;
  PPUPrepaid:any;
  isweekend:boolean;
  packs:any = [];
  usageSince:any;
  IsAdmin:boolean = false;
  IsCBSVal:Boolean = false;
  mobileNumber:any = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public detailedUsageService: DetailedUsageService, public gatewayService:GatewayService, 
    public dashboardService: DashboardService, public events:Events,  private renderer: Renderer2) {
  //this.detailedInternetUsage = this.detailedUsageService.GetDetailedusage();
  let planName = this.gatewayService.getPlanName();
  console.log("**************** plan name****************"+planName);
  this.cardMapping = ConstantData.dialMapping.postpaid[planName];
  console.log("***************** Card mapping ****************"+JSON.stringify(this.cardMapping));
  this.internetPlans = this.dashboardService.GetAllDataPostpaidPrepaid();
  this.usageSince = this.gatewayService.getUsageSince();
  //this.GBSharePlan = this.dashboardService.GetUnits();
  this.userInfo = this.dashboardService.GetUserInfo();
  this.mobileNumber = this.userInfo.ListOfServices.Services.MobileNumber;
  this.isPrepaid = this.gatewayService.isPrePaid();
  this.isweekend = this.dashboardService.GetIsWeekend();
  //var isCBS = gatewayService.GetCBSValue();
  //var units = dashboardService.GetUnits();
  this.packs = ["YOUTUBEPACK","INSTAGRAMPACK","FBPACK"];
  let isReloadDashboard:boolean = this.dashboardService.GetIsReloadDashboardCalled();
  this.IsCBSVal = this.gatewayService.GetCBSValue();
  if(isReloadDashboard==true){
    //alert(isReloadDashboard);
    this.generateInternetCards();
    this.dashboardService.SetIsReloadDashboardCalled(false);
  }else{
    this.cardArray = this.detailedUsageService.GetcardsDetails();
  }
  //let isCBSVal:Boolean = false;  
  //alert(this.IsCBSVal);
  if(this.IsCBSVal==true)               
  var unitsVal = this.dashboardService.GetUnits();
  else
  var unitsVal = null;
  this.IsAdmin = this.detailedUsageService.GetIsGBAdmin();
  var cardMappingData = {
    "internetPlans":this.dashboardService.GetAllDataPostpaidPrepaid(),
    "isPrepaid":this.isPrepaid,
    "isCBS":this.IsCBSVal,
    "units":unitsVal
  }
  // this.cardArray = [
  //   {
  //     "title":"Internet Pack 1",
  //     "subtitle":"3 days plan",
  //     "cardType":"Card CTA #1"
  //   },
  //   {
  //     "title":"Internet Pack 2",
  //     "subtitle":"3 days plan 2",
  //     "cardType":"Card Toggle/CTA #1"
  //   },
  //   {
  //     "title":"Internet Pack 3",
  //     "subtitle":"3 days plan 2",
  //     "cardType":"Card Usage with Limit #1"
  //   },
  //   {
  //     "title":"Internet Pack 4",
  //     "subtitle":"3 days plan",
  //     "cardType":"Card PPU #1"
  //   },
  //   {
  //     "title":"Internet Pack 5",
  //     "subtitle":"3 days plan 2",
  //     "cardType":"Card Description+ Toggle #1"
  //   },
  //   {
  //     "title":"Internet Pack 6",
  //     "subtitle":"3 days plan 2",
  //     "cardType":"Card Description #1"
  //   },
  //   {
  //     "title":"Internet Pack 7",
  //     "subtitle":"3 days plan",
  //     "cardType":"Card Usage with Limit (Kad Ceria) #1"
  //   },
  //   {
  //     "title":"Internet Pack 8",
  //     "subtitle":"3 days plan 2",
  //     "cardType":"Card Bundle (with Toggle/CTA) #1"
  //   },
  //   {
  //     "title":"Internet Pack 9",
  //     "subtitle":"3 days plan 2",
  //     "cardType":"Card Info (Roaming) #1"
  //   },
  //   {
  //     "title":"Internet Pack 10",
  //     "subtitle":"3 days plan 2",
  //     "cardType":"Card Info (Plan Activation) #1"
  //   },
  //   {
  //     "title":"Internet Pack 11",
  //     "subtitle":"3 days plan 2",
  //     "cardType":"Group Sharing Data #1"
  //   }
  // ]

  }

  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Internet detailed usage", this.renderer);
  }

  formatGBValue(val){
    if(val==""|| val==null || val==undefined)
    return;
    var formattedStr = val.substr(0,val.indexOf(' '));
    formattedStr = Number(formattedStr);
    return formattedStr;
  }

  GbshareUpdate(){
    this.IsAdmin = false;
    console.log("ionViewDidEnter");
    var userInfo = this.dashboardService.GetUserInfo();
    var adminNumber = userInfo["ListOfServices"].Services.BillingType;
    var GroupId  = userInfo["ListOfServices"].Services.SmeGroupId;
    if(adminNumber == "Billable"){
      if(GroupId){
        this.IsAdmin = true;
       // this.GbShareRedirection(GroupId);    
      }
    }
    return this.IsAdmin;
  }

  formatDate(val){
    if(val==""|| val==null || val==undefined)
    return;
    var dt = val.substring(6, 8)+'/'+val.substring(4, 6)+'/'+val.substring(0, 4);
    return dt;
  }

  formatGBShareDate(val){
    if(val){
    var str = val.substr(0,10);
    return str;
    }else{
      return '';
    }
  }

  generateInternetCards(){
    if(this.IsCBSVal == false){
    this.generateBasePlanCard();
    }
    this.generateGBShareCards();
    if(this.internetPlans.subscriberQuota.subscriberWeekendInternet){
        this.generateWeekendPlanCard();
    }    
    this.generateAddOnes();
    if(this.isPrepaid){
      this.generatePPUInternetPrepaidCard();
    }
    this.detailedUsageService.SetcardsDetails(this.cardArray);
  }
  generateBasePlanCard(){
      if(this.internetPlans.subscriberQuota.subscriberBaseQuota=="" || this.internetPlans.subscriberQuota.subscriberBaseQuota==null || this.internetPlans.subscriberQuota.subscriberBaseQuota==undefined){
        var NoInternet = {
          "title":"No Internet Plan",
          "subtitle":"You're not on any Internet plan yet",
          "date":"",
          "action":"Subscribe Now",
          "class":"icon_internet icon",
          "cardType":"Card CTA #1"
        }    
        this.cardArray.push(NoInternet);
    }else{
      if(this.isPrepaid){
      //var plan = this.internetPlans.subscriberQuota.subscriberBaseQuota.PLAN_TYPE;
      var plan = "Prepaid Internet"
      }else{
        var plan = "Postpaid Internet"
      }
      
      var BUSINESSNAME = this.internetPlans.subscriberQuota.subscriberBaseQuota.BUSINESSNAME;
      var Used = this.formatGBValue(this.internetPlans.subscriberQuota.subscriberBaseQuota.QUOTAUSAGE_GIGABYTES);
      var Total = this.formatGBValue(this.internetPlans.subscriberQuota.subscriberBaseQuota.TOTALQUOTAVALUE_GIGABYTES);
      var Balance = this.formatGBValue(this.internetPlans.subscriberQuota.subscriberBaseQuota.QUOTABALANCE_GIGABYTES);
      var productID = this.internetPlans.subscriberQuota.subscriberBaseQuota.PRODUCT_ID;
      var date = this.internetPlans.subscriberQuota.subscriberBaseQuota.EXPIRYDATE;
      var flag = this.internetPlans.subscriberQuota.subscriberBaseQuota.AUTORENEWALFLAG;
      let AUTORENEWALFLAG:boolean;
      if(flag=="0"){
      AUTORENEWALFLAG =true;
      }

      if(!this.isPrepaid)
      AUTORENEWALFLAG = false;

      if(date!=""){
        date = this.formatDate(date);
      }else{
        date = '';
      }
      var internetBaseData = {
        "title":plan,
        "subtitle":BUSINESSNAME,
        "dataUsed":Used,
        "dataTotal":Total,
        "dataBalance":Balance,
        "date":date,
        "autoRenew":AUTORENEWALFLAG,
        "productId":productID,
        "unit":'GB',
        "class":"icon_internet icon",
        "cardType":"Card Toggle/CTA #1"
      }
      this.cardArray.push(internetBaseData);
    }
    }
    
    generateWeekendPlanCard(){
      //var plan = this.internetPlans.subscriberQuota.subscriberWeekendInternet.PLAN_TYPE;
      if(this.isPrepaid){
        //var plan = this.internetPlans.subscriberQuota.subscriberBaseQuota.PLAN_TYPE;
        var plan = "Weekend Prepaid Internet"
        }else{
          var plan = "Weekend Postpaid Internet"
        }
      var BUSINESSNAME = this.internetPlans.subscriberQuota.subscriberWeekendInternet.BUSINESSNAME;
      var Used = this.formatGBValue(this.internetPlans.subscriberQuota.subscriberWeekendInternet.QUOTAUSAGE_GIGABYTES);
      var Total = this.formatGBValue(this.internetPlans.subscriberQuota.subscriberWeekendInternet.TOTALQUOTAVALUE_GIGABYTES);
      var Balance = this.formatGBValue(this.internetPlans.subscriberQuota.subscriberWeekendInternet.QUOTABALANCE_GIGABYTES);
      var productID = this.internetPlans.subscriberQuota.subscriberWeekendInternet.PRODUCT_ID;
      var date = this.internetPlans.subscriberQuota.subscriberWeekendInternet.EXPIRYDATE;
      var flag = this.internetPlans.subscriberQuota.subscriberWeekendInternet.AUTORENEWALFLAG;
      let AUTORENEWALFLAG:boolean;
      if(flag=="0"){
      AUTORENEWALFLAG =true;
      }
      if(date!=""){
        date = this.formatDate(date);
      }else{
        date = '';
      }
      var internetBaseData = {
        "title":plan,
        "subtitle":BUSINESSNAME,
        "dataUsed":Used,
        "dataTotal":Total,
        "dataBalance":Balance,
        "date":date,
        "unit":'GB',
        "autoRenew":AUTORENEWALFLAG,
        "productId":productID,
        "class":"icon_internet icon",
        "cardType":"Card Toggle/CTA #1"
      }
      this.cardArray.push(internetBaseData);
    }

    generateAddOnes(){
      for(let obj in this.internetPlans.subscriberQuota.subscribedAddOnPacks){
          var pack = this.internetPlans.subscriberQuota.subscribedAddOnPacks[obj];
          if(pack.length>1){
            for(let i in pack){
              this.generateCardUsageWithLimit(pack[i]);
            }
          }else{
            this.generateCardUsageWithLimit(pack);
          }
      }
      // if(this.internetPlans.subscriberQuota.MUSICPACKLIST){
      //   this.generateMusicPacks();   
      // }
      try{
        if(this.internetPlans.subscriberQuota.MUSICPACKLIST.MUSICPACK.length>1){
          this.generateMusicPack();   
        }
      }catch(e){
        console.log("e"+e);
      }
      if(this.internetPlans.subscriberQuota.SUBSCRIBEDFREEBIESLIST){
        this.generatefreebiePacks();
      }
      if(this.internetPlans.subscriberQuota.DEFAULTFREEBIESLIST){
        this.generateDEFAULTFREEBIESLIST();
      }
      if(this.internetPlans.subscriberQuota.FREEBIESLIST){
        this.generateFREEBIESLISTPacks();
      }
      if(this.internetPlans.subscriberQuota.subscribedFreeVideoWalla){
        this.generatesubscribedFreeVideoWalla();
      }      
      if(this.internetPlans.subscriberQuota.ASIAPASS){
        this.generateAsiaPacks();
      }
      if(this.internetPlans.subscriberQuota.subscriberInternetBurungHantuQuota){
        this.generateBurungHantuQuotaPacks();
      }
      if(this.internetPlans.subscriberQuota.TripleBonusQuotaList){
        this.generateTripleBonusQuotaList();
      }
      if(this.internetPlans.subscriberQuota.MYDEALS){
      }
      // if(this.internetPlans.subscriberQuota.KadCeriaUsageDetail){
      //   this.generateKadCeriaUsageDetail();
      // }
      // if(this.internetPlans.subscriberQuota.subscriberBonusQuotaList){
      //   this.generaterBonusQuotaListPacks();
      // }    
    }

    generateMusicPacks(){ 
        if(this.internetPlans.subscriberQuota.MUSICPACKLIST.length>1){
            for(let i in this.internetPlans.subscriberQuota.MUSICPACKLIST){
              var musicPack = this.internetPlans.subscriberQuota.MUSICPACKLIST[i].MUSICPACK;
              console.log("Inside Musicpack:"+JSON.stringify(musicPack));
              var flag = this.internetPlans.subscriberQuota.MUSICPACKLIST[i].MUSICPACK.AUTORENEWAL_FLAG;
              let AUTORENEWALFLAG:boolean;
              if(flag=="0"){
              AUTORENEWALFLAG =true;
              }
              if(musicPack.PLAN=='UNLIMITED'){
                var internetBaseData = {
                  "title":musicPack.DETAIL,
                  "subtitle":musicPack.PLAN,
                  "date":this.formatDate(musicPack.EXPIRYDATE),
                  "cardType":"Card Description+ Toggle #1",
                  "class":"icon_add-ons icon",
                  "autoRenew":AUTORENEWALFLAG
                
                }
            }else{
              var internetBaseData = {
                "title":musicPack.DETAIL,
                "subtitle":musicPack.PLAN,
                "date":this.formatDate(musicPack.EXPIRYDATE),
                "class":"icon_add-ons icon",
                "cardType":"Card Description+ Toggle #1",
                "autoRenew":AUTORENEWALFLAG
              }
            }
            this.cardArray.push(internetBaseData);
            }
        }else{
          var musicPack = this.internetPlans.subscriberQuota.MUSICPACKLIST.MUSICPACK;
          var flag = this.internetPlans.subscriberQuota.MUSICPACKLIST.MUSICPACK.AUTORENEWAL_FLAG;
              let AUTORENEWALFLAG:boolean;
              if(flag=="0"){
              AUTORENEWALFLAG =true;
              }
          if(musicPack.PLAN=='UNLIMITED'){
              var internetBaseData = {
                "title":musicPack.DETAIL,
                "subtitle":musicPack.PLAN,
                "date":this.formatDate(musicPack.EXPIRYDATE),
                "class":"icon_add-ons icon",
                "cardType":"Card Description+ Toggle #1",
                "autoRenew":AUTORENEWALFLAG
              }
          }else{
            var internetBaseData = {
              "title":musicPack.DETAIL,
              "subtitle":musicPack.PLAN,
              "date":this.formatDate(musicPack.EXPIRYDATE),
              "class":"icon_add-ons icon",
              "cardType":"Card Description+ Toggle #1",
              "autoRenew":AUTORENEWALFLAG
            }
          }
          this.cardArray.push(internetBaseData);
      }    
    }

    generateMusicPack(){    
      if(this.internetPlans.subscriberQuota.MUSICPACKLIST.MUSICPACK.length>1){
          for(let i in this.internetPlans.subscriberQuota.MUSICPACKLIST.MUSICPACK){
            var musicPack = this.internetPlans.subscriberQuota.MUSICPACKLIST.MUSICPACK[i];
            var flag = this.internetPlans.subscriberQuota.MUSICPACKLIST.MUSICPACK[i].AUTORENEWAL_FLAG;
            let AUTORENEWALFLAG:boolean;
            if(flag=="0"){
            AUTORENEWALFLAG =true;
            }
            if(musicPack.PLAN=='UNLIMITED'){
              var internetBaseData = {
                "title":musicPack.DETAIL,
                "subtitle":musicPack.PLAN,
                "date":this.formatDate(musicPack.EXPIRYDATE),
                "productId":musicPack.productID,
                "cardType":"Card Description+ Toggle #1",
                "class":"icon_add-ons icon",
                "autoRenew":AUTORENEWALFLAG
              }
          }else{
            var internetBaseData = {
              "title":musicPack.DETAIL,
              "subtitle":musicPack.PLAN,
              "date":this.formatDate(musicPack.EXPIRYDATE),
              "productId":musicPack.productID,
              "class":"icon_add-ons icon",
              "cardType":"Card Description+ Toggle #1",
              "autoRenew":AUTORENEWALFLAG
            }
          }
          this.cardArray.push(internetBaseData);
          }
      }else{
        var musicPack = this.internetPlans.subscriberQuota.MUSICPACKLIST.MUSICPACK;
        var flag = this.internetPlans.subscriberQuota.MUSICPACKLIST.MUSICPACK.AUTORENEWAL_FLAG;
            let AUTORENEWALFLAG:boolean;
            if(flag=="0"){
            AUTORENEWALFLAG =true;
            }
        if(musicPack.PLAN=='UNLIMITED'){
            var internetBaseData = {
              "title":musicPack.DETAIL,
              "subtitle":musicPack.PLAN,
              "date":this.formatDate(musicPack.EXPIRYDATE),
              "productId":musicPack.productID,
              "class":"icon_add-ons icon",
              "cardType":"Card Description+ Toggle #1",
              "autoRenew":AUTORENEWALFLAG
            }
        }else{
          var internetBaseData = {
            "title":musicPack.DETAIL,
            "subtitle":musicPack.PLAN,
            "date":this.formatDate(musicPack.EXPIRYDATE),
            "productId":musicPack.productID,
            "class":"icon_add-ons icon",
            "cardType":"Card Description+ Toggle #1",
            "autoRenew":AUTORENEWALFLAG
          }
        }
        this.cardArray.push(internetBaseData);
    }    
  }

    generatefreebiePacks(){     
      if(this.internetPlans.subscriberQuota.SUBSCRIBEDFREEBIESLIST.SUBSCRIBEDFREEBIE.length>1){        
        for(let i in this.internetPlans.subscriberQuota.SUBSCRIBEDFREEBIESLIST.SUBSCRIBEDFREEBIE){
          var pack = this.internetPlans.subscriberQuota.SUBSCRIBEDFREEBIESLIST.SUBSCRIBEDFREEBIE[i];
          this.generateCardUsageWithLimit(pack);
        }
      }else{
        var pack = this.internetPlans.subscriberQuota.SUBSCRIBEDFREEBIESLIST.SUBSCRIBEDFREEBIE;
        this.generateCardUsageWithLimit(pack);
      }
    }

    generateDEFAULTFREEBIESLIST(){
      if(this.internetPlans.subscriberQuota.DEFAULTFREEBIESLIST.DEFAULTFREEBIE.length>1){        
        for(let i in this.internetPlans.subscriberQuota.DEFAULTFREEBIESLIST.DEFAULTFREEBIE){
          var pack = this.internetPlans.subscriberQuota.DEFAULTFREEBIESLIST.DEFAULTFREEBIE[i];
          this.generateCardUsageWithLimit(pack);
        }
      }else{
        var pack = this.internetPlans.subscriberQuota.DEFAULTFREEBIESLIST.DEFAULTFREEBIE;
        this.generateCardUsageWithLimit(pack);
      }
    }

    generateFREEBIESLISTPacks(){     
      if(this.internetPlans.subscriberQuota.FREEBIESLIST.DEFAULTFREEBIE.length>1){        
        for(let i in this.internetPlans.subscriberQuota.FREEBIESLIST.DEFAULTFREEBIE){
          var pack = this.internetPlans.subscriberQuota.FREEBIESLIST.DEFAULTFREEBIE[i];
          if(pack.BUSINESSNAME!="Unlimited Calls to Celcom Network")
          this.createUnlimitedPlan(pack);
        }
      }else{
        var pack = this.internetPlans.subscriberQuota.FREEBIESLIST.DEFAULTFREEBIE;
        if(pack.BUSINESSNAME!="Unlimited Calls to Celcom Network")
        this.createUnlimitedPlan(pack);
      }
    }

    generaterBonusQuotaListPacks(){
      if(this.internetPlans.subscriberQuota.subscriberBonusQuotaList.length>1){        
        for(let i in this.internetPlans.subscriberQuota.subscriberBonusQuotaList){
          var pack = this.internetPlans.subscriberQuota.subscriberBonusQuotaList[i].subscriberBonusQuota;
          var flag = pack.AUTORENEWALFLAG;         
          let AUTORENEWALFLAG:boolean;
          if(flag=="0"){
          AUTORENEWALFLAG =true;
          }
          //this.generateCardUsageWithLimit(pack);
          var internetData = {
            "title":pack.PACKNAME,
            "subtitle":pack.BUSINESSNAME,
            "dataUsed":this.formatGBValue(pack.QUOTAUSAGE_GIGABYTES),
            "dataTotal":this.formatGBValue(pack.TOTALQUOTAVALUE_GIGABYTES),
            "dataBalance":this.formatGBValue(pack.QUOTABALANCE_GIGABYTES),
            "date":this.formatDate(pack.EXPIRYDATE),
            "unit":'GB',
            "autoRenew":AUTORENEWALFLAG,
            "productId":pack.PRODUCT_ID,
            "class":"icon_add-ons icon",
            "cardType":"Card Toggle/CTA #1"
          }
          this.cardArray.push(internetData);
        }
      }else{
        var pack = this.internetPlans.subscriberQuota.subscriberBonusQuotaList.subscriberBonusQuota;
        var flag = pack.AUTORENEWALFLAG;        
        let AUTORENEWALFLAG:boolean;
        if(flag=="0"){
        AUTORENEWALFLAG =true;
        }
        //this.generateCardUsageWithLimit(pack);
        var internetData = {
          "title":pack.BUSINESSNAME,
          "subtitle":pack.BUSINESSNAME,
          "dataUsed":this.formatGBValue(pack.QUOTAUSAGE_GIGABYTES),
          "dataTotal":this.formatGBValue(pack.TOTALQUOTAVALUE_GIGABYTES),
          "dataBalance":this.formatGBValue(pack.QUOTABALANCE_GIGABYTES),
          "date":this.formatDate(pack.EXPIRYDATE),
          "unit":'GB',
          "autoRenew":AUTORENEWALFLAG,
          "productId":pack.PRODUCT_ID,
          "class":"icon_add-ons icon",
          "cardType":"Card Toggle/CTA #1"
        }
        this.cardArray.push(internetData);
      }
    }

    generateAsiaPacks(){
      var data = [];
      let title;
      if(this.internetPlans.subscriberQuota.ASIAPASS.ASIAPASSINTERNET){
      if(this.internetPlans.subscriberQuota.ASIAPASS.ASIAPASSINTERNET.length>1){
        for(let i in this.internetPlans.subscriberQuota.ASIAPASS.ASIAPASSINTERNET){
          var pack = this.internetPlans.subscriberQuota.ASIAPASS.ASIAPASSINTERNET[i];
          title = pack.PACKNAME;
          var internetData = {
            "title":pack.PACKNAME,
            "subtitle":pack.BUSINESSNAME,
            "dataUsed":this.formatGBValue(pack.QUOTAUSAGE_GIGABYTES),
            "dataTotal":this.formatGBValue(pack.TOTALQUOTAVALUE_GIGABYTES),
            "dataBalance":this.formatGBValue(pack.QUOTABALANCE_GIGABYTES),
            "date":this.formatDate(pack.EXPIRYDATE),
            "autoRenew":pack.AUTORENEWALFLAG,
            "productId":pack.PRODUCT_ID,
            "unit":'GB',
            "class":"icon_add-ons icon",
            "cardType":"Card Bundle (with Toggle/CTA) #1"
          }
          data.push(internetData);
         // this.generateCardUsageWithLimit(pack);
        }
      }else{
        var pack = this.internetPlans.subscriberQuota.ASIAPASS.ASIAPASSINTERNET;
        title = pack.PACKNAME;
        var internetData = {
          "title":pack.PACKNAME,
          "subtitle":pack.BUSINESSNAME,
          "dataUsed":this.formatGBValue(pack.QUOTAUSAGE_GIGABYTES),
          "dataTotal":this.formatGBValue(pack.TOTALQUOTAVALUE_GIGABYTES),
          "dataBalance":this.formatGBValue(pack.QUOTABALANCE_GIGABYTES),
          "date":this.formatDate(pack.EXPIRYDATE),
          "unit":'GB',
          "autoRenew":pack.AUTORENEWALFLAG,
          "productId":pack.PRODUCT_ID,
          "class":"icon_add-ons icon",
          "cardType":"Card Bundle (with Toggle/CTA) #1"
        }
        data.push(internetData);
        //this.generateCardUsageWithLimit(pack);
      }
      this.cardArray.push({"title":title,"data":data,"class":"icon_add-ons icon","cardType":"Card Bundle (with Toggle/CTA) #1"});
    }
    }

    generateBurungHantuQuotaPacks(){
      if(this.internetPlans.subscriberQuota.subscriberInternetBurungHantuQuota.length>1){        
        for(let i in this.internetPlans.subscriberQuota.subscriberInternetBurungHantuQuota){
          var pack = this.internetPlans.subscriberQuota.subscriberInternetBurungHantuQuota[i];
          this.generateCardUsageWithLimit(pack);
        }
      }else{
        var pack = this.internetPlans.subscriberQuota.subscriberInternetBurungHantuQuota;
        this.generateCardUsageWithLimit(pack);
      }
    }

    generatesubscribedFreeVideoWalla(){
      if(this.internetPlans.subscriberQuota.subscribedFreeVideoWalla.length>1){        
        for(let i in this.internetPlans.subscriberQuota.subscribedFreeVideoWalla){
          var pack = this.internetPlans.subscriberQuota.subscribedFreeVideoWalla[i];
          this.generateCardUsageWithLimit(pack);
        }
      }else{
        var pack = this.internetPlans.subscriberQuota.subscribedFreeVideoWalla;
        this.generateCardUsageWithLimit(pack);
      }
    }

    generateTripleBonusQuotaList(){
      if(this.internetPlans.subscriberQuota.TripleBonusQuotaList.TripleBonusQuota.length>1){        
        for(let i in this.internetPlans.subscriberQuota.TripleBonusQuotaList.TripleBonusQuota){
          var pack = this.internetPlans.subscriberQuota.TripleBonusQuotaList.TripleBonusQuota[i];
          this.generateCardUsageWithLimit(pack);
        }
      }else{
        var pack = this.internetPlans.subscriberQuota.TripleBonusQuotaList.TripleBonusQuota;
        this.generateCardUsageWithLimit(pack);
      }
    }

    generateKadCeriaUsageDetail(){
      if(this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage){
        if(this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage.length>1){        
          for(let i in this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage){
            var pack = this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage[i];          
            var KadCeriaData = {
              "title":'Kad Ceria',
              "subtitle":'Enjoy 10% extra credit with every RM10 and above reload',
              "date":this.formatDate(pack.KadCeriaSMSExpiryDate),
              "class":"kad-icon icon",
              "cardType":"Card Description #1"
            }
            this.cardArray.push(KadCeriaData);
          }
        }else{
          var pack = this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage;
          var KadCeriaData = {
            "title":'Kad Ceria',
            "subtitle":'Enjoy 10% extra credit with every RM10 and above reload',
            "date":this.formatDate(pack.KadCeriaSMSExpiryDate),
            "class":"kad-icon icon",
            "cardType":"Card Description #1"
          }
          this.cardArray.push(KadCeriaData);
        }
      }
      if(this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage){
        if(this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage.length>1){        
          for(let i in this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage){
            var pack = this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage[i];          
            var KadCeriaData = {
              "title":'Kad Ceria',
              "subtitle":'Enjoy 10% extra credit with every RM10 and above reload',
              "date":this.formatDate(pack.KadCeriaSMSExpiryDate),
              "class":"kad-icon icon",
              "cardType":"Card Description #1"
            }
            this.cardArray.push(KadCeriaData);
          }
        }else{
          var pack = this.internetPlans.subscriberQuota.KadCeriaUsageDetail.KadCeriaSMSUsage;
          var KadCeriaData = {
            "title":'Kad Ceria',
            "subtitle":'Enjoy 10% extra credit with every RM10 and above reload',
            "date":this.formatDate(pack.KadCeriaSMSExpiryDate),
            "class":"kad-icon icon",
            "cardType":"Card Description #1"
          }
          this.cardArray.push(KadCeriaData);
        }
      }
    }

    generateCardUsageWithLimit(pack){
      var flag = pack.AUTORENEWALFLAG;      
      let AUTORENEWALFLAG:boolean;
      if(flag=="0"){
      AUTORENEWALFLAG =true;
      }
      let className:any;
      if(pack.BUSINESSNAME.indexOf("Unlimited")>-1 || pack.BUSINESSNAME.indexOf("UNLIMITED")>-1){
        this.GenerateUnlimited(pack);
      }else{
        if(pack.PACKNAME.indexOf("FACEBOOK")>-1 || pack.BUSINESSNAME.indexOf("FACEBOOK")>-1){
          className = "facebook-icon icon";
        }else if(pack.PACKNAME.indexOf("INSTAGRAM")>-1 || pack.BUSINESSNAME.indexOf("BUSINESSNAME")>-1){
          className = "icon_instagram icon";
        }else if(pack.PACKNAME.indexOf("Youtube")>-1 || pack.PACKNAME.indexOf("YOUTUBE")>-1 || pack.BUSINESSNAME.indexOf("Youtube")>-1 || pack.BUSINESSNAME.indexOf("YOUTUBE")>-1){
          className = "youtube-icon icon"
        }else if(pack.PACKNAME.indexOf("NIGHT")>-1 || pack.BUSINESSNAME.indexOf("NIGHT")>-1){
          className = "icon_late-night icon"
        }else{
          className = "icon_add-ons icon";
        }
      var internetData = {
        "title":pack.PACKNAME,
        "subtitle":pack.BUSINESSNAME,
        "dataUsed":this.formatGBValue(pack.QUOTAUSAGE_GIGABYTES),
        "dataTotal":this.formatGBValue(pack.TOTALQUOTAVALUE_GIGABYTES),
        "dataBalance":this.formatGBValue(pack.QUOTABALANCE_GIGABYTES),
        "date":this.formatDate(pack.EXPIRYDATE),
        "unit":'GB',
        "autoRenew":AUTORENEWALFLAG,
        "productId":pack.PRODUCT_ID,
        "class":className,
        "cardType":"Card Toggle/CTA #1"
      }
      this.cardArray.push(internetData);
    }
    }

    GenerateUnlimited(pack){
      var flag = pack.AUTORENEWALFLAG;      
      let AUTORENEWALFLAG:boolean;
      if(flag=="0"){
        AUTORENEWALFLAG =true;
      }
      let className:any;
      if(pack.PACKNAME.indexOf("FACEBOOK")>-1 || pack.BUSINESSNAME.indexOf("FACEBOOK")>-1){
        className = "facebook-icon icon";
      }else if(pack.PACKNAME.indexOf("INSTAGRAM")>-1 || pack.BUSINESSNAME.indexOf("BUSINESSNAME")>-1){
        className = "icon_instagram icon";
      }else if(pack.PACKNAME.indexOf("Youtube")>-1 || pack.PACKNAME.indexOf("YOUTUBE")>-1 || pack.BUSINESSNAME.indexOf("Youtube")>-1 || pack.BUSINESSNAME.indexOf("YOUTUBE")>-1){
        className = "youtube-icon icon"
      }else if(pack.PACKNAME.indexOf("NIGHT")>-1 || pack.BUSINESSNAME.indexOf("NIGHT")>-1){
        className = "icon_late-night icon"
      }else{
        className = "icon_add-ons icon";
      }
      var internetBaseData = {
        "title":pack.PACKNAME,
        "subtitle":pack.BUSINESSNAME,
        "date":this.formatDate(pack.EXPIRYDATE),
        "cardType":"Card Description+ Toggle #1",
        "class":className,
        "autoRenew":AUTORENEWALFLAG,
        "productId":pack.PRODUCT_ID
      }
      this.cardArray.push(internetBaseData);
    }

    createUnlimitedPlan(pack){
      if(pack.PACKTYPE=='Unlimited'){
        var internetBaseData = {
          "title":pack.BUSINESSNAME,
          "subtitle":pack.PACKTYPE,
          "date":'Valid till '+this.formatDate(pack.EXPIRYDATE),
          "cardType":"Card Description #1",
          "class":"icon_internet icon",
          "productId":pack.PRODUCT_ID
        }
          this.cardArray.push(internetBaseData);
        }else{
          this.generateCardUsageWithLimit(pack);
        }
      }

    generateGBShareCards1(){
      var isCBS = this.gatewayService.GetCBSValue();
      this.IsAdmin = this.GbshareUpdate();
      //alert(isCBS);
      if(isCBS==true){                
        var units = this.dashboardService.GetUnits();
        //alert("Customer is CBS"+JSON.stringify(units));
        if(units){
          try{
            if(units.FreeUnitItem.length>1){
              for(var obj in units.FreeUnitItem){
                //alert(obj);
                //alert(units.FreeUnitItem[objis].FreeUnitTypeName);
                if(units.FreeUnitItem[obj].FreeUnitTypeName== 'C_MI_Data_Onetime'){
                  var date = this.formatGBShareDate(units.FreeUnitItem[obj].FreeUnitItemDetail.ExpireTime);
                  var dataUsedVal = units.FreeUnitItem[obj].TotalInitialAmount-units.FreeUnitItem[obj].TotalUnusedAmount;
                  var dataTotal = units.FreeUnitItem[obj].TotalInitialAmount;
                  var dataBalance = dataTotal-dataUsedVal;
                  var internetData = {
                    "title":"GBshare Internet Quota Add-on",
                    "subtitle":"Individual Quota",
                    "dataUsed":dataUsedVal,
                    "dataTotal":dataTotal,
                    "dataBalance":dataBalance,
                    "date":date,
                    "unit":'GB',
                    "autoRenew":false,
                    "class":"share-icon icon",
                    "cardType":"Group Sharing Data #1"
                  }
                  this.cardArray.push(internetData);
                }
                var date = this.formatGBShareDate(units.FreeUnitItem[obj].FreeUnitItemDetail.ExpireTime);
                if(units.FreeUnitItem[obj].FreeUnitTypeName=="C_MI_Data_Monthly"){
                  if(units.FreeUnitItem[obj].MemberUsageList){
                    if(units.FreeUnitItem[obj].MemberUsageList.length>1){
                      
                      for(let i in units.FreeUnitItem[obj].MemberUsageList){
                        if(this.mobileNumber==('0'+units.FreeUnitItem[obj].MemberUsageList[i].PrimaryIdentity)){
                          let dataUsedVal:any = units.FreeUnitItem[obj].MemberUsageList[i].UsedAmount;
                          var dataTotal = units.FreeUnitItem[obj].MemberUsageList[i].ShareLimit;
                          var dataBalance = dataTotal-dataUsedVal;
                          var internetBaseData = {
                            "title":"GBshare",
                            "subtitle":"Allocated Sharing",
                            "dataUsed":dataUsedVal,
                            "dataTotal":dataTotal,
                            "dataBalance":dataBalance,
                            "date":date,
                            "unit":'GB',
                            "autoRenew":0,
                            "class":"share-icon icon",
                            "cardType":"Group Sharing Data #1"
                          }
                          this.cardArray.push(internetBaseData);
                        }
                      }                 
                      }else{
                        let dataUsedVal:any = units.FreeUnitItem[obj].MemberUsageList.UsedAmount;
                        var dataTotal = units.FreeUnitItem[obj].MemberUsageList.ShareLimit;
                        var dataBalance = dataTotal-dataUsedVal;
                        var internetBaseData = {
                          "title":"GBshare",
                          "subtitle":"Allocated Sharing",
                          "dataUsed":dataUsedVal,
                          "dataTotal":dataTotal,
                          "dataBalance":dataBalance,
                          "date":date,
                          "unit":'GB',
                          "autoRenew":0,
                          "class":"share-icon icon",
                          "cardType":"Group Sharing Data #1"
                        }
                        this.cardArray.push(internetBaseData);
                      }
                    }else{
                      var GBShare = {
                        "title":"GBshare",
                        "subtitle":"Allocated Sharing",
                        "dataUsed":0,
                        "dataTotal":0,
                        "dataBalance":0,
                        "date":date,
                        "unit":'GB',
                        "autoRenew":0,
                        "class":"share-icon icon",
                        "cardType":"Group Sharing Data #1"
                      }
                      this.cardArray.push(GBShare);
                    }  
                  }       
              }
            
            }else{
              var date = this.formatGBShareDate(units.FreeUnitItem.FreeUnitItemDetail.ExpireTime);
            }
            }catch(e){
              var date = this.formatGBShareDate(units.FreeUnitItem.FreeUnitItemDetail.ExpireTime);
            }       
    }
  }
}

generateGBShareCards(){
  var isCBS = this.gatewayService.GetCBSValue();
  //alert(isCBS);
  if(isCBS==true){                
    var units = this.dashboardService.GetUnits();
    //alert("Customer is CBS"+JSON.stringify(units));
    if(units){   
    try{
        if(units.FreeUnitItem.FreeUnitItemDetail.length>1){
          //alert("units.FreeUnitItem.FreeUnitItemDetail.ExpireTime"+units.FreeUnitItem.FreeUnitItemDetail[0].ExpireTime);
            var date = this.formatGBShareDate(units.FreeUnitItem.FreeUnitItemDetail[0].ExpireTime);
          }else{
            var date = this.formatGBShareDate(units.FreeUnitItem.FreeUnitItemDetail.ExpireTime);
          }
          if(units.FreeUnitItem.MemberUsageList){
          if(units.FreeUnitItem.MemberUsageList.length>1){
            
            for(let i in units.FreeUnitItem.MemberUsageList){
              if(this.mobileNumber==('0'+units.FreeUnitItem.MemberUsageList[i].PrimaryIdentity)){
                let dataUsedVal:any = units.FreeUnitItem.MemberUsageList[i].UsedAmount;
                var dataTotal = units.FreeUnitItem.MemberUsageList[i].ShareLimit;
                var dataBalance = dataTotal-dataUsedVal;
                var internetBaseData = {
                  "title":"GBshare",
                  "subtitle":"Allocated Sharing",
                  "dataUsed":dataUsedVal,
                  "dataTotal":dataTotal,
                  "dataBalance":dataBalance,
                  "date":date,
                  "unit":'GB',
                  "autoRenew":0,
                  "class":"share-icon icon",
                  "cardType":"Group Sharing Data #1"
                }
                this.cardArray.push(internetBaseData);
              }
            }                 
            }else{
              let dataUsedVal:any = units.FreeUnitItem.MemberUsageList.UsedAmount;
              var dataTotal = units.FreeUnitItem.MemberUsageList.ShareLimit;
              var dataBalance = dataTotal-dataUsedVal;
              var internetBaseData = {
                "title":"GBshare",
                "subtitle":"Allocated Sharing",
                "dataUsed":dataUsedVal,
                "dataTotal":dataTotal,
                "dataBalance":dataBalance,
                "date":date,
                "unit":'GB',
                "autoRenew":0,
                "class":"share-icon icon",
                "cardType":"Group Sharing Data #1"
              }
              this.cardArray.push(internetBaseData);
            }
          }else{
            var GBShare = {
              "title":"GBshare",
              "subtitle":"Allocated Sharing",
              "dataUsed":0,
              "dataTotal":0,
              "dataBalance":0,
              "date":date,
              "unit":'GB',
              "autoRenew":0,
              "class":"share-icon icon",
              "cardType":"Group Sharing Data #1"
            }
            this.cardArray.push(GBShare);
          }         
        }catch(e){
          this.generateGBShareCards1();
        }
    //alert("date"+date);
      }
    }
}

    generatePPUInternetPrepaidCard(){
      try{
        this.PPUPrepaid = this.detailedUsageService.GetDetailedUsagePrepaid();
        var PPUInternet = this.PPUPrepaid.DataCreditUsageDetails;
        let total: any = parseFloat(PPUInternet.TotalPayPerUseData);        
        var internetPPUData = {
          "title":"Pay Per Use",
          "subtitle":"",
          "data":[
            {
            "key":"Usage since",
            "value":this.usageSince
            },
            {
            "key":"Total usage",
            "value":parseFloat(PPUInternet.TotalPayPerUseData).toFixed(2)
            }
            // {
            // "key":"Total Pay Per Use Data Fund",
            // "value":parseFloat(PPUInternet.TotalPayPerUseDataFund).toFixed(2)
            // }
          ],
          "TotalDataUsage":parseFloat(PPUInternet.TotalAllDataInternetFund).toFixed(2),
          "class":"icon_pay-per-use icon",
          "cardType":"Card PPU #1"
        }
        this.cardArray.push(internetPPUData);
      }catch(e){
        console.log(e);
      }
    }

    functiontofindIndexByKeyValue(arraytosearch, key, valuetosearch) {
      console.log("Inside functiontofindIndexByKeyValue");
      console.log("Inside arraytosearch:"+arraytosearch);
      console.log("Inside key:"+key);
      console.log("Inside valuetosearch:"+valuetosearch);
      for (var i = 0; i < arraytosearch.length; i++) {
              if (arraytosearch[i][key] == valuetosearch) {
                return i;
              }
            }
          return null;
        }

    onChange(obj){
      console.log("Obj info:"+JSON.stringify(obj));
     
      var array = this.cardArray;
      console.log("Inside this.cardArray: "+JSON.stringify(array));
      
      var newindex = this.functiontofindIndexByKeyValue(array, "autoRenew", false);
      console.log("newindex:"+newindex);
      debugger;
      this.gatewayService.FetchStopAutoRenew(obj.productId).then((responseFetchStopAutoRenew) => {
        //alert(JSON.stringify(responseFetchStopAutoRenew));
        let resp:any = responseFetchStopAutoRenew
        if(resp.STOP_RENEWAL_Response.ResponseMessage=="Success"){
			      debugger;
            console.log("Success Response:"+resp.STOP_RENEWAL_Response.ResponseMessage);
            this.cardArray[newindex].autoRenew= false;
         }else{
			      debugger;
            console.log("Other Response:"+resp.STOP_RENEWAL_Response.ErrorDesc);
            this.cardArray[newindex].autoRenew = true;          
        }
		        this.detailedUsageService.SetcardsDetails(this.cardArray);
      }).catch(err => {
        //alert(JSON.stringify(err));
      });
    }

    goToSubscriptions(value){   
      this.events.publish("goToRoamingTab",value);
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad DetailedUsageInternetPage');
    }
    cardTitleChange(titleString){
      try{
        return titleString.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
     }catch(e){
        return "";
      }
    }

}
