
/**
 * @author Capgemini 
 */

/**
 * @Module Name - utilService
 * @Description - Singleton class for webservices, This is common gateway for all fetch/post calls 
 */

export class DashboardService {
userInfo;
postpaidBillingInfo;
allDataPostpaidPrepaid;
overAllPrepaidData;
balancePrepaidData;
creditBalance;
creditDate;
roamStatus;
roamStatusPre;
freeUnits;
talkPostpoaid;
isWeeneknd:boolean = false;
isReloadDashboardCalled:boolean = false;
isKadCeria:boolean = false;
usageSince:any;

SetUserInfo(obj){
  this.userInfo = obj;
}

GetUserInfo(){
  return this.userInfo.Envelope.Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput;
}

SetCreditBalance(obj){
  this.creditBalance = obj;
}

GetCreditBalance(){
  return this.creditBalance;
}

SetCreditDate(obj){
  this.creditDate = obj;
}

GetCreditDate(){
  return this.creditDate;
}

SetPostpaidBillingInfo(obj){
  this.postpaidBillingInfo = obj;
}

GetPostpaidBillingInfo(){
  return this.postpaidBillingInfo.Envelope.Body.FirstAppUsageResponse.ResponseBody;
}

SetAllDataPostpaidPrepaid(obj){
  this.allDataPostpaidPrepaid = obj;
}

GetAllDataPostpaidPrepaid(){
  return this.allDataPostpaidPrepaid.MAX_StatusResponse;
}

SetOverAllPrepaidData(obj){
  this.overAllPrepaidData = obj;
}

GetOverAllPrepaidData(){
  return this.overAllPrepaidData.Envelope.Body;
}

SetUsageSince(val){
  this.usageSince = val;
}

GetUsageSince(){
  return this.usageSince;
}

SetBalancePrepaidData(obj){
  this.balancePrepaidData = obj;
      try{
      var productList = this.balancePrepaidData.Envelope.Body.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.SubscriberInfo.Product;
      //alert(JSON.stringify(productList));
      if(productList.length>1){
        for(let obj in productList){
          //alert("productList[obj].Id"+productList[obj].Id);
          if(productList[obj].Id=='40838'){
            this.isKadCeria = true;
          }
        }
      }else{
        if(productList.Id=='40838')
        this.isKadCeria = true;
      }
    }catch(e){
      console.log("error"+e);
    }
}

IsKADCeria(){
  return this.isKadCeria;
}

GetBalancePrepaidData(){
  return this.balancePrepaidData.Envelope.Body.IntegrationEnquiryResultMsg.IntegrationEnquiryResult;
}

SetRoamStatus(obj){
  this.roamStatus = obj;
}

GetRoamStatus(){
  try{
    return this.roamStatus.MAX_StatusResponse.subscriberQuota.subscriberInternetRoamingQuota.ACTIVEPASSLIST;
  }catch(e){
    return "";
  }
}

SetRoamStatusPre(obj){
  this.roamStatusPre = obj;
}

GetRoamStatusPre(){
  try{
    return this.roamStatusPre.MAX_StatusResponse.subscriberQuota.subscriberInternetRoamingQuota.ACTIVEPASSLIST;
  }catch(e){
    return "";
  }
}

SetUnits(obj){
  this.freeUnits = obj;
}

GetUnits(){
  return this.freeUnits.Envelope.Body.processResponse.QueryFreeUnitResult;
}

SetTalkPostpaid(obj){
  this.talkPostpoaid = obj;
}

GetTalkPostpaid(){
  var talkdetails;
  try{
    talkdetails = this.talkPostpoaid.Envelope.Body.IntegrationEnquiryResponse.ResponseBody.TalkDetails.TalkDetails;
    return talkdetails;
  }catch(e){
    return "";
  }
}

GetSMSPostpaid(){
  var smsDetails;
  try{
    smsDetails = this.talkPostpoaid.Envelope.Body.IntegrationEnquiryResponse.ResponseBody.SMSDetails.SMSDetails;
    return smsDetails;
  }catch(e){
    return "";
  }
}

SetIsWeekend(val){
  this.isWeeneknd = val;
}

GetIsWeekend(){
  return this.isWeeneknd;
}

SetIsReloadDashboardCalled(flag){
  this.isReloadDashboardCalled = flag;
}

GetIsReloadDashboardCalled(){
  return this.isReloadDashboardCalled;
}

Initialize(){
  this.isKadCeria = false;
}

}

