import { Component, NgZone,ChangeDetectorRef, Renderer2 } from '@angular/core';
import { NavController, LoadingController, NavParams } from 'ionic-angular';
import { DetailedUsagePage } from "../detailed-usage/detailed-usage";
import { GatewayService } from '../../global/utilService';
import { GbShareService } from '../../global/gbshareServices';
import { LocalStorage } from '../../global/localStorage';
import { ConstantData } from '../../global/constantService';
import { DashboardService } from './homeService';
import { DetailedUsageService } from '../detailed-usage/detailedUsageService';
import { ServicesPage } from "../services/services";
import { ReloadPage } from "../reload/reload";
import { PayBillPage } from "../pay-bill/pay-bill";
import { PrivilegesPage } from "../privileges/privileges";
import {GlobalVars} from "../../providers/globalVars";
import {PlanDetailsPage} from "../plan-details/plan-details";
import { SubscriptionsPage } from '../../pages/subscriptions/subscriptions';
import {SubscriptionsTabService } from '../../pages/subscriptions/subscriptionsTabService';
import { GbSharePage } from "../gb-share/gb-share";
import { GbShareAllocatePage } from '../gb-share-allocate/gb-share-allocate';
import { Events } from "ionic-angular";
import { SupportPage } from '../../pages/support/support';

import _ from 'underscore';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  billAmount:any;
  billDate2:any;
  billingCycle:any;
  dataUsed:number;
  dataTotal:number;
  dataUsedVal:any = '0.00';
  dataQuota:any;
  validityDate:any = '';
  dataTitle:string = "No Internet Plan";
  dataUnit:string = '';
  dataUsedUnit:string = '';
  talkUsedVal: any = 0;
  talkTotal: number = 0;
  talkPostpaid:any;
  talkDesc:any;
  talkDate:any;
  talkQuata:any;
  talkUnit:any = 'Minutes';
  talktotalUnit:any = 'Minutes';
  talkTitle:string = "Call";
  billDate:any;
  reloadDate:any;

  smsUsedVal: any = '0.00';
  smsTotal: number = null;
  smsDesc:any;
  smsDate:any;
  smsQuata:any = 0;
  smsUnit:any;
  smsTotalUnit:any;
  smsTitle:string = "Roaming SMS Charges";

  romTalkUsedVal: any = '0.00';
  romTalkTotal: number = 0;
  romTalkDesc:any;
  romTalkDate:any;
  romTalkQuata:any = 0;
  romTalklUnit:any = "Minutes";
  romTotalTalkUnit:any = "Minutes";
  romTalkTitle:string = "Roaming Call Charges";

  romUsedVal: any = '0.00';
  romTotal: number = null;
  romDesc:any;
  romDate:any;
  romQuata:number = 0;
  romUnit:any = "GB";
  romTotalUnit:any = "GB";
  romTitle:string = "No Roaming Internet Plan";

  screenWidth:number;
  screenHeight:number;
  gaugeWidth:number;
  gaugeHeight:number;
  gaugeRadius:number;
  gaugeRadius10:number;
  drawerOptions: any;
  response:any;
  userInfo:any={};
  capZoneValue: any;
  callbarredValue: any;

  postpaidBillingInfo:any={};
  balanceSummary:any={};
  HLRProfileDetails:any={};
  BADetailsRetrieve:any={};
  callBarDetails:any={};
  slideIndex: number = 0;

  prepaidOverallInternet:any={};
  prepaidCurrentBalance:any={};
  isPrepaid:boolean;
  dataPrepaidPostpaid:any={};

  roamingDetails:any={};
  smsDetails:any={};
  talkDetails:any={};
  prepaidData:any={};

  dataValue: any;
  talkValue: any;
  smsValue: any;
  RomValue: any;
  RomTalkValue: any;
  overlayHidden: boolean = true;
  planName: any;
  planDetails:any;
  capZoneActive:any = this.globalVar.getCapZoneStatus();
  callBarredActive:any = this.globalVar.getcallBarredStatus();

  freeUnits:any;
  loader:any;
  mobileNumber:any;

  usageSince:any;
  adminNumber:boolean = false;
  gbChoose:any;
  GroupId:any;

  reloadTriggered: boolean = false;
  capZoneValue1: any;
  callbarredValue1: any;
  dataValue1: any;
  talkValue1: any;
  smsValue1: any;
  RomValue1: any;
  RomTalkValue1: any;
  VLR:boolean = false;
  refresh:boolean = false;
  IsAdmin:boolean = false;
  zIndex: any;

  constructor(public navCtrl: NavController, public gatewayService:GatewayService, 
    public localStorage:LocalStorage, public loadingCtrl: LoadingController, 
    public dashboardService:DashboardService, public detailedUsageService: DetailedUsageService, 
    public globalVar: GlobalVars, public navParams: NavParams, 
    public subscriptionsTabService:SubscriptionsTabService, public zone: NgZone, 
    public events: Events,public gbshareService:GbShareService, private ref: ChangeDetectorRef,
    private renderer: Renderer2 ) {

    console.log('constructor called');
    //alert('constructor called');
    this.loader = this.gatewayService.LoaderService();
    this.usageSince = this.gatewayService.getUsageSince();
    this.gatewayService.setOnChangeCalled(true);  
    this.refreshData();
    gatewayService.registerObserver(this);
    events.subscribe('GBShareStatusUpdate',()=>{
      console.log("**Event*");
        this.GbshareUpdate();
      
})
    this.zIndex = document.getElementsByClassName('my-header') as HTMLCollectionOf<HTMLElement>;
    //GBShareStatusUpdate
  }
   ionViewDidLoad() {
    
    console.log("ionViewDidLoad");
    this.adminNumber = false;
    console.log("ionViewDidEnter");
    var userInfo = this.dashboardService.GetUserInfo();
    var adminNumber = userInfo["ListOfServices"].Services.BillingType;
    this.GroupId  = userInfo["ListOfServices"].Services.SmeGroupId;
    if(adminNumber == "Billable"){
      if(this.GroupId){
        this.adminNumber = true;
       // this.GbShareRedirection(GroupId);    
      }
    }
  }
  GbshareUpdate(){
    this.adminNumber = false;
    console.log("ionViewDidEnter");
    var userInfo = this.dashboardService.GetUserInfo();
    var adminNumber = userInfo["ListOfServices"].Services.BillingType;
    this.GroupId  = userInfo["ListOfServices"].Services.SmeGroupId;
    if(adminNumber == "Billable"){
      if(this.GroupId){
        this.adminNumber = true;
       // this.GbShareRedirection(GroupId);    
      }
    }
    return this.adminNumber;
  }
  ionViewWillEnter(){
    var isReloadDashboard = this.gatewayService.getFlagForDashboardReload();
    console.log('isReloadDashboard flag' + isReloadDashboard);
    //alert('isReloadDashboard flag' + isReloadDashboard);
    var _that = this;
    //isReloadDashboard = true;
    if (isReloadDashboard) {
      //alert("if");
      this.refreshDashboardData('');
    }
    // else{
    //   alert('else');
    //   this.refreshData();
    // }
  }
  ionViewDidEnter(){
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Home", this.renderer);
  }
  GbShareRedirection(GroupId){        
        var params = {
          "GroupID":GroupId //665811549 //014831735
        }  
        var loader = this.gatewayService.LoaderService();
        this.loader.present();
        this.gbshareService.GBShareStatus(params).then((res)=> {
        this.loader.dismiss();   
          console.log("*****GBShareStatus*******"+JSON.stringify(res));
        
        if(res["Envelope"].Body.PurchaseProductCBSResponse.Response.ResultCode == "Operation successfully."){
             if(res["Envelope"].Body.PurchaseProductCBSResponse.PurchaseProductCBSResult.ModifyOffering.OfferingID.OfferingID == "1"){
              
              this.gbChoose = "Group";
              this.navCtrl.setRoot(GbSharePage);
             }else if(res["Envelope"].Body.PurchaseProductCBSResponse.PurchaseProductCBSResult.ModifyOffering.OfferingID.OfferingID == "0"){
              //this.nav.setRoot(GbShareAllocatePage);
              this.gbChoose = "Allocation";
              this.navCtrl.setRoot(GbShareAllocatePage);
            }else{
              this.gbChoose =""
              //this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");              
            }
        }else{
          this.gbChoose="";
         // this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
        }
        
         }).catch(err => {
          this.loader.dismiss();  
          console.log("*****reload*******"+JSON.stringify(err));
          if(err=="No internet connection")
          return;
        //  this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
          // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
        });
       }

  doRefresh(e,fab1,fab){
    this.refresh = true;
    setTimeout(() => {
      console.log('Async operation has ended');
      e.complete();
    }, 6000);
    fab1.close();
    fab.close();
	this.hideOverlay(event,true);
    this.refreshDashboardData(e);    
  }
  refreshDashboardData(e) {
    try {
      //alert("calling reload dashboard again as flag is false");
      var _that = this;
      //var mobile_number = { mobilenum: _that.selectedNumber };
      //_that.jsonstoreService.putData(mobile_number, "mobilenumber");
      //_that.gatewayService.SetMobileNumber(_that.selectedNumber);
      var loader = _that.gatewayService.LoaderService();
       loader.present();
      _that.gatewayService.ReloadDashboard().then((res) => {
        //loader.dismiss();
        var userInfo = _that.dashboardService.GetUserInfo();
        _that.gatewayService.setFlagForDashboardReload(false);
        if(this.refresh){
          e.complete();
          this.refresh = false;
        }
        loader.dismiss();
     //   _that.Plan = userInfo.ListOfServices.Services.PLAN;
      //  _that.setTheme();
        //_that.nav.setRoot(HomePage);
        //alert("notify observer");
       _that.gatewayService.notifyObservers("Dashboard");
      }).catch(err => {
        loader.dismiss();
        //alert("error");
      //  this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
        //alert("Fetch Dashboard Info onChange " + JSON.stringify(err));//FetchAllDataPostpaidPrepaid
      });
    } catch (e) {
      //alert("error1");
     // this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
      //alert("error" + e);
    }


  }
  updateDashboard(){
    // var _that = this;
    // this.loader.dismiss();
    // this.zone.run(() => {
      this.reloadTriggered = !this.reloadTriggered;
      this.refreshData();
   // });
    //this.refreshData();
  }

  update(){

  }
  callingMethod(){

  }

  updateDial(){

  }
  updateLifestyle(){
  }
  refreshReloadDate(){
    //function dates (){
      var today = new Date();
      let dd:any = today.getDate();
      let mm:any = today.getMonth()+1; //January is 0!

      var yyyy = today.getFullYear().toString().substr(-2);
      //alert(yyyy);
      if(dd<10){
          dd='0'+dd;
      }
      if(mm<10){
          mm='0'+mm;
      }
      var setDate = dd+'/'+mm+'/'+yyyy;

      let hours:any = today.getHours();
      let minutes:any = today.getMinutes();
      var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ampm;
      //alert(setDate+' at '+strTime);
      return setDate+' at '+strTime;
      //return setDate+' at '+strTime;
      //}
  }

  refreshData(){
    //this.billAmount= 0;
    this.billDate2 =0;
    //this.billingCycle=null;
    this.dataUsed =0.00;
    this.dataTotal = null;
    this.dataUsedVal = '0.00';
    this.dataQuota='';
    this.validityDate = 'Validity Date : None';
    this.dataTitle="No Internet Plan";
    this.dataUnit='';
    this.dataUsedUnit='GB';
    this.talkUsedVal = 0;
    this.talkTotal= 0;
    this.talkPostpaid = null;
    this.talkDesc = '';
    this.talkDate = null;
    this.talkQuata = 'of';
    this.talkUnit = 'Minutes';
    this.talktotalUnit= 'Minutes';
    this.talkTitle= "Call";
    this.billDate = null;
    this.reloadDate = null;

    this.smsUsedVal = 'RM0.00';
    this.smsTotal= null;
    this.smsDesc = '';
    this.smsDate = 'Validity Date : None';
    this.smsQuata= '';
    this.smsUnit = '';
    this.smsTotalUnit = '';
    this.smsTitle = "Roaming SMS Charges";

    this.romTalkUsedVal= 'RM0.00';
    this.romTalkTotal = null;
    this.romTalkDesc = '';
    this.romTalkDate= 'Validity Date : None';
    this.romTalkQuata = '';
    this.romTalklUnit= '';
    this.romTotalTalkUnit = '';
    this.romTalkTitle = "Roaming Call Charges";

    this.romUsedVal = '0.00';
    this.romTotal = null;
    this.romDesc='';
    this.romDate='Validity Date : None';
    this.romQuata = 0;
    this.romUnit = "GB";
    this.romTotalUnit= "";
    this.romTitle = "No Roaming Internet Plan";

  
    this.response=null;
    this.userInfo={};
    this.capZoneValue = null;
    this.callbarredValue = null;

    this.postpaidBillingInfo={};
    this.balanceSummary={};
    this.HLRProfileDetails={};
    this.BADetailsRetrieve={};
    this.callBarDetails={};
    this.slideIndex = 0;

    this.prepaidOverallInternet={};
    this.prepaidCurrentBalance={};
    this.isPrepaid = false;
    this.dataPrepaidPostpaid={};

    this.roamingDetails={};
    this.smsDetails={};
    this.talkDetails={};
    this.prepaidData={};

    this.dataValue= 0.00;
    this.talkValue= 0;
    this.smsValue= 0;
    this.RomValue= 0.00;
    this.RomTalkValue= 0;
    this.overlayHidden = true;
    this.capZoneActive = this.globalVar.getCapZoneStatus();
    this.callBarredActive = this.globalVar.getcallBarredStatus();

    this.freeUnits = null;
    this.mobileNumber = null;
    this.billAmount = '';
    //this.usageSince = null;
    //var isReloadDashboard = this.gatewayService.getFlagForDashboardReload();
    var _that = this;
    //isReloadDashboard = true;
    // if(isReloadDashboard){
    //   this.gatewayService.ReloadDashboard().then((res)=> {
    //     console.log('data reloaded');
    //     _that.gatewayService.setFlagForDashboardReload(false);


    //   }).catch(err => {
    //       //_that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
    //       //alert("Fetch Dashboard Info "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid

    //   });
    // }
    try{
    this.reloadDate = this.refreshReloadDate();
    this.gatewayService.setBasePlanForSubscription(false);

    this.isPrepaid = this.gatewayService.isPrePaid();
    this.dataUsed = 0.00;
    this.dataUsedVal = '0.00';
    this.dataTotal = null;

    // this.talkUsedVal = 0;// = 450;
    // this.talkTotal = 0;
    // this.talkDate = "";

    //this.smsUsedVal= 0;
    //this.smsTotal= 0;

    // this.romUsedVal= "0.00";
    // this.romTotal= 0.00;

    

    //   this.drawerOptions = {
    //     handleHeight: 50,
    //     thresholdFromBottom: 200,
    //     thresholdFromTop: 200,
    //     bounceBack: true
    // };
    try{
      var str = this.gatewayService.GetVLR();
      if(str!="" || str!=null || str!=undefined) {
        this.VLR = true;
      }else{
        this.VLR = false;
      }      
    }catch(e){
      this.VLR = false;
      console.log("Error "+JSON.stringify(e));
    }

    this.userInfo = this.dashboardService.GetUserInfo();
    this.mobileNumber = this.userInfo.ListOfServices.Services.MobileNumber;
    this.planName = this.userInfo.ListOfServices.Services.PLAN;//ProdPromName;
    this.billDate = this.userInfo.ListOfServices.Services.BillCycle;
    //alert("this.planName"+this.planName);
    if(this.planName.indexOf('Plan')>-1)
    this.planName = this.planName.replace(/ Plan$/, "");
    if(!this.isPrepaid){
      this.planDetails = ConstantData.dialMapping.postpaid[this.planName];
    }else{
      this.planDetails = ConstantData.dialMapping.prepaid[this.planName];
    }
    //alert("this.planName 2"+this.planName);
    //alert(JSON.stringify(this.planDetails));
    if(ConstantData.isDashboardBypassed==false){
      var isCBS = this.gatewayService.GetCBSValue();
      //alert(isCBS);
      if(isCBS==true){
        var mob = this.mobileNumber;
        var isAdmin = this.GbshareUpdate();
        //alert('isadmin'+isAdmin);
        var units = this.dashboardService.GetUnits();
        // this.dataUsedVal = units.TotalIndividualQuota;
        // this.dataTotal = units.TotalGroupQuota;
        if(units){
          try{
              if(units.FreeUnitItem.FreeUnitItemDetail.length>1){
              //alert("units.FreeUnitItem.FreeUnitItemDetail.ExpireTime"+units.FreeUnitItem.FreeUnitItemDetail[0].ExpireTime);
                var date = this.formatGBShareDate(units.FreeUnitItem.FreeUnitItemDetail[0].ExpireTime);
              }else{
                var date = this.formatGBShareDate(units.FreeUnitItem.FreeUnitItemDetail.ExpireTime);
              }             
              // if(isAdmin){
              //   //alert("admin number if");
              //     this.dataTitle = "Internet Balance";
              //     this.dataUsedVal = units.FreeUnitItem.TotalUnusedAmount;
              //     this.dataTotal = units.FreeUnitItem.TotalInitialAmount;
              //     this.validityDate = 'Valid till '+date;
              //     this.dataUnit = 'GB';
              //     console.log("Valid Till"+this.validityDate);
              // }else{
                // if(this.internetPlans.subscriberQuota.TripleBonusQuotaList.TripleBonusQuota.length>1){        
                //   for(let i in this.internetPlans.subscriberQuota.TripleBonusQuotaList.TripleBonusQuota){
                //     var pack = this.internetPlans.subscriberQuota.TripleBonusQuotaList.TripleBonusQuota[i];
                //     this.generateCardUsageWithLimit(pack);
                //   }
                // }else{
                //   var pack = this.internetPlans.subscriberQuota.TripleBonusQuotaList.TripleBonusQuota;
                //   this.generateCardUsageWithLimit(pack);
                // }                
                if(units.FreeUnitItem.MemberUsageList.length>1){
                  for(let i in units.FreeUnitItem.MemberUsageList){
                    if(mob==('0'+units.FreeUnitItem.MemberUsageList[i].PrimaryIdentity)){
                      this.dataTitle = "Internet Balance";
                      this.dataUsedVal = units.FreeUnitItem.MemberUsageList[i].ShareLimit-units.FreeUnitItem.MemberUsageList[i].UsedAmount;
                      this.dataTotal = units.FreeUnitItem.MemberUsageList[i].ShareLimit;
                      this.validityDate = 'Valid till '+date;
                      this.dataUnit = 'GB';
                      this.dataUsedUnit = 'GB';
                    }
                  }                 
                  }else{
                        this.dataTitle = "Internet Balance";
                        this.dataUsedVal = units.FreeUnitItem.MemberUsageList.ShareLimit-units.FreeUnitItem.MemberUsageList.UsedAmount;
                        this.dataTotal = units.FreeUnitItem.MemberUsageList.ShareLimit;
                        this.validityDate = 'Valid till '+date;
                        this.dataUnit = 'GB';
                        this.dataUsedUnit = 'GB';
                  }
              //}
            }catch(e){
              try{
                if(units.FreeUnitItem.length>1){
                  for(var obj in units.FreeUnitItem){                   
                    if(units.FreeUnitItem[obj].FreeUnitTypeName=="C_MI_Data_Monthly"){
                      var date = this.formatGBShareDate(units.FreeUnitItem[obj].FreeUnitItemDetail.ExpireTime);
                      // if(isAdmin){
                      //   //alert("else admin if")
                      //   this.dataTitle = "Internet Balance";
                      //   this.dataUsedVal = units.FreeUnitItem[obj].TotalUnusedAmount;
                      //   this.dataTotal = units.FreeUnitItem[obj].TotalInitialAmount;
                      //   this.validityDate = 'Valid till '+date;
                      //   this.dataUnit = 'GB';                        
                      // }else{ 
                        //alert("else admin else");                       
                        // if(units.FreeUnitItem[obj].MemberUsageList){
                        //   //alert("else admin else inside");
                        //   this.dataTitle = "Internet Balance";
                        //   this.dataUsedVal = units.FreeUnitItem[obj].MemberUsageList.ShareLimit-units.FreeUnitItem[obj].MemberUsageList.UsedAmount;
                        //   this.dataTotal = units.FreeUnitItem[obj].MemberUsageList.ShareLimit;
                        //   this.validityDate = 'Valid till '+date;
                        //   this.dataUnit = 'GB'
                        // }
                        if(units.FreeUnitItem[obj].MemberUsageList.length>1){
                          for(let i in units.FreeUnitItem[obj].MemberUsageList){
                            if(mob==('0'+units.FreeUnitItem[obj].MemberUsageList[i].PrimaryIdentity)){
                              this.dataTitle = "Internet Balance";
                              this.dataUsedVal = units.FreeUnitItem[obj].MemberUsageList[i].ShareLimit-units.FreeUnitItem[obj].MemberUsageList[i].UsedAmount;
                              this.dataTotal = units.FreeUnitItem[obj].MemberUsageList[i].ShareLimit;
                              this.validityDate = 'Valid till '+date;
                              this.dataUnit = 'GB';
                              this.dataUsedUnit = 'GB';
                            }
                          }                 
                          }else{
                              this.dataTitle = "Internet Balance";
                              this.dataUsedVal = units.FreeUnitItem[obj].MemberUsageList.ShareLimit-units.FreeUnitItem[obj].MemberUsageList.UsedAmount;
                              this.dataTotal = units.FreeUnitItem[obj].MemberUsageList.ShareLimit;
                              this.validityDate = 'Valid till '+date;
                              this.dataUnit = 'GB';
                              this.dataUsedUnit = 'GB';
                          }
                      //}
                    }
                  }                             
                }
                }catch(e){
                  //alert("e"+e);
                }     
            }
        
        }
      
      }
      else{
          try{
             this.dataPrepaidPostpaid = this.dashboardService.GetAllDataPostpaidPrepaid();
             if(this.isWeekend() && this.dataPrepaidPostpaid.subscriberQuota.subscriberWeekendInternet){
                  this.dataTitle = "Weekend Internet Balance";//this.dataPrepaidPostpaid.subscriberQuota.subscriberWeekendInternet.PLAN_TYPE;
                  this.dataUsedVal = this.formatGBValue(this.dataPrepaidPostpaid.subscriberQuota.subscriberWeekendInternet.QUOTABALANCE_GIGABYTES);
                  this.dataTotal = this.formatGBValue(this.dataPrepaidPostpaid.subscriberQuota.subscriberWeekendInternet.TOTALQUOTAVALUE_GIGABYTES);
                  var date = this.dataPrepaidPostpaid.subscriberQuota.subscriberWeekendInternet.EXPIRYDATE;
                  this.dataUnit = 'GB';
                  this.dataUsedUnit = 'GB';
                  this.dataQuota = "of";
                  if(this.isPrepaid){
                    if(date!=""){
                      this.validityDate = 'Valid till '+this.formatDate(date);
                    }else{
                      this.validityDate = 'Validity Date : None';
                    }
                  }else{
                    if(date!=""){
                      this.validityDate = 'Valid till '+this.getBillingEndCycle(this.billDate);//this.formatDate(date);
                    }else{
                      this.validityDate = 'Validity Date : None';
                    }
                  }
             }else{
                //alert("this.dataPrepaidPostpaid"+JSON.stringify(this.dataPrepaidPostpaid));
                //this.dataPrepaidPostpaid.subscriberQuota.subscriberBaseQuota = undefined;
                if(this.dataPrepaidPostpaid.subscriberQuota.subscriberBaseQuota=="" || this.dataPrepaidPostpaid.subscriberQuota.subscriberBaseQuota==null || this.dataPrepaidPostpaid.subscriberQuota.subscriberBaseQuota==undefined){
                  this.validityDate = 'Validity Date : None';
                }else{
                  this.gatewayService.setBasePlanForSubscription(true);
                  this.dataTitle = "Internet Balance";this.dataPrepaidPostpaid.subscriberQuota.subscriberBaseQuota.PLAN_TYPE;
                  this.dataUsedVal = this.formatGBValue(this.dataPrepaidPostpaid.subscriberQuota.subscriberBaseQuota.QUOTABALANCE_GIGABYTES);
                  this.dataTotal = this.formatGBValue(this.dataPrepaidPostpaid.subscriberQuota.subscriberBaseQuota.TOTALQUOTAVALUE_GIGABYTES);
                  var date = this.dataPrepaidPostpaid.subscriberQuota.subscriberBaseQuota.EXPIRYDATE;
                  this.dataUnit = 'GB';
                  this.dataUsedUnit = 'GB';
                  this.dataQuota = "of";
                  if(this.isPrepaid){
                    if(date!=""){
                      this.validityDate = 'Valid till '+this.formatDate(date);
                    }else{
                      this.validityDate = 'Validity Date : None';
                    }
                  }else{
                    if(date!=""){
                      this.validityDate = 'Valid till '+this.getBillingEndCycle(this.billDate);//this.formatDate(date);
                    }else{
                      this.validityDate = 'Validity Date : None';
                    }
                  }
                }
            }
          }catch(e){
            //alert("Could not load MAXSTATUS data");
          }
      }
  }
   // this.prepaidOverallInternet = this.dashboardService.GetOverAllPrepaidData();
   // alert("this.prepaidOverallInternet"+JSON.stringify(this.prepaidOverallInternet));
   // this.prepaidCurrentBalance = this.dashboardService.GetBalancePrepaidData();
    this.gatewayService.SetXPAX(this.userInfo.ListOfServices.Services.ServiceType);
    this.gatewayService.notifyObservers("Menu");
    console.log("**************************************************Menu");
    if(ConstantData.isDashboardBypassed==false){
    if(this.isPrepaid){
      try{
            this.prepaidCurrentBalance = this.dashboardService.GetBalancePrepaidData();
            var balanceRecord = this.prepaidCurrentBalance.BalanceRecordList.BalanceRecord;
            if(balanceRecord.length>1){
              for(let obj in balanceRecord){
                if(balanceRecord[obj].AccountType==2000){
                  var amount = (balanceRecord[obj].Balance/10000).toString();
                  this.billAmount = parseFloat(amount).toFixed(2);
                  //this.billingCycle = this.formatDate(balanceRecord[obj].ExpireTime);
                }
                if(balanceRecord[obj].AccountType=="2572"){
                  this.detailedUsageService.SetKadCeria(balanceRecord[obj]);
                }
              }
            }else{
              var amount = (this.prepaidCurrentBalance.BalanceRecordList.BalanceRecord.Balance/10000).toString();
              this.billAmount = parseFloat(amount).toFixed(2);
            // this.billingCycle = this.formatDate(this.prepaidCurrentBalance.BalanceRecordList.BalanceRecord.ExpireTime);
            }
      //console.log(JSON.stringify(this.prepaidCurrentBalance));
              console.log('date is '+ JSON.stringify(this.prepaidCurrentBalance.SubscriberState.ActiveStop));
              if (this.prepaidCurrentBalance.SubscriberState.ActiveStop.CDATA != undefined) {
                var tempExp = this.prepaidCurrentBalance.SubscriberState.ActiveStop.CDATA;
              } else {
                var tempExp = this.prepaidCurrentBalance.SubscriberState.ActiveStop;
              }
            console.log(tempExp);
            //this.dashboardService.SetCreditDate(tempExp);
            this.billingCycle = this.formatDate(tempExp);
            }catch(e){
              //alert("Error in billing service");
            }
            //this.billingCycle = this.getxpaxDate(this.prepaidCurrentBalance.BillingCycleDate.BillCycleEndDate.CDATA);            
            this.handleTalkPrepaid();
            this.handleRoamingPostpaid();            
    }else{
      try{
            this.handleTalkPostpaid();
            this.handleRoamingPostpaid();
            this.postpaidBillingInfo = this.dashboardService.GetPostpaidBillingInfo();
            //alert("bill info"+JSON.stringify(this.dashboardService.GetPostpaidBillingInfo()));
            this.balanceSummary = this.postpaidBillingInfo.BalanceSummaryDetails;
            //alert(JSON.stringify(this.balanceSummary));
            //alert(this.balanceSummary.CmuBalanceSummaryVbc.DueNow);
            this.HLRProfileDetails = this.postpaidBillingInfo.HLRProfileDetails;
            this.BADetailsRetrieve = this.postpaidBillingInfo.BADetailsRetrieve;
            this.callBarDetails = this.postpaidBillingInfo.CallBarDetails;


            this.billAmount = parseFloat(this.balanceSummary.CmuBalanceSummaryVbc.LatestAmountDue).toFixed(2);
            //alert(this.billAmount);
            this.billingCycle = this.formatDate(this.balanceSummary.CmuBalanceSummaryVbc.DueDate)//this.getBillingCycle(this.userInfo.ListOfServices.Services.BillCycle);

        //}        
        if(_that.gatewayService.getOnChangeCalled()){
          console.log("**************************************************notify called");
          //_that.gatewayService.notifyObservers("Dial");
          //_that.gatewayService.setOnChangeCalled(false);
        }
    }catch(e){
        console.log("***************************** Issue with services"+e);
      }
    }}
    console.log("UserInfo "+JSON.stringify(this.userInfo));
      
  }catch(e){
    //alert("refresh dashboard "+e);
  }

  if(!this.reloadTriggered){

    this.capZoneValue = {
      dataUsed: 20,
      dataUsedVal: 20,
      dataTotal: 20,
      title:'INTERNET BALANCE',
      message:'',
      gaugeID:'gauge-container0',
      dataUsedUnit:'GB',
      qouta:'of',
      desc:'',
      dataTotalUnit:'GB',
      tabNumber:'0',
      capZone:true
    }


  this.callbarredValue = {
    dataUsed: 20,
    dataUsedVal: 20,
    dataTotal: 20,
    title:'INTERNET BALANCE',
    message:'',
    gaugeID:'gauge-container00',
    dataUsedUnit:'GB',
    qouta:'of',
    desc:'',
    dataTotalUnit:'GB',
    tabNumber:'0',
    callbarred:true
  }
    this.dataValue = {
    dataUsed: 0.00,
    dataUsedVal: this.dataUsedVal,//15.8888,
    dataTotal: this.dataTotal,//20.34567,
    title:this.dataTitle,
    message:this.validityDate,
    gaugeID:'gauge-container1',
    dataUsedUnit:this.dataUsedUnit,
    qouta:this.dataQuota,
    desc:'',
    dataTotalUnit:this.dataUnit,
    tabNumber:'0'
  }
  
  this.talkValue = {
    dataUsed: 0,
    dataUsedVal: this.talkUsedVal,//22,
    dataTotal: this.talkTotal,
    title:this.talkTitle,
    message:this.talkDate,
    gaugeID:'gauge-container2',
    dataUsedUnit:this.talkUnit,
    qouta:this.talkQuata,
    desc:this.talkDesc,
    dataTotalUnit:this.talktotalUnit,
    tabNumber:'1'
  }
  this.smsValue = {
    dataUsed: 0.00,
    dataUsedVal: this.smsUsedVal,//150,
    dataTotal: this.smsTotal,//200,
    title:this.smsTitle,
    message:this.smsDate,
    gaugeID:'gauge-container3',
    dataUsedUnit:this.smsUnit,
    qouta:this.smsQuata,
    desc:this.smsDesc,
    dataTotalUnit:this.smsTotalUnit,
    tabNumber:'3'
  }

  this.RomValue = {
    dataUsed: 0.00,
    dataUsedVal: this.romUsedVal,//250,
    dataTotal: this.romTotal,//500,
    title:this.romTitle,
    message:this.romDate,
    gaugeID:'gauge-container4',
    dataUsedUnit:this.romUnit,
    qouta:this.romQuata,
    desc:this.romDesc,
    dataTotalUnit:this.romTotalUnit,
    tabNumber:'3'
  }

  this.RomTalkValue = {
    dataUsed: 0.00,
    dataUsedVal: this.romTalkUsedVal,//250,
    dataTotal: this.romTalkTotal,//500,
    title:this.romTalkTitle,
    message:this.romTalkDate,
    gaugeID:'gauge-container5',
    dataUsedUnit:this.romTalklUnit,
    qouta:this.romTalkQuata,
    desc:this.romTalkDesc,
    dataTotalUnit:this.romTotalTalkUnit,
    tabNumber:'3'
  }
}else{
    this.capZoneValue1 = {
      dataUsed: 20,
      dataUsedVal: 20,
      dataTotal: 20,
      title:'INTERNET BALANCE',
      message:'',
      gaugeID:'gauge-container01',
      dataUsedUnit:'GB',
      qouta:'of',
      desc:'',
      dataTotalUnit:'GB',
      tabNumber:'0',
      capZone:true
    }


    this.callbarredValue1 = {
      dataUsed: 20,
      dataUsedVal: 20,
      dataTotal: 20,
      title:'INTERNET BALANCE',
      message:'',
      gaugeID:'gauge-container001',
      dataUsedUnit:'GB',
      qouta:'of',
      desc:'',
      dataTotalUnit:'GB',
      tabNumber:'0',
      callbarred:true
    }
      this.dataValue1 = {
      dataUsed: 0.00,
      dataUsedVal: this.dataUsedVal,//15.8888,
      dataTotal: this.dataTotal,//20.34567,
      title:this.dataTitle,
      message:this.validityDate,
      gaugeID:'gauge-container11',
      dataUsedUnit:this.dataUsedUnit,
      qouta:this.dataQuota,
      desc:'',
      dataTotalUnit:this.dataUnit,
      tabNumber:'0'
    }

    this.talkValue1 = {
      dataUsed: 0,
      dataUsedVal: this.talkUsedVal,//22,
      dataTotal: this.talkTotal,
      title:this.talkTitle,
      message:this.talkDate,
      gaugeID:'gauge-container21',
      dataUsedUnit:this.talkUnit,
      qouta:this.talkQuata,
      dataTotalUnit:this.talktotalUnit,
      desc:this.talkDesc,
      tabNumber:'1'
    }
    this.smsValue1 = {
      dataUsed: 0.00,
      dataUsedVal: this.smsUsedVal,//150,
      dataTotal: this.smsTotal,//200,
      title:this.smsTitle,
      message:this.smsDate,
      gaugeID:'gauge-container3',
      dataUsedUnit:this.smsUnit,
      qouta:this.smsQuata,
      dataTotalUnit:this.smsTotalUnit,
      desc:this.smsDesc,
      tabNumber:'3'
    }
  
    this.RomValue1 = {
      dataUsed: 0.00,
      dataUsedVal: this.romUsedVal,//250,
      dataTotal: this.romTotal,//500,
      title:this.romTitle,
      message:this.romDate,
      gaugeID:'gauge-container4',
      dataUsedUnit:this.romUnit,
      qouta:this.romQuata,
      dataTotalUnit:this.romTotalUnit,
      desc:this.romDesc,
      tabNumber:'3'
    }
  
    this.RomTalkValue1 = {
      dataUsed: 0.00,
      dataUsedVal: this.romTalkUsedVal,//250,
      dataTotal: this.romTalkTotal,//500,
      title:this.romTalkTitle,
      message:this.romTalkDate,
      gaugeID:'gauge-container5',
      dataUsedUnit:this.romTalklUnit,
      qouta:this.romTalkQuata,
      dataTotalUnit:this.romTotalTalkUnit,
      desc:this.romTalkDesc,
      tabNumber:'3'
    }
  }

  this.drawerOptions = {
    handleHeight: 43,
    thresholdFromBottom: 200,
    thresholdFromTop: 200,
    bounceBack: true
  };
   this.events.publish("dashboardRefreshed",this.dataValue1);
   
  }

  handleTalkPostpaid(){
    console.log("In handle talk");
    try{
    this.talkPostpaid = this.dashboardService.GetTalkPostpaid();
    if(this.talkPostpaid){
    var balance = this.talkPostpaid.Balance;//this.planDetails["Call"]["Dial"];
    var total = this.talkPostpaid.TotalTalk;
    if(balance=="Unlimited" || total=="Unlimited" || total=="NA" || total=="NaN"){
      this.talkUsedVal = null,
      this.talkTotal = null,
      this.talkDesc = "Unlimited",
      this.talkTitle = "CALL BALANCE"
      this.talkDate = 'Valid till '+this.getBillingDate(this.billDate);
      this.talkQuata = 'Calls to all network';
      this.talkUnit = '';
      this.talktotalUnit = '';
    }else{
      //alert("Limited Talk"+this.talkPostpaid.Balance);
        try{
          this.talkUsedVal = this.talkPostpaid.Balance;
          this.talkTotal = this.talkPostpaid.TotalTalk;
          this.talkTitle = "CALL";
          //this.talkDesc = "of";
          if(this.talkPostpaid.ExpireTime){
            this.talkDate = 'Valid till '+this.formatDate(this.talkPostpaid.ExpireTime)//this.getBillingDate(this.billDate);
          }else{
            this.talkDate = 'Validity Date : None';
          }
          this.talkQuata = 'of';
          this.talkUnit = 'Minutes';
          this.talktotalUnit = 'Minutes';
        }catch(e){
          //alert("in catch"+e);
          this.talkUsedVal = 0;
          this.talkTotal = 0;
          this.talkTitle = "CALL";
          this.talkQuata = 'of';
          this.talkDesc = "Calls to all network";
          // if(this.talkPostpaid.ExpireTime){
          //   this.talkDate = 'valid till '+this.formatDate(this.talkPostpaid.ExpireTime)//this.getBillingDate(this.billDate);
          // }else{
            this.talkDate = 'Validity Date : None';
          //}
          this.talkUnit = 'Minutes';
          this.talktotalUnit = 'Minutes';
        }
    }
    // else{
    //   //this.roamingDetails = this.detailedUsageService.GetDetailedusageRoamingPostpaid();
    //   //alert("this.talkDetails"+JSON.stringify(this.talkDetails));
    //   this.talkDetails = this.detailedUsageService.GetDetailedusageTalkPostpaid();
    //   //alert("this.talkDetails"+JSON.stringify(this.talkDetails));
    //   //var formattedRoamingObj = _.object(_.pluck(this.roamingDetails, 'ParameterName'), _.pluck(this.roamingDetails, 'ParameterValue'));
    //   var formattedTalkObj = _.object(_.pluck(this.talkDetails, 'ParameterName'), _.pluck(this.talkDetails, 'ParameterValue'));
    //   this.talkUsedVal = parseFloat(formattedTalkObj["TotalUsage"]).toFixed(2);
    //   this.talkDesc = "Within network";
    //   this.talkQuata = "Within all network";
    //   this.talkTitle = "TOTAL CALL CHARGES";
    //   this.talkDate = 'Since '+this.getBillingDate(this.billDate);
    //   this.talkTotal = null;    
    //   this.talkUnit = '';
    //   this.talktotalUnit = '';
    // }
  }else{
    //alert("its not available");
    this.talkUsedVal = 0;
    this.talkTotal = 0;
    this.talkTitle = "CALL";
    this.talkQuata = 'of';
    this.talkDesc = "Calls to all network";
    // if(this.talkPostpaid.ExpireTime){
    //   this.talkDate = 'valid till '+this.formatDate(this.talkPostpaid.ExpireTime)//this.getBillingDate(this.billDate);
    // }else{
      this.talkDate = 'Validity Date : None';
    //}
    this.talkUnit = 'Minutes';
    this.talktotalUnit = 'Minutes';
  }
  }catch(e){
    //alert(e);
  }
  }

  formatValue(val,val1){    
    var str = val.substring(0, val.length-2);
    if(str.indexOf('0.0')>-1){
      str = val1.substring(0, val1.length-2);
    }
    return str;
  }

  formatUnit(val,val1){
    var unit = 'GB';    
    var str = val.substring(0, val.length-2);
    if(str.indexOf('0.0')>-1){
      unit = 'MB';
    }
    return unit;
  }

  handleRoamingPostpaid(){    
    var roamingPass;
    if(!this.isPrepaid){
      roamingPass = this.dashboardService.GetRoamStatus();
      this.roamingDetails = this.detailedUsageService.GetDetailedusageRoamingPostpaid();
      this.talkDetails = this.detailedUsageService.GetDetailedusageTalkPostpaid();
      this.smsDetails = this.detailedUsageService.GetDetailedusageSMSPostpaid();
    }else{      
      roamingPass = this.dashboardService.GetRoamStatusPre();
    }   
      
      if(roamingPass==""|| roamingPass==null || roamingPass==undefined)
      return;
      var data = [];
      var nos = roamingPass["PASS"];
      if(roamingPass["PASS"].length>1)
      data = roamingPass["PASS"];
      else
      data.push(roamingPass["PASS"]);
      //alert(JSON.stringify(data));
      for(var obj in data){
        var title = data[obj]["PASS_NAME"]; 
        //alert(title);  
      if(title=='7 DAY 3 IN 1 PASS'){        
          this.romUsedVal = this.formatMBValue(data[obj]["BALANCE"]);
          this.romTotal = this.formatMBValue(data[obj]["QUOTA"]);
          this.romTitle = "Roaming Internaet Balance";
          this.romUnit = this.getUnitData(data[obj]["BALANCE"]);
          this.romTotalUnit = this.getUnitData(data[obj]["QUOTA"]);
          this.romDate = this.formatDate(data[obj]["PASS_VALIDUNTILL"]);

          this.romTalkUsedVal = null,
          this.romTalkTotal = null,
          this.romTalkDesc = "Calls to all network",
          this.romDesc = "Unlimited",
          this.romTalkTitle = "Roaming Call Balance"
          this.romTalkDate = 'Valid till '+this.romDate;
          this.romTalkQuata = '';
          this.romTalklUnit = '';
          this.romTotalTalkUnit = '';

          this.smsUsedVal = null,
          this.smsTotal = null,
          this.smsDesc = "Calls to all network",
          this.smsTitle = "Roaming SMS Balance"
          this.smsDate = 'Valid till '+this.romDate;
          this.smsQuata = '';
          this.smsUnit = '';
          this.smsDesc = 'Unlimited';
          this.smsTotalUnit = '';
       }else if(title=="1 DAY CALLS AND SMS PASS"){
          this.romTalkUsedVal = null,
          this.romTalkTotal = null,
          this.romTalkDesc = "Calls to all network",
          this.romTalkTitle = "Roaming Call Balance"
          this.romTalkDate = 'Valid till '+this.romDate;
          this.romTalkQuata = '';
          this.romTalklUnit = '';
          this.romTalkDesc = 'Unlimited';
          this.romTotalTalkUnit = '';

          this.smsUsedVal = null,
          this.smsTotal = null,
          this.smsDesc = "Calls to all network",
          this.smsTitle = "Roaming SMS Balance"
          this.smsDate = 'Valid till '+this.romDate;
          this.smsQuata = '';
          this.smsUnit = '';
          this.smsDesc = 'Unlimited';
          this.smsTotalUnit = '';
      }
      if(title=="1-Day SOCIAL ROAM PASS"){

        var dataPre = [];
        var socialPass = data[obj]["PACK_LIST"]["PACK"];
        if(socialPass.length>1)
          dataPre = socialPass;
        else
          dataPre.push(socialPass);
          for(var obj in dataPre){
            if(dataPre[obj].BUSINESSNAME == 'SOCIAL MEDIA'){            

            this.romUsedVal = this.formatGBValue(dataPre[obj]["QUOTABALANCE_GIGABYTES"]);
            this.romTotal = this.formatMBValue(dataPre[obj]["TOTALQUOTAVALUE_GIGABYTES"]);
            this.romTitle = "Roaming Internaet Balance";
            this.romUnit = this.getUnitData(dataPre[obj]["QUOTABALANCE_GIGABYTES"]);
            this.romTotalUnit = this.getUnitData(dataPre[obj]["TOTALQUOTAVALUE_GIGABYTES"]);
            this.romDate = this.formatDate(dataPre[obj]["PASS_VALIDUNTILL"]);
           
          }
          }          
      }
      }
    //this.romTalklUnit:any;
    //this.romTotalTalkUnit:any;
    
    //var formattedRoamingObj = _.object(_.pluck(this.roamingDetails, 'ParameterName'), _.pluck(this.roamingDetails, 'ParameterValue'));
    // var formattedTalkObj = _.object(_.pluck(this.talkDetails, 'ParameterName'), _.pluck(this.talkDetails, 'ParameterValue'));
    // this.romTalkUsedVal = formattedTalkObj["TotalUsage"];
    // this.romTalkTotal = formattedTalkObj["Allnet"];
    // //this.talkDesc = "Within network";
    // this.romTalkTitle = "ROAMING CALLS CHARGES";
    // this.romTalkDate = 'Since '+this.getBillingDate(this.billDate);
    // var formattedSMSObj = _.object(_.pluck(this.smsDetails, 'ParameterName'), _.pluck(this.smsDetails, 'ParameterValue'));
    // //alert("formattedSMSObj"+JSON.stringify(formattedSMSObj));
    // this.smsUsedVal = formattedSMSObj["SMSCount"];
    // this.smsTotal = formattedSMSObj["TotalCharge"];
    // this.smsTitle = "ROAMING SMS CHARGES";
  }

  handleTalkPrepaid(){
    try{
      var detailedUsageTalk = this.detailedUsageService.GetDetailedUsagePrepaid();
      var talkPPU = detailedUsageTalk.TalkCreditUsageDetails;
      var offnet:any = parseFloat(talkPPU.TotalTalkCreditUsage)-parseFloat(talkPPU.TotalOnNetCallUsage);
      let total: any = parseFloat(talkPPU.TotalOnNetCallUsage)+parseFloat(offnet)+parseFloat(talkPPU.TotalIDDCallUsage);
      this.talkUsedVal = "RM"+total.toFixed(2);
      this.talkQuata = "Calls to all network";
      this.talkTitle = "TOTAL CALL CHARGES";
      this.talkDate = 'Since '+this.usageSince;
      this.talkTotal = null;    
      this.talkUnit = '';
      this.talktotalUnit = '';
    }catch(e){
      //alert("in talk "+e);
    }
  }

  handleRoamingPrepaid(){
    //this.prepaidData =  this.detailedUsageService.GetDetailedUsagePrepaid();
    //alert(JSON.stringify(this.prepaidData));
  }

  formatGBValue(val){
    var formattedStr = val.substr(0,val.indexOf(' '));
    if(formattedStr.indexOf('.')>-1){
      formattedStr = parseFloat(formattedStr).toFixed(2)
    }else{
      formattedStr = Number(formattedStr);
    }
    return formattedStr;
  }

  formatMBValue(val){
    var str = val.substring(0, val.length-2);
    return str;
  }

  getUnitData(val){
    var unit = val.slice(-2);
    return unit;
  }

  formatGBShareDate(val){
    if(val){
    var str = val.substr(0,10);
    return str;
    }else{
      return '';
    }
  }

  getBillingCycle(day){
    //alert(day)
    //let day :any = 8
    let dt = new Date();
    let dayOfMonth = dt.getDate();

    if(dayOfMonth < day){
      if(dt.getMonth()!=0){
        dt.setMonth((dt.getMonth()-1)%12);
      }else{
        dt.setMonth(11);
        dt.setFullYear(dt.getFullYear()-1);
      }
    }

    let dateStrStart = this.addLeadingZeroIfNeeded(day)+'/'+this.addLeadingZeroIfNeeded((dt.getMonth()+1))+'/'+dt.getFullYear();
    //let newDate = new Date();

    var nextMonth = dt.getMonth()+2;

    if(nextMonth>=11){
      dt.setFullYear(dt.getFullYear()+1);
    }

    dt.setMonth((dt.getMonth()+2)%12);
    var month = dt.getMonth();
    if(month==0)
    month = 12;

    let dateStrEnd = this.addLeadingZeroIfNeeded(day)+'/'+this.addLeadingZeroIfNeeded(month)+'/'+dt.getFullYear();
    this.billDate2 = dateStrEnd;
    let dateStr = dateStrStart+' - '+dateStrEnd;
    this.dashboardService.SetUsageSince(dateStrStart);
    return dateStr;

    // var day = 8;
    // var d = new Date();
    // var m = d.getMonth()+1;
    // var y = d.getFullYear();
    // var curretdate = day+'/'+m+'/'+y;
    // alert(curretdate);

    // var now = new Date();

    // var day = 20
    // var current;
    // if (now.getMonth() == 11) {
    //     current = new Date(now.getFullYear() + 1, now.getMonth() + 1, day-1);
    // } else {
    //     current = new Date(now.getFullYear(), now.getMonth() + 1, day-1);
    // }
    // var monthnumber = parseInt(current.getMonth())+1;
    // var newMonth =  current.getDate() +'/'+ monthnumber +'/'+ current.getFullYear();

    //  alert(newMonth);
  }

  getBillingEndCycle(day){
    //alert(day)
    //let day :any = 8
    let dt = new Date();
    let dayOfMonth = dt.getDate();

    if(dayOfMonth < day){
      if(dt.getMonth()!=0){
        dt.setMonth((dt.getMonth()-1)%12);
      }else{
        dt.setMonth(11);
        dt.setFullYear(dt.getFullYear()-1);
      }
    }
    let dateStrStart = this.addLeadingZeroIfNeeded(day)+'/'+this.addLeadingZeroIfNeeded((dt.getMonth()+1))+'/'+dt.getFullYear();
    //let newDate = new Date();
	var nextMonth = dt.getMonth()+2;

    if(nextMonth>=11){
      dt.setFullYear(dt.getFullYear()+1);
    }
    dt.setMonth((dt.getMonth()+2)%12);
    var month = dt.getMonth();
   
    if(month==0)
    month = 12

    
    let dateStrEnd = this.addLeadingZeroIfNeeded(day-1)+'/'+this.addLeadingZeroIfNeeded(month)+'/'+dt.getFullYear();

    return dateStrEnd;
  }

  getBillingDate(day){
    let dt = new Date();
    var year = dt.getFullYear().toString();
    let dateStrStart = this.addLeadingZeroIfNeeded(day)+'/'+this.addLeadingZeroIfNeeded((dt.getMonth()+1))+'/'+year.substring(2, 4);

    return dateStrStart;
  }

  usageSinceDate(){
    var dt = new Date();
    dt.setDate(dt.getDate() + 1);
    dt.setMonth(dt.getMonth()-1);
    var day = dt.getDate();
    var year = dt.getFullYear().toString();

    let dateStrStart = this.addLeadingZeroIfNeeded(day)+'/'+this.addLeadingZeroIfNeeded((dt.getMonth()+1))+'/'+year.substring(2, 4);
    return dateStrStart;
  }

  isWeekend(){
    var dt = new Date();
    var day = dt.getDay();
    if(day==0 || day ==6){
    this.dashboardService.SetIsWeekend(true);
    return true;
    }
    else{
    this.dashboardService.SetIsWeekend(false);
    return false;
    }
  }

  // getBillingStartDate(){

  // }
  addLeadingZeroIfNeeded(dateNumber) {
    if (String(dateNumber).length === 1) {
        return '0' + String(dateNumber);
    }

    return String(dateNumber);
  }

  formatDate(val){
    if(val==""|| val==null || val==undefined)
    return;
    var dt = val.substring(6, 8)+'/'+val.substring(4, 6)+'/'+val.substring(2, 4);
    return dt;
  }

  getxpaxDate(day){
      // let formattedDate;
      // let year = str.substr(0,4);
      // let month = str.substr(4,6);
      // let day = str.substr(6,8);

      // formattedDate = day+'/'+month+'/'+year;
      // return formattedDate;

     // var day = "20171116";
      var year = day.match(/.{1,4}/g);
      var date = year[1].match(/.{1,2}/g);
      var finaldate = date[1] +'/'+date[0] +'/'+year[0]
      return finaldate;
  }

  DataAlignmentTalk(str){
    var day = str.slice(0, 8) ;
    var year = day.match(/.{1,4}/g);
    var date = year[1].match(/.{1,2}/g);
    var finalDate = date[1]+'/'+date[0]+'/'+year[0];
    return finalDate;
    }

  viewDetailsPage(selectedTab) {
    var loader = this.gatewayService.LoaderService();
    loader.present();
    this.navCtrl.push(DetailedUsagePage);
    loader.dismiss();
  }
  // goToSubInternet(){
  //   this.navCtrl.setRoot(SubInternetPage);
  // }
  // goToAddOns(){
  //   this.navCtrl.setRoot(AddOnsPage);
  // }
  // goToRoaming(){
  //   this.navCtrl.setRoot(RoamingPage);
  // }
  goToSubscriptions(value,feb1){
    this.subscriptionsTabService.SetTabInfo(value);
    // this.navCtrl.setRoot(SubscriptionsPage);
    feb1.close();
    this.hideOverlay(event);
    this.navCtrl.push(SubscriptionsPage);
  }
  
  gotoService(){
    // this.navCtrl.setRoot(ServicesPage);

    this.navCtrl.push(ServicesPage);    
  }
  
  gotoGBShare(){
    console.log("**GroupID"+this.GroupId);
    this.GbShareRedirection(this.GroupId);    
    //this.navCtrl.push(GbSharePage);
  }

  gotoSupportPage(){
    // this.navCtrl.setRoot(SupportPage);
    
    this.navCtrl.push(SupportPage);    
  }

  gotoReload(fab1,event){
    // this.navCtrl.setRoot(ReloadPage);
    console.log("fab1 ",fab1);
    if(fab1 == 'none') {
      console.log("fab1 ",fab1);
    }
    else {
      fab1.close();
      this.hideOverlay(event);
    }

    this.navCtrl.push(ReloadPage);    
  }

  gotoPaybill(){
    // this.navCtrl.setRoot(PayBillPage);

    this.navCtrl.push(PayBillPage);    
  }

  gotoPriviledges(){
    //this.navCtrl.push(PrivilegesPage);
    // this.navCtrl.setRoot(PrivilegesPage);

    this.navCtrl.push(PrivilegesPage);    
  }
  
  public hideOverlay(event,forcehide?,forceshow?) {
    
    event.stopPropagation();
    this.overlayHidden = !this.overlayHidden;
    if(forcehide){
      this.overlayHidden = true;
    }
    if(forceshow){
      this.overlayHidden = false;
    }
    if(!this.overlayHidden){
      document.getElementsByTagName('notification-drawer')[0].className='zindex9';
      if(navigator.platform == "iPhone")
        this.zIndex[0].style.zIndex = '1';
      else
        this.zIndex[0].style = 'z-index: 1';
    }
    else{
      document.getElementsByTagName('notification-drawer')[0].className='';
      if(navigator.platform == "iPhone")
        this.zIndex[0].style.zIndex = '';
      else
        this.zIndex[0].style = '';
    }
    this.ref.detectChanges(); 
  }

  goToPlanDetails() {
    this.navCtrl.push(PlanDetailsPage);
  }
  gotoPrivileges(){
    // this.navCtrl.setRoot(PrivilegesPage);

    this.navCtrl.push(PrivilegesPage);    
  }


}



