import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { GatewayService } from '../../../../global/utilService';

/**
 * Generated class for the ContactUsSocialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-contact-us-social',
  templateUrl: 'contact-us-social.html',
})
export class ContactUsSocialPage {

  isPrePaid:any;
  fbUrl:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public theInAppBrowser:InAppBrowser, public gatewayService:GatewayService, 
    private renderer: Renderer2) {
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Follow us", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactUsSocialPage');
  }

  openFacebookPage(){
     this.isPrePaid= this.gatewayService.isPrePaid();
     
     if(this.isPrePaid){
       this.fbUrl= "https://www.facebook.com/xpaxfb/";
     }else{
       this.fbUrl = "https://www.facebook.com/celcom/";
     }
       const browser = this.theInAppBrowser.create(this.fbUrl,"location=no");
        browser.show();
  }

  openInstagramPage(){
    const browser = this.theInAppBrowser.create("https://www.instagram.com/celcom/","location=no");
     browser.show();
  }

  openYoutubePage(){
     const browser = this.theInAppBrowser.create("https://www.youtube.com/user/CelcomChannel","location=no");
     browser.show();
  }

  openTwitterPage(){
      const browser = this.theInAppBrowser.create("https://twitter.com/celcom/","location=no");
     browser.show();
  }

}
