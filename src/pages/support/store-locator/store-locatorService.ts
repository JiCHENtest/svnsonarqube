import { Injectable } from '@angular/core';
import { GatewayService } from '../../../global/utilService';
import { ConstantData } from '../../../global/constantService';


@Injectable()
export class StoreLocatorService {
    constructor(public gatewayService: GatewayService) {
    };

    getStoreData() {
        return new Promise((resolve, reject) => {
            this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetStoreLocatorData, "POST", {}).then((res) => {
                return resolve(res);
            }).catch(e => {
                console.log("component  failure" + e);
                reject("Store not found.");
            });
        });
    }
}   