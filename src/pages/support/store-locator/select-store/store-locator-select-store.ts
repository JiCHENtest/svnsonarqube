import {Component, Renderer2} from '@angular/core';
import {ModalController, NavController, NavParams, LoadingController} from 'ionic-angular';
import {StoreLocatorStoreFilterPage} from "../store-filter/store-locator-store-filter";
import {StoreLocatorStoreDetailsPage} from "../store-details/store-locator-store-details";

import { StoreLocatorService } from '../store-locatorService';
import { GatewayService } from '../../../../global/utilService'; 

@Component({
  selector: 'page-store-locator-select-store',
  templateUrl: 'store-locator-select-store.html',
})
export class StoreLocatorSelectStorePage {

  storeRegion: any=[{value:'',label:'All Regions'},
                    {value:'Central',label:'Central'},
                    {value:'Eastern',label:'Eastern'},
                    {value:'Northern',label:'Northern'},
                    {value:'Sabah',label:'Sabah'},
                    {value:'Sarawak',label:'Sarawak'},
                    {value:'Southern',label:'Southern'}
                    ];

    selected_filter: any = {
        region:[""],
        is_open_24hr:[""],
        store_type:[""]
    };

    storeArray: any = [{}];

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController, 
    public navParams: NavParams,
    public storeLocatorService: StoreLocatorService,
    public loadingCtrl: LoadingController,
    private renderer: Renderer2,
    public gatewayService: GatewayService) {

  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Store locator", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StoreLocatorSelectStorePage');
    this.getStoreLocatorData();
  }

  getStoreLocatorData(){
    var loader = this.loadingCtrl.create({
      content: "Loading",
      duration: 500
    });
    loader.present().then(() => {
      this.storeLocatorService.getStoreData().then((res) => {
        this.storeArray = res;
        loader.dismiss();
      }, (res) => {
        console.log("storelocator -> getStoreLocatorData -> res");
        loader.dismiss();
      });
    }); 
  }

  openStoreLocatorFilterModal () {
    const modalOptions:any = {
      showBackdrop: false,
      enableBackdropDismiss: true
    }
    const  modal = this.modalCtrl.create(StoreLocatorStoreFilterPage, {"selectedFilter":this.selected_filter}, modalOptions );
    modal.onDidDismiss(data => {
         this.selected_filter.is_open_24hr = data.is_open_24hr;
         this.selected_filter.store_type = data.store_type;
         console.log(data);
       });
    modal.present();

  }

  openStoreDetail(store) {
    this.navCtrl.push(StoreLocatorStoreDetailsPage,{storeInfo:store});
  }
}
