import { Pipe,PipeTransform } from '@angular/core';

@Pipe({
    name: 'StoreFilter',
    pure: false
})

export class StoreFilterPipe implements PipeTransform {
    transform(stores: any[], store_filter: any): any {

        for (let key in store_filter) {
            let value = store_filter[key];
            if(value.length>1 && value.indexOf("")!=-1){
                value.splice(value.indexOf(""),1);
            }
           stores = stores.filter(store => new RegExp(value.join('|')).test(store[key]));
        }
        return stores;

    }
}