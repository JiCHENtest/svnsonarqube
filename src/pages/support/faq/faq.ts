import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {FaqCategoryPage} from "./faq-category/faq-category";
import { GatewayService } from '../../../global/utilService';
/**
 * Generated class for the FaqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {
isPrepaid:boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public gatewayService:GatewayService, private renderer: Renderer2) {
  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("Faq", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqPage');
  }
  ionViewWillEnter(){
    this.isPrepaid = this.gatewayService.isPrePaid();
  }

  goToFaqCategory(category){
    this.navCtrl.push( FaqCategoryPage,{category: category} );
  }

}
