import { Component, Renderer2 } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GatewayService } from '../../../global/utilService';
import { PUKService } from './pukService';

@Component({
  selector: 'page-puk',
  templateUrl: 'puk.html',
})
export class PukPage {

  pukNumber: any;
  mobileNumber: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public gatewayService: GatewayService, public pukService:PUKService, private renderer: Renderer2) {

  }

  ionViewDidEnter() {
    this.gatewayService.resetAllAnalyticData();
    this.gatewayService.setAnalyticPageNameProfileSegment("PUK inquiry", this.renderer);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PukPage');
    this.mobileNumber = this.gatewayService.GetMobileNumber();
    this.pukService.fetchPUKDetails().then((res)=>{
      console.log("PUK enquiry adaptor response: "+res);
      this.pukNumber = res;
    });
  }

}
