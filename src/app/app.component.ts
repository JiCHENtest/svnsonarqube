import { Component, ViewChild, Renderer2, NgZone } from '@angular/core';
import { Nav, Platform, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AlertController, LoadingController } from 'ionic-angular';
import { Keyboard } from "@ionic-native/keyboard";

import { HomePage } from '../pages/home/home';
import { TransactionHistoryPage } from '../pages/transaction-history/transaction-history';
import { SubscriptionsPage } from '../pages/subscriptions/subscriptions';
import { ServicesPage } from '../pages/services/services';
import { PrivilegesPage } from '../pages/privileges/privileges';
import { SupportPage } from '../pages/support/support';
import { SettingsPage } from "../pages/settings/settings";
import { MobileConnectPage } from '../pages/login/mobile-connect/mobile-connect';
import { PaymentRecieptPage } from "../pages/payment-reciept/payment-reciept";
import { LogoutPage } from "../pages/logout/logout";
import { GlobalVars } from "../providers/globalVars";
import { NetworkLoginPage } from "../pages/login/network-login/network-login";
import { SplashScreen2Page } from "../pages/splash-screen2/splash-screen2";
import { GatewayService } from '../global/utilService';
import { ReloadService } from '../global/reloadServices';

import { GbShareService } from '../global/gbshareServices';

import { JsonstoreService } from '../global/jsonstore';
import { LocalStorage } from '../global/localStorage';
import { ConstantData } from '../global/constantService';
import { AlertService } from '../global/alert-service';
import { HandleError } from '../global/handleerror';


import { DashboardService } from '../pages/home/homeService';
import { DetailedUsageService } from '../pages/detailed-usage/detailedUsageService';
import { Sim } from '@ionic-native/sim';
import { ReloadPage } from "../pages/reload/reload";
import { PayBillPage } from "../pages/pay-bill/pay-bill";
import { GbSharePage } from "../pages/gb-share/gb-share";
import { GbShareAllocatePage } from '../pages/gb-share-allocate/gb-share-allocate';
import { AppMinimize } from '@ionic-native/app-minimize';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { PayBillService } from '../pages/pay-bill/payBillService';
import { Events } from "ionic-angular";
// service
import { SubscriptionsTabService } from '../pages/subscriptions/subscriptionsTabService';
// Addition style guide tabs
import { StyleGuidePage } from '../pages/style-guide/style-guide';
import { SplashscreenPage } from '../pages/splashscreen/splashscreen';

declare var WLAuthorizationManager;
declare var MFPPush;
declare var WL;

@Component({
  templateUrl: 'app.html'
})
export class celcom {

  @ViewChild(Nav) nav: Nav;


  rootPage: any;


  opts: any;

  //to activate XPAX Theme  you must update XPAXTheme boolean to true
  XPAXTheme: boolean = this.globalVar.getXPAXTheme();
  pages: Array<{ title: string, component: any, iclass: string }>;
  AuthHandler: any;
  isUserLoggedIn: any;
  isFirstTimeUser: any;
  isMFPInited: Boolean = false;
  userInfo: any = {};
  isPrepaid: boolean;
  networkStatus: any;
  PrepaidOrPostpaid: any;
  MobileNumber: any = "";
  currentPageClass;
  mobileList: any;
  selectedNumber;
  count: number = 0;
  Plan;
  ProdPlanName:any;
  subMobileList: string[] = [];
  backButtonHold: boolean = true;
  adminNumber: boolean = false;
  gbChoose: string;
  flgforUpdate: boolean = false;
  fetchMenuDetails: Boolean = false;
  validNumber: boolean = false;
  changeCalled: boolean = false;
  updateCount:number = 0;
  
  // selectOptions: any;
  constructor(private keyboard: Keyboard, public modalCtrl: ModalController, private openNativeSettings: OpenNativeSettings, public platform: Platform, public alertCtrl: AlertController, private alertCtrlInstance: AlertService, private renderer: Renderer2, public statusBar: StatusBar, public splashScreen: SplashScreen, public gatewayService: GatewayService, public jsonstoreService: JsonstoreService, public handleerror: HandleError, public localStorage: LocalStorage, private sim: Sim, public loadingCtrl: LoadingController, public dashboardService: DashboardService,public DTService: DetailedUsageService, public globalVar: GlobalVars, public zone: NgZone, private appMinimize: AppMinimize, public gbshareService: GbShareService, public reloadService: ReloadService, public paybillService: PayBillService, public subscriptionsTabService: SubscriptionsTabService, public events: Events) {
    //this.currentPageClass = this;

    gatewayService.registerObserver(this);
    this.isUserLoggedIn = this.localStorage.get("IsUserLoggedIn", "false")
    console.log('isUserLoggedIn' + this.isUserLoggedIn);
    this.isFirstTimeUser = this.localStorage.get("IsFirstTimeUser", "true");
    console.log('isFirstTimeUser' + this.isFirstTimeUser);
    this.networkStatus = this.gatewayService.GetNetworkStatus();
    this.PrepaidOrPostpaid = this.localStorage.get("PrepaidOrPostpaid", "Postpaid");
    // this.MobileNumber = this.localStorage.get("MobileNum","");
    this.mobileList = [this.MobileNumber];


    this.localStorage.set("tempCount", 0);
    this.count = this.localStorage.get("tempCount", 0);
   
    console.log("Count ", this.count)
    if (this.isMFPInited == false && this.count == 0) {

      console.log("Count in ", this.count)
     
      renderer.listen('document', 'mfpjsonjsloaded', () => {
        console.log("mfp is loaded and renderer is called");
        //alert("before"+this.isMFPInited);
        this.zone.run(() => {
          console.log("mfp is loaded and renderer is called 2")
          this.isMFPInited = true;
        });

        //alert("after"+this.isMFPInited);
        if (this.count == 0) {
          this.localStorage.set("tempCount", 1);
          this.count = this.localStorage.get("tempCount", 0);
          console.log("Count in ++ ", this.count)
          this.initializeApp();
        }
        else {
          console.log("Count in else ++ ", this.count, this.isMFPInited)
        }
      })
    }
    else {
      console.log("isMFPInited in else ++ ", this.isMFPInited)
    }


   
  }
  GbShareRedirection(GroupId) {


    var params = {
      "GroupID": GroupId //665811549 //014831735
    }
    //var loader = this.gatewayService.LoaderService();
    //this.loader.present();
    this.gbshareService.GBShareStatus(params).then((res) => {
      //this.loader.dismiss();   
      console.log("*****GBShareStatus*******" + JSON.stringify(res));

      if (res["Envelope"].Body.PurchaseProductCBSResponse.Response.ResultCode == "Operation successfully.") {
        if (res["Envelope"].Body.PurchaseProductCBSResponse.PurchaseProductCBSResult.ModifyOffering.OfferingID.OfferingID == "1") {
          //this.nav.setRoot(GbSharePage);
          this.gbChoose = "Group";
        } else if (res["Envelope"].Body.PurchaseProductCBSResponse.PurchaseProductCBSResult.ModifyOffering.OfferingID.OfferingID == "0") {
          //this.nav.setRoot(GbShareAllocatePage);
          this.gbChoose = "Allocation";
        } else {
          this.gbChoose = ""
          //this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");              
        }
      } else {
        this.gbChoose = "";
        // this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
      }

    }).catch(err => {
     
      //this.loader.dismiss();  
      console.log("*****reload*******" + JSON.stringify(err));
      if(err=="No internet connection")
      return;
      //  this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
      // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    });
  }
  registerDevice() {
    WLAuthorizationManager.obtainAccessToken("push.mobileclient").then(
      MFPPush.registerDevice(
        null,
        function (successResponse) {
          console.log("Successfully registered");
          console.log("Register device info:" + JSON.stringify(successResponse));
          //alert("Register device info:" + JSON.stringify(successResponse));
        },
        function (failureResponse) {
          console.log("Failed to register");
          console.log("Failed to register device:" + JSON.stringify(failureResponse));
          //alert("Failed to register device:" + JSON.stringify(failureResponse));
        }
      )
    );
  }

  //   registerPush() {
  //     //WLAuthorizationManager.obtainAccessToken("push.mobileclient").then(
  //      // window.MFPClientDefer = angular.injector(['ng']).get('$q').defer();;
  //       //window.wlCommonInit = window.MFPClientDefer.resolve;
  //       //window.MFPClientDefer.promise.then(function wlCommonInit() {
  //           // Common initialization code goes here or use the angular service MFPClientPromise  
  //           var _that = this;
  //           console.log('MobileFirst Client SDK Initilized');

  //           MFPPush.initialize(
  //               function(successResponse) {
  //                   WL.Logger.debug("Successfully intialized");
  //                   //alert("MFP Push Successfully intialized");
  //                   MFPPush.registerNotificationsCallback(_that.notificationReceived);
  //                   _that.registerDevice();
  //               },
  //               function(failureResponse) {
  //                   console.log("Failed to initialize");
  //               });

  //       //     var notificationReceived = function(message) {
  //       //         alert(message.alert);
  //       //         //alert(navigator.userAgent);
  //       //         // if(/iPhone|iPad|iPod/i.test(navigator.userAgent)){
  //       //         //         NotificationText = message.alert.body
  //       //         // }
  //       //         // else{
  //       //         // NotificationText = message.alert;
  //       //         //     //alert(message.alert);
  //       //         // }
  //       //         // angular.pushnotification(NotificationText);
  //       //     };


  //       // });
  //     //);
  // } 
  // notificationReceived(message){
  //   alert(message.alert);
  //   //alert(navigator.userAgent);
  //   // if(/iPhone|iPad|iPod/i.test(navigator.userAgent)){
  //   //         NotificationText = message.alert.body
  //   // }
  //   // else{
  //   // NotificationText = message.alert;
  //   //     //alert(message.alert);
  //   // }
  //   // angular.pushnotification(NotificationText);
  // };


  menuDetails() {
    this.isPrepaid = this.gatewayService.isPrePaid();
    console.log("******" + this.isPrepaid);
    if (this.isPrepaid) {
      this.pages = [
        { title: 'Home', component: HomePage, iclass: 'home-icon' },
        { title: 'Reload', component: ReloadPage, iclass: 'reload-icon' },
        { title: 'Transactions', component: TransactionHistoryPage, iclass: 'transactionHistory-icon' },
        { title: 'Subscriptions', component: SubscriptionsPage, iclass: 'subscription-icon' },
        { title: 'Services', component: ServicesPage, iclass: 'services-icon' },
        { title: 'Privileges', component: PrivilegesPage, iclass: 'privileges-icon' },
        { title: 'Support', component: SupportPage, iclass: 'support-icon' },
        { title: 'Settings', component: SettingsPage, iclass: 'settings-icon' }
      ];
    } else {
      this.pages = [
        { title: 'Home', component: HomePage, iclass: 'home-icon' },
        { title: 'Pay Bill', component: PayBillPage, iclass: 'paybill-icon' },
        { title: 'Subscriptions', component: SubscriptionsPage, iclass: 'subscription-icon' },
        { title: 'Services', component: ServicesPage, iclass: 'services-icon' },
        { title: 'Transactions', component: TransactionHistoryPage, iclass: 'transactionHistory-icon' },
        { title: 'Privileges', component: PrivilegesPage, iclass: 'privileges-icon' },
        { title: 'Support', component: SupportPage, iclass: 'support-icon' },
        { title: 'Settings', component: SettingsPage, iclass: 'settings-icon' },
        { title: 'GBShare', component: GbSharePage, iclass: 'settings-icon' }
      ];
    }
  }

  setChange(){
    //alert("set changed");
    this.changeCalled = true;
  }

  update(theme: boolean) {
    this.menuDetails();
    this.XPAXTheme = theme;
    console.log("XPAX theme ijj"+ this.XPAXTheme);
  //  this.globalVar.setXPAXTheme(this.globalVar.XPAXTheme, this.renderer);
    this.globalVar.setXPAXTheme(this.XPAXTheme, this.renderer);
    if(this.updateCount == 0){   

    //alert("update is called"+theme);
    this.adminNumber = false;
    // this.MobileNumber = this.localStorage.get("MobileNum","");
   
    //this.selectedNumber =  this.gatewayService.GetMobileNumber();
    var userInfo = this.dashboardService.GetUserInfo();
    var adminNumber = userInfo["ListOfServices"].Services.BillingType;
    var GroupId = userInfo["ListOfServices"].Services.SmeGroupId;

   this.gatewayService.SetBillingType(adminNumber);
    if (adminNumber == "Billable") {
      if (GroupId) {
        this.adminNumber = true;
        this.DTService.SetISGBAdmin(true);
        this.GbShareRedirection(GroupId);
      }
    }
    this.Plan = userInfo.ListOfServices.Services.PLAN;
    this.ProdPlanName=userInfo.ListOfServices.Services.ProdPromName;

    var loader = this.gatewayService.LoaderService();
    //loader.present();
    // this.gatewayService.FeachAllSubMObileNumber().then((res) => {
    // loader.dismiss(); 
    if (adminNumber == "Billable") {
    var res = this.gatewayService.GetcustomerRetriveResponse();
    var getService = res["outputCPResp"].services

    this.mobileList = [];
    console.log("loaded  is called");
    if (res["outputCPResp"].services.length == undefined && res["outputCPResp"].services.mobileNumber) {
      var loginNumber = this.gatewayService.GetMobileNumber();
      this.mobileList.push(res["outputCPResp"].services.mobileNumber);
      this.selectedNumber = res["outputCPResp"].services.mobileNumber;
      var checknumber = res["outputCPResp"].services.principleMobileNumber;


    } else {
      getService.forEach(element => {
        var loginNumber = this.gatewayService.GetMobileNumber();

        var checkDoublication = this.mobileList.includes(element.mobileNumber);
        if (!checkDoublication) {
          //this.allNumber.push(getNewNumber);
          if (element.pre_Pos_Indicator  ==  "Postpaid"  ||  element.pre_Pos_Indicator  ==  "Prepaid") {
            this.mobileList.push(element.mobileNumber);
          }
        }



      });
      var chooseNumber = this.gatewayService.GetMobileNumber();

      chooseNumber = this.mobileList.indexOf(chooseNumber);
      this.selectedNumber = this.mobileList[Number(chooseNumber)];


    }
  }else{
   // this.selectedNumber = "";
    this.mobileList = [];
    var loginNumber = this.gatewayService.GetMobileNumber();
    this.mobileList.push(loginNumber);

    
    this.selectedNumber = loginNumber;
    
  }
  this.updateCount = this.updateCount +1;
}
    //console.log("*****reload*******"+JSON.stringify(this.mobileList));
    // }).catch(err => {
    //   loader.dismiss(); 
    //   console.log("*****reload*******" + JSON.stringify(err));
    //   this.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
    //   //alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
    // });
  }

  updateDashboard() {

  }
  callingMethod() {

  }

  updateDial() {

  }
  updateLifestyle(){
  }
  onChange(no) {
    try {
      var _that = this;
      var isChange = this.gatewayService.getOnChangeCalled();
	       var currentNumber = _that.gatewayService.GetMobileNumber();
if(currentNumber){
  this.gatewayService.setPreviousNumber(currentNumber);
}
      
      if (this.changeCalled == true) {
        //alert("isChange");
        //alert(no)
        //var mobile_number = { mobilenum: _that.selectedNumber };
        //_that.jsonstoreService.putData(mobile_number, "mobilenumber");
        _that.gatewayService.SetMobileNumber(_that.selectedNumber);
        _that.gatewayService.reset();
        var loader = _that.gatewayService.LoaderService();
        loader.present();
        _that.gatewayService.ReloadDashboard().then((res) => {
          loader.dismiss();
          //alert("on change");
          var userInfo = _that.dashboardService.GetUserInfo();
          console.log("CheckedData" + userInfo);
          _that.gatewayService.setOnChangeCalled(true);

          _that.Plan = userInfo.ListOfServices.Services.PLAN;
          _that.ProdPlanName=userInfo.ListOfServices.Services.ProdPromName;
          _that.flgforUpdate = true;
          _that.setT();
          
          _that.events.publish('updateMydeals','one');
          
          //_that.nav.setRoot(HomePage);

          _that.gatewayService.notifyObservers("Dashboard");
        }).catch(err => {
          loader.dismiss();
          if(err=="No internet connection")
          return;
          this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
		  var chooseNumber = _that.gatewayService.getPreviousNumber();
          chooseNumber = _that.mobileList.indexOf(chooseNumber);
      _that.selectedNumber = _that.mobileList[Number(chooseNumber)];
	  _that.gatewayService.SetMobileNumber(_that.selectedNumber);
          //alert("Fetch Dashboard Info onChange " + JSON.stringify(err));//FetchAllDataPostpaidPrepaid
        });
      }
    } catch (e) {
      if(e=="No internet connection")
      return;
      this.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      //alert("error" + e);
    }

  }


  showAlertToHandleNavigate(title, msg, btn) {
    if (Array.isArray(btn)) {

      try {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          buttons: [{
            text: btn[1],
            cssClass:'submit-button',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
              this.nav.setRoot(MobileConnectPage);
            }
          },
          {
            text: btn[0],
            cssClass:'submit-button',
            handler: () => {
              this.nav.setRoot(MobileConnectPage);
              this.openNativeSettings.open("wifi");
              console.log('Call to Action Clicked');
            }
          }],
          cssClass: 'success-message error-message'
          // enableBackdropDismiss:(dismissOnPageChange==null)?false:dismissOnPageChange
        });
        alert.present();

      }
      catch (e) {
       
        
        console.log("Exception : Alert open ", e);
        if(e=="No internet connection")
        return;
      }
    }
  }

  NavigateToPage() {
    console.log('Navigate to page');
    //alert("navigate to page");
    var _that = this;
    this.zone.run(() => {
    console.log("inside zone.run");
      if (_that.isFirstTimeUser == "true" && _that.isUserLoggedIn == "false") {
        console.log('is first time user');
        console.log(_that.networkStatus);
        if (_that.networkStatus == null || _that.networkStatus == 'none') {
          console.log("NavigateToPage if no n/w");
          // const alert = this.alertCtrl.create({
          //   title: 'No internet detected',
          //   subTitle: ' ',
          //  message:"Please turn on the wifi or connect to the Celcom network to use the celcom now application",
          //   buttons: [
          //   {
          //     text: 'Connect to wifi',
          //     handler: () => {
          //       console.log('connect to wifi');
          //     }
          //   }],
          //   cssClass: 'error-message'
          // });
          // alert.present();
          _that.splashScreen.hide();
          this.showAlertToHandleNavigate("Uh Oh. No Internet Detected ", "Please turn on mobile data or connect to a wifi network to continue.", ["Settings", "Close"]);

        }
        else if (_that.networkStatus == 'cellular' || _that.networkStatus == '2g' || _that.networkStatus == '3g' || _that.networkStatus == '4g') {
          //this.rootPage= TermsConditionsPage;
          //console.log('network login');
          console.log("1. network status check : "+_that.networkStatus);
          _that.splashScreen.hide();
          _that.nav.setRoot(MobileConnectPage);
        }
        else if (_that.networkStatus == 'wifi' || _that.networkStatus == 'unknown' || _that.networkStatus == 'ethernet') {
          //this.rootPage=MobileConnectPage;
          console.log("2. network status check : "+_that.networkStatus);
          _that.splashScreen.hide();
          _that.nav.setRoot(MobileConnectPage);
        }
      }
      else if (_that.isFirstTimeUser == "false" && _that.isUserLoggedIn == "true") {
        console.log("3. First time user : "+_that.isFirstTimeUser);
        console.log("4. isUserLoggedTo : "+_that.isUserLoggedIn);
        _that.splashScreen.hide();
        _that.rootPage = HomePage;

      } else if (_that.isFirstTimeUser == "false" && _that.isUserLoggedIn == "false") {
        console.log("5. network status : "+_that.networkStatus);
        //if user is on celcom network or on wifi??
        if (_that.networkStatus == null || _that.networkStatus == 'none') {
          console.log("NavigateToPage if none..No internet detected ---> NetworkStatus ---> "+_that.networkStatus);
          //   let alert = _that.alertCtrl.create({
          //     title: 'Network Check',
          //     subTitle: 'There is no internet connection!!',
          //     buttons: ['OK']
          // });
          // alert.present();
          _that.showAlertToHandleNavigate("Uh Oh. No Internet Detected ", "Please turn on mobile data or connect to a wifi network to continue.", ["Settings", "Close"]);
        }
        else if (_that.networkStatus == 'cellular' || _that.networkStatus == '2g' || _that.networkStatus == '3g' || _that.networkStatus == '4g') {
          console.log("6. network status : "+_that.networkStatus);
          //this.rootPage=HomePage;
          _that.splashScreen.hide();
          _that.rootPage = HomePage;
        }
        else if (_that.networkStatus == 'wifi' || _that.networkStatus == 'unknown' || _that.networkStatus == 'ethernet') {
         console.log("7. network status : "+_that.networkStatus);
          //this.rootPage=MobileConnectPage;
          _that.splashScreen.hide();
          _that.nav.setRoot(MobileConnectPage);
        }else{
          //New condition added to avoid unresponsive blank page
          console.log("in network else condition, no condition statisfied");
          this.alertCtrlInstance.showAlert("Uh Oh. Network detection failed ", "We could not detect your network. Please login via connect", "OK");
          _that.splashScreen.hide(); 
          _that.nav.setRoot(MobileConnectPage);
        }
      }else{
        //New condition added to avoid unresponsive blank page
          console.log("in network else condition, no condition statisfied, first time user and is logged in before");
          //this.alertCtrlInstance.showAlert("Uh Oh. Network detection failed ", "We could not detect your network. Please login via connect", "OK");
          _that.splashScreen.hide(); 
          _that.nav.setRoot(MobileConnectPage);
      }
    });
 
  }
  setT(){
    this.XPAXTheme = this.gatewayService.isPrePaid();}

  setTheme() {
    this.XPAXTheme = this.gatewayService.isPrePaid();

    if (this.flgforUpdate) {
      this.flgforUpdate = false;
      if (this.nav.getActive().name  ==  "HomePage") {
        //loader.dismiss();
        this.events.publish('GBShareStatusUpdate');
      } else {
        // alert(2);
        //loader.dismiss();
        this.nav.setRoot(HomePage);
      }
    }
  }
  setPages() {
    if (this.XPAXTheme) {
      this.pages = [
        { title: 'Home', component: HomePage, iclass: 'home-icon' },
        { title: 'Reload', component: ReloadPage, iclass: 'reload-icon' },
        { title: 'Transaction History', component: TransactionHistoryPage, iclass: 'transactionHistory-icon' },
        { title: 'Subscriptions', component: SubscriptionsPage, iclass: 'subscription-icon' },
        { title: 'Services', component: ServicesPage, iclass: 'services-icon' },
        { title: 'Privileges', component: PrivilegesPage, iclass: 'privileges-icon' },
        { title: 'Support', component: SupportPage, iclass: 'support-icon' },
        { title: 'Settings', component: SettingsPage, iclass: 'settings-icon' }
        // { title: 'OnBoarding', component: OnBoardingGuidePage, iclass: 'settings-icon' },//just for demo
        // { title: 'Style Guide', component: StyleGuidePage, iclass: 'settings-icon' },//just for demo
        // { title: 'Card Guide', component: CardDesignGuidePage, iclass: 'settings-icon' },//just for demo
        // { title: 'Card Design Example Guide', component: CardDesignGuideExamplePage, iclass: 'settings-icon' },//just for demo
        // { title: 'Toggle XPAX', component: HomePage, iclass: 'settings-icon' } ,//just for demo

      ];
    }
    else {
      this.pages = [
        { title: 'Home', component: HomePage, iclass: 'home-icon' },
        { title: 'Pay Your Bill', component: PayBillPage, iclass: 'paybill-icon' },
        { title: 'Transaction History', component: TransactionHistoryPage, iclass: 'transactionHistory-icon' },
        { title: 'Subscriptions', component: SubscriptionsPage, iclass: 'subscription-icon' },
        { title: 'Services', component: ServicesPage, iclass: 'services-icon' },
        { title: 'Privileges', component: PrivilegesPage, iclass: 'privileges-icon' },
        { title: 'Support', component: SupportPage, iclass: 'support-icon' },
        { title: 'Settings', component: SettingsPage, iclass: 'settings-icon' }
        // { title: 'OnBoarding', component: OnBoardingGuidePage, iclass: 'settings-icon' },//just for demo
        // { title: 'Style Guide', component: StyleGuidePage, iclass: 'settings-icon' },//just for demo
        // { title: 'Card Guide', component: CardDesignGuidePage, iclass: 'settings-icon' },//just for demo
        // { title: 'Card Design Example Guide', component: CardDesignGuideExamplePage, iclass: 'settings-icon' },//just for demo
        // { title: 'Toggle XPAX', component: HomePage, iclass: 'settings-icon' } ,//just for demo

      ];
    }
  }
  initializeApp() {
    console.log("init");
    var _that = this;
    this.platform.ready().then(() => {
      let splash = this.modalCtrl.create(SplashscreenPage);
            splash.present();
      //this.splashScreen.hide(); 
      this.networkStatus = this.gatewayService.GetNetworkStatus();
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
     
      console.log("Platform ready called");
      // let status bar overlay webview
      this.statusBar.overlaysWebView(true);
      // set status bar to white
      /*if (this.platform.is('android')) {
      this.statusBar.styleLightContent();
      }else{
        this.statusBar.styleDefault();
      }*/
      this.statusBar.styleLightContent();
      this.keyboard.hideKeyboardAccessoryBar(false);
      this.keyboard.disableScroll(true);

      if (this.platform.is('android')) {
        this.platform.registerBackButtonAction((e) => {
          // this.backButtonHold = true;
          console.log('***BACK' + this.nav.getActive().name);
          if (this.nav.getActive().name == "HomePage") {
            this.appMinimize.minimize();
          }else{
              if (this.nav.getActive().name == "ReloadPage" || this.nav.getActive().name == "TransactionHistoryPage" || this.nav.getActive().name == "SubscriptionsPage" || this.nav.getActive().name == "ServicesPage" || this.nav.getActive().name == "SupportPage" || this.nav.getActive().name == "SettingsPage" || this.nav.getActive().name == "PrivilegesPage" || this.nav.getActive().name == "PayBillPage" || this.nav.getActive().name == "GbSharePage" || this.nav.getActive().name == "GbShareAllocatePage") {
               this.nav.pop();
              }else{
               this.nav.pop();
             }
              // navigator.app.backHistory();
              // navigator.app
              // this.nav._app.goBack();
              
          }
        });
      }

      this.jsonstoreService.initJSONStore().then(function () {
        //check network login first then check sim and network
        //_that.registerPush();
        _that.splashScreen.hide();
        var loader = _that.gatewayService.LoaderService();

        //if (_that.isFirstTimeUser == "true" && _that.isUserLoggedIn == "false") {
          if (_that.isUserLoggedIn == "false") {
          if (_that.networkStatus == 'cellular' || _that.networkStatus == '2g' || _that.networkStatus == '3g' || _that.networkStatus == '4g') {
            loader.present();
            //check network login
            _that.doNetworkLogin().then((res) => {
              //alert("Do network login");
              console.log(res);
              console.log(res["msisdn"]);
              //res["msisdn"] = "60138371168";
              if (res["msisdn"] != null) {
                console.log('if msisdn');
                //go to dashboard
                //alert("go to dashboard");
                var msisdn = res["msisdn"];
                console.log('if msisdn 1');
                var number = msisdn.substr(1);
                console.log('if msisdn 2');
                var mobile_number = { mobilenum: number };
                console.log('if msisdn 3');

                //check celcom number or not
                _that.validNumber = false;
                //var _that = this;
                var selectedNumber = number;
                console.log('if msisdn 4');
                _that.reloadService.checkAddedNewNumber(selectedNumber).then((res) => {
                  console.log("inside checkAddedNewNumber " + JSON.stringify(res));
                  //_that.serviceIndicator = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.ListOfServices.Services.ServiceType;
                  if (selectedNumber.substring(0, 3) == "011" && selectedNumber.length == 10) {
                    loader.dismiss();
                    _that.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid Number", "Please input a valid Celcom phone number. (If starting with 011 then put 11 digits).", "Try again");
                  }
                  else {
                    _that.validNumber = true;
                    //if BRN number, don't login
                    var checkBRN = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.CustomerIDType;

                    if (checkBRN == "BRN") {
                      loader.dismiss();
                      let alert = this.alertCtrl.create({
                        title: "Thanks for your interest in Celcom Life.",
                        subTitle: "At the moment, the app is for use by XPAX and postpaid customers only.",
                        buttons: [
                          {
                            text: "Ok",
                            role: "Ok",
                            cssClass: 'error-button-float-right submit-button',
                            handler: () => {
                              console.log('Call to Action Clicked');
                              _that.nav.setRoot(MobileConnectPage);
                            }
                          }],
                        cssClass: 'success-message error-message',
                        enableBackdropDismiss: false
              
                      });
                      alert.present();
                      //_that.alertCtrlInstance.showAlert("Thanks for your interest in Celcom Life.", "At the moment, the app is for use by XPAX and postpaid customers only.", "OK");
                    }
                    else {
                      _that.jsonstoreService.putData(mobile_number, "mobilenumber"); // data { key : value , <collection_name>}
                      _that.gatewayService.SetMobileNumber(number);
                      _that.reloadService.setingLoginNumber(number);
                      _that.localStorage.set("IsUserLoggedIn", "true");
                      _that.gatewayService.ReloadDashboard().then((res) => {
                        //alert("Dashboard");              
                        var userInfo = _that.dashboardService.GetUserInfo();
                        _that.gatewayService.SetXPAX(userInfo.ListOfServices.Services.ServiceType);
                        //_that.reloadService.setingLoginNumberServiceType(userInfo.ListOfServices.Services.ServiceType);
                        _that.gatewayService.notifyObservers("Menu");
                        if (_that.isFirstTimeUser == "true") {
                          _that.nav.setRoot(NetworkLoginPage);
                          loader.dismiss();
                        } else {
                          _that.nav.setRoot(HomePage);
                          loader.dismiss();
                        }

                      }).catch(err => {
                        loader.dismiss();
                        if(err=="No internet connection")
                        return;
                        _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
                        //alert("Fetch Dashboard Info "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
                       
                      });
                    }

                  }


                }).catch(err => {
                  loader.dismiss();
                 
                  console.log("*****app component*******" + JSON.stringify(err));
                  if(err=="No internet connection")
                  return;
                  _that.alertCtrlInstance.showAlert("Notification", "Not a Celcom number.", "OK");

                  // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
                });


              } else {
                console.log('else msisdn');
                loader.dismiss();
                //check for sim
                //for Android
                _that.checkForSim();
              }
            }).catch(err => {
              console.log('network login catch');
              console.log(err);
              loader.dismiss();
              if(err=="No internet connection")
              return;
              //_that.nav.setRoot(MobileConnectPage);
              _that.checkForSim();
            });
          }
          else {
            //_that.nav.setRoot(MobileConnectPage);
            _that.checkForSim();
          }
        }
        else {

          if (_that.isFirstTimeUser == "false" && _that.isUserLoggedIn == "true") {
            console.log('logged in already');
            _that.afterSimDetectDashboard();

          }
          else {
            //check for sim

            _that.checkForSim();
          }
        }
      });
      this.keyboard.hideKeyboardAccessoryBar(false);
    });

  }

  checkForSim() {
    var _that = this;
    //for Android
    if (_that.platform.is('android')) {
      _that.sim.hasReadPermission().then(
        (info) => {
          if (!info) {
            _that.sim.requestReadPermission().then(
              () => {
                console.log('Permission granted');
                //if user already logged in, don't need to detect sim
                if (_that.isFirstTimeUser == "false" && _that.isUserLoggedIn == "true") {
                  _that.afterSimDetectDashboard();
                }
                else {
                  _that.getSimCardInfo();
                }
              }
              ,
              () => {
                //'Sim detection Permission denied' showing alert
                //   let alert = this.alertCtrl.create({
                //     title: 'Sim detection Error',
                //     subTitle: 'Sim detection Permission denied, kindly enable sim detection from settings',
                //     buttons: [],
                //     cssClass: 'error-message',
                //     enableBackdropDismiss: false
                //   });
                //  alert.present();
                if (_that.isFirstTimeUser == "false" && _that.isUserLoggedIn == "true") {
                  _that.afterSimDetectDashboard();
                }
                else {
                  _that.nav.setRoot(MobileConnectPage);
                }

              }
            );
          }
          else {
            //if user already logged in, don't need to detect sim
            if (_that.isFirstTimeUser == "false" && _that.isUserLoggedIn == "true") {
              _that.afterSimDetectDashboard();
            }
            else {
              _that.getSimCardInfo();
            }


          }
        }
      );
    }

    //for iOS
    else {
      //if user already logged in, don't need to detect sim
      if (_that.isFirstTimeUser == "false" && _that.isUserLoggedIn == "true") {
        _that.afterSimDetectDashboard();
      }
      else {
        _that.getSimCardInfo();
      }

    }
  }
  getSimCardInfo() {
    var _that = this;
    this.sim.getSimInfo().then(
      (info) => {
        //console.log('Sim info: '+ JSON.stringify(info));
        //console.log('info: '+ info.cards);
        //console.log('info: '+ info.cards.length);
        console.log("Network detect::" + JSON.stringify(info));

        var celcomFlag = false;
        var blankFlag = false;
        var isSimPresent = true;
        console.log(info.cards);
        if (info.cards == undefined) {
          console.log('cards undefined');
          isSimPresent = false;
          if (info.carrierName == "CELCOM" || info.carrierName == "MY CELCOM") {
            console.log('if carrier celcom');
            _that.afterSimDetectDashboard();
          }
          else {
            console.log('else1');
          
            if (ConstantData.isLoginBypassed == false) {
              _that.navigatetoSplash();
            } else {
              console.log('else');
              _that.NavigateToPage();
            }
          
          }
        }
        else {
          for (var i = 0; i < info.cards.length; i++) {
            var cr = info.cards[i].carrierName;
            //cr = "";
            //cr=="IND airtel" || cr=="IDEA" ||
            //in case of blank carrier name

            if (cr == "CELCOM" || cr == "MY CELCOM") {
              celcomFlag = true;
            } else {
              blankFlag = true;
            }
          }
        }
        console.log(celcomFlag);//false
        console.log(blankFlag);//true
        console.log(isSimPresent);//true
        // alert("card info::"+JSON.stringify(info.cards[0].carrierName));
        //var carrier=JSON.stringify(info.cards.carrierName);
        if (isSimPresent) {
          if (celcomFlag) {
            //alert("User is already logged in");
            //write code here
            _that.afterSimDetectDashboard();

          }
          else {
            /* start commented by hitesh for prod issue - no celcom sim then go to explore to celcom*/
            if (blankFlag) {
              _that.splashScreen.hide();
              //_that.nav.setRoot(MobileConnectPage);// for internal testing
              if (_that.networkStatus == 'cellular' || _that.networkStatus == '2g' || _that.networkStatus == '3g' || _that.networkStatus == '4g') {
              
                _that.nav.setRoot(SplashScreen2Page);
              }
              else if (_that.networkStatus == 'wifi' || _that.networkStatus == 'unknown' || _that.networkStatus == 'ethernet') {
                
                
                _that.nav.setRoot(MobileConnectPage);
              }
              else{
                
                _that.nav.setRoot(SplashScreen2Page);
              }
            }
            else {
              _that.splashScreen.hide();
              _that.rootPage = SplashScreen2Page; //commented to test on test devices
            }
             /* end commented by hitesh for prod issue - no celcom sim then go to explore to celcom */
            //_that.splashScreen.hide();
              //_that.rootPage = SplashScreen2Page;
              //_that.nav.setRoot(SplashScreen2Page);

          }
        }
        else {
          console.log('no sim');
          if (_that.isFirstTimeUser == "false" && _that.isUserLoggedIn == "true") {
            _that.splashScreen.hide();
            _that.afterSimDetectDashboard();

          }
          else {
            _that.splashScreen.hide();
            if (_that.networkStatus == 'cellular' || _that.networkStatus == '2g' || _that.networkStatus == '3g' || _that.networkStatus == '4g') {
              
              _that.nav.setRoot(MobileConnectPage);
            }
            else if (_that.networkStatus == 'wifi' || _that.networkStatus == 'unknown' || _that.networkStatus == 'ethernet') {
              
              
              _that.nav.setRoot(MobileConnectPage);
            }
            else{
              
              _that.nav.setRoot(SplashScreen2Page);
            }
            //_that.nav.setRoot(MobileConnectPage);
          }
        }
        // else {
        //   this.zone.run(() => {
        //    //alert("in splash");
        //    _that.rootPage = SplashScreen2Page;
        //    // _that.nav.setRoot(SplashScreen2Page);

        //   });


        // }

      },
      (err) => console.log('Unable to get sim info: ' + JSON.stringify(err))
    );
  }

  navigatetoSplash() {
    this.splashScreen.hide();
    this.nav.setRoot(SplashScreen2Page);
  }

  afterSimDetectDashboard() {
    //alert("GETTING LoginNumber after sim detection");
    var _that = this;
    var mobile_number = {};
    _that.jsonstoreService.getData(mobile_number, "mobilenumber").then(
      (result_mob) => {
        console.log("mobile_number update() ", JSON.stringify(result_mob));
        //alert("mobile_number update() "+JSON.stringify(result_mob));
        _that.MobileNumber = result_mob.length > 0 ? result_mob[0].json.mobilenum : "";
        _that.mobileList[0] = _that.MobileNumber;
        _that.gatewayService.SetMobileNumber(_that.MobileNumber);
        _that.reloadService.setingLoginNumber(_that.MobileNumber);
        if (_that.isFirstTimeUser == "false" && _that.isUserLoggedIn == "true") {
          _that.gatewayService.ReloadDashboard().then((res) => {
            //alert("afterSimDetectDashboard");
            _that.userInfo = _that.dashboardService.GetUserInfo();
            _that.reloadService.setingLoginNumberServiceType(_that.userInfo.ListOfServices.Services.ServiceType);
            _that.Plan = _that.userInfo.ListOfServices.Services.PLAN;
            _that.ProdPlanName=_that.userInfo.ListOfServices.Services.ProdPromName;
            //alert("reload dashboard "+JSON.stringify(_that.userInfo));
            //this.MobileNumber = this.localStorage.get("MobileNum","");
            //_that.mobileList[0] = _that.MobileNumber;

            _that.setTheme();
            _that.NavigateToPage();
            //_that.gatewayService.notifyObservers("Dashboard");
          }).catch(err => {
            if(err=="No internet connection")
            return;
            _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
            //alert("Fetch Dashboard Info in " + JSON.stringify(err));//FetchAllDataPostpaidPrepaid

          });

        } else {
          //_that.dashboardService.RegisterCallbackToUpdateMenu(_that.setTheme);
          _that.NavigateToPage();
        }
      },
      (err) => {
        console.log("mobile_number update() err ", err);
        _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
      });


  }




  openPage(page) {
    // to set default page if going through menu
    this.subscriptionsTabService.SetTabInfo(0);

    if (page.title == "Toggle CAP Zone") {
      this.globalVar.setCapZoneStatus(!this.globalVar.getCapZoneStatus());
      if (this.globalVar.getcallBarredStatus()) {
        this.globalVar.setcallBarredStatus(!this.globalVar.getcallBarredStatus());
      }
    }

    if (page.title == "Toggle CAll Barred") {
      this.globalVar.setcallBarredStatus(!this.globalVar.getcallBarredStatus());
      if (this.globalVar.getCapZoneStatus()) {
        this.globalVar.setCapZoneStatus(!this.globalVar.getCapZoneStatus());
      }
    }
    var userInfo = this.dashboardService.GetUserInfo();
    var adminNumber = userInfo["ListOfServices"].Services.BillingType;
    var GroupId = userInfo["ListOfServices"].Services.SmeGroupId;
    if (adminNumber == "Billable") {
      if (GroupId) {
        this.adminNumber = true;
        this.GbShareRedirection(GroupId);
      }
    }

    if (this.nav.getActive().component == page.component) {
      this.events.publish('GBShareStatusUpdate');
      if (page.title == "GBShare") {
        if (this.gbChoose == "Allocation") {
          this.nav.setRoot(GbShareAllocatePage);
        } else if (this.gbChoose == "Group") {
          this.nav.setRoot(GbSharePage);
        }
      } else {
      }
    } else {
      var userInfo = this.dashboardService.GetUserInfo();
      var GroupId = userInfo["ListOfServices"].Services.SmeGroupId;
      if (page.title == "GBShare") {
        if (this.gbChoose == "Allocation") {
          this.nav.setRoot(GbShareAllocatePage);
        } else if (this.gbChoose == "Group") {
          this.nav.setRoot(GbSharePage);
        } else {
          //  this.nav.setRoot(page.component);
        }
      } else {
        // this.nav.setRoot(page.component);
        console.log("page.component ",page.component);
        if(page.title == "Home"){
          this.nav.setRoot(page.component);        
        }else{
          this.nav.push(page.component);        
        }
        
      }
    }
  }

  logout() {
    var _that = this;
    this.updateCount = 0;
    _that.localStorage.set("IsUserLoggedIn", "false");
    _that.gatewayService.reset();
    var paybillData =  "";
    this.paybillService.setPhone_BookNumber(paybillData);
    var reloadData = "";
    this.reloadService.setPhonebookNumber(reloadData);
    this.gatewayService.setOnChangeCalled(false);
    _that.gatewayService.SetMobileNumber(null);
    _that.selectedNumber = null;
    _that.selectedNumber = '';
    this.changeCalled = false;
    this.dashboardService.Initialize();
    this.DTService.Initialize();
    this.globalVar.setXPAXTheme(false, this.renderer);
    if (_that.networkStatus == null || _that.networkStatus == 'none') {
      this.nav.setRoot(MobileConnectPage);
    }
    if (this.networkStatus == 'wifi' || this.networkStatus == 'unknown' || this.networkStatus == 'ethernet') {
      this.nav.setRoot(MobileConnectPage);
    }
    
    else { 
     
      this.nav.setRoot(LogoutPage);
     
    }
  }


  afterLogoutSimInfo() {
    var _that = this;
    _that.sim.getSimInfo().then(
      (info) => {
        console.log('Sim info: ' + JSON.stringify(info))
        // alert("Network detect::"+JSON.stringify(info) );

        var flag = false;
        var blankFlag = false;
        var isSimPresent = true;
        if (info.cards == undefined) {
          isSimPresent = false;
          console.log('hi');
          _that.nav.setRoot(MobileConnectPage);
          //  return;
        } else {
          for (var i = 0; i < info.cards.length; i++) {
            var cr = info.cards[i].carrierName;
            if (cr == "CELCOM" || cr== "MY CELCOM") {
              flag = true;
            } else {
              blankFlag = true;
            }
          }
        }

        if (isSimPresent) {
          if (flag) {
            var _that = this;
            if (_that.networkStatus == null || _that.networkStatus == 'none') {

              //  const alert = this.alertCtrl.create({
              //  title: 'No internet detected',
              //   subTitle: ' ',
              //  message:"Please turn on the wifi or connect to the Celcom network to use the celcom now application",
              //    buttons: [
              //   {
              //    text: 'Connect to wifi',
              //     handler: () => {
              //       console.log('connect to wifi');
              //     }
              //    }],
              //    cssClass: 'error-message'
              //  });
              //  alert.present();
              _that.showAlertToHandleNavigate("Uh Oh. No Internet Detected ", "Please turn on mobile data or connect to a wifi network to continue.", ["Settings", "Close"]);


            }
            else if (_that.networkStatus == 'cellular' || _that.networkStatus == '2g' || _that.networkStatus == '3g' || _that.networkStatus == '4g') {
              //this.rootPage= TermsConditionsPage;
             
              _that.nav.setRoot(LogoutPage);
            }
            else if (_that.networkStatus == 'wifi' || _that.networkStatus == 'unknown' || _that.networkStatus == 'ethernet') {
              //this.rootPage=MobileConnectPage;
              // _that.splashScreen.hide();
              _that.nav.setRoot(MobileConnectPage);
            }



          }
          else {
            if (blankFlag) {
              _that.nav.setRoot(MobileConnectPage);
            }
            else {
              _that.nav.setRoot(MobileConnectPage);
            }

          }
        }
        else {

          _that.nav.setRoot(MobileConnectPage);


        }

      },
      (err) => {
        console.log('Unable to get sim info: ' + JSON.stringify(err))
        _that.nav.setRoot(MobileConnectPage);
      }
    );

  }


  doNetworkLogin() {

    var params = {};
    return new Promise((resolve, reject) => {
      this.gatewayService.CallAdapter(ConstantData.adapterUrls.networkLogin, "POST", params).then(function (res) {
        console.log("************************************* component success network login" + JSON.stringify(res));
        resolve(res);
      })
        .catch(function (e) {
          console.log("****************************************** component failure network login" + e);
          //alert("error network login ");
         
          reject(e);
          
        });
    });
  }
}



