import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule  } from 'ionic-angular';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import { Keyboard} from "@ionic-native/keyboard";
import { EmailComposer } from '@ionic-native/email-composer';
import { CallNumber } from '@ionic-native/call-number';

import { celcom } from './app.component';
import { HomePage } from '../pages/home/home';
import { DetailedUsagePage} from '../pages/detailed-usage/detailed-usage';
import { DetailedUsageInternetPage} from '../pages/detailed-usage/detailed-usage-internet/detailed-usage-internet';
import { DetailedUsageRoamingPage} from '../pages/detailed-usage/detailed-usage-roaming/detailed-usage-roaming';
import { DetailedUsageSmsPage} from '../pages/detailed-usage/detailed-usage-sms/detailed-usage-sms';
import { DetailedUsageTalkPage} from '../pages/detailed-usage/detailed-usage-talk/detailed-usage-talk';
import { MobileConnectPage } from '../pages/login/mobile-connect/mobile-connect';
import { ConnectTacPage } from '../pages/login/connect-tac/connect-tac';
import { OnBoardingGuidePage} from '../pages/on-boarding-guide/on-boarding-guide';
import { TransactionHistoryPage } from '../pages/transaction-history/transaction-history';
import { PaymentRecieptPage } from '../pages/payment-reciept/payment-reciept';

import { SubscriptionService } from '../pages/subscriptions/subscriptionService';
import { SubscriptionsPage } from '../pages/subscriptions/subscriptions';
import { SubInternetPage } from '../pages/subscriptions/sub-internet/sub-internet';
import { AddOnsPage } from '../pages/subscriptions/add-ons/add-ons';
import { IddPage } from '../pages/subscriptions/idd/idd';
import { RoamingPage } from '../pages/subscriptions/roaming/roaming';
import { SubscriptionConfirmationPage } from '../pages/subscriptions/subscription-confirmation/subscription-confirmation';


import { SuperTabsModule } from "../components/custom-ionic-tabs";
import { SuperTabsController } from "../components/custom-ionic-tabs/providers/super-tabs-controller";

import { ServicesPage } from '../pages/services/services';
import { PrivilegesPage } from '../pages/privileges/privileges';
import { NotificationsPage } from '../pages/notifications/notifications';
import { SupportPage } from '../pages/support/support';
import { SettingsPage } from "../pages/settings/settings";
import { MyAccountPage } from "../pages/settings/my-account/my-account";
import { PaymentMethodPage } from "../pages/settings/payment-method/payment-method";
import { LogoutPage } from "../pages/logout/logout";
import {SplashScreen2Page} from "../pages/splash-screen2/splash-screen2";
import {GlobalVars} from "../providers/globalVars";
import {NotificationTab1Page} from "../components/notification-drawer/notification-tab1/notification-tab1";
import {NotificationTab2Page} from "../components/notification-drawer/notification-tab2/notification-tab2";
import {NotificationTab3Page} from "../components/notification-drawer/notification-tab3/notification-tab3";
import {NotificationTab4Page} from "../components/notification-drawer/notification-tab4/notification-tab4";
import {NetworkLoginPage} from "../pages/login/network-login/network-login";
import {NetworkLoginLegalPage} from "../pages/login/network-login/network-login-legal/network-login-legal";
import {LoginLegalTermsPage} from "../pages/login/network-login/network-login-legal/login-legal-terms/login-legal-terms";
import {LoginLegalPrivacyPage} from "../pages/login/network-login/network-login-legal/login-legal-privacy/login-legal-privacy";

import { UsageIndicatorComponent } from "../components/card-component/usage-indicator/usage-indcator";
import {NotificationDrawer} from "../components/notification-drawer/notification-drawer";
import {MobileConnectAccountPage} from "../pages/login/mobile-connect/account-setup/account-setup";
import {StyleGuidePage} from "../pages/style-guide/style-guide";
import {NetSpeedCheckPage} from "../pages/net-speed-check/net-speed-check";
import {NetSpeedCheckInfoPage} from "../pages/net-speed-check-info/net-speed-check-info";
import { OleOleTermsConditionsPage } from "../pages/services/oleole-shop/oleole-terms-conditions/oleole-terms-conditions"

import { DeviceSelfHelpPage } from '../pages/support/device-self-help/device-self-help';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { Uid } from '@ionic-native/uid';

import { GatewayService } from '../global/utilService';
import { ReloadService } from '../global/reloadServices';
import { JsonstoreService } from '../global/jsonstore';
import { LocalStorage } from '../global/localStorage';
import { NetcheckerProvider } from '../global/netchecker';
import { SpeedcheckerProvider } from '../global/speedchecker';
import { HandleError } from '../global/handleerror';
import { AlertService } from '../global/alert-service';
import { GbShareService } from '../global/gbshareServices';

import { DashboardService } from '../pages/home/homeService';
import { Network } from '@ionic-native/network';
import { NetworkLoginService } from "../pages/login/network-login/network-loginService";
import { AndroidPermissions} from '@ionic-native/android-permissions';
import { Sim } from '@ionic-native/sim';
import { MyDealsPage } from "../pages/my-deals/my-deals";
import { MyDealsModalPage } from "../pages/my-deals-modal/my-deals-modal";
// services
import { MyDealProvider } from '../global/mydealservice';

// oleole service
import { OleoleService } from '../global/oleole-service';


import { ConnectTacTwoPage } from "../pages/connect-tac-two/connect-tac-two";

import { PlanDetailsPage } from "../pages/plan-details/plan-details";
import { PlanDetailsTermsAndConditionsPage } from "../pages/plan-details/plan-details-terms-and-conditions/plan-details-terms-and-conditions";

import { DetailedUsageService } from '../pages/detailed-usage/detailedUsageService';
import { NativePageTransitions } from "@ionic-native/native-page-transitions";
import {ResetPasswordPage} from "../pages/settings/my-account/rest-password/reset-password";
import { RaphaelDrawerComponent } from "../components/raphael-drawer/raphael-drawer";
import { RaphaelDrawerSpeedTestComponent } from "../components/raphael-drawer-speed-test/raphael-drawer-speed-test";
import { CardDesignGuidePage } from "../pages/card-design-guide/card-design-guide";
import { ReloadPage } from "../pages/reload/reload";
import { CreditReloadPage } from "../pages/reload/credit-reload/credit-reload";
import { InternetPlansPage } from "../pages/reload/internet-plans/internet-plans";
import { ReloadCodePage } from "../pages/reload/reload-code/reload-code";
import { CardDesignGuideExamplePage } from "../pages/card-design-guide-example/card-design-guide-example";
import { ReloadPopoverComponent } from "../components/reload-popover/reload-popover";
import { AddNumberModalPage } from "../pages/add-number-modal/add-number-modal";
import { SubscriptionSelectCountryPage } from "../pages/subscriptions/subscription-select-country/subscription-select-country";
import { CountrySubscriptionsPage } from "../pages/subscriptions/country-subscriptions/country-subscriptions";
import { PayBillPage } from "../pages/pay-bill/pay-bill";
import { BillFullAmountPage } from "../pages/pay-bill/bill-full-amount/bill-full-amount";
import { BillOtherAmountPage } from "../pages/pay-bill/bill-other-amount/bill-other-amount";
import { BillReloadCodePage } from "../pages/pay-bill/bill-reload-code/bill-reload-code";
import { ConfirmationScreensPage } from "../pages/confirmation-screens/confirmation-screens";
import { ConfirmationScreensMydealsPage } from "../pages/confirmation-screens-mydeals/confirmation-screens-mydeals";

import { ChangeCreditcardModalPage } from "../pages/change-creditcard-modal/change-creditcard-modal";

import {ProductDetailsDesignGuidePage} from "../pages/product-details-design-guide/product-details-design-guide";
import {SubscriptionProductDetailsDesignGuidePage} from "../pages/subscriptions/subscription-product-details-design-guide/subscription-product-details-design-guide";

import { OleoleShopPage } from "../pages/services/oleole-shop/oleole-shop";
import { CataloguePage } from "../pages/services/oleole-shop/catalogue/catalogue";
import { GiftsRecievedPage } from "../pages/services/oleole-shop/gifts-recieved/gifts-recieved";
import { ReloadBonusPage } from "../pages/reload-bonus/reload-bonus";
import { PromotionalCampaignPage } from "../pages/promotional-campaign/promotional-campaign-bonus";
import {MobileConnectService} from "../pages/login/mobile-connect/mobileConnectService";
import { InAppBrowser, InAppBrowserOptions} from '@ionic-native/in-app-browser';
import { DatePipe } from '@angular/common';
import { SubscriptionsTabService} from '../pages/subscriptions/subscriptionsTabService';
import { GbSharePage } from "../pages/gb-share/gb-share";
import { AutoBillingPage } from "../pages/services/auto-billing/auto-billing";
import { OleoleCatalogueListPage } from "../pages/services/oleole-shop/oleole-catalogue-list/oleole-catalogue-list";
import { OleoleGiftsRecievedListPage } from "../pages/services/oleole-shop/oleole-gifts-recieved-list/oleole-gifts-received-list";
import { GbShareAllocatePage } from "../pages/gb-share-allocate/gb-share-allocate";
import { SpeedTestPage } from "../pages/support/speed-test/speed-test";
import { SpeedTestProvider } from "../pages/support/speed-test/speed-test-service";
import { FaqPage } from "../pages/support/faq/faq";
import { ContactUsPage } from "../pages/support/contact-us/contact-us";
import { FeedbackPage } from "../pages/support/feedback/feedback";
import { PukPage } from "../pages/support/puk/puk";
import { PUKService } from "../pages/support/puk/pukService";
import { CreditTransferService } from "../pages/services/credit-manage/credit-transfer/credit-transferService";
import { FaqCategoryPage } from "../pages/support/faq/faq-category/faq-category";
import { TransactionHistoryDetailsPage } from "../pages/transaction-history/history-details/history-details";
import { TransactionHistoryTransactionTabPage } from "../pages/transaction-history/transaction-tab/transaction-tab";
import { TransactionHistoryBillPaymentTabPage } from "../pages/transaction-history/bill-payment-tab/bill-payment-tab";
import { CreditManagePage } from "../pages/services/credit-manage/credit-manage";
import { ValidityExtensionPage } from "../pages/services/credit-manage/validity-extension/validity-extension";
import { ValidityExtensionService } from "../pages/services/credit-manage/validity-extension/validity-extensionService";
import { CreditTransferPage } from "../pages/services/credit-manage/credit-transfer/credit-transfer";
import { CreditAdvancePage } from "../pages/services/credit-manage/credit-advance/credit-advance";
import { TransactionHistoryDetailsPayBillPage } from "../pages/transaction-history/history-details-pay-bill/history-details-pay-bill";
import { StoreLocatorSelectStorePage } from "../pages/support/store-locator/select-store/store-locator-select-store";
import { StoreLocatorStoreDetailsPage } from "../pages/support/store-locator/store-details/store-locator-store-details";
import { StoreLocatorStoreFilterPage } from "../pages/support/store-locator/store-filter/store-locator-store-filter";
import { StoreLocatorStoreMapPage } from "../pages/support/store-locator/store-map/store-locator-store-map";
import { StoreLocatorService } from '../pages/support/store-locator/store-locatorService';

import { StoreFilterPipe } from "../pages/support/store-locator/select-store/store_filter.pipe";

import { OrderByFilterPipe } from '../pages/transaction-history/orderby_filter.pipe';
import { UpcomingToLatestPipe } from '../components/notification-drawer/notification-tab1/arrangeWithLatestPipe';

//
import { PaymentMethodsPage } from "../pages/settings/payment-methods/payment-methods";
import { ViewCardDetailsPage } from "../pages/settings/payment-methods/view-card-details/view-card-details";
import { RewardsPage } from "../pages/privileges/rewards/rewards";
import { OfferingPage } from "../pages/privileges/rewards/offering/offering";
import { RedeemedPage } from "../pages/privileges/rewards/redeemed/redeemed";
import { PrivRewardsPage } from '../pages/privileges/first-style/priv-rewards/priv-rewards';
import { PrivOfferingPage } from "../pages/privileges/first-style/priv-rewards/priv-offering/priv-offering";
import { PrivRedeemedPage } from "../pages/privileges/first-style/priv-rewards/priv-redeemed/priv-redeemed";
import { UpgradeLatestPlanPage } from "../pages/services/upgrade-latest-plan/upgrade-latest-plan";
import { UpgardeLatestPlanService } from "../pages/services/upgrade-latest-plan/upgrade-latest-planService";

import { ContactUsSocialPage } from "../pages/support/contact-us/contact-us-social/contact-us-social";
import { ContactUsHotlinePage } from "../pages/support/contact-us/contact-us-hotline/contact-us-hotline";
import { SimReplacementPage } from "../pages/services/sim-replacement/sim-replacement";
import { SimReplaConfirmPage } from "../pages/services/sim-replacement/sim-repla-confirm/sim-repla-confirm";
import { SimReplacementProvider } from "../pages/services/sim-replacement/sim-replacementProvider";
import { DeliveryOptionsPage } from "../pages/services/sim-replacement/delivery-options/delivery-options";
import { DeliveryInformationPage } from "../pages/services/sim-replacement/delivery-information/delivery-information";
import {FirstStyleOneoffPage} from "../pages/privileges/first-style/first-style-one-off/first-style-one-off";
import { ScrollToModule } from "@nicky-lenaers/ngx-scroll-to";
import { FirstSytleOptinPage } from "../pages/privileges/first-style/first-style-optin/first-style-optin";
import { FirstStyleSelectTypePage } from "../pages/privileges/first-style/first-style-select-type/first-style-select-type";

import { PayBillService } from '../pages/pay-bill/payBillService';
import { TransactionHistroyService } from '../pages/transaction-history/transactionHistroyService';

//
import {AutoBillingService} from '../pages/services/auto-billing/AutobillingService';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { SMS } from '@ionic-native/sms';
import { AppMinimize } from '@ionic-native/app-minimize';
import { PaymentMethodsService } from '../pages/settings/payment-methods/paymentMethodsService';
import { CreditManageService } from '../pages/services/credit-manage/CreditManageService';
import { ConnectTacTransferPage } from '../pages/connect-tac-transfer/connect-tac-transfer';
import { ProductDetailsPage } from '../pages/privileges/first-style/product-details/product-details';
import { FileOpener } from '@ionic-native/file-opener';
import { File } from '@ionic-native/file';

import { SplashscreenPage } from '../pages/splashscreen/splashscreen';

//RM price filter
import { RMPrice } from '../global/pipes/rm-price';

// Analytic service
import { AnalyticsService } from '../global/analytic.service';

// Renderer service
import { RendererService } from '../global/renderer.service';

@NgModule({
  declarations: [
    celcom,
    HomePage,
    MobileConnectPage,
    ConnectTacPage,
    OnBoardingGuidePage,
    UsageIndicatorComponent,
    TransactionHistoryPage,
    SubscriptionsPage,
    SubInternetPage,
    AddOnsPage,
    IddPage,
    RoamingPage,
    SubscriptionConfirmationPage,
    ConfirmationScreensPage,
    ConfirmationScreensMydealsPage,
    SimReplaConfirmPage,
    ServicesPage,
    PrivilegesPage,
    NotificationsPage,
    SupportPage,
    SettingsPage,
    MyAccountPage,
    DetailedUsagePage,
    DetailedUsageInternetPage,
    DetailedUsageRoamingPage,
    DetailedUsageSmsPage,
    DetailedUsageTalkPage,
    DeviceSelfHelpPage,
    PaymentMethodPage,
    LogoutPage,
    SplashScreen2Page,
    NotificationTab1Page,
    NotificationTab2Page,
    NotificationTab3Page,
    NotificationTab4Page,
    NetworkLoginPage,
    NetworkLoginLegalPage,
    LoginLegalTermsPage,
    LoginLegalPrivacyPage,
    NotificationDrawer,
    MobileConnectAccountPage,
    StyleGuidePage,
    MyDealsPage,
    PlanDetailsPage,
    PlanDetailsTermsAndConditionsPage,
    MyDealsModalPage,
    NetSpeedCheckPage,
    CardDesignGuidePage,
    NetSpeedCheckInfoPage,
    RaphaelDrawerComponent,
    RaphaelDrawerSpeedTestComponent,
     ReloadPage,
    CreditReloadPage,
    InternetPlansPage,
    ReloadCodePage,
    ReloadPopoverComponent,
    AddNumberModalPage,
    SubInternetPage,
    AddOnsPage,
    IddPage,
    RoamingPage,
    PayBillPage,
    RoamingPage,
    SubscriptionConfirmationPage,
    SubscriptionSelectCountryPage,
    BillFullAmountPage,
    BillOtherAmountPage,
    BillReloadCodePage,
    ConfirmationScreensPage,
    ConfirmationScreensMydealsPage,
    ChangeCreditcardModalPage,
    ProductDetailsDesignGuidePage,
    SubscriptionProductDetailsDesignGuidePage,
    OleoleShopPage,
    CataloguePage,
    GiftsRecievedPage,
    ReloadBonusPage,
    PromotionalCampaignPage,
    ResetPasswordPage,
    CardDesignGuideExamplePage,
      GbSharePage,
    AutoBillingPage,
    OleoleCatalogueListPage,
    OleoleGiftsRecievedListPage,
    GbShareAllocatePage,
    SpeedTestPage,
    FaqPage,
    ContactUsPage,
    FeedbackPage,
    PukPage,
    FaqCategoryPage,
    TransactionHistoryPage,
    TransactionHistoryDetailsPage,
    TransactionHistoryTransactionTabPage,
    TransactionHistoryBillPaymentTabPage,
    CreditManagePage,
    ValidityExtensionPage,
    CreditTransferPage,
    CreditAdvancePage,
    TransactionHistoryDetailsPayBillPage,
    PaymentRecieptPage,
 PaymentMethodsPage,
    ViewCardDetailsPage,
    TransactionHistoryDetailsPayBillPage,
    StoreLocatorSelectStorePage,
    StoreLocatorStoreDetailsPage,
    StoreLocatorStoreFilterPage,
    StoreLocatorStoreMapPage,
    StoreFilterPipe,
    OrderByFilterPipe,
    UpcomingToLatestPipe,
    RewardsPage,
    OfferingPage,
    RedeemedPage,
    PrivRewardsPage,
    PrivOfferingPage,
    PrivRedeemedPage,
    UpgradeLatestPlanPage,
    ContactUsSocialPage,
    ContactUsHotlinePage,
    SimReplacementPage,
    DeliveryOptionsPage,
    DeliveryInformationPage,
    FirstSytleOptinPage,
    FirstStyleOneoffPage,
    FirstStyleSelectTypePage,
    CountrySubscriptionsPage,
    ConnectTacTwoPage,
    ConnectTacTransferPage,
    ProductDetailsPage,
    OleOleTermsConditionsPage,
    RMPrice,
    SplashscreenPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(celcom, {
      backButtonText: '',
      mode: 'ios',
      iconMode: 'ios',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      pageTransition: 'ios-transition',
      platforms: {
        ios: {
          statusbarPadding: true,
          autoFocusAssist: false
        },
        android: {
          statusbarPadding: true,
          autoFocusAssist: false
        }
      }
    }),
    SuperTabsModule.forRoot(),
    ScrollToModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    celcom,
    HomePage,
    MobileConnectPage,
    ConnectTacPage,
    //TermsConditionsPage,
    OnBoardingGuidePage,
    TransactionHistoryPage,
    SubscriptionsPage,
    SubInternetPage,
    AddOnsPage,
    IddPage,
    RoamingPage,
    SubscriptionConfirmationPage,
    ConfirmationScreensPage,
    ConfirmationScreensMydealsPage,
    ServicesPage,
    PrivilegesPage,
    NotificationsPage,
    SupportPage,
    SettingsPage,
    MyAccountPage,
    DetailedUsagePage,
    DetailedUsageInternetPage,
    DetailedUsageRoamingPage,
    DetailedUsageSmsPage,
    DetailedUsageTalkPage,
    DeviceSelfHelpPage,
    PaymentMethodPage,
    LogoutPage,
    SplashScreen2Page,
    NotificationTab1Page,
    NotificationTab2Page,
    NotificationTab3Page,
    NotificationTab4Page,
    NetworkLoginPage,
    NetworkLoginLegalPage,
    LoginLegalTermsPage,
    LoginLegalPrivacyPage,
    NotificationDrawer,
    MobileConnectAccountPage,
    StyleGuidePage,
    MyDealsPage,
    MyDealsModalPage,
     PlanDetailsPage,
    PlanDetailsTermsAndConditionsPage,
    NetSpeedCheckPage,
    CardDesignGuidePage,
    ReloadPage,
    CreditReloadPage,
    InternetPlansPage,
    ReloadCodePage,
    CardDesignGuideExamplePage,
    ReloadPopoverComponent,
    AddNumberModalPage,
    SubInternetPage,
    AddOnsPage,
    IddPage,
    RoamingPage,
    PayBillPage,
    RoamingPage,
    SubscriptionConfirmationPage,
    SubscriptionSelectCountryPage,
    CountrySubscriptionsPage,
    BillFullAmountPage,
    BillOtherAmountPage,
    BillReloadCodePage,
    GbSharePage,
    SimReplaConfirmPage,
    NetSpeedCheckInfoPage,
    ChangeCreditcardModalPage,
    ProductDetailsDesignGuidePage,
    SubscriptionProductDetailsDesignGuidePage,
    OleoleShopPage,
    CataloguePage,
    GiftsRecievedPage,
    ReloadBonusPage,
    PromotionalCampaignPage,
    AutoBillingPage,
    OleoleCatalogueListPage,
    OleoleGiftsRecievedListPage,

    GbShareAllocatePage,
    SpeedTestPage,
    FaqPage,
    ContactUsPage,
    FeedbackPage,
    PukPage,
    FaqCategoryPage,
    TransactionHistoryPage,
    TransactionHistoryDetailsPage,
    TransactionHistoryTransactionTabPage,
    TransactionHistoryBillPaymentTabPage,
    CreditManagePage,
    ValidityExtensionPage,
    CreditTransferPage,
    CreditAdvancePage,
    TransactionHistoryDetailsPayBillPage,
    PaymentMethodsPage,
    ViewCardDetailsPage,
    RewardsPage,
    OfferingPage,
    RedeemedPage,
    PrivRewardsPage,
    PrivOfferingPage,
    PrivRedeemedPage,
    ContactUsSocialPage,
    ContactUsHotlinePage,

    TransactionHistoryDetailsPayBillPage,
    StoreLocatorSelectStorePage,
    StoreLocatorStoreDetailsPage,
    StoreLocatorStoreFilterPage,
    StoreLocatorStoreMapPage,
    UpgradeLatestPlanPage,
    SimReplacementPage,
    DeliveryOptionsPage,
    DeliveryInformationPage,
    FirstSytleOptinPage,
    FirstStyleOneoffPage,
    FirstStyleSelectTypePage,

    ResetPasswordPage,
    PaymentRecieptPage,
    ConnectTacTwoPage,
    ConnectTacTransferPage,
    ProductDetailsPage,
    OleOleTermsConditionsPage,
    SplashscreenPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LaunchNavigator,
    Uid,
    NativePageTransitions,
    Keyboard,
    EmailComposer,
    CallNumber,
    SuperTabsController,
    OpenNativeSettings,
    GlobalVars,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SuperTabsController,
    GatewayService,
    ReloadService,
    GbShareService,
    Network,
    JsonstoreService,
    LocalStorage,
    AndroidPermissions,
    Sim,Contacts,
    DashboardService,
    DetailedUsageService,
    NetworkLoginService,
    NetcheckerProvider,
    SpeedcheckerProvider,
    HandleError,
    AlertService,
    MobileConnectService,
   InAppBrowser,
   DatePipe,
   SubscriptionsTabService,
   SubscriptionService,
   PayBillService,
   TransactionHistroyService,
   AutoBillingService,
   Contacts,
   PUKService,
   SpeedTestProvider,
   CreditTransferService,
   ValidityExtensionService,
   UpgardeLatestPlanService,
   StoreLocatorService,
   SMS,
   PaymentMethodsService,
   MyDealProvider,
   OleoleService,
   MyDealProvider,
   AppMinimize,
   CreditManageService,
   SimReplacementProvider,FileOpener,File,
   RendererService,
   AnalyticsService
  ]
})
export class AppModule {}
