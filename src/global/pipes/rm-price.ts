import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'RMprice'
})
export class RMPrice implements PipeTransform {
  transform(val: any, args: string): any {
    var whiteSpace = new RegExp(/^\s+$/).test(val);

    if (val != "undefined" && val != "NA" && val != null && val && !whiteSpace) {
      if (!isNaN(val)) {
        return this.getUpdatedValue(val);
      } else if (val.substring(0, 2) == "RM") {
        return this.getUpdatedValue(Number(val.substring(2)).toFixed(2));
      } else {
        return this.getDefaultvalue(args);
      }
    } else {
      return this.getDefaultvalue(args);
    }
  }

  getUpdatedValue(val) {
    if (val > 0 || val == 0) {
      return "RM" + parseFloat(val).toFixed(2);
    } else {
      var positiveVal: any = Math.abs(val);
      return "-RM" + parseFloat(positiveVal).toFixed(2);
    }
  }

  getDefaultvalue(args) {
    if (args != "" && args == "nodash") {
      return "";
    } else if (args != "" && args == "doubledash") {
      return "--";
    } else {
      return "_";
    }
  }
}