// import { Headers, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { AlertController,Events  } from 'ionic-angular';


// pages
import { ConnectTacPage } from './../pages/login/connect-tac/connect-tac';

// service
import { ConstantData } from '../global/constantService';
import { GatewayService } from '../global/utilService';
import { DashboardService } from '../pages/home/homeService';
import { AlertService } from '../global/alert-service';
import { HandleError } from '../global/handleerror';
import { ReloadPage } from './../pages/reload/reload';
import { SubscriptionService } from '../pages/subscriptions/subscriptionService';
import {ReloadService} from '../global/reloadServices';

@Injectable()
export class OleoleService {
    
    giftListObj : any;
    mcpGiftListObj : any;
    xpaxGiftListObj : any;
    title: string;

    //oleole gift
    loader : any;
    userInfo : any;
    buttonText:any;

    isActiveNum : boolean = false;
    constructor(   
        public gatewayService:GatewayService,
        public dashboardService : DashboardService,
        public handleerror: HandleError,
        private alertCtrlInstance: AlertService,
        public alertCtrl: AlertController, 
        public subscriptionService:SubscriptionService,
        public reloadService:ReloadService,
        public events: Events) {
           
        this.events.subscribe("tittle", (tittle) => {
            this.title = tittle;
        });  

        }

    SetGiftList(obj) {
        console.log("SetGiftList ",JSON.stringify(obj));
        this.giftListObj = obj;
    }

    GetGiftList() {
        console.log("GetGiftList ",JSON.stringify(this.giftListObj));
        return this.giftListObj.VOUCHERCATALOGUE.VOUCHER;        
        // return this.giftListObj.ProductInquiryResponse.ELIGIBLE_PRODUCTS;
    }

    SetMcpGiftListObj(obj) {
        this.mcpGiftListObj = obj;
    }

    GetMcpGiftListObj() {
        return this.mcpGiftListObj;
    }

    SetXpaxGiftListObj(obj) {
        console.log("SetXpaxGiftListObj ",obj);
        this.xpaxGiftListObj = obj;
    }

    GetXpaxGiftListObj() {
        this.xpaxGiftListObj;
    }
  

   
    callGiftAdapter(params): Promise<any> {
        console.log("callGiftAdapter ",JSON.stringify(params));

        var _that = this;
        _that.loader = _that.gatewayService.LoaderService();
        //loader.duration = 2000;
        _that.loader.present();

        var funcName = "GiftTreq2";

        return new Promise((resolve, reject) => {
            _that.gatewayService.CallAdapter(ConstantData.adapterUrls.GiftTreq2,"POST",params).then(function(res){
                console.log("********************************************** component success gift MCP & Xpax "+JSON.stringify(res));
                _that.loader.dismiss();

                var results_json = JSON.parse(JSON.stringify(res));   
                // _that.alertToShow(result);
            
                _that.handleerror.handleErrorGiftPurchase(results_json,funcName).then((result) => {
                    if(result == true) {
                        console.log("MCP & Xpax gift success ",result);
                        resolve(res);
                        // _that.navCtrl.push(ConnectTacPage);
                        _that.loader.dismiss();
                        // _that.alertToShow(results_json);          
                    }
                    else {
                        console.log("MCP & Xpax gift else ",result);
                        _that.loader.dismiss();
                        resolve(res);
                        // _that.alertToShow(results_json);
                        // reject("error : no data found.");
                        // _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
                    }

                },(err)=>{
                    console.log("MCP & Xpax gift  err ",err);
                    _that.loader.dismiss();
                     
                    // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "You'll receive an SMS confirmation shortly.", "OK");
                    const alert_success = _that.alertCtrl.create({
                        title: 'Hang on. We are working on your request.',
                        // subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
                        message: "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.",
                        buttons: [
                          {
                            text: 'OK',
                            handler: () => {
                              console.log('Call to showAlertForHangOnes show alert 1 Action Clicked');
                            //   this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - (2 + 1)));
                            // resolve(JSON.parse(JSON.stringify({"status":-1,"responseText":"","responseHeaders":{},"errorMsg":"Request timed out.","errorCode":"REQUEST_TIMEOUT"})));
                            resolve(err);
                            }
                          }],
                        cssClass: 'success-message'
                    });
                    alert_success.present();
                }); 
            
            })
            .catch(function(e){
                console.log("******************************************** component failure MCP & Xpax gift  "+e);
                console.log("error user info MCP & Xpax gift  ConfirmationScreensPage");
                // reject(e);
                _that.loader.dismiss();
                if(e=="No internet connection")
                return;
                // _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK"); 
                // for timeout and incorrect response
                // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "You'll receive an SMS confirmation shortly.", "OK");
                const alert_success = _that.alertCtrl.create({
                    title: 'Hang on. We are working on your request.',
                    // subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
                    message: "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.",
                    buttons: [
                      {
                        text: 'OK',
                        handler: () => {
                          console.log('Call to showAlertForHangOnes show alert 2 Action Clicked');
                        //   this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - (2 + 1)));
                        resolve(JSON.parse(JSON.stringify(e)));
                        }
                      }],
                    cssClass: 'success-message'
                });
                alert_success.present();


                // _that.alertCtrlInstance.showAlert("Hang on. We are working on your request.", "Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated.", "OK");
                
            });
        });
    }


    alertShow(result,confirmDetails,mobileNo, send_or_req)  {
        console.log("OleoleService -> alertShow() -> send_or_req = ",send_or_req,"\n result = ",JSON.stringify(result));
        // MIPurchaseResponse
        this.userInfo = this.dashboardService.GetUserInfo();
        if(this.userInfo.ListOfServices.Services.Pre_Pos_Indicator == 'Prepaid') {
            this.buttonText="Reload";
        }else{
            this.buttonText="Paybill";
        }

       
        

        console.log("alertShow");
        return new Promise((resolve, reject) => {
            var result_info = result.MAXUPPurchaseResponse != undefined ? result.MAXUPPurchaseResponse : (result.MIPurchaseResponse != undefined  ? result.MIPurchaseResponse : result.ROAMPASSPURCHASERESPONSE);
            var insufficientBal_Check;
        
            if(result_info==result.MIPurchaseResponse){
               if(result_info.ErrorDesc=="insufficient credit balance" || result_info.Error_Code=="Balance-IS-insufficient") {
                insufficientBal_Check=true;
               }else{insufficientBal_Check=false;}
            }else if(result_info==result.MAXUPPurchaseResponse){
                if(result_info.ErrorDesc=="Insufficient credit balance"){
                    insufficientBal_Check=true;
                }else{insufficientBal_Check=false;}
            }else{
                if(result_info.ErrorDesc=="Insufficient credit balance"){
                    insufficientBal_Check=true;
                }else{insufficientBal_Check=false;}
            }
            if(result_info.ResponseMessage == "Success" || (result_info.PURCHASE_STATUS == "0" || result_info.PURCHASE_STATUS == 0)) {

                // this.alertCtrlInstance.showAlertToHandleNavigate(
                //   "Congratulation!", 
                //   ((this.confirmDetails.PLAN_DESCRIPTION != undefined ? this.confirmDetails.PLAN_DESCRIPTION : this.confirmDetails.PASS_NAME)+" "+"has been added to mobile "+this.mobileNo), 
                //   "Done",this.navCtrl
                    // );
                this.gatewayService.setFlagForDashboardReload(true); 
                const alerts = this.alertCtrl.create({
                    title: (send_or_req == 'requesting' ? 'Hope You Get Your Gift Soon':(send_or_req == 'sending'? 'Somebody&#39;s Gonna Be Happy': (send_or_req == 'self'? 'Yay!': 'Yay!') )),
                    // subTitle: ,
                    // message: (confirmDetails.PLAN_DESCRIPTION != undefined ? confirmDetails.PLAN_DESCRIPTION : confirmDetails.PASS_NAME)+" "+"has been "+(send_or_req == 'requesting'? 'request successfully from ' : 'added to ')+mobileNo,

                    // mobile number has gifted oleole_name to mobile number
                    //Change by Kush
                    message: ( (send_or_req == 'sending') ? '6'+this.gatewayService.GetMobileNumber()+' has gifted '+(confirmDetails.PLAN_DESCRIPTION != undefined ? confirmDetails.PLAN_DESCRIPTION : confirmDetails.PASS_NAME) : this.title+" "+(confirmDetails.PLAN_DESCRIPTION != undefined ? confirmDetails.PLAN_DESCRIPTION : confirmDetails.PASS_NAME))+(send_or_req == 'requesting'? ' request has been sent to ' : (send_or_req == 'self' || send_or_req == 'subscription'? ' has been added to ':' to ') )+mobileNo,
                    buttons: [
                        {
                            text: 'Done',
                            cssClass:'error-button-float-right submit-button',
                            handler: () => {
                            console.log('Call to Action Subscriptions');              
                                //this.navCtrl.popToRoot();
                                resolve(true);
                            }
                        }
                    ],
                    cssClass: 'success-message'
                });
                alerts.present();
            }
          
               else{

               
            if(insufficientBal_Check==true){
               
                  
                const alerts = this.alertCtrl.create({
                    title: 'Uh Oh. Your Credit Balance is a Bit Low',
                    // subTitle: ,
                    message: "Please top-up your credit balance before proceeding",
                    // (this.confirmDetails.PLAN_DESCRIPTION != undefined? this.confirmDetails.PLAN_DESCRIPTION : this.confirmDetails.PASS_NAME)+" "+"has not been added to mobile "+this.mobileNo,
                    buttons: [           
                        {
                          
                            text: this.buttonText,
                            cssClass:'submit-button',
                            handler: () => {
                                console.log('Navigate to reload');
                               // this.subscriptionService.notifyObservers();
                               this.events.publish('GotoReload');
                                // this.navCtrl.setRoot(ReloadPage);
                                // this.navCtrl.popToRoot();
                               // resolve(false);
                            }
                        },{
                            text: 'Cancel',
                            cssClass:'submit-button',
                            handler: () => {
                                console.log('Cancel clicked');
                                // this.navCtrl.popToRoot();
                                resolve(false);
                            }
                        }
                    ],
                    cssClass: 'success-message error-message',
                    enableBackdropDismiss: false
                });
                alerts.present();

            }
            else{
                // this.alertCtrlInstance.showAlertToHandleNavigate(
               //   "Failed!", 
               //   ((this.confirmDetails.PLAN_DESCRIPTION != undefined? this.confirmDetails.PLAN_DESCRIPTION : this.confirmDetails.PASS_NAME)+" "+"has not been added to mobile "+this.mobileNo), 
               //   "Try Again Later.",this.navCtrl
               // );
               const alerts = this.alertCtrl.create({
                //    title: (send_or_req != 'subscription'? 'Uh Oh. Ole Ole is Busy':'Uh Oh. Add-On System is Busy'),
                   // subTitle: ,
                //    title: (send_or_req != 'subscription'? 'Uh Oh. Ole Ole is Busy':'Uh Oh. Add-On System is Busy'),
                title: (send_or_req != 'subscription'? 'Uh Oh. OleOle is Busy':' Hang on. We are working on your request.'),
                  
                //    message:(send_or_req == 'requesting'? 'System is unable to request for a gift':(send_or_req == 'sending')? 'System is unable to send a gift':'System is unable to add the subscription to the '+this.userInfo.ListOfServices.Services.Pre_Pos_Indicator+' user'),
                   message:(send_or_req == 'requesting'? 'Please try again later':(send_or_req == 'sending')? 'Please try again later':"Upon successful subscription, you'll receive an SMS confirmation shortly. Please wait while your subscription detail is being updated."),

                   //message: (confirmDetails.PLAN_DESCRIPTION != undefined? confirmDetails.PLAN_DESCRIPTION : confirmDetails.PASS_NAME)+" "+"has not been "+(send_or_req == 'requesting'? 'requested successfully from ' : 'added to ')+mobileNo,
                   

                   // (this.confirmDetails.PLAN_DESCRIPTION != undefined? this.confirmDetails.PLAN_DESCRIPTION : this.confirmDetails.PASS_NAME)+" "+"has not been added to mobile "+this.mobileNo,
                   buttons: [           
                       {
                           text: 'OK    ',
                           cssClass:'error-button-float-right submit-button',
                           handler: () => {
                               console.log('Failed Call to Action Subscriptions');
                               // this.navCtrl.popToRoot();
                               resolve(false);
                           }
                       }
                   ],
                   cssClass: 'success-message error-message'
               });
               alerts.present();
            
           }
            }
           
        });
    }

    SetInactive(val) {
        this.isActiveNum = val;
    }

    GetInactive() {
        return this.isActiveNum;
    }

    isInactiveNumber(numberToVerify) {
  
        var selectedNumber = numberToVerify.replace(/[^0-9]/g, "");
        return new Promise((resolve, reject) => {
            this.checkAddedNewNumber(selectedNumber).then((res)=> {
                console.log("isInactiveNumber check "+JSON.stringify(res));
                var serviceIndicator = res["Envelope"].Body.SBLCustomerRetrieveENTOutputCollection.SBLCustomerRetrieveENTOutput.ListOfServices.Services.ServiceType;
                
                console.log("Oleole service isInactiveNumber ",serviceIndicator);

                if(serviceIndicator== "Prepaid" || serviceIndicator=="CNVRGTPrepaid" || serviceIndicator=="Postpaid" || serviceIndicator=="CNVRGTPostpaid"){
                    
                    if(selectedNumber.substring(0,3)=="011" && selectedNumber.length==10){
                        this.SetInactive(false);
                        // this.events.publish('isActiveNum', false,"2");
                        
                        resolve(false);
                        this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid Number", "Please input a valid celcom phone number. (If starting with 011 then put 11 digits).", "Try again");
                    }
                    else {
                        this.SetInactive(true);
                        // this.events.publish('isActiveNum', true,"1");                        
                        console.log("isInactiveNumber() = ",true);
                        resolve(true);
                    }
                }
                else{
                    if(this.gatewayService.isPrePaid()){
                        this.SetInactive(false);
                       
                        
                        resolve(false);                     
                        this.alertCtrlInstance.showAlert("Uh Oh. Fingers Too Fast?", "Please enter a valid Celcom number (10 or 11 digits only)", "Try again");  
                    }else{
                        this.SetInactive(false); 
                        
                        
                        resolve(false);   
                        this.alertCtrlInstance.showAlert("Uh Oh. Fingers Too Fast?", "Please enter a valid Celcom number (10 or 11 digits only)", "Try again"); 
                    }   
                    
                    // this.events.publish('isActiveNum', false,"3");
                }
        
            }).catch(err => {
                
                console.log("*****oleole check number is inactiveor not*****11**"+JSON.stringify(err));
                if(err=="No internet connection")
                return;

                this.SetInactive(false);
                // this.events.publish('isActiveNum', false,"4");
                //this.alertCtrlInstance.showAlert("Notification", "Not a celcom number.", "OK");
               // this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid Number", "Invalid Mobile number - Please enter Celcom number only (Wrong number/ not celcom num/ too long..)", "OK");
                this.alertCtrlInstance.showAlert("Uh Oh. That's Not a Valid Number", "Please enter a valid Celcom number (10 or 11 digits only)", "OK");
                resolve(false); 
                // alert("Fetch Dashboard Info onChange "+JSON.stringify(err));//FetchAllDataPostpaidPrepaid
            });
        });    
      
    }

    // for oleole bnumber (whitelist and non-whitelist)
    checkAddedNewNumber(addedNumber) {
        var _that = this;
        //alert("fetch info mobile number"+this.mobileNumber);
        try {
          var params = {
            "SERIAL_NUM": addedNumber//this.localStorage.get("MobileNum","")
          }
          return new Promise((resolve, reject) => {
            this.gatewayService.CallAdapter(ConstantData.adapterUrls.GetNumberValidation, "POST", params).then(function (res) {
              console.log("*********************************** component success checkAddedNewNumber for Oleole bnumber " + JSON.stringify(res));
              _that.handleerror.handleErrorFetchUserInfo(res, "FetchUserInfo").then((result) => {
                if (result == true) {
                  console.log("hai if  for Oleole bnumber  ", result);
                  resolve(res);
                }
                else {
                  console.log("hai else  for Oleole bnumber  ", result);
                  reject("error : no data found.");
                }
    
              }, (err) => {
                console.log("hai else err  for Oleole bnumber  ", err);
                reject("error : no data found.");
              });
            })
              .catch(function (e) {
                console.log("******************************* component  failure checkAddedNewNumber  for Oleole bnumber " + e);
                //alert("error user info FetchUserInfo");
    
                reject(e);
              });
          });
        } catch (e) {
          //alert("error"+JSON.stringify(e));
        }
      }
    
}