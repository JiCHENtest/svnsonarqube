import { Injectable } from '@angular/core';


declare var cordova;
/*
  Generated class for the NetcheckerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NetcheckerProvider {

  registeredMsisdn ;
  registeredId;

  msisdnText : any;
  registrationId : any;

  constructor() {
    console.log('Hello NetcheckerProvider Provider');    
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad NetSpeedCheckPage');
  }

  startService() {
    console.log('startService()');
    cordova.plugins.aptus.startService(function(success) { alert("Background service started"); }, function(err) { alert("Error: " + err); });
  }
  
  startServiceWithNotification() {
    console.log('startServiceWithNotification()');
    
    var settings = {
      display_notification : true
    };

    
    cordova.plugins.aptus.startService(function
      (success){ 
        alert("Background service started"); 
      }, 
      function(err) {
         alert("Error: " + err); 
      }, settings);
  }
  
  stopService() {
    console.log('stopService()');
    cordova.plugins.aptus.stopService();
  }
  
   requestSettings() {
    console.log("requestSettings");
    cordova.plugins.aptus.requestSettings();
  }
  
   sendData() {
    console.log("sendData");
    cordova.plugins.aptus.sendData();
  }
  
  refreshMsisdn(num: any) : any{
    console.log("refreshMsisdn");
    var _that = this;
    try{
        return new Promise((resolve, reject) => {
            return cordova.plugins.aptus.getMsisdn(function(msisdn : any) {
                console.log("getMsisdn: -" + msisdn);
                
                _that.registeredMsisdn = msisdn;

                if (_that.registeredMsisdn == "") {
                    console.log("refreshMsisdn if");
                    //   document.getElementById('msisdnText').textContent = "MSISDN: Undefined--"; 
                    _that.msisdnText = "Undefined--";            
                } 
                else {
                    if(num == 1) {
                        _that.startService();
                        console.log(" num startService ",num);
                    }
                    else if(num == 2) {
                        _that.startServiceWithNotification();
                        console.log(" num startServiceWithNotification ",num);
                    }
                    console.log("refreshMsisdn else");          
                    //   document.getElementById('msisdnText').textContent = "MSISDN: " + _that.registeredMsisdn;
                    _that.msisdnText = _that.registeredMsisdn;            
                }


                cordova.plugins.aptus.getRegistrationId(function(registrationId) {
                    console.log("getRegistrationId:" + registrationId);
                    _that.registeredId = registrationId;
                    if (_that.registeredId == null || _that.registeredId == "") {
                        //   document.getElementById('registrationId').textContent = "Registration ID: Not Registered";
                        resolve(registrationId);
                        _that.registrationId = "Not Registered";
                    } else {
                        //   document.getElementById('registrationId').textContent = "Registration ID: " + _that.registeredId;
                        _that.registrationId = _that.registeredId;
                        resolve(registrationId);
                    }
                });
            });
        });
    }
    catch(e){
      console.log("Error 1 ",e);
    }
   
  }
  
   promptForMsisdn() : any{
        if (this.registeredMsisdn == null || this.registeredMsisdn == "" || this.registeredId == null || this.registeredId == "") {
            var msisdn = prompt("Please enter your phone number", "");
            return new Promise((resolve, reject) => {
                if (msisdn != null && msisdn.length > 0) {
                    cordova.plugins.aptus.setMsisdn(msisdn);
                    return this.refreshMsisdn(1).then(res =>{
                            console.log("promptForMsisdn in ",res);
                            resolve(res);
                    },err =>{
                        console.log("promptForMsisdn in err ",err);
                        reject();
                    });

                    //   this.startService();
                }
            });
        
        } else {
            this.startService();
            console.log("promptForMsisdn else ");
        }
        
  }
  
  promptForMsisdnWithNotification() : any {
    if (this.registeredMsisdn == null || this.registeredMsisdn == "" || this.registeredId == null || this.registeredId == "") {
        var msisdn = prompt("Please enter your phone number", "");
        return new Promise((resolve, reject) => {
            if (msisdn != null && msisdn.length > 0) {
                cordova.plugins.aptus.setMsisdn(msisdn);
                return this.refreshMsisdn(2).then(res =>{
                    console.log("promptForMsisdnWithNotification in ",res);
                    resolve(res);
                },err =>{
                    console.log("promptForMsisdnWithNotification in err ",err);
                    reject();
                });

                //   this.startServiceWithNotification();
            }  
        });
    } else {
        this.startServiceWithNotification();
    }
  }

  getMsisdnText() : any {
      console.log("GetMsisdnText() msisdnText ",this.msisdnText);
      return this.msisdnText;
  }

  getRegistrationId() : any {
    console.log("getRegistrationId() registrationId ",this.registrationId);
    return this.registrationId;
  }
  
}
