import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AnalyticsService {


  private page_name: string;
  private profile_segment: string;
  private journey_name : string
  private journey_depth : string;
  private transact_product: string;
  private transact_value: string;
  private transact_method: string;
  private transact_result: string;


  

  constructor() {
    
    this.page_name = "";
    this.profile_segment = "";
    this.journey_name = "";
    this.journey_depth = "";
    this.transact_product = "";
    this.transact_value = "";
    this.transact_method = "" ;
    this.transact_result = "";
  }

  //Below code is for parent child communication Via a service layer
  // Observable string sources
 // private isAllComponentLoaded = new Subject<boolean>();

  // Observable string streams
//  componentLoadConfirmed$ = this.isAllComponentLoaded.asObservable();

  // Service message commands 
 

  /**
   * For page name details
   * eg: "First Gold | Postpaid Plans | Celcom"
   */
  get pageName(): string {
    return this.page_name;
  }

  set pageName(value: string) {
    this.page_name = value;
  }

 

get profileSegement(): string{
  return this.profile_segment;
}
set profileSegement(value: string){
  this.profile_segment = value;
}


get journeyName(): string{
  return this.journey_name;
}
set journeyName(value: string){
   this.journey_name = value;
}


get jouneryDepth(): string{
  return this.journey_depth;
}
set jouneryDepth(value: string){
   this.journey_depth = value;
}

get transactProduct(): string{
  return this.transact_product;
}
set transactProduct(value: string){
   this.transact_product = value;
}

get transactValue(): string{
  return this.transact_value;
}
set transactValue(value: string){
   this.transact_value = value;
}

get transactMethod(): string{
  return this.transact_method;
}
set transactMethod(value: string){
   this.transact_method = value;
}

get transactResult(): string{
  return this.transact_result;
}
set transactResult(value: string){
   this.transact_result = value;
}





 


  /**
   * For internal purpose to check number of component for a page
   */
  

  /**
   * Method to get all analytics data
   */
  getAllAnalyticsData() {
    let contextData = {
      page_name: this.pageName,
      profile_segment :this.profile_segment,
      journey_name : this.journey_name,
      journey_depth : this.journey_depth,
      transact_product : this.transact_product,
      transact_value : this.transact_value,
      transact_method : this.transact_method,
      transact_result : this.transact_result,
    }
    return contextData;
  }

  /**
   * Method to reset all the data
   */
  resetAllAnalyticData() {
    this.pageName = "";
    this.profile_segment = "";
    this.journey_name = ""; 
    this.journey_depth = "";  
    this.transact_product = "";
    this.transact_value = "";
    this.transact_method = "";
    this.transact_result = "";

  }

  getAvailableAnalyticsData() {
    let contextData: any = {};
    if (this.pageName != "") contextData.page_name = this.pageName;
    if (this.profile_segment !== "") contextData.profile_segment = this.profile_segment;
    if (this.journey_name !== "") contextData.journey_name = this.journey_name;
    if (this.journey_depth !== "") contextData.journey_depth = this.journey_depth;
    if (this.transact_product !== "") contextData.transact_product = this.transact_product;
    if (this.transact_value !== "") contextData.transact_value = this.transact_value;
    if (this.transact_result !== "") contextData.transact_result = this.transact_result;
    if (this.transact_method !== "") contextData.transact_method = this.transact_method;

    return contextData;
  }
}
