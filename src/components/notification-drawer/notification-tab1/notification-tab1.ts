import { Component, ViewChild, NgZone } from "@angular/core";
import { NavController, NavParams, Nav, Events, ModalController } from "ionic-angular";
import { GlobalVars } from "../../../providers/globalVars";
import { GatewayService } from '../../../global/utilService';
import { AlertService } from '../../../global/alert-service';
import { PayBillPage } from "../../../pages/pay-bill/pay-bill";
import { DashboardService } from '../../../pages/home/homeService';
import { ReloadPage } from '../../../pages/reload/reload';
import { SubscriptionsPage } from '../../../pages/subscriptions/subscriptions';
import { CreditAdvancePage } from '../../../pages/services/credit-manage/credit-advance/credit-advance';
import { ValidityExtensionPage } from "../../../pages/services/credit-manage/validity-extension/validity-extension";
import { AlertController } from 'ionic-angular';
import { MyDealProvider } from '../../../global/mydealservice';
import { ProductDetailsDesignGuidePage } from '../../../pages/product-details-design-guide/product-details-design-guide';
import { MyDealsPage } from '../../../pages/my-deals/my-deals';
import { ConstantData } from '../../../global/constantService';
import { ConfirmationScreensMydealsPage } from "../../../pages/confirmation-screens-mydeals/confirmation-screens-mydeals";
import { HandleError } from '../../../global/handleerror';

/**
 * Generated class for the NotificationTab1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-notification-tab1',
  templateUrl: 'notification-tab1.html'
})
export class NotificationTab1Page {
  @ViewChild(Nav) nav: Nav;
  capZoneCss: any = this.globalVar.getCapZoneStatus();
  callbarredCss: any = this.globalVar.getcallBarredStatus();
  disablePromiseToPay: boolean = true;
  isPrePaid: any;
  prepaidCurrentBalance: any;
  billAmount: any;
  showCreditLimitNotification: boolean = false;
  showCreditExpiryNotification: boolean = false;
  billingCycle: any;
  expiryDate: any;
  todayDate: any;
  dayDiff: any;
  isReloadDashboard: any;
  showCreditAdvanceNotification: boolean = false;
  tempDate: any;
  todayDateForCompare: any;
  showValidityExtension: any;
  daysLeft: any;
  internetDataObj: any;
  internetUsagePercentage: any;
  showInternetUsageNotification: any;
  dashboardRefreshPerformed: any = false;
  notificationCount: number;
  showKawKaw: any = false;
  RegisterFlag: any;
  kawkaw: any;
  optKawKaw: any;
  dayCount: any;
  modalOptions: any;
  myDealsList: any = {};
  

  constructor(public navCtrl: NavController,
    public events: Events,
    private alertCtrlInstance: AlertService,
    public navParams: NavParams,
    public globalVar: GlobalVars,
    public gatewayService: GatewayService,
    public dashboardService: DashboardService,
    public zone: NgZone,
    public mydealservice: MyDealProvider,
    public modalCtrl: ModalController,
    public handleerror: HandleError) {
    // alert("register observer");
    //gatewayService.registerObserver(this);
    //this.notificationObject=[];
    console.log('call barred status' + this.callbarredCss);
    console.log('cap zone status' + this.capZoneCss);
    this.events.subscribe("dashboardRefreshed", (dataObj) => {
      this.capZoneCss = this.globalVar.getCapZoneStatus();

      this.callbarredCss = this.globalVar.getcallBarredStatus();
      this.showCreditLimitNotification = false;
      this.showCreditExpiryNotification = false;
      this.showCreditAdvanceNotification = false;
      this.showValidityExtension = false;
      this.notificationCount = 0;
      this.showKawKaw = false;
      if (dataObj != undefined && dataObj != null && dataObj != "") {
        this.checkInternetBalance(dataObj);
      }
      this.dashboardRefreshPerformed = true;
      this.loadNotification();
    });
    this.myDealsList.offerList = [{}];
  }

  loadNotification() {
    this.notificationCount = 0;
    this.getPrePosInformation();
    this.getCreditBalance();
    //this.mydealsList();
  }
  ionViewWillEnter() {
    console.log('ionviewwillenter notification tab 1');

    //call promiseToPay eligibility check if user is in call bar or capzone
    if (this.globalVar.getCapZoneStatus() || this.globalVar.getcallBarredStatus()) {
      this.PTPEligibility();
    }
    console.log("ionViewDidLoad");
    if (!this.dashboardRefreshPerformed)
      this.loadNotification();

    this.gatewayService.CheckyouthQRY().then((res) => {

      if (res["STDNT_CHECK"].IS_ELIGIBLE == "Y" && res["STDNT_CHECK"].REGISTERED_ALRDY == "Y") {
        this.RegisterFlag = true;
        this.kawkaw = "kawkaw";
        this.showKawKaw = false;
        //alert("1. howKawKaw : "+this.showKawKaw);
      } else if (res["STDNT_CHECK"].IS_ELIGIBLE == "Y" && res["STDNT_CHECK"].REGISTERED_ALRDY == "N") {
        this.RegisterFlag = false;
        this.kawkaw = "kawkaw";
        this.showKawKaw = true;
        //alert("2. showKawKaw : "+this.showKawKaw);
      } else if (res["STDNT_CHECK"].IS_ELIGIBLE == "N") {
        this.showKawKaw = false;
        //alert("3. showKawKaw : "+this.showKawKaw);
        this.kawkaw = false;
      }
    }).catch(err => {
      this.showKawKaw = false;
      // alert("in catch showKawKaw : "+this.showKawKaw);
    });
    this.mydealsList();
    /**temp code */
    // let myDealsList = this.mydealservice.staticFetchMydeal();
    // this.myDealsList = myDealsList.getPromotionOffersResponse;
    // let now: Date = new Date();
    // now.setDate(now.getDate() - 1);
    // this.notificationCount += this.myDealsList.offerList.filter((a)=>{
    //   let date1 = new Date(a['expirationDate']);
    //   return date1.getTime() >= now.getTime();  
    // }).length; 
    // this.events.publish("updateNotificationCount", this.notificationCount);
    /**temp code */
  }

  mydealsList() {
    console.log('ionViewWillEnter NotificationDrawer');

    var _that = this;
    var loader = this.gatewayService.LoaderService();
    
    loader.present();

     
    
      var params = {
        "msisdn":_that.gatewayService.GetMobileNumber(),
      }      

      console.log("_that.gatewayService.GetMobileNumber() ",_that.gatewayService.GetMobileNumber());
      // return new Promise((resolve, reject) => {
        _that.gatewayService.CallAdapter(ConstantData.adapterUrls.FetchMydealsStatic,"POST",params).then(function(res){
            console.log("************************************* component success FetchMydeals"+JSON.stringify(res));
          _that.handleerror.handleErrorFetchMydeals(res,"FetchMydeals").then((result) => {
            
            if(result == true) {
                  console.log("hai if FetchMydeals ",result);
                  // resolve(res);
                  loader.dismiss();
                  _that.mydealservice.SetFetchMydeals(res);
                  // _that.events.publish('mydeal','hai');   
                  _that.myDealsList = res['getPromotionOffersResponse'];
                  let now: Date = new Date();
                  now.setDate(now.getDate() - 1);
                  _that.notificationCount += _that.myDealsList.offerList.filter((a)=>{
                    let date1 = new Date(a['expirationDate']);
                    return date1.getTime() >= new Date().getTime();  
                  }).length; 
                  _that.events.publish("updateNotificationCount", _that.notificationCount);
                  
              }
              else {
                  console.log("hai else FetchMydeals ",result);
                  loader.dismiss();
                  _that.mydealservice.SetFetchMydeals(res);
                  // _that.events.publish('mydeal','hai');  
                  
                  
                  // _that.alertCtrlInstance.showAlert("Notification", "No data found.", "OK");
                  // reject("error : no data found.");
                  _that.myDealsList = res['getPromotionOffersResponse'];
                  let now: Date = new Date();
                  now.setDate(now.getDate() - 1);
                  _that.notificationCount += _that.myDealsList.offerList.filter((a)=>{
                    let date1 = new Date(a['expirationDate']);
                    return date1.getTime() >= new Date().getTime();  
                  }).length;  
                  _that.events.publish("updateNotificationCount", _that.notificationCount);
              }
          });
              // this.handleError(res,"FetchMydeals");
              // resolve(res);
        })
        .catch(function(e){
          console.log("****************************************** component failure FetchMydeals"+e);
          //alert("error user info FetchMydeals ");
          loader.dismiss();
          // _that.alertCtrlInstance.showAlert("Uh Oh. Please be patient. System's busy.", "Please try again later", "OK");
          // reject(e);
        });
  }

  presenMydealspageModal(mydealdeatils) {
    let modal = this.modalCtrl.create(MyDealsPage, { "mydealsdetails": mydealdeatils }, this.modalOptions);
    modal.present();
  }

  presenMydealspageCinfirmation(mydealdeatils) {
    this.navCtrl.parent.parent.push(ConfirmationScreensMydealsPage, { "mydealPlanInfo": mydealdeatils });
  }

  goToPayBill() {
    this.events.publish('goToPayBill');
    //this.nav.setRoot(PayBillPage);
  }

  promiseToPay() {
    //call promiseToPay eligibility check if user is in call bar or capzone
    if (this.globalVar.getCapZoneStatus() || this.globalVar.getcallBarredStatus()) {
      this.PTPEligibility();
    }

  }

  PTPEligibility() {
    var  userInfo  = this.dashboardService.GetUserInfo();
    var billingAccountNumber  =  userInfo.ListOfServices.Services.AssetBillingAccountNo;
    var params = { "billingAccountNumber": billingAccountNumber };
    var _that = this;
    var loader = _that.gatewayService.LoaderService();
    loader.present();
    return new Promise((resolve, reject) => {

      _that.gatewayService.PromiseToPayCall(params).then((res) => {
        console.log(res);
        if (res["outputCreditCheckResp"] != undefined) {
          if (res["outputCreditCheckResp"].statusId == 'S') {
            console.log('yes');
            // _that.globalVar.setCapZoneStatus(false);
            // _that.globalVar.setcallBarredStatus(false);
            _that.gatewayService.setFlagForDashboardReload(true);
            _that.gatewayService.setPTPEligibilityData(res);
            _that.disablePromiseToPay = false;
            //_that.alertCtrlInstance.showAlert("Success", "Promise to pay success.", "OK");
          }
        }
        else {
          _that.disablePromiseToPay = true;
        }

        resolve();
        loader.dismiss();
      }).catch(err => {
        //alert("error FetchUserInfo"+JSON.stringify(err));
        loader.dismiss();
        reject(err);//FetchUserInfo
      });
    });
  }

  checkInternetBalance(dataObj) {
    this.internetDataObj = dataObj;
    if (this.internetDataObj.dataTotal != 0.00) {
      this.internetUsagePercentage = ((this.internetDataObj.dataTotal - this.internetDataObj.dataUsed) / this.internetDataObj.dataTotal) * 100;
      this.internetUsagePercentage = (this.internetUsagePercentage).toFixed(2);
      if ((this.internetDataObj.dataTotal != this.internetDataObj.dataUsedVal) && this.internetUsagePercentage < 20.00) {
        this.showInternetUsageNotification = true;
        // this.notificationCount++;
      } else {
        this.showInternetUsageNotification = false;
      }
    } else {
      this.showInternetUsageNotification = false;
    }
  }


  formatDate(val) {
    //alert("date val : "+val);
    //val="20180123235959";
    if (val == "" || val == null || val == undefined)
      return;
    var dt = val.substring(4, 6) + '/' + val.substring(6, 8) + '/' + val.substring(2, 4);
    return dt;
  };

  getCreditBalance() {
    if (this.isPrePaid) {
      this.prepaidCurrentBalance = this.dashboardService.GetBalancePrepaidData();
      //alert("dashboard service : "+JSON.stringify(this.prepaidCurrentBalance));
      var balanceRecord = this.prepaidCurrentBalance.BalanceRecordList.BalanceRecord;
      //alert("balance : "+JSON.stringify(balanceRecord));
      if (balanceRecord.length > 1) {
        for (let obj in balanceRecord) {
          if (balanceRecord[obj].AccountType == 2000) {
            let amount = (balanceRecord[obj].Balance / 10000).toString();
            this.billAmount = parseFloat(amount).toFixed(2);
            //this.billingCycle =  balanceRecord[obj].ExpireTime;  //this.formatDate(balanceRecord[obj].ExpireTime);
            // if (this.prepaidCurrentBalance.SubscriberState.ActiveStop.CDATA != undefined) {
            //     var tempExp = this.prepaidCurrentBalance.SubscriberState.ActiveStop.CDATA;
            //   } else {
            //     var tempExp = this.prepaidCurrentBalance.SubscriberState.ActiveStop;
            //   }
          }
        }
      } else {
        let amount = (this.prepaidCurrentBalance.BalanceRecordList.BalanceRecord.Balance / 10000).toString();
        this.billAmount = parseFloat(amount).toFixed(2);
      }
      if (this.prepaidCurrentBalance.SubscriberState.ActiveStop.CDATA != undefined) {
        this.billingCycle = this.prepaidCurrentBalance.SubscriberState.ActiveStop.CDATA;
      } else {
        this.billingCycle = this.prepaidCurrentBalance.SubscriberState.ActiveStop;
      }

      //alert("bill amount : "+this.billAmount);
      if (this.billAmount < 5.00) {
        this.showCreditLimitNotification = true;
        this.notificationCount++;
      } else {
        this.showCreditLimitNotification = false;
      }
      // alert("showCreditLimitNotification"+this.showCreditLimitNotification);
      this.expiryDate = this.formatDate(this.billingCycle);
      //this.todayDate= this.formatDate(new Date());
      //this.dayDiff = ((new Date(this.expiryDate) - new Date(this.todayDate)  / 1000 / 60 / 60 / 24);
      this.expiryDate = new Date(this.expiryDate);
      this.todayDate = new Date();
      this.tempDate = this.todayDate.getMonth() + 1 + "/" + this.todayDate.getDate() + "/" + this.todayDate.getFullYear();
      this.todayDateForCompare = new Date(this.tempDate);
      this.dayDiff = (this.expiryDate - this.todayDateForCompare) / 1000 / 60 / 60 / 24;
      this.dayDiff = (this.dayDiff).toFixed(2);
      this.daysLeft = parseInt(this.dayDiff) + 1;

      if (this.daysLeft == 1) {
        this.dayCount = "day";
      } else {
        this.dayCount = "days";
      }

      //alert("dayDiff : "+this.dayDiff)
      //alert("the billing cycle date : "+this.billingCycle);

      if ((parseInt(this.dayDiff) + 1 == 7) || (parseInt(this.dayDiff) + 1 == 3) || (parseInt(this.dayDiff) + 1 == 1)) {
        this.showCreditExpiryNotification = true;
        this.notificationCount++;
      } else {
        this.showCreditExpiryNotification = false;
      }
      //alert("credit Expiry notification : "+this.showCreditExpiryNotification);

      if (this.billAmount < 1.00 && this.dayDiff >= 0.00) {
        this.showCreditAdvanceNotification = true;
        //this.notificationCount++;
      } else {
        this.showCreditAdvanceNotification = false;
      }
      /**temp code */
      //this.dayDiff = -1.00;
      /**temp code */
      if (this.dayDiff < 0.00) {
        this.showValidityExtension = true;
        this.notificationCount++;
      } else {
        this.showValidityExtension = false;
      }
    }

    //this.showCreditLimitNotification=true;

    if (this.capZoneCss) {
      this.notificationCount++;
    }
    if (this.callbarredCss) {
      this.notificationCount++;
    }
    if (this.showKawKaw) {
      this.notificationCount++;
    }

    if (this.showInternetUsageNotification) {
      this.notificationCount++;
    }
    this.events.publish("updateNotificationCount", this.notificationCount);
  }
  gotoReload() {
    //this.navCtrl.setRoot(ReloadPage);
    //this.event.publish("navigateToReload");
    this.events.publish('navigateToReload');
  }

  gotoSubscription() {
    this.navCtrl.setRoot(SubscriptionsPage);
  }

  gotoCreditAdvance() {
    this.events.publish('navigateToCreditAdvance');
  }

  getPrePosInformation() {
    this.isPrePaid = this.gatewayService.isPrePaid();
    //this.presentAlert("prepaid/postpaid : "+this.isPrePaid);
  }
  gotoValidityExtension() {
    this.events.publish('navigateToValidityExtension');
  }


  redeemKawKawBonus() {
    this.optKawKaw = {
      registerFlag: this.RegisterFlag,
      kawkaw: this.kawkaw
    }
    this.events.publish("redeemKawKaw", this.optKawKaw);
  }

  updateCreditLimit() {
    var  userInfo  = this.dashboardService.GetUserInfo();
    console.log(userInfo);
    var billingAccountNumber  =  userInfo.ListOfServices.Services.AssetBillingAccountNo;
    var mobile_number  =  this.gatewayService.GetMobileNumber();
    var postpaidBillingInfo = this.dashboardService.GetPostpaidBillingInfo();
    console.log("postpaid billing info " + JSON.stringify(postpaidBillingInfo));
    var totalOutStandAmount = postpaidBillingInfo.BalanceSummaryDetails.CmuBalanceSummaryVbc.LatestAmountDue;
    var unbilledAmount = postpaidBillingInfo.BalanceSummaryDetails.CmuBalanceSummaryVbc.UnbilledUsage;

    var PTPEligibilityData = this.gatewayService.getPTPEligibilityData();
    console.log("PTPEligibilityData" + JSON.stringify(PTPEligibilityData));
    var currentCreditLimit = PTPEligibilityData.outputCreditCheckResp.CreditLimit;
    var params = {
      "BillingAccountNumber": billingAccountNumber,
      "ServiceNumber": mobile_number,
      "totalOutStandAmount": totalOutStandAmount,
      "unbilledAmount": unbilledAmount,
      "currentCreditLimit": currentCreditLimit
    };
    console.log(params);

    //var params = {"BillingAccountNumber":"153019195", "ServiceNumber": "0192539279"};
    var _that = this;
    var loader = _that.gatewayService.LoaderService();
    loader.present();
    return new Promise((resolve, reject) => {

      _that.gatewayService.updateCreditLimit(params).then((res) => {
        console.log(res);
        var status = res["Envelope"].Body.SRCreate_Output.ListOfCelservicerequestthinout.CelServiceRequestThin.Status;
        console.log();
        if (status == 'Failed') {
          console.log('udate credit limit failed');
          _that.disablePromiseToPay = false;
          _that.alertCtrlInstance.showAlert("Failed", "Your credit limit can not be upgraded.", "OK");
        }
        else {
          console.log('udate credit limit success');
          _that.globalVar.setCapZoneStatus(false);
          _that.globalVar.setcallBarredStatus(false);
          _that.callbarredCss = false;
          _that.capZoneCss = false;
          console.log('call barred status' + _that.callbarredCss);
          console.log('cap zone status' + _that.capZoneCss);

          _that.gatewayService.setFlagForDashboardReload(true);
          _that.disablePromiseToPay = true;
          _that.alertCtrlInstance.showAlert("Success", "Your credit limit has been upgraded.", "OK");
        }

        resolve();
        loader.dismiss();
      }).catch(err => {
        //alert("error FetchUserInfo"+JSON.stringify(err));
        loader.dismiss();
        reject(err);//FetchUserInfo
      });
    });
  }
}
