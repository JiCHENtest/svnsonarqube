import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: "upcomingToLatest"
})
export class UpcomingToLatestPipe implements PipeTransform{
  transform(array: any, fieldName: any): any {
    if (array) {
      let now: Date = new Date();
      now.setDate(now.getDate() - 1);
      
      array.sort((a: any, b: any) => {
        let date1: any = new Date(a[fieldName]).getTime() ;
        let date2: any = new Date(b[fieldName]).getTime();
        return date1-date2;
      });

     return array.filter((a)=>{
        let date1 = new Date(a[fieldName]);
        return date1.getTime() >= now.getTime();  
      });
    }
  }
}