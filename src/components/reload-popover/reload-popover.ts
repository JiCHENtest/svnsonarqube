import { Component } from '@angular/core';
import { NavParams, ViewController, ModalController } from 'ionic-angular';
import { AddNumberModalPage } from '../../pages/add-number-modal/add-number-modal';
import { GatewayService } from '../../global/utilService';
import { ReloadService } from '../../global/reloadServices';
import { AlertService } from '../../global/alert-service';
import { PayBillService } from '../../pages/pay-bill/payBillService';
import { CreditManageService } from '../../pages/services/credit-manage/CreditManageService';
/**
 * Generated class for the ReloadPopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'reload-popover',
  templateUrl: 'reload-popover.html'
})
export class ReloadPopoverComponent {

  text: string;
  mobileList:string[]=[];
  allNumber:string[]=[];
  selected: string = "friends";
  checkBooked :boolean = false;
  constructor(public viewCtrl: ViewController, public navParams: NavParams, public modalCtrl: ModalController, public gatewayService:GatewayService,
    public reloadService:ReloadService, public paybillService:PayBillService, public creditMService:CreditManageService) {
    console.log('Hello ReloadPopoverComponent Component');
    this.text = 'Hello World';
    this.selected = this.navParams.get('selected');
    this.mobileList = this.navParams.get('allNumber');
  }

  dismiss() {
    console.log('******* ReloadPopover ***');
if(this.checkBooked == true)
this.viewCtrl.dismiss();
else
    this.viewCtrl.dismiss(this.selected);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ReloadPopover ***');
  }

  presenAddNumberModal() {
    console.log('*****ionViewDidLoad ReloadPopover ***');
    this.checkBooked = true;
    let modal = this.modalCtrl.create(AddNumberModalPage);
    modal.present();
    modal.onDidDismiss(data => { 
     // alert("getNumber"+data);
     if(this.gatewayService.isPrePaid()){
      if(data){
        this.reloadService.setPhonebookNumber(data);
        this.creditMService.setPhonebookNumber(data);
      }else{
        this.reloadService.setPhonebookNumber(data);
        this.creditMService.setPhonebookNumber(data);
      }
}else{

  if(data){
    this.paybillService.setPhone_BookNumber(data);
  }else{
    this.paybillService.setPhone_BookNumber(data);
  }

}


     
     });
   this.dismiss();
  }

}
